/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.junit.Test;

/**
 * @author dpaniagua.
 *
 */
public class ReportConvertFormatTest {

	@Test
	public void testValidarExtensionArchivo() {
		try {
//			File file = new File("C:/Users/Diego/Documents/prueba_anexo.pdf");		
//			File file = new File("C:/Users/Diego/Documents/CV Ing Diego Paniagua.docx");		
//			File file = new File("C:/Users/Diego/Documents/patineta.gif");		
//			File file = new File("C:/Users/Diego/Documents/madagascar.jpg");		
//			File file = new File("C:/Users/Diego/Documents/Incidencias.xlsx");
			File file = new File("C:/Users/Diego/Documents/HolaDiegoExcel.xls");
			
			DataInputStream input = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
			int fileSignature = input.readInt();
			System.out.println(fileSignature);
			
			String extensionFile = FilenameUtils.getExtension(file.getAbsolutePath());
			input.close();			
			
			if (extensionFile.equals("pdf")) {
				if (fileSignature != 0x25504446) {
					System.out.println("El archivo esta corrupto");
					throw new IOException();
				}
			} else if (extensionFile.equals("png")) {
				if (fileSignature != 0x89504E47) {
					System.out.println("El archivo esta corrupto");
					throw new IOException();					
				}
			} else if (extensionFile.equals("txt")) {
				if (fileSignature == 0x686F6C61) {
					System.out.println("El archivo esta corrupto");
					throw new IOException();					
				}
			} else if (extensionFile.equals("doc")) {
				System.out.println("Es un doc");
				if (fileSignature != 0xD0CF11E0) {
					System.out.println("El archivo esta corrupto");
					throw new IOException();					
				}
			} else if (extensionFile.equals("docx")) {
				System.out.println("Es un docx");
				if (fileSignature != 0x504B0304) {
					System.out.println("El archivo esta corrupto");
					throw new IOException();					
				}
			} else if (extensionFile.equals("gif")) {
				System.out.println("Es un gif");
				if (fileSignature != 0x47494638 || fileSignature != 0x47494638) {
					System.out.println("El archivo esta corrupto");
					throw new IOException();					
				}
			} else if (extensionFile.equals("jpg")) {
				System.out.println("Es un jpg o jpeg");
				if (fileSignature != 0xFFD8FFE0) {
					System.out.println("El archivo esta corrupto");
					throw new IOException();					
				}				
			} else if (extensionFile.equals("xls")) {
				System.out.println("Es un xls");
				if (fileSignature != 0xD0CF11E0) {
					System.out.println("El archivo esta corrupto");
					throw new IOException();					
				}				
			} else if (extensionFile.equals("xlsx")) {
				System.out.println("Es un xlsx");
				if (fileSignature != 0x504B0304) {
					System.out.println("El archivo esta corrupto");
					throw new IOException();					
				}				
			} else if (extensionFile.equals("dwg")) {
				System.out.println("Es un dwg");
				if (fileSignature != 0x4143) {
					System.out.println("El archivo esta corrupto");
					throw new IOException();					
				}				
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

}
