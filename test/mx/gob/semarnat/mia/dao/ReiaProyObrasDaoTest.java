/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import static org.junit.Assert.*;

import java.util.List;

import mx.gob.semarnat.mia.model.ReiaProyCondiciones;
import mx.gob.semarnat.mia.model.ReiaProyObras;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author C�sar
 *
 */
public class ReiaProyObrasDaoTest {

	@Test
	public void buscarReiaProyObrasTest() {
		String folio = "11379";
		ReiaProyObrasDAO reiaProyObrasDao = new ReiaProyObrasDAO();
		ReiaProyCondicionesDAO reiaProyCondicionesDAO = new ReiaProyCondicionesDAO();
		
		int obrasGuardadas = reiaProyObrasDao.countByFolio(folio);
		assertNotEquals(obrasGuardadas, 0);
		List<ReiaProyObras> reiaObrasTab = reiaProyObrasDao.findByFolioCategoria(folio, 16L);
		assertNotNull(reiaObrasTab);
		ReiaProyObras reiaObraGuardada = reiaObrasTab.get(0);
		System.out.println("Folio: " + reiaObraGuardada.getFolio() + " Id obra" + reiaObraGuardada.getObra().getIdObra());
		List<ReiaProyCondiciones> reiaCondicionesObra = reiaProyCondicionesDAO.findByFolioObra(folio, reiaObraGuardada.getObra().getIdObra());
		for (ReiaProyCondiciones reiaProyCondicion : reiaCondicionesObra) {
			System.out.println("Folio: " + reiaProyCondicion.getFolio() + "Condicion: " + reiaProyCondicion.getCondicion());
		}
	}
	
	@Test
	public void buscarObrasCapturadasTest() {
		String folio = "14497";
		ReiaProyObrasDAO reiaProyObrasDao = new ReiaProyObrasDAO();
		
		List<ReiaProyObras> obrasCapturadas = reiaProyObrasDao.findByFolioObrasCapturadas(folio);
		assertNotNull(obrasCapturadas);
		for (ReiaProyObras reiaProyObras : obrasCapturadas) {
			System.out.println(reiaProyObras.getClave() + " - " + reiaProyObras.getObra().getObra());
		}
	}

}
