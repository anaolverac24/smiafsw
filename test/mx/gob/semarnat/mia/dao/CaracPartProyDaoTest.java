package mx.gob.semarnat.mia.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class CaracPartProyDaoTest {

	@Test
	public void testGetCaracPartSIGEIAPROM() {
		SigeiaDao sigeiaDAO = new SigeiaDao();
		
		try {
			//Se recupera la lista de informacion georeferenciada por SIGEIA en Suelo Vegetacion Proyecto.
			List<Object[]> listaSueloVeget = sigeiaDAO.consultarCaracPartProySIGEIA("8440");
			if (listaSueloVeget.size() > 0) {
				//Se itera la lista de registros de sigeia a mostrarse en Caracteristicar Particulares del Proyecto.
				for (Object[] objects : listaSueloVeget) {
					System.out.println("NOMBRE OBRA: " + objects[0]);
					System.out.println("SUPERFICIE: " + objects[1]);
				}
				assertTrue(listaSueloVeget.size() > 0);
			} else {
				assertTrue(listaSueloVeget.size() > 0);			
			}
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
