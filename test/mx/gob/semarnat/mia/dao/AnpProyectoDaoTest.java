/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import mx.gob.semarnat.mia.model.AnpProyecto;
import mx.gob.semarnat.mia.model.ArticuloReglaAnp;
import mx.gob.semarnat.mia.model.ArticuloReglaAnpPK;

/**
 * @author C�sar
 *
 */
public class AnpProyectoDaoTest {

	/**
	 * Test para crear un nuevo ANP ne la bd
	 */
	@Test
	public void testCreateAnp() {
		MiaDao mia = new MiaDao();
		
		AnpProyecto anpProyecto = new AnpProyecto("10344",(short) 1,(short) 5);
		anpProyecto.setAnpTipo("Estatal");
		anpProyecto.setAnpNombre("Ejemplo Anp");
		anpProyecto.setAnpCategoriaManejo("Parque Estatal");
		anpProyecto.setAnpUltimoDecreto(new Date());
		anpProyecto.setAnpFechaManejo(new Date());
		
		ArticuloReglaAnpPK anpPk = new ArticuloReglaAnpPK("10344",(short) 1,(short) 5,(short) 5);
		ArticuloReglaAnp reglaAnp = new ArticuloReglaAnp(anpPk);
		reglaAnp.setAnpArticuloRegla("A");
		reglaAnp.setAnpArtNomRegla("NA");
		reglaAnp.setAnpArtNomVinculacion("Ejemplo vinculacion");
		ArticuloReglaAnpPK anpPk2 = new ArticuloReglaAnpPK("10344",(short) 1,(short) 5,(short) 6);
		ArticuloReglaAnp reglaAnp2 = new ArticuloReglaAnp(anpPk2);
		reglaAnp2.setAnpArticuloRegla("R");
		reglaAnp2.setAnpArtNomRegla("NA");
		reglaAnp2.setAnpArtNomVinculacion("Ejemplo vinculacion 2");
		
		List<ArticuloReglaAnp> listAnpReg = new ArrayList<ArticuloReglaAnp>();
		listAnpReg.add(reglaAnp);
		listAnpReg.add(reglaAnp2);
		
		anpProyecto.setArticuloReglaAnpList(listAnpReg);
		
		try {
			mia.persist(anpProyecto);
			System.out.println("Nuevo ANP guardado correctamente!");
		} catch (Exception e) {			
			e.printStackTrace();
		}
		
	}
	
	
	/**
	 * Tets para eliminar proyectos anps
	 */
	@Test
	public void testEliminarAnp() {
		MiaDao mia = new MiaDao();
		AnpProyecto anp = mia.getAnpProyectoById("10344", (short) 1, (short)5);
		
		try {			
			for (ArticuloReglaAnp anpReg : anp.getArticuloReglaAnpList()){
				mia.eliminarAnpProyectoRegla(anpReg.getArticuloReglaAnpPK().getFolioProyecto(), anpReg.getArticuloReglaAnpPK().getSerialProyecto(),
						anpReg.getArticuloReglaAnpPK().getAnpId());
			}
			mia.eliminarAnpProyecto(anp.getAnpProyectoPK().getFolioProyecto(), anp.getAnpProyectoPK().getSerialProyecto(), anp.getAnpProyectoPK().getAnpId());
			System.out.println("Anp eliminado correctamente!");
		} catch (Exception e) {			
			e.printStackTrace();
		}		
	}
	
	/**
	 * Test para crear articuloas anp
	 */
	@Test
	public void createArticuloRegla() {
		MiaDao mia = new MiaDao();
		short id = mia.getMaxArticuloReglaAnpProyecto("10344",(short) 1);
    	ArticuloReglaAnpPK pk = new ArticuloReglaAnpPK("10344",(short) 1,(short) 7, id);
    	ArticuloReglaAnp art = new ArticuloReglaAnp(pk);
    	
    	AnpProyecto anp = mia.getAnpProyectoById("10344", (short) 1, (short)7);
    	art.setAnpProyecto(anp);
    	
    	try {
			mia.persist(art);
			System.out.println("Articulo guardado correctamente!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
