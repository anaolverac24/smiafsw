/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import java.math.BigDecimal;

import javax.persistence.EntityManager;

import org.junit.Test;

import mx.gob.semarnat.mia.model.ArchivosProyecto;
import mx.gob.semarnat.mia.util.ArchivoUtil;

/**
 * @author C�sar
 *
 */
public class ArchivosProyectoDaoTest {
	
	/**
	 * Test de creacion de archivo proyecto
	 */
	@Test
	public void crearArchivoProyecto() {
		EntityManager em = EntityFactory.getMiaMF().createEntityManager();
		
		ArchivosProyecto archivo = new ArchivosProyecto("14500", (short) 1, (short) 3, (short) 6, (short) 0, (short) 0);
		archivo.setId((short) 1);
		archivo.setNombre("Test");
		archivo.setDescripcion("prueba de archivo");
		archivo.setFileName("test2");
		archivo.setUrl("url test");
		archivo.setTamanio(new BigDecimal(1500.500));
		archivo.setExtension("pdf");
		archivo.setReservado("0");
		
        try {
            em.getTransaction().begin();
            em.persist(archivo);
            em.getTransaction().commit();
            System.out.println("Archivo guardado!");
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
	}

	/**
	 * Test de la utileria de archivo
	 */
	@Test
	public void archivoUtilTest() {
		String cadena = " yo, @mi:l; t�st � | 1234567890rsrsrs";
		
		String resultado = ArchivoUtil.adaptarReglaCadena(cadena);
		System.out.println(resultado);
	}
}
