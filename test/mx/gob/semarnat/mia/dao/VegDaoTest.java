/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import mx.gob.semarnat.mia.model.VegFase;
import mx.gob.semarnat.mia.model.VegTipo;

/**
 * @author C�sar
 *
 */
public class VegDaoTest {

	/**
	 * Consulta los objetos Veg a traves de los DAO
	 */
	@Test
	public void testConsultarVegTipos() {
		VegTipoDAO vegTipoDAO = new VegTipoDAO();	
		List<VegTipo> vegTipos = vegTipoDAO.findAll();
		for (VegTipo vegTipo : vegTipos) {
			System.out.println(vegTipo.getVegNombre());
		}
		VegTipo vegTipo = vegTipoDAO.findByName("Matorral crasicaule con veg. secundaria");
		System.out.println("Buscado por nombre: " + vegTipo.getVegNombre() + " ----------");
	}
	
	/**
	 * Inserta las fases de los diferentes VegTipos
	 */
	@Test
	public void testCrearTipoFases() {
		VegTipoDAO vegTipoDAO = new VegTipoDAO();
		VegFaseDAO vegFaseDAO = new VegFaseDAO();
		
		List<String> listaTipos = new ArrayList<String>();		
		
		VegFase vegFase = vegFaseDAO.find(5);
		for (String tipoName : listaTipos) {
			VegTipo vegTipo = vegTipoDAO.findByName(tipoName);			
			vegTipo.getListaVegFases().add(vegFase);
			
			try {
				vegTipoDAO.edit(vegTipo);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
	}
}
