/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Test;

import mx.gob.semarnat.mia.model.SueloVegetacionDetalle;
import mx.gob.semarnat.mia.model.SueloVegetacionProyecto;
import mx.gob.semarnat.mia.model.VegFase;
import mx.gob.semarnat.mia.model.VegGrupo;
import mx.gob.semarnat.mia.model.VegTipo;

/**
 * @author C�sar
 *
 */
public class SueloVegetacionDaoTest {

	/**
	 * Busca suelos
	 */
	@Test
	public void testConsultarSuelos() {
		MiaDao miaDao = new MiaDao();
		List<SueloVegetacionProyecto> listaSuelos = miaDao.getSueloVegetacionProyecto("8440", (short)1);
		for (SueloVegetacionProyecto sueloVegetacionProyecto : listaSuelos) {
			System.out.println(sueloVegetacionProyecto.getSueloComponente());
			for (SueloVegetacionDetalle detalle : sueloVegetacionProyecto.getListaDetalle()) {
				System.out.println(detalle.getSuperficie());
			}
		}
	}
	
	/**
	 * Creacion de un nuevo suelo y su detalle
	 */
	@Test
	public void testCreateSuelo() {
		MiaDao miaDao = new MiaDao();		
		short nextId = miaDao.getMaxSueloVegetacionProyecto("8440", (short) 1);
		SueloVegetacionProyecto suelo = new SueloVegetacionProyecto("8440", (short) 1, nextId);
		suelo.setSueloComponente("Prueba");
		suelo.setUsoSueloInegi("Prueba");
		suelo.setUsoSueloActual("Prueba");
		suelo.setSueloDescripcion("Prueba");
		suelo.setSueloAreaSigeia(2342.56);
		suelo.setSueloTipoEcov("Prueba");
		suelo.setSueloTipoGen("Prueba");
		suelo.setSueloFaseVs("Prueba");
		suelo.setSueloValidaResultado("P");
		suelo.setSueloSuperficeProm(8.9334);
		
		try {
			miaDao.merge(suelo);
			System.out.println("Insertado");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		SueloVegetacionDetalle sueloDetalle = new SueloVegetacionDetalle(suelo.getSueloVegetacionProyectoPK().getFolioProyecto(),
				suelo.getSueloVegetacionProyectoPK().getSerialProyecto(), suelo.getSueloVegetacionProyectoPK().getSueloVegProyId());
		VegGrupoDAO vegGDao = new VegGrupoDAO();
		VegTipoDAO vegTDao = new VegTipoDAO();
		
		VegGrupo vegGrupo = vegGDao.find(1);
		VegTipo vegTipo = vegTDao.find(1);
		VegFase vegFase = vegTipo.getListaVegFases().get(0);
		sueloDetalle.setVegGrupo(vegGrupo);
		sueloDetalle.setVegTipo(vegTipo);
		sueloDetalle.setVegFase(vegFase);
		sueloDetalle.setSuperficie(234.45);
		
		SueloVegetacionDetalleDAO detalleDao = new SueloVegetacionDetalleDAO();
		try {
			detalleDao.create(sueloDetalle);
			System.out.println("Detalle guardado !");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
