/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Test;

import mx.gob.semarnat.mia.model.PduProyecto;
import mx.gob.semarnat.mia.model.PdumProyecto;
import mx.gob.semarnat.mia.model.PdumProyectoPK;
import mx.gob.semarnat.mia.model.Proyecto;

/**
 * @author C�sar
 *
 */
public class PdumProyectoDaoTest {
	
	/**
	 * Test para crear pdus en la bd
	 */
	@Test
	public void testCreatePdu() {
		MiaDao mia = new MiaDao();
		List<String> ids = new ArrayList<String>();
		ids.add("2");
		ids.add("3");
		ids.add("4");
		ids.add("5");
		
		for (String id : ids) {
			PdumProyectoPK pduPk = new PdumProyectoPK("10344",(short) 1,Short.parseShort(id));
			PdumProyecto pdu = new PdumProyecto(pduPk);
			pdu.setPdumNombre("Ejemplo de pdum");
			pdu.setDeSigeia(true);
			pdu.setFechaPublicacion(new Date());
			pdu.setPdumClaveUsos("Ejemplo claves usos");
			pdu.setPdumComp("Ejemplo comp");
			pdu.setPdumCos("Ejemplo cos");
			pdu.setPdumCus("Ejemplo cus");
			pdu.setPdumFechaPublicacion("20/10/2015");
			pdu.setPdumUsos("Ejemplo usos");
			pdu.setPdumVinculacion("Ejemplo vinculacion");
			try {				
				mia.persist(pdu);				
			} catch (Exception e) {				
				e.printStackTrace();
			}
		}		
	}
	
	/**
	 * Test de actualizar pdu
	 */
	@Test
	public void testUpdatePDU() {
		MiaDao mia = new MiaDao();
		PdumProyecto pduSeleccionado = mia.getPdumProyecto("12037",(short) 1, (short) 9);
		pduSeleccionado.setPdumVinculacion("ababababba");
		try {
			mia.persist(pduSeleccionado);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		PdumProyecto pduAct = mia.getPdumProyecto("12037",(short) 1, (short) 9);
		System.out.println("Actualizado!" + pduAct.getPdumVinculacion());
	}

	@Test
	public void testAplica() {
		MiaDao mia = new MiaDao();
		try {
			mia.actualizarPDURespuestaAplica("8440",(short) 1, "S");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
