/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.web;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.gob.semarnat.mia.dao.EntityFactory;
import mx.gob.semarnat.mia.dao.SinatecProcedure;
import mx.gob.semarnat.mia.dao.VisorDao;
import mx.gob.semarnat.mia.model.AnexosProyecto;
import mx.gob.semarnat.mia.model.CatParametro;
import mx.gob.semarnat.mia.model.EspecificacionAnexo;
import mx.gob.semarnat.mia.model.EstudiosEspProy;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.ProyectoPK;
import mx.gob.semarnat.mia.model.SustanciaAnexo;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author mauricio
 */
public class CargarArchivo extends HttpServlet {

    private static final String EXT = "pdf,jpg,gif,png,dwg";
    private static final int THRESHOLD_SIZE = 1024 * 1024 * 3; 	// 3MB
    private static final int MAX_FILE_SIZE = 1024 * 1024 * 40; // 40MB
    private static final int MAX_REQUEST_SIZE = 1024 * 1024 * 50; // 50MB
    private final VisorDao dao = new VisorDao();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        EntityManager em = EntityFactory.getMiaMF().createEntityManager();
        Query q = em.createQuery("SELECT a FROM CatParametro a WHERE a.parDescripcion = 'ADJUNTOS'");
        CatParametro c = (CatParametro) q.getSingleResult();
        
        String UPLOAD_DIRECTORY = c.getParRuta();
        //String UPLOAD_DIRECTORY = "C:/wars/";
        em.close();

        String info = "";
        String tipo = "";
        String folioProyecto = "";
        String especifiacionId = "";
        Short serialProyecto = (short) 0;
        Short normaId = (short) 0;

        short capituloId = (short) 0;
        short subcapituloId = (short) 0;
        short seccionId = (short) 0;
        short apartadoId = (short) 0;
        short sustanciaId = (short) 0;
        short estudioId = (short) 0;

        try {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(THRESHOLD_SIZE);
            factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setFileSizeMax(MAX_FILE_SIZE);
            upload.setSizeMax(MAX_REQUEST_SIZE);

            List<Map<String, String>> adjuntos = new ArrayList<Map<String, String>>();

            List<AnexosProyecto> anexos = new ArrayList<AnexosProyecto>();
            List formItems = upload.parseRequest(request);
            Iterator iter = formItems.iterator();

            System.out.println("Items " + formItems.size());
            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();
                
                if (item.isFormField()) {
                    String tmpField = item.getFieldName();
                    String tmpTxt = item.getString();
                    String url = "";
 
                   if (tmpField.contains("tipo")) {
                        tipo = item.getString();
                    }

                    if (tmpField.contains("info")) {
                        if (tipo.equals("anexo_especificacion")) {
                            info = item.getString("UTF-8");
                            System.out.println("info " + info);
                            String[] args = info.split(",");
                            folioProyecto = args[1];
                            especifiacionId = args[0];
                            serialProyecto = new Short(args[2]);
                            normaId = new Short(args[3]);
                            
                            //Construye la ruta donde depositará el archivo
                            UPLOAD_DIRECTORY = UPLOAD_DIRECTORY + folioProyecto + "/";
                            System.out.println(" anexo_especificacion Ruta Construida  -->  " + UPLOAD_DIRECTORY);
                            
                            File folder = new File(UPLOAD_DIRECTORY);
                            if (!folder.exists()) {
                                folder.mkdirs();                         
                            }
                        }
                        if (tipo.equals("anexo_general")) {
                            info = item.getString("UTF-8");
                            System.out.println("info " + info);
                            String[] args = info.split(",");

                            capituloId = new Short(args[0]);
                            subcapituloId = new Short(args[1]);
                            seccionId = new Short(args[2]);
                            apartadoId = new Short(args[3]);
                            folioProyecto = args[4];
                            serialProyecto = new Short(args[5]);
                            
                            
                            //Construye la ruta donde depositará el archivo
                            UPLOAD_DIRECTORY = UPLOAD_DIRECTORY + folioProyecto + "/";
                            System.out.println(" anexo_general Ruta Construida  -->  " + UPLOAD_DIRECTORY);
                            
                            File folder = new File(UPLOAD_DIRECTORY);
                            if (!folder.exists()) {
                                folder.mkdirs();                         
                            }
                        }
                        if (tipo.equals("anexo_sustancia")) {
                            info = item.getString("UTF-8");
                            System.out.println("info " + info);
                            String[] args = info.split(",");

                            folioProyecto = args[0];
                            serialProyecto = new Short(args[1]);
                            sustanciaId = new Short(args[2]);
                            
                            //Construye la ruta donde depositará el archivo
                            UPLOAD_DIRECTORY = UPLOAD_DIRECTORY + folioProyecto + "/";
                            System.out.println(" anexo_sustancia Ruta Construida  -->  " + UPLOAD_DIRECTORY);
                            
                            File folder = new File(UPLOAD_DIRECTORY);
                            if (!folder.exists()) {
                                folder.mkdirs();                         
                            }
                        }
                        if (tipo.equals("anexo_estudio")) {
                            info = item.getString("UTF-8");
                            System.out.println("info " + info);
                            String[] args = info.split(",");

                            folioProyecto = args[4];
                            serialProyecto = new Short(args[5]);
                            estudioId = new Short(args[6]);
                            
                            //Construye la ruta donde depositará el archivo
                            UPLOAD_DIRECTORY = UPLOAD_DIRECTORY + folioProyecto + "/";
                            System.out.println(" anexo_estudio Ruta Construida  -->  " + UPLOAD_DIRECTORY);
                            
                            File folder = new File(UPLOAD_DIRECTORY);
                            if (!folder.exists()) {
                                folder.mkdirs();                         
                            }
                        }

                    }

                    if (tmpField.contains("txtArea")) {
                        int n = Integer.parseInt(tmpField.replace("txtArea", ""));
                        System.out.println("n " + n);
                        //url = UPLOAD_DIRECTORY + UUID.randomUUID().toString();
                            String nombre = item.getString("UTF-8");
                            String ext = item.getFieldName();
                            
                            System.out.println(url);
                            Map<String, String> tmp = new HashMap<String, String>();
                            tmp.put("descripcion", item.getString("UTF-8"));                            
                            item = (FileItem) iter.next();
                            String utf8Nombre = item.getName();
                            String Nombre = new String(utf8Nombre.getBytes("ISO-8859-1"), "UTF-8");
                            
                            //Reemplazo de caracteres especiales
                            Nombre = Normalizer.normalize(Nombre, Normalizer.Form.NFD);
                            //Nombre = Nombre.replaceAll("[^a-zA-Z0-9.-]", "_");
                            Nombre = Nombre.replaceAll("[^\\sa-zA-Z0-9._-]", "");
                            
                            //Busca si el nombre de archivo no existe
                            Long conteo = dao.conteoAdjuntos(folioProyecto, serialProyecto, Nombre);
                            if (conteo > 0) {
                                Nombre = "[" + (conteo + 1) + "]" + Nombre;
                            }
                            
                            tmp.put("nombre", new File(Nombre).getName());                            
                            url = UPLOAD_DIRECTORY + Nombre;
                            tmp.put("url", url);
                            
                            File storeFile = new File(url);
                            item.write(storeFile);
                            tmp.put("tam", "" + storeFile.length());
                            adjuntos.add(tmp);
                    } else {
                        System.out.println("tmpField " + tmpField);
                    }
                }
            }

            int i = 0;
            //SinatecProcedure sp = new SinatecProcedure();
            Integer nvo = ((Proyecto) dao.getObject(Proyecto.class, new ProyectoPK(folioProyecto, serialProyecto))).getProyLote() != null
                    ? ((Proyecto) dao.getObject(Proyecto.class, new ProyectoPK(folioProyecto, serialProyecto))).getProyLote().intValue()
                    : null;
            for (Map<String, String> a : adjuntos) {
                System.out.println("tipo " + tipo);
                if (tipo.equals("anexo_especificacion")) {
                    System.out.println("guardando anexo_especificacion");
                    short idanexo = dao.numRegAnexoEspe(folioProyecto, serialProyecto, especifiacionId);
                    EspecificacionAnexo tmp = new EspecificacionAnexo(folioProyecto, serialProyecto, especifiacionId, normaId, idanexo);
                    System.out.println("tmp " + tmp.getEspecificacionAnexoPK().toString());
                    tmp.setEspecAnexoDescripcion(a.get("descripcion"));
//                    tmp.setEspecAnexoTamanio(BigDecimal.valueOf(Double.parseDouble(a.get("tam"))));
                    tmp.setEspecAnexoUrl(a.get("url"));
                    System.out.println("" + a.get("nombre"));
                    String[] tnomext = a.get("nombre").split("\\.");

                    tmp.setEspecAnexoExtension(tnomext[tnomext.length - 1]);
                    tmp.setEspecAnexoNombre(a.get("nombre"));
                    VisorDao.persist(tmp);
                    //procedimiento SIANTEC
                    //sp.solicitudCarga(Integer.parseInt(folioProyecto),tmp.getEspecAnexoDescripcion() , tmp.getEspecAnexoNombre(), nvo);
                }
                if (tipo.equals("anexo_general")) {
                    System.out.println("guardando anexo_general");

                    short anexoId = dao.numRegAnexo(capituloId, subcapituloId, seccionId, apartadoId, folioProyecto, serialProyecto);
                    System.out.println("anexoId " + anexoId);
                    anexoId++;
                    System.out.println("anexoId " + anexoId);

                    AnexosProyecto tmp = new AnexosProyecto(capituloId, subcapituloId, seccionId, apartadoId, anexoId, folioProyecto, serialProyecto);
                    tmp.setAnexoDescripcion(a.get("descripcion"));
//                    tmp.setEspecAnexoTamanio(BigDecimal.valueOf(Double.parseDouble(a.get("tam"))));
                    tmp.setAnexoTamanio(new BigDecimal("1"));
                    tmp.setAnexoUrl(a.get("url"));
                    System.out.println("" + a.get("nombre"));
                    String[] tnomext = a.get("nombre").split("\\.");
                    tmp.setAnexoExtension(tnomext[tnomext.length - 1]);
                    tmp.setAnexoNombre(a.get("nombre"));
                    VisorDao.persist(tmp);
                    //procedimiento SIANTEC                    
                    //sp.solicitudCarga(Integer.parseInt(folioProyecto),tmp.getAnexoDescripcion() , tmp.getAnexoNombre(), nvo);
                }
                if (tipo.equals("anexo_sustancia")) {
                    System.out.println("guardando anexo_sustancia");
                    short anexoSustId = dao.numRegAnexoSust();
                    System.out.println("sus_anexo_id " + anexoSustId);
                    anexoSustId++;
                    System.out.println("sus_anexo_id " + anexoSustId);
//                    System.out.println(""+sustanciaId+":"+anexoSustId+":"+folioProyecto+":"+serialProyecto);
                    SustanciaAnexo tmp = new SustanciaAnexo(sustanciaId, anexoSustId, folioProyecto, serialProyecto);
                    tmp.setAnexoDesc("Sustancias - Hoja de seguridad");
//                    tmp.setAnexoDesc(a.get("descripcion"));
                    tmp.setAnexoTamanio(new BigDecimal("1"));
                    tmp.setAnexoUrl(a.get("url"));
                    System.out.println("" + a.get("nombre"));
                    String[] tnomext = a.get("nombre").split("\\.");
                    tmp.setAnexoExtension(tnomext[tnomext.length - 1]);
                    tmp.setAnexoNombre(a.get("nombre"));
                    VisorDao.merge(tmp);
                    //procedimiento SIANTEC
                    //sp.solicitudCarga(Integer.parseInt(folioProyecto),tmp.getAnexoDesc() , tmp.getAnexoNombre(), nvo);
                }
                if (tipo.equals("anexo_estudio")) {
                    System.out.println("guardando anexo_estudio");
                    short anexoEstId = dao.numRegAnexoEst(folioProyecto,serialProyecto);
                    System.out.println("sus_anexo_id " + anexoEstId);
                    anexoEstId++;
                    System.out.println("sus_anexo_id " + anexoEstId);
//                    System.out.println(""+sustanciaId+":"+anexoSustId+":"+folioProyecto+":"+serialProyecto);
                    //EstudiosEspProy tmp = new EstudiosEspProy(folioProyecto, serialProyecto, estudioId);
//                    tmp.setAnexoDescripcion(a.get("descripcion"));
                  /*  tmp.setAnexoDescripcion("Anexo de estudios especiales");
                    //tmp.setAnexoTamanio(new BigDecimal("1"));
                    tmp.setAnexoUrl(a.get("url"));
                    System.out.println("" + a.get("nombre"));
                    String[] tnomext = a.get("nombre").split("\\.");
                    tmp.setAnexoExtension(tnomext[tnomext.length - 1]);
                    tmp.setAnexoNombre(a.get("nombre"));
                    VisorDao.mergeEst(tmp);
                    //procedimiento SIANTEC     */               
                    //sp.solicitudCarga(Integer.parseInt(folioProyecto),tmp.getAnexoDescripcion() , tmp.getAnexoNombre(), nvo);
                }

                i++;
            }
            
            //ArrayList<Object[]> lstAdjuntos = dao.obtenerAdjuntos(folioProyecto, serialProyecto);

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title></title>");
            out.println("</head>");
            out.println("<body>");
            out.println("Cargando documento");

            System.out.println("INFO " + info);
            if (tipo.equals("anexo_especificacion")) {

                out.println("<script>location.href='" + request.getContextPath() + "/faces/modarchivos/cargaespecificaciones.xhtml?info=" + info + "';</script>");
            }
            if (tipo.equals("anexo_general")) {
                out.println("<script>location.href='" + request.getContextPath() + "/faces/modarchivos/cargaarchivo.xhtml?info=" + info + "';</script>");
            }
            if (tipo.equals("anexo_sustancia")) {
                out.println("<script>location.href='" + request.getContextPath() + "/faces/modarchivos/cargaHojaSeguridad.xhtml?info=" + info + "';</script>");
            }
            if (tipo.equals("anexo_estudio")) {
                out.println("<script>location.href='" + request.getContextPath() + "/faces/modarchivos/cargaEstudios.xhtml?info=" + info + "';</script>");
            }
            out.println("</body>");
            out.println("</html>");

        } catch (FileUploadException f) {
            f.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

