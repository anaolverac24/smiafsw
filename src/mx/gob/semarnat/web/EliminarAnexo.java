/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.web;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.gob.semarnat.mia.dao.EntityFactory;
import mx.gob.semarnat.mia.model.CatParametro;

/**
 *
 * @author mauricio
 */
public class EliminarAnexo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String idt = request.getParameter("idt");
            String tipo = request.getParameter("tipo");
            String info = request.getParameter("info");
            String folio = request.getParameter("folio");
            String serial = request.getParameter("serial");
            
            String anexoUrl;

//            if (tipo.equals("especificaciones")) {//Esta opción no se usa en la MIA
//                EntityManager em = EntityFactory.getMiaMF().createEntityManager();
//                em.getTransaction().begin();
//                Query q = em.createQuery("DELETE FROM EspecificacionAnexo a WHERE a.especificacionAnexoPK.especificacionAnexoId = :idt");
//                q.setParameter("idt", Short.valueOf(idt));
//                q.executeUpdate();
//                em.getTransaction().commit();
//                em.close();
//            } else 
            if (tipo.equals("general")) {
                try {
                    EntityManager em = EntityFactory.getMiaMF().createEntityManager();
                    em.getTransaction().begin();

                    //Obtención de la ruta para su eliminación en el sitio ftp
                    Query q = em.createQuery("SELECT a.anexoUrl FROM AnexosProyecto a WHERE a.anexosProyectoPK.anexoId = :idt");
                    q.setParameter("idt", Short.valueOf(idt));
                    anexoUrl = (String)q.getSingleResult();
                    eliminaArchivoRemoto(anexoUrl);

                    //Eliminación en BD
                    q = em.createQuery("DELETE FROM AnexosProyecto a WHERE a.anexosProyectoPK.anexoId = :idt");
                    q.setParameter("idt", Short.valueOf(idt));
                    q.executeUpdate();
                    em.getTransaction().commit();
                    em.close();
                } catch (Exception e) {
                    System.out.println("Ocurrió un error durante la eliminación del archivo");
                    e.printStackTrace();
                }
            } else if (tipo.equals("anexo_sustancia")) {
                try {
                    EntityManager em = EntityFactory.getMiaMF().createEntityManager();
                    em.getTransaction().begin();
                    
                    //Obtención de la ruta para su eliminación en el sitio ftp
                    Query q = em.createQuery("SELECT a.anexoUrl FROM SustanciaAnexo a WHERE a.sustanciaAnexoPK.sustanciaAnexoId = :idt");
                    q.setParameter("idt", Short.valueOf(idt));
                    anexoUrl = (String)q.getSingleResult();
                    eliminaArchivoRemoto(anexoUrl);

                    //Eliminación en BD
                    q = em.createQuery("DELETE FROM SustanciaAnexo a WHERE a.sustanciaAnexoPK.sustanciaAnexoId = :idt");
                    q.setParameter("idt", Short.valueOf(idt));
                    q.executeUpdate();
                    em.getTransaction().commit();
                    em.close();
                } catch (Exception e) {
                    System.out.println("Ocurrió un error durante la eliminación del archivo");
                    e.printStackTrace();
                }
            } else if (tipo.equals("anexo_estudio")) {
                try {
                    EntityManager em = EntityFactory.getMiaMF().createEntityManager();
                    em.getTransaction().begin();
                    
                    //Obtención de la ruta para su eliminación en el sitio ftp
                    Query q = em.createQuery("SELECT a.anexoUrl FROM EstudiosEspProy a "
                            + " WHERE a.estudiosEspProyPK.estudioId =:idt "
                            + "   AND a.estudiosEspProyPK.folioSerial =:folio "
                            + "   AND a.estudiosEspProyPK.serialProyecto =:serial ");
                    q.setParameter("idt", Short.valueOf(idt));
                    q.setParameter("folio", folio);
                    q.setParameter("serial", Short.valueOf(serial));
                    anexoUrl = (String)q.getSingleResult();
                    eliminaArchivoRemoto(anexoUrl);

                    //Eliminación en BD
                    q = em.createQuery("UPDATE EstudiosEspProy a "
                            + " SET a.anexoUrl = null, a.anexoDescripcion = null, a.anexoNombre = null, a.anexoTamanio = null, a.anexoExtension = null "
                            + " WHERE a.estudiosEspProyPK.estudioId =:idt "
                            + "   AND a.estudiosEspProyPK.folioSerial =:folio "
                            + "   AND a.estudiosEspProyPK.serialProyecto =:serial ");
                    q.setParameter("idt", Short.valueOf(idt));
                    q.setParameter("folio", folio);
                    q.setParameter("serial", Short.valueOf(serial));
                    q.executeUpdate();
                    em.getTransaction().commit();
                    em.close();
                } catch (Exception e) {
                    System.out.println("Ocurrió un error durante la eliminación del archivo");
                    e.printStackTrace();
                }
            }

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title></title>");
            out.println("</head>");
            out.println("<body>");
            out.println("Eliminando documento");
            if (tipo.equals("especificaciones")) {
                out.println("<script>location.href='" + request.getContextPath() + "/faces/modarchivos/cargaespecificaciones.xhtml?info=" + info + "';</script>");
            }
            if (tipo.equals("general")) {
                out.println("<script>location.href='" + request.getContextPath() + "/faces/modarchivos/cargaarchivo.xhtml?info=" + info + "';</script>");
            }
            out.println("</body>");
            out.println("</html>");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }
    
    private void eliminaArchivoRemoto(String rutaArch) {
        /*
        EntityManager em = EntityFactory.getMiaMF().createEntityManager();
        Query q = em.createQuery("SELECT a FROM CatParametro a WHERE a.parDescripcion = 'ADJUNTOS'");
        CatParametro c = (CatParametro) q.getSingleResult();
        
        String UPLOAD_DIRECTORY = c.getParRuta();
        em.close();
        */
        
        try {
            File file = new File(rutaArch);
            Files.deleteIfExists(file.toPath());
        } catch (Exception e) {
            e.printStackTrace();
        }

        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
