/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_ESTUDIO_ESP_SUB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatEstudioEspSub.findAll", query = "SELECT c FROM CatEstudioEspSub c"),
    @NamedQuery(name = "CatEstudioEspSub.findByEstudioId", query = "SELECT c FROM CatEstudioEspSub c WHERE c.catEstudioEspSubPK.estudioId = :estudioId"),
    @NamedQuery(name = "CatEstudioEspSub.findByNsub", query = "SELECT c FROM CatEstudioEspSub c WHERE c.catEstudioEspSubPK.nsub = :nsub")})
public class CatEstudioEspSub implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CatEstudioEspSubPK catEstudioEspSubPK;

    public CatEstudioEspSub() {
    }

    public CatEstudioEspSub(CatEstudioEspSubPK catEstudioEspSubPK) {
        this.catEstudioEspSubPK = catEstudioEspSubPK;
    }

    public CatEstudioEspSub(short estudioId, short nsub) {
        this.catEstudioEspSubPK = new CatEstudioEspSubPK(estudioId, nsub);
    }

    public CatEstudioEspSubPK getCatEstudioEspSubPK() {
        return catEstudioEspSubPK;
    }

    public void setCatEstudioEspSubPK(CatEstudioEspSubPK catEstudioEspSubPK) {
        this.catEstudioEspSubPK = catEstudioEspSubPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catEstudioEspSubPK != null ? catEstudioEspSubPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatEstudioEspSub)) {
            return false;
        }
        CatEstudioEspSub other = (CatEstudioEspSub) object;
        if ((this.catEstudioEspSubPK == null && other.catEstudioEspSubPK != null) || (this.catEstudioEspSubPK != null && !this.catEstudioEspSubPK.equals(other.catEstudioEspSubPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatEstudioEspSub[ catEstudioEspSubPK=" + catEstudioEspSubPK + " ]";
    }
    
}
