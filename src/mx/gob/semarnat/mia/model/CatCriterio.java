/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_CRITERIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatCriterio.findAll", query = "SELECT c FROM CatCriterio c"),
    @NamedQuery(name = "CatCriterio.findByCriterioId", query = "SELECT c FROM CatCriterio c WHERE c.criterioId = :criterioId"),
    @NamedQuery(name = "CatCriterio.findByCriterioNombre", query = "SELECT c FROM CatCriterio c WHERE c.criterioNombre = :criterioNombre")})
public class CatCriterio implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "criterioId")
    private List<CriteriosProyecto> criteriosProyectoList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CRITERIO_ID")
    private Short criterioId;
    @Basic(optional = false)
    @Column(name = "CRITERIO_NOMBRE")
    private String criterioNombre;

    public CatCriterio() {
    }

    public CatCriterio(Short criterioId) {
        this.criterioId = criterioId;
    }

    public CatCriterio(Short criterioId, String criterioNombre) {
        this.criterioId = criterioId;
        this.criterioNombre = criterioNombre;
    }

    public Short getCriterioId() {
        return criterioId;
    }

    public void setCriterioId(Short criterioId) {
        this.criterioId = criterioId;
    }

    public String getCriterioNombre() {
        return criterioNombre;
    }

    public void setCriterioNombre(String criterioNombre) {
        this.criterioNombre = criterioNombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (criterioId != null ? criterioId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatCriterio)) {
            return false;
        }
        CatCriterio other = (CatCriterio) object;
        if ((this.criterioId == null && other.criterioId != null) || (this.criterioId != null && !this.criterioId.equals(other.criterioId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatCriterio[ criterioId=" + criterioId + " ]";
    }

    @XmlTransient
    public List<CriteriosProyecto> getCriteriosProyectoList() {
        return criteriosProyectoList;
    }

    public void setCriteriosProyectoList(List<CriteriosProyecto> criteriosProyectoList) {
        this.criteriosProyectoList = criteriosProyectoList;
    }
    
}
