/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "ANP_PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AnpProyecto.findAll", query = "SELECT a FROM AnpProyecto a"),
    @NamedQuery(name = "AnpProyecto.findByFolioProyecto", query = "SELECT a FROM AnpProyecto a WHERE a.anpProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "AnpProyecto.findBySerialProyecto", query = "SELECT a FROM AnpProyecto a WHERE a.anpProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "AnpProyecto.findByAnpId", query = "SELECT a FROM AnpProyecto a WHERE a.anpProyectoPK.anpId = :anpId"),
    @NamedQuery(name = "AnpProyecto.findByAnpTipo", query = "SELECT a FROM AnpProyecto a WHERE a.anpTipo = :anpTipo"),
    @NamedQuery(name = "AnpProyecto.findByAnpNombre", query = "SELECT a FROM AnpProyecto a WHERE a.anpNombre = :anpNombre"),
    @NamedQuery(name = "AnpProyecto.findByAnpCategoriaManejo", query = "SELECT a FROM AnpProyecto a WHERE a.anpCategoriaManejo = :anpCategoriaManejo"),
    @NamedQuery(name = "AnpProyecto.findByAnpUltimoDecreto", query = "SELECT a FROM AnpProyecto a WHERE a.anpUltimoDecreto = :anpUltimoDecreto"),
    @NamedQuery(name = "AnpProyecto.findByAnpFechaManejo", query = "SELECT a FROM AnpProyecto a WHERE a.anpFechaManejo = :anpFechaManejo")})
public class AnpProyecto implements Serializable {
    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "anpProyecto")
    private List<ArticuloReglaAnp> articuloReglaAnpList;
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AnpProyectoPK anpProyectoPK;
    @Column(name = "ANP_TIPO")
    private String anpTipo;
    @Column(name = "ANP_NOMBRE")
    private String anpNombre;
    @Column(name = "ANP_CATEGORIA_MANEJO")
    private String anpCategoriaManejo;
    @Column(name = "ANP_ULTIMO_DECRETO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date anpUltimoDecreto;
    @Column(name = "ANP_FECHA_MANEJO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date anpFechaManejo;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    @Transient
    private String idt = "1";
    
    @Column(name="CLAVE_ORIGEN_REGISTRO")
    private Integer claveOrigenRegistro;
     
    @Column(name="ID_ARCHIVO_DECRETO")
    private Short idArchivoDecreto;
    
    @Column(name="ID_ARCHIVO_PROGRAMA")
    private Short idArchivoPrograma;
    
    public AnpProyecto() {
    }

    public AnpProyecto(AnpProyectoPK anpProyectoPK) {
        this.anpProyectoPK = anpProyectoPK;
    }

    public AnpProyecto(String folioProyecto, short serialProyecto, short anpId) {
        this.anpProyectoPK = new AnpProyectoPK(folioProyecto, serialProyecto, anpId);
    }

    public AnpProyectoPK getAnpProyectoPK() {
        return anpProyectoPK;
    }

    public void setAnpProyectoPK(AnpProyectoPK anpProyectoPK) {
        this.anpProyectoPK = anpProyectoPK;
    }

    public String getAnpTipo() {
        return anpTipo;
    }

    public void setAnpTipo(String anpTipo) {
        this.anpTipo = anpTipo;
    }

    public String getAnpNombre() {
        return anpNombre;
    }

    public void setAnpNombre(String anpNombre) {
        this.anpNombre = anpNombre;
    }

    public String getAnpCategoriaManejo() {
        return anpCategoriaManejo;
    }

    public void setAnpCategoriaManejo(String anpCategoriaManejo) {
        this.anpCategoriaManejo = anpCategoriaManejo;
    }

    public Date getAnpUltimoDecreto() {
        return anpUltimoDecreto;
    }

    public void setAnpUltimoDecreto(Date anpUltimoDecreto) {
        this.anpUltimoDecreto = anpUltimoDecreto;
    }

    public Date getAnpFechaManejo() {
        return anpFechaManejo;
    }

    public void setAnpFechaManejo(Date anpFechaManejo) {
        this.anpFechaManejo = anpFechaManejo;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (anpProyectoPK != null ? anpProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnpProyecto)) {
            return false;
        }
        AnpProyecto other = (AnpProyecto) object;
        if ((this.anpProyectoPK == null && other.anpProyectoPK != null) || (this.anpProyectoPK != null && !this.anpProyectoPK.equals(other.anpProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.AnpProyecto[ anpProyectoPK=" + anpProyectoPK + " ]";
    }

    @XmlTransient
    public List<ArticuloReglaAnp> getArticuloReglaAnpList() {
        return articuloReglaAnpList;
    }

    public void setArticuloReglaAnpList(List<ArticuloReglaAnp> articuloReglaAnpList) {
        this.articuloReglaAnpList = articuloReglaAnpList;
    }

    /**
     * @return the idt
     */
    public String getIdt() {
        return idt;
    }

    /**
     * @param idt the idt to set
     */
    public void setIdt(String idt) {
        this.idt = idt;
    }

	/**
	 * @return the claveOrigenRegistro
	 */
	public Integer getClaveOrigenRegistro() {
		return claveOrigenRegistro;
	}

	/**
	 * @param claveOrigenRegistro the claveOrigenRegistro to set
	 */
	public void setClaveOrigenRegistro(Integer claveOrigenRegistro) {
		this.claveOrigenRegistro = claveOrigenRegistro;
	}

	/**
	 * @return the idArchivoDecreto
	 */
	public Short getIdArchivoDecreto() {
		return idArchivoDecreto;
	}

	/**
	 * @param idArchivoDecreto the idArchivoDecreto to set
	 */
	public void setIdArchivoDecreto(Short idArchivoDecreto) {
		this.idArchivoDecreto = idArchivoDecreto;
	}

	/**
	 * @return the idArchivoPrograma
	 */
	public Short getIdArchivoPrograma() {
		return idArchivoPrograma;
	}

	/**
	 * @param idArchivoPrograma the idArchivoPrograma to set
	 */
	public void setIdArchivoPrograma(Short idArchivoPrograma) {
		this.idArchivoPrograma = idArchivoPrograma;
	}
    
}
