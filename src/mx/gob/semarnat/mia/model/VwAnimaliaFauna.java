/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "VW_ANIMALIA_FAUNA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VwAnimaliaFauna.findAll", query = "SELECT v FROM VwAnimaliaFauna v"),
    @NamedQuery(name = "VwAnimaliaFauna.findByAnimaliaReino", query = "SELECT v FROM VwAnimaliaFauna v WHERE v.animaliaReino = :animaliaReino"),
    @NamedQuery(name = "VwAnimaliaFauna.findByAnimaliaClase", query = "SELECT v FROM VwAnimaliaFauna v WHERE v.animaliaClase = :animaliaClase"),
    @NamedQuery(name = "VwAnimaliaFauna.findByAnimaliaOrden", query = "SELECT v FROM VwAnimaliaFauna v WHERE v.animaliaOrden = :animaliaOrden"),
    @NamedQuery(name = "VwAnimaliaFauna.findByAnimaliaFamilia", query = "SELECT v FROM VwAnimaliaFauna v WHERE v.animaliaFamilia = :animaliaFamilia"),
    @NamedQuery(name = "VwAnimaliaFauna.findByAnimaliaGenero", query = "SELECT v FROM VwAnimaliaFauna v WHERE v.animaliaGenero = :animaliaGenero"),
    @NamedQuery(name = "VwAnimaliaFauna.findByAnimaliaEspecie", query = "SELECT v FROM VwAnimaliaFauna v WHERE v.animaliaEspecie = :animaliaEspecie"),
    @NamedQuery(name = "VwAnimaliaFauna.findByAnimaliaNombreCientifico", query = "SELECT v FROM VwAnimaliaFauna v WHERE v.animaliaNombreCientifico = :animaliaNombreCientifico"),
    @NamedQuery(name = "VwAnimaliaFauna.findByAnimaliaNombreComun", query = "SELECT v FROM VwAnimaliaFauna v WHERE v.animaliaNombreComun = :animaliaNombreComun"),
    @NamedQuery(name = "VwAnimaliaFauna.findByAnimaliaCategoriaNom059", query = "SELECT v FROM VwAnimaliaFauna v WHERE v.animaliaCategoriaNom059 = :animaliaCategoriaNom059"),
    @NamedQuery(name = "VwAnimaliaFauna.findByAnimaliaCites", query = "SELECT v FROM VwAnimaliaFauna v WHERE v.animaliaCites = :animaliaCites")})
public class VwAnimaliaFauna implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ANIMALIA_REINO")
    private String animaliaReino;
    @Column(name = "ANIMALIA_CLASE")
    private String animaliaClase;
    @Column(name = "ANIMALIA_ORDEN")
    private String animaliaOrden;
    @Column(name = "ANIMALIA_FAMILIA")
    private String animaliaFamilia;
    @Column(name = "ANIMALIA_GENERO")
    private String animaliaGenero;
    @Column(name = "ANIMALIA_ESPECIE")
    private String animaliaEspecie;
    @Column(name = "ANIMALIA_NOMBRE_CIENTIFICO")
    private String animaliaNombreCientifico;
    @Column(name = "ANIMALIA_NOMBRE_COMUN")
    private String animaliaNombreComun;
    @Column(name = "ANIMALIA_CATEGORIA_NOM059")
    private String animaliaCategoriaNom059;
    @Column(name = "ANIMALIA_CITES")
    private String animaliaCites;

    public VwAnimaliaFauna() {
    }

    public String getAnimaliaReino() {
        return animaliaReino;
    }

    public void setAnimaliaReino(String animaliaReino) {
        this.animaliaReino = animaliaReino;
    }

    public String getAnimaliaClase() {
        return animaliaClase;
    }

    public void setAnimaliaClase(String animaliaClase) {
        this.animaliaClase = animaliaClase;
    }

    public String getAnimaliaOrden() {
        return animaliaOrden;
    }

    public void setAnimaliaOrden(String animaliaOrden) {
        this.animaliaOrden = animaliaOrden;
    }

    public String getAnimaliaFamilia() {
        return animaliaFamilia;
    }

    public void setAnimaliaFamilia(String animaliaFamilia) {
        this.animaliaFamilia = animaliaFamilia;
    }

    public String getAnimaliaGenero() {
        return animaliaGenero;
    }

    public void setAnimaliaGenero(String animaliaGenero) {
        this.animaliaGenero = animaliaGenero;
    }

    public String getAnimaliaEspecie() {
        return animaliaEspecie;
    }

    public void setAnimaliaEspecie(String animaliaEspecie) {
        this.animaliaEspecie = animaliaEspecie;
    }

    public String getAnimaliaNombreCientifico() {
        return animaliaNombreCientifico;
    }

    public void setAnimaliaNombreCientifico(String animaliaNombreCientifico) {
        this.animaliaNombreCientifico = animaliaNombreCientifico;
    }

    public String getAnimaliaNombreComun() {
        return animaliaNombreComun;
    }

    public void setAnimaliaNombreComun(String animaliaNombreComun) {
        this.animaliaNombreComun = animaliaNombreComun;
    }

    public String getAnimaliaCategoriaNom059() {
        return animaliaCategoriaNom059;
    }

    public void setAnimaliaCategoriaNom059(String animaliaCategoriaNom059) {
        this.animaliaCategoriaNom059 = animaliaCategoriaNom059;
    }

    public String getAnimaliaCites() {
        return animaliaCites;
    }

    public void setAnimaliaCites(String animaliaCites) {
        this.animaliaCites = animaliaCites;
    }
    
}
