/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author marcog
 */
@Entity
@Table(name = "REIA_PROY_OBRAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReiaProyObras.findAll", query = "SELECT r FROM ReiaProyObras r"),
    @NamedQuery(name = "ReiaProyObras.findByFolio", query = "SELECT r FROM ReiaProyObras r WHERE r.folio = :folio"),
    @NamedQuery(name = "ReiaProyObras.findByFolioObrasCapturadas", query = "SELECT r FROM ReiaProyObras r WHERE r.folio = :folio and r.obra.obra != 'NO' ORDER BY r.clave ASC"),
    @NamedQuery(name = "ReiaProyObras.findByObra", query = "SELECT r FROM ReiaProyObras r WHERE r.obra = :obra"),
    @NamedQuery(name = "ReiaProyObras.findByFolioEstatusObra", query = "SELECT r FROM ReiaProyObras r WHERE r.folio = :folio AND r.obra.idCategoria.estatus=:estatus AND r.obra.fraccion IS NOT NULL"),
    @NamedQuery(name = "ReiaProyObras.findByFolioEstatusObraCount", query = "SELECT COUNT(r) FROM ReiaProyObras r WHERE r.folio = :folio AND r.obra.idCategoria.estatus=:estatus AND r.obra.fraccion IS NOT NULL"),
    @NamedQuery(name = "ReiaProyObras.findByFolioCount", query = "SELECT COUNT(r) FROM ReiaProyObras r WHERE r.folio = :folio"),
    @NamedQuery(name = "ReiaProyObras.findByClave", query = "SELECT r FROM ReiaProyObras r WHERE r.clave = :clave"),
    @NamedQuery(name = "ReiaProyObras.findByCategoria", query = "SELECT r FROM ReiaProyObras r WHERE r.folio = :folio AND r.obra.idCategoria.idCategoria = :idCategoria")})
public class ReiaProyObras implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "FOLIO")
    private String folio;
        
    @JoinColumn(name = "OBRA", referencedColumnName = "ID_OBRA")
    @ManyToOne
    private ReiaObras obra;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(generator="InvSeq") 
    @SequenceGenerator(name="InvSeq",sequenceName="seq_proy_obras")    
    @Basic(optional = false)
    @Column(name = "CLAVE")
    private Long clave;
        
    @Column(name = "PRINCIPAL")
    private String principal;
    
    @Column(name = "EXCEPTUADA")
    private String exceptuada;
    

    public ReiaProyObras() {
    }

    public ReiaProyObras(Long clave) {
        this.clave = clave;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public ReiaObras getObra() {
        return obra;
    }

    public void setObra(ReiaObras obra) {
        this.obra = obra;
    }

    public Long getClave() {
        return clave;
    }

    public void setClave(Long clave) {
        this.clave = clave;
    }
    
    

    /***
     * indica si el proyecto-obra esta exceptuada, osea:   exceptuada = 0 ( SI requiere validacion )  y exceptuada = 1 ( NO requiere validacion ) 
     * @return
     */
    public String getExceptuada() {
		return exceptuada;
	}

    /***
     * indica si el proyecto-obra esta exceptuada, osea:   exceptuada = 0 ( SI requiere validacion )  y exceptuada = 1 ( NO requiere validacion )
     * @param exceptuada
     */
	public void setExceptuada(String exceptuada) {
		this.exceptuada = exceptuada;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (clave != null ? clave.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReiaProyObras)) {
            return false;
        }
        ReiaProyObras other = (ReiaProyObras) object;
        if ((this.clave == null && other.clave != null) || (this.clave != null && !this.clave.equals(other.clave))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ReiaProyObras[ clave=" + clave + " ]";
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }
    
}
