/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "ETAPA_PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EtapaProyecto.findAll", query = "SELECT e FROM EtapaProyecto e"),
    @NamedQuery(name = "EtapaProyecto.findByFolioSerial", query = "SELECT e FROM EtapaProyecto e WHERE e.etapaProyectoPK.folioProyecto=:folioProyecto AND e.etapaProyectoPK.serialProyecto=:serialProyecto")})
public class EtapaProyecto implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "etapaProyecto")
    private List<ActividadEtapa> actividadEtapaList;
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EtapaProyectoPK etapaProyectoPK;
    @Basic(optional = false)
    @Column(name = "MESES")
    private short meses;
    @Basic(optional = false)
    @Column(name = "ANIOS")
    private short anios;
    @Basic(optional = false)
    @Lob
    @Column(name = "ETAPA_DESCRIPCION_OBRA_ACT")
    private String etapaDescripcionObraAct;
    @Column(name = "SEMANAS")
    private Short semanas;
    @JoinColumn(name = "ETAPA_ID", referencedColumnName = "ETAPA_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CatEtapa catEtapa;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "etapaProyecto")
    private List<ContaminanteProyecto> contaminanteProyectoList;

    public EtapaProyecto() {
    }

    public EtapaProyecto(EtapaProyectoPK etapaProyectoPK) {
        this.etapaProyectoPK = etapaProyectoPK;
    }

    public EtapaProyecto(EtapaProyectoPK etapaProyectoPK, short meses, short anios, String etapaDescripcionObraAct) {
        this.etapaProyectoPK = etapaProyectoPK;
        this.meses = meses;
        this.anios = anios;
        this.etapaDescripcionObraAct = etapaDescripcionObraAct;
    }

    public EtapaProyecto(String folioProyecto, short serialProyecto, short etapaId) {
        this.etapaProyectoPK = new EtapaProyectoPK(folioProyecto, serialProyecto, etapaId);
    }

    public EtapaProyectoPK getEtapaProyectoPK() {
        return etapaProyectoPK;
    }

    public void setEtapaProyectoPK(EtapaProyectoPK etapaProyectoPK) {
        this.etapaProyectoPK = etapaProyectoPK;
    }

    public short getMeses() {
        return meses;
    }

    public void setMeses(short meses) {
        this.meses = meses;
    }

    public short getAnios() {
        return anios;
    }

    public void setAnios(short anios) {
        this.anios = anios;
    }

    public String getEtapaDescripcionObraAct() {
        return etapaDescripcionObraAct;
    }

    public void setEtapaDescripcionObraAct(String etapaDescripcionObraAct) {
        this.etapaDescripcionObraAct = etapaDescripcionObraAct;
    }

    public Short getSemanas() {
        return semanas;
    }

    public void setSemanas(Short semanas) {
        this.semanas = semanas;
    }

    public CatEtapa getCatEtapa() {
        return catEtapa;
    }

    public void setCatEtapa(CatEtapa catEtapa) {
        this.catEtapa = catEtapa;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @XmlTransient
    public List<ContaminanteProyecto> getContaminanteProyectoList() {
        return contaminanteProyectoList;
    }

    public void setContaminanteProyectoList(List<ContaminanteProyecto> contaminanteProyectoList) {
        this.contaminanteProyectoList = contaminanteProyectoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (etapaProyectoPK != null ? etapaProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EtapaProyecto)) {
            return false;
        }
        EtapaProyecto other = (EtapaProyecto) object;
        if ((this.etapaProyectoPK == null && other.etapaProyectoPK != null) || (this.etapaProyectoPK != null && !this.etapaProyectoPK.equals(other.etapaProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.EtapaProyecto[ etapaProyectoPK=" + etapaProyectoPK + " ]";
    }

    @XmlTransient
    public List<ActividadEtapa> getActividadEtapaList() {
        return actividadEtapaList;
    }

    public void setActividadEtapaList(List<ActividadEtapa> actividadEtapaList) {
        this.actividadEtapaList = actividadEtapaList;
    }
    
}
