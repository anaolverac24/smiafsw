/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "ACUIFEROS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Acuiferos.findAll", query = "SELECT a FROM Acuiferos a"),
    @NamedQuery(name = "Acuiferos.findByFolioVersion", query = "SELECT e.clvAcui, e.nomAcui, e.descDispo, e.fechaDof, e.sobreexp FROM Acuiferos e  WHERE e.acuiferosPK.numFolio = :folio and e.acuiferosPK.version = :serial group by e.clvAcui, e.nomAcui, e.descDispo, e.fechaDof, e.sobreexp")
       })
public class Acuiferos implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AcuiferosPK acuiferosPK;
    @Column(name = "CLV_ACUI")
    private String clvAcui;
    @Column(name = "NOM_ACUI")
    private String nomAcui;
    @Column(name = "DESC_DISPO")
    private String descDispo;
    @Column(name = "FECHA_DOF")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaDof;
    @Column(name = "SOBREEXP")
    private String sobreexp;
    @Column(name = "SUP_EA")
    private Double supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private Double areabuffer;
    @Column(name = "AREA")
    private Double area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "acuiferos")
    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public Acuiferos() {
    }

    public Acuiferos(AcuiferosPK acuiferosPK) {
        this.acuiferosPK = acuiferosPK;
    }

    public Acuiferos(String numFolio, String cveProy, String cveArea, short version) {
        this.acuiferosPK = new AcuiferosPK(numFolio, cveProy, cveArea, version);
    }

    public AcuiferosPK getAcuiferosPK() {
        return acuiferosPK;
    }

    public void setAcuiferosPK(AcuiferosPK acuiferosPK) {
        this.acuiferosPK = acuiferosPK;
    }

    public String getClvAcui() {
        return clvAcui;
    }

    public void setClvAcui(String clvAcui) {
        this.clvAcui = clvAcui;
    }

    public String getNomAcui() {
        return nomAcui;
    }

    public void setNomAcui(String nomAcui) {
        this.nomAcui = nomAcui;
    }

    public String getDescDispo() {
        return descDispo;
    }

    public void setDescDispo(String descDispo) {
        this.descDispo = descDispo;
    }

    public Date getFechaDof() {
        return fechaDof;
    }

    public void setFechaDof(Date fechaDof) {
        this.fechaDof = fechaDof;
    }

    public String getSobreexp() {
        return sobreexp;
    }

    public void setSobreexp(String sobreexp) {
        this.sobreexp = sobreexp;
    }

    public Double getSupEa() {
        return supEa;
    }

    public void setSupEa(Double supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public Double getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(Double areabuffer) {
        this.areabuffer = areabuffer;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (acuiferosPK != null ? acuiferosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acuiferos)) {
            return false;
        }
        Acuiferos other = (Acuiferos) object;
        if ((this.acuiferosPK == null && other.acuiferosPK != null) || (this.acuiferosPK != null && !this.acuiferosPK.equals(other.acuiferosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.Acuiferos[ acuiferosPK=" + acuiferosPK + " ]";
    }
    
}
