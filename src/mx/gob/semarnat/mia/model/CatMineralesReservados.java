/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_MINERALES_RESERVADOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatMineralesReservados.findAll", query = "SELECT c FROM CatMineralesReservados c")})
public class CatMineralesReservados implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_MINERALES")
    private Short idMinerales;
    @Basic(optional = false)
    @Column(name = "ID_CLASIFICACION")
    private short idClasificacion;
    @Basic(optional = false)
    @Column(name = "DESCRIPCION_MINERALES")
    private String descripcionMinerales;

    public CatMineralesReservados() {
    }

    public CatMineralesReservados(Short idMinerales) {
        this.idMinerales = idMinerales;
    }

    public CatMineralesReservados(Short idMinerales, short idClasificacion, String descripcionMinerales) {
        this.idMinerales = idMinerales;
        this.idClasificacion = idClasificacion;
        this.descripcionMinerales = descripcionMinerales;
    }

    public Short getIdMinerales() {
        return idMinerales;
    }

    public void setIdMinerales(Short idMinerales) {
        this.idMinerales = idMinerales;
    }

    public short getIdClasificacion() {
        return idClasificacion;
    }

    public void setIdClasificacion(short idClasificacion) {
        this.idClasificacion = idClasificacion;
    }

    public String getDescripcionMinerales() {
        return descripcionMinerales;
    }

    public void setDescripcionMinerales(String descripcionMinerales) {
        this.descripcionMinerales = descripcionMinerales;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMinerales != null ? idMinerales.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatMineralesReservados)) {
            return false;
        }
        CatMineralesReservados other = (CatMineralesReservados) object;
        if ((this.idMinerales == null && other.idMinerales != null) || (this.idMinerales != null && !this.idMinerales.equals(other.idMinerales))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatMineralesReservados[ idMinerales=" + idMinerales + " ]";
    }
    
}
