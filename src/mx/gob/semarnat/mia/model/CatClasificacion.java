/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_CLASIFICACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatClasificacion.findAll", query = "SELECT c FROM CatClasificacion c"),
    @NamedQuery(name = "CatClasificacion.findByClasificacionId", query = "SELECT c FROM CatClasificacion c WHERE c.clasificacionId = :clasificacionId"),
    @NamedQuery(name = "CatClasificacion.findByClasificacionDescripcion", query = "SELECT c FROM CatClasificacion c WHERE c.clasificacionDescripcion = :clasificacionDescripcion")})
public class CatClasificacion implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clasificacionId")
    private List<PrediocolinProy> prediocolinProyList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CLASIFICACION_ID")
    private Short clasificacionId;
    @Column(name = "CLASIFICACION_DESCRIPCION")
    private String clasificacionDescripcion;

    public CatClasificacion() {
    }

    public CatClasificacion(Short clasificacionId) {
        this.clasificacionId = clasificacionId;
    }

    public Short getClasificacionId() {
        return clasificacionId;
    }

    public void setClasificacionId(Short clasificacionId) {
        this.clasificacionId = clasificacionId;
    }

    public String getClasificacionDescripcion() {
        return clasificacionDescripcion;
    }

    public void setClasificacionDescripcion(String clasificacionDescripcion) {
        this.clasificacionDescripcion = clasificacionDescripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clasificacionId != null ? clasificacionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatClasificacion)) {
            return false;
        }
        CatClasificacion other = (CatClasificacion) object;
        if ((this.clasificacionId == null && other.clasificacionId != null) || (this.clasificacionId != null && !this.clasificacionId.equals(other.clasificacionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatClasificacion[ clasificacionId=" + clasificacionId + " ]";
    }

    @XmlTransient
    public List<PrediocolinProy> getPrediocolinProyList() {
        return prediocolinProyList;
    }

    public void setPrediocolinProyList(List<PrediocolinProy> prediocolinProyList) {
        this.prediocolinProyList = prediocolinProyList;
    }
    
}
