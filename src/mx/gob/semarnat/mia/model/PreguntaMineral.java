/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "PREGUNTA_MINERAL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PreguntaMineral.findAll", query = "SELECT p FROM PreguntaMineral p")})
public class PreguntaMineral implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PreguntaMineralPK preguntaMineralPK;

    public PreguntaMineral() {
    }

    public PreguntaMineral(PreguntaMineralPK preguntaMineralPK) {
        this.preguntaMineralPK = preguntaMineralPK;
    }

    public PreguntaMineral(String folioProyecto, short serialProyecto, short preguntaId, short idMinerales) {
        this.preguntaMineralPK = new PreguntaMineralPK(folioProyecto, serialProyecto, preguntaId, idMinerales);
    }

    public PreguntaMineralPK getPreguntaMineralPK() {
        return preguntaMineralPK;
    }

    public void setPreguntaMineralPK(PreguntaMineralPK preguntaMineralPK) {
        this.preguntaMineralPK = preguntaMineralPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (preguntaMineralPK != null ? preguntaMineralPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PreguntaMineral)) {
            return false;
        }
        PreguntaMineral other = (PreguntaMineral) object;
        if ((this.preguntaMineralPK == null && other.preguntaMineralPK != null) || (this.preguntaMineralPK != null && !this.preguntaMineralPK.equals(other.preguntaMineralPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.PreguntaMineral[ preguntaMineralPK=" + preguntaMineralPK + " ]";
    }
    
}
