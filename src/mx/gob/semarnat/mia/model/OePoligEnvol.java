/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "OE_POLIG_ENVOL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OePoligEnvol.findAll", query = "SELECT o FROM OePoligEnvol o")})
public class OePoligEnvol implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OePoligEnvolPK oePoligEnvolPK;
    @Column(name = "NOM_OE")
    private String nomOe;
    @Column(name = "TIPO_OE")
    private String tipoOe;
    @Column(name = "UGA")
    private String uga;
    @Column(name = "UGA_COMP")
    private String ugaComp;
    @Column(name = "POLITICA")
    private String politica;
    @Column(name = "POL_MAPEAR")
    private String polMapear;
    @Column(name = "USO_PRED")
    private String usoPred;
    @Column(name = "CRITERIOS")
    private String criterios;
    @Column(name = "SUP_EA")
    private Double supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private Double areabuffer;
    @Column(name = "AREA")
    private Double area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oePoligEnvol")
    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public OePoligEnvol() {
    }

    public OePoligEnvol(OePoligEnvolPK oePoligEnvolPK) {
        this.oePoligEnvolPK = oePoligEnvolPK;
    }

    public OePoligEnvol(String numFolio, String cveProy, String cveArea, short version) {
        this.oePoligEnvolPK = new OePoligEnvolPK(numFolio, cveProy, cveArea, version);
    }

    public OePoligEnvolPK getOePoligEnvolPK() {
        return oePoligEnvolPK;
    }

    public void setOePoligEnvolPK(OePoligEnvolPK oePoligEnvolPK) {
        this.oePoligEnvolPK = oePoligEnvolPK;
    }

    public String getNomOe() {
        return nomOe;
    }

    public void setNomOe(String nomOe) {
        this.nomOe = nomOe;
    }

    public String getTipoOe() {
        return tipoOe;
    }

    public void setTipoOe(String tipoOe) {
        this.tipoOe = tipoOe;
    }

    public String getUga() {
        return uga;
    }

    public void setUga(String uga) {
        this.uga = uga;
    }

    public String getUgaComp() {
        return ugaComp;
    }

    public void setUgaComp(String ugaComp) {
        this.ugaComp = ugaComp;
    }

    public String getPolitica() {
        return politica;
    }

    public void setPolitica(String politica) {
        this.politica = politica;
    }

    public String getPolMapear() {
        return polMapear;
    }

    public void setPolMapear(String polMapear) {
        this.polMapear = polMapear;
    }

    public String getUsoPred() {
        return usoPred;
    }

    public void setUsoPred(String usoPred) {
        this.usoPred = usoPred;
    }

    public String getCriterios() {
        return criterios;
    }

    public void setCriterios(String criterios) {
        this.criterios = criterios;
    }

    public Double getSupEa() {
        return supEa;
    }

    public void setSupEa(Double supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public Double getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(Double areabuffer) {
        this.areabuffer = areabuffer;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (oePoligEnvolPK != null ? oePoligEnvolPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OePoligEnvol)) {
            return false;
        }
        OePoligEnvol other = (OePoligEnvol) object;
        if ((this.oePoligEnvolPK == null && other.oePoligEnvolPK != null) || (this.oePoligEnvolPK != null && !this.oePoligEnvolPK.equals(other.oePoligEnvolPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.OePoligEnvol[ oePoligEnvolPK=" + oePoligEnvolPK + " ]";
    }
    
}
