/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author mauricio
 */
@Embeddable
public class PrediocolinProyPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "FOLIO_PROYECTO")
    private String folioProyecto;
    @Basic(optional = false)
    @Column(name = "SERIAL_PROYECTO")
    private short serialProyecto;
    @Basic(optional = false)
    @Column(name = "PREDIO_COLIN_ID")
    private short predioColinId;

    public PrediocolinProyPK() {
    }

    public PrediocolinProyPK(String folioProyecto, short serialProyecto, short predioColinId) {
        this.folioProyecto = folioProyecto;
        this.serialProyecto = serialProyecto;
        this.predioColinId = predioColinId;
    }

    public String getFolioProyecto() {
        return folioProyecto;
    }

    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    public short getSerialProyecto() {
        return serialProyecto;
    }

    public void setSerialProyecto(short serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    public int getPredioColinId() {
        return predioColinId;
    }

    public void setPredioColinId(short predioColinId) {
        this.predioColinId = predioColinId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (folioProyecto != null ? folioProyecto.hashCode() : 0);
        hash += (int) serialProyecto;
        hash += (int) predioColinId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PrediocolinProyPK)) {
            return false;
        }
        PrediocolinProyPK other = (PrediocolinProyPK) object;
        if ((this.folioProyecto == null && other.folioProyecto != null) || (this.folioProyecto != null && !this.folioProyecto.equals(other.folioProyecto))) {
            return false;
        }
        if (this.serialProyecto != other.serialProyecto) {
            return false;
        }
        if (this.predioColinId != other.predioColinId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.PrediocolinProyPK[ folioProyecto=" + folioProyecto + ", serialProyecto=" + serialProyecto + ", predioColinId=" + predioColinId + " ]";
    }
    
}
