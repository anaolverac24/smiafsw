/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "AVANCE_PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AvanceProyecto.findAll", query = "SELECT a FROM AvanceProyecto a")})
public class AvanceProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AvanceProyectoPK avanceProyectoPK;
    @Column(name = "AVANCE")
    private Short avance;

    public AvanceProyecto() {
    }

    public AvanceProyecto(AvanceProyectoPK avanceProyectoPK) {
        this.avanceProyectoPK = avanceProyectoPK;
    }

    public AvanceProyecto(String folioProyecto, short serialProyeco, short capitulo) {
        this.avanceProyectoPK = new AvanceProyectoPK(folioProyecto, serialProyeco, capitulo);
    }

    public AvanceProyectoPK getAvanceProyectoPK() {
        return avanceProyectoPK;
    }

    public void setAvanceProyectoPK(AvanceProyectoPK avanceProyectoPK) {
        this.avanceProyectoPK = avanceProyectoPK;
    }

    public Short getAvance() {
        return avance;
    }

    public void setAvance(Short avance) {
        this.avance = avance;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (avanceProyectoPK != null ? avanceProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AvanceProyecto)) {
            return false;
        }
        AvanceProyecto other = (AvanceProyecto) object;
        if ((this.avanceProyectoPK == null && other.avanceProyectoPK != null) || (this.avanceProyectoPK != null && !this.avanceProyectoPK.equals(other.avanceProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.AvanceProyecto[ avanceProyectoPK=" + avanceProyectoPK + " ]";
    }
    
}
