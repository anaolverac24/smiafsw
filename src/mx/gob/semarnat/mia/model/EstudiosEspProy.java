/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "ESTUDIOS_ESP_PROY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstudiosEspProy.findAll", query = "SELECT e FROM EstudiosEspProy e"),
    @NamedQuery(name = "EstudiosEspProy.findByFolioSerial", query = "SELECT e FROM EstudiosEspProy e WHERE e.estudiosEspProyPK.folioSerial = :folioSerial"),
    @NamedQuery(name = "EstudiosEspProy.findBySerialProyecto", query = "SELECT e FROM EstudiosEspProy e WHERE e.estudiosEspProyPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "EstudiosEspProy.findByEstudioId", query = "SELECT e FROM EstudiosEspProy e WHERE e.estudiosEspProyPK.estudioId = :estudioId"),
    @NamedQuery(name = "EstudiosEspProy.findByEstudioEspecial", query = "SELECT e FROM EstudiosEspProy e WHERE e.estudioEspecial = :estudioEspecial"),
    @NamedQuery(name = "EstudiosEspProy.findByAnexoUrl", query = "SELECT e FROM EstudiosEspProy e WHERE e.anexoUrl = :anexoUrl"),
    @NamedQuery(name = "EstudiosEspProy.findByAnexoDescripcion", query = "SELECT e FROM EstudiosEspProy e WHERE e.anexoDescripcion = :anexoDescripcion"),
    @NamedQuery(name = "EstudiosEspProy.findByAnexoNombre", query = "SELECT e FROM EstudiosEspProy e WHERE e.anexoNombre = :anexoNombre"),
    @NamedQuery(name = "EstudiosEspProy.findByAnexoTamanio", query = "SELECT e FROM EstudiosEspProy e WHERE e.anexoTamanio = :anexoTamanio"),
    @NamedQuery(name = "EstudiosEspProy.findBySerialFolio", query = "SELECT e FROM EstudiosEspProy e  WHERE e.estudiosEspProyPK.folioSerial = :folio and e.estudiosEspProyPK.serialProyecto = :serial"), 
    @NamedQuery(name = "EstudiosEspProy.findByAnexoExtension", query = "SELECT e FROM EstudiosEspProy e WHERE e.anexoExtension = :anexoExtension")})
public class EstudiosEspProy implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EstudiosEspProyPK estudiosEspProyPK;
    @Column(name = "ESTUDIO_ESPECIAL")
    private Short estudioEspecial;
    @Column(name = "ANEXO_URL")
    private String anexoUrl;
    @Column(name = "ANEXO_DESCRIPCION")
    private String anexoDescripcion;
    @Column(name = "ANEXO_NOMBRE")
    private String anexoNombre;
    
    @Column(name = "ANEXO_TAMANIO")
    private BigDecimal anexoTamanio;
    @Column(name = "ANEXO_EXTENSION")
    private String anexoExtension;
    
    @Column(name = "ESTUDIO_ESP_NOMBRE_PROM")
    private String nombrePromovente;
    
    @Column(name ="CAPITULO_ID")
    private short  capituloId;
    
    @Column(name="SECCION_ID")
    private short seccionId;
    
    @Column(name="RESERVADO")
    private String reservado;
    
    
    @JoinColumn(name = "ESTUDIO_ESPECIAL", referencedColumnName = "ESTUDIO_ID", insertable = false, updatable = false)
    @ManyToOne(optional = true)
    private CatEstEsp catEstsEsp;
    
    
    
    

    public EstudiosEspProy() {
    }

    public EstudiosEspProy(EstudiosEspProyPK estudiosEspProyPK) {
        this.estudiosEspProyPK = estudiosEspProyPK;
    }

    public EstudiosEspProy(String folioSerial, short serialProyecto, short estudioId) {
        this.estudiosEspProyPK = new EstudiosEspProyPK(folioSerial, serialProyecto, estudioId);
    }

    public EstudiosEspProyPK getEstudiosEspProyPK() {
        return estudiosEspProyPK;
    }

    public void setEstudiosEspProyPK(EstudiosEspProyPK estudiosEspProyPK) {
        this.estudiosEspProyPK = estudiosEspProyPK;
    }

    public String getAnexoUrl() {
        return anexoUrl;
    }

    public void setAnexoUrl(String anexoUrl) { 
        this.anexoUrl = anexoUrl;
    }

    public String getAnexoDescripcion() {
        return anexoDescripcion;
    }

    public void setAnexoDescripcion(String anexoDescripcion) {
        this.anexoDescripcion = anexoDescripcion;
    }

    public String getAnexoNombre() {
        return anexoNombre;
    }

    public void setAnexoNombre(String anexoNombre) {
        this.anexoNombre = anexoNombre;
    }

    public BigDecimal getAnexoTamanio() {
        return anexoTamanio;
    }

    public void setAnexoTamanio(BigDecimal anexoTamanio) {
        this.anexoTamanio = anexoTamanio;
    }

    public String getAnexoExtension() {
        return anexoExtension;
    }

    public void setAnexoExtension(String anexoExtension) {
        this.anexoExtension = anexoExtension;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (estudiosEspProyPK != null ? estudiosEspProyPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstudiosEspProy)) {
            return false;
        }
        EstudiosEspProy other = (EstudiosEspProy) object;
        if ((this.estudiosEspProyPK == null && other.estudiosEspProyPK != null) || (this.estudiosEspProyPK != null && !this.estudiosEspProyPK.equals(other.estudiosEspProyPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.EstudiosEspProy[ estudiosEspProyPK=" + estudiosEspProyPK + " ]";
    }

    public Short getEstudioEspecial() {
        return estudioEspecial;
    }

    public void setEstudioEspecial(Short estudioEpecial) {
        this.estudioEspecial = estudioEpecial;
    }

	public String getNombrePromovente() {
		return nombrePromovente;
	}

	public void setNombrePromovente(String nombrePromovente) {
		this.nombrePromovente = nombrePromovente;
	}

	public short getCapituloId() {
		return capituloId;
	}

	public void setCapituloId(short capituloId) {
		this.capituloId = capituloId;
	}

	public short getSeccionId() {
		return seccionId;
	}

	public void setSeccionId(short seccionId) {
		this.seccionId = seccionId;
	}

	public String getReservado() {
		return reservado;
	}

	public void setReservado(String reservado) {
		this.reservado = reservado;
	}

	public CatEstEsp getCatEstsEsp() {
		return catEstsEsp;
	}

	public void setCatEstsEsp(CatEstEsp catEstsEsp) {
		this.catEstsEsp = catEstsEsp;
	}
    
    
    
    
    
    
}
