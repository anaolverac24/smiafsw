/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_POET")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatPoet.findAll", query = "SELECT c FROM CatPoet c")})
public class CatPoet implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "POET_ID")
    private Short poetId;
    @Basic(optional = false)
    @Column(name = "POET_DESCRIPCION")
    private String poetDescripcion;
    @Column(name = "POET_FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date poetFecha;
    @Column(name = "POET_OFICIO")
    private String poetOficio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catPoet")
    private List<PoetProyecto> poetProyectoList;

    public CatPoet() {
    }

    public CatPoet(Short poetId) {
        this.poetId = poetId;
    }

    public CatPoet(Short poetId, String poetDescripcion) {
        this.poetId = poetId;
        this.poetDescripcion = poetDescripcion;
    }

    public Short getPoetId() {
        return poetId;
    }

    public void setPoetId(Short poetId) {
        this.poetId = poetId;
    }

    public String getPoetDescripcion() {
        return poetDescripcion;
    }

    public void setPoetDescripcion(String poetDescripcion) {
        this.poetDescripcion = poetDescripcion;
    }

    public Date getPoetFecha() {
        return poetFecha;
    }

    public void setPoetFecha(Date poetFecha) {
        this.poetFecha = poetFecha;
    }

    public String getPoetOficio() {
        return poetOficio;
    }

    public void setPoetOficio(String poetOficio) {
        this.poetOficio = poetOficio;
    }

    @XmlTransient
    public List<PoetProyecto> getPoetProyectoList() {
        return poetProyectoList;
    }

    public void setPoetProyectoList(List<PoetProyecto> poetProyectoList) {
        this.poetProyectoList = poetProyectoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (poetId != null ? poetId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatPoet)) {
            return false;
        }
        CatPoet other = (CatPoet) object;
        if ((this.poetId == null && other.poetId != null) || (this.poetId != null && !this.poetId.equals(other.poetId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatPoet[ poetId=" + poetId + " ]";
    }
    
}
