/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_PARQUE_INDUSTRIAL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatParqueIndustrial.findAll", query = "SELECT c FROM CatParqueIndustrial c")})
public class CatParqueIndustrial implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PARQUE_ID")
    private Short parqueId;
    @Basic(optional = false)
    @Column(name = "PARQUE_DESCRIPCION")
    private String parqueDescripcion;
    @Column(name = "PARQUE_FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date parqueFecha;
    @Column(name = "PARQUE_OFICIO")
    private String parqueOficio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catParqueIndustrial")
    private List<ParqueIndustrialProyecto> parqueIndustrialProyectoList;

    public CatParqueIndustrial() {
    }

    public CatParqueIndustrial(Short parqueId) {
        this.parqueId = parqueId;
    }

    public CatParqueIndustrial(Short parqueId, String parqueDescripcion) {
        this.parqueId = parqueId;
        this.parqueDescripcion = parqueDescripcion;
    }

    public Short getParqueId() {
        return parqueId;
    }

    public void setParqueId(Short parqueId) {
        this.parqueId = parqueId;
    }

    public String getParqueDescripcion() {
        return parqueDescripcion;
    }

    public void setParqueDescripcion(String parqueDescripcion) {
        this.parqueDescripcion = parqueDescripcion;
    }

    public Date getParqueFecha() {
        return parqueFecha;
    }

    public void setParqueFecha(Date parqueFecha) {
        this.parqueFecha = parqueFecha;
    }

    public String getParqueOficio() {
        return parqueOficio;
    }

    public void setParqueOficio(String parqueOficio) {
        this.parqueOficio = parqueOficio;
    }

    @XmlTransient
    public List<ParqueIndustrialProyecto> getParqueIndustrialProyectoList() {
        return parqueIndustrialProyectoList;
    }

    public void setParqueIndustrialProyectoList(List<ParqueIndustrialProyecto> parqueIndustrialProyectoList) {
        this.parqueIndustrialProyectoList = parqueIndustrialProyectoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (parqueId != null ? parqueId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatParqueIndustrial)) {
            return false;
        }
        CatParqueIndustrial other = (CatParqueIndustrial) object;
        if ((this.parqueId == null && other.parqueId != null) || (this.parqueId != null && !this.parqueId.equals(other.parqueId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatParqueIndustrial[ parqueId=" + parqueId + " ]";
    }
    
}
