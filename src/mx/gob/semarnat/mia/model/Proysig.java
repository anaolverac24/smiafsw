/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "PROYSIG")
public class Proysig implements Serializable {

    @Id
    @Column(name = "OBJECTID")
    private int ObjectId;
    @Column(name = "NUM_FOLIO")
    private String numFolio;
    @Column(name = "CVE_PROY")
    private String cveProy;
    @Column(name = "CVE_AREA")
    private String cveArea;
    @Column(name = "VERSION")
    private Short version;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "RESOLUCION")
    private String resolucion;
    @Column(name = "ID_RES")
    private Integer idRes;
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ROWLAYER")
    private Integer rowlayer;
    @Column(name = "OBJECTID_1")
    private Integer objectid1;
    @Column(name = "OBJECTID_2")
    private Integer objectid2;
    @Column(name = "SHAPE")
    private Double shape;
    @Column(name = "SHAPE_AREA")
    private BigDecimal shapeArea;
    @Column(name = "SHAPE_LEN")
    private Double shapeLen;
    @Column(name = "PROYPOL_ID")
    private Short proypolId;
    @Column(name = "PROYLIN_ID")
    private Short proylinId;
    @Column(name = "PROYMPUN_ID")
    private Short proympunId;

    /**
     * @return the ObjectId
     */
    public int getObjectId() {
        return ObjectId;
    }

    /**
     * @param ObjectId the ObjectId to set
     */
    public void setObjectId(int ObjectId) {
        this.ObjectId = ObjectId;
    }

    /**
     * @return the numFolio
     */
    public String getNumFolio() {
        return numFolio;
    }

    /**
     * @param numFolio the numFolio to set
     */
    public void setNumFolio(String numFolio) {
        this.numFolio = numFolio;
    }

    /**
     * @return the cveProy
     */
    public String getCveProy() {
        return cveProy;
    }

    /**
     * @param cveProy the cveProy to set
     */
    public void setCveProy(String cveProy) {
        this.cveProy = cveProy;
    }

    /**
     * @return the cveArea
     */
    public String getCveArea() {
        return cveArea;
    }

    /**
     * @param cveArea the cveArea to set
     */
    public void setCveArea(String cveArea) {
        this.cveArea = cveArea;
    }

    /**
     * @return the version
     */
    public Short getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(Short version) {
        this.version = version;
    }

    /**
     * @return the proy
     */
    public String getProy() {
        return proy;
    }

    /**
     * @param proy the proy to set
     */
    public void setProy(String proy) {
        this.proy = proy;
    }

    /**
     * @return the descrip
     */
    public String getDescrip() {
        return descrip;
    }

    /**
     * @param descrip the descrip to set
     */
    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    /**
     * @return the comp
     */
    public String getComp() {
        return comp;
    }

    /**
     * @param comp the comp to set
     */
    public void setComp(String comp) {
        this.comp = comp;
    }

    /**
     * @return the resolucion
     */
    public String getResolucion() {
        return resolucion;
    }

    /**
     * @param resolucion the resolucion to set
     */
    public void setResolucion(String resolucion) {
        this.resolucion = resolucion;
    }

    /**
     * @return the idRes
     */
    public Integer getIdRes() {
        return idRes;
    }

    /**
     * @param idRes the idRes to set
     */
    public void setIdRes(Integer idRes) {
        this.idRes = idRes;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the rowlayer
     */
    public Integer getRowlayer() {
        return rowlayer;
    }

    /**
     * @param rowlayer the rowlayer to set
     */
    public void setRowlayer(Integer rowlayer) {
        this.rowlayer = rowlayer;
    }

    /**
     * @return the objectid1
     */
    public Integer getObjectid1() {
        return objectid1;
    }

    /**
     * @param objectid1 the objectid1 to set
     */
    public void setObjectid1(Integer objectid1) {
        this.objectid1 = objectid1;
    }

    /**
     * @return the objectid2
     */
    public Integer getObjectid2() {
        return objectid2;
    }

    /**
     * @param objectid2 the objectid2 to set
     */
    public void setObjectid2(Integer objectid2) {
        this.objectid2 = objectid2;
    }

    /**
     * @return the shape
     */
    public Double getShape() {
        return shape;
    }

    /**
     * @param shape the shape to set
     */
    public void setShape(Double shape) {
        this.shape = shape;
    }

    /**
     * @return the shapeArea
     */
    public BigDecimal getShapeArea() {
        return shapeArea;
    }

    /**
     * @param shapeArea the shapeArea to set
     */
    public void setShapeArea(BigDecimal shapeArea) {
        this.shapeArea = shapeArea;
    }

    /**
     * @return the shapeLen
     */
    public Double getShapeLen() {
        return shapeLen;
    }

    /**
     * @param shapeLen the shapeLen to set
     */
    public void setShapeLen(Double shapeLen) {
        this.shapeLen = shapeLen;
    }

    /**
     * @return the proypolId
     */
    public Short getProypolId() {
        return proypolId;
    }

    /**
     * @param proypolId the proypolId to set
     */
    public void setProypolId(Short proypolId) {
        this.proypolId = proypolId;
    }

    /**
     * @return the proylinId
     */
    public Short getProylinId() {
        return proylinId;
    }

    /**
     * @param proylinId the proylinId to set
     */
    public void setProylinId(Short proylinId) {
        this.proylinId = proylinId;
    }

    /**
     * @return the proympunId
     */
    public Short getProympunId() {
        return proympunId;
    }

    /**
     * @param proympunId the proympunId to set
     */
    public void setProympunId(Short proympunId) {
        this.proympunId = proympunId;
    }
    

}
