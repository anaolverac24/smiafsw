/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_ETAPA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatEtapa.findAll", query = "SELECT c FROM CatEtapa c")})
public class CatEtapa implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "etapaId")
    private List<MedPrevImpactProy> medPrevImpactProyList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "etapaId")
    private List<ImpacAmbProyecto> impacAmbProyectoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catEtapa")
    private List<ServicioProyecto> servicioProyectoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catEtapa")
    private List<InsumosProyecto> insumosProyectoList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ETAPA_ID")
    private Short etapaId;
    @Basic(optional = false)
    @Column(name = "ETAPA_DESCRIPCION")
    private String etapaDescripcion;
    @OneToMany(mappedBy = "etapaId")
    private List<ImpactoProyecto> impactoProyectoList;
    @OneToMany(mappedBy = "etapaId")
    private List<SustanciaProyecto> sustanciaProyectoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catEtapa")
    private List<EtapaProyecto> etapaProyectoList;

    public CatEtapa() {
    }

    public CatEtapa(Short etapaId) {
        this.etapaId = etapaId;
    }

    public CatEtapa(Short etapaId, String etapaDescripcion) {
        this.etapaId = etapaId;
        this.etapaDescripcion = etapaDescripcion;
    }

    public Short getEtapaId() {
        return etapaId;
    }

    public void setEtapaId(Short etapaId) {
        this.etapaId = etapaId;
    }

    public String getEtapaDescripcion() {
        return etapaDescripcion;
    }

    public void setEtapaDescripcion(String etapaDescripcion) {
        this.etapaDescripcion = etapaDescripcion;
    }

    @XmlTransient
    public List<ImpactoProyecto> getImpactoProyectoList() {
        return impactoProyectoList;
    }

    public void setImpactoProyectoList(List<ImpactoProyecto> impactoProyectoList) {
        this.impactoProyectoList = impactoProyectoList;
    }

    @XmlTransient
    public List<SustanciaProyecto> getSustanciaProyectoList() {
        return sustanciaProyectoList;
    }

    public void setSustanciaProyectoList(List<SustanciaProyecto> sustanciaProyectoList) {
        this.sustanciaProyectoList = sustanciaProyectoList;
    }

    @XmlTransient
    public List<EtapaProyecto> getEtapaProyectoList() {
        return etapaProyectoList;
    }

    public void setEtapaProyectoList(List<EtapaProyecto> etapaProyectoList) {
        this.etapaProyectoList = etapaProyectoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (etapaId != null ? etapaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatEtapa)) {
            return false;
        }
        CatEtapa other = (CatEtapa) object;
        if ((this.etapaId == null && other.etapaId != null) || (this.etapaId != null && !this.etapaId.equals(other.etapaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatEtapa[ etapaId=" + etapaId + " ]";
    }

    @XmlTransient
    public List<InsumosProyecto> getInsumosProyectoList() {
        return insumosProyectoList;
    }

    public void setInsumosProyectoList(List<InsumosProyecto> insumosProyectoList) {
        this.insumosProyectoList = insumosProyectoList;
    }

    @XmlTransient
    public List<ServicioProyecto> getServicioProyectoList() {
        return servicioProyectoList;
    }

    public void setServicioProyectoList(List<ServicioProyecto> servicioProyectoList) {
        this.servicioProyectoList = servicioProyectoList;
    }

    @XmlTransient
    public List<ImpacAmbProyecto> getImpacAmbProyectoList() {
        return impacAmbProyectoList;
    }

    public void setImpacAmbProyectoList(List<ImpacAmbProyecto> impacAmbProyectoList) {
        this.impacAmbProyectoList = impacAmbProyectoList;
    }

    @XmlTransient
    public List<MedPrevImpactProy> getMedPrevImpactProyList() {
        return medPrevImpactProyList;
    }

    public void setMedPrevImpactProyList(List<MedPrevImpactProy> medPrevImpactProyList) {
        this.medPrevImpactProyList = medPrevImpactProyList;
    }
    
}
