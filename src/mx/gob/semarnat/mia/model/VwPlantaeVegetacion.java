/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "VW_PLANTAE_VEGETACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VwPlantaeVegetacion.findAll", query = "SELECT v FROM VwPlantaeVegetacion v"),
    @NamedQuery(name = "VwPlantaeVegetacion.findByPlantaeReino", query = "SELECT v FROM VwPlantaeVegetacion v WHERE v.plantaeReino = :plantaeReino"),
    @NamedQuery(name = "VwPlantaeVegetacion.findByPlantaeClase", query = "SELECT v FROM VwPlantaeVegetacion v WHERE v.plantaeClase = :plantaeClase"),
    @NamedQuery(name = "VwPlantaeVegetacion.findByPlantaeOrden", query = "SELECT v FROM VwPlantaeVegetacion v WHERE v.plantaeOrden = :plantaeOrden"),
    @NamedQuery(name = "VwPlantaeVegetacion.findByPlantaeFamilia", query = "SELECT v FROM VwPlantaeVegetacion v WHERE v.plantaeFamilia = :plantaeFamilia"),
    @NamedQuery(name = "VwPlantaeVegetacion.findByPlantaeGenero", query = "SELECT v FROM VwPlantaeVegetacion v WHERE v.plantaeGenero = :plantaeGenero"),
    @NamedQuery(name = "VwPlantaeVegetacion.findByPlantaeEspecie", query = "SELECT v FROM VwPlantaeVegetacion v WHERE v.plantaeEspecie = :plantaeEspecie"),
    @NamedQuery(name = "VwPlantaeVegetacion.findByPlantaeNombreCientifico", query = "SELECT v FROM VwPlantaeVegetacion v WHERE v.plantaeNombreCientifico = :plantaeNombreCientifico"),
    @NamedQuery(name = "VwPlantaeVegetacion.findByPlantaeNombreComun", query = "SELECT v FROM VwPlantaeVegetacion v WHERE v.plantaeNombreComun = :plantaeNombreComun"),
    @NamedQuery(name = "VwPlantaeVegetacion.findByPlantaeCategoriaNom059", query = "SELECT v FROM VwPlantaeVegetacion v WHERE v.plantaeCategoriaNom059 = :plantaeCategoriaNom059"),
    @NamedQuery(name = "VwPlantaeVegetacion.findByPlantaeCites", query = "SELECT v FROM VwPlantaeVegetacion v WHERE v.plantaeCites = :plantaeCites")})
public class VwPlantaeVegetacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "PLANTAE_REINO")
    private String plantaeReino;
    @Column(name = "PLANTAE_CLASE")
    private String plantaeClase;
    @Column(name = "PLANTAE_ORDEN")
    private String plantaeOrden;
    @Column(name = "PLANTAE_FAMILIA")
    private String plantaeFamilia;
    @Column(name = "PLANTAE_GENERO")
    private String plantaeGenero;
    @Column(name = "PLANTAE_ESPECIE")
    private String plantaeEspecie;
    @Column(name = "PLANTAE_NOMBRE_CIENTIFICO")
    private String plantaeNombreCientifico;
    @Column(name = "PLANTAE_NOMBRE_COMUN")
    private String plantaeNombreComun;
    @Column(name = "PLANTAE_CATEGORIA_NOM059")
    private String plantaeCategoriaNom059;
    @Column(name = "PLANTAE_CITES")
    private String plantaeCites;

    @Override
    public String toString() {
        return "VwPlantaeVegetacion{" + "plantaeReino=" + plantaeReino + ", plantaeClase=" + plantaeClase + ", plantaeOrden=" + plantaeOrden + ", plantaeFamilia=" + plantaeFamilia + ", plantaeGenero=" + plantaeGenero + ", plantaeEspecie=" + plantaeEspecie + ", plantaeNombreCientifico=" + plantaeNombreCientifico + ", plantaeNombreComun=" + plantaeNombreComun + ", plantaeCategoriaNom059=" + plantaeCategoriaNom059 + ", plantaeCites=" + plantaeCites + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.plantaeNombreCientifico != null ? this.plantaeNombreCientifico.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VwPlantaeVegetacion other = (VwPlantaeVegetacion) obj;
        if ((this.plantaeNombreCientifico == null) ? (other.plantaeNombreCientifico != null) : !this.plantaeNombreCientifico.equals(other.plantaeNombreCientifico)) {
            return false;
        }
        return true;
    }

    public VwPlantaeVegetacion() {
    }

    public String getPlantaeReino() {
        return plantaeReino;
    }

    public void setPlantaeReino(String plantaeReino) {
        this.plantaeReino = plantaeReino;
    }

    public String getPlantaeClase() {
        return plantaeClase;
    }

    public void setPlantaeClase(String plantaeClase) {
        this.plantaeClase = plantaeClase;
    }

    public String getPlantaeOrden() {
        return plantaeOrden;
    }

    public void setPlantaeOrden(String plantaeOrden) {
        this.plantaeOrden = plantaeOrden;
    }

    public String getPlantaeFamilia() {
        return plantaeFamilia;
    }

    public void setPlantaeFamilia(String plantaeFamilia) {
        this.plantaeFamilia = plantaeFamilia;
    }

    public String getPlantaeGenero() {
        return plantaeGenero;
    }

    public void setPlantaeGenero(String plantaeGenero) {
        this.plantaeGenero = plantaeGenero;
    }

    public String getPlantaeEspecie() {
        return plantaeEspecie;
    }

    public void setPlantaeEspecie(String plantaeEspecie) {
        this.plantaeEspecie = plantaeEspecie;
    }

    public String getPlantaeNombreCientifico() {
        return plantaeNombreCientifico;
    }

    public void setPlantaeNombreCientifico(String plantaeNombreCientifico) {
        this.plantaeNombreCientifico = plantaeNombreCientifico;
    }

    public String getPlantaeNombreComun() {
        return plantaeNombreComun;
    }

    public void setPlantaeNombreComun(String plantaeNombreComun) {
        this.plantaeNombreComun = plantaeNombreComun;
    }

    public String getPlantaeCategoriaNom059() {
        return plantaeCategoriaNom059;
    }

    public void setPlantaeCategoriaNom059(String plantaeCategoriaNom059) {
        this.plantaeCategoriaNom059 = plantaeCategoriaNom059;
    }

    public String getPlantaeCites() {
        return plantaeCites;
    }

    public void setPlantaeCites(String plantaeCites) {
        this.plantaeCites = plantaeCites;
    }
    
}
