/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Humberto
 */
@Entity
@Table(name = "EVALUACION_PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EvaluacionProyecto.findAll", query = "SELECT e FROM EvaluacionProyecto e"),
    @NamedQuery(name = "EvaluacionProyecto.findByBitacoraProyecto", query = "SELECT e FROM EvaluacionProyecto e WHERE e.bitacoraProyecto = :bitacoraProyecto"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaSistesisProyecto", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaSistesisProyecto = :evaSistesisProyecto"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaValCritPromovente", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaValCritPromovente = :evaValCritPromovente"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaValCritEvaluador", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaValCritEvaluador = :evaValCritEvaluador"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaMontCritPromovente", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaMontCritPromovente = :evaMontCritPromovente"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaMontCritEvaluador", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaMontCritEvaluador = :evaMontCritEvaluador"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaCheckListRealizado", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaCheckListRealizado = :evaCheckListRealizado"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaPagoDerechosRealizado", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaPagoDerechosRealizado = :evaPagoDerechosRealizado"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaGacetaFechaPublicacion", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaGacetaFechaPublicacion = :evaGacetaFechaPublicacion"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaGacetaSeparata", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaGacetaSeparata = :evaGacetaSeparata"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaGacetaPeriodoInicio", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaGacetaPeriodoInicio = :evaGacetaPeriodoInicio"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaGacetaPeriodoFinal", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaGacetaPeriodoFinal = :evaGacetaPeriodoFinal"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaExtractoFechaPublicacion", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaExtractoFechaPublicacion = :evaExtractoFechaPublicacion"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaExtractoDiarios", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaExtractoDiarios = :evaExtractoDiarios"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaLotePrevencion", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaLotePrevencion = :evaLotePrevencion"),
    @NamedQuery(name = "EvaluacionProyecto.findByEvaLoteInfoAdicional", query = "SELECT e FROM EvaluacionProyecto e WHERE e.evaLoteInfoAdicional = :evaLoteInfoAdicional")})
public class EvaluacionProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "BITACORA_PROYECTO")
    private String bitacoraProyecto;
    @Column(name = "EVA_SISTESIS_PROYECTO")
    private String evaSistesisProyecto;
    @Column(name = "EVA_VAL_CRIT_PROMOVENTE")
    private Short evaValCritPromovente;
    @Column(name = "EVA_VAL_CRIT_EVALUADOR")
    private Short evaValCritEvaluador;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "EVA_MONT_CRIT_PROMOVENTE")
    private BigDecimal evaMontCritPromovente;
    @Column(name = "EVA_MONT_CRIT_EVALUADOR")
    private BigDecimal evaMontCritEvaluador;
    @Column(name = "EVA_CHECK_LIST_REALIZADO")
    private String evaCheckListRealizado;
    @Column(name = "EVA_PAGO_DERECHOS_REALIZADO")
    private String evaPagoDerechosRealizado;
    @Column(name = "EVA_GACETA_FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date evaGacetaFechaPublicacion;
    @Column(name = "EVA_GACETA_SEPARATA")
    private String evaGacetaSeparata;
    @Column(name = "EVA_GACETA_PERIODO_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date evaGacetaPeriodoInicio;
    @Column(name = "EVA_GACETA_PERIODO_FINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date evaGacetaPeriodoFinal;
    @Column(name = "EVA_EXTRACTO_FECHA_PUBLICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date evaExtractoFechaPublicacion;
    @Column(name = "EVA_EXTRACTO_DIARIOS")
    private String evaExtractoDiarios;
    @Column(name = "EVA_LOTE_PREVENCION")
    private Long evaLotePrevencion;
    @Column(name = "EVA_LOTE_INFO_ADICIONAL")
    private Long evaLoteInfoAdicional;

    public EvaluacionProyecto() {
    }

    public EvaluacionProyecto(String bitacoraProyecto) {
        this.bitacoraProyecto = bitacoraProyecto;
    }

    public String getBitacoraProyecto() {
        return bitacoraProyecto;
    }

    public void setBitacoraProyecto(String bitacoraProyecto) {
        this.bitacoraProyecto = bitacoraProyecto;
    }

    public String getEvaSistesisProyecto() {
        return evaSistesisProyecto;
    }

    public void setEvaSistesisProyecto(String evaSistesisProyecto) {
        this.evaSistesisProyecto = evaSistesisProyecto;
    }

    public Short getEvaValCritPromovente() {
        return evaValCritPromovente;
    }

    public void setEvaValCritPromovente(Short evaValCritPromovente) {
        this.evaValCritPromovente = evaValCritPromovente;
    }

    public Short getEvaValCritEvaluador() {
        return evaValCritEvaluador;
    }

    public void setEvaValCritEvaluador(Short evaValCritEvaluador) {
        this.evaValCritEvaluador = evaValCritEvaluador;
    }

    public BigDecimal getEvaMontCritPromovente() {
        return evaMontCritPromovente;
    }

    public void setEvaMontCritPromovente(BigDecimal evaMontCritPromovente) {
        this.evaMontCritPromovente = evaMontCritPromovente;
    }

    public BigDecimal getEvaMontCritEvaluador() {
        return evaMontCritEvaluador;
    }

    public void setEvaMontCritEvaluador(BigDecimal evaMontCritEvaluador) {
        this.evaMontCritEvaluador = evaMontCritEvaluador;
    }

    public String getEvaCheckListRealizado() {
        return evaCheckListRealizado;
    }

    public void setEvaCheckListRealizado(String evaCheckListRealizado) {
        this.evaCheckListRealizado = evaCheckListRealizado;
    }

    public String getEvaPagoDerechosRealizado() {
        return evaPagoDerechosRealizado;
    }

    public void setEvaPagoDerechosRealizado(String evaPagoDerechosRealizado) {
        this.evaPagoDerechosRealizado = evaPagoDerechosRealizado;
    }

    public Date getEvaGacetaFechaPublicacion() {
        return evaGacetaFechaPublicacion;
    }

    public void setEvaGacetaFechaPublicacion(Date evaGacetaFechaPublicacion) {
        this.evaGacetaFechaPublicacion = evaGacetaFechaPublicacion;
    }

    public String getEvaGacetaSeparata() {
        return evaGacetaSeparata;
    }

    public void setEvaGacetaSeparata(String evaGacetaSeparata) {
        this.evaGacetaSeparata = evaGacetaSeparata;
    }

    public Date getEvaGacetaPeriodoInicio() {
        return evaGacetaPeriodoInicio;
    }

    public void setEvaGacetaPeriodoInicio(Date evaGacetaPeriodoInicio) {
        this.evaGacetaPeriodoInicio = evaGacetaPeriodoInicio;
    }

    public Date getEvaGacetaPeriodoFinal() {
        return evaGacetaPeriodoFinal;
    }

    public void setEvaGacetaPeriodoFinal(Date evaGacetaPeriodoFinal) {
        this.evaGacetaPeriodoFinal = evaGacetaPeriodoFinal;
    }

    public Date getEvaExtractoFechaPublicacion() {
        return evaExtractoFechaPublicacion;
    }

    public void setEvaExtractoFechaPublicacion(Date evaExtractoFechaPublicacion) {
        this.evaExtractoFechaPublicacion = evaExtractoFechaPublicacion;
    }

    public String getEvaExtractoDiarios() {
        return evaExtractoDiarios;
    }

    public void setEvaExtractoDiarios(String evaExtractoDiarios) {
        this.evaExtractoDiarios = evaExtractoDiarios;
    }

    public Long getEvaLotePrevencion() {
        return evaLotePrevencion;
    }

    public void setEvaLotePrevencion(Long evaLotePrevencion) {
        this.evaLotePrevencion = evaLotePrevencion;
    }

    public Long getEvaLoteInfoAdicional() {
        return evaLoteInfoAdicional;
    }

    public void setEvaLoteInfoAdicional(Long evaLoteInfoAdicional) {
        this.evaLoteInfoAdicional = evaLoteInfoAdicional;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bitacoraProyecto != null ? bitacoraProyecto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EvaluacionProyecto)) {
            return false;
        }
        EvaluacionProyecto other = (EvaluacionProyecto) object;
        if ((this.bitacoraProyecto == null && other.bitacoraProyecto != null) || (this.bitacoraProyecto != null && !this.bitacoraProyecto.equals(other.bitacoraProyecto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.EvaluacionProyecto[ bitacoraProyecto=" + bitacoraProyecto + " ]";
    }
    
}
