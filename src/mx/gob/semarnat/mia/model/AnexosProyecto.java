/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "ANEXOS_PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AnexosProyecto.findAll", query = "SELECT a FROM AnexosProyecto a")})
public class AnexosProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AnexosProyectoPK anexosProyectoPK;
    @Column(name = "ANEXO_NOMBRE")
    private String anexoNombre;
    @Column(name = "ANEXO_URL")
    private String anexoUrl;
    @Column(name = "ANEXO_EXTENSION")
    private String anexoExtension;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ANEXO_TAMANIO")
    private BigDecimal anexoTamanio;
    @Column(name = "ANEXO_DESCRIPCION")
    private String anexoDescripcion;
    @Column(name = "ANEXO_NOMBRE_PROM")
    private String anexoNombreProm;
    @Column(name = "RESERVADO")
    private String reservado;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public AnexosProyecto() {
    }

    public AnexosProyecto(AnexosProyectoPK anexosProyectoPK) {
        this.anexosProyectoPK = anexosProyectoPK;
    }

    public AnexosProyecto(short capituloId, short subcapituloId, short seccionId, short apartadoId, short anexoId, String folioProyecto, short serialProyecto) {
        this.anexosProyectoPK = new AnexosProyectoPK(capituloId, subcapituloId, seccionId, apartadoId, anexoId, folioProyecto, serialProyecto);
    }

    public AnexosProyectoPK getAnexosProyectoPK() {
        return anexosProyectoPK;
    }

    public void setAnexosProyectoPK(AnexosProyectoPK anexosProyectoPK) {
        this.anexosProyectoPK = anexosProyectoPK;
    }

    public String getAnexoNombre() {
        return anexoNombre;
    }

    public void setAnexoNombre(String anexoNombre) {
        this.anexoNombre = anexoNombre;
    }

    public String getAnexoUrl() {
        return anexoUrl;
    }

    public void setAnexoUrl(String anexoUrl) {
        this.anexoUrl = anexoUrl;
    }

    public String getAnexoExtension() {
        return anexoExtension;
    }

    public void setAnexoExtension(String anexoExtension) {
        this.anexoExtension = anexoExtension;
    }

    public BigDecimal getAnexoTamanio() {
        return anexoTamanio;
    }

    public void setAnexoTamanio(BigDecimal anexoTamanio) {
        this.anexoTamanio = anexoTamanio;
    }

    public String getAnexoDescripcion() {
        return anexoDescripcion;
    }

    public void setAnexoDescripcion(String anexoDescripcion) {
        this.anexoDescripcion = anexoDescripcion;
    }

    public String getAnexoNombreProm() {
		return anexoNombreProm;
	}

	public void setAnexoNombreProm(String anexoNombreProm) {
		this.anexoNombreProm = anexoNombreProm;
	}

	public String getReservado() {
		return reservado;
	}

	public void setReservado(String reservado) {
		this.reservado = reservado;
	}

	public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (anexosProyectoPK != null ? anexosProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnexosProyecto)) {
            return false;
        }
        AnexosProyecto other = (AnexosProyecto) object;
        if ((this.anexosProyectoPK == null && other.anexosProyectoPK != null) || (this.anexosProyectoPK != null && !this.anexosProyectoPK.equals(other.anexosProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.AnexosProyecto[ anexosProyectoPK=" + anexosProyectoPK + " ]";
    }
    
}
