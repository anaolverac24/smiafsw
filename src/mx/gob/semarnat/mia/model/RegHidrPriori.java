/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "REG_HIDR_PRIORI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RegHidrPriori.findAll", query = "SELECT r FROM RegHidrPriori r")})
public class RegHidrPriori implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RegHidrPrioriPK regHidrPrioriPK;
    @Column(name = "IDDTAP")
    private String iddtap;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "REGION")
    private String region;
    @Column(name = "BIODIV")
    private String biodiv;
    @Column(name = "AMENAZA")
    private String amenaza;
    @Column(name = "USO")
    private String uso;
    @Column(name = "DESCONO")
    private String descono;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "regHidrPriori")
    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public RegHidrPriori() {
    }

    public RegHidrPriori(RegHidrPrioriPK regHidrPrioriPK) {
        this.regHidrPrioriPK = regHidrPrioriPK;
    }

    public RegHidrPriori(String numFolio, String cveProy, String cveArea, short version) {
        this.regHidrPrioriPK = new RegHidrPrioriPK(numFolio, cveProy, cveArea, version);
    }

    public RegHidrPrioriPK getRegHidrPrioriPK() {
        return regHidrPrioriPK;
    }

    public void setRegHidrPrioriPK(RegHidrPrioriPK regHidrPrioriPK) {
        this.regHidrPrioriPK = regHidrPrioriPK;
    }

    public String getIddtap() {
        return iddtap;
    }

    public void setIddtap(String iddtap) {
        this.iddtap = iddtap;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getBiodiv() {
        return biodiv;
    }

    public void setBiodiv(String biodiv) {
        this.biodiv = biodiv;
    }

    public String getAmenaza() {
        return amenaza;
    }

    public void setAmenaza(String amenaza) {
        this.amenaza = amenaza;
    }

    public String getUso() {
        return uso;
    }

    public void setUso(String uso) {
        this.uso = uso;
    }

    public String getDescono() {
        return descono;
    }

    public void setDescono(String descono) {
        this.descono = descono;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (regHidrPrioriPK != null ? regHidrPrioriPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegHidrPriori)) {
            return false;
        }
        RegHidrPriori other = (RegHidrPriori) object;
        if ((this.regHidrPrioriPK == null && other.regHidrPrioriPK != null) || (this.regHidrPrioriPK != null && !this.regHidrPrioriPK.equals(other.regHidrPrioriPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.RegHidrPriori[ regHidrPrioriPK=" + regHidrPrioriPK + " ]";
    }
    
}
