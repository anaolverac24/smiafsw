/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrador
 */
@Entity
@Table(name = "PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proyecto_1.findAll", query = "SELECT p FROM Proyecto_1 p"),
    @NamedQuery(name = "Proyecto_1.findByFolioProyecto", query = "SELECT p FROM Proyecto_1 p WHERE p.proyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "Proyecto_1.findBySerialProyecto", query = "SELECT p FROM Proyecto_1 p WHERE p.proyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "Proyecto_1.findByProySupuesto", query = "SELECT p FROM Proyecto_1 p WHERE p.proySupuesto = :proySupuesto"),
    @NamedQuery(name = "Proyecto_1.findByTipoAsentamientoId", query = "SELECT p FROM Proyecto_1 p WHERE p.tipoAsentamientoId = :tipoAsentamientoId"),
    @NamedQuery(name = "Proyecto_1.findByCveTipoVial", query = "SELECT p FROM Proyecto_1 p WHERE p.cveTipoVial = :cveTipoVial"),
    @NamedQuery(name = "Proyecto_1.findByNtipo", query = "SELECT p FROM Proyecto_1 p WHERE p.ntipo = :ntipo"),
    @NamedQuery(name = "Proyecto_1.findByNrama", query = "SELECT p FROM Proyecto_1 p WHERE p.nrama = :nrama"),
    @NamedQuery(name = "Proyecto_1.findByProyNombre", query = "SELECT p FROM Proyecto_1 p WHERE p.proyNombre = :proyNombre"),
    @NamedQuery(name = "Proyecto_1.findByProyNombreVialidad", query = "SELECT p FROM Proyecto_1 p WHERE p.proyNombreVialidad = :proyNombreVialidad"),
    @NamedQuery(name = "Proyecto_1.findByProyNumeroExterior", query = "SELECT p FROM Proyecto_1 p WHERE p.proyNumeroExterior = :proyNumeroExterior"),
    @NamedQuery(name = "Proyecto_1.findByProyNumeroInterior", query = "SELECT p FROM Proyecto_1 p WHERE p.proyNumeroInterior = :proyNumeroInterior"),
    @NamedQuery(name = "Proyecto_1.findByProyNombreAsentamiento", query = "SELECT p FROM Proyecto_1 p WHERE p.proyNombreAsentamiento = :proyNombreAsentamiento"),
    @NamedQuery(name = "Proyecto_1.findByProyCodigoPostal", query = "SELECT p FROM Proyecto_1 p WHERE p.proyCodigoPostal = :proyCodigoPostal"),
    @NamedQuery(name = "Proyecto_1.findByProyLocalidad", query = "SELECT p FROM Proyecto_1 p WHERE p.proyLocalidad = :proyLocalidad"),
    @NamedQuery(name = "Proyecto_1.findByProyInversionRequerida", query = "SELECT p FROM Proyecto_1 p WHERE p.proyInversionRequerida = :proyInversionRequerida"),
    @NamedQuery(name = "Proyecto_1.findByProyMedidasPrevencion", query = "SELECT p FROM Proyecto_1 p WHERE p.proyMedidasPrevencion = :proyMedidasPrevencion"),
    @NamedQuery(name = "Proyecto_1.findByProyEmpleosPermanentes", query = "SELECT p FROM Proyecto_1 p WHERE p.proyEmpleosPermanentes = :proyEmpleosPermanentes"),
    @NamedQuery(name = "Proyecto_1.findByProyEmpleosTemporales", query = "SELECT p FROM Proyecto_1 p WHERE p.proyEmpleosTemporales = :proyEmpleosTemporales"),
    @NamedQuery(name = "Proyecto_1.findByProyTiempoVidaAnios", query = "SELECT p FROM Proyecto_1 p WHERE p.proyTiempoVidaAnios = :proyTiempoVidaAnios"),
    @NamedQuery(name = "Proyecto_1.findByProyTiempoVidaMeses", query = "SELECT p FROM Proyecto_1 p WHERE p.proyTiempoVidaMeses = :proyTiempoVidaMeses"),
    @NamedQuery(name = "Proyecto_1.findByProyTiempoVidaSemanas", query = "SELECT p FROM Proyecto_1 p WHERE p.proyTiempoVidaSemanas = :proyTiempoVidaSemanas"),
    @NamedQuery(name = "Proyecto_1.findByProyDiagramaGantt", query = "SELECT p FROM Proyecto_1 p WHERE p.proyDiagramaGantt = :proyDiagramaGantt"),
    @NamedQuery(name = "Proyecto_1.findByProyRespTecIgualLegal", query = "SELECT p FROM Proyecto_1 p WHERE p.proyRespTecIgualLegal = :proyRespTecIgualLegal"),
    @NamedQuery(name = "Proyecto_1.findByRespTecId", query = "SELECT p FROM Proyecto_1 p WHERE p.respTecId = :respTecId"),
    @NamedQuery(name = "Proyecto_1.findByEstudioRiesgoId", query = "SELECT p FROM Proyecto_1 p WHERE p.estudioRiesgoId = :estudioRiesgoId"),
    @NamedQuery(name = "Proyecto_1.findByProyInversionFederalOri", query = "SELECT p FROM Proyecto_1 p WHERE p.proyInversionFederalOri = :proyInversionFederalOri"),
    @NamedQuery(name = "Proyecto_1.findByProyInversionEstatalOri", query = "SELECT p FROM Proyecto_1 p WHERE p.proyInversionEstatalOri = :proyInversionEstatalOri"),
    @NamedQuery(name = "Proyecto_1.findByProyInversionMunicipalOri", query = "SELECT p FROM Proyecto_1 p WHERE p.proyInversionMunicipalOri = :proyInversionMunicipalOri"),
    @NamedQuery(name = "Proyecto_1.findByProyInversionPrivadaOri", query = "SELECT p FROM Proyecto_1 p WHERE p.proyInversionPrivadaOri = :proyInversionPrivadaOri"),
    @NamedQuery(name = "Proyecto_1.findByProyRepresentanteId", query = "SELECT p FROM Proyecto_1 p WHERE p.proyRepresentanteId = :proyRepresentanteId"),
    @NamedQuery(name = "Proyecto_1.findByProyDescNat", query = "SELECT p FROM Proyecto_1 p WHERE p.proyDescNat = :proyDescNat"),
    @NamedQuery(name = "Proyecto_1.findByProyDemandaServ", query = "SELECT p FROM Proyecto_1 p WHERE p.proyDemandaServ = :proyDemandaServ"),
    @NamedQuery(name = "Proyecto_1.findByProyDescPaisaje", query = "SELECT p FROM Proyecto_1 p WHERE p.proyDescPaisaje = :proyDescPaisaje"),
    @NamedQuery(name = "Proyecto_1.findByProyDiagAmbiental", query = "SELECT p FROM Proyecto_1 p WHERE p.proyDiagAmbiental = :proyDiagAmbiental"),
    @NamedQuery(name = "Proyecto_1.findByProyJustMetodologia", query = "SELECT p FROM Proyecto_1 p WHERE p.proyJustMetodologia = :proyJustMetodologia"),
    @NamedQuery(name = "Proyecto_1.findByProyRequiMontos", query = "SELECT p FROM Proyecto_1 p WHERE p.proyRequiMontos = :proyRequiMontos"),
    @NamedQuery(name = "Proyecto_1.findByProyEsceSinP", query = "SELECT p FROM Proyecto_1 p WHERE p.proyEsceSinP = :proyEsceSinP"),
    @NamedQuery(name = "Proyecto_1.findByProyEsceConP", query = "SELECT p FROM Proyecto_1 p WHERE p.proyEsceConP = :proyEsceConP"),
    @NamedQuery(name = "Proyecto_1.findByProyEsceConPymed", query = "SELECT p FROM Proyecto_1 p WHERE p.proyEsceConPymed = :proyEsceConPymed"),
    @NamedQuery(name = "Proyecto_1.findByProyDescEvaluAlter", query = "SELECT p FROM Proyecto_1 p WHERE p.proyDescEvaluAlter = :proyDescEvaluAlter"),
    @NamedQuery(name = "Proyecto_1.findByClaveProyecto", query = "SELECT p FROM Proyecto_1 p WHERE p.claveProyecto = :claveProyecto"),
    @NamedQuery(name = "Proyecto_1.findByBitacoraProyecto", query = "SELECT p FROM Proyecto_1 p WHERE p.bitacoraProyecto = :bitacoraProyecto"),
    @NamedQuery(name = "Proyecto_1.findByNsub", query = "SELECT p FROM Proyecto_1 p WHERE p.nsub = :nsub"),
    @NamedQuery(name = "Proyecto_1.findByNsec", query = "SELECT p FROM Proyecto_1 p WHERE p.nsec = :nsec"),
    @NamedQuery(name = "Proyecto_1.findByEstatusProyecto", query = "SELECT p FROM Proyecto_1 p WHERE p.estatusProyecto = :estatusProyecto"),
    @NamedQuery(name = "Proyecto_1.findByProyDomEstablecido", query = "SELECT p FROM Proyecto_1 p WHERE p.proyDomEstablecido = :proyDomEstablecido"),
    @NamedQuery(name = "Proyecto_1.findByProyNormaId", query = "SELECT p FROM Proyecto_1 p WHERE p.proyNormaId = :proyNormaId"),
    @NamedQuery(name = "Proyecto_1.findByProySupPoetPdu", query = "SELECT p FROM Proyecto_1 p WHERE p.proySupPoetPdu = :proySupPoetPdu"),
    @NamedQuery(name = "Proyecto_1.findByProyEntAfectado", query = "SELECT p FROM Proyecto_1 p WHERE p.proyEntAfectado = :proyEntAfectado"),
    @NamedQuery(name = "Proyecto_1.findByProyMunAfectado", query = "SELECT p FROM Proyecto_1 p WHERE p.proyMunAfectado = :proyMunAfectado"),
    @NamedQuery(name = "Proyecto_1.findByProyLote", query = "SELECT p FROM Proyecto_1 p WHERE p.proyLote = :proyLote"),
    @NamedQuery(name = "Proyecto_1.findByProyValorCriterio", query = "SELECT p FROM Proyecto_1 p WHERE p.proyValorCriterio = :proyValorCriterio"),
    @NamedQuery(name = "Proyecto_1.findByProyCriterioMonto", query = "SELECT p FROM Proyecto_1 p WHERE p.proyCriterioMonto = :proyCriterioMonto"),
    @NamedQuery(name = "Proyecto_1.findByProyServreq", query = "SELECT p FROM Proyecto_1 p WHERE p.proyServreq = :proyServreq"),
    @NamedQuery(name = "Proyecto_1.findByProyIndProcyoperaciones", query = "SELECT p FROM Proyecto_1 p WHERE p.proyIndProcyoperaciones = :proyIndProcyoperaciones"),
    @NamedQuery(name = "Proyecto_1.findByProyIndActivitipoind", query = "SELECT p FROM Proyecto_1 p WHERE p.proyIndActivitipoind = :proyIndActivitipoind"),
    @NamedQuery(name = "Proyecto_1.findByProyIndCapacidadDisenio", query = "SELECT p FROM Proyecto_1 p WHERE p.proyIndCapacidadDisenio = :proyIndCapacidadDisenio"),
    @NamedQuery(name = "Proyecto_1.findByProyIndDiagramaproc", query = "SELECT p FROM Proyecto_1 p WHERE p.proyIndDiagramaproc = :proyIndDiagramaproc"),
    @NamedQuery(name = "Proyecto_1.findByProyIndProcContaminantes", query = "SELECT p FROM Proyecto_1 p WHERE p.proyIndProcContaminantes = :proyIndProcContaminantes"),
    @NamedQuery(name = "Proyecto_1.findByProyIndProcSistemasagua", query = "SELECT p FROM Proyecto_1 p WHERE p.proyIndProcSistemasagua = :proyIndProcSistemasagua"),
    @NamedQuery(name = "Proyecto_1.findByProyIndProcSistemasconge", query = "SELECT p FROM Proyecto_1 p WHERE p.proyIndProcSistemasconge = :proyIndProcSistemasconge"),
    @NamedQuery(name = "Proyecto_1.findByDescDelimSistemAmb", query = "SELECT p FROM Proyecto_1 p WHERE p.descDelimSistemAmb = :descDelimSistemAmb"),
    @NamedQuery(name = "Proyecto_1.findByDescDelimAreaInflu", query = "SELECT p FROM Proyecto_1 p WHERE p.descDelimAreaInflu = :descDelimAreaInflu"),
    @NamedQuery(name = "Proyecto_1.findByDescDelimSitProy", query = "SELECT p FROM Proyecto_1 p WHERE p.descDelimSitProy = :descDelimSitProy"),
    @NamedQuery(name = "Proyecto_1.findByJustProy", query = "SELECT p FROM Proyecto_1 p WHERE p.justProy = :justProy"),
    @NamedQuery(name = "Proyecto_1.findByDescMetutil", query = "SELECT p FROM Proyecto_1 p WHERE p.descMetutil = :descMetutil"),
    @NamedQuery(name = "Proyecto_1.findByFinyoSegReq", query = "SELECT p FROM Proyecto_1 p WHERE p.finyoSegReq = :finyoSegReq"),
    @NamedQuery(name = "Proyecto_1.findByConclusionProy", query = "SELECT p FROM Proyecto_1 p WHERE p.conclusionProy = :conclusionProy"),
    @NamedQuery(name = "Proyecto_1.findByMedioSocioproy", query = "SELECT p FROM Proyecto_1 p WHERE p.medioSocioproy = :medioSocioproy"),
    @NamedQuery(name = "Proyecto_1.findByDescDiagamb", query = "SELECT p FROM Proyecto_1 p WHERE p.descDiagamb = :descDiagamb"),
    @NamedQuery(name = "Proyecto_1.findByBibliografiaFauna", query = "SELECT p FROM Proyecto_1 p WHERE p.bibliografiaFauna = :bibliografiaFauna"),
    @NamedQuery(name = "Proyecto_1.findByBibliografiaFlora", query = "SELECT p FROM Proyecto_1 p WHERE p.bibliografiaFlora = :bibliografiaFlora"),
    @NamedQuery(name = "Proyecto_1.findByProyJustmetUtil", query = "SELECT p FROM Proyecto_1 p WHERE p.proyJustmetUtil = :proyJustmetUtil")})
public class Proyecto_1 implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProyectoPK proyectoPK;
    @Column(name = "PROY_SUPUESTO")
    private Character proySupuesto;
    @Column(name = "TIPO_ASENTAMIENTO_ID")
    private Short tipoAsentamientoId;
    @Column(name = "CVE_TIPO_VIAL")
    private Short cveTipoVial;
    @Column(name = "NTIPO")
    private Short ntipo;
    @Column(name = "NRAMA")
    private Short nrama;
    @Column(name = "PROY_NOMBRE")
    private String proyNombre;
    @Column(name = "PROY_NOMBRE_VIALIDAD")
    private String proyNombreVialidad;
    @Column(name = "PROY_NUMERO_EXTERIOR")
    private Integer proyNumeroExterior;
    @Column(name = "PROY_NUMERO_INTERIOR")
    private Integer proyNumeroInterior;
    @Column(name = "PROY_NOMBRE_ASENTAMIENTO")
    private String proyNombreAsentamiento;
    @Column(name = "PROY_CODIGO_POSTAL")
    private Integer proyCodigoPostal;
    @Column(name = "PROY_LOCALIDAD")
    private String proyLocalidad;
    @Lob
    @Column(name = "PROY_UBICACION_DESCRITA")
    private String proyUbicacionDescrita;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PROY_INVERSION_REQUERIDA")
    private BigDecimal proyInversionRequerida;
    @Column(name = "PROY_MEDIDAS_PREVENCION")
    private BigDecimal proyMedidasPrevencion;
    @Column(name = "PROY_EMPLEOS_PERMANENTES")
    private Integer proyEmpleosPermanentes;
    @Column(name = "PROY_EMPLEOS_TEMPORALES")
    private Integer proyEmpleosTemporales;
    @Column(name = "PROY_TIEMPO_VIDA_ANIOS")
    private Short proyTiempoVidaAnios;
    @Column(name = "PROY_TIEMPO_VIDA_MESES")
    private Short proyTiempoVidaMeses;
    @Column(name = "PROY_TIEMPO_VIDA_SEMANAS")
    private Short proyTiempoVidaSemanas;
    @Lob
    @Column(name = "PROY_DESC_MEDIDAS_COMPEN")
    private String proyDescMedidasCompen;
    @Lob
    @Column(name = "PROY_DESC_PROTEC_CONSERV")
    private String proyDescProtecConserv;
    @Lob
    @Column(name = "PROY_DESC_PARTICULAR")
    private String proyDescParticular;
    @Lob
    @Column(name = "PROY_DESC_AMBIENTE")
    private String proyDescAmbiente;
    @Column(name = "PROY_DIAGRAMA_GANTT")
    private String proyDiagramaGantt;
    @Column(name = "PROY_RESP_TEC_IGUAL_LEGAL")
    private Character proyRespTecIgualLegal;
    @Column(name = "RESP_TEC_ID")
    private Short respTecId;
    @Column(name = "ESTUDIO_RIESGO_ID")
    private Short estudioRiesgoId;
    @Column(name = "PROY_INVERSION_FEDERAL_ORI")
    private BigDecimal proyInversionFederalOri;
    @Column(name = "PROY_INVERSION_ESTATAL_ORI")
    private BigDecimal proyInversionEstatalOri;
    @Column(name = "PROY_INVERSION_MUNICIPAL_ORI")
    private BigDecimal proyInversionMunicipalOri;
    @Column(name = "PROY_INVERSION_PRIVADA_ORI")
    private BigDecimal proyInversionPrivadaOri;
    @Column(name = "PROY_REPRESENTANTE_ID")
    private BigInteger proyRepresentanteId;
    @Column(name = "PROY_DESC_NAT")
    private String proyDescNat;
    @Column(name = "PROY_DEMANDA_SERV")
    private Character proyDemandaServ;
    @Column(name = "PROY_DESC_PAISAJE")
    private String proyDescPaisaje;
    @Column(name = "PROY_DIAG_AMBIENTAL")
    private String proyDiagAmbiental;
    @Column(name = "PROY_JUST_METODOLOGIA")
    private String proyJustMetodologia;
    @Column(name = "PROY_REQUI_MONTOS")
    private Character proyRequiMontos;
    @Column(name = "PROY_ESCE_SIN_P")
    private String proyEsceSinP;
    @Column(name = "PROY_ESCE_CON_P")
    private String proyEsceConP;
    @Column(name = "PROY_ESCE_CON_PYMED")
    private String proyEsceConPymed;
    @Column(name = "PROY_DESC_EVALU_ALTER")
    private String proyDescEvaluAlter;
    @Column(name = "CLAVE_PROYECTO")
    private String claveProyecto;
    @Column(name = "BITACORA_PROYECTO")
    private String bitacoraProyecto;
    @Column(name = "NSUB")
    private Short nsub;
    @Column(name = "NSEC")
    private Short nsec;
    @Column(name = "ESTATUS_PROYECTO")
    private String estatusProyecto;
    @Column(name = "PROY_DOM_ESTABLECIDO")
    private Character proyDomEstablecido;
    @Column(name = "PROY_NORMA_ID")
    private Short proyNormaId;
    @Column(name = "PROY_SUP_POET_PDU")
    private Character proySupPoetPdu;
    @Column(name = "PROY_ENT_AFECTADO")
    private String proyEntAfectado;
    @Column(name = "PROY_MUN_AFECTADO")
    private String proyMunAfectado;
    @Column(name = "PROY_LOTE")
    private Long proyLote;
    @Column(name = "PROY_VALOR_CRITERIO")
    private Short proyValorCriterio;
    @Column(name = "PROY_CRITERIO_MONTO")
    private BigDecimal proyCriterioMonto;
    @Column(name = "PROY_SERVREQ")
    private Character proyServreq;
    @Column(name = "PROY_IND_PROCYOPERACIONES")
    private String proyIndProcyoperaciones;
    @Column(name = "PROY_IND_ACTIVITIPOIND")
    private String proyIndActivitipoind;
    @Column(name = "PROY_IND_CAPACIDAD_DISENIO")
    private String proyIndCapacidadDisenio;
    @Column(name = "PROY_IND_DIAGRAMAPROC")
    private String proyIndDiagramaproc;
    @Column(name = "PROY_IND_PROC_CONTAMINANTES")
    private String proyIndProcContaminantes;
    @Column(name = "PROY_IND_PROC_SISTEMASAGUA")
    private String proyIndProcSistemasagua;
    @Column(name = "PROY_IND_PROC_SISTEMASCONGE")
    private String proyIndProcSistemasconge;
    @Column(name = "DESC_DELIM_SISTEM_AMB")
    private String descDelimSistemAmb;
    @Column(name = "DESC_DELIM_AREA_INFLU")
    private String descDelimAreaInflu;
    @Column(name = "DESC_DELIM_SIT_PROY")
    private String descDelimSitProy;
    @Column(name = "JUST_PROY")
    private String justProy;
    @Column(name = "DESC_METUTIL")
    private String descMetutil;
    @Column(name = "FINYO_SEG_REQ")
    private Character finyoSegReq;
    @Lob
    @Column(name = "ESCE_SIN_PROY")
    private String esceSinProy;
    @Lob
    @Column(name = "ESCE_CON_PROY")
    private String esceConProy;
    @Lob
    @Column(name = "ESCE_CON_PROY_MED")
    private String esceConProyMed;
    @Lob
    @Column(name = "PRONOS_AMB")
    private String pronosAmb;
    @Column(name = "CONCLUSION_PROY")
    private String conclusionProy;
    @Lob
    @Column(name = "EVA_ALTERNATIVAS")
    private String evaAlternativas;
    @Lob
    @Column(name = "BIBLIO_PROY")
    private String biblioProy;
    @Lob
    @Column(name = "MANIFESTACION_RES")
    private String manifestacionRes;
    @Column(name = "MEDIO_SOCIOPROY")
    private String medioSocioproy;
    @Lob
    @Column(name = "DESC_SUELO")
    private String descSuelo;
    @Lob
    @Column(name = "DESC_PAISAJE_PROY")
    private String descPaisajeProy;
    @Lob
    @Column(name = "DESC_GEOS")
    private String descGeos;
    @Column(name = "DESC_DIAGAMB")
    private String descDiagamb;
    @Lob
    @Column(name = "ANALISISBIO_VEG")
    private String analisisbioVeg;
    @Column(name = "BIBLIOGRAFIA_FAUNA")
    private String bibliografiaFauna;
    @Column(name = "BIBLIOGRAFIA_FLORA")
    private String bibliografiaFlora;
    @Column(name = "PROY_JUSTMET_UTIL")
    private String proyJustmetUtil;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto")
    private Collection<InfobioProyecto> infobioProyectoCollection;

    public Proyecto_1() {
    }

    public Proyecto_1(ProyectoPK proyectoPK) {
        this.proyectoPK = proyectoPK;
    }

    public Proyecto_1(String folioProyecto, short serialProyecto) {
        this.proyectoPK = new ProyectoPK(folioProyecto, serialProyecto);
    }

    public ProyectoPK getProyectoPK() {
        return proyectoPK;
    }

    public void setProyectoPK(ProyectoPK proyectoPK) {
        this.proyectoPK = proyectoPK;
    }

    public Character getProySupuesto() {
        return proySupuesto;
    }

    public void setProySupuesto(Character proySupuesto) {
        this.proySupuesto = proySupuesto;
    }

    public Short getTipoAsentamientoId() {
        return tipoAsentamientoId;
    }

    public void setTipoAsentamientoId(Short tipoAsentamientoId) {
        this.tipoAsentamientoId = tipoAsentamientoId;
    }

    public Short getCveTipoVial() {
        return cveTipoVial;
    }

    public void setCveTipoVial(Short cveTipoVial) {
        this.cveTipoVial = cveTipoVial;
    }

    public Short getNtipo() {
        return ntipo;
    }

    public void setNtipo(Short ntipo) {
        this.ntipo = ntipo;
    }

    public Short getNrama() {
        return nrama;
    }

    public void setNrama(Short nrama) {
        this.nrama = nrama;
    }

    public String getProyNombre() {
        return proyNombre;
    }

    public void setProyNombre(String proyNombre) {
        this.proyNombre = proyNombre;
    }

    public String getProyNombreVialidad() {
        return proyNombreVialidad;
    }

    public void setProyNombreVialidad(String proyNombreVialidad) {
        this.proyNombreVialidad = proyNombreVialidad;
    }

    public Integer getProyNumeroExterior() {
        return proyNumeroExterior;
    }

    public void setProyNumeroExterior(Integer proyNumeroExterior) {
        this.proyNumeroExterior = proyNumeroExterior;
    }

    public Integer getProyNumeroInterior() {
        return proyNumeroInterior;
    }

    public void setProyNumeroInterior(Integer proyNumeroInterior) {
        this.proyNumeroInterior = proyNumeroInterior;
    }

    public String getProyNombreAsentamiento() {
        return proyNombreAsentamiento;
    }

    public void setProyNombreAsentamiento(String proyNombreAsentamiento) {
        this.proyNombreAsentamiento = proyNombreAsentamiento;
    }

    public Integer getProyCodigoPostal() {
        return proyCodigoPostal;
    }

    public void setProyCodigoPostal(Integer proyCodigoPostal) {
        this.proyCodigoPostal = proyCodigoPostal;
    }

    public String getProyLocalidad() {
        return proyLocalidad;
    }

    public void setProyLocalidad(String proyLocalidad) {
        this.proyLocalidad = proyLocalidad;
    }

    public String getProyUbicacionDescrita() {
        return proyUbicacionDescrita;
    }

    public void setProyUbicacionDescrita(String proyUbicacionDescrita) {
        this.proyUbicacionDescrita = proyUbicacionDescrita;
    }

    public BigDecimal getProyInversionRequerida() {
        return proyInversionRequerida;
    }

    public void setProyInversionRequerida(BigDecimal proyInversionRequerida) {
        this.proyInversionRequerida = proyInversionRequerida;
    }

    public BigDecimal getProyMedidasPrevencion() {
        return proyMedidasPrevencion;
    }

    public void setProyMedidasPrevencion(BigDecimal proyMedidasPrevencion) {
        this.proyMedidasPrevencion = proyMedidasPrevencion;
    }

    public Integer getProyEmpleosPermanentes() {
        return proyEmpleosPermanentes;
    }

    public void setProyEmpleosPermanentes(Integer proyEmpleosPermanentes) {
        this.proyEmpleosPermanentes = proyEmpleosPermanentes;
    }

    public Integer getProyEmpleosTemporales() {
        return proyEmpleosTemporales;
    }

    public void setProyEmpleosTemporales(Integer proyEmpleosTemporales) {
        this.proyEmpleosTemporales = proyEmpleosTemporales;
    }

    public Short getProyTiempoVidaAnios() {
        return proyTiempoVidaAnios;
    }

    public void setProyTiempoVidaAnios(Short proyTiempoVidaAnios) {
        this.proyTiempoVidaAnios = proyTiempoVidaAnios;
    }

    public Short getProyTiempoVidaMeses() {
        return proyTiempoVidaMeses;
    }

    public void setProyTiempoVidaMeses(Short proyTiempoVidaMeses) {
        this.proyTiempoVidaMeses = proyTiempoVidaMeses;
    }

    public Short getProyTiempoVidaSemanas() {
        return proyTiempoVidaSemanas;
    }

    public void setProyTiempoVidaSemanas(Short proyTiempoVidaSemanas) {
        this.proyTiempoVidaSemanas = proyTiempoVidaSemanas;
    }

    public String getProyDescMedidasCompen() {
        return proyDescMedidasCompen;
    }

    public void setProyDescMedidasCompen(String proyDescMedidasCompen) {
        this.proyDescMedidasCompen = proyDescMedidasCompen;
    }

    public String getProyDescProtecConserv() {
        return proyDescProtecConserv;
    }

    public void setProyDescProtecConserv(String proyDescProtecConserv) {
        this.proyDescProtecConserv = proyDescProtecConserv;
    }

    public String getProyDescParticular() {
        return proyDescParticular;
    }

    public void setProyDescParticular(String proyDescParticular) {
        this.proyDescParticular = proyDescParticular;
    }

    public String getProyDescAmbiente() {
        return proyDescAmbiente;
    }

    public void setProyDescAmbiente(String proyDescAmbiente) {
        this.proyDescAmbiente = proyDescAmbiente;
    }

    public String getProyDiagramaGantt() {
        return proyDiagramaGantt;
    }

    public void setProyDiagramaGantt(String proyDiagramaGantt) {
        this.proyDiagramaGantt = proyDiagramaGantt;
    }

    public Character getProyRespTecIgualLegal() {
        return proyRespTecIgualLegal;
    }

    public void setProyRespTecIgualLegal(Character proyRespTecIgualLegal) {
        this.proyRespTecIgualLegal = proyRespTecIgualLegal;
    }

    public Short getRespTecId() {
        return respTecId;
    }

    public void setRespTecId(Short respTecId) {
        this.respTecId = respTecId;
    }

    public Short getEstudioRiesgoId() {
        return estudioRiesgoId;
    }

    public void setEstudioRiesgoId(Short estudioRiesgoId) {
        this.estudioRiesgoId = estudioRiesgoId;
    }

    public BigDecimal getProyInversionFederalOri() {
        return proyInversionFederalOri;
    }

    public void setProyInversionFederalOri(BigDecimal proyInversionFederalOri) {
        this.proyInversionFederalOri = proyInversionFederalOri;
    }

    public BigDecimal getProyInversionEstatalOri() {
        return proyInversionEstatalOri;
    }

    public void setProyInversionEstatalOri(BigDecimal proyInversionEstatalOri) {
        this.proyInversionEstatalOri = proyInversionEstatalOri;
    }

    public BigDecimal getProyInversionMunicipalOri() {
        return proyInversionMunicipalOri;
    }

    public void setProyInversionMunicipalOri(BigDecimal proyInversionMunicipalOri) {
        this.proyInversionMunicipalOri = proyInversionMunicipalOri;
    }

    public BigDecimal getProyInversionPrivadaOri() {
        return proyInversionPrivadaOri;
    }

    public void setProyInversionPrivadaOri(BigDecimal proyInversionPrivadaOri) {
        this.proyInversionPrivadaOri = proyInversionPrivadaOri;
    }

    public BigInteger getProyRepresentanteId() {
        return proyRepresentanteId;
    }

    public void setProyRepresentanteId(BigInteger proyRepresentanteId) {
        this.proyRepresentanteId = proyRepresentanteId;
    }

    public String getProyDescNat() {
        return proyDescNat;
    }

    public void setProyDescNat(String proyDescNat) {
        this.proyDescNat = proyDescNat;
    }

    public Character getProyDemandaServ() {
        return proyDemandaServ;
    }

    public void setProyDemandaServ(Character proyDemandaServ) {
        this.proyDemandaServ = proyDemandaServ;
    }

    public String getProyDescPaisaje() {
        return proyDescPaisaje;
    }

    public void setProyDescPaisaje(String proyDescPaisaje) {
        this.proyDescPaisaje = proyDescPaisaje;
    }

    public String getProyDiagAmbiental() {
        return proyDiagAmbiental;
    }

    public void setProyDiagAmbiental(String proyDiagAmbiental) {
        this.proyDiagAmbiental = proyDiagAmbiental;
    }

    public String getProyJustMetodologia() {
        return proyJustMetodologia;
    }

    public void setProyJustMetodologia(String proyJustMetodologia) {
        this.proyJustMetodologia = proyJustMetodologia;
    }

    public Character getProyRequiMontos() {
        return proyRequiMontos;
    }

    public void setProyRequiMontos(Character proyRequiMontos) {
        this.proyRequiMontos = proyRequiMontos;
    }

    public String getProyEsceSinP() {
        return proyEsceSinP;
    }

    public void setProyEsceSinP(String proyEsceSinP) {
        this.proyEsceSinP = proyEsceSinP;
    }

    public String getProyEsceConP() {
        return proyEsceConP;
    }

    public void setProyEsceConP(String proyEsceConP) {
        this.proyEsceConP = proyEsceConP;
    }

    public String getProyEsceConPymed() {
        return proyEsceConPymed;
    }

    public void setProyEsceConPymed(String proyEsceConPymed) {
        this.proyEsceConPymed = proyEsceConPymed;
    }

    public String getProyDescEvaluAlter() {
        return proyDescEvaluAlter;
    }

    public void setProyDescEvaluAlter(String proyDescEvaluAlter) {
        this.proyDescEvaluAlter = proyDescEvaluAlter;
    }

    public String getClaveProyecto() {
        return claveProyecto;
    }

    public void setClaveProyecto(String claveProyecto) {
        this.claveProyecto = claveProyecto;
    }

    public String getBitacoraProyecto() {
        return bitacoraProyecto;
    }

    public void setBitacoraProyecto(String bitacoraProyecto) {
        this.bitacoraProyecto = bitacoraProyecto;
    }

    public Short getNsub() {
        return nsub;
    }

    public void setNsub(Short nsub) {
        this.nsub = nsub;
    }

    public Short getNsec() {
        return nsec;
    }

    public void setNsec(Short nsec) {
        this.nsec = nsec;
    }

    public String getEstatusProyecto() {
        return estatusProyecto;
    }

    public void setEstatusProyecto(String estatusProyecto) {
        this.estatusProyecto = estatusProyecto;
    }

    public Character getProyDomEstablecido() {
        return proyDomEstablecido;
    }

    public void setProyDomEstablecido(Character proyDomEstablecido) {
        this.proyDomEstablecido = proyDomEstablecido;
    }

    public Short getProyNormaId() {
        return proyNormaId;
    }

    public void setProyNormaId(Short proyNormaId) {
        this.proyNormaId = proyNormaId;
    }

    public Character getProySupPoetPdu() {
        return proySupPoetPdu;
    }

    public void setProySupPoetPdu(Character proySupPoetPdu) {
        this.proySupPoetPdu = proySupPoetPdu;
    }

    public String getProyEntAfectado() {
        return proyEntAfectado;
    }

    public void setProyEntAfectado(String proyEntAfectado) {
        this.proyEntAfectado = proyEntAfectado;
    }

    public String getProyMunAfectado() {
        return proyMunAfectado;
    }

    public void setProyMunAfectado(String proyMunAfectado) {
        this.proyMunAfectado = proyMunAfectado;
    }

    public Long getProyLote() {
        return proyLote;
    }

    public void setProyLote(Long proyLote) {
        this.proyLote = proyLote;
    }

    public Short getProyValorCriterio() {
        return proyValorCriterio;
    }

    public void setProyValorCriterio(Short proyValorCriterio) {
        this.proyValorCriterio = proyValorCriterio;
    }

    public BigDecimal getProyCriterioMonto() {
        return proyCriterioMonto;
    }

    public void setProyCriterioMonto(BigDecimal proyCriterioMonto) {
        this.proyCriterioMonto = proyCriterioMonto;
    }

    public Character getProyServreq() {
        return proyServreq;
    }

    public void setProyServreq(Character proyServreq) {
        this.proyServreq = proyServreq;
    }

    public String getProyIndProcyoperaciones() {
        return proyIndProcyoperaciones;
    }

    public void setProyIndProcyoperaciones(String proyIndProcyoperaciones) {
        this.proyIndProcyoperaciones = proyIndProcyoperaciones;
    }

    public String getProyIndActivitipoind() {
        return proyIndActivitipoind;
    }

    public void setProyIndActivitipoind(String proyIndActivitipoind) {
        this.proyIndActivitipoind = proyIndActivitipoind;
    }

    public String getProyIndCapacidadDisenio() {
        return proyIndCapacidadDisenio;
    }

    public void setProyIndCapacidadDisenio(String proyIndCapacidadDisenio) {
        this.proyIndCapacidadDisenio = proyIndCapacidadDisenio;
    }

    public String getProyIndDiagramaproc() {
        return proyIndDiagramaproc;
    }

    public void setProyIndDiagramaproc(String proyIndDiagramaproc) {
        this.proyIndDiagramaproc = proyIndDiagramaproc;
    }

    public String getProyIndProcContaminantes() {
        return proyIndProcContaminantes;
    }

    public void setProyIndProcContaminantes(String proyIndProcContaminantes) {
        this.proyIndProcContaminantes = proyIndProcContaminantes;
    }

    public String getProyIndProcSistemasagua() {
        return proyIndProcSistemasagua;
    }

    public void setProyIndProcSistemasagua(String proyIndProcSistemasagua) {
        this.proyIndProcSistemasagua = proyIndProcSistemasagua;
    }

    public String getProyIndProcSistemasconge() {
        return proyIndProcSistemasconge;
    }

    public void setProyIndProcSistemasconge(String proyIndProcSistemasconge) {
        this.proyIndProcSistemasconge = proyIndProcSistemasconge;
    }

    public String getDescDelimSistemAmb() {
        return descDelimSistemAmb;
    }

    public void setDescDelimSistemAmb(String descDelimSistemAmb) {
        this.descDelimSistemAmb = descDelimSistemAmb;
    }

    public String getDescDelimAreaInflu() {
        return descDelimAreaInflu;
    }

    public void setDescDelimAreaInflu(String descDelimAreaInflu) {
        this.descDelimAreaInflu = descDelimAreaInflu;
    }

    public String getDescDelimSitProy() {
        return descDelimSitProy;
    }

    public void setDescDelimSitProy(String descDelimSitProy) {
        this.descDelimSitProy = descDelimSitProy;
    }

    public String getJustProy() {
        return justProy;
    }

    public void setJustProy(String justProy) {
        this.justProy = justProy;
    }

    public String getDescMetutil() {
        return descMetutil;
    }

    public void setDescMetutil(String descMetutil) {
        this.descMetutil = descMetutil;
    }

    public Character getFinyoSegReq() {
        return finyoSegReq;
    }

    public void setFinyoSegReq(Character finyoSegReq) {
        this.finyoSegReq = finyoSegReq;
    }

    public String getEsceSinProy() {
        return esceSinProy;
    }

    public void setEsceSinProy(String esceSinProy) {
        this.esceSinProy = esceSinProy;
    }

    public String getEsceConProy() {
        return esceConProy;
    }

    public void setEsceConProy(String esceConProy) {
        this.esceConProy = esceConProy;
    }

    public String getEsceConProyMed() {
        return esceConProyMed;
    }

    public void setEsceConProyMed(String esceConProyMed) {
        this.esceConProyMed = esceConProyMed;
    }

    public String getPronosAmb() {
        return pronosAmb;
    }

    public void setPronosAmb(String pronosAmb) {
        this.pronosAmb = pronosAmb;
    }

    public String getConclusionProy() {
        return conclusionProy;
    }

    public void setConclusionProy(String conclusionProy) {
        this.conclusionProy = conclusionProy;
    }

    public String getEvaAlternativas() {
        return evaAlternativas;
    }

    public void setEvaAlternativas(String evaAlternativas) {
        this.evaAlternativas = evaAlternativas;
    }

    public String getBiblioProy() {
        return biblioProy;
    }

    public void setBiblioProy(String biblioProy) {
        this.biblioProy = biblioProy;
    }

    public String getManifestacionRes() {
        return manifestacionRes;
    }

    public void setManifestacionRes(String manifestacionRes) {
        this.manifestacionRes = manifestacionRes;
    }

    public String getMedioSocioproy() {
        return medioSocioproy;
    }

    public void setMedioSocioproy(String medioSocioproy) {
        this.medioSocioproy = medioSocioproy;
    }

    public String getDescSuelo() {
        return descSuelo;
    }

    public void setDescSuelo(String descSuelo) {
        this.descSuelo = descSuelo;
    }

    public String getDescPaisajeProy() {
        return descPaisajeProy;
    }

    public void setDescPaisajeProy(String descPaisajeProy) {
        this.descPaisajeProy = descPaisajeProy;
    }

    public String getDescGeos() {
        return descGeos;
    }

    public void setDescGeos(String descGeos) {
        this.descGeos = descGeos;
    }

    public String getDescDiagamb() {
        return descDiagamb;
    }

    public void setDescDiagamb(String descDiagamb) {
        this.descDiagamb = descDiagamb;
    }

    public String getAnalisisbioVeg() {
        return analisisbioVeg;
    }

    public void setAnalisisbioVeg(String analisisbioVeg) {
        this.analisisbioVeg = analisisbioVeg;
    }

    public String getBibliografiaFauna() {
        return bibliografiaFauna;
    }

    public void setBibliografiaFauna(String bibliografiaFauna) {
        this.bibliografiaFauna = bibliografiaFauna;
    }

    public String getBibliografiaFlora() {
        return bibliografiaFlora;
    }

    public void setBibliografiaFlora(String bibliografiaFlora) {
        this.bibliografiaFlora = bibliografiaFlora;
    }

    public String getProyJustmetUtil() {
        return proyJustmetUtil;
    }

    public void setProyJustmetUtil(String proyJustmetUtil) {
        this.proyJustmetUtil = proyJustmetUtil;
    }

    @XmlTransient
    public Collection<InfobioProyecto> getInfobioProyectoCollection() {
        return infobioProyectoCollection;
    }

    public void setInfobioProyectoCollection(Collection<InfobioProyecto> infobioProyectoCollection) {
        this.infobioProyectoCollection = infobioProyectoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proyectoPK != null ? proyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proyecto_1)) {
            return false;
        }
        Proyecto_1 other = (Proyecto_1) object;
        if ((this.proyectoPK == null && other.proyectoPK != null) || (this.proyectoPK != null && !this.proyectoPK.equals(other.proyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.Proyecto_1[ proyectoPK=" + proyectoPK + " ]";
    }
    
}
