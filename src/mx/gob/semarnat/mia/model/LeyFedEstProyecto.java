/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "LEY_FED_EST_PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LeyFedEstProyecto.findAll", query = "SELECT l FROM LeyFedEstProyecto l"),
    @NamedQuery(name = "LeyFedEstProyecto.findByFolioProyecto", query = "SELECT l FROM LeyFedEstProyecto l WHERE l.leyFedEstProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "LeyFedEstProyecto.findBySerialProyecto", query = "SELECT l FROM LeyFedEstProyecto l WHERE l.leyFedEstProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "LeyFedEstProyecto.findByLeyFeId", query = "SELECT l FROM LeyFedEstProyecto l WHERE l.leyFedEstProyectoPK.leyFeId = :leyFeId"),
    @NamedQuery(name = "LeyFedEstProyecto.findByLeyFeArticulo", query = "SELECT l FROM LeyFedEstProyecto l WHERE l.leyFeArticulo = :leyFeArticulo"),
    @NamedQuery(name = "LeyFedEstProyecto.findByLeyFeFraccion", query = "SELECT l FROM LeyFedEstProyecto l WHERE l.leyFeFraccion = :leyFeFraccion"),
    @NamedQuery(name = "LeyFedEstProyecto.findByLeyFeInciso", query = "SELECT l FROM LeyFedEstProyecto l WHERE l.leyFeInciso = :leyFeInciso"),
    @NamedQuery(name = "LeyFedEstProyecto.findByLeyFeDescrip", query = "SELECT l FROM LeyFedEstProyecto l WHERE l.leyFeDescrip = :leyFeDescrip"),
    @NamedQuery(name = "LeyFedEstProyecto.findByLeyFeBandera", query = "SELECT l FROM LeyFedEstProyecto l WHERE l.leyFeBandera = :leyFeBandera")})
public class LeyFedEstProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LeyFedEstProyectoPK leyFedEstProyectoPK;
    @Column(name = "LEY_FE_ARTICULO")
    private String leyFeArticulo;
    @Column(name = "LEY_FE_FRACCION")
    private String leyFeFraccion;
    @Column(name = "LEY_FE_INCISO")
    private String leyFeInciso;
    @Lob
    @Column(name = "LEY_FE_VINCULACION")
    private String leyFeVinculacion;
    @Column(name = "LEY_FE_DESCRIP")
    private String leyFeDescrip;
    @Column(name = "LEY_FE_BANDERA")
    private String leyFeBandera;
    @JoinColumn(name = "LEY_ID", referencedColumnName = "LEY_ID")
    @ManyToOne
    private CatLey leyId;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;
    
    @Column(name = "ID_LEY_SEQ")
    private Short idLeySeq;

    @Column(name = "FECHA_ULTIMA_ACTUALIZACION")    
    private String fechaUltimaActualizacion;
    
    @Column(name="ID_ARCHIVO_PROGRAMA")
    private Short idArchivoProyecto;  
    
    public LeyFedEstProyecto() {
    }

    public LeyFedEstProyecto(LeyFedEstProyectoPK leyFedEstProyectoPK) {
        this.leyFedEstProyectoPK = leyFedEstProyectoPK;
    }

    public LeyFedEstProyecto(String folioProyecto, short serialProyecto, short leyFeId) {
        this.leyFedEstProyectoPK = new LeyFedEstProyectoPK(folioProyecto, serialProyecto, leyFeId);
    }

    public LeyFedEstProyectoPK getLeyFedEstProyectoPK() {
        return leyFedEstProyectoPK;
    }

    public void setLeyFedEstProyectoPK(LeyFedEstProyectoPK leyFedEstProyectoPK) {
        this.leyFedEstProyectoPK = leyFedEstProyectoPK;
    }

    public String getLeyFeArticulo() {
        return leyFeArticulo;
    }

    public void setLeyFeArticulo(String leyFeArticulo) {
        this.leyFeArticulo = leyFeArticulo;
    }

    public String getLeyFeFraccion() {
        return leyFeFraccion;
    }

    public void setLeyFeFraccion(String leyFeFraccion) {
        this.leyFeFraccion = leyFeFraccion;
    }

    public String getLeyFeInciso() {
        return leyFeInciso;
    }

    public void setLeyFeInciso(String leyFeInciso) {
        this.leyFeInciso = leyFeInciso;
    }

    public String getLeyFeVinculacion() {
        return leyFeVinculacion;
    }
    
    public String getLeyFeVinculacionCorta(){
        if(leyFeVinculacion.length()<199){
            return leyFeVinculacion;
        }
        return leyFeVinculacion.substring(0, 199);
    }

    public void setLeyFeVinculacion(String leyFeVinculacion) {
        this.leyFeVinculacion = leyFeVinculacion;
    }

    public String getLeyFeDescrip() {
        return leyFeDescrip;
    }

    public void setLeyFeDescrip(String leyFeDescrip) {
        this.leyFeDescrip = leyFeDescrip;
    }

    public String getLeyFeBandera() {
        return leyFeBandera;
    }

    public void setLeyFeBandera(String leyFeBandera) {
        this.leyFeBandera = leyFeBandera;
    }

    public CatLey getLeyId() {
        return leyId;
    }

    public void setLeyId(CatLey leyId) {
        this.leyId = leyId;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (leyFedEstProyectoPK != null ? leyFedEstProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LeyFedEstProyecto)) {
            return false;
        }
        LeyFedEstProyecto other = (LeyFedEstProyecto) object;
        if ((this.leyFedEstProyectoPK == null && other.leyFedEstProyectoPK != null) || (this.leyFedEstProyectoPK != null && !this.leyFedEstProyectoPK.equals(other.leyFedEstProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.LeyFedEstProyecto[ leyFedEstProyectoPK=" + leyFedEstProyectoPK + " ]";
    }

	/**
	 * @return the idLeySeq
	 */
	public Short getIdLeySeq() {
		return idLeySeq;
	}

	/**
	 * @param idLeySeq the idLeySeq to set
	 */
	public void setIdLeySeq(Short idLeySeq) {
		this.idLeySeq = idLeySeq;
	}

	/**
	 * @return the fechaUltimaActualizacion
	 */
	public String getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	/**
	 * @param fechaUltimaActualizacion the nombreLey to set
	 */
	public void setFechaUltimaActualizacion(String fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	/**
	 * @return the idArchivoProyecto
	 */
	public Short getIdArchivoProyecto() {
		return idArchivoProyecto;
	}

	/**
	 * @param idArchivoProyecto the idArchivoProyecto to set
	 */
	public void setIdArchivoProyecto(Short idArchivoProyecto) {
		this.idArchivoProyecto = idArchivoProyecto;
	}
    
}
