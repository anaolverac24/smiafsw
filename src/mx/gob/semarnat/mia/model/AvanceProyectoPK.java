/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author mauricio
 */
@Embeddable
public class AvanceProyectoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "FOLIO_PROYECTO")
    private String folioProyecto;
    @Basic(optional = false)
    @Column(name = "SERIAL_PROYECO")
    private short serialProyeco;
    @Basic(optional = false)
    @Column(name = "CAPITULO")
    private short capitulo;

    public AvanceProyectoPK() {
    }

    public AvanceProyectoPK(String folioProyecto, short serialProyeco, short capitulo) {
        this.folioProyecto = folioProyecto;
        this.serialProyeco = serialProyeco;
        this.capitulo = capitulo;
    }

    public String getFolioProyecto() {
        return folioProyecto;
    }

    public void setFolioProyecto(String folioProyecto) {
        this.folioProyecto = folioProyecto;
    }

    public short getSerialProyeco() {
        return serialProyeco;
    }

    public void setSerialProyeco(short serialProyeco) {
        this.serialProyeco = serialProyeco;
    }

    public short getCapitulo() {
        return capitulo;
    }

    public void setCapitulo(short capitulo) {
        this.capitulo = capitulo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (folioProyecto != null ? folioProyecto.hashCode() : 0);
        hash += (int) serialProyeco;
        hash += (int) capitulo;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AvanceProyectoPK)) {
            return false;
        }
        AvanceProyectoPK other = (AvanceProyectoPK) object;
        if ((this.folioProyecto == null && other.folioProyecto != null) || (this.folioProyecto != null && !this.folioProyecto.equals(other.folioProyecto))) {
            return false;
        }
        if (this.serialProyeco != other.serialProyeco) {
            return false;
        }
        if (this.capitulo != other.capitulo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.AvanceProyectoPK[ folioProyecto=" + folioProyecto + ", serialProyeco=" + serialProyeco + ", capitulo=" + capitulo + " ]";
    }
    
}
