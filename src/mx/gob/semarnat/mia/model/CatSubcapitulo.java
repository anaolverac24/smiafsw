/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_SUBCAPITULO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatSubcapitulo.findAll", query = "SELECT c FROM CatSubcapitulo c")})
public class CatSubcapitulo implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CatSubcapituloPK catSubcapituloPK;
    @Column(name = "SUCBAPITULO_DESCRIPCION")
    private String sucbapituloDescripcion;
    @Column(name = "SUPCAPITULO_AYUDA")
    private String supcapituloAyuda;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catSubcapitulo")
    private List<CatSeccion> catSeccionList;
    @JoinColumns({
        @JoinColumn(name = "ID_TRAMITE", referencedColumnName = "ID_TRAMITE", insertable = false, updatable = false),
        @JoinColumn(name = "NSUB", referencedColumnName = "NSUB", insertable = false, updatable = false),
        @JoinColumn(name = "CAPITULO_ID", referencedColumnName = "CAPITULO_ID", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private CatCapitulo catCapitulo;

    public CatSubcapitulo() {
    }

    public CatSubcapitulo(CatSubcapituloPK catSubcapituloPK) {
        this.catSubcapituloPK = catSubcapituloPK;
    }

    public CatSubcapitulo(int idTramite, short nsub, short capituloId, short subcapituloId) {
        this.catSubcapituloPK = new CatSubcapituloPK(idTramite, nsub, capituloId, subcapituloId);
    }

    public CatSubcapituloPK getCatSubcapituloPK() {
        return catSubcapituloPK;
    }

    public void setCatSubcapituloPK(CatSubcapituloPK catSubcapituloPK) {
        this.catSubcapituloPK = catSubcapituloPK;
    }

    public String getSucbapituloDescripcion() {
        return sucbapituloDescripcion;
    }

    public void setSucbapituloDescripcion(String sucbapituloDescripcion) {
        this.sucbapituloDescripcion = sucbapituloDescripcion;
    }

    public String getSupcapituloAyuda() {
        return supcapituloAyuda;
    }

    public void setSupcapituloAyuda(String supcapituloAyuda) {
        this.supcapituloAyuda = supcapituloAyuda;
    }

    @XmlTransient
    public List<CatSeccion> getCatSeccionList() {
        return catSeccionList;
    }

    public void setCatSeccionList(List<CatSeccion> catSeccionList) {
        this.catSeccionList = catSeccionList;
    }

    public CatCapitulo getCatCapitulo() {
        return catCapitulo;
    }

    public void setCatCapitulo(CatCapitulo catCapitulo) {
        this.catCapitulo = catCapitulo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catSubcapituloPK != null ? catSubcapituloPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatSubcapitulo)) {
            return false;
        }
        CatSubcapitulo other = (CatSubcapitulo) object;
        if ((this.catSubcapituloPK == null && other.catSubcapituloPK != null) || (this.catSubcapituloPK != null && !this.catSubcapituloPK.equals(other.catSubcapituloPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatSubcapitulo[ catSubcapituloPK=" + catSubcapituloPK + " ]";
    }
    
}
