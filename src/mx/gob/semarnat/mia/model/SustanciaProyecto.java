/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "SUSTANCIA_PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SustanciaProyecto.findAll", query = "SELECT s FROM SustanciaProyecto s")})
public class SustanciaProyecto implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sustanciaProyecto")
    private Collection<SustanciaAnexo> sustanciaAnexoCollection;
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SustanciaProyectoPK sustanciaProyectoPK;
    @Basic(optional = false)
    @Column(name = "CTUN_CLVE")
    private short ctunClve;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SUSTANCIA_CANTIDAD_ALMACENADA")
    private BigDecimal sustanciaCantidadAlmacenada;
//    @Column(name = "ANEXO_URL")
//    private String anexoUrl;
//    @Column(name = "ANEXO_DESCRIPCION")
//    private String anexoDescripcion;
//    @Column(name = "ANEXO_NOMBRE")
//    private String anexoNombre;
//    @Column(name = "ANEXO_TAMANIO")
//    private BigDecimal anexoTamanio;
    @Column(name = "SUSTANCIA_PROMOVENTE")
    private String sustanciaPromovente;
//    @Column(name = "ANEXO_EXTENSION")
//    private String anexoExtension;
    @JoinColumn(name = "ETAPA_ID", referencedColumnName = "ETAPA_ID")
    @ManyToOne
    private CatEtapa etapaId;
    @JoinColumn(name = "SUSTANCIA_ID", referencedColumnName = "SUSTANCIA_ID")
    @ManyToOne(optional = false)
    private CatSustanciaAltamRiesgosa sustanciaId;
        
    @Column(name = "EXCEDE_LIMITE")
    private String excedeLimite;
    
    @Column(name="ID_ARCHIVO_PROGRAMA")
    private Short idArchivoProyecto;
    
    public SustanciaProyecto() {
      
    }

    public SustanciaProyecto(SustanciaProyectoPK sustanciaProyectoPK) {
        this.sustanciaProyectoPK = sustanciaProyectoPK;
    }

    public SustanciaProyecto(SustanciaProyectoPK sustanciaProyectoPK, short ctunClve) {
        this.sustanciaProyectoPK = sustanciaProyectoPK;
        this.ctunClve = ctunClve;
    }

    public SustanciaProyecto(String folioProyecto, short serialProyecto, short sustProyId) {
        this.sustanciaProyectoPK = new SustanciaProyectoPK(folioProyecto, serialProyecto, sustProyId);
    }

    public SustanciaProyectoPK getSustanciaProyectoPK() {
        return sustanciaProyectoPK;
    }

    public void setSustanciaProyectoPK(SustanciaProyectoPK sustanciaProyectoPK) {
        this.sustanciaProyectoPK = sustanciaProyectoPK;
    }

    public short getCtunClve() {
        return ctunClve;
    }

    public void setCtunClve(short ctunClve) {
        this.ctunClve = ctunClve;
    }

    public BigDecimal getSustanciaCantidadAlmacenada() {
        return sustanciaCantidadAlmacenada;
    }

    public void setSustanciaCantidadAlmacenada(BigDecimal sustanciaCantidadAlmacenada) {
        this.sustanciaCantidadAlmacenada = sustanciaCantidadAlmacenada;
    }

//    public String getAnexoUrl() {
//        return anexoUrl;
//    }
//
//    public void setAnexoUrl(String anexoUrl) {
//        this.anexoUrl = anexoUrl;
//    }
//
//    public String getAnexoDescripcion() {
//        return anexoDescripcion;
//    }
//
//    public void setAnexoDescripcion(String anexoDescripcion) {
//        this.anexoDescripcion = anexoDescripcion;
//    }
//
//    public String getAnexoNombre() {
//        return anexoNombre;
//    }

//    public void setAnexoNombre(String anexoNombre) {
//        this.anexoNombre = anexoNombre;
//    }
//
//    public BigDecimal getAnexoTamanio() {
//        return anexoTamanio;
//    }
//
//    public void setAnexoTamanio(BigDecimal anexoTamanio) {
//        this.anexoTamanio = anexoTamanio;
//    }

    public String getSustanciaPromovente() {
        return sustanciaPromovente;
    }

    public void setSustanciaPromovente(String sustanciaPromovente) {
        this.sustanciaPromovente = sustanciaPromovente;
    }

//    public String getAnexoExtension() {
//        return anexoExtension;
//    }
//
//    public void setAnexoExtension(String anexoExtension) {
//        this.anexoExtension = anexoExtension;
//    }

    public CatEtapa getEtapaId() {
        return etapaId;
    }

    public void setEtapaId(CatEtapa etapaId) {
        this.etapaId = etapaId;
    }

    public CatSustanciaAltamRiesgosa getSustanciaId() {
        System.out.println("sust: " + sustanciaId.getSustanciaId());
        return sustanciaId;
    }

    public void setSustanciaId(CatSustanciaAltamRiesgosa sustanciaId) {
        this.sustanciaId = sustanciaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sustanciaProyectoPK != null ? sustanciaProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SustanciaProyecto)) {
            return false;
        }
        SustanciaProyecto other = (SustanciaProyecto) object;
        if ((this.sustanciaProyectoPK == null && other.sustanciaProyectoPK != null) || (this.sustanciaProyectoPK != null && !this.sustanciaProyectoPK.equals(other.sustanciaProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.SustanciaProyecto[ sustanciaProyectoPK=" + sustanciaProyectoPK + " ]";
    }

    @XmlTransient
    public Collection<SustanciaAnexo> getSustanciaAnexoCollection() {
        return sustanciaAnexoCollection;
    }

    public void setSustanciaAnexoCollection(Collection<SustanciaAnexo> sustanciaAnexoCollection) {
        this.sustanciaAnexoCollection = sustanciaAnexoCollection;
    }

	public String getExcedeLimite() {
		return excedeLimite;
	}

	public void setExcedeLimite(String excedeLimite) {
		this.excedeLimite = excedeLimite;
	}

	public Short getIdArchivoProyecto() {
		return idArchivoProyecto;
	}

	public void setIdArchivoProyecto(Short idArchivoProyecto) {
		this.idArchivoProyecto = idArchivoProyecto;
	}
	
	


    
    
}
