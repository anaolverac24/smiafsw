/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import mx.gob.semarnat.mia.converter.CatClasificacionBConverter;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "PREDIOCOLIN_PROY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PrediocolinProy.findAll", query = "SELECT p FROM PrediocolinProy p"),
    @NamedQuery(name = "PrediocolinProy.findByFolioProyecto", query = "SELECT p FROM PrediocolinProy p WHERE p.prediocolinProyPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "PrediocolinProy.findBySerialProyecto", query = "SELECT p FROM PrediocolinProy p WHERE p.prediocolinProyPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "PrediocolinProy.findByPredioColinId", query = "SELECT p FROM PrediocolinProy p WHERE p.prediocolinProyPK.predioColinId = :predioColinId"),
    @NamedQuery(name = "PrediocolinProy.findByPredioNombre", query = "SELECT p FROM PrediocolinProy p WHERE p.predioNombre = :predioNombre"),
    @NamedQuery(name = "PrediocolinProy.findByPredioDescripcion", query = "SELECT p FROM PrediocolinProy p WHERE p.predioDescripcion = :predioDescripcion")})
public class PrediocolinProy implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PrediocolinProyPK prediocolinProyPK;
    @Column(name = "PREDIO_NOMBRE")
    private String predioNombre;
    @Column(name = "PREDIO_DESCRIPCION")
    private String predioDescripcion;
    @JoinColumn(name = "CLASIFICACION_ID", referencedColumnName = "CLASIFICACION_ID")
    @ManyToOne(optional = false)
    private CatClasificacion clasificacionId;
    @JoinColumn(name = "REFERENCIA_ID", referencedColumnName = "REFERENCIA_ID")
    @ManyToOne(optional = false)
    private CatReferencia referenciaId;
    @JoinColumn(name = "TEMPORALIDAD_ID", referencedColumnName = "TEMPORALIDAD_ID")
    @ManyToOne
    private CatTemporalidad temporalidadId;
    @JoinColumn(name = "CLASIFICACION_B_ID", referencedColumnName = "CLASIFICACION_B_ID")
    @ManyToOne(optional = false)
    private CatClasificacionB clasificacionBId;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public PrediocolinProy() {
    }

    public PrediocolinProy(PrediocolinProyPK prediocolinProyPK) {
        this.prediocolinProyPK = prediocolinProyPK;
    }

    public PrediocolinProy(String folioProyecto, short serialProyecto, short predioColinId) {
        this.prediocolinProyPK = new PrediocolinProyPK(folioProyecto, serialProyecto, predioColinId);
    }

    public PrediocolinProyPK getPrediocolinProyPK() {
        return prediocolinProyPK;
    }

    public void setPrediocolinProyPK(PrediocolinProyPK prediocolinProyPK) {
        this.prediocolinProyPK = prediocolinProyPK;
    }

    public String getPredioNombre() {
        return predioNombre;
    }

    public void setPredioNombre(String predioNombre) {
        this.predioNombre = predioNombre;
    }

    public String getPredioDescripcion() {
        return predioDescripcion;
    }

    public String getPredioDescripcionCorta(){
        if(predioDescripcion.length()<199){
            return predioDescripcion;
        }
        return predioDescripcion.substring(0, 199);
    }
    
    public void setPredioDescripcion(String predioDescripcion) {
        this.predioDescripcion = predioDescripcion;
    }

    public CatClasificacion getClasificacionId() {
        return clasificacionId;
    }

    public void setClasificacionId(CatClasificacion clasificacionId) {
        this.clasificacionId = clasificacionId;
    }

    public CatReferencia getReferenciaId() {
        return referenciaId;
    }

    public void setReferenciaId(CatReferencia referenciaId) {
        this.referenciaId = referenciaId;
    }

    public CatTemporalidad getTemporalidadId() {
        return temporalidadId;
    }

    public void setTemporalidadId(CatTemporalidad temporalidadId) {
        this.temporalidadId = temporalidadId;
    }
    
    public CatClasificacionB getClasificacionBId() {
		return clasificacionBId;
	}

	public void setClasificacionBId(CatClasificacionB clasificacionBId) {
		this.clasificacionBId = clasificacionBId;
	}

	public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (prediocolinProyPK != null ? prediocolinProyPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PrediocolinProy)) {
            return false;
        }
        PrediocolinProy other = (PrediocolinProy) object;
        if ((this.prediocolinProyPK == null && other.prediocolinProyPK != null) || (this.prediocolinProyPK != null && !this.prediocolinProyPK.equals(other.prediocolinProyPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.PrediocolinProy[ prediocolinProyPK=" + prediocolinProyPK + " ]";
    }
    
}
