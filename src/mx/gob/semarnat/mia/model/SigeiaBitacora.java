/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "SIGEIA_BITACORA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SigeiaBitacora.findAll", query = "SELECT s FROM SigeiaBitacora s")})
public class SigeiaBitacora implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SigeiaBitacoraPK sigeiaBitacoraPK;
    @Column(name = "CODIGO_ERROR")
    private BigInteger codigoError;
    @Column(name = "IDEVENTO")
    private BigInteger idevento;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sigeiaBitacora")
    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public SigeiaBitacora() {
    }

    public SigeiaBitacora(SigeiaBitacoraPK sigeiaBitacoraPK) {
        this.sigeiaBitacoraPK = sigeiaBitacoraPK;
    }

    public SigeiaBitacora(String numFolio, String cveProy, String cveArea, short version) {
        this.sigeiaBitacoraPK = new SigeiaBitacoraPK(numFolio, cveProy, cveArea, version);
    }

    public SigeiaBitacoraPK getSigeiaBitacoraPK() {
        return sigeiaBitacoraPK;
    }

    public void setSigeiaBitacoraPK(SigeiaBitacoraPK sigeiaBitacoraPK) {
        this.sigeiaBitacoraPK = sigeiaBitacoraPK;
    }

    public BigInteger getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(BigInteger codigoError) {
        this.codigoError = codigoError;
    }

    public BigInteger getIdevento() {
        return idevento;
    }

    public void setIdevento(BigInteger idevento) {
        this.idevento = idevento;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sigeiaBitacoraPK != null ? sigeiaBitacoraPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SigeiaBitacora)) {
            return false;
        }
        SigeiaBitacora other = (SigeiaBitacora) object;
        if ((this.sigeiaBitacoraPK == null && other.sigeiaBitacoraPK != null) || (this.sigeiaBitacoraPK != null && !this.sigeiaBitacoraPK.equals(other.sigeiaBitacoraPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.SigeiaBitacora[ sigeiaBitacoraPK=" + sigeiaBitacoraPK + " ]";
    }
    
}
