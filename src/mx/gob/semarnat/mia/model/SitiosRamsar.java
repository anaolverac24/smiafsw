/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "SITIOS_RAMSAR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SitiosRamsar.findAll", query = "SELECT s FROM SitiosRamsar s")})
public class SitiosRamsar implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SitiosRamsarPK sitiosRamsarPK;
    @Column(name = "RAMSAR")
    private String ramsar;
    @Column(name = "FECHA_INGR")
    private String fechaIngr;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sitiosRamsar")
    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public SitiosRamsar() {
    }

    public SitiosRamsar(SitiosRamsarPK sitiosRamsarPK) {
        this.sitiosRamsarPK = sitiosRamsarPK;
    }

    public SitiosRamsar(String numFolio, String cveProy, String cveArea, short version) {
        this.sitiosRamsarPK = new SitiosRamsarPK(numFolio, cveProy, cveArea, version);
    }

    public SitiosRamsarPK getSitiosRamsarPK() {
        return sitiosRamsarPK;
    }

    public void setSitiosRamsarPK(SitiosRamsarPK sitiosRamsarPK) {
        this.sitiosRamsarPK = sitiosRamsarPK;
    }

    public String getRamsar() {
        return ramsar;
    }

    public void setRamsar(String ramsar) {
        this.ramsar = ramsar;
    }

    public String getFechaIngr() {
        return fechaIngr;
    }

    public void setFechaIngr(String fechaIngr) {
        this.fechaIngr = fechaIngr;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sitiosRamsarPK != null ? sitiosRamsarPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SitiosRamsar)) {
            return false;
        }
        SitiosRamsar other = (SitiosRamsar) object;
        if ((this.sitiosRamsarPK == null && other.sitiosRamsarPK != null) || (this.sitiosRamsarPK != null && !this.sitiosRamsarPK.equals(other.sitiosRamsarPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.SitiosRamsar[ sitiosRamsarPK=" + sitiosRamsarPK + " ]";
    }
    
}
