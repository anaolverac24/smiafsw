/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author EdgarSC
 */
@Entity
@Table(name = "ACTIVIDAD_ETAPA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ActividadEtapa.findAll", query = "SELECT a FROM ActividadEtapa a"),
    @NamedQuery(name = "ActividadEtapa.findByFolioProyecto", query = "SELECT a FROM ActividadEtapa a WHERE a.actividadEtapaPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "ActividadEtapa.findBySerialProyecto", query = "SELECT a FROM ActividadEtapa a WHERE a.actividadEtapaPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "ActividadEtapa.findByEtapaId", query = "SELECT a FROM ActividadEtapa a WHERE a.actividadEtapaPK.etapaId = :etapaId"),
    @NamedQuery(name = "ActividadEtapa.findByActividadEtapaId", query = "SELECT a FROM ActividadEtapa a WHERE a.actividadEtapaPK.actividadEtapaId = :actividadEtapaId"),
    @NamedQuery(name = "ActividadEtapa.findByActividadNombre", query = "SELECT a FROM ActividadEtapa a WHERE a.actividadNombre = :actividadNombre")})
//    @NamedQuery(name = "ActividadEtapa.findByActividadJustificacion", query = "SELECT a FROM ActividadEtapa a WHERE a.actividadJustificacion = :actividadJustificacion"),
//    @NamedQuery(name = "ActividadEtapa.findByActividadMetodologia", query = "SELECT a FROM ActividadEtapa a WHERE a.actividadMetodologia = :actividadMetodologia")    
public class ActividadEtapa implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ActividadEtapaPK actividadEtapaPK;
    @Column(name = "ACTIVIDAD_NOMBRE")
    private String actividadNombre;
    @Lob
    @Column(name = "ACTIVIDAD_DESCRIPCION")
    private String actividadDescripcion;
//    @Column(name = "ACTIVIDAD_JUSTIFICACION")
//    private String actividadJustificacion;
//    @Column(name = "ACTIVIDAD_METODOLOGIA")
//    private String actividadMetodologia;
    @JoinColumns({
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "ETAPA_ID", referencedColumnName = "ETAPA_ID", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private EtapaProyecto etapaProyecto;

    public ActividadEtapa() {
    }

    public ActividadEtapa(ActividadEtapaPK actividadEtapaPK) {
        this.actividadEtapaPK = actividadEtapaPK;
    }

    public ActividadEtapa(String folioProyecto, short serialProyecto, short etapaId, short actividadEtapaId) {
        this.actividadEtapaPK = new ActividadEtapaPK(folioProyecto, serialProyecto, etapaId, actividadEtapaId);
    }

    public ActividadEtapaPK getActividadEtapaPK() {
        return actividadEtapaPK;
    }

    public void setActividadEtapaPK(ActividadEtapaPK actividadEtapaPK) {
        this.actividadEtapaPK = actividadEtapaPK;
    }

    public String getActividadNombre() {
        return actividadNombre;
    }

    public void setActividadNombre(String actividadNombre) {
        this.actividadNombre = actividadNombre;
    }
    
    public String getActividadNombreCorto(){
        if(actividadNombre.length()<39){
            return actividadNombre;
        }
        return actividadNombre.substring(0, 39);
    }

    public String getActividadDescripcion() {
        return actividadDescripcion;
    }

    public void setActividadDescripcion(String actividadDescripcion) {
        this.actividadDescripcion = actividadDescripcion;
    }
    
    public String getActividadDescripcionCorta(){
        if(actividadDescripcion.length()<199){
            return actividadDescripcion;
        }
        return actividadDescripcion.substring(0, 199);
    }

//    public String getActividadJustificacion() {
//        return actividadJustificacion;
//    }
//
//    public void setActividadJustificacion(String actividadJustificacion) {
//        this.actividadJustificacion = actividadJustificacion;
//    }
//
//    public String getActividadMetodologia() {
//        return actividadMetodologia;
//    }
//
//    public void setActividadMetodologia(String actividadMetodologia) {
//        this.actividadMetodologia = actividadMetodologia;
//    }

    public EtapaProyecto getEtapaProyecto() {
        return etapaProyecto;
    }

    public void setEtapaProyecto(EtapaProyecto etapaProyecto) {
        this.etapaProyecto = etapaProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (actividadEtapaPK != null ? actividadEtapaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ActividadEtapa)) {
            return false;
        }
        ActividadEtapa other = (ActividadEtapa) object;
        if ((this.actividadEtapaPK == null && other.actividadEtapaPK != null) || (this.actividadEtapaPK != null && !this.actividadEtapaPK.equals(other.actividadEtapaPK))) {
            return false;
        }
        
        if (this.actividadEtapaPK.getActividadEtapaId() == other.actividadEtapaPK.getActividadEtapaId()
                &&
                this.actividadEtapaPK.getSerialProyecto()== other.actividadEtapaPK.getSerialProyecto()
                &&
                this.actividadEtapaPK.getEtapaId() == this.actividadEtapaPK.getEtapaId()){
            return true;
        }
        
        return false;
    }

    @Override
    public String toString() {
        // return "mx.gob.semarnat.mia.model.ActividadEtapa[ actividadEtapaPK=" + actividadEtapaPK + " ]";
    	return actividadNombre;
    }
    
}
