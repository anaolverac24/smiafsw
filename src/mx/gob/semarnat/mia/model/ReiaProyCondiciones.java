/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author marcog
 */
@Entity
@Table(name = "REIA_PROY_CONDICIONES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReiaProyCondiciones.findAll", query = "SELECT r FROM ReiaProyCondiciones r"),
    @NamedQuery(name = "ReiaProyCondiciones.findByclaveObraProy", query = "SELECT r FROM ReiaProyCondiciones r WHERE r.claveObraProy = :claveObraProy"),
    @NamedQuery(name = "ReiaProyCondiciones.findByCondicion", query = "SELECT r FROM ReiaProyCondiciones r WHERE r.condicion = :condicion"),
    @NamedQuery(name = "ReiaProyCondiciones.findByRespuesta", query = "SELECT r FROM ReiaProyCondiciones r WHERE r.respuesta = :respuesta"),
    @NamedQuery(name = "ReiaProyCondiciones.findByClaveCondicion", query = "SELECT r FROM ReiaProyCondiciones r WHERE r.claveObraProy.clave = :claveObraProy and r.condicion = :condicion"),
    @NamedQuery(name = "ReiaProyCondiciones.findByClave", query = "SELECT r FROM ReiaProyCondiciones r WHERE r.clave = :clave")})
public class ReiaProyCondiciones implements Serializable {
	
    private static final long serialVersionUID = 1L;
    
    // Se tiene que borrar la variable FOLIO 
    //    @Column(name = "FOLIO")
    //    private String folio;
    // Se agrega la nueva variable CLAVE_OBRA_PROY para relacionar con ,a tabla ReiaProyObras
    @JoinColumn(name = "CLAVE_OBRA_PROY", referencedColumnName = "CLAVE")
    @ManyToOne(cascade=CascadeType.REMOVE)
    private ReiaProyObras claveObraProy;
    @Column(name = "CONDICION")
    private Long condicion;
    @Column(name = "RESPUESTA")
    private Long respuesta;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(generator="InvSeqC") 
    @SequenceGenerator(name="InvSeqC",sequenceName="seq_proy_condiciones")    
    @Basic(optional = false)
    private Long clave;

    public ReiaProyCondiciones() {
    }

    public ReiaProyCondiciones(Long clave) {
        this.clave = clave;
    }

//    public String getFolio() {
//        return folio;
//    }
//    public void setFolio(String folio) {
//        this.folio = folio;
//    }

    public Long getCondicion() {
        return condicion;
    }

    public void setCondicion(Long condicion) {
        this.condicion = condicion;
    }

    public Long getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Long respuesta) {
        this.respuesta = respuesta;
    }

    public Long getClave() {
        return clave;
    }

    public void setClave(Long clave) {
        this.clave = clave;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clave != null ? clave.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReiaProyCondiciones)) {
            return false;
        }
        ReiaProyCondiciones other = (ReiaProyCondiciones) object;
        if ((this.clave == null && other.clave != null) || (this.clave != null && !this.clave.equals(other.clave))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ReiaProyCondiciones[ clave=" + clave + " ]";
    }

	/**
	 * @return the claveObraProy
	 */
	public ReiaProyObras getClaveObraProy() {
		return claveObraProy;
	}

	/**
	 * @param claveObraProy the claveObraProy to set
	 */
	public void setClaveObraProy(ReiaProyObras claveObraProy) {
		this.claveObraProy = claveObraProy;
	}
    
}
