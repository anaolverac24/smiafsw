/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "SITIOS_CONTAMINADOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SitiosContaminados.findAll", query = "SELECT s FROM SitiosContaminados s")})
public class SitiosContaminados implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SitiosContaminadosPK sitiosContaminadosPK;
    @Column(name = "REGISTRO")
    private String registro;
    @Column(name = "A_RASOC")
    private String aRasoc;
    @Column(name = "A_CLASIT")
    private String aClasit;
    @Column(name = "A_OCLASI")
    private String aOclasi;
    @Column(name = "CONSULTO")
    private String consulto;
    @Column(name = "A_IDFTE")
    private String aIdfte;
    @Column(name = "O_IDFTE")
    private String oIdfte;
    @Column(name = "SUP_EA")
    private BigInteger supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private BigInteger areabuffer;
    @Column(name = "AREA")
    private BigInteger area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sitiosContaminados")
    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public SitiosContaminados() {
    }

    public SitiosContaminados(SitiosContaminadosPK sitiosContaminadosPK) {
        this.sitiosContaminadosPK = sitiosContaminadosPK;
    }

    public SitiosContaminados(String numFolio, String cveProy, String cveArea, short version) {
        this.sitiosContaminadosPK = new SitiosContaminadosPK(numFolio, cveProy, cveArea, version);
    }

    public SitiosContaminadosPK getSitiosContaminadosPK() {
        return sitiosContaminadosPK;
    }

    public void setSitiosContaminadosPK(SitiosContaminadosPK sitiosContaminadosPK) {
        this.sitiosContaminadosPK = sitiosContaminadosPK;
    }

    public String getRegistro() {
        return registro;
    }

    public void setRegistro(String registro) {
        this.registro = registro;
    }

    public String getARasoc() {
        return aRasoc;
    }

    public void setARasoc(String aRasoc) {
        this.aRasoc = aRasoc;
    }

    public String getAClasit() {
        return aClasit;
    }

    public void setAClasit(String aClasit) {
        this.aClasit = aClasit;
    }

    public String getAOclasi() {
        return aOclasi;
    }

    public void setAOclasi(String aOclasi) {
        this.aOclasi = aOclasi;
    }

    public String getConsulto() {
        return consulto;
    }

    public void setConsulto(String consulto) {
        this.consulto = consulto;
    }

    public String getAIdfte() {
        return aIdfte;
    }

    public void setAIdfte(String aIdfte) {
        this.aIdfte = aIdfte;
    }

    public String getOIdfte() {
        return oIdfte;
    }

    public void setOIdfte(String oIdfte) {
        this.oIdfte = oIdfte;
    }

    public BigInteger getSupEa() {
        return supEa;
    }

    public void setSupEa(BigInteger supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigInteger getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(BigInteger areabuffer) {
        this.areabuffer = areabuffer;
    }

    public BigInteger getArea() {
        return area;
    }

    public void setArea(BigInteger area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sitiosContaminadosPK != null ? sitiosContaminadosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SitiosContaminados)) {
            return false;
        }
        SitiosContaminados other = (SitiosContaminados) object;
        if ((this.sitiosContaminadosPK == null && other.sitiosContaminadosPK != null) || (this.sitiosContaminadosPK != null && !this.sitiosContaminadosPK.equals(other.sitiosContaminadosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.SitiosContaminados[ sitiosContaminadosPK=" + sitiosContaminadosPK + " ]";
    }
    
}
