/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "OE_GRAL_TERRIT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OeGralTerrit.findAll", query = "SELECT o FROM OeGralTerrit o")})
public class OeGralTerrit implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OeGralTerritPK oeGralTerritPK;
    @Column(name = "REGION")
    private String region;
    @Column(name = "UAB")
    private Double uab;
    @Column(name = "NOM")
    private String nom;
    @Column(name = "POLIT_CVE")
    private Double politCve;
    @Column(name = "POLITICA")
    private String politica;
    @Column(name = "AAP_NOM")
    private String aapNom;
    @Column(name = "RECTOR")
    private String rector;
    @Column(name = "COADYUVANT")
    private String coadyuvant;
    @Column(name = "ASOCIADOS")
    private String asociados;
    @Column(name = "OTROS_SECT")
    private String otrosSect;
    @Column(name = "POB_2010")
    private String pob2010;
    @Column(name = "REG_INDIG")
    private String regIndig;
    @Column(name = "EDO_ACTUAL")
    private String edoActual;
    @Column(name = "CTO_PLA_20")
    private String ctoPla20;
    @Column(name = "MED_PLA_20")
    private String medPla20;
    @Column(name = "LAR_PLA_20")
    private String larPla20;
    @Column(name = "ESTRATEGIA")
    private String estrategia;
    @Column(name = "SUP_EA")
    private Double supEa;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "AREABUFFER")
    private Double areabuffer;
    @Column(name = "AREA")
    private Double area;
    @Column(name = "FECHA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oeGralTerrit")
    private List<SigeiaAnalisis> sigeiaAnalisisList;

    public OeGralTerrit() {
    }

    public OeGralTerrit(OeGralTerritPK oeGralTerritPK) {
        this.oeGralTerritPK = oeGralTerritPK;
    }

    public OeGralTerrit(String numFolio, String cveProy, String cveArea, short version) {
        this.oeGralTerritPK = new OeGralTerritPK(numFolio, cveProy, cveArea, version);
    }

    public OeGralTerritPK getOeGralTerritPK() {
        return oeGralTerritPK;
    }

    public void setOeGralTerritPK(OeGralTerritPK oeGralTerritPK) {
        this.oeGralTerritPK = oeGralTerritPK;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Double getUab() {
        return uab;
    }

    public void setUab(Double uab) {
        this.uab = uab;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Double getPolitCve() {
        return politCve;
    }

    public void setPolitCve(Double politCve) {
        this.politCve = politCve;
    }

    public String getPolitica() {
        return politica;
    }

    public void setPolitica(String politica) {
        this.politica = politica;
    }

    public String getAapNom() {
        return aapNom;
    }

    public void setAapNom(String aapNom) {
        this.aapNom = aapNom;
    }

    public String getRector() {
        return rector;
    }

    public void setRector(String rector) {
        this.rector = rector;
    }

    public String getCoadyuvant() {
        return coadyuvant;
    }

    public void setCoadyuvant(String coadyuvant) {
        this.coadyuvant = coadyuvant;
    }

    public String getAsociados() {
        return asociados;
    }

    public void setAsociados(String asociados) {
        this.asociados = asociados;
    }

    public String getOtrosSect() {
        return otrosSect;
    }

    public void setOtrosSect(String otrosSect) {
        this.otrosSect = otrosSect;
    }

    public String getPob2010() {
        return pob2010;
    }

    public void setPob2010(String pob2010) {
        this.pob2010 = pob2010;
    }

    public String getRegIndig() {
        return regIndig;
    }

    public void setRegIndig(String regIndig) {
        this.regIndig = regIndig;
    }

    public String getEdoActual() {
        return edoActual;
    }

    public void setEdoActual(String edoActual) {
        this.edoActual = edoActual;
    }

    public String getCtoPla20() {
        return ctoPla20;
    }

    public void setCtoPla20(String ctoPla20) {
        this.ctoPla20 = ctoPla20;
    }

    public String getMedPla20() {
        return medPla20;
    }

    public void setMedPla20(String medPla20) {
        this.medPla20 = medPla20;
    }

    public String getLarPla20() {
        return larPla20;
    }

    public void setLarPla20(String larPla20) {
        this.larPla20 = larPla20;
    }

    public String getEstrategia() {
        return estrategia;
    }

    public void setEstrategia(String estrategia) {
        this.estrategia = estrategia;
    }

    public Double getSupEa() {
        return supEa;
    }

    public void setSupEa(Double supEa) {
        this.supEa = supEa;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public Double getAreabuffer() {
        return areabuffer;
    }

    public void setAreabuffer(Double areabuffer) {
        this.areabuffer = areabuffer;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (oeGralTerritPK != null ? oeGralTerritPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OeGralTerrit)) {
            return false;
        }
        OeGralTerrit other = (OeGralTerrit) object;
        if ((this.oeGralTerritPK == null && other.oeGralTerritPK != null) || (this.oeGralTerritPK != null && !this.oeGralTerritPK.equals(other.oeGralTerritPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.OeGralTerrit[ oeGralTerritPK=" + oeGralTerritPK + " ]";
    }
    
}
