/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "DISPOSICION_PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DisposicionProyecto.findAll", query = "SELECT d FROM DisposicionProyecto d"),
    @NamedQuery(name = "DisposicionProyecto.findByFolioProyecto", query = "SELECT d FROM DisposicionProyecto d WHERE d.disposicionProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "DisposicionProyecto.findBySerialProyecto", query = "SELECT d FROM DisposicionProyecto d WHERE d.disposicionProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "DisposicionProyecto.findByDisposicionProyId", query = "SELECT d FROM DisposicionProyecto d WHERE d.disposicionProyectoPK.disposicionProyId = :disposicionProyId"),
    @NamedQuery(name = "DisposicionProyecto.findByDisposicionNombre", query = "SELECT d FROM DisposicionProyecto d WHERE d.disposicionNombre = :disposicionNombre")})
public class DisposicionProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DisposicionProyectoPK disposicionProyectoPK;
    @Column(name = "DISPOSICION_NOMBRE")
    private String disposicionNombre;
    @Lob
    @Column(name = "DISPOSICION_VINCULACION")
    private String disposicionVinculacion;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public DisposicionProyecto() {
    }

    public DisposicionProyecto(DisposicionProyectoPK disposicionProyectoPK) {
        this.disposicionProyectoPK = disposicionProyectoPK;
    }

    public DisposicionProyecto(String folioProyecto, short serialProyecto, short disposicionProyId) {
        this.disposicionProyectoPK = new DisposicionProyectoPK(folioProyecto, serialProyecto, disposicionProyId);
    }

    public DisposicionProyectoPK getDisposicionProyectoPK() {
        return disposicionProyectoPK;
    }

    public void setDisposicionProyectoPK(DisposicionProyectoPK disposicionProyectoPK) {
        this.disposicionProyectoPK = disposicionProyectoPK;
    }

    public String getDisposicionNombre() {
        return disposicionNombre;
    }
    
    public String getDisposicionNombreCorto(){
        if(disposicionNombre.length()<99){
            return disposicionNombre;
        }
        return disposicionNombre.substring(0, 99);
    }

    public void setDisposicionNombre(String disposicionNombre) {
        this.disposicionNombre = disposicionNombre;
    }

    public String getDisposicionVinculacion() {
        return disposicionVinculacion;
    }
    
    public String getDisposicionVinculacionCorta(){
        if(disposicionVinculacion.length()<199){
            return disposicionVinculacion;
        }
        return disposicionVinculacion.substring(0, 199);
    }

    public void setDisposicionVinculacion(String disposicionVinculacion) {
        this.disposicionVinculacion = disposicionVinculacion;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (disposicionProyectoPK != null ? disposicionProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DisposicionProyecto)) {
            return false;
        }
        DisposicionProyecto other = (DisposicionProyecto) object;
        if ((this.disposicionProyectoPK == null && other.disposicionProyectoPK != null) || (this.disposicionProyectoPK != null && !this.disposicionProyectoPK.equals(other.disposicionProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.DisposicionProyecto[ disposicionProyectoPK=" + disposicionProyectoPK + " ]";
    }
    
}
