/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "ESPECIFICACION_ANEXO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EspecificacionAnexo.findAll", query = "SELECT e FROM EspecificacionAnexo e")})
public class EspecificacionAnexo implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EspecificacionAnexoPK especificacionAnexoPK;
    @Column(name = "ESPEC_ANEXO_DESCRIPCION")
    private String especAnexoDescripcion;
    @Column(name = "ESPEC_ANEXO_URL")
    private String especAnexoUrl;
    @Column(name = "ESPEC_ANEXO_EXTENSION")
    private String especAnexoExtension;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ESPEC_ANEXO_TAMANIO")
    private BigDecimal especAnexoTamanio;
    @Column(name = "ESPEC_ANEXO_NOMBRE")
    private String especAnexoNombre;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "ESPECIFICACION_ID", referencedColumnName = "ESPECIFICACION_ID", insertable = false, updatable = false),
        @JoinColumn(name = "NORMA_ID", referencedColumnName = "NORMA_ID", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private EspecificacionProyecto especificacionProyecto;

    public EspecificacionAnexo() {
    }

    public EspecificacionAnexo(EspecificacionAnexoPK especificacionAnexoPK) {
        this.especificacionAnexoPK = especificacionAnexoPK;
    }

    public EspecificacionAnexo(String folioProyecto, short serialProyecto, String especificacionId, short normaId, short especificacionAnexoId) {
        this.especificacionAnexoPK = new EspecificacionAnexoPK(folioProyecto, serialProyecto, especificacionId, normaId, especificacionAnexoId);
    }

    public EspecificacionAnexoPK getEspecificacionAnexoPK() {
        return especificacionAnexoPK;
    }

    public void setEspecificacionAnexoPK(EspecificacionAnexoPK especificacionAnexoPK) {
        this.especificacionAnexoPK = especificacionAnexoPK;
    }

    public String getEspecAnexoDescripcion() {
        return especAnexoDescripcion;
    }

    public void setEspecAnexoDescripcion(String especAnexoDescripcion) {
        this.especAnexoDescripcion = especAnexoDescripcion;
    }

    public String getEspecAnexoUrl() {
        return especAnexoUrl;
    }

    public void setEspecAnexoUrl(String especAnexoUrl) {
        this.especAnexoUrl = especAnexoUrl;
    }

    public String getEspecAnexoExtension() {
        return especAnexoExtension;
    }

    public void setEspecAnexoExtension(String especAnexoExtension) {
        this.especAnexoExtension = especAnexoExtension;
    }

    public BigDecimal getEspecAnexoTamanio() {
        return especAnexoTamanio;
    }

    public void setEspecAnexoTamanio(BigDecimal especAnexoTamanio) {
        this.especAnexoTamanio = especAnexoTamanio;
    }

    public String getEspecAnexoNombre() {
        return especAnexoNombre;
    }

    public void setEspecAnexoNombre(String especAnexoNombre) {
        this.especAnexoNombre = especAnexoNombre;
    }

    public EspecificacionProyecto getEspecificacionProyecto() {
        return especificacionProyecto;
    }

    public void setEspecificacionProyecto(EspecificacionProyecto especificacionProyecto) {
        this.especificacionProyecto = especificacionProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (especificacionAnexoPK != null ? especificacionAnexoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EspecificacionAnexo)) {
            return false;
        }
        EspecificacionAnexo other = (EspecificacionAnexo) object;
        if ((this.especificacionAnexoPK == null && other.especificacionAnexoPK != null) || (this.especificacionAnexoPK != null && !this.especificacionAnexoPK.equals(other.especificacionAnexoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.EspecificacionAnexo[ especificacionAnexoPK=" + especificacionAnexoPK + " ]";
    }
    
}
