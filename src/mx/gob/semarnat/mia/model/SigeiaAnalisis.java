/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "SIGEIA_ANALISIS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SigeiaAnalisis.findAll", query = "SELECT s FROM SigeiaAnalisis s")})
public class SigeiaAnalisis implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SigeiaAnalisisPK sigeiaAnalisisPK;
    @Basic(optional = false)
    @Column(name = "OBJECTID")
    private BigInteger objectid;
    @Column(name = "ID_ANALISIS")
    private String idAnalisis;
    @JoinColumns({
        @JoinColumn(name = "ANP_FEDERAL_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "ANP_FEDERAL_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "ANP_FEDERAL_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "ANP_FEDERAL_VERSION", referencedColumnName = "VERSION"),
        @JoinColumn(name = "ANP_FEDERAL_IDR", referencedColumnName = "IDR")})
    @ManyToOne(optional = false)
    private AnpFederal anpFederal;
    @JoinColumns({
        @JoinColumn(name = "AICA_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "AICA_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "AICA_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "AICA_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private Aica aica;
    @JoinColumns({
        @JoinColumn(name = "ACUIFEROS_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "ACUIFEROS_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "ACUIFEROS_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "ACUIFEROS_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private Acuiferos acuiferos;
    @JoinColumns({
        @JoinColumn(name = "ANP_ESTATAL_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "ANP_ESTATAL_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "ANP_ESTATAL_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "ANP_ESTATAL_VERSION", referencedColumnName = "VERSION"),
        @JoinColumn(name = "ANP_ESTATAL_IDR", referencedColumnName = "IDR")})
    @ManyToOne(optional = false)
    private AnpEstatal anpEstatal;
    @JoinColumns({
        @JoinColumn(name = "INSTR_URBAN_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "INSTR_URBAN_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "INSTR_URBAN_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "INSTR_URBAN_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private InstrUrbanos instrUrbanos;
    @JoinColumns({
        @JoinColumn(name = "ANP_FED_ZONANUC_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "ANP_FED_ZONANUC_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "ANP_FED_ZONANUC_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "ANP_FED_ZONANUC_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private AnpFedZonanucleo anpFedZonanucleo;
    @JoinColumns({
        @JoinColumn(name = "SITIOS_RAMSAR_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "SITIOS_RAMSAR_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "SITIOS_RAMSAR_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "SITIOS_RAMSAR_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private SitiosRamsar sitiosRamsar;
//    @JoinColumns({
//        @JoinColumn(name = "OE_REGIO_1_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
//        @JoinColumn(name = "OE_REGIO_1_CVE_PROY", referencedColumnName = "CVE_PROY"),
//        @JoinColumn(name = "OE_REGIO_1_CVE_AREA", referencedColumnName = "CVE_AREA"),
//        @JoinColumn(name = "OE_REGION_1_VERSION", referencedColumnName = "VERSION")})
//    @ManyToOne(optional = false)
//    private OeRegionales1 oeRegionales1;
    @JoinColumns({
        @JoinColumn(name = "UMA_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "UMA_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "UMA_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "UMA_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private Uma uma;
    @JoinColumns({
        @JoinColumn(name = "ANP_FED_ZONIFIC_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "ANP_FED_ZONIFIC_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "ANP_FED_ZONIFIC_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "ANP_FED_ZONIFIC_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private AnpFedZonificacion anpFedZonificacion;
    @JoinColumns({
        @JoinColumn(name = "REG_MARIN_PRIO_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "REG_MARIN_PRIO_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "REG_MARIN_PRIO_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "REG_MARIN_PRIO_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private RegMarinasPriori regMarinasPriori;
    @JoinColumns({
        @JoinColumn(name = "OE_MARINOS_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "OE_MARINOS_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "OE_MARINOS_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "OE_MARINOS_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private OeMarinos oeMarinos;
    @JoinColumns({
        @JoinColumn(name = "OGMSIG_OBJECTID_1", referencedColumnName = "OBJECTID_1"),
        @JoinColumn(name = "OGMSIG_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "OGMSIG_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "OGMSIG_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "OGMSIG_VERSION", referencedColumnName = "VERSION"),
        @JoinColumn(name = "OGMSIG_OBJECTID", referencedColumnName = "OBJECTID")})
    @ManyToOne(optional = false)
    private Ogmsig ogmsig;
    @JoinColumns({
        @JoinColumn(name = "REG_TER_PRIO_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "REG_TER_PRIO_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "REG_TER_PRIO_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "REG_TER_PRIO_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private RegTerrPriori regTerrPriori;
    @JoinColumns({
        @JoinColumn(name = "LOCALID_INDIGS_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "LOCALID_INDIG_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "LOCALID_INDIG_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "LOCALID_INDIG_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private LocalidIndigenas localidIndigenas;
    @JoinColumns({
        @JoinColumn(name = "SITIOS_CONT_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "SITIOS_CONT_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "SITIOS_CONT_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "SITIOS_CONT_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private SitiosContaminados sitiosContaminados;
    @JoinColumns({
        @JoinColumn(name = "OE_POLIG_ENV_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "OE_POLIG_ENV_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "OE_POLIG_ENV_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "OE_POLIG_ENV_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private OePoligEnvol oePoligEnvol;
    @JoinColumns({
        @JoinColumn(name = "OE_GRAL_TER_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "OE_GRAL_TER_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "OE_GRAL_TER_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "OE_GRAL_TER_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private OeGralTerrit oeGralTerrit;
    @JoinColumns({
        @JoinColumn(name = "ANP_MUICIPAL_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "ANP_MUICIPAL_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "ANP_MUICIPAL_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "ANP_MUICIPAL_VERSION", referencedColumnName = "VERSION"),
        @JoinColumn(name = "ANP_MUICIPAL_IDR", referencedColumnName = "IDR")})
    @ManyToOne(optional = false)
    private AnpMuicipal anpMuicipal;
    @JoinColumns({
        @JoinColumn(name = "SIGEIA_BITAC_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "SIGEIA_BITAC_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "SIGEIA_BITAC_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "SIGEIA_BITAC_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private SigeiaBitacora sigeiaBitacora;
    @JoinColumns({
        @JoinColumn(name = "MANGLARES_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "MANGLARES_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "MANGLARES_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "MANGLARES_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private Manglares manglares;
    @JoinColumns({
        @JoinColumn(name = "USO_SUEL_VEG_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "USO_SUEL_VEG_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "USO_SUEL_VEG_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "USO_SUEL_VEG_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private UsoSueloVeget usoSueloVeget;
//    @JoinColumns({
//        @JoinColumn(name = "MPIOS_CRUZVSHAM_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
//        @JoinColumn(name = "MPIOS_CRUZVSHAM_CVE_PROY", referencedColumnName = "CVE_PROY"),
//        @JoinColumn(name = "MPIOS_CRUZVSHAM_CVE_AREA", referencedColumnName = "CVE_AREA"),
//        @JoinColumn(name = "MPIOS_CRUZVSHAM_VERSION", referencedColumnName = "VERSION")})
//    @ManyToOne(optional = false)
//    private MpiosCruzadavshambre mpiosCruzadavshambre;
//    @JoinColumns({
//        @JoinColumn(name = "PROYSIG_OBJECTID", referencedColumnName = "OBJECTID"),
//        @JoinColumn(name = "PROYSIG_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
//        @JoinColumn(name = "PROYSIG_CVE_PROY", referencedColumnName = "CVE_PROY"),
//        @JoinColumn(name = "PROYSIG_CVE_AREA", referencedColumnName = "CVE_AREA"),
//        @JoinColumn(name = "PROYSIG_VERSION", referencedColumnName = "VERSION")})
//    @ManyToOne(optional = false)
//    private Proysig proysig;
    @JoinColumns({
        @JoinColumn(name = "MOD_SIG_OBJECTID", referencedColumnName = "OBJECTID"),
        @JoinColumn(name = "MOD_SIG_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "MOD_SIG_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "MOD_SIG_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "MOD_SIG_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private ModSig modSig;
    @JoinColumns({
        @JoinColumn(name = "APROVECH_FORES_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "APROVECH_FORES_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "APROVECH_FORES_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "APROVECH_FORES_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private AprovechForestales aprovechForestales;
    @JoinColumns({
        @JoinColumn(name = "CLIMAS_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "CLIMAS_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "CLIMAS_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "CLIMAS_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private Climas climas;
    @JoinColumns({
        @JoinColumn(name = "DIST_RIEGO_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "DIST_RIEGO_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "DIST_RIEGO_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "DIST_RIEGO_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private DistritoRiego distritoRiego;
//    @JoinColumns({
//        @JoinColumn(name = "OE_REGIO_3_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
//        @JoinColumn(name = "OE_REGIO_3_CVE_PROY", referencedColumnName = "CVE_PROY"),
//        @JoinColumn(name = "OE_REGIO_3_CVE_AREA", referencedColumnName = "CVE_AREA"),
//        @JoinColumn(name = "OE_REGIO_3_VERSION", referencedColumnName = "VERSION")})
//    @ManyToOne(optional = false)
//    private OeRegionales3 oeRegionales3;
//    @JoinColumns({
//        @JoinColumn(name = "OE_REGIO_2_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
//        @JoinColumn(name = "OE_REGIO_2_CVE_PROY", referencedColumnName = "CVE_PROY"),
//        @JoinColumn(name = "OE_REGIO_2_CVE_AREA", referencedColumnName = "CVE_AREA"),
//        @JoinColumn(name = "OE_REGIO_2_VERSION", referencedColumnName = "VERSION")})
//    @ManyToOne(optional = false)
//    private OeRegionales2 oeRegionales2;
    @JoinColumns({
        @JoinColumn(name = "REG_HIDR_PRIO_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "REG_HIDR_PRIO_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "REG_HIDR_PRIO_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "REG_HIDR_PRIO_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private RegHidrPriori regHidrPriori;
    @JoinColumns({
        @JoinColumn(name = "ORD_EC_LOC_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "ORD_EC_LOC_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "ORD_EC_LOC_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "ORD_EC_LOC_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private OrdEcologLoc ordEcologLoc;
    @JoinColumns({
        @JoinColumn(name = "EXCAVI_SIG_OBJECTID", referencedColumnName = "OBJECTID"),
        @JoinColumn(name = "EXCAVI_SIG_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "EXCAVI_SIG_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "EXCAVI_SIG_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "EXCAVI_SIG_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private ExcaviSig excaviSig;
    @JoinColumns({
        @JoinColumn(name = "ENTIDAD_FED_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "ENTIDAD_FED_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "ENTIDAD_FED_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "ENTIDAD_FEDE_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private EntidadFederativa entidadFederativa;
    @JoinColumns({
        @JoinColumn(name = "HUMEDALES_NUM_FOLIO", referencedColumnName = "NUM_FOLIO"),
        @JoinColumn(name = "HUMEDALES_CVE_PROY", referencedColumnName = "CVE_PROY"),
        @JoinColumn(name = "HUMEDALES_CVE_AREA", referencedColumnName = "CVE_AREA"),
        @JoinColumn(name = "HUMEDALES_VERSION", referencedColumnName = "VERSION")})
    @ManyToOne(optional = false)
    private Humedales humedales;

    public SigeiaAnalisis() {
    }

    public SigeiaAnalisis(SigeiaAnalisisPK sigeiaAnalisisPK) {
        this.sigeiaAnalisisPK = sigeiaAnalisisPK;
    }

    public SigeiaAnalisis(SigeiaAnalisisPK sigeiaAnalisisPK, BigInteger objectid) {
        this.sigeiaAnalisisPK = sigeiaAnalisisPK;
        this.objectid = objectid;
    }

    public SigeiaAnalisis(String numFolio, String cveProy, String cveArea, short version) {
        this.sigeiaAnalisisPK = new SigeiaAnalisisPK(numFolio, cveProy, cveArea, version);
    }

    public SigeiaAnalisisPK getSigeiaAnalisisPK() {
        return sigeiaAnalisisPK;
    }

    public void setSigeiaAnalisisPK(SigeiaAnalisisPK sigeiaAnalisisPK) {
        this.sigeiaAnalisisPK = sigeiaAnalisisPK;
    }

    public BigInteger getObjectid() {
        return objectid;
    }

    public void setObjectid(BigInteger objectid) {
        this.objectid = objectid;
    }

    public String getIdAnalisis() {
        return idAnalisis;
    }

    public void setIdAnalisis(String idAnalisis) {
        this.idAnalisis = idAnalisis;
    }

    public AnpFederal getAnpFederal() {
        return anpFederal;
    }

    public void setAnpFederal(AnpFederal anpFederal) {
        this.anpFederal = anpFederal;
    }

    public Aica getAica() {
        return aica;
    }

    public void setAica(Aica aica) {
        this.aica = aica;
    }

    public Acuiferos getAcuiferos() {
        return acuiferos;
    }

    public void setAcuiferos(Acuiferos acuiferos) {
        this.acuiferos = acuiferos;
    }

    public AnpEstatal getAnpEstatal() {
        return anpEstatal;
    }

    public void setAnpEstatal(AnpEstatal anpEstatal) {
        this.anpEstatal = anpEstatal;
    }

    public InstrUrbanos getInstrUrbanos() {
        return instrUrbanos;
    }

    public void setInstrUrbanos(InstrUrbanos instrUrbanos) {
        this.instrUrbanos = instrUrbanos;
    }

    public AnpFedZonanucleo getAnpFedZonanucleo() {
        return anpFedZonanucleo;
    }

    public void setAnpFedZonanucleo(AnpFedZonanucleo anpFedZonanucleo) {
        this.anpFedZonanucleo = anpFedZonanucleo;
    }

    public SitiosRamsar getSitiosRamsar() {
        return sitiosRamsar;
    }

    public void setSitiosRamsar(SitiosRamsar sitiosRamsar) {
        this.sitiosRamsar = sitiosRamsar;
    }

//    public OeRegionales1 getOeRegionales1() {
//        return oeRegionales1;
//    }
//
//    public void setOeRegionales1(OeRegionales1 oeRegionales1) {
//        this.oeRegionales1 = oeRegionales1;
//    }

    public Uma getUma() {
        return uma;
    }

    public void setUma(Uma uma) {
        this.uma = uma;
    }

    public AnpFedZonificacion getAnpFedZonificacion() {
        return anpFedZonificacion;
    }

    public void setAnpFedZonificacion(AnpFedZonificacion anpFedZonificacion) {
        this.anpFedZonificacion = anpFedZonificacion;
    }

    public RegMarinasPriori getRegMarinasPriori() {
        return regMarinasPriori;
    }

    public void setRegMarinasPriori(RegMarinasPriori regMarinasPriori) {
        this.regMarinasPriori = regMarinasPriori;
    }

    public OeMarinos getOeMarinos() {
        return oeMarinos;
    }

    public void setOeMarinos(OeMarinos oeMarinos) {
        this.oeMarinos = oeMarinos;
    }

    public Ogmsig getOgmsig() {
        return ogmsig;
    }

    public void setOgmsig(Ogmsig ogmsig) {
        this.ogmsig = ogmsig;
    }

    public RegTerrPriori getRegTerrPriori() {
        return regTerrPriori;
    }

    public void setRegTerrPriori(RegTerrPriori regTerrPriori) {
        this.regTerrPriori = regTerrPriori;
    }

    public LocalidIndigenas getLocalidIndigenas() {
        return localidIndigenas;
    }

    public void setLocalidIndigenas(LocalidIndigenas localidIndigenas) {
        this.localidIndigenas = localidIndigenas;
    }

    public SitiosContaminados getSitiosContaminados() {
        return sitiosContaminados;
    }

    public void setSitiosContaminados(SitiosContaminados sitiosContaminados) {
        this.sitiosContaminados = sitiosContaminados;
    }

    public OePoligEnvol getOePoligEnvol() {
        return oePoligEnvol;
    }

    public void setOePoligEnvol(OePoligEnvol oePoligEnvol) {
        this.oePoligEnvol = oePoligEnvol;
    }

    public OeGralTerrit getOeGralTerrit() {
        return oeGralTerrit;
    }

    public void setOeGralTerrit(OeGralTerrit oeGralTerrit) {
        this.oeGralTerrit = oeGralTerrit;
    }

    public AnpMuicipal getAnpMuicipal() {
        return anpMuicipal;
    }

    public void setAnpMuicipal(AnpMuicipal anpMuicipal) {
        this.anpMuicipal = anpMuicipal;
    }

    public SigeiaBitacora getSigeiaBitacora() {
        return sigeiaBitacora;
    }

    public void setSigeiaBitacora(SigeiaBitacora sigeiaBitacora) {
        this.sigeiaBitacora = sigeiaBitacora;
    }

    public Manglares getManglares() {
        return manglares;
    }

    public void setManglares(Manglares manglares) {
        this.manglares = manglares;
    }

    public UsoSueloVeget getUsoSueloVeget() {
        return usoSueloVeget;
    }

    public void setUsoSueloVeget(UsoSueloVeget usoSueloVeget) {
        this.usoSueloVeget = usoSueloVeget;
    }

    //public MpiosCruzadavshambre getMpiosCruzadavshambre() {
      //  return mpiosCruzadavshambre;
    //}

   // public void setMpiosCruzadavshambre(MpiosCruzadavshambre mpiosCruzadavshambre) {
   //     this.mpiosCruzadavshambre = mpiosCruzadavshambre;
    //}

    //public Proysig getProysig() {
      //  return proysig;
    //}

    //public void setProysig(Proysig proysig) {
      //  this.proysig = proysig;
   // }

    public ModSig getModSig() {
        return modSig;
    }

    public void setModSig(ModSig modSig) {
        this.modSig = modSig;
    }

    public AprovechForestales getAprovechForestales() {
        return aprovechForestales;
    }

    public void setAprovechForestales(AprovechForestales aprovechForestales) {
        this.aprovechForestales = aprovechForestales;
    }

    public Climas getClimas() {
        return climas;
    }

    public void setClimas(Climas climas) {
        this.climas = climas;
    }

    public DistritoRiego getDistritoRiego() {
        return distritoRiego;
    }

    public void setDistritoRiego(DistritoRiego distritoRiego) {
        this.distritoRiego = distritoRiego;
    }

//    public OeRegionales3 getOeRegionales3() {
//        return oeRegionales3;
//    }
//
//    public void setOeRegionales3(OeRegionales3 oeRegionales3) {
//        this.oeRegionales3 = oeRegionales3;
//    }

//    public OeRegionales2 getOeRegionales2() {
//        return oeRegionales2;
//    }
//
//    public void setOeRegionales2(OeRegionales2 oeRegionales2) {
//        this.oeRegionales2 = oeRegionales2;
//    }

    public RegHidrPriori getRegHidrPriori() {
        return regHidrPriori;
    }

    public void setRegHidrPriori(RegHidrPriori regHidrPriori) {
        this.regHidrPriori = regHidrPriori;
    }

    public OrdEcologLoc getOrdEcologLoc() {
        return ordEcologLoc;
    }

    public void setOrdEcologLoc(OrdEcologLoc ordEcologLoc) {
        this.ordEcologLoc = ordEcologLoc;
    }

    public ExcaviSig getExcaviSig() {
        return excaviSig;
    }

    public void setExcaviSig(ExcaviSig excaviSig) {
        this.excaviSig = excaviSig;
    }

    public EntidadFederativa getEntidadFederativa() {
        return entidadFederativa;
    }

    public void setEntidadFederativa(EntidadFederativa entidadFederativa) {
        this.entidadFederativa = entidadFederativa;
    }

    public Humedales getHumedales() {
        return humedales;
    }

    public void setHumedales(Humedales humedales) {
        this.humedales = humedales;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sigeiaAnalisisPK != null ? sigeiaAnalisisPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SigeiaAnalisis)) {
            return false;
        }
        SigeiaAnalisis other = (SigeiaAnalisis) object;
        if ((this.sigeiaAnalisisPK == null && other.sigeiaAnalisisPK != null) || (this.sigeiaAnalisisPK != null && !this.sigeiaAnalisisPK.equals(other.sigeiaAnalisisPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.SigeiaAnalisis[ sigeiaAnalisisPK=" + sigeiaAnalisisPK + " ]";
    }
    
}
