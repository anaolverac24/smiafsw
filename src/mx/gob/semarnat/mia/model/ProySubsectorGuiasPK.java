package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the PROY_SUBSECTOR_GUIAS database table.
 * 
 */
@Embeddable
public class ProySubsectorGuiasPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="SUBSECTOR_ID")
	private long subsectorId;

	@Column(name="TRAMITE_CVE")
	private String tramiteCve;

	@Column(name="URL_GUIA")
	private String urlGuia;

	public ProySubsectorGuiasPK() {
	}
	public long getSubsectorId() {
		return this.subsectorId;
	}
	public void setSubsectorId(long subsectorId) {
		this.subsectorId = subsectorId;
	}
	public String getTramiteCve() {
		return this.tramiteCve;
	}
	public void setTramiteCve(String tramiteCve) {
		this.tramiteCve = tramiteCve;
	}
	public String getUrlGuia() {
		return this.urlGuia;
	}
	public void setUrlGuia(String urlGuia) {
		this.urlGuia = urlGuia;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ProySubsectorGuiasPK)) {
			return false;
		}
		ProySubsectorGuiasPK castOther = (ProySubsectorGuiasPK)other;
		return 
			(this.subsectorId == castOther.subsectorId)
			&& this.tramiteCve.equals(castOther.tramiteCve)
			&& this.urlGuia.equals(castOther.urlGuia);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.subsectorId ^ (this.subsectorId >>> 32)));
		hash = hash * prime + this.tramiteCve.hashCode();
		hash = hash * prime + this.urlGuia.hashCode();
		
		return hash;
	}
}