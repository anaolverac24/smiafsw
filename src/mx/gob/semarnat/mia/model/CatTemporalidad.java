/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_TEMPORALIDAD")
@XmlRootElement
public class CatTemporalidad implements Serializable {
    @OneToMany(mappedBy = "temporalidadId")
    private List<PrediocolinProy> prediocolinProyList;

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "TEMPORALIDAD_ID")
    private Short temporalidadId;
    @Column(name = "TEMPORALIDAD_DESCRIPCION")
    private String temporalidadDescripcion;

    public CatTemporalidad() {
    }

    public CatTemporalidad(Short temporalidadId) {
        this.temporalidadId = temporalidadId;
    }

    public Short getTemporalidadId() {
        return temporalidadId;
    }

    public void setTemporalidadId(Short temporalidadId) {
        this.temporalidadId = temporalidadId;
    }

    public String getTemporalidadDescripcion() {
        return temporalidadDescripcion;
    }

    public void setTemporalidadDescripcion(String temporalidadDescripcion) {
        this.temporalidadDescripcion = temporalidadDescripcion;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (temporalidadId != null ? temporalidadId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatTemporalidad)) {
            return false;
        }
        CatTemporalidad other = (CatTemporalidad) object;
        if ((this.temporalidadId == null && other.temporalidadId != null) || (this.temporalidadId != null && !this.temporalidadId.equals(other.temporalidadId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatTemporalidad[ temporalidadId=" + temporalidadId + " ]";
    }
    
    @XmlTransient
    public List<PrediocolinProy> getPrediocolinProyList() {
        return prediocolinProyList;
    }

    public void setPrediocolinProyList(List<PrediocolinProy> prediocolinProyList) {
        this.prediocolinProyList = prediocolinProyList;
    }
    
}
