/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "EXCAVI_PTS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExcaviPts.findAll", query = "SELECT e FROM ExcaviPts e")})
public class ExcaviPts implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "OBJECTID")
    private BigInteger objectid;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "RESOLUCION")
    private String resolucion;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_RES")
    private Integer idRes;
    @Column(name = "SHAPE")
    private BigInteger shape;
    @Column(name = "PROY")
    private String proy;
    @OneToMany(mappedBy = "excaviPtsIdRes")
    private List<ExcaviSig> excaviSigList;

    public ExcaviPts() {
    }

    public ExcaviPts(Integer idRes) {
        this.idRes = idRes;
    }

    public ExcaviPts(Integer idRes, BigInteger objectid) {
        this.idRes = idRes;
        this.objectid = objectid;
    }

    public BigInteger getObjectid() {
        return objectid;
    }

    public void setObjectid(BigInteger objectid) {
        this.objectid = objectid;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public String getResolucion() {
        return resolucion;
    }

    public void setResolucion(String resolucion) {
        this.resolucion = resolucion;
    }

    public Integer getIdRes() {
        return idRes;
    }

    public void setIdRes(Integer idRes) {
        this.idRes = idRes;
    }

    public BigInteger getShape() {
        return shape;
    }

    public void setShape(BigInteger shape) {
        this.shape = shape;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    @XmlTransient
    public List<ExcaviSig> getExcaviSigList() {
        return excaviSigList;
    }

    public void setExcaviSigList(List<ExcaviSig> excaviSigList) {
        this.excaviSigList = excaviSigList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRes != null ? idRes.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExcaviPts)) {
            return false;
        }
        ExcaviPts other = (ExcaviPts) object;
        if ((this.idRes == null && other.idRes != null) || (this.idRes != null && !this.idRes.equals(other.idRes))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.ExcaviPts[ idRes=" + idRes + " ]";
    }
    
}
