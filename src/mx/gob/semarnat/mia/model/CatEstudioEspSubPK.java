/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author mauricio
 */
@Embeddable
public class CatEstudioEspSubPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "ESTUDIO_ID")
    private short estudioId;
    @Basic(optional = false)
    @Column(name = "NSUB")
    private short nsub;

    public CatEstudioEspSubPK() {
    }

    public CatEstudioEspSubPK(short estudioId, short nsub) {
        this.estudioId = estudioId;
        this.nsub = nsub;
    }

    public short getEstudioId() {
        return estudioId;
    }

    public void setEstudioId(short estudioId) {
        this.estudioId = estudioId;
    }

    public short getNsub() {
        return nsub;
    }

    public void setNsub(short nsub) {
        this.nsub = nsub;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) estudioId;
        hash += (int) nsub;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatEstudioEspSubPK)) {
            return false;
        }
        CatEstudioEspSubPK other = (CatEstudioEspSubPK) object;
        if (this.estudioId != other.estudioId) {
            return false;
        }
        if (this.nsub != other.nsub) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatEstudioEspSubPK[ estudioId=" + estudioId + ", nsub=" + nsub + " ]";
    }
    
}
