/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Humberto
 */
@Embeddable
public class CatApartadoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "ID_TRAMITE")
    private int idTramite;
    @Basic(optional = false)
    @Column(name = "NSUB")
    private short nsub;
    @Basic(optional = false)
    @Column(name = "CAPITULO_ID")
    private short capituloId;
    @Basic(optional = false)
    @Column(name = "SUBCAPITULO_ID")
    private short subcapituloId;
    @Basic(optional = false)
    @Column(name = "SECCION_ID")
    private short seccionId;
    @Basic(optional = false)
    @Column(name = "APARTADO_ID")
    private short apartadoId;

    public CatApartadoPK() {
    }

    public CatApartadoPK(int idTramite, short nsub, short capituloId, short subcapituloId, short seccionId, short apartadoId) {
        this.idTramite = idTramite;
        this.nsub = nsub;
        this.capituloId = capituloId;
        this.subcapituloId = subcapituloId;
        this.seccionId = seccionId;
        this.apartadoId = apartadoId;
    }

    public int getIdTramite() {
        return idTramite;
    }

    public void setIdTramite(int idTramite) {
        this.idTramite = idTramite;
    }

    public short getNsub() {
        return nsub;
    }

    public void setNsub(short nsub) {
        this.nsub = nsub;
    }

    public short getCapituloId() {
        return capituloId;
    }

    public void setCapituloId(short capituloId) {
        this.capituloId = capituloId;
    }

    public short getSubcapituloId() {
        return subcapituloId;
    }

    public void setSubcapituloId(short subcapituloId) {
        this.subcapituloId = subcapituloId;
    }

    public short getSeccionId() {
        return seccionId;
    }

    public void setSeccionId(short seccionId) {
        this.seccionId = seccionId;
    }

    public short getApartadoId() {
        return apartadoId;
    }

    public void setApartadoId(short apartadoId) {
        this.apartadoId = apartadoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idTramite;
        hash += (int) nsub;
        hash += (int) capituloId;
        hash += (int) subcapituloId;
        hash += (int) seccionId;
        hash += (int) apartadoId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatApartadoPK)) {
            return false;
        }
        CatApartadoPK other = (CatApartadoPK) object;
        if (this.idTramite != other.idTramite) {
            return false;
        }
        if (this.nsub != other.nsub) {
            return false;
        }
        if (this.capituloId != other.capituloId) {
            return false;
        }
        if (this.subcapituloId != other.subcapituloId) {
            return false;
        }
        if (this.seccionId != other.seccionId) {
            return false;
        }
        if (this.apartadoId != other.apartadoId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatApartadoPK[ idTramite=" + idTramite + ", nsub=" + nsub + ", capituloId=" + capituloId + ", subcapituloId=" + subcapituloId + ", seccionId=" + seccionId + ", apartadoId=" + apartadoId + " ]";
    }
    
}
