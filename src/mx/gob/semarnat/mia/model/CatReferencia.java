/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CAT_REFERENCIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatReferencia.findAll", query = "SELECT c FROM CatReferencia c"),
    @NamedQuery(name = "CatReferencia.findByReferenciaId", query = "SELECT c FROM CatReferencia c WHERE c.referenciaId = :referenciaId"),
    @NamedQuery(name = "CatReferencia.findByReferenciaDescripcion", query = "SELECT c FROM CatReferencia c WHERE c.referenciaDescripcion = :referenciaDescripcion")})
public class CatReferencia implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "referenciaId")
    private List<PrediocolinProy> prediocolinProyList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REFERENCIA_ID")
    private Short referenciaId;
    @Column(name = "REFERENCIA_DESCRIPCION")
    private String referenciaDescripcion;

    public CatReferencia() {
    }

    public CatReferencia(Short referenciaId) {
        this.referenciaId = referenciaId;
    }

    public Short getReferenciaId() {
        return referenciaId;
    }

    public void setReferenciaId(Short referenciaId) {
        this.referenciaId = referenciaId;
    }

    public String getReferenciaDescripcion() {
        return referenciaDescripcion;
    }

    public void setReferenciaDescripcion(String referenciaDescripcion) {
        this.referenciaDescripcion = referenciaDescripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (referenciaId != null ? referenciaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatReferencia)) {
            return false;
        }
        CatReferencia other = (CatReferencia) object;
        if ((this.referenciaId == null && other.referenciaId != null) || (this.referenciaId != null && !this.referenciaId.equals(other.referenciaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatReferencia[ referenciaId=" + referenciaId + " ]";
    }

    @XmlTransient
    public List<PrediocolinProy> getPrediocolinProyList() {
        return prediocolinProyList;
    }

    public void setPrediocolinProyList(List<PrediocolinProy> prediocolinProyList) {
        this.prediocolinProyList = prediocolinProyList;
    }
    
}
