/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model.sinatUsdb;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author eescalona
 */
@Entity
@Table(name = "SINAT_USDB.SINAT_SINATEC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SinatSinatec.findAll", query = "SELECT c FROM SinatSinatec c"),
    @NamedQuery(name = "SinatSinatec.findByFolio", query = "SELECT c FROM SinatSinatec c WHERE c.folio = :folio"),
    @NamedQuery(name = "SinatSinatec.findByBitacora", query = "SELECT c FROM SinatSinatec c WHERE c.bitacora = :bitacora"),
    @NamedQuery(name = "SinatSinatec.findByProyecto", query = "SELECT c FROM SinatSinatec c WHERE c.proyecto = :proyecto"),
    @NamedQuery(name = "SinatSinatec.findByUrlConstancia", query = "SELECT c FROM SinatSinatec c WHERE c.urlConstancia = :urlConstancia"),
    @NamedQuery(name = "SinatSinatec.findByMensaje", query = "SELECT c FROM SinatSinatec c WHERE c.mensaje = :mensaje")
})

public class SinatSinatec implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Id
    @Column(name = "FOLIO")
    private Integer folio;
    @Basic(optional = false)
    @Column(name = "BITACORA")
    private String bitacora;
    @Column(name = "PROYECTO")
    private String proyecto;
    @Column(name = "URL_CONSTANCIA")
    private String urlConstancia;
    @Column(name = "MENSAJE")
    private String mensaje;
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    public SinatSinatec() {
    }

    public SinatSinatec(Integer folio, String bitacora, String proyecto, String urlConstancia, String mensaje, Date fecha) {
        this.folio = folio;
        this.bitacora = bitacora;
        this.proyecto = proyecto;
        this.urlConstancia = urlConstancia;
        this.mensaje = mensaje;
        this.fecha = fecha;
    }



    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SinatSinatec)) {
            return false;
        }
        SinatSinatec other = (SinatSinatec) object;
        if ( this.folio == null && other.folio != null && !this.folio.equals(other.folio) ) {
            return false;
        }
        return true;
    }

    /*
    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.sinat_usdb.SinatSinatec[ " + comentarioProyectoPK + " ]";
    }
    */

    public Integer getFolio() {
        return folio;
    }

    public void setFolio(Integer folio) {
        this.folio = folio;
    }

    public String getBitacora() {
        return bitacora;
    }

    public void setBitacora(String bitacora) {
        this.bitacora = bitacora;
    }

    public String getProyecto() {
        return proyecto;
    }

    public void setProyecto(String proyecto) {
        this.proyecto = proyecto;
    }

    public String getUrlConstancia() {
        return urlConstancia;
    }

    public void setUrlConstancia(String urlConstancia) {
        this.urlConstancia = urlConstancia;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
}
