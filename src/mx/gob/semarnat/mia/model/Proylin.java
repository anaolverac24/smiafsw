/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "PROYLIN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proylin.findAll", query = "SELECT p FROM Proylin p")})
public class Proylin implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "OBJECTID")
    private BigInteger objectid;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "RESOLUCION")
    private String resolucion;
    @Column(name = "ID_RES")
    private Integer idRes;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "OBJECTID_1")
    private Long objectid1;
    @Column(name = "OBJECTID_2")
    private Long objectid2;
    @Column(name = "ROWLAYER")
    private Long rowlayer;
    @Column(name = "SHAPE")
    private BigInteger shape;
    @Column(name = "SHAPE_LEN")
    private BigInteger shapeLen;
//    @OneToMany(mappedBy = "proylinId")
//    private List<Proysig> proysigList;

    public Proylin() {
    }

    public Proylin(Long id) {
        this.id = id;
    }

    public Proylin(Long id, BigInteger objectid) {
        this.id = id;
        this.objectid = objectid;
    }

    public BigInteger getObjectid() {
        return objectid;
    }

    public void setObjectid(BigInteger objectid) {
        this.objectid = objectid;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getResolucion() {
        return resolucion;
    }

    public void setResolucion(String resolucion) {
        this.resolucion = resolucion;
    }

    public Integer getIdRes() {
        return idRes;
    }

    public void setIdRes(Integer idRes) {
        this.idRes = idRes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getObjectid1() {
        return objectid1;
    }

    public void setObjectid1(Long objectid1) {
        this.objectid1 = objectid1;
    }

    public Long getObjectid2() {
        return objectid2;
    }

    public void setObjectid2(Long objectid2) {
        this.objectid2 = objectid2;
    }

    public Long getRowlayer() {
        return rowlayer;
    }

    public void setRowlayer(Long rowlayer) {
        this.rowlayer = rowlayer;
    }

    public BigInteger getShape() {
        return shape;
    }

    public void setShape(BigInteger shape) {
        this.shape = shape;
    }

    public BigInteger getShapeLen() {
        return shapeLen;
    }

    public void setShapeLen(BigInteger shapeLen) {
        this.shapeLen = shapeLen;
    }

//    @XmlTransient
//    public List<Proysig> getProysigList() {
//        return proysigList;
//    }
//
//    public void setProysigList(List<Proysig> proysigList) {
//        this.proysigList = proysigList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proylin)) {
            return false;
        }
        Proylin other = (Proylin) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.Proylin[ id=" + id + " ]";
    }
    
}
