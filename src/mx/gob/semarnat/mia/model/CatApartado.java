/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Humberto
 */
@Entity
@Table(name = "CAT_APARTADO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatApartado.findAll", query = "SELECT c FROM CatApartado c"),
    @NamedQuery(name = "CatApartado.findByIdTramite", query = "SELECT c FROM CatApartado c WHERE c.catApartadoPK.idTramite = :idTramite"),
    @NamedQuery(name = "CatApartado.findByNsub", query = "SELECT c FROM CatApartado c WHERE c.catApartadoPK.nsub = :nsub"),
    @NamedQuery(name = "CatApartado.findByCapituloId", query = "SELECT c FROM CatApartado c WHERE c.catApartadoPK.capituloId = :capituloId"),
    @NamedQuery(name = "CatApartado.findBySubcapituloId", query = "SELECT c FROM CatApartado c WHERE c.catApartadoPK.subcapituloId = :subcapituloId"),
    @NamedQuery(name = "CatApartado.findBySeccionId", query = "SELECT c FROM CatApartado c WHERE c.catApartadoPK.seccionId = :seccionId"),
    @NamedQuery(name = "CatApartado.findByApartadoId", query = "SELECT c FROM CatApartado c WHERE c.catApartadoPK.apartadoId = :apartadoId"),
    @NamedQuery(name = "CatApartado.findByApartadoDescripcion", query = "SELECT c FROM CatApartado c WHERE c.apartadoDescripcion = :apartadoDescripcion"),
    @NamedQuery(name = "CatApartado.findByApartadoAyuda", query = "SELECT c FROM CatApartado c WHERE c.apartadoAyuda = :apartadoAyuda")})
public class CatApartado implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CatApartadoPK catApartadoPK;
    @Column(name = "APARTADO_DESCRIPCION")
    private String apartadoDescripcion;
    @Column(name = "APARTADO_AYUDA")
    private String apartadoAyuda;
    @JoinColumns({
        @JoinColumn(name = "ID_TRAMITE", referencedColumnName = "ID_TRAMITE", insertable = false, updatable = false),
        @JoinColumn(name = "NSUB", referencedColumnName = "NSUB", insertable = false, updatable = false),
        @JoinColumn(name = "SECCION_ID", referencedColumnName = "SECCION_ID", insertable = false, updatable = false),
        @JoinColumn(name = "CAPITULO_ID", referencedColumnName = "CAPITULO_ID", insertable = false, updatable = false),
        @JoinColumn(name = "SUBCAPITULO_ID", referencedColumnName = "SUBCAPITULO_ID", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private CatSeccion catSeccion;

    public CatApartado() {
    }

    public CatApartado(CatApartadoPK catApartadoPK) {
        this.catApartadoPK = catApartadoPK;
    }

    public CatApartado(int idTramite, short nsub, short capituloId, short subcapituloId, short seccionId, short apartadoId) {
        this.catApartadoPK = new CatApartadoPK(idTramite, nsub, capituloId, subcapituloId, seccionId, apartadoId);
    }

    public CatApartadoPK getCatApartadoPK() {
        return catApartadoPK;
    }

    public void setCatApartadoPK(CatApartadoPK catApartadoPK) {
        this.catApartadoPK = catApartadoPK;
    }

    public String getApartadoDescripcion() {
        return apartadoDescripcion;
    }

    public void setApartadoDescripcion(String apartadoDescripcion) {
        this.apartadoDescripcion = apartadoDescripcion;
    }

    public String getApartadoAyuda() {
        return apartadoAyuda;
    }

    public void setApartadoAyuda(String apartadoAyuda) {
        this.apartadoAyuda = apartadoAyuda;
    }

    public CatSeccion getCatSeccion() {
        return catSeccion;
    }

    public void setCatSeccion(CatSeccion catSeccion) {
        this.catSeccion = catSeccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catApartadoPK != null ? catApartadoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatApartado)) {
            return false;
        }
        CatApartado other = (CatApartado) object;
        if ((this.catApartadoPK == null && other.catApartadoPK != null) || (this.catApartadoPK != null && !this.catApartadoPK.equals(other.catApartadoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatApartado[ catApartadoPK=" + catApartadoPK + " ]";
    }
    
}
