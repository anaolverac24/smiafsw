/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "CRITERIOS_PROYECTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CriteriosProyecto.findAll", query = "SELECT c FROM CriteriosProyecto c"),
    @NamedQuery(name = "CriteriosProyecto.findByFolioProyecto", query = "SELECT c FROM CriteriosProyecto c WHERE c.criteriosProyectoPK.folioProyecto = :folioProyecto"),
    @NamedQuery(name = "CriteriosProyecto.findBySerialProyecto", query = "SELECT c FROM CriteriosProyecto c WHERE c.criteriosProyectoPK.serialProyecto = :serialProyecto"),
    @NamedQuery(name = "CriteriosProyecto.findByCriterioProyId", query = "SELECT c FROM CriteriosProyecto c WHERE c.criteriosProyectoPK.criterioProyId = :criterioProyId"),
    @NamedQuery(name = "CriteriosProyecto.findByCriterioDescripcion", query = "SELECT c FROM CriteriosProyecto c WHERE c.criterioDescripcion = :criterioDescripcion")})
public class CriteriosProyecto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CriteriosProyectoPK criteriosProyectoPK;
    @Column(name = "CRITERIO")
    private String criterio;
    @Column(name = "CRITERIO_DESCRIPCION")
    private String criterioDescripcion;
    @JoinColumn(name = "CRITERIO_ID", referencedColumnName = "CRITERIO_ID")
    @ManyToOne(optional = false)
    private CatCriterio criterioId;
    @JoinColumns({
        @JoinColumn(name = "FOLIO_PROYECTO", referencedColumnName = "FOLIO_PROYECTO", insertable = false, updatable = false),
        @JoinColumn(name = "SERIAL_PROYECTO", referencedColumnName = "SERIAL_PROYECTO", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Proyecto proyecto;

    public CriteriosProyecto() {
    }

    public CriteriosProyecto(CriteriosProyectoPK criteriosProyectoPK) {
        this.criteriosProyectoPK = criteriosProyectoPK;
    }

    public CriteriosProyecto(String folioProyecto, short serialProyecto, short criterioProyId) {
        this.criteriosProyectoPK = new CriteriosProyectoPK(folioProyecto, serialProyecto, criterioProyId);
    }
    
    public CriteriosProyecto(CatCriterio criterioId, String criterio, String criterioDescripcion) {
        this.criterioId = criterioId;
        this.criterio = criterio;
        this.criterioDescripcion = criterioDescripcion;
    }
    
    public CriteriosProyecto(String folioProyecto, short serialProyecto, short criterioProyId, CatCriterio criterioId, String criterio, String criterioDescripcion) {
        this.criteriosProyectoPK = new CriteriosProyectoPK(folioProyecto, serialProyecto, criterioProyId);
        this.criterioId = criterioId;
        this.criterio = criterio;
        this.criterioDescripcion = criterioDescripcion;
    }

    public CriteriosProyectoPK getCriteriosProyectoPK() {
        return criteriosProyectoPK;
    }

    public void setCriteriosProyectoPK(CriteriosProyectoPK criteriosProyectoPK) {
        this.criteriosProyectoPK = criteriosProyectoPK;
    }
    
    public String getCriterio() {
        return criterio;
    }

    public void setCriterio(String criterio) {
        this.criterio = criterio;
    }

    public String getCriterioDescripcion() {
        return criterioDescripcion;
    }

    public void setCriterioDescripcion(String criterioDescripcion) {
        this.criterioDescripcion = criterioDescripcion;
    }
    
    public String getCriterioDescripcionCorta(){
        if(criterioDescripcion.length()<199){
            return criterioDescripcion;
        }
        return criterioDescripcion.substring(0, 199);
    }

    public CatCriterio getCriterioId() {
        return criterioId;
    }

    public void setCriterioId(CatCriterio criterioId) {
        this.criterioId = criterioId;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (criteriosProyectoPK != null ? criteriosProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CriteriosProyecto)) {
            return false;
        }
        CriteriosProyecto other = (CriteriosProyecto) object;
        if ((this.criteriosProyectoPK == null && other.criteriosProyectoPK != null) || (this.criteriosProyectoPK != null && !this.criteriosProyectoPK.equals(other.criteriosProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CriteriosProyecto[ criteriosProyectoPK=" + criteriosProyectoPK + " ]";
    }
    
}
