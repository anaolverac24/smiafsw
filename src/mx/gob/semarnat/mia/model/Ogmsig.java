/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "OGMSIG")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ogmsig.findAll", query = "SELECT o FROM Ogmsig o")})
public class Ogmsig implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OgmsigPK ogmsigPK;
    @Column(name = "PROY")
    private String proy;
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "COMP")
    private String comp;
    @Column(name = "NUM_SOL")
    private String numSol;
    @Column(name = "PROMOV")
    private String promov;
    @Column(name = "TIPO_SEM")
    private String tipoSem;
    @Column(name = "ETAPA_DE_LIBERACION")
    private String etapaDeLiberacion;
    @Column(name = "DICTAMEN")
    private String dictamen;
    @Column(name = "CVE_SEM")
    private String cveSem;
    @Column(name = "SHAPE")
    private BigInteger shape;
    @Column(name = "SHAPE_AREA")
    private BigInteger shapeArea;
    @Column(name = "SHAPE_LEN")
    private BigInteger shapeLen;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ogmsig")
    private List<SigeiaAnalisis> sigeiaAnalisisList;
    @JoinColumn(name = "OGMLIN_OBJECTID", referencedColumnName = "OBJECTID")
    @ManyToOne
    private Ogmlin ogmlinObjectid;
    @JoinColumn(name = "OGMMPUN_OBJECTID", referencedColumnName = "OBJECTID")
    @ManyToOne
    private Ogmmpun ogmmpunObjectid;
    @JoinColumn(name = "OGMPOL_OBJECTID", referencedColumnName = "OBJECTID")
    @ManyToOne
    private Ogmpol ogmpolObjectid;

    public Ogmsig() {
    }

    public Ogmsig(OgmsigPK ogmsigPK) {
        this.ogmsigPK = ogmsigPK;
    }

    public Ogmsig(BigInteger objectid1, String numFolio, String cveProy, String cveArea, short version, long objectid) {
        this.ogmsigPK = new OgmsigPK(objectid1, numFolio, cveProy, cveArea, version, objectid);
    }

    public OgmsigPK getOgmsigPK() {
        return ogmsigPK;
    }

    public void setOgmsigPK(OgmsigPK ogmsigPK) {
        this.ogmsigPK = ogmsigPK;
    }

    public String getProy() {
        return proy;
    }

    public void setProy(String proy) {
        this.proy = proy;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getNumSol() {
        return numSol;
    }

    public void setNumSol(String numSol) {
        this.numSol = numSol;
    }

    public String getPromov() {
        return promov;
    }

    public void setPromov(String promov) {
        this.promov = promov;
    }

    public String getTipoSem() {
        return tipoSem;
    }

    public void setTipoSem(String tipoSem) {
        this.tipoSem = tipoSem;
    }

    public String getEtapaDeLiberacion() {
        return etapaDeLiberacion;
    }

    public void setEtapaDeLiberacion(String etapaDeLiberacion) {
        this.etapaDeLiberacion = etapaDeLiberacion;
    }

    public String getDictamen() {
        return dictamen;
    }

    public void setDictamen(String dictamen) {
        this.dictamen = dictamen;
    }

    public String getCveSem() {
        return cveSem;
    }

    public void setCveSem(String cveSem) {
        this.cveSem = cveSem;
    }

    public BigInteger getShape() {
        return shape;
    }

    public void setShape(BigInteger shape) {
        this.shape = shape;
    }

    public BigInteger getShapeArea() {
        return shapeArea;
    }

    public void setShapeArea(BigInteger shapeArea) {
        this.shapeArea = shapeArea;
    }

    public BigInteger getShapeLen() {
        return shapeLen;
    }

    public void setShapeLen(BigInteger shapeLen) {
        this.shapeLen = shapeLen;
    }

    @XmlTransient
    public List<SigeiaAnalisis> getSigeiaAnalisisList() {
        return sigeiaAnalisisList;
    }

    public void setSigeiaAnalisisList(List<SigeiaAnalisis> sigeiaAnalisisList) {
        this.sigeiaAnalisisList = sigeiaAnalisisList;
    }

    public Ogmlin getOgmlinObjectid() {
        return ogmlinObjectid;
    }

    public void setOgmlinObjectid(Ogmlin ogmlinObjectid) {
        this.ogmlinObjectid = ogmlinObjectid;
    }

    public Ogmmpun getOgmmpunObjectid() {
        return ogmmpunObjectid;
    }

    public void setOgmmpunObjectid(Ogmmpun ogmmpunObjectid) {
        this.ogmmpunObjectid = ogmmpunObjectid;
    }

    public Ogmpol getOgmpolObjectid() {
        return ogmpolObjectid;
    }

    public void setOgmpolObjectid(Ogmpol ogmpolObjectid) {
        this.ogmpolObjectid = ogmpolObjectid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ogmsigPK != null ? ogmsigPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ogmsig)) {
            return false;
        }
        Ogmsig other = (Ogmsig) object;
        if ((this.ogmsigPK == null && other.ogmsigPK != null) || (this.ogmsigPK != null && !this.ogmsigPK.equals(other.ogmsigPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.Ogmsig[ ogmsigPK=" + ogmsigPK + " ]";
    }
    
}
