/*
 *
 */
package mx.gob.semarnat.mia.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Registro de errores
 *
 * @author Mauricio Solis
 */
public class Log {

    private static final String sql = "INSERT INTO DGIRA_MIAE.ERROR_APLICATIVO_PROYECTO (FOLIO_PROYECTO, SERIAL_PROYECTO, ERROR_FECHA_HORA, ERROR_DESCRIPCION) VALUES (?, ?, SYSDATE, ?)";

    /**
     * Registro de error
     *
     * @param folio folio proyecto
     * @param serial serial del proyecto
     * @param msg mensaje de errir
     */
    public static void error(String folio, short serial, String msg) {
        try {
            Connection con = DBConnectionFactory.getConnection();
            PreparedStatement ps = con.prepareCall(sql);
            ps.setString(1, folio);
            ps.setInt(2, serial);
            ps.setString(3, msg);
            ps.executeUpdate();
            con.close();
        } catch (SQLException sqe) {
            System.out.println("" + sqe);
            System.out.println("---------------------------");
            System.out.println("" + msg);
        }
    }
}
