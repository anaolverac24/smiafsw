/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.util;

import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import mx.gob.semarnat.mia.model.CatEtapa;
import mx.gob.semarnat.mia.model.InsumosProyecto;

/**
 *
 * @author mauricio
 */
public class InsumosProyectoU {

    //pk
    private String folioSerial;
    private short serialProyecto;
    private short etapaId;
    private short insumosId;
    //cols
    private String insumoNomCmoun;
    private String insumoNomTecnico;
    private String insumoEstadoFisico;
    private BigDecimal insumoCantAlm;
    private Short cveClve;
    private BigDecimal insumoCantMensual;
    private Short clveCantMensual;
    private CatEtapa catEtapa;

    public InsumosProyectoU() {
    }

    public InsumosProyectoU(String folioSerial, short serialProyecto, short etapaId, short insumosId) {
        this.folioSerial = folioSerial;
        this.serialProyecto = serialProyecto;
        this.etapaId = etapaId;
        this.insumosId = insumosId;
        this.catEtapa = new CatEtapa((short)1);
    }

    public InsumosProyectoU(InsumosProyecto insumo) {
        this.folioSerial = insumo.getInsumosProyectoPK().getFolioSerial();
        this.serialProyecto = insumo.getInsumosProyectoPK().getSerialProyecto();
        this.etapaId = insumo.getInsumosProyectoPK().getEtapaId();
        this.insumosId = insumo.getInsumosProyectoPK().getInsumosId();
        this.insumoNomCmoun = insumo.getInsumoNomCmoun();
        this.insumoNomTecnico = insumo.getInsumoNomTecnico();
        this.insumoEstadoFisico = insumo.getInsumoEstadoFisico();
        this.insumoCantAlm = insumo.getInsumoCantAlm();
        this.cveClve = insumo.getCveClve();
        this.insumoCantMensual = insumo.getInsumoCantMensual();
        this.clveCantMensual = insumo.getClveCantMensual();
        this.catEtapa = insumo.getCatEtapa();
    }

    public InsumosProyecto convert() {
        InsumosProyecto insumo = new InsumosProyecto(folioSerial, serialProyecto, etapaId, insumosId);
        insumo.setCatEtapa(catEtapa);
        insumo.setClveCantMensual(clveCantMensual);
        insumo.setCveClve(cveClve);
        insumo.setInsumoCantAlm(insumoCantAlm);
        insumo.setInsumoCantMensual(insumoCantMensual);
        insumo.setInsumoEstadoFisico(insumoEstadoFisico);
        insumo.setInsumoNomCmoun(insumoNomCmoun);
        insumo.setInsumoNomTecnico(insumoNomTecnico);
        return insumo;
    }

    @Override
    public String toString() {
        return "InsumosProyectoU{" + "folioSerial=" + folioSerial + ", serialProyecto=" + serialProyecto + ", etapaId=" + etapaId + ", insumosId=" + insumosId + ", insumoNomCmoun=" + insumoNomCmoun + ", insumoNomTecnico=" + insumoNomTecnico + ", insumoEstadoFisico=" + insumoEstadoFisico + ", insumoCantAlm=" + insumoCantAlm + ", cveClve=" + cveClve + ", insumoCantMensual=" + insumoCantMensual + ", clveCantMensual=" + clveCantMensual + ", catEtapa=" + catEtapa + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.folioSerial != null ? this.folioSerial.hashCode() : 0);
        hash = 37 * hash + this.serialProyecto;
        hash = 37 * hash + this.etapaId;
        hash = 37 * hash + this.insumosId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        InsumosProyectoU other = (InsumosProyectoU) obj;
//        if ((this.folioSerial == null) ? (other.folioSerial != null) : !this.folioSerial.equals(other.folioSerial)) {
//            return false;
//        }

        if (this.folioSerial.equals(other.folioSerial) && this.serialProyecto == other.serialProyecto && this.insumosId == other.insumosId) {
            return true;
        }

//        if (this.serialProyecto != other.serialProyecto) {
//            return false;
//        }
//        if (this.etapaId != other.etapaId) {
//            return false;
//        }
//        if (this.insumosId != other.insumosId) {
//            return false;
//        }
        return false;
    }

    /**
     * @param folioSerial the folioSerial to set
     */
    public void setFolioSerial(String folioSerial) {
        this.folioSerial = folioSerial;
    }

    /**
     * @param serialProyecto the serialProyecto to set
     */
    public void setSerialProyecto(short serialProyecto) {
        this.serialProyecto = serialProyecto;
    }

    /**
     * @return the etapaId
     */
    public short getEtapaId() {
        return etapaId;
    }

    /**
     * @param etapaId the etapaId to set
     */
    public void setEtapaId(short etapaId) {
        this.etapaId = etapaId;
    }

    /**
     * @return the insumosId
     */
    public short getInsumosId() {
        return insumosId;
    }

    /**
     * @param insumosId the insumosId to set
     */
    public void setInsumosId(short insumosId) {
        this.insumosId = insumosId;
    }

    /**
     * @return the insumoNomCmoun
     */
    public String getInsumoNomCmoun() {
        return insumoNomCmoun;
    }

    /**
     * @param insumoNomCmoun the insumoNomCmoun to set
     */
    public void setInsumoNomCmoun(String insumoNomCmoun) {
        this.insumoNomCmoun = insumoNomCmoun;
    }

    /**
     * @return the insumoNomTecnico
     */
    public String getInsumoNomTecnico() {
        return insumoNomTecnico;
    }

    /**
     * @param insumoNomTecnico the insumoNomTecnico to set
     */
    public void setInsumoNomTecnico(String insumoNomTecnico) {
        this.insumoNomTecnico = insumoNomTecnico;
    }

    /**
     * @return the insumoEstadoFisico
     */
    public String getInsumoEstadoFisico() {
        return insumoEstadoFisico;
    }

    /**
     * @param insumoEstadoFisico the insumoEstadoFisico to set
     */
    public void setInsumoEstadoFisico(String insumoEstadoFisico) {
        this.insumoEstadoFisico = insumoEstadoFisico;
    }

    /**
     * @return the insumoCantAlm
     */
    public BigDecimal getInsumoCantAlm() {
        return insumoCantAlm;
    }

    /**
     * @param insumoCantAlm the insumoCantAlm to set
     */
    public void setInsumoCantAlm(BigDecimal insumoCantAlm) {
        this.insumoCantAlm = insumoCantAlm;
    }

    /**
     * @return the cveClve
     */
    public Short getCveClve() {
        return cveClve;
    }

    /**
     * @param cveClve the cveClve to set
     */
    public void setCveClve(Short cveClve) {
        this.cveClve = cveClve;
    }

    /**
     * @return the insumoCantMensual
     */
    public BigDecimal getInsumoCantMensual() {
        return insumoCantMensual;
    }

    /**
     * @param insumoCantMensual the insumoCantMensual to set
     */
    public void setInsumoCantMensual(BigDecimal insumoCantMensual) {
        this.insumoCantMensual = insumoCantMensual;
    }

    /**
     * @return the clveCantMensual
     */
    public Short getClveCantMensual() {
        return clveCantMensual;
    }

    /**
     * @param clveCantMensual the clveCantMensual to set
     */
    public void setClveCantMensual(Short clveCantMensual) {
        this.clveCantMensual = clveCantMensual;
    }

    /**
     * @return the catEtapa
     */
    public CatEtapa getCatEtapa() {
        return catEtapa;
    }

    /**
     * @param catEtapa the catEtapa to set
     */
    public void setCatEtapa(CatEtapa catEtapa) {
        this.catEtapa = catEtapa;
    }
}
