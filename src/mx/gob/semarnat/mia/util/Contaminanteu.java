/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.util;

import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
import mx.gob.semarnat.mia.dao.EntityFactory;
import mx.gob.semarnat.mia.dao.MiaDao;
import mx.gob.semarnat.mia.model.CatContaminante;
import mx.gob.semarnat.mia.model.ContaminanteProyecto;

/**
 *
 * @author mauricio
 */
public class Contaminanteu {

    private final MiaDao miaDao = new MiaDao();
    private ContaminanteProyecto contaminante;
    private List<SelectItem> contaminanteCat = new ArrayList<SelectItem>();

    public Contaminanteu() {
    }
    
    public void selTipo() {
        contaminanteCat.clear();
        Short tipo = contaminante.getContaminanteTipo();
        List<CatContaminante>cCat = miaDao.getContaminantes(tipo);
        for (CatContaminante t :  cCat){
            SelectItem s = new SelectItem(t,t.getContaminanteNombre());
            contaminanteCat.add(s);
        }
    }

    /**
     * @return the contaminante
     */
    public ContaminanteProyecto getContaminante() {
        return contaminante;
    }

    /**
     * @param contaminante the contaminante to set
     */
    public void setContaminante(ContaminanteProyecto contaminante) {
        this.contaminante = contaminante;
//        System.out.println("contaminante.getTipoContaminante() " + contaminante.getTipoContaminante());
//        if (contaminante.getTipoContaminante() == null){
//            contaminante.setTipoContaminante((short)(1));
//        }
    }

    /**
     * @return the contaminanteCat
     */
    public List<SelectItem> getContaminanteCat() {
        return contaminanteCat;
    }

    /**
     * @param contaminanteCat the contaminanteCat to set
     */
    public void setContaminanteCat(List<SelectItem> contaminanteCat) {
        this.contaminanteCat = contaminanteCat;
    }
}
