/**
 * 
 */
package mx.gob.semarnat.mia.util;


/**
 * @author Cesar
 *
 */
public class ArchivoUtil {
	
	/**
	 * Arreglo de caracteres invalidos
	 */
	
	private static final String[] caracteres = {"/","!","@","#","$","%","^","&","*","(",")","|","\\",",",":",";","+","¿","{","}","°","¬","´","'","~","`","¡"};
	
	/**
	 * Limite de caracteres a 40 pero se reservan 5 caracteres para extensión de archivo
	 */
	private static final int limiteCaracteres = 35;

	/**
	 * Elimina los caracteres invalidos dentro de una cadena
	 * @param cadena
	 * @return cadena sin caracteres invalidos
	 */
	public static final String eliminarCaracteresInvalidos(String cadena) {
		boolean contieneCaracteres;
		
		do {
			contieneCaracteres = false;
			for (String caracter : caracteres) {
				if (cadena.contains(caracter)) {
					contieneCaracteres = true;
					cadena = cadena.replace(caracter, "");
					break;
				}
			}
		} while (contieneCaracteres);
		
		//Se reemplazan los espacios en blanco por guiones bajos
		cadena = cadena.replace(" ", "_");
		
		return cadena;
	}
	
	/**
	 * Elimina acentos en la cadena reemplazando caracter original por el ascii
	 * @param cadena
	 * @return cadenas sin acentos
	 */
	public static final String eliminarAcentos(String cadena) {
	    String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ"; 
	    String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
	    
	    for (int i=0; i<original.length(); i++) {	        
	    	cadena = cadena.replace(original.charAt(i), ascii.charAt(i));
	    }
	    
		return cadena;
	}
	
	/**
	 * Acorta una cadena en base al limite de caracteres
	 * @param cadena
	 * @return cadena acortada en base al limite de caracteres
	 */
	public static final String delimitarCadena(String cadena) {
		if (cadena.length() > limiteCaracteres) {
			cadena = cadena.substring(0, limiteCaracteres);
		}
		
		return cadena;
	}
	
	/**
	 * Adapta una cadena aplicando todos los metodos utiles
	 * @param cadena
	 * @return cadena adaptada
	 */
	public static final String adaptarReglaCadena(String cadena) {
		cadena = eliminarCaracteresInvalidos(cadena);
		cadena = eliminarAcentos(cadena);
		cadena = delimitarCadena(cadena);
		
		return cadena;
	}
}
