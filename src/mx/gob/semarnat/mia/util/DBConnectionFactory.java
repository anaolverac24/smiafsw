/*
 *
 */
package mx.gob.semarnat.mia.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import oracle.jdbc.pool.OracleDataSource;

/**
 * Conexion a la bd por jdbc
 * @author Mauricio Solis 
 */
public class DBConnectionFactory {

    private static OracleDataSource ods;

    static {
        try {
            Properties config = new Properties();
            config.load(DBConnectionFactory.class.getResourceAsStream("/mx/gob/semarnat/mia/Config.properties"));
            ods = new OracleDataSource();
            ods.setDriverType("thin");
            ods.setServerName(config.getProperty("serverName"));
            ods.setNetworkProtocol("tcp");
            //ods.setDatabaseName(config.getProperty("databaseName")); // Este se utiliza si se tiene SID
            ods.setServiceName(config.getProperty("databaseName")); // Este se utiliza si se tiene SERVICE NAME
            ods.setPortNumber(Integer.parseInt(config.getProperty("portNumber")));
            ods.setUser(config.getProperty("user"));
            ods.setPassword(config.getProperty("password"));
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     *
     * @return @throws SQLException
     */
    public static Connection getConnection() throws SQLException {
        Connection conn = ods.getConnection();
        return conn;
    }

}
