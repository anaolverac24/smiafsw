/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.util;

/**
 *
 * @author mauricio
 */
public class VProgramau {
private String regla;
private String vinculacion;

    /**
     * @return the regla
     */
    public String getRegla() {
        return regla;
    }

    /**
     * @param regla the regla to set
     */
    public void setRegla(String regla) {
        this.regla = regla;
    }

    /**
     * @return the vinculacion
     */
    public String getVinculacion() {
        return vinculacion;
    }

    /**
     * @param vinculacion the vinculacion to set
     */
    public void setVinculacion(String vinculacion) {
        this.vinculacion = vinculacion;
    }

}
