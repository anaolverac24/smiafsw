/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.util;

/**
 *
 * @author mauricio
 */
public class VDecretou {

    private String articulo;
    private String vinculacion;

    /**
     * @return the articulo
     */
    public String getArticulo() {
        return articulo;
    }

    /**
     * @param articulo the articulo to set
     */
    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    /**
     * @return the vinculacion
     */
    public String getVinculacion() {
        return vinculacion;
    }

    /**
     * @param vinculacion the vinculacion to set
     */
    public void setVinculacion(String vinculacion) {
        this.vinculacion = vinculacion;
    }
}
