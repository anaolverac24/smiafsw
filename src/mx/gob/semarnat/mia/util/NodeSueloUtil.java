/**
 * 
 */
package mx.gob.semarnat.mia.util;

import java.util.List;

import mx.gob.semarnat.mia.model.SueloVegetacionDetalle;
import mx.gob.semarnat.mia.model.SueloVegetacionProyecto;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 * @author Cesar
 *
 */
public class NodeSueloUtil {

	/**
	 * Convierte una lista de objeto SueloVegetacionProyecto a un objeto TreeNode
	 * @param listaSuelos
	 * @return arbol TreeNode
	 */
	public TreeNode generarNodos(List<SueloVegetacionProyecto> listaSuelos) {
		TreeNode root = new DefaultTreeNode(
				new NodeSueloVegetacion(0, "-", "-", "-", "-", "-", 0.0, "-"), null);
		
		//Iteracion de los suelos(Nodos Padre)
		for (SueloVegetacionProyecto sueloVegetacionProyecto : listaSuelos) {
			TreeNode nodeSuelo = 
				new DefaultTreeNode(new NodeSueloVegetacion(
						sueloVegetacionProyecto.getSueloVegetacionProyectoPK().getSueloVegProyId(),
						sueloVegetacionProyecto.getSueloComponente(),
						sueloVegetacionProyecto.getSueloTipoEcov(),
						sueloVegetacionProyecto.getSueloTipoGen(),
						sueloVegetacionProyecto.getSueloFaseVs(),
						sueloVegetacionProyecto.getSueloDescripcion(),
						sueloVegetacionProyecto.getSueloDiagnosticoCorto(),
						sueloVegetacionProyecto.getSueloAreaSigeia(),
						"Padre"), root);
			
			//Iteracion de los suelo detalle(Nodos hijos)
			for (SueloVegetacionDetalle sueloDetalle : sueloVegetacionProyecto.getListaDetalle()) {
				TreeNode nodeDetalle = 
						new DefaultTreeNode(new NodeSueloVegetacion(
							sueloDetalle.getIdSueloVegetacionDetalle(),
							"->",
							sueloDetalle.getVegGrupo().getVegNombre(),
							sueloDetalle.getVegTipo().getVegNombre(),
							sueloDetalle.getVegFase().getVegNombre(),
							"-",
							sueloDetalle.getDiagnosticoCorto(),
							sueloDetalle.getSuperficie(),
							"Hijo"), nodeSuelo);
			}
		}
		
		return root;
	}
}
