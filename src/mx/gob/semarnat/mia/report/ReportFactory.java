/*
 *  To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.report;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import mx.gob.semarnat.mia.util.DBConnectionFactory;
import mx.gob.semarnat.mia.view.SIGEIAView;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

/**
 *
 * @author mauricio
 */
public class ReportFactory {
	
    public static final Font TITULO1 = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.BLACK);
    public static final Font TITULO2 = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.BLACK);
    public static final Font TITULO3 = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.BLACK);
    public static final Font TITULO4 = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK);
    public static final Font TITULO_CARATULA = new Font(Font.FontFamily.HELVETICA, 20, Font.BOLD);
    public static final Font TITULO_CARATULA2 = new Font(Font.FontFamily.COURIER, 14, Font.NORMAL);

    public static final Font PARRAFO = new Font(Font.FontFamily.HELVETICA, 8);
    public static final Font PARRAFO_TITULO = new Font(Font.FontFamily.HELVETICA, 12);
    public static final Font PARRAFO_NEGRITA = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);
    public static final Font PARRAFO_SUB = new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE, BaseColor.BLACK);

    /**
     * EventListner for Content
     */
    private static class ContentEvent extends PdfPageEventHelper {

        private int page;
        Map<String, Integer> index = new LinkedHashMap<String, Integer>();

        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            page++;
        }

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            try {
                Image img = Image.getInstance(this.getClass().getResource("logoSEMARNAT_hoz.png"));
                img.setAbsolutePosition(400, 780);
                writer.getDirectContent().addImage(img);
            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onChapter(PdfWriter writer, Document document,
                float paragraphPosition, Paragraph title
        ) {
            index.put(title.getContent(), page);
        }

        @Override
        public void onSection(PdfWriter writer, Document document,
                float paragraphPosition, int depth, Paragraph title
        ) {
            onChapter(writer, document, paragraphPosition, title);
        }
    }

    /**
     * EventListner for Index
     */
    private static class IndexEvent extends PdfPageEventHelper {

        private final String[] datos;
        private int page;
        private boolean body;

        public IndexEvent(String[] datos) {
            this.datos = datos;
        }

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            try {
                Image img = Image.getInstance(ReportFactory.class.getResource("/mx/gob/semarnat/mia/report/logoSEMARNAT_hoz.png"));
                img.scalePercent(40f);
                img.setAbsolutePosition(10, 780);
                String t1 = "Subsecretaria de Gestión para la Protección Ambiental";
                String t2 = "Dirección General de Impacto y Riesgo Ambiental";

                writer.getDirectContent().addImage(img);
                writer.getDirectContent().beginText();
                writer.getDirectContent().setFontAndSize(BaseFont.createFont(), 12);
                writer.getDirectContent().showTextAligned(0, t1, 220, 825, 0);
                writer.getDirectContent().showTextAligned(0, t2, 235, 810, 0);
                writer.getDirectContent().setFontAndSize(BaseFont.createFont(), 9);
                String NombreProyecto = datos[0];

                int TamanoNombre = 0;
                if (NombreProyecto.length() < 80) {
                    TamanoNombre = NombreProyecto.length();
                } else {
                    TamanoNombre = 80;
                }

                writer.getDirectContent().showTextAligned(0, "Proyecto: " + NombreProyecto.substring(0, TamanoNombre), 210, 800, 0);
                if (NombreProyecto.length() > 80) {
                    writer.getDirectContent().showTextAligned(0, "               - " + NombreProyecto.substring(81, NombreProyecto.length()), 210, 790, 0);
                }

                writer.getDirectContent().showTextAligned(0, "Sector: " + datos[1], 210, 780, 0);
                writer.getDirectContent().showTextAligned(0, "Subsector: " + datos[2], 350, 780, 0);
                writer.getDirectContent().endText();
            } catch (Exception e) {
                System.out.println("Error en ruta de imagen");
                e.printStackTrace();
            }
            // set page number on content
            if (body) {
                page++;
                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER,
                        new Phrase(page + ""), (document.right() + document.left()) / 2, document.bottom() - 18, 0);
            }
        }
    }

    public Connection getCon() throws ClassNotFoundException, SQLException {
//        Connection connection = null;
//        String driverDB = "oracle.jdbc.OracleDriver";
//        String urlDB = "jdbc:oracle:thin:@scan-db.semarnat.gob.mx:1525/SINDESA";
//        String userDB = "DGIRA_MIAE";
//        String passDB = "DGIRA_MIAE";
//
//        Class.forName(driverDB);
//        connection = DriverManager.getConnection(urlDB, userDB, passDB);
        return DBConnectionFactory.getConnection();
    }

    @SuppressWarnings("resource")
	public byte[] pdf(String folio, Short serial, String url) throws DocumentException, IOException, ClassNotFoundException, SQLException {
        ByteArrayOutputStream ouBs = new ByteArrayOutputStream();
        ByteArrayOutputStream cBs = new ByteArrayOutputStream();
        String sTipoProyecto = "", claveTramite = null;
        short shSector;

        System.out.println("Folio |" + folio + "|");

        Connection con = getCon();
        Statement st = con.createStatement();
        
        try {
            Statement stQuery = con.createStatement();
            ResultSet rsQuery = stQuery.executeQuery("SELECT M.CLAVE_TRAMITE FROM SINATEC.VEXDATOSTRAMITE V INNER JOIN CATALOGO_TRAMITES.MODALIDAD M  ON V.BGID_TRAMITE_LK = M.ID_TRAMITE AND V.VCCLAVE_TRAMITE = M.CLAVE_TRAMITE "
                    + "WHERE v.bgtramiteid = '" + folio + "'");
            if (rsQuery.next()) {
                claveTramite = rsQuery.getString("CLAVE_TRAMITE");
            }
            
            rsQuery.close();
            stQuery.close();
            
        } catch (Exception e) {
            claveTramite = null;
            e.printStackTrace();
        }
        
        ResultSet rsProy = st.executeQuery("SELECT FOLIO_PROYECTO, SERIAL_PROYECTO, PROY_SUPUESTO, TIPO_ASENTAMIENTO_ID, CVE_TIPO_VIAL, NTIPO, NRAMA, PROY_NOMBRE, "
                + "PROY_NOMBRE_VIALIDAD, PROY_NUMERO_EXTERIOR, PROY_NUMERO_INTERIOR, PROY_NOMBRE_ASENTAMIENTO, PROY_CODIGO_POSTAL, PROY_LOCALIDAD, PROY_UBICACION_DESCRITA, "
                + "to_char(round(PROY_INVERSION_REQUERIDA,2),'999,999,999,999.99') as PROY_INVERSION_REQUERIDA, to_char(round(PROY_MEDIDAS_PREVENCION,2),'999,999,999,999.99') as PROY_MEDIDAS_PREVENCION, "
                + "to_char(round(PROY_INVERSION_REQUERIDA+PROY_MEDIDAS_PREVENCION,2),'999,999,999,999.99') as TOTAL_INVERSION_REQUERIDA, "
                + "PROY_EMPLEOS_PERMANENTES, PROY_EMPLEOS_TEMPORALES, PROY_TIEMPO_VIDA_ANIOS, PROY_TIEMPO_VIDA_MESES, "
                + "PROY_TIEMPO_VIDA_SEMANAS, PROY_DESC_MEDIDAS_COMPEN, PROY_DESC_PROTEC_CONSERV, PROY_DESC_PARTICULAR, PROY_DESC_AMBIENTE, PROY_DIAGRAMA_GANTT, "
                + "PROY_RESP_TEC_IGUAL_LEGAL, RESP_TEC_ID, ESTUDIO_RIESGO_ID, PROY_INVERSION_FEDERAL_ORI, PROY_INVERSION_ESTATAL_ORI, PROY_INVERSION_MUNICIPAL_ORI, "
                + "PROY_INVERSION_PRIVADA_ORI, PROY_REPRESENTANTE_ID, PROY_DESC_NAT, PROY_DEMANDA_SERV, PROY_DESC_PAISAJE, PROY_DIAG_AMBIENTAL, PROY_JUST_METODOLOGIA, "
                + "PROY_REQUI_MONTOS, PROY_ESCE_SIN_P, PROY_ESCE_CON_P, PROY_ESCE_CON_PYMED, PROY_DESC_EVALU_ALTER, CLAVE_PROYECTO, BITACORA_PROYECTO, NSUB, NSEC, ESTATUS_PROYECTO, "
                + "PROY_DOM_ESTABLECIDO, PROY_NORMA_ID, PROY_SUP_POET_PDU, PROY_ENT_AFECTADO, PROY_MUN_AFECTADO, PROY_LOTE, PROY_VALOR_CRITERIO, PROY_CRITERIO_MONTO, PROY_SERVREQ, "
                + "PROY_IND_PROCYOPERACIONES, PROY_IND_ACTIVITIPOIND, PROY_IND_CAPACIDAD_DISENIO, PROY_IND_DIAGRAMAPROC, PROY_IND_PROC_CONTAMINANTES, PROY_IND_PROC_SISTEMASAGUA, "
                + "PROY_IND_PROC_SISTEMASCONGE, DESC_DELIM_SISTEM_AMB, DESC_DELIM_AREA_INFLU, DESC_DELIM_SIT_PROY, JUST_PROY, DESC_METUTIL, FINYO_SEG_REQ, ESCE_SIN_PROY, "
                + "ESCE_CON_PROY, ESCE_CON_PROY_MED, PRONOS_AMB, CONCLUSION_PROY, EVA_ALTERNATIVAS, BIBLIO_PROY, MANIFESTACION_RES, MEDIO_SOCIOPROY, DESC_SUELO, DESC_PAISAJE_PROY, "
                + "DESC_GEOS, DESC_DIAGAMB, ANALISISBIO_VEG, BIBLIOGRAFIA_FAUNA, BIBLIOGRAFIA_FLORA, PROY_JUSTMET_UTIL, PROY_SUSTANCIA_APLICA, PROY_EXPLOSIVOS_APLICA, "
                + "PROY_GLOSARIO, PROY_ODISPOSICION_APLICA, PROY_MONTO_ETAPAS_APLICA, CLIMA_FEN_MET_OBSERV, TEMP_FEN_MET_DESCRIP, HIDRO_SUBT_OBSERVA, HIDRO_SUP_OBSERVA"
                + " FROM PROYECTO WHERE FOLIO_PROYECTO = '" + folio + "' AND SERIAL_PROYECTO = " + serial);

        
        if (rsProy.next()) {
            Document content = new Document(PageSize.A4, 50, 50, 50, 50);
            PdfWriter contentWriter = PdfWriter.getInstance(content, new ByteArrayOutputStream());
            ContentEvent event = new ContentEvent();
            contentWriter.setPageEvent(event);
            content.open();

            shSector = rsProy.getString("NSUB") != null && rsProy.getString("NSUB").compareToIgnoreCase("null") != 0
                    ? Short.parseShort(rsProy.getString("NSUB")) : 0;

            List<Chapter> chapterList = new ArrayList<Chapter>();

            // Promovente
            String nombrePromovente = "";
            String tipoPersona = "";
            String rfcPromovente = "";
            Statement stDp = con.createStatement();
            ResultSet rsDp = stDp.executeQuery("SELECT * FROM sinatec.Vexdatosusuario v WHERE v.bgtramiteid = " + folio);
            if (rsDp.next()) {
                tipoPersona = notNullFilter(rsDp.getString("VCTIPOPERSONA"));
                nombrePromovente = (tipoPersona.equals("MOR")) ? notNullFilter(rsDp.getString("VCRAZONSOCIAL")) : notNullFilter(rsDp.getString("VCNOMBRE")) + " " + notNullFilter(rsDp.getString("VCAPELLIDOPATERNO") + " " + rsDp.getString("VCAPELLIDOMATERNO"));
                rfcPromovente = notNullFilter(rsDp.getString("VCRFC"));
            }

            //Nombre del trámite          
            String nombreTramite = "";
            rsDp.close();
            stDp = con.createStatement();
            rsDp = stDp.executeQuery("SELECT NOMBRE_CORTO FROM SINATEC.VEXDATOSTRAMITE V INNER JOIN CATALOGO_TRAMITES.MODALIDAD M  ON V.BGID_TRAMITE_LK = M.ID_TRAMITE AND V.VCCLAVE_TRAMITE = M.CLAVE_TRAMITE "
                    + "WHERE v.bgtramiteid = " + folio);
            if (rsDp.next()) {
                nombreTramite = notNullFilter(rsDp.getString("NOMBRE_CORTO"));
            } else {
                nombreTramite = "Sin Datos en SINATEC";
            }

            // Responsable
            String[] resTec = new String[10];
            rsDp.close();
            rsDp = stDp.executeQuery("SELECT * FROM Resp_Tec_Proyecto r WHERE r.folio_Proyecto = " + folio);
            if (rsDp.next()) {
                resTec[0] = notNullFilter(rsDp.getString("RESP_TEC_NOMBRE"));
                resTec[1] = notNullFilter(rsDp.getString("RESP_TEC_APELLIDO_1"));
                resTec[2] = notNullFilter(rsDp.getString("RESP_TEC_APELLIDO_2"));
                resTec[3] = notNullFilter(rsDp.getString("RESP_TEC_RFC"));
                resTec[4] = notNullFilter(rsDp.getString("RESP_TEC_CURP"));
            }

            rsDp.close();
            stDp.close();

            // Sector
            String ssector = "";
            String ssubSector = "";
            String srama = "";
            Statement stSp = con.createStatement();
            ResultSet rsSp = stSp.executeQuery("SELECT * FROM catalogos.Rama_Proyecto v WHERE v.nrama = " + rsProy.getString("NRAMA"));
            if (rsSp.next()) {
                srama = notNullFilter(rsSp.getString("RAMA"));
            }
            rsSp = stSp.executeQuery("SELECT * FROM catalogos.Subsector_Proyecto v WHERE v.nsub = " + rsProy.getString("NSUB"));
            if (rsSp.next()) {
                ssubSector = notNullFilter(rsSp.getString("SUBSECTOR"));
            }
            rsSp = stSp.executeQuery("SELECT * FROM catalogos.sector_Proyecto v WHERE v.nsec = " + rsProy.getString("NSEC"));
            if (rsSp.next()) {
                ssector = notNullFilter(rsSp.getString("SECTOR"));
            }

            //Clave del Proyecto y bitácora
            String ClaveProyecto = "";
            String Bitacora = "";
            String FechaRegistro = "";
            rsSp = stSp.executeQuery("SELECT * FROM SINATEC.VEXDATOSTRAMITE v WHERE v.BGTRAMITEID = " + folio);
            if (rsSp.next()) {
                ClaveProyecto = notNullFilter(rsSp.getString("VCPROYECTO"));
                Bitacora = notNullFilter(rsSp.getString("VCBITACORA"));
                FechaRegistro = notNullFilter(rsSp.getString("DTFECHAREGISTRO").substring(0, 19)); // Primeros 19 caracteres. Fecha + H:M:S
            }

            rsSp.close();
            stSp.close();

            //<editor-fold defaultstate="collapsed" desc="Capitulo 1">
            // Capitulo 1.
            Chunk chapTitle = new Chunk("Datos generales del proyecto, del promovente y del responsable del estudio de impacto ambiental", TITULO1);
            Chapter chapter = new Chapter(new Paragraph(chapTitle), 1);
            chapTitle.setLocalDestination(chapter.getTitle().getContent());

            Chunk sec1_1Title = new Chunk("Datos generales del proyecto", TITULO2);
            Section seccion1_1 = chapter.addSection(new Paragraph(sec1_1Title));
            sec1_1Title.setLocalDestination(seccion1_1.getTitle().getContent());
            seccion1_1.setIndentationLeft(10);
            seccion1_1.add(new Paragraph("\n"));
            //Se crean los parrafos
            Paragraph DatosGenerales = new Paragraph();

            Chunk NombreProyecto = new Chunk("Nombre del proyecto: ", TITULO4);
            DatosGenerales.add(NombreProyecto);
            DatosGenerales.add("" + notNullFilter(rsProy.getString("PROY_NOMBRE")));

            DatosGenerales.add(Chunk.NEWLINE);

            Chunk SectorProyecto = new Chunk("Sector: ", TITULO4);
            DatosGenerales.add(SectorProyecto);
            DatosGenerales.add(ssector);

            DatosGenerales.add(Chunk.NEWLINE);

            Chunk SubsectorProyecto = new Chunk("Subsector: ", TITULO4);
            DatosGenerales.add(SubsectorProyecto);
            DatosGenerales.add(ssubSector);

            DatosGenerales.add(Chunk.NEWLINE);

            Chunk RamaProyecto = new Chunk("Rama: ", TITULO4);
            DatosGenerales.add(RamaProyecto);
            DatosGenerales.add(srama);

            Statement st2 = con.createStatement();
            ResultSet tipo = st2.executeQuery("SELECT * FROM CATALOGOS.TIPO_PROYECTO WHERE NRAMA = " + rsProy.getString("NRAMA") + " AND NTIPO = " + rsProy.getString("NTIPO"));

            DatosGenerales.add(Chunk.NEWLINE);

            Chunk TipoProyecto = new Chunk("Tipo de Proyecto: ", TITULO4);
            DatosGenerales.add(TipoProyecto);

            if (tipo.next()) {
                //seccion1_1.add(new Paragraph("Tipo de Proyecto: " + tipo.getString("TIPO_PROYECTO")));
                DatosGenerales.add("" + notNullFilter(tipo.getString("TIPO_PROYECTO")));
                sTipoProyecto = notNullFilter(tipo.getString("TIPO_PROYECTO"));
            }
            seccion1_1.add(DatosGenerales);
            seccion1_1.add(new Paragraph("\n"));
            
            
            
            /**
             * OBRAS Y ACTIVIDADES DEL REIA
             */
            Chunk sec1_REIA_Title = new Chunk("Obras y actividades previstas en el artículo 5 del REIA", TITULO3);
            Section seccion1_REIA = seccion1_1.addSection(new Paragraph(sec1_REIA_Title));
            seccion1_REIA.setIndentationLeft(10);
            seccion1_REIA.add(new Paragraph("\n"));

            PdfPTable tblREIA = new PdfPTable(6);
            tblREIA.setWidthPercentage((float) 100);
            float[] tblREIAColumnWidths = new float[] { 10f, 15f, 15f, 15f, 15f, 15f };
            tblREIA.setWidths(tblREIAColumnWidths);

            PdfPCell titCategoria = new PdfPCell(new Phrase("Categoría", TITULO3));
            PdfPCell titFraccion = new PdfPCell(new Phrase("Fracción", TITULO3));
            PdfPCell titObra = new PdfPCell(new Phrase("Obra", TITULO3));
            PdfPCell titPrincipal = new PdfPCell(new Phrase("Principal", TITULO3));
            PdfPCell titExceptuada = new PdfPCell(new Phrase("Exceptuada", TITULO3));
            PdfPCell titCondExce = new PdfPCell(new Phrase("Condición/Excepción", TITULO3));
            PdfPCell titRespuesta = new PdfPCell(new Phrase("Respuesta", TITULO3));

            ResultSet rsREIA = st2.executeQuery("SELECT RO.ID_OBRA, RCA.INCISO, RCA.CATEGORIA, RO.FRACCION, RO.OBRA"
            		+ ", CASE RPO.PRINCIPAL WHEN '0' THEN 'NO' ELSE 'SI' END AS PRINCIPAL"
            		+ ", CASE RPO.EXCEPTUADA WHEN '0' THEN 'SI' ELSE 'NO' END AS EXCEPTUADA"
            		+ " FROM REIA_PROY_OBRAS RPO"
    				+ " INNER JOIN REIA_OBRAS RO ON RPO.OBRA = RO.ID_OBRA"
    				+ " INNER JOIN REIA_CATEGORIAS RCA ON RO.ID_CATEGORIA = RCA.ID_CATEGORIA"
    				+ " WHERE RPO.FOLIO ='" + folio + "' AND RO.OBRA NOT LIKE 'NO'"
    				+ " ORDER BY RPO.CLAVE");

            // Recorrido de registros de OBRAS
            while (rsREIA.next()) {
            	Long valIdObra = rsREIA.getLong("ID_OBRA");
            	PdfPCell valInciso = new PdfPCell(new Phrase("" + notNullFilter(rsREIA.getString("INCISO")) + ")"));
            	PdfPCell valCategoria = new PdfPCell(new Phrase("" + notNullFilter(rsREIA.getString("CATEGORIA"))));
            	PdfPCell valFraccion = new PdfPCell(new Phrase("" + notNullFilter(rsREIA.getString("FRACCION"))));
            	PdfPCell valObra = new PdfPCell(new Phrase("" + notNullFilter(rsREIA.getString("OBRA"))));
            	PdfPCell valPrincipal = new PdfPCell(new Phrase("" + notNullFilter(rsREIA.getString("PRINCIPAL"))));
            	PdfPCell valExceptuada = new PdfPCell(new Phrase("" + notNullFilter(rsREIA.getString("EXCEPTUADA"))));

            	// Primera fila: Títulos
            	valInciso.setRowspan(2);
            	valInciso.setVerticalAlignment(Element.ALIGN_MIDDLE);
            	valInciso.setHorizontalAlignment(Element.ALIGN_CENTER);
            	tblREIA.addCell(valInciso);
            	tblREIA.addCell(titCategoria);
            	tblREIA.addCell(titFraccion);
            	tblREIA.addCell(titObra);
            	tblREIA.addCell(titPrincipal);
            	tblREIA.addCell(titExceptuada);

            	// Segunda fila: Valores
            	tblREIA.addCell(valCategoria);
            	tblREIA.addCell(valFraccion);
            	tblREIA.addCell(valObra);
            	
            	valPrincipal.setVerticalAlignment(Element.ALIGN_MIDDLE);
            	valPrincipal.setHorizontalAlignment(Element.ALIGN_CENTER);
            	tblREIA.addCell(valPrincipal);
            	
            	valExceptuada.setVerticalAlignment(Element.ALIGN_MIDDLE);
            	valExceptuada.setHorizontalAlignment(Element.ALIGN_CENTER);
            	tblREIA.addCell(valExceptuada);

            	
            	String sqlREIACondiciones = " FROM REIA_PROY_OBRAS PRO, REIA_PROY_CONDICIONES RPC, REIA_CONDICIONES RC"
            			+ " WHERE PRO.CLAVE = RPC.CLAVE_OBRA_PROY AND RPC.CONDICION = RC.ID_CONDICION"
    					+ " AND PRO.FOLIO = '" + folio + "' AND PRO.OBRA = " + valIdObra;
            	
            	String sqlTotalCondicionesPorObra = "SELECT COUNT(*) AS TOTAL" + sqlREIACondiciones;
            	
            	Statement stCondiciones = con.createStatement();
            	
            	ResultSet rsTotalCondiciones = stCondiciones.executeQuery(sqlTotalCondicionesPorObra);
            	
            	int total = 0;
            	
            	if (rsTotalCondiciones.next()) {
            		total = rsTotalCondiciones.getInt("TOTAL");
            	}
            	rsTotalCondiciones.close();
            	
            	if (total > 0) {
            		String sqlCondicionesPorObra = "SELECT RC.CONDICION, CASE RPC.RESPUESTA WHEN 1 THEN 'SI' ELSE 'NO' END RESPUESTA" + sqlREIACondiciones
            				+ " ORDER BY RC.ID_CONDICION";
            		
            		// Tercera fila: Título "Condición/Excepción, Respuesta"
                	titCondExce.setColspan(5);
                	tblREIA.addCell(titCondExce);
                	titRespuesta.setColspan(1);
                	tblREIA.addCell(titRespuesta);
                	
                	// Recuperación de registros de CONDICIONES
                	ResultSet rsCondiciones = stCondiciones.executeQuery(sqlCondicionesPorObra);
                	
                	while (rsCondiciones.next()) {
                		PdfPCell valCondExce = new PdfPCell(new Phrase("" + notNullFilter(rsCondiciones.getString("CONDICION"))));
                    	PdfPCell valRespuesta = new PdfPCell(new Phrase("" + notNullFilter(rsCondiciones.getString("RESPUESTA"))));
                		
                		// Cuarta fila: Valor "Condición/Excepción, Respuesta"
                    	valCondExce.setColspan(5);
                    	tblREIA.addCell(valCondExce);
                    	valRespuesta.setColspan(1);
                    	valRespuesta.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    	valRespuesta.setHorizontalAlignment(Element.ALIGN_CENTER);
                    	tblREIA.addCell(valRespuesta);
                	}
                	rsCondiciones.close();
                	stCondiciones.close();
                	// Fin del recorrido de CONDICIONES
            	}

            }
            rsREIA.close();

            seccion1_REIA.add(new Paragraph("\n"));
            seccion1_REIA.add(tblREIA);
            seccion1_REIA.add(new Paragraph("\n"));
            // Fin de tabla OBRAS Y ACTIVIDADES DEL REIA
            


            Chunk sec1_2Title = new Chunk("Datos generales del promovente", TITULO2);
            Section seccion1_2 = chapter.addSection(new Paragraph(sec1_2Title));
            sec1_2Title.setLocalDestination(seccion1_1.getTitle().getContent());
            seccion1_2.setIndentationLeft(10);
            seccion1_2.add(new Paragraph("\n"));

            Paragraph DatosPromovente = new Paragraph();

            Chunk NombrePromovente = new Chunk("Nombre del promovente: ", TITULO4);
            DatosPromovente.add(NombrePromovente);
            DatosPromovente.add(nombrePromovente);

            DatosPromovente.add(Chunk.NEWLINE);

            Chunk RFCPromovente = new Chunk("RFC: ", TITULO4);
            DatosPromovente.add(RFCPromovente);
            DatosPromovente.add(rfcPromovente);

            seccion1_2.add(DatosPromovente);
            seccion1_2.add(new Paragraph("\n"));

            //Representante legal
            Statement stRl = con.createStatement();
            ResultSet rsRl = stRl.executeQuery("SELECT * FROM sinatec.Vexdatosusuariorep v WHERE v.bgtramiteid = " + folio);
            Statement stRl_rfc = con.createStatement();
            ResultSet rsRl_rfc = stRl_rfc.executeQuery("SELECT * FROM rep_legal_proyecto v WHERE v.folio_proyecto = " + folio);
            String strRfcRepLegal = "";
            if (rsRl_rfc.next()) {
                strRfcRepLegal = notNullFilter(rsRl_rfc.getString("RFC"));
            }
            rsRl_rfc.close();
            stRl_rfc.close();
            
            if (rsRl.next()) {

                Chunk sec1_4Title = new Chunk("Datos generales del representante legal", TITULO2);
                Section seccion1_4 = chapter.addSection(new Paragraph(sec1_4Title));
                sec1_4Title.setLocalDestination(seccion1_1.getTitle().getContent());
                seccion1_4.setIndentationLeft(10);
                seccion1_4.add(new Paragraph("\n"));

                Paragraph datosRepLegal = new Paragraph();

                Chunk nombreRepLegal = new Chunk("Nombre del representante legal: ", TITULO4);
                datosRepLegal.add(nombreRepLegal);
                datosRepLegal.add( notNullFilter(rsRl.getString("VCNOMBRE")) + " " +
                        notNullFilter(rsRl.getString("VCAPELLIDO1")) + " " +
                        notNullFilter(rsRl.getString("VCAPELLIDO2")) );
                datosRepLegal.add(Chunk.NEWLINE);

                Chunk correoRepLegal = new Chunk("Correo electrónico del representante legal: ", TITULO4);
                datosRepLegal.add(correoRepLegal);
                datosRepLegal.add(notNullFilter(rsRl.getString("VCCORREO")));
                datosRepLegal.add(Chunk.NEWLINE);

                Chunk curpRepLegal = new Chunk("CURP del representante legal: ", TITULO4);
                datosRepLegal.add(curpRepLegal);
                datosRepLegal.add(notNullFilter(rsRl.getString("VCCURP")));
                datosRepLegal.add(Chunk.NEWLINE);

                Chunk rfcRepLegal = new Chunk("RFC del representante legal: ", TITULO4);
                datosRepLegal.add(rfcRepLegal);
                datosRepLegal.add(strRfcRepLegal);
                datosRepLegal.add(Chunk.NEWLINE);

                seccion1_4.add(datosRepLegal);
                seccion1_4.add(new Paragraph("\n"));
                
            }

            rsRl.close();
            stRl.close();
            

            Chunk sec1_3Title = new Chunk("Datos generales del responsable de la elaboración del estudio de impacto ambiental.", TITULO2);
            Section seccion1_3 = chapter.addSection(new Paragraph(sec1_3Title));
            sec1_3Title.setLocalDestination(seccion1_1.getTitle().getContent());
            seccion1_3.setIndentationLeft(10);
            seccion1_3.add(new Paragraph("\n"));

            Paragraph DatosResponsable = new Paragraph();

            Chunk RFCResponsable = new Chunk("RFC: ", TITULO4);
            DatosResponsable.add(RFCResponsable);
            DatosResponsable.add(resTec[3]);

            DatosResponsable.add(Chunk.NEWLINE);
            Chunk CURPResponsable = new Chunk("CURP: ", TITULO4);
            DatosResponsable.add(CURPResponsable);
            DatosResponsable.add(resTec[4]);

            DatosResponsable.add(Chunk.NEWLINE);
            Chunk NombreResponsable = new Chunk("Nombre del responsable técnico: ", TITULO4);
            DatosResponsable.add(NombreResponsable);
            DatosResponsable.add(resTec[0]);

            DatosResponsable.add(Chunk.NEWLINE);
            Chunk ApaternoResponsable = new Chunk("Apellido Paterno: ", TITULO4);
            DatosResponsable.add(ApaternoResponsable);
            DatosResponsable.add(resTec[1]);

            DatosResponsable.add(Chunk.NEWLINE);
            Chunk AmaternoResponsable = new Chunk("Apellido Materno: ", TITULO4);
            DatosResponsable.add(AmaternoResponsable);
            DatosResponsable.add(resTec[2]);

            seccion1_3.add(DatosResponsable);

            seccion1_3.add(new Paragraph("\n"));
            content.add(chapter);
            chapterList.add(chapter);
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Capitulo 2">
            // capitulo 2
            Chunk chapTitle2 = new Chunk("Información general del proyecto.", TITULO1);
            Chapter chapter2 = new Chapter(new Paragraph(chapTitle2), 2);
            chapTitle2.setLocalDestination(chapter2.getTitle().getContent());

            Chunk sec2_1Title = new Chunk("Información general del proyecto", TITULO2);
            Section seccion2_1 = chapter2.addSection(new Paragraph(sec2_1Title));
            sec2_1Title.setLocalDestination(seccion2_1.getTitle().getContent());
            seccion2_1.setIndentationLeft(10);
            seccion2_1.add(new Paragraph("\n"));

            Chunk sec2_1_1Title = new Chunk("Naturaleza del proyecto.", TITULO2);
            Section seccion2_1_1 = seccion2_1.addSection(new Paragraph(sec2_1_1Title));
            seccion2_1_1.setIndentationLeft(10);
            seccion2_1_1.add(new Paragraph("\n"));

            String NaturalezadeProyecto = "<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("PROY_DESC_NAT")) + "</p>";
            Paragraph NaturalezaProyecto = ReportUtil.parseHtmlParagraph(NaturalezadeProyecto);
            seccion2_1_1.add(NaturalezaProyecto);
            seccion2_1_1.add(new Paragraph("\n"));

            //Justificacion 
            if (ReportUtil.muestraApartado(claveTramite, shSector, "2-1-1-1")) {
                Chunk sec2_1_1_1Title = new Chunk("Justificación.", TITULO2);
                Section seccion2_1_1_1 = seccion2_1.addSection(new Paragraph(sec2_1_1_1Title));
                seccion2_1_1_1.setIndentationLeft(10);
                seccion2_1_1_1.add(new Paragraph("\n"));

                String strJustificacion = "<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("JUST_PROY")) + "</p>";
                Paragraph pJustificacion = ReportUtil.parseHtmlParagraph(strJustificacion);
                seccion2_1_1_1.add(pJustificacion);
                seccion2_1_1_1.add(new Paragraph("\n"));
            }

            /**
             * TABLA: 2.1 "SELECCION DEL SITIO"
             */
            Chunk sec2_1_2Title = new Chunk("Selección del sitio.", TITULO2);
            Section seccion2_1_2 = seccion2_1.addSection(new Paragraph(sec2_1_2Title));
            seccion2_1_1.setIndentationLeft(10);
            seccion2_1_1.add(new Paragraph("\n"));
            
            PdfPTable tblSeleccionSitio = new PdfPTable(3);
            tblSeleccionSitio.setWidthPercentage((float) 100);
            float[] tblSelSitColumnWidths = new float[] {5f, 40f, 55f};
            tblSeleccionSitio.setWidths(tblSelSitColumnWidths);
            
            PdfPCell titTipoCriterio = new PdfPCell(new Phrase("Tipo de Criterio", TITULO3));
            PdfPCell titCriterio = new PdfPCell(new Phrase("Criterio", TITULO3));
            PdfPCell titDescrCrit = new PdfPCell(new Phrase("Descripción", TITULO3));
            
            ResultSet rsCriterios = st2.executeQuery("SELECT A.CRITERIO_PROY_ID, B.CRITERIO_NOMBRE, A.CRITERIO, A.CRITERIO_DESCRIPCION"
            		+ " FROM CRITERIOS_PROYECTO A, CAT_CRITERIO B WHERE A.CRITERIO_ID = B.CRITERIO_ID AND A.FOLIO_PROYECTO = '" 
            		+ folio + "' AND A.SERIAL_PROYECTO = " + serial + " ORDER BY CRITERIO_PROY_ID");
            
            while (rsCriterios.next()) {
                PdfPCell valCriterioId = new PdfPCell(new Phrase("" + notNullFilter(rsCriterios.getString("CRITERIO_PROY_ID")), TITULO3));
                PdfPCell valTipoCrit = new PdfPCell(new Phrase("" + notNullFilter(rsCriterios.getString("CRITERIO_NOMBRE"))));
                PdfPCell valCriterio = new PdfPCell(new Phrase("" + notNullFilter(rsCriterios.getString("CRITERIO"))));
                PdfPCell valDescrCrit = new PdfPCell(new Phrase("" + notNullFilter(rsCriterios.getString("CRITERIO_DESCRIPCION"))));
                
                // Primera fila: Títulos
                valCriterioId.setRowspan(4);
                valCriterioId.setVerticalAlignment(Element.ALIGN_MIDDLE);
                valCriterioId.setHorizontalAlignment(Element.ALIGN_CENTER);
                tblSeleccionSitio.addCell(valCriterioId);
                tblSeleccionSitio.addCell(titTipoCriterio);
                tblSeleccionSitio.addCell(titCriterio);
                
                // Segunda fila: Valores
                tblSeleccionSitio.addCell(valTipoCrit);
                tblSeleccionSitio.addCell(valCriterio);

                // Tercera fila: Título "Descripción"
                titDescrCrit.setColspan(2);
                tblSeleccionSitio.addCell(titDescrCrit);
                
                // Cuarta fila: Valor "Descripción"
                valDescrCrit.setColspan(2);
                tblSeleccionSitio.addCell(valDescrCrit);
            }
            rsCriterios.close();
            
            seccion2_1_2.add(new Paragraph("\n"));
            
            seccion2_1_2.add(tblSeleccionSitio);
            seccion2_1_2.add(new Paragraph("\n"));
            // Cierre de TABLA: 2.1 "SELECCION DEL SITIO"
            System.out.println("\nDespués de tabla: Selección del sitio.\n");
            
            
            // 2.1.3
            Chunk sec2_1_3Title = new Chunk("Ubicación física del proyecto", TITULO2);
            Section seccion2_1_3 = seccion2_1.addSection(new Paragraph(sec2_1_3Title));
            seccion2_1_3.setIndentationLeft(10);
            PdfPTable tableUniFis = new PdfPTable(3);
            seccion2_1_3.add(new Paragraph("\n"));

            //Contacto SIGEIA
            Chunk sec2_1_3sigeia = new Chunk("Contacto SIGEIA", TITULO4);
            String UrlSIGEIA = SIGEIAView.getUrlPdf(folio, notNullFilter(rsProy.getString("CLAVE_PROYECTO")), serial.toString().trim(), notNullFilter(rsProy.getString("ESTATUS_PROYECTO")));
            sec2_1_3sigeia.setAnchor(UrlSIGEIA);
            seccion2_1_3.add(sec2_1_3sigeia);
            seccion2_1_3.add(new Paragraph("\n"));

            tableUniFis.setWidthPercentage((float) 100);

            Paragraph EntidadSIGEIA = new Paragraph(new Chunk("Entidad Federativa", TITULO4));
            Paragraph MunicipioSIGEIA = new Paragraph(new Chunk("Municipio", TITULO4));
            Paragraph AreaSIGEIA = new Paragraph(new Chunk("Superficie m2", TITULO4));

            PdfPCell uh1 = new PdfPCell(EntidadSIGEIA);
            PdfPCell uh2 = new PdfPCell(MunicipioSIGEIA);
            PdfPCell uh3 = new PdfPCell(AreaSIGEIA);

            uh1.setHorizontalAlignment(Element.ALIGN_CENTER);
            uh2.setHorizontalAlignment(Element.ALIGN_CENTER);
            uh3.setHorizontalAlignment(Element.ALIGN_CENTER);

            tableUniFis.addCell(uh1);
            tableUniFis.addCell(uh2);
            tableUniFis.addCell(uh3);

            ResultSet ubicacion = st2.executeQuery("SELECT count(*) FROM Mpios_Cruzadavshambre a WHERE a.comp = 'OBRA' and a.num_Folio = '" + folio + "' and a.version = " + serial);
            if (ubicacion.next()) {
                ubicacion = st2.executeQuery("SELECT a.c_Edomun, a.clv_Munici, a.nom_Estado, a.nom_Munici, to_char(sum(round(a.area,2)),'999,999,999,999.99') FROM Mpios_Cruzadavshambre a WHERE a.comp = 'OBRA' and a.num_Folio = '" + folio + "' and a.version = " + serial + "  GROUP BY a.c_Edomun, a.clv_Munici, a.nom_Estado, a.nom_Munici");
            } else {
                ubicacion = st2.executeQuery("SELECT a.c_Edomun, a.clv_Munici, a.nom_Estado, a.nom_Munici, to_char(sum(round(a.area,2)),'999,999,999,999.99') FROM Mpios_Cruzadavshambre a WHERE a.comp = 'PREDIO' and a.num_Folio = '" + folio + "' and a.version = " + serial + "  GROUP BY a.c_Edomun, a.clv_Munici, a.nom_Estado, a.nom_Munici");
            }

            while (ubicacion.next()) {
                PdfPCell c1 = new PdfPCell(new Phrase("" + notNullFilter(ubicacion.getString(3))));
                PdfPCell c2 = new PdfPCell(new Phrase("" + notNullFilter(ubicacion.getString(4))));
                PdfPCell c3 = new PdfPCell(new Phrase("" + notNullFilter(ubicacion.getString(5))));
                c3.setHorizontalAlignment(Element.ALIGN_RIGHT);

                tableUniFis.addCell(c1);
                tableUniFis.addCell(c2);
                tableUniFis.addCell(c3);
            }
            ubicacion.close();
            seccion2_1_3.add(new Paragraph("\n"));
            seccion2_1_3.add(tableUniFis);
            seccion2_1_3.add(new Paragraph("\n"));

            String pdes = notNullFilter(rsProy.getString("proy_dom_establecido"));
            Paragraph pUbicacionProyecto = new Paragraph();

            if (pdes != null && pdes.equals("S")) {
                Chunk UbicacionProyecto = new Chunk("Domicilio: ", TITULO4);
                pUbicacionProyecto.add(UbicacionProyecto);
                pUbicacionProyecto.add(new Chunk(Chunk.NEWLINE));
                ResultSet rsUbicacionProyecto = st2.executeQuery("SELECT CTV.DESCRIPCION AS TIPO_VIALIDAD, P.PROY_NOMBRE_VIALIDAD AS NOMBRE_VIALIDAD, P.PROY_NUMERO_EXTERIOR AS NUMERO_EXTERIOR, P.PROY_NUMERO_INTERIOR AS NUMERO_INTERIOR, CTA.NOMBRE AS TIPO_ASENTAMIENTO, P.PROY_NOMBRE_ASENTAMIENTO AS NOMBRE_ASENTAMIENTO, P.PROY_CODIGO_POSTAL AS CODIGO_POSTAL  FROM DGIRA_MIAE2.PROYECTO P "
                        + "INNER JOIN CATALOGOS.CAT_VIALIDAD CTV     ON P.CVE_TIPO_VIAL =  CTV.CVE_TIPO_VIAL "
                        + "INNER JOIN CATALOGOS.CAT_TIPO_ASEN  CTA ON P.TIPO_ASENTAMIENTO_ID = CTA.CVE_TIPO_ASEN "
                        + "WHERE FOLIO_PROYECTO = '" + folio + "' AND SERIAL_PROYECTO = " + serial);

                if (rsUbicacionProyecto.next()) {
                    //Tipo de Vialidad
                    Chunk cTipoVialidad = new Chunk("Tipo Vialidad: ", TITULO4);
                    pUbicacionProyecto.add(cTipoVialidad);
                    pUbicacionProyecto.add("" + notNullFilter(rsUbicacionProyecto.getString("TIPO_VIALIDAD")));
                    pUbicacionProyecto.add(new Chunk(Chunk.NEWLINE));

                    //Nombre Vialidad
                    Chunk cNombreVialidad = new Chunk("Nombre Vialidad: ", TITULO4);
                    pUbicacionProyecto.add(cNombreVialidad);
                    pUbicacionProyecto.add("" + notNullFilter(rsUbicacionProyecto.getString("NOMBRE_VIALIDAD")));
                    pUbicacionProyecto.add(new Chunk(Chunk.NEWLINE));

                    //Número Exterior
                    Chunk cNumeroExterior = new Chunk("Número Exterior: ", TITULO4);
                    pUbicacionProyecto.add(cNumeroExterior);
                    pUbicacionProyecto.add("" + notNullFilter(rsUbicacionProyecto.getString("NUMERO_EXTERIOR")));
                    pUbicacionProyecto.add(new Chunk(Chunk.NEWLINE));

                    //Número Interior
                    Chunk cNumeroInterior = new Chunk("Número Interior: ", TITULO4);
                    pUbicacionProyecto.add(cNumeroInterior);
                    pUbicacionProyecto.add("" + notNullFilter(rsUbicacionProyecto.getString("NUMERO_INTERIOR")));
                    pUbicacionProyecto.add(new Chunk(Chunk.NEWLINE));

                    //Tipo de Asentamiento
                    Chunk cTipoAsentamiento = new Chunk("Tipo Asentamiento: ", TITULO4);
                    pUbicacionProyecto.add(cTipoAsentamiento);
                    pUbicacionProyecto.add("" + notNullFilter(rsUbicacionProyecto.getString("TIPO_ASENTAMIENTO")));
                    pUbicacionProyecto.add(new Chunk(Chunk.NEWLINE));

                    //Nombre Asentamiento
                    Chunk cNombreAsentamiento = new Chunk("Nombre Asentamiento: ", TITULO4);
                    pUbicacionProyecto.add(cNombreAsentamiento);
                    pUbicacionProyecto.add("" + notNullFilter(rsUbicacionProyecto.getString("NOMBRE_ASENTAMIENTO")));
                    pUbicacionProyecto.add(new Chunk(Chunk.NEWLINE));

                    //Código Postal
                    Chunk cCodigoPostal = new Chunk("Código Postal: ", TITULO4);
                    pUbicacionProyecto.add(cCodigoPostal);
                    pUbicacionProyecto.add("" + notNullFilter(rsUbicacionProyecto.getString("CODIGO_POSTAL")));
                    pUbicacionProyecto.add(new Chunk(Chunk.NEWLINE));

                    rsUbicacionProyecto.close();
                } else {
                    rsUbicacionProyecto.close();
                }
            } else {
                Chunk UbicacionProyecto = new Chunk("Localización del proyecto: ", TITULO4);
                pUbicacionProyecto.add(UbicacionProyecto);
                pUbicacionProyecto.add(new Chunk(Chunk.NEWLINE));
                pUbicacionProyecto.add("" + notNullFilter(rsProy.getString("PROY_UBICACION_DESCRITA")));
                pUbicacionProyecto.setAlignment(Element.ALIGN_JUSTIFIED);
            }

            seccion2_1_3.add(pUbicacionProyecto);

            seccion2_1_3.add(new Paragraph("\n"));
            seccion2_1_3.add(new Paragraph(new Chunk("Planos Adicionales del proyecto ", TITULO4)));
            seccion2_1_3.add(new Paragraph("\n"));

            // Muestra tabla de Adjuntos
            // CAPITULO_ID='2' AND SUBCAPITULO_ID='1' AND SECCION_ID='3'";
            PdfPTable tblArchsPlanosAdic = generarTablaArchivos(folio, serial, url, (short) 2, (short) 1, (short) 3, (short) -1);
            seccion2_1_3.add(tblArchsPlanosAdic);
            seccion2_1_3.add(new Paragraph("\n"));


            //====================================== REPORTE 2.1.4. INVERSION REQUERIDA =================================================================================         

            PdfPTable tableInvesion = new PdfPTable(2);
            tableInvesion.setWidthPercentage((float) 100);
            
        	Paragraph CostoInversion = new Paragraph(new Chunk("Costo de la inversión requerida ", TITULO4));
            Paragraph CostoMedidas = new Paragraph(new Chunk("Costo de medidas de prevención y mitigación", TITULO4));
            Paragraph InversionTotal = new Paragraph(new Chunk("Inversión Total", TITULO4));

            CostoInversion.setAlignment(Element.ALIGN_LEFT);
            CostoMedidas.setAlignment(Element.ALIGN_LEFT);
            InversionTotal.setAlignment(Element.ALIGN_LEFT);

            // 2.1.4
            /**
             * TABLA: INVERSION Y EMPLEOS
             */
            Chunk sec2_1_4Title = new Chunk("Inversión y Empleos", TITULO2);
            Section seccion2_1_4 = seccion2_1.addSection(new Paragraph(sec2_1_4Title));
            seccion2_1_4.setIndentationLeft(10);
            seccion2_1_4.add(new Paragraph("\n"));
            
            String montoEtapasAplica = rsProy.getString("PROY_MONTO_ETAPAS_APLICA");
            if (montoEtapasAplica != null && montoEtapasAplica.compareTo("S") == 0) {
            	
            	String sqlInverEtapas = "SELECT CASE INVERSION_PREPARACION WHEN 0 THEN '0' ELSE TO_CHAR(ROUND(INVERSION_PREPARACION, 2), '999,999,999,999.99') END AS INVERSION_PREPARACION"
            		+ ", EMPLEOS_TEMPORALES_PREP, EMPLEOS_PERMANENTES_PREP, (EMPLEOS_TEMPORALES_PREP + EMPLEOS_PERMANENTES_PREP) AS TOTAL_EMPLEOS_PREP"
        			+ ", CASE INVERSION_CONSTRUCCION WHEN 0 THEN '0' ELSE TO_CHAR(ROUND(INVERSION_CONSTRUCCION, 2), '999,999,999,999.99') END AS INVERSION_CONSTRUCCION"
            		+ ", EMPLEOS_TEMPORALES_CONS, EMPLEOS_PERMANENTES_CONS, (EMPLEOS_TEMPORALES_CONS + EMPLEOS_PERMANENTES_CONS) AS TOTAL_EMPLEOS_CONS"
					+ ", CASE INVERSION_OPERACION WHEN 0 THEN '0' ELSE TO_CHAR(ROUND(INVERSION_OPERACION, 2), '999,999,999,999.99') END AS INVERSION_OPERACION"
            		+ ", EMPLEOS_TEMPORALES_OPER, EMPLEOS_PERMANENTES_OPER, (EMPLEOS_TEMPORALES_OPER + EMPLEOS_PERMANENTES_OPER) AS TOTAL_EMPLEOS_OPER"
					+ ", CASE INVERSION_ABANDONO WHEN 0 THEN '0' ELSE TO_CHAR(ROUND(INVERSION_ABANDONO, 2), '999,999,999,999.99') END AS INVERSION_ABANDONO"
            		+ ", EMPLEOS_TEMPORALES_ABAN, EMPLEOS_PERMANENTES_ABAN, (EMPLEOS_TEMPORALES_ABAN + EMPLEOS_PERMANENTES_ABAN) AS TOTAL_EMPLEOS_ABAN"
            		+ ", TO_CHAR(ROUND("
            		+ "(CASE WHEN INVERSION_PREPARACION IS NULL THEN 0 ELSE INVERSION_PREPARACION END" 
            		+ " + CASE WHEN INVERSION_CONSTRUCCION IS NULL THEN 0 ELSE INVERSION_CONSTRUCCION END"
            		+ " + CASE WHEN INVERSION_OPERACION IS NULL THEN 0 ELSE INVERSION_OPERACION END"
            		+ " + CASE WHEN INVERSION_ABANDONO IS NULL THEN 0 ELSE INVERSION_ABANDONO END), 2), '999,999,999,999.99') AS SUBTOTAL_INVERSION"
            		+ ", CASE WHEN EMPLEOS_TEMPORALES_PREP IS NULL THEN 0 ELSE EMPLEOS_TEMPORALES_PREP END"
            		+ " + CASE WHEN EMPLEOS_TEMPORALES_CONS IS NULL THEN 0 ELSE EMPLEOS_TEMPORALES_CONS END"
            		+ " + CASE WHEN EMPLEOS_TEMPORALES_OPER IS NULL THEN 0 ELSE EMPLEOS_TEMPORALES_OPER END" 
            		+ " + CASE WHEN EMPLEOS_TEMPORALES_ABAN IS NULL THEN 0 ELSE EMPLEOS_TEMPORALES_ABAN END AS TOTAL_EMPLEOS_TEMP"
            		+ ", CASE WHEN EMPLEOS_PERMANENTES_PREP IS NULL THEN 0 ELSE EMPLEOS_PERMANENTES_PREP END"
            		+ " + CASE WHEN EMPLEOS_PERMANENTES_CONS IS NULL THEN 0 ELSE EMPLEOS_PERMANENTES_CONS END"
            		+ " + CASE WHEN EMPLEOS_PERMANENTES_OPER IS NULL THEN 0 ELSE EMPLEOS_PERMANENTES_OPER END" 
            		+ " + CASE WHEN EMPLEOS_PERMANENTES_ABAN IS NULL THEN 0 ELSE EMPLEOS_PERMANENTES_ABAN END AS TOTAL_EMPLEOS_PERM"
            		+ ", TO_CHAR(ROUND(MEDIDAS_MITIGACION, 2), '999,999,999,999.99') AS MEDIDAS_MITIGACION"
            		+ ", TO_CHAR(ROUND("
            		+ "(CASE WHEN INVERSION_PREPARACION IS NULL THEN 0 ELSE INVERSION_PREPARACION END" 
            		+ " + CASE WHEN INVERSION_CONSTRUCCION IS NULL THEN 0 ELSE INVERSION_CONSTRUCCION END"
            		+ " + CASE WHEN INVERSION_OPERACION IS NULL THEN 0 ELSE INVERSION_OPERACION END"
            		+ " + CASE WHEN INVERSION_ABANDONO IS NULL THEN 0 ELSE INVERSION_ABANDONO END"
            		+ " + CASE WHEN MEDIDAS_MITIGACION IS NULL THEN 0 ELSE MEDIDAS_MITIGACION END), 2), '999,999,999,999.99') AS TOTAL_INVERSION"
					+ " FROM INVERSION_ETAPAS WHERE FOLIO_PROYECTO = '" + folio + "' AND SERIAL_PROYECTO = " + serial;
            	
            	PdfPTable tblInversionEtapas = generarTablaInversionEtapas(con, sqlInverEtapas);
                seccion2_1_4.add(tblInversionEtapas);
                seccion2_1_4.add(new Paragraph("\n"));
            }
            else {
	            Paragraph CostoInversion2 = new Paragraph(new Chunk("$ " + notNullFilter(rsProy.getString("PROY_INVERSION_REQUERIDA"))));
	            Paragraph CostoMedidas2 = new Paragraph(new Chunk("$ " + notNullFilter(rsProy.getString("PROY_MEDIDAS_PREVENCION"))));
	            Paragraph InversionTotal2 = new Paragraph(new Chunk("$ " + notNullFilter(rsProy.getString("TOTAL_INVERSION_REQUERIDA"))));
	
	            CostoInversion2.setAlignment(Element.ALIGN_RIGHT);
	            CostoMedidas2.setAlignment(Element.ALIGN_RIGHT);
	            InversionTotal2.setAlignment(Element.ALIGN_RIGHT);
	
	            PdfPCell cCostoInversion2 = new PdfPCell(CostoInversion2);
	            PdfPCell cCostoMedidas2 = new PdfPCell(CostoMedidas2);
	            PdfPCell cInversionTotal2 = new PdfPCell(InversionTotal2);
	
	            cCostoInversion2.setHorizontalAlignment(Element.ALIGN_RIGHT);
	            cCostoMedidas2.setHorizontalAlignment(Element.ALIGN_RIGHT);
	            cInversionTotal2.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	            tableInvesion.addCell(new PdfPCell(CostoInversion));
	            tableInvesion.addCell(cCostoInversion2);
	            tableInvesion.addCell(new PdfPCell(CostoMedidas));
	            tableInvesion.addCell(cCostoMedidas2);
	            tableInvesion.addCell(new PdfPCell(InversionTotal));
	            tableInvesion.addCell(cInversionTotal2);
	            
	            //Empleos
	            Paragraph EmpleosPermanentes = new Paragraph(new Chunk("Empleos Permanentes", TITULO4));
	            Paragraph EmpleosTemporales = new Paragraph(new Chunk("Empleos Temporales", TITULO4));
	            Paragraph EmpleosTotales = new Paragraph(new Chunk("Empleos Totales", TITULO4));
	
	            EmpleosPermanentes.setAlignment(Element.ALIGN_LEFT);
	            EmpleosTemporales.setAlignment(Element.ALIGN_LEFT);
	            EmpleosTotales.setAlignment(Element.ALIGN_LEFT);
	
	            Paragraph EmpleosPermanentes2 = new Paragraph(new Chunk("" + notNullFilter(rsProy.getString("PROY_EMPLEOS_PERMANENTES"))));
	            Paragraph EmpleosTemporales2 = new Paragraph(new Chunk("" + notNullFilter(rsProy.getString("PROY_EMPLEOS_TEMPORALES"))));
	            Paragraph EmpleosTotales2 = new Paragraph(new Chunk("" + (rsProy.getInt("PROY_EMPLEOS_TEMPORALES") + rsProy.getInt("PROY_EMPLEOS_PERMANENTES"))));
	
	            EmpleosPermanentes2.setAlignment(Element.ALIGN_RIGHT);
	            EmpleosTemporales2.setAlignment(Element.ALIGN_RIGHT);
	            EmpleosTotales2.setAlignment(Element.ALIGN_RIGHT);
	
	            PdfPCell cEmpleosPermanentes2 = new PdfPCell(EmpleosPermanentes2);
	            PdfPCell cEmpleosTemporales2 = new PdfPCell(EmpleosTemporales2);
	            PdfPCell cEmpleosTotales2 = new PdfPCell(EmpleosTotales2);
	
	            cEmpleosPermanentes2.setHorizontalAlignment(Element.ALIGN_RIGHT);
	            cEmpleosTemporales2.setHorizontalAlignment(Element.ALIGN_RIGHT);
	            cEmpleosTotales2.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	            tableInvesion.addCell(new PdfPCell(EmpleosPermanentes));
	            tableInvesion.addCell(cEmpleosPermanentes2);
	            tableInvesion.addCell(new PdfPCell(EmpleosTemporales));
	            tableInvesion.addCell(cEmpleosTemporales2);
	            tableInvesion.addCell(new PdfPCell(EmpleosTotales));
	            tableInvesion.addCell(cEmpleosTotales2);
	
	            seccion2_1_4.add(tableInvesion);
	
	            seccion2_1_4.add(new Paragraph("\n"));
	            
	            //Origen
	            try {
	                if (shSector == 7 && rsProy.getInt("NRAMA") == 30) {
	                    PdfPTable tablaOrigen = new PdfPTable(3);
	                    tablaOrigen.setWidthPercentage((float) 100);
	                    PdfPCell tipoOrigen = new PdfPCell(new Phrase("Origen", TITULO4));
	                    PdfPCell montoOrigen = new PdfPCell(new Phrase("Moneda nacional", TITULO4));
	                    PdfPCell porcOrigen = new PdfPCell(new Phrase("Porcentaje", TITULO4));
	                    tablaOrigen.addCell(tipoOrigen);
	                    tablaOrigen.addCell(montoOrigen);
	                    tablaOrigen.addCell(porcOrigen);
	
	                    String strPorcentaje = ReportUtil.porcentajeInversion(1, rsProy.getInt("PROY_INVERSION_FEDERAL_ORI"), 
	                            rsProy.getInt("PROY_INVERSION_ESTATAL_ORI"), rsProy.getInt("PROY_INVERSION_MUNICIPAL_ORI"), rsProy.getInt("PROY_INVERSION_PRIVADA_ORI")); 
	
	                    Paragraph tipoFed = new Paragraph(new Chunk("Federal"));
	                    Paragraph montoFed = new Paragraph(new Chunk("$ " + notNullFilter(rsProy.getString("PROY_INVERSION_FEDERAL_ORI"))));
	                    Paragraph porcFed = new Paragraph(new Chunk("" + strPorcentaje + "%" ));
	                    tipoFed.setAlignment(Element.ALIGN_LEFT);
	                    montoFed.setAlignment(Element.ALIGN_RIGHT);
	                    porcFed.setAlignment(Element.ALIGN_RIGHT);
	                    tablaOrigen.addCell(tipoFed);
	                    tablaOrigen.addCell(montoFed);
	                    tablaOrigen.addCell(porcFed);
	
	
	                    //Estatal
	                    strPorcentaje = ReportUtil.porcentajeInversion(2, rsProy.getInt("PROY_INVERSION_FEDERAL_ORI"), 
	                            rsProy.getInt("PROY_INVERSION_ESTATAL_ORI"), rsProy.getInt("PROY_INVERSION_MUNICIPAL_ORI"), rsProy.getInt("PROY_INVERSION_PRIVADA_ORI")); 
	                    tipoFed = new Paragraph(new Chunk("Estatal"));
	                    montoFed = new Paragraph(new Chunk("$ " + notNullFilter(rsProy.getString("PROY_INVERSION_ESTATAL_ORI"))));
	                    porcFed = new Paragraph(new Chunk("" + strPorcentaje + "%" ));
	                    tablaOrigen.addCell(tipoFed);
	                    tablaOrigen.addCell(montoFed);
	                    tablaOrigen.addCell(porcFed);
	
	                    //Municipal
	                    strPorcentaje = ReportUtil.porcentajeInversion(3, rsProy.getInt("PROY_INVERSION_FEDERAL_ORI"), 
	                            rsProy.getInt("PROY_INVERSION_ESTATAL_ORI"), rsProy.getInt("PROY_INVERSION_MUNICIPAL_ORI"), rsProy.getInt("PROY_INVERSION_PRIVADA_ORI")); 
	                    tipoFed = new Paragraph(new Chunk("Municipal"));
	                    montoFed = new Paragraph(new Chunk("$ " + notNullFilter(rsProy.getString("PROY_INVERSION_MUNICIPAL_ORI"))));
	                    porcFed = new Paragraph(new Chunk("" + strPorcentaje + "%" ));
	                    tablaOrigen.addCell(tipoFed);
	                    tablaOrigen.addCell(montoFed);
	                    tablaOrigen.addCell(porcFed);
	
	                    //Privada
	                    strPorcentaje = ReportUtil.porcentajeInversion(4, rsProy.getInt("PROY_INVERSION_FEDERAL_ORI"), 
	                            rsProy.getInt("PROY_INVERSION_ESTATAL_ORI"), rsProy.getInt("PROY_INVERSION_MUNICIPAL_ORI"), rsProy.getInt("PROY_INVERSION_PRIVADA_ORI")); 
	                    tipoFed = new Paragraph(new Chunk("Privada"));
	                    montoFed = new Paragraph(new Chunk("$ " + notNullFilter(rsProy.getString("PROY_INVERSION_PRIVADA_ORI"))));
	                    porcFed = new Paragraph(new Chunk("" + strPorcentaje + "%" ));
	                    tablaOrigen.addCell(tipoFed);
	                    tablaOrigen.addCell(montoFed);
	                    tablaOrigen.addCell(porcFed);
	
	
	                    seccion2_1_4.add(tablaOrigen);
	
	                    seccion2_1_4.add(new Paragraph("\n"));
	                }            
	            } catch (Exception e){
	                e.printStackTrace();
	            }
        	}
            // Cierre de TABLA: 2.1.4 "INVERSION Y EMPLEOS"
            System.out.println("\nDespués de tabla: Inversión y empleos.\n");
            
            // 2.1.5
            Chunk sec2_1_5Title = new Chunk("Dimensiones del proyecto.", TITULO2);
            Section seccion2_1_5 = seccion2_1.addSection(new Paragraph(sec2_1_5Title));
            seccion2_1_5.setIndentationLeft(10);

            String sql2_1_5 = "SELECT a.comp a, a.descrip b, to_char(round(a.shape,2),'999,999,999,999.99') c, to_char(round(a.shape/10000,4),'999,999,999.9999') d FROM Proysig a WHERE NUM_FOLIO = '" + folio + "' and VERSION = " + serial + " ORDER BY A.ID";
            PdfPTable tableDimProy = ReportUtil.buildTable(con, sql2_1_5, new String[]{"a", "b", "c", "d"}, new String[]{"Componente", "Descripción", "Superficie m2", "Superficie Ha"});

            seccion2_1_5.add(new Paragraph("\n"));
            seccion2_1_5.add(new Paragraph("Superficie total del predio y del proyecto"));
            seccion2_1_5.add(new Paragraph("\n"));
            seccion2_1_5.add(tableDimProy);
            seccion2_1_5.add(new Paragraph("\n"));
            seccion2_1_5.add(new Paragraph("Tipo de vegetación"));
            seccion2_1_5.add(new Paragraph("\n"));

            PdfPTable tblCap2TipoVeg = new PdfPTable(6);
            PdfPTable tblCap4TipoVeg = new PdfPTable(7);
            PdfPTable tableUsoSueloCAP = new PdfPTable(6);
            tblCap2TipoVeg.setWidthPercentage((float) 100);
            tblCap4TipoVeg.setWidthPercentage((float) 100);
            tableUsoSueloCAP.setWidthPercentage((float) 100);
            
            // Nombres de Columnas para encabezado
            PdfPCell titComponente = new PdfPCell(new Phrase("Componente", TITULO4));
            PdfPCell titTipVegDescr = new PdfPCell(new Phrase("Descripción", TITULO4));
            PdfPCell titGrupoVeg = new PdfPCell(new Phrase("Grupo de\nvegetación", TITULO4));
            PdfPCell titTVUsoSuelo = new PdfPCell(new Phrase("Tipo de\nvegetación o\nuso de suelo", TITULO4));
            PdfPCell titFaseVeg = new PdfPCell(new Phrase("Fase de\nvegetación", TITULO4));
            PdfPCell titTVSuperficie = new PdfPCell(new Phrase("Superficie (m2)", TITULO4));
            PdfPCell titDiag = new PdfPCell(new Phrase("Diagnóstico", TITULO4));
            PdfPCell titUsoAct = new PdfPCell(new Phrase("Uso suelo", TITULO4));
            
            // Fila con encabezado de tabla Capítulo 2 - Tipo de vegetación (sólo registros de SIGEIA)
            tblCap2TipoVeg.addCell(titComponente);
            tblCap2TipoVeg.addCell(titTipVegDescr);
            tblCap2TipoVeg.addCell(titGrupoVeg);
            tblCap2TipoVeg.addCell(titTVUsoSuelo);
            tblCap2TipoVeg.addCell(titFaseVeg);
            tblCap2TipoVeg.addCell(titTVSuperficie);
            
            // Fila con encabezado de tabla Capítulo 2 - Uso de suelo y/o cuerpos de agua del predio
            tableUsoSueloCAP.addCell(titComponente);
            tableUsoSueloCAP.addCell(titTipVegDescr);
            tableUsoSueloCAP.addCell(titGrupoVeg);
            tableUsoSueloCAP.addCell(titTVUsoSuelo);
            tableUsoSueloCAP.addCell(titFaseVeg);
            tableUsoSueloCAP.addCell(titTVSuperficie);
            
            // Fila con encabezado de tabla Capítulo 4 - Tipo de vegetación
            tblCap4TipoVeg.addCell(titComponente);
            tblCap4TipoVeg.addCell(titTipVegDescr);
            tblCap4TipoVeg.addCell(titGrupoVeg);
            tblCap4TipoVeg.addCell(titTVUsoSuelo);
            tblCap4TipoVeg.addCell(titFaseVeg);
            tblCap4TipoVeg.addCell(titTVSuperficie);
            tblCap4TipoVeg.addCell(titUsoAct);

            // Consulta para registros Padre
            String sqlTipoVeg_Padre = "SELECT SUELO_VEG_PROY_ID, SUELO_COMPONENTE, SUELO_DESCRIPCION, SUELO_TIPO_ECOV, SUELO_TIPO_GEN, SUELO_FASE_VS"
        		+ ", TO_CHAR(ROUND(SUELO_AREA_SIGEIA,2), '999,999,999,999.99') AS SUELO_AREA_SIGEIA_M2"
        		+ ", SUELO_DIAGNOSTICO, USO_SUELO_ACTUAL "
        		+ " FROM SUELO_VEGETACION_PROYECTO"
        		+ " WHERE FOLIO_PROYECTO = '" + folio + "' AND SERIAL_PROYECTO = " + serial
        		+ " ORDER BY SUELO_VEG_PROY_ID";
            
            ResultSet rsTipoVegetacion_Padre = st2.executeQuery(sqlTipoVeg_Padre);
            
            while (rsTipoVegetacion_Padre.next()) {
                int valTVId = rsTipoVegetacion_Padre.getInt(1);
                PdfPCell valTVComponente = new PdfPCell(new Phrase("" + notNullFilter(rsTipoVegetacion_Padre.getString(2))));
                PdfPCell valTVDescripcion = new PdfPCell(new Phrase("" + notNullFilter(rsTipoVegetacion_Padre.getString(3))));
                PdfPCell valTVGrupoVeg = new PdfPCell(new Phrase("" + notNullFilter(rsTipoVegetacion_Padre.getString(4))));
                PdfPCell valTipoVeg = new PdfPCell(new Phrase("" + notNullFilter(rsTipoVegetacion_Padre.getString(5))));
                PdfPCell valTVFaseVeg = new PdfPCell(new Phrase("" + notNullFilter(rsTipoVegetacion_Padre.getString(6))));
                PdfPCell valTVSuperficie = new PdfPCell(new Phrase("" + notNullFilter(rsTipoVegetacion_Padre.getString(7))));
                PdfPCell valTVDiagnostico = new PdfPCell(new Phrase("" + notNullFilter(rsTipoVegetacion_Padre.getString(8))));
                PdfPCell valUsoAct = new PdfPCell(new Phrase("" + notNullFilter(rsTipoVegetacion_Padre.getString(9))));
                
                // Agrega fila (registro tipo Padre) Capítulo 2 - Tipo de vegetación (sólo registros de SIGEIA)
                tblCap2TipoVeg.addCell(valTVComponente);
                tblCap2TipoVeg.addCell(valTVDescripcion);
                tblCap2TipoVeg.addCell(valTVGrupoVeg);
                tblCap2TipoVeg.addCell(valTipoVeg);
                tblCap2TipoVeg.addCell(valTVFaseVeg);
                valTVSuperficie.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tblCap2TipoVeg.addCell(valTVSuperficie);
                
                // Agrega fila (registro tipo Padre) Capítulo 2 - Uso de suelo y/o cuerpos de agua del predio
                tableUsoSueloCAP.addCell(valTVComponente);
                tableUsoSueloCAP.addCell(valTVDescripcion);
                tableUsoSueloCAP.addCell(valTVGrupoVeg);
                tableUsoSueloCAP.addCell(valTipoVeg);
                tableUsoSueloCAP.addCell(valTVFaseVeg);
                tableUsoSueloCAP.addCell(valTVSuperficie);
                
                // Agrega fila (registro tipo Padre) Capítulo 4 - Tipo de vegetación
                tblCap4TipoVeg.addCell(valTVComponente);
                tblCap4TipoVeg.addCell(valTVDescripcion);
                tblCap4TipoVeg.addCell(valTVGrupoVeg);
                tblCap4TipoVeg.addCell(valTipoVeg);
                tblCap4TipoVeg.addCell(valTVFaseVeg);
                tblCap4TipoVeg.addCell(valTVSuperficie);
                tblCap4TipoVeg.addCell(valUsoAct);
                // Agrega fila Diagnóstico
                titDiag.setColspan(7);
                tblCap4TipoVeg.addCell(titDiag);
                //
                valTVDiagnostico.setColspan(7);
                valTVDiagnostico.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                tblCap4TipoVeg.addCell(valTVDiagnostico);
                
                
                // Consulta para registros Hijo
                String sqlTipoVeg_Hijo = "SELECT VG.VEG_NOMBRE AS GRUPO_VEGETACION, VT.VEG_NOMBRE AS TIPO_VEGETACION, VF.VEG_NOMBRE AS FASE_VEGETACION"
            		+ ", TO_CHAR(ROUND(SVD.SUPERFICIE,2), '999,999,999,999.99') AS SUPERFICIE_M2"
            		+ ", SVD.DIAGNOSTICO"
            		+ " FROM SUELO_VEGETACION_DETALLE SVD, VEG_GRUPO VG, VEG_TIPO VT, VEG_FASE VF"
            		+ " WHERE SVD.ID_GRUPO_VEGETACION = VG.ID_VEG_GRUPO AND SVD.ID_TIPO_VEGETACION = VT.ID_VEG_TIPO AND SVD.ID_FASE_VEGETACION = VF.ID_VEG_FASE"
            		+ " AND FOLIO_PROYECTO = '" + folio + "' AND SERIAL_PROYECTO = " + serial + " AND SUELO_VEG_PROY_ID = " + valTVId
            		+ " ORDER BY ID_SEQ_SUE_VEG_DETALLE";
                
                Statement stTemp = con.createStatement();
                ResultSet rsTipoVegetacion_Hijo = stTemp.executeQuery(sqlTipoVeg_Hijo);
                
                while (rsTipoVegetacion_Hijo.next()) {
                	valTVGrupoVeg = new PdfPCell(new Phrase("" + notNullFilter(rsTipoVegetacion_Hijo.getString("GRUPO_VEGETACION"))));
                	valTipoVeg = new PdfPCell(new Phrase("" + notNullFilter(rsTipoVegetacion_Hijo.getString("TIPO_VEGETACION"))));
                	valTVFaseVeg = new PdfPCell(new Phrase("" + notNullFilter(rsTipoVegetacion_Hijo.getString("FASE_VEGETACION"))));
                	valTVSuperficie = new PdfPCell(new Phrase("" + notNullFilter(rsTipoVegetacion_Hijo.getString("SUPERFICIE_M2"))));
                	valTVDiagnostico = new PdfPCell(new Phrase("" + notNullFilter(rsTipoVegetacion_Hijo.getString("DIAGNOSTICO"))));

                	PdfPCell val_Componente_Hijo = new PdfPCell(new Phrase("->"));
                	PdfPCell val_Descripcion_Hijo = new PdfPCell(new Phrase("-"));
                	
                	val_Componente_Hijo.setHorizontalAlignment(Element.ALIGN_CENTER);
                	val_Descripcion_Hijo.setHorizontalAlignment(Element.ALIGN_CENTER);
                	valTVSuperficie.setHorizontalAlignment(Element.ALIGN_RIGHT);
                	
                	// Agrega fila (registro tipo Hijo) Capítulo 2 - Uso de suelo y/o cuerpos de agua del predio
                	tableUsoSueloCAP.addCell(val_Componente_Hijo);
                	tableUsoSueloCAP.addCell(val_Descripcion_Hijo);
                	tableUsoSueloCAP.addCell(valTVGrupoVeg);
                	tableUsoSueloCAP.addCell(valTipoVeg);
                	tableUsoSueloCAP.addCell(valTVFaseVeg);
                	tableUsoSueloCAP.addCell(valTVSuperficie);
                	
                    
                    // Agrega fila (registro tipo Hijo) Capítulo 4 - Tipo de vegetación
                	tblCap4TipoVeg.addCell(val_Componente_Hijo);
                	tblCap4TipoVeg.addCell(val_Descripcion_Hijo);
                	tblCap4TipoVeg.addCell(valTVGrupoVeg);
                	tblCap4TipoVeg.addCell(valTipoVeg);
                	tblCap4TipoVeg.addCell(valTVFaseVeg);
                	tblCap4TipoVeg.addCell(valTVSuperficie);
                    // Agrega fila Diagnóstico
                	valTVDiagnostico.setColspan(6);
                    valTVDiagnostico.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    tblCap4TipoVeg.addCell(valTVDiagnostico);
                }
                rsTipoVegetacion_Hijo.close();
                stTemp.close();
                
            }
            rsTipoVegetacion_Padre.close();
            
            seccion2_1_5.add(tblCap2TipoVeg);
            seccion2_1_5.add(new Paragraph("\n"));

            // 2.1.6
            Chunk sec2_1_6Title = new Chunk("Servicios Requeridos por el proyecto.", TITULO2);
            Section seccion2_1_6 = seccion2_1.addSection(new Paragraph(sec2_1_6Title));
            seccion2_1_6.setIndentationLeft(10);
            seccion2_1_6.add(new Paragraph("\n"));
            
            if ( rsProy.getString("PROY_SERVREQ") != null && rsProy.getString("PROY_SERVREQ").compareToIgnoreCase("S") == 0) {
            	
            	/**
                 * TABLA: SERVICIOS REQUERIDOS DEL PROYECTO
                 */          	
            	ResultSet rsServicios = st2.executeQuery("WITH REGS_SECUENCIALES AS ("
        			+ "SELECT B.ETAPA_ID"
        			+ ", CASE A.SERVICIO_ID WHEN 9999 THEN A.SERVICIO_NOMBRE ELSE C.SERVICIO_NOMBRE END AS SERVICIO_NOMBRE"
        			+ ", B.ETAPA_DESCRIPCION, A.SERVICIO_DISPONIBLE, A.SERVICIO_SUMINISTRADO, A.SERVICIO_DESCRIPCION"
        			+ " FROM SERVICIO_PROYECTO A, CAT_ETAPA B, CAT_SERVICIO C"
        			+ " WHERE A.ETAPA_ID = B.ETAPA_ID AND A.SERVICIO_ID = C.SERVICIO_ID"
        			+ " AND A.FOLIO_PROYECTO = '" + folio + "' AND A.SERIAL_PROYECTO = " + serial
        			+ " ORDER BY B.ETAPA_ID"
        			+ ")"
        			+ "SELECT ROWNUM AS SECUENCIAL, rs.* FROM REGS_SECUENCIALES rs"
        			);
            	
        		int etapaAnt = 0; // Se inicializa etapa a 0 para comenzar a recorrer los registros
                
                PdfPTable tableServicios = new PdfPTable(4);
                float[] tblServColumnWidths = new float[] {5f, 35f, 15f, 45f};
                tableServicios.setWidths(tblServColumnWidths);
                tableServicios.setWidthPercentage((float) 100);
                
                PdfPCell titServicioId = new PdfPCell(new Phrase("#", TITULO3));
                PdfPCell titNombreServ = new PdfPCell(new Phrase("Servicio", TITULO3));
                PdfPCell titDisponible = new PdfPCell(new Phrase("Disponible", TITULO3));
                PdfPCell titSuministrado = new PdfPCell(new Phrase("Suministrado por", TITULO3));
                
                while (rsServicios.next()) {
                	int etapaActual = Integer.parseInt(rsServicios.getString("ETAPA_ID").trim());
                	String etapa = rsServicios.getString("ETAPA_DESCRIPCION").trim();
                	
                	// Si existe etapa anterior
                    if (etapaAnt != 0) {
                    	// Si se encuentra una etapa distinta de la anterior
                    	if (etapaAnt != etapaActual){
                    		
                    		// Muestra la tabla en el archivo PDF
                    		seccion2_1_6.add(tableServicios);
                    		seccion2_1_6.add(new Paragraph("\n"));
                    		
                    		// Muestra el título de la etapa encontrada
                    		Chunk sec2_1_6_Etapa = new Chunk(notNullFilter(etapa), TITULO3);
                        	Section seccion2_1_6_Etapa = seccion2_1_6.addSection(new Paragraph(sec2_1_6_Etapa));
                        	sec2_1_6_Etapa.setLocalDestination(seccion2_1_6_Etapa.getTitle().getContent());
                        	seccion2_1_6_Etapa.setIndentationLeft(10);
                        	seccion2_1_6_Etapa.add(new Paragraph("\n"));
                        	
                        	// Actualiza valor de etapa anterior
                    		etapaAnt = etapaActual;
                    		
                            // Se crea una nueva tabla para agregar datos de la etapa encontrada
                            tableServicios = new PdfPTable(4);
                            tableServicios.setWidths(tblServColumnWidths);
                            tableServicios.setWidthPercentage((float) 100);
                            
                            // Fila con Títulos
                            titServicioId.setHorizontalAlignment(Element.ALIGN_CENTER);
                            tableServicios.addCell(titServicioId);
                            tableServicios.addCell(titNombreServ);
                            titDisponible.setHorizontalAlignment(Element.ALIGN_CENTER);
                            tableServicios.addCell(titDisponible);
                            titSuministrado.setHorizontalAlignment(Element.ALIGN_CENTER);
                            tableServicios.addCell(titSuministrado);
                    	}
                    }
                    else
                    {
                    	// Muestra el título de la primera etapa encontrada
                    	Chunk sec2_1_6_Etapa = new Chunk(notNullFilter(etapa), TITULO3);
                    	Section seccion2_1_6_Etapa = seccion2_1_6.addSection(new Paragraph(sec2_1_6_Etapa));
                    	sec2_1_6_Etapa.setLocalDestination(seccion2_1_6_Etapa.getTitle().getContent());
                    	seccion2_1_6_Etapa.setIndentationLeft(10);
                    	seccion2_1_6_Etapa.add(new Paragraph("\n"));
                    	
                    	// Actualiza valor de etapa anterior
                    	etapaAnt = etapaActual;
                    	
                    	// Fila con Títulos
                        titServicioId.setHorizontalAlignment(Element.ALIGN_CENTER);
                        tableServicios.addCell(titServicioId);
                        tableServicios.addCell(titNombreServ);
                        titDisponible.setHorizontalAlignment(Element.ALIGN_CENTER);
                        tableServicios.addCell(titDisponible);
                        titSuministrado.setHorizontalAlignment(Element.ALIGN_CENTER);
                        tableServicios.addCell(titSuministrado);
                    }
                    
                    PdfPCell valServicioId = new PdfPCell(new Phrase("" + notNullFilter(rsServicios.getString("SECUENCIAL")), TITULO3));
                    PdfPCell valNombreServicio = new PdfPCell(new Phrase("" + notNullFilter(rsServicios.getString("SERVICIO_NOMBRE"))));
                    String valServDispon = "NO";
                    String servDisponible = rsServicios.getString("SERVICIO_DISPONIBLE");
                    if (null != servDisponible && servDisponible.compareToIgnoreCase("S") == 0) {
                        valServDispon = "SI";
                    }
                    PdfPCell valDisponible = new PdfPCell(new Phrase(valServDispon));
                    PdfPCell valSuministrado = new PdfPCell(new Phrase("" + notNullFilter(rsServicios.getString("SERVICIO_SUMINISTRADO"))));
                    PdfPCell valDescrServ = new PdfPCell(new Phrase("" + notNullFilter(rsServicios.getString("SERVICIO_DESCRIPCION"))));
                    
                    // Filas con Valores
                    valServicioId.setRowspan(2);
                    valServicioId.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    valServicioId.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableServicios.addCell(valServicioId);
                    tableServicios.addCell(valNombreServicio);
                    valDisponible.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableServicios.addCell(valDisponible);
                    tableServicios.addCell(valSuministrado);
                    valDescrServ.setColspan(3);
                    tableServicios.setHorizontalAlignment(Paragraph.ALIGN_JUSTIFIED);
                    tableServicios.addCell(valDescrServ);
                }
                rsServicios.close();
                
                // Muestra, en el archivo PDF, la última tabla generada
                seccion2_1_6.add(tableServicios);
                seccion2_1_6.add(new Paragraph("\n")); 
            	
            } else {
                Paragraph sinServicios = new Paragraph();
                sinServicios.add("El proyecto no demanda servicios");
                seccion2_1_6.add(sinServicios);
            }
            seccion2_1_6.add(new Paragraph("\n"));
            // Cierre de TABLA: SERVICIOS REQUERIDOS DEL PROYECTO
            System.out.println("\nDespués de tabla: Servicios requeridos del proyecto.\n");

            
            /**
             * TABLA: "2.2.1. CARACTERISTICAS PARTICULARES DEL PROYECTO"
             */
            // 2-2
            Chunk sec2_2Title = new Chunk("Características particulares del proyecto", TITULO2);
            Section seccion2_2 = chapter2.addSection(new Paragraph(sec2_2Title));
            sec2_2Title.setLocalDestination(seccion2_2.getTitle().getContent());
            seccion2_2.setIndentationLeft(10);
            seccion2_2.add(new Paragraph("\n"));

            // 2.2.1
            Chunk sec2_2_1Title = new Chunk("Características particulares del proyecto", TITULO2);
            Section seccion2_2_1 = seccion2_2.addSection(new Paragraph(sec2_2_1Title));
            sec2_2_1Title.setLocalDestination(seccion2_2_1.getTitle().getContent());
            seccion2_2_1.setIndentationLeft(10);
            seccion2_2_1.add(new Paragraph("\n"));

            PdfPTable tableCarPart = new PdfPTable(2);
            tableCarPart.setWidthPercentage((float) 100);
            float[] tblCarParColumnWidths = new float[] {25f, 75f};
            tableCarPart.setWidths(tblCarParColumnWidths);
            
            PdfPCell etiqId = new PdfPCell(new Phrase("#", TITULO3));
            PdfPCell etiqNomObra = new PdfPCell(new Phrase("Nombre de Obra", TITULO3));
            PdfPCell etiqSupUnidad = new PdfPCell(new Phrase("Superficie", TITULO3));
            PdfPCell etiqObraAct = new PdfPCell(new Phrase("Obra/Actividad", TITULO3));
            PdfPCell etiqNatural = new PdfPCell(new Phrase("Naturaleza", TITULO3));
            PdfPCell etiqTempor = new PdfPCell(new Phrase("Temporalidad", TITULO3));
            PdfPCell etiqDescrip = new PdfPCell(new Phrase("Descripción", TITULO3));

            ResultSet carPar = st2.executeQuery("WITH REGS_SECUENCIALES AS ("
        		+ "SELECT A.NOMBRE_OBRA, A.SUPERFICIE, D.CTUN_DESC, A.OBRA_ACTIVIDAD"
        		+ ", B.NATURALEZA_DESCRIPCION, C.TEMPORALIDAD_DESCRIPCION, A.DESCRIPCION"
    			+ " FROM DGIRA_MIAE2.CARAC_PART_PROY A"
				+ " LEFT JOIN DGIRA_MIAE2.CAT_NATURALEZA B ON (A.NATURALEZA_ID = B.NATURALEZA_ID)"
				+ " LEFT JOIN DGIRA_MIAE2.CAT_TEMPORALIDAD C ON (A.TEMPORALIDAD_ID = C.TEMPORALIDAD_ID)"
				+ " LEFT JOIN CATALOGOS.CAT_UNIDAD_MEDIDA D ON (A.CTUN_CLVE = D.CTUN_CLVE)"
				+ " WHERE A.FOLIO_PROYECTO = '" + folio + "' AND A.SERIAL_PROYECTO = " + serial
				+ " ORDER BY A.CARACTERISTICA_ID)"
				+ " SELECT ROWNUM AS SECUENCIAL, rs.* FROM REGS_SECUENCIALES rs");
            
            while (carPar.next()) {
            	PdfPCell valId = new PdfPCell(new Phrase("" + notNullFilter(carPar.getString("SECUENCIAL")), TITULO3));
            	PdfPCell valNomObra = new PdfPCell(new Phrase("" + notNullFilter(carPar.getString("NOMBRE_OBRA"))));
            	PdfPCell valSupUnidad = new PdfPCell(new Phrase("" + notNullFilter(carPar.getString("SUPERFICIE")
            			+ " " + notNullFilter(carPar.getString("CTUN_DESC")))));
                PdfPCell valObraAct = new PdfPCell(new Phrase("" + notNullFilter(carPar.getString("OBRA_ACTIVIDAD"))));
                PdfPCell valNatural = new PdfPCell(new Phrase("" + notNullFilter(carPar.getString("NATURALEZA_DESCRIPCION"))));
                PdfPCell valTempor = new PdfPCell(new Phrase("" + notNullFilter(carPar.getString("TEMPORALIDAD_DESCRIPCION"))));
                PdfPCell valDescrip = new PdfPCell(new Phrase("" + notNullFilter(carPar.getString("DESCRIPCION"))));
                
                // Fila "#"
                etiqId.setFixedHeight(30f);
                etiqId.setVerticalAlignment(Element.ALIGN_MIDDLE);
                tableCarPart.addCell(etiqId);
                valId.setVerticalAlignment(Element.ALIGN_MIDDLE);
                tableCarPart.addCell(valId);
                
                // Fila "Nombre de Obra"
                tableCarPart.addCell(etiqNomObra);
                tableCarPart.addCell(valNomObra);
                
                // Fila "Superficie + Unidad"
                tableCarPart.addCell(etiqSupUnidad);
                tableCarPart.addCell(valSupUnidad);
                
                // Fila "Obra/Actividad"
                tableCarPart.addCell(etiqObraAct);
                tableCarPart.addCell(valObraAct);
                
                // Fila "Naturaleza"
                tableCarPart.addCell(etiqNatural);
                tableCarPart.addCell(valNatural);
                
                // Fila "Temporalidad"
                tableCarPart.addCell(etiqTempor);
                tableCarPart.addCell(valTempor);
                
                // Fila "Descripción"
                tableCarPart.addCell(etiqDescrip);
                valDescrip.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                tableCarPart.addCell(valDescrip);
            }
            carPar.close();
            
            seccion2_2_1.add(tableCarPart);
            seccion2_2_1.add(new Paragraph("\n"));
            // Cierre de TABLA: "2.2.1. CARACTERISTICAS PARTICULARES DEL PROYECTO"
            System.out.println("\nDespués de tabla: Características particulares del proyecto.\n");

            
            // 2.2.2
            Chunk sec2_2_2Title = new Chunk("Uso actual de suelo y/o cuerpos de agua en el sitio del proyecto y en sus colindancias", TITULO2);
            Section seccion2_2_2 = seccion2_2.addSection(new Paragraph(sec2_2_2Title));
            sec2_2_2Title.setLocalDestination(seccion2_2_2.getTitle().getContent());
            seccion2_2_2.setIndentationLeft(10);
            seccion2_2_2.add(new Paragraph("\n"));

            // 2.2.2.1
            /**
             * TABLA: 2.2.2.1 "USO DE SUELO Y/O CUERPOS DE AGUA DEL PREDIO"
             */
            Chunk sec2_2_2_1Title = new Chunk("Uso de suelo y/o Cuerpos de Agua del predio", TITULO2);
            Section seccion2_2_2_1 = seccion2_2_2.addSection(new Paragraph(sec2_2_2_1Title));
            sec2_2_2_1Title.setLocalDestination(seccion2_2_2_1.getTitle().getContent());
            seccion2_2_2_1.add(new Paragraph("\n"));
            
            /**
             * La tabla fue creada y llenada previamente con la del capítulo 2 - Tipo de vegetación,
             *  ya que la información se toma de las mismas tablas,
             *  la única diferencia es que esta tabla lleva la información de los registros Hijos creados a los registros Padre de SIGEIA
             */
            seccion2_2_2_1.add(tableUsoSueloCAP);
            seccion2_2_2_1.add(new Paragraph("\n"));
            // Cierre de TABLA: 2.2.2.1 "USO DE SUELO Y/O CUERPOS DE AGUA DEL PREDIO"
            System.out.println("\nDespués de tabla: Uso de suelo y/o cuerpos de agua del predio.\n");
            

            // 2.2.2.2
            /**
             * TABLA: 2.2.2.2 "USO DE SUELO DE PREDIOS COLINDANTES"
             */
            Chunk sec2_2_2_2Title = new Chunk("Uso de suelo de predios colindantes", TITULO2);
            Section seccion2_2_2_2 = seccion2_2_2.addSection(new Paragraph(sec2_2_2_2Title));
            sec2_2_2_2Title.setLocalDestination(seccion2_2_2_1.getTitle().getContent());
            seccion2_2_2_2.add(new Paragraph("\n"));

            PdfPTable tableUsoPredCol = new PdfPTable(5);
            tableUsoPredCol.setWidthPercentage((float) 100);
            float[] tblUSPCColumnWidths = new float[] {5f, 35f, 20f, 20f, 20f};
            tableUsoPredCol.setWidths(tblUSPCColumnWidths);
            
            PdfPCell etiqPredio = new PdfPCell(new Phrase("Predio", TITULO3));
            PdfPCell etiqClasif = new PdfPCell(new Phrase("Clasificación", TITULO3));
            PdfPCell etiqUsoSuelo = new PdfPCell(new Phrase("Uso de suelo", TITULO3));
            PdfPCell etiqRef = new PdfPCell(new Phrase("Referencia", TITULO3));
            PdfPCell etiqDescr = new PdfPCell(new Phrase("Descripción", TITULO3));

            ResultSet predCol = st2.executeQuery("SELECT P.PREDIO_COLIN_ID, P.PREDIO_NOMBRE, C.CLASIFICACION_DESCRIPCION,"
            		+ " C2.CLASIFICACION_B, R.REFERENCIA_DESCRIPCION, P.PREDIO_DESCRIPCION" 
            		+ " FROM Prediocolin_Proy P, CAT_CLASIFICACION C, CAT_CLASIFICACION_B C2, CAT_REFERENCIA R"
            		+ " WHERE P.CLASIFICACION_ID = C.CLASIFICACION_ID AND P.CLASIFICACION_B_ID = C2.CLASIFICACION_B_ID"
            		+ " AND P.REFERENCIA_ID = R.REFERENCIA_ID AND P.FOLIO_PROYECTO = '" + folio + "' AND P.SERIAL_PROYECTO = " + serial 
            		+ " ORDER BY P.PREDIO_COLIN_ID");
            
            while (predCol.next()) {
            	PdfPCell valPredioId = new PdfPCell(new Phrase("" + notNullFilter(predCol.getString("PREDIO_COLIN_ID")), TITULO3));
                PdfPCell valPredio = new PdfPCell(new Phrase("" + notNullFilter(predCol.getString("PREDIO_NOMBRE"))));
                PdfPCell valClasif = new PdfPCell(new Phrase("" + notNullFilter(predCol.getString("CLASIFICACION_DESCRIPCION"))));
                PdfPCell valUsoSuelo = new PdfPCell(new Phrase("" + notNullFilter(predCol.getString("CLASIFICACION_B"))));
                PdfPCell valRef = new PdfPCell(new Phrase("" + notNullFilter(predCol.getString("REFERENCIA_DESCRIPCION"))));
                PdfPCell valDescr = new PdfPCell(new Phrase("" + notNullFilter(predCol.getString("PREDIO_DESCRIPCION"))));
               
                // Primera fila: Títulos
                valPredioId.setRowspan(4);
                valPredioId.setVerticalAlignment(Element.ALIGN_MIDDLE);
                valPredioId.setHorizontalAlignment(Element.ALIGN_CENTER);
                tableUsoPredCol.addCell(valPredioId);
                tableUsoPredCol.addCell(etiqPredio);
                tableUsoPredCol.addCell(etiqClasif);
                tableUsoPredCol.addCell(etiqUsoSuelo);
                tableUsoPredCol.addCell(etiqRef);
                
                // Segunda fila: Valores
                tableUsoPredCol.addCell(valPredio);
                tableUsoPredCol.addCell(valClasif);
                tableUsoPredCol.addCell(valUsoSuelo);
                tableUsoPredCol.addCell(valRef);
                
                // Tercera fila: Título "Descripción"
                etiqDescr.setColspan(4);
                tableUsoPredCol.addCell(etiqDescr);
                
                // Cuarta fila: Valor "Descripción"
                valDescr.setColspan(4);
                valDescr.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                tableUsoPredCol.addCell(valDescr);
            }
            predCol.close();
            seccion2_2_2_2.add(tableUsoPredCol);
            seccion2_2_2_2.add(new Paragraph("\n"));
            // Cierre de TABLA: 2.2.2.2 "USO DE SUELO DE PREDIOS COLINDANTES"
            System.out.println("\nDespués de tabla: Uso de suelo de predios colindantes.\n");

            // 2.2.3
            if (ReportUtil.muestraApartado(claveTramite, shSector, "2-2-3-0")) {
                Chunk sec2_2_3Title = new Chunk("Descripción de obras principales del proyecto", TITULO2);
                Section seccion2_2_3 = seccion2_2.addSection(new Paragraph(sec2_2_3Title));
                sec2_2_3Title.setLocalDestination(seccion2_2_3.getTitle().getContent());
                seccion2_2_3.setIndentationLeft(10);
                seccion2_2_3.add(new Paragraph("\n"));
                seccion2_2_3.add(new Paragraph("Tipo de actividad industrial.", TITULO3));
                seccion2_2_3.add(new Paragraph("\n"));
                seccion2_2_3.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("PROY_IND_ACTIVITIPOIND")) + "</p>"));

                seccion2_2_3.add(new Paragraph("\n"));
                seccion2_2_3.add(new Paragraph("Procesos y Operaciones Unitarias.", TITULO3));
                seccion2_2_3.add(new Paragraph("\n"));
                seccion2_2_3.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("PROY_IND_PROCYOPERACIONES")) + "</p>"));

                seccion2_2_3.add(new Paragraph("\n"));
                seccion2_2_3.add(new Paragraph("¿Cuál es la Capacidad de Diseño de los Equipos que se Utilizarán?", TITULO3));
                seccion2_2_3.add(new Paragraph("\n"));
                seccion2_2_3.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("PROY_IND_CAPACIDAD_DISENIO")) + "</p>"));

                seccion2_2_3.add(new Paragraph("\n"));
                seccion2_2_3.add(new Paragraph("Identificar y mencionar, en que parte del diagrama de procesos, los puntos y equipos donde se generan contaminantes al aire, agua y suelo, así como los de mayor riesgo indicando los equipos para prevenir o reducir contamintantes, asi como para el control y prevención de riesgos.", TITULO3));
                seccion2_2_3.add(new Paragraph("\n"));
                seccion2_2_3.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("PROY_IND_DIAGRAMAPROC")) + "</p>"));

                seccion2_2_3.add(new Paragraph("\n"));
                seccion2_2_3.add(new Paragraph("Indicar si Contará con sistemas de reutilización de agua, en caso afirmativo describa el sistema que se utilizará.", TITULO3));
                seccion2_2_3.add(new Paragraph("\n"));
                seccion2_2_3.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("PROY_IND_PROC_SISTEMASAGUA")) + "</p>"));

                seccion2_2_3.add(new Paragraph("\n"));
                seccion2_2_3.add(new Paragraph("El proyecto incluye sistemas para la congeneración y recuperación de energias, describa el sistema.", TITULO3));
                seccion2_2_3.add(new Paragraph("\n"));
                seccion2_2_3.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("PROY_IND_PROC_SISTEMASCONGE")) + "</p>"));
                seccion2_2_3.add(new Paragraph("\n"));
                            	
                // Muestra tabla de Adjuntos
                // CAPITULO_ID='2' AND SUBCAPITULO_ID='2' AND SECCION_ID='3'"
                PdfPTable tblArchsObrasPrincProy = generarTablaArchivos(folio, serial, url, (short) 2, (short) 2, (short) 3, (short) -1);
                seccion2_2_3.add(tblArchsObrasPrincProy);
                seccion2_2_3.add(new Paragraph("\n"));
                
            }
            
            
            /**
             * Programa general de trabajo
             */
            Chunk sec2_2_4Title_Programa = new Chunk("Programa general de trabajo.", TITULO2);
            Section seccion2_2_4_Programa = seccion2_2.addSection(new Paragraph(sec2_2_4Title_Programa));
            sec2_2_4Title_Programa.setLocalDestination(seccion2_2_4_Programa.getTitle().getContent());
            seccion2_2_4_Programa.setIndentationLeft(10);
            seccion2_2_4_Programa.add(new Paragraph("\n"));
            
            short seccionDpPt = 3;
            if (rsProy.getString("NSUB") != null && rsProy.getString("NSUB").equals("6")) {
                seccionDpPt = 4;
            }
            
            Chunk sec2_2_4_Anexos = new Chunk("Adjuntos", TITULO3);
            seccion2_2_4_Programa.add(sec2_2_4_Anexos);
            seccion2_2_4_Programa.add(new Paragraph("\n"));
        	
            // CAPITULO_ID='2' AND SUBCAPITULO_ID='2' AND SECCION_ID='" + seccionDpPt + "'";
            PdfPTable tblArchsProgGralTrab = generarTablaArchivos(folio, serial, url, (short) 2, (short) 2, seccionDpPt, (short) -1);
            seccion2_2_4_Programa.add(tblArchsProgGralTrab);
            seccion2_2_4_Programa.add(new Paragraph("\n"));
            System.out.println("\nDespués de tabla: Programa general de trabajo.\n");
            

            // 2.2.4
            Chunk sec2_2_4Title = new Chunk("Duración del proyecto y programa de trabajo.", TITULO2);
            Section seccion2_2_4 = seccion2_2.addSection(new Paragraph(sec2_2_4Title));
            sec2_2_4Title.setLocalDestination(seccion2_2_4.getTitle().getContent());
            seccion2_2_4.setIndentationLeft(10);
            seccion2_2_4.add(new Paragraph("\n"));

            String sql224p = "SELECT ETAPA_DESCRIPCION, ANIOS, MESES, SEMANAS FROM ETAPA_PROYECTO A, CAT_ETAPA B WHERE A.ETAPA_ID = B.ETAPA_ID AND A.FOLIO_PROYECTO = '" + folio + "' AND SERIAL_PROYECTO = " + serial + " ORDER BY A.ETAPA_ID";

            PdfPTable tabPreparacion = ReportUtil.buildTable(con, sql224p, new String[]{"ETAPA_DESCRIPCION", "ANIOS", "MESES", "SEMANAS"}, new String[]{"Etapa", "Año", "Mes", "Semanas"});
            seccion2_2_4.add(tabPreparacion);
            seccion2_2_4.add(new Paragraph("\n"));


            // 2.2.5 Información Bioteconólogica
            if ( ReportUtil.muestraApartado(claveTramite, shSector, "2-2-4-0") ) {
                Chunk sec2_2_5Title = new Chunk("Información Biotecnológica de las especies a cultivar.", TITULO2);
                Section seccion2_2_5 = seccion2_2.addSection(new Paragraph(sec2_2_5Title));
                sec2_2_5Title.setLocalDestination(seccion2_2_5.getTitle().getContent());
                seccion2_2_5.setIndentationLeft(10);
                seccion2_2_5.add(new Paragraph("\n"));
                
                
                String sql225p = "SELECT IP.INFO_BIO_ID, IP.BIO_NOMBRE, IP.BIO_ORIGEN, IP.BIO_NUMORG_CULTIVAR, IP.BIO_DESC_ATRIBUTOS_AME "
                		+ " , CASE  IP.BIO_ESP_FORRAJERA WHEN 'S' THEN 'SI' WHEN 'N' THEN 'N0' ELSE '' END as BIO_ESP_FORRAJERA "
                		+ " , CEA.DESCRIPCION "
                		+ " FROM INFOBIO_PROYECTO IP INNER JOIN  CAT_ESPECIES_ACUIFEROS CEA ON IP.ID_ESPECIES= CEA.ID WHERE FOLIO_PROYECTO = '" + folio + "' AND SERIAL_PROYECTO = " + serial + " ORDER BY info_bio_id";
                ResultSet rsBiotecno = st2.executeQuery(sql225p);                
            
                while (rsBiotecno.next()) {
                    seccion2_2_5.add(new Paragraph(new Chunk("NOMBRE: " + notNullFilter(rsBiotecno.getString("BIO_NOMBRE")))));
                    seccion2_2_5.add(new Paragraph(new Chunk("ORIGEN: " + notNullFilter(rsBiotecno.getString("BIO_ORIGEN")))));
                    seccion2_2_5.add(new Paragraph(new Chunk("TIPO DE ESPECIE: " + notNullFilter(rsBiotecno.getString("DESCRIPCION")))));
                    seccion2_2_5.add(new Paragraph(new Chunk("NO. DE ORGANISMOS A CULTIVAR: " + notNullFilter(rsBiotecno.getString("BIO_NUMORG_CULTIVAR")))));
                    seccion2_2_5.add(new Paragraph(new Chunk("DESCRIPCIÓN DE SUS ATRIBUTOS Y/O AMENAZAS POTENCIALES:")));
                    Paragraph pDescOrgamismos = new Paragraph(notNullFilter(rsBiotecno.getString("BIO_DESC_ATRIBUTOS_AME") + "\r\n"));
                    pDescOrgamismos.setAlignment(Element.ALIGN_JUSTIFIED);   
                    seccion2_2_5.add(pDescOrgamismos);
                    //seccion2_2_5.add(new Paragraph(new Chunk("DESCRIPCIÓN DE SUS ATRIBUTOS Y/O AMENAZAS POTENCIALES: " + notNullFilter(rsBiotecno.getString("BIO_DESC_ATRIBUTOS_AME")))));
                    seccion2_2_5.add(new Paragraph(new Chunk("¿LA ESPECIE ES FORRAJERA?: " + notNullFilter(rsBiotecno.getString("BIO_ESP_FORRAJERA")))));
                    seccion2_2_5.add(new Paragraph("\n"));

                    String sqlBiotecnoCicloVida = "SELECT "
                            + " CASE  BIO_CRIAS WHEN 'S' THEN 'Si' WHEN 'N' THEN 'No' ELSE '' END as BIO_CRIAS "
                            + " , CASE  BIO_SEMILLAS WHEN 'S' THEN 'Si' WHEN 'N' THEN 'No' ELSE '' END as BIO_SEMILLAS "
                            + " , CASE  BIO_POSTLARVAS WHEN 'S' THEN 'Si' WHEN 'N' THEN 'No' ELSE '' END as BIO_POSTLARVAS "
                            + " , CASE  BIO_JUVENILES WHEN 'S' THEN 'Si' WHEN 'N' THEN 'No' ELSE '' END as BIO_JUVENILES "
                            + " , CASE  BIO_ADULT_REPROD WHEN 'S' THEN 'Si' WHEN 'N' THEN 'No' ELSE '' END as BIO_ADULT_REPROD "
                            + "FROM INFOBIO_PROYECTO WHERE FOLIO_PROYECTO = '" + folio + "' AND SERIAL_PROYECTO = " + serial 
                            + " AND INFO_BIO_ID = " + rsBiotecno.getString("INFO_BIO_ID");

                    PdfPTable tabBiotecnoCicloVida = ReportUtil.buildTable(con, sqlBiotecnoCicloVida, new String[]{"BIO_CRIAS", "BIO_SEMILLAS", "BIO_POSTLARVAS", "BIO_JUVENILES", "BIO_ADULT_REPROD"}, new String[]{"Crías", "Semillas", "Portalarvas", "Juveniles", "Adultos reproductivos"});
                    seccion2_2_5.add(tabBiotecnoCicloVida);
                    seccion2_2_5.add(new Paragraph("\n"));

                    String sqlBiotecnoBiomasa = "SELECT BIO_NUMCICLOS, BIO_BIOMASA_INIC, BIO_UNIDAD_INIC, BIO_BIOMASA_ESPERADA, BIO_UNIDAD_ESPERADA "
                            + "FROM INFOBIO_PROYECTO WHERE FOLIO_PROYECTO = '" + folio + "' AND SERIAL_PROYECTO = " + serial 
                            + " AND INFO_BIO_ID = " + rsBiotecno.getString("INFO_BIO_ID");

                    PdfPTable tabBiotecnoBiomasa = ReportUtil.buildTable(con, sqlBiotecnoBiomasa, new String[]{"BIO_NUMCICLOS", "BIO_BIOMASA_INIC", "BIO_UNIDAD_INIC", "BIO_BIOMASA_ESPERADA", "BIO_UNIDAD_ESPERADA"}, new String[]{"No. de ciclos", "Biomasa inicial", "Unidad inicial", "Biomasa esperada", "Unidad esperada"});
                    seccion2_2_5.add(tabBiotecnoBiomasa);
                    seccion2_2_5.add(new Paragraph("\n"));

                    String sqlBiotecnoAlimento = "SELECT BIO_TIPO_ALIMENTO, BIO_CANT_ALIMENTO || ' ' || BIO_UNIDAD_ALIMENTO AS BIO_CANT_ALIMENTO, BIO_FORMA_ALMACEN"
                            + " FROM INFOBIO_PROYECTO WHERE FOLIO_PROYECTO = '" + folio + "' AND SERIAL_PROYECTO = " + serial 
                            + " AND INFO_BIO_ID = " + rsBiotecno.getString("INFO_BIO_ID");

                    PdfPTable tabBiotecnoAlimento = ReportUtil.buildTable(con, sqlBiotecnoAlimento, new String[]{"BIO_TIPO_ALIMENTO", "BIO_CANT_ALIMENTO", "BIO_FORMA_ALMACEN"}, new String[]{"Tipo de alimento", "Cantidad de alimento", "Forma de almacenamiento"});
                    seccion2_2_5.add(tabBiotecnoAlimento);
                    seccion2_2_5.add(new Paragraph("\n"));

                    String sqlBiotecnoAbono = "SELECT BIO_TIPABONO, BIO_USO_ABONO, BIO_CANT_SUMINISTRO || ' ' || BIO_UNIDAD_SUMINISTRO AS BIO_CANT_SUMINISTRO, BIO_FORM_ALMAC_ABONO"
                            + " FROM INFOBIO_PROYECTO WHERE FOLIO_PROYECTO = '" + folio + "' AND SERIAL_PROYECTO = " + serial 
                            + " AND INFO_BIO_ID = " + rsBiotecno.getString("INFO_BIO_ID");

                    PdfPTable tabBiotecnoAbono = ReportUtil.buildTable(con, sqlBiotecnoAbono, new String[]{"BIO_TIPABONO", "BIO_USO_ABONO", "BIO_CANT_SUMINISTRO", "BIO_FORM_ALMAC_ABONO"}, new String[]{"Tipo de abono y/o fertilizante", "Forma de uso del fertilizante", "Cantidad de suministro", "Forma de almacenamiento"});
                    seccion2_2_5.add(tabBiotecnoAbono);
                    seccion2_2_5.add(new Paragraph("\n"));
                    seccion2_2_5.add(new Paragraph("\n"));
                    
                }
                rsBiotecno.close();
            
                
            }
//            Chunk sec2_2_3Title = new Chunk("Descripción de obras principales del proyecto", TITULO2);
//            Section seccion2_2_3 = seccion2_2.addSection(new Paragraph(sec2_2_3Title));
//            sec2_2_3Title.setLocalDestination(seccion2_2_3.getTitle().getContent());
//            seccion2_2_3.setIndentationLeft(10);
//            seccion2_2_3.add(new Paragraph("\n"));
            
            // 2.2.6
            
            
            /**
             * TABLA: 2.2.6 "ETAPAS DEL PROYECTO"
             */
            Chunk sec2_2_6Title = new Chunk("Etapas del proyecto.", TITULO2);
            Section seccion2_2_6 = seccion2_2.addSection(new Paragraph(sec2_2_6Title));
            sec2_2_6Title.setLocalDestination("Descripción de obras principales del proyecto");
            seccion2_2_6.setIndentationLeft(10);
            seccion2_2_6.add(new Paragraph("\n"));
            seccion2_2_6.add(new Paragraph("\n"));
            seccion2_2_6.add(sec2_2_6Title);
            seccion2_2_6.add(new Paragraph("\n"));
            
            short subsector = 0, seccionEtapa = 0;
            String strNsub = rsProy.getString("NSUB");
            
            if (null != strNsub && !strNsub.isEmpty()) {
            	subsector = (short)Short.parseShort(strNsub);
            }
            
            seccionEtapa = (subsector == 4 || subsector == 6) ? (short)5 : (short)4;
            
            Statement stCatEtapas = con.createStatement();
            String sqlCatEtapas = "SELECT ETAPA_ID, ETAPA_DESCRIPCION FROM CAT_ETAPA ORDER BY ETAPA_ID";
            ResultSet rsCatEtapas = stCatEtapas.executeQuery(sqlCatEtapas);

            while (rsCatEtapas.next()) {
            	String srtEtapaId = rsCatEtapas.getString("ETAPA_ID").trim();
            	Short etapaId = Short.parseShort(srtEtapaId);
                String etapa = rsCatEtapas.getString("ETAPA_DESCRIPCION").trim();
                
            	String sql226p = "SELECT ACTIVIDAD_ETAPA_ID, nvl(ACTIVIDAD_NOMBRE,'') ACTIVIDAD_NOMBRE, ACTIVIDAD_DESCRIPCION"
            		+ " FROM ACTIVIDAD_ETAPA"
            		+ " WHERE FOLIO_PROYECTO = '" + folio + "' AND SERIAL_PROYECTO = " + serial
            		+ " AND ETAPA_ID = " + etapaId
            		+ " ORDER BY ACTIVIDAD_ETAPA_ID";
                ResultSet rsActividades = st2.executeQuery(sql226p);
                
                // Muestra título de Etapa encontrada
            	Chunk sec2_2_6_Etapa = new Chunk(notNullFilter(etapa), TITULO3);
            	Section seccion2_2_6_Etapa = seccion2_2_6.addSection(new Paragraph(sec2_2_6_Etapa));
            	sec2_2_6_Etapa.setLocalDestination(seccion2_2_6_Etapa.getTitle().getContent());
            	seccion2_2_6_Etapa.setIndentationLeft(10);
            	seccion2_2_6_Etapa.add(new Paragraph("\n"));
            	
            	PdfPTable tableActividades = new PdfPTable(2);
                float[] tblEtapaColumnWidths = new float[] {5f, 95f};
                tableActividades.setWidths(tblEtapaColumnWidths);
                tableActividades.setWidthPercentage((float) 100);
                
                while (rsActividades.next()) {                   
                    
                    Chunk cTitNombreActiv = new Chunk("Nombre de Actividad: ", TITULO3);
                    String valNombreActiv = notNullFilter(rsActividades.getString("ACTIVIDAD_NOMBRE"));
                    cTitNombreActiv.append(valNombreActiv);
                    
                    PdfPCell valActividadId = new PdfPCell(new Phrase("" + notNullFilter(rsActividades.getString("ACTIVIDAD_ETAPA_ID")), TITULO3));
                    PdfPCell valNombreActividad = new PdfPCell(new Phrase(cTitNombreActiv));
                    PdfPCell valDescripcionActiv = new PdfPCell(new Phrase("" + notNullFilter(rsActividades.getString("ACTIVIDAD_DESCRIPCION"))));
                    
                    // Primera fila: # y Título y valor de "Nombre de Actividad"
                    valActividadId.setRowspan(2);
                    valActividadId.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    valActividadId.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableActividades.addCell(valActividadId);
                    tableActividades.addCell(valNombreActividad);
                    
                    // Segunda fila: Valor de Descripción de la Actividad
                    valDescripcionActiv.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    tableActividades.addCell(valDescripcionActiv);
                }
                rsActividades.close();
                
                // Muestra tabla de Actividades para la Etapa
        		seccion2_2_6.add(tableActividades);
        		seccion2_2_6.add(new Paragraph("\n"));

        		// Muestra título de Adjuntos
        		Chunk sec2_2_6_Anexos = new Chunk("Adjuntos", TITULO3);
            	seccion2_2_6.add(sec2_2_6_Anexos);
            	seccion2_2_6.add(new Paragraph("\n"));
            	
        		// Muestra tabla de Adjuntos para la Etapa encontrada
            	// CAPITULO_ID='2' AND SUBCAPITULO_ID='2' AND SECCION_ID='" + vSeccion + "'";
        		PdfPTable tblArchsEtapa = generarTablaArchivos(folio, serial, url, (short) 2, (short) 2, seccionEtapa, (short) etapaId);
        		seccion2_2_6.add(tblArchsEtapa);
        		seccion2_2_6.add(new Paragraph("\n"));
            }
            rsCatEtapas.close();
            stCatEtapas.close();
            // Cierre de TABLA: 2.2.6 "ETAPAS DEL PROYECTO"
            System.out.println("\nDespués de tabla: Etapas del proyecto.\n");

            
            /**
             * TABLA: 2.2.7 "INSUMOS"
             */
            int etapaAnt = 0, etapaActual = 0; // Se inicializa etapa a 0 para comenzar a recorrer los registros
            
            // 2.2.7 Insumos (2.2.10)
            if (ReportUtil.muestraApartado(claveTramite, shSector, "2-2-5-0|2-2-6-0")) {
                Chunk sec2_2_7Title = new Chunk("Insumos.", TITULO2);
                Section seccion2_2_7 = seccion2_2.addSection(new Paragraph(sec2_2_7Title));
                sec2_2_7Title.setLocalDestination("Descripción de obras principales del proyecto");
                seccion2_2_7.setIndentationLeft(10);
                seccion2_2_7.add(new Paragraph("\n"));

                String sql227 = "WITH REGS_SECUENCIALES AS ("
            		+ "SELECT B.ETAPA_ID, ETAPA_DESCRIPCION, INSUMO_NOM_CMOUN, INSUMO_NOM_TECNICO, INSUMO_ESTADO_FISICO"
            		+ ", INSUMO_CANT_ALM || ' ' || C.CTUN_ABRE AS CANTIDAD_ALMACENADA, INSUMO_CANT_MENSUAL || ' ' || CTUN_ABRE AS CANTIDAD_MENSUAL"
            		+ " FROM INSUMOS_PROYECTO A, CAT_ETAPA B, CATALOGOS.CAT_UNIDAD_MEDIDA C"
            		+ " WHERE A.ETAPA_ID = B.ETAPA_ID AND A.CVE_CLVE = C.CTUN_CLVE"
            		+ " AND A.FOLIO_PROYECTO = '" + folio + "' AND SERIAL_PROYECTO = " + serial
            		+ " ORDER BY B.ETAPA_ID, INSUMOS_ID)"
            		+ " SELECT ROWNUM AS SECUENCIAL, rs.* FROM REGS_SECUENCIALES rs";
                
                ResultSet rsInsumos = st2.executeQuery(sql227);
                etapaAnt = 0; // Se inicializa etapa a 0 para comenzar a recorrer los registros
                
                PdfPTable tableInsumos = new PdfPTable(6);
                float[] tblEtapaInsColumnWidths = new float[] {4f, 25f, 25f, 15f, 16f, 15f};
                tableInsumos.setWidths(tblEtapaInsColumnWidths);
                tableInsumos.setWidthPercentage((float) 100);
                
                PdfPCell titInsumoId = new PdfPCell(new Phrase("#", TITULO3));
                PdfPCell titNombreInsumo = new PdfPCell(new Phrase("Nombre", TITULO3));
                PdfPCell titNombreTecnico = new PdfPCell(new Phrase("Nombre técnico", TITULO3));
                PdfPCell titEstadoFisico = new PdfPCell(new Phrase("Estado físico", TITULO3));
                PdfPCell titCantidadAlm = new PdfPCell(new Phrase("Cantidad almacenada", TITULO3));
                PdfPCell titCantidadMens = new PdfPCell(new Phrase("Cantidad mensual", TITULO3));
                
                while (rsInsumos.next()) {
                	etapaActual = Integer.parseInt(rsInsumos.getString("ETAPA_ID").trim());
                	String etapa = rsInsumos.getString("ETAPA_DESCRIPCION").trim();
                	
                	// Si existe etapa anterior
                    if (etapaAnt != 0) {
                    	// Si se encuentra una etapa distinta de la anterior
                    	if (etapaAnt != etapaActual){
                    		
                    		// Muestra la tabla en el archivo PDF
                    		seccion2_2_7.add(tableInsumos);
                    		seccion2_2_7.add(new Paragraph("\n"));
                    		
                    		// Muestra el título de la siguiente etapa encontrada
                        	Chunk sec2_2_7_Etapa = new Chunk(notNullFilter(etapa), TITULO3);
                        	Section seccion2_2_7_Etapa = seccion2_2_7.addSection(new Paragraph(sec2_2_7_Etapa));
                        	sec2_2_7_Etapa.setLocalDestination(seccion2_2_7_Etapa.getTitle().getContent());
                        	seccion2_2_7_Etapa.setIndentationLeft(10);
                        	seccion2_2_7_Etapa.add(new Paragraph("\n"));
                        	
                        	// Actualiza valor de etapa anterior
                        	etapaAnt = etapaActual;
                    		
                            // Se crea una nueva tabla para agregar datos de la etapa encontrada
                            tableInsumos = new PdfPTable(6);
                            tableInsumos.setWidths(tblEtapaInsColumnWidths);
                            tableInsumos.setWidthPercentage((float) 100);
                            
                            // Fila con Títulos
                            titInsumoId.setHorizontalAlignment(Element.ALIGN_CENTER);
                            tableInsumos.addCell(titInsumoId);
                            titNombreInsumo.setHorizontalAlignment(Element.ALIGN_CENTER);
                            tableInsumos.addCell(titNombreInsumo);
                            titNombreTecnico.setHorizontalAlignment(Element.ALIGN_CENTER);
                            tableInsumos.addCell(titNombreTecnico);
                            titEstadoFisico.setHorizontalAlignment(Element.ALIGN_CENTER);
                            tableInsumos.addCell(titEstadoFisico);
                            titCantidadAlm.setHorizontalAlignment(Element.ALIGN_CENTER);
                            tableInsumos.addCell(titCantidadAlm);
                            titCantidadMens.setHorizontalAlignment(Element.ALIGN_CENTER);
                            tableInsumos.addCell(titCantidadMens);
                    	}
                    }
                    else
                    {
                    	// Muestra el título de la primera etapa encontrada
                    	Chunk sec2_2_7_Etapa = new Chunk(notNullFilter(etapa), TITULO3);
                    	Section seccion2_2_7_Etapa = seccion2_2_7.addSection(new Paragraph(sec2_2_7_Etapa));
                    	sec2_2_7_Etapa.setLocalDestination(seccion2_2_7_Etapa.getTitle().getContent());
                    	seccion2_2_7_Etapa.setIndentationLeft(10);
                    	seccion2_2_7_Etapa.add(new Paragraph("\n"));
                    	
                    	// Actualiza valor de etapa anterior
                    	etapaAnt = etapaActual;
                    	
                    	// Fila con Títulos
                        titInsumoId.setHorizontalAlignment(Element.ALIGN_CENTER);
                        tableInsumos.addCell(titInsumoId);
                        titNombreInsumo.setHorizontalAlignment(Element.ALIGN_CENTER);
                        tableInsumos.addCell(titNombreInsumo);
                        titNombreTecnico.setHorizontalAlignment(Element.ALIGN_CENTER);
                        tableInsumos.addCell(titNombreTecnico);
                        titEstadoFisico.setHorizontalAlignment(Element.ALIGN_CENTER);
                        tableInsumos.addCell(titEstadoFisico);
                        titCantidadAlm.setHorizontalAlignment(Element.ALIGN_CENTER);
                        tableInsumos.addCell(titCantidadAlm);
                        titCantidadMens.setHorizontalAlignment(Element.ALIGN_CENTER);
                        tableInsumos.addCell(titCantidadMens);
                    }
                    
                    PdfPCell valInsumoId = new PdfPCell(new Phrase("" + notNullFilter(rsInsumos.getString("SECUENCIAL")), TITULO3));
                    PdfPCell valNombreComun = new PdfPCell(new Phrase("" + notNullFilter(rsInsumos.getString("INSUMO_NOM_CMOUN"))));
                    PdfPCell valNombreTecnico = new PdfPCell(new Phrase("" + notNullFilter(rsInsumos.getString("INSUMO_NOM_TECNICO"))));
                    PdfPCell valEstadoFisico = new PdfPCell(new Phrase("" + notNullFilter(rsInsumos.getString("INSUMO_ESTADO_FISICO"))));
                    PdfPCell valCantidadAlm = new PdfPCell(new Phrase("" + notNullFilter(rsInsumos.getString("CANTIDAD_ALMACENADA"))));
                    PdfPCell valCantidadMens = new PdfPCell(new Phrase("" + notNullFilter(rsInsumos.getString("CANTIDAD_MENSUAL"))));
                    
                    // Filas con Valores
                    valInsumoId.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    valInsumoId.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableInsumos.addCell(valInsumoId);
                    tableInsumos.addCell(valNombreComun);
                    tableInsumos.addCell(valNombreTecnico);
                    tableInsumos.addCell(valEstadoFisico);
                    valCantidadAlm.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableInsumos.addCell(valCantidadAlm);
                    valCantidadMens.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableInsumos.addCell(valCantidadMens);
                }
                rsInsumos.close();
                
                // Muestra, en el archivo PDF, la última tabla generada
                seccion2_2_7.add(tableInsumos);
                seccion2_2_7.add(new Paragraph("\n"));
            }
            // Cierre de TABLA: 2.2.7 "INSUMOS"
            System.out.println("\nDespués de tabla: Insumos.\n");

            
            /**
             * TABLA: 2.2.8 "SUSTANCIAS RIESGOSAS"
             */
            // 2.2.8 Sustancias peligrosas (2.2.11)
            Chunk sec2_2_8Title = new Chunk("Sustancias riesgosas.", TITULO2);
            Section seccion2_2_8 = seccion2_2.addSection(new Paragraph(sec2_2_8Title));
            sec2_2_8Title.setLocalDestination(seccion2_2_8.getTitle().getContent());
            seccion2_2_8.setIndentationLeft(10);
            seccion2_2_8.add(new Paragraph("\n"));

            String sql228 = "WITH REGS_SECUENCIALES AS ("
        		+ " SELECT ETAPA_ID, ETAPA_DESCRIPCION, SUSTANCIA_DESCRIPCION, SUSTANCIA_CANTIDAD_ALMACENADA || ' ' || CTUN_DESC AS CANTIDAD_UNIDAD"
        		+ " , AP.SEQ_ID, AP.FILENAME || '.' || AP.EXTENSION AS NOMBRE_ARCHIVO"
        		+ " FROM ("
        		+ "SELECT B.ETAPA_ID, FOLIO_PROYECTO, SERIAL_PROYECTO, SUST_PROY_ID, ID_ARCHIVO_PROGRAMA, ETAPA_DESCRIPCION,"
        		+ " CASE A.SUSTANCIA_ID WHEN 9999 THEN SUSTANCIA_PROMOVENTE ELSE SUSTANCIA_DESCRIPCION END AS SUSTANCIA_DESCRIPCION,"
        		+ " SUSTANCIA_CANTIDAD_ALMACENADA,"
        		+ " (SELECT CTUN_DESC FROM CATALOGOS.CAT_UNIDAD_MEDIDA D WHERE C.SUSTANCIA_UNIDAD = D.CTUN_CLVE) AS CTUN_DESC"
        		+ " FROM SUSTANCIA_PROYECTO A, CAT_ETAPA B, CAT_SUSTANCIA_ALTAM_RIESGOSA C"
        		+ " WHERE A.ETAPA_ID = B.ETAPA_ID AND A.SUSTANCIA_ID = C.SUSTANCIA_ID"
        		+ " AND A.FOLIO_PROYECTO = '" + folio + "' AND A.SERIAL_PROYECTO = " + serial
        		+ ") SUST"
        		+ " LEFT JOIN ARCHIVOS_PROYECTO AP ON SUST.ID_ARCHIVO_PROGRAMA = AP.SEQ_ID"
        		+ " ORDER BY SUST.ETAPA_ID, SUST_PROY_ID"
        		+ ")"
        		+ " SELECT ROWNUM AS SECUENCIAL, rs.* FROM REGS_SECUENCIALES rs";

            ResultSet rsSustancias = st2.executeQuery(sql228);
            etapaAnt = 0;
            
            PdfPTable tableSustancias = new PdfPTable(4);
            float[] tblEtapaSustColumnWidths = new float[] {4f, 38f, 18f, 40f};
            tableSustancias.setWidths(tblEtapaSustColumnWidths);
            tableSustancias.setWidthPercentage((float) 100);
            
            PdfPCell titSustanciaId = new PdfPCell(new Phrase("#", TITULO3));
            PdfPCell titTipoSustancia = new PdfPCell(new Phrase("Tipo de Sustancia", TITULO3));
            PdfPCell titCantidadAlm = new PdfPCell(new Phrase("Cantidad Almacenada", TITULO3));
            PdfPCell titHojaSeguridad = new PdfPCell(new Phrase("Hoja de Seguridad", TITULO3));
            
            while (rsSustancias.next()) {
            	etapaActual = Integer.parseInt(rsSustancias.getString("ETAPA_ID").trim());
            	String etapa = rsSustancias.getString("ETAPA_DESCRIPCION").trim();
            	
            	// Si existe etapa anterior
                if (etapaAnt != 0) {
                	// Si se encuentra una etapa distinta de la anterior
                	if (etapaAnt != etapaActual){
                		
                		// Muestra tabla de Sustancias para la Etapa encontrada
                		seccion2_2_8.add(tableSustancias);
                		seccion2_2_8.add(new Paragraph("\n"));
                		
                		// Muestra título de Etapa encontrada
                    	Chunk sec2_2_8_Etapa = new Chunk(notNullFilter(etapa), TITULO3);
                    	Section seccion2_2_8_Etapa = seccion2_2_8.addSection(new Paragraph(sec2_2_8_Etapa));
                    	sec2_2_8_Etapa.setLocalDestination(seccion2_2_8_Etapa.getTitle().getContent());
                    	seccion2_2_8_Etapa.setIndentationLeft(10);
                    	seccion2_2_8_Etapa.add(new Paragraph("\n"));
                    	
                    	// Actualiza valor de etapa anterior
                    	etapaAnt = etapaActual;
                		
                        // Se crea una nueva tabla para agregar datos de la etapa encontrada
                        tableSustancias = new PdfPTable(4);
                        tableSustancias.setWidths(tblEtapaSustColumnWidths);
                        tableSustancias.setWidthPercentage((float) 100);
                        
                        // Fila con Títulos
                        titSustanciaId.setHorizontalAlignment(Element.ALIGN_CENTER);
                        tableSustancias.addCell(titSustanciaId);
                        titTipoSustancia.setHorizontalAlignment(Element.ALIGN_CENTER);
                        tableSustancias.addCell(titTipoSustancia);
                        titCantidadAlm.setHorizontalAlignment(Element.ALIGN_CENTER);
                        tableSustancias.addCell(titCantidadAlm);
                        titHojaSeguridad.setHorizontalAlignment(Element.ALIGN_CENTER);
                        tableSustancias.addCell(titHojaSeguridad);
                	}
                }
                else
                {
                	// Muestra título de primera Etapa encontrada
                	Chunk sec2_2_8_Etapa = new Chunk(notNullFilter(etapa), TITULO3);
                	Section seccion2_2_8_Etapa = seccion2_2_8.addSection(new Paragraph(sec2_2_8_Etapa));
                	sec2_2_8_Etapa.setLocalDestination(seccion2_2_8_Etapa.getTitle().getContent());
                	seccion2_2_8_Etapa.setIndentationLeft(10);
                	seccion2_2_8_Etapa.add(new Paragraph("\n"));
                	
                	// Actualiza valor de etapa anterior
                	etapaAnt = etapaActual;
                	
                	// Fila con Títulos
                    titSustanciaId.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableSustancias.addCell(titSustanciaId);
                    titTipoSustancia.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableSustancias.addCell(titTipoSustancia);
                    titCantidadAlm.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableSustancias.addCell(titCantidadAlm);
                    titHojaSeguridad.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableSustancias.addCell(titHojaSeguridad);
                }
                
                PdfPCell valSustanciaId = new PdfPCell(new Phrase("" + notNullFilter(rsSustancias.getString("SECUENCIAL")), TITULO3));
                PdfPCell valTipoSustancia = new PdfPCell(new Phrase("" + notNullFilter(rsSustancias.getString("SUSTANCIA_DESCRIPCION"))));
                PdfPCell valCantidadAlm = new PdfPCell(new Phrase("" + notNullFilter(rsSustancias.getString("CANTIDAD_UNIDAD"))));

                PdfPCell ligaHojaSeg = new PdfPCell(new Phrase("Sin adjunto", PARRAFO));
                ligaHojaSeg.setHorizontalAlignment(Element.ALIGN_CENTER);
                String id = rsSustancias.getString("SEQ_ID");
                if (null != id){
                    String filename = rsSustancias.getString("NOMBRE_ARCHIVO");
                    if (null != filename) {
                    	ligaHojaSeg = generarCeldaArchivo(filename, url, id.trim());
                    }
            	}

                // Filas con Valores
                valSustanciaId.setVerticalAlignment(Element.ALIGN_MIDDLE);
                titSustanciaId.setHorizontalAlignment(Element.ALIGN_CENTER);
                tableSustancias.addCell(valSustanciaId);
                tableSustancias.addCell(valTipoSustancia);
                tableSustancias.addCell(valCantidadAlm);
                tableSustancias.addCell(ligaHojaSeg);
            }
            rsSustancias.close();
            
            // Muestra tabla de Sustancias para la última etapa encontrada
            seccion2_2_8.add(tableSustancias);
            seccion2_2_8.add(new Paragraph("\n"));
            // Cierre de TABLA: 2.2.8 "SUSTANCIAS RIESGOSAS"
            System.out.println("\nDespués de tabla: Sustancias riesgosas.\n");
            

            /**
             * TABLA: 2.2.9 "EXPLOSIVOS"
             */
            // 2.2.9 Explosivos(2.2.12) si aplica el apartado se pinta
            if (ReportUtil.muestraApartado(claveTramite, shSector, "2-2-6-0|2-2-7-0")) {
                // 2.2.9 Explosivos (2.2.11)
                Chunk sec2_2_9Title = new Chunk("Explosivos.", TITULO2);
                Section seccion2_2_9 = seccion2_2.addSection(new Paragraph(sec2_2_9Title));
                sec2_2_9Title.setLocalDestination(seccion2_2_9.getTitle().getContent());
                seccion2_2_9.setIndentationLeft(10);
                seccion2_2_9.add(new Paragraph("\n"));
                
                if (rsProy.getString("PROY_EXPLOSIVOS_APLICA") != null && rsProy.getString("PROY_EXPLOSIVOS_APLICA").compareTo("S") == 0) {

                    String sql229 = "WITH REGS_SECUENCIALES AS ("
                    		+ " SELECT A.ETAPA_ID, C.ETAPA_DESCRIPCION, B.ACTIVIDAD_NOMBRE, A.ACTIVIDAD_JUSTIFICACION, A.ACTIVIDAD_METODOLOGIA"
                    		+ " FROM EXPLOSIVO_ACTIVIDAD_ETAPA A, ACTIVIDAD_ETAPA B, CAT_ETAPA C"
                    		+ " WHERE A.ACTIVIDAD_ETAPA_ID = B.ACTIVIDAD_ETAPA_ID AND A.ETAPA_ID = B.ETAPA_ID"
                    		+ " AND A.FOLIO_PROYECTO  = B.FOLIO_PROYECTO AND A.SERIAL_PROYECTO = B.SERIAL_PROYECTO"
                    		+ " AND A.ETAPA_ID = C.ETAPA_ID"
                    		+ " AND A.FOLIO_PROYECTO = '" + folio + "' AND A.SERIAL_PROYECTO = " + serial 
                    		+ " ORDER BY A.ETAPA_ID, A.EXPLOSIVO_ETAPA_ID"
                    		+ ")"
                    		+ " SELECT ROWNUM AS SECUENCIAL, rs.* FROM REGS_SECUENCIALES rs";
    
                    ResultSet rsExplosivos = st2.executeQuery(sql229);
                    etapaAnt = 0; // Se inicializa etapa a 0 para comenzar a recorrer los registros
                    
                    PdfPTable tableExplosivos = new PdfPTable(4);
                    float[] tblEtapaExpColumnWidths = new float[] {4f, 20f, 38f, 38f};
                    tableExplosivos.setWidths(tblEtapaExpColumnWidths);
                    tableExplosivos.setWidthPercentage((float) 100);
                    
                    PdfPCell titExplosivoId = new PdfPCell(new Phrase("#", TITULO3));
                    PdfPCell titActividad = new PdfPCell(new Phrase("Actividad", TITULO3));
                    PdfPCell titJustificacion = new PdfPCell(new Phrase("Justificación", TITULO3));
                    PdfPCell titMetodologia = new PdfPCell(new Phrase("Metodología", TITULO3));
                    
                    while (rsExplosivos.next()) {
                    	etapaActual = Integer.parseInt(rsExplosivos.getString("ETAPA_ID").trim());
                    	String etapa = rsExplosivos.getString("ETAPA_DESCRIPCION").trim();
                    	
                    	// Si existe etapa anterior
                        if (etapaAnt != 0) {
                        	// Si se encuentra una etapa distinta de la anterior
                        	if (etapaAnt != etapaActual){
                        		
                        		// Muestra la tabla en el archivo PDF
                        		seccion2_2_9.add(tableExplosivos);
                        		seccion2_2_9.add(new Paragraph("\n"));
                        		
                        		// Muestra el título de la siguiente etapa encontrada
                            	Chunk sec2_2_9_Etapa = new Chunk(notNullFilter(etapa), TITULO3);
                            	Section seccion2_2_9_Etapa = seccion2_2_9.addSection(new Paragraph(sec2_2_9_Etapa));
                            	sec2_2_9_Etapa.setLocalDestination(seccion2_2_9_Etapa.getTitle().getContent());
                            	seccion2_2_9_Etapa.setIndentationLeft(10);
                            	seccion2_2_9_Etapa.add(new Paragraph("\n"));
                            	
                            	// Actualiza valor de etapa anterior
                            	etapaAnt = etapaActual;
                        		
                                // Se crea una nueva tabla para agregar datos de la etapa encontrada
                                tableExplosivos = new PdfPTable(4);
                                tableExplosivos.setWidths(tblEtapaExpColumnWidths);
                                tableExplosivos.setWidthPercentage((float) 100);
                                
                                // Fila con Títulos
                                titExplosivoId.setHorizontalAlignment(Element.ALIGN_CENTER);
                                tableExplosivos.addCell(titExplosivoId);
                                titActividad.setHorizontalAlignment(Element.ALIGN_CENTER);
                                tableExplosivos.addCell(titActividad);
                                titJustificacion.setHorizontalAlignment(Element.ALIGN_CENTER);
                                tableExplosivos.addCell(titJustificacion);
                                titMetodologia.setHorizontalAlignment(Element.ALIGN_CENTER);
                                tableExplosivos.addCell(titMetodologia);
                        	}
                        }
                        else
                        {
                        	// Muestra el título de la primera etapa encontrada
                        	Chunk sec2_2_9_Etapa = new Chunk(notNullFilter(etapa), TITULO3);
                        	Section seccion2_2_9_Etapa = seccion2_2_9.addSection(new Paragraph(sec2_2_9_Etapa));
                        	sec2_2_9_Etapa.setLocalDestination(seccion2_2_9_Etapa.getTitle().getContent());
                        	seccion2_2_9_Etapa.setIndentationLeft(10);
                        	seccion2_2_9_Etapa.add(new Paragraph("\n"));
                        	
                        	// Actualiza valor de etapa anterior
                        	etapaAnt = etapaActual;
                        	
                        	// Fila con Títulos
                            titExplosivoId.setHorizontalAlignment(Element.ALIGN_CENTER);
                            tableExplosivos.addCell(titExplosivoId);
                            titActividad.setHorizontalAlignment(Element.ALIGN_CENTER);
                            tableExplosivos.addCell(titActividad);
                            titJustificacion.setHorizontalAlignment(Element.ALIGN_CENTER);
                            tableExplosivos.addCell(titJustificacion);
                            titMetodologia.setHorizontalAlignment(Element.ALIGN_CENTER);
                            tableExplosivos.addCell(titMetodologia);
                        }
                        
                        PdfPCell valExplosivoId = new PdfPCell(new Phrase("" + notNullFilter(rsExplosivos.getString("SECUENCIAL")), TITULO3));
                        PdfPCell valNombreActividad = new PdfPCell(new Phrase("" + notNullFilter(rsExplosivos.getString("ACTIVIDAD_NOMBRE"))));
                        PdfPCell valJustificacion = new PdfPCell(new Phrase("" + notNullFilter(rsExplosivos.getString("ACTIVIDAD_JUSTIFICACION"))));
                        PdfPCell valMetodologia = new PdfPCell(new Phrase("" + notNullFilter(rsExplosivos.getString("ACTIVIDAD_METODOLOGIA"))));
                        
                        // Filas con Valores
                        valExplosivoId.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        valExplosivoId.setHorizontalAlignment(Element.ALIGN_CENTER);
                        tableExplosivos.addCell(valExplosivoId);
                        //valNombreActividad.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                        tableExplosivos.addCell(valNombreActividad);
                        valJustificacion.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                        tableExplosivos.addCell(valJustificacion);
                        valMetodologia.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                        tableExplosivos.addCell(valMetodologia);
                    }
                    rsExplosivos.close();
                    
                    // Muestra, en el archivo PDF, la última tabla generada
                    seccion2_2_9.add(tableExplosivos);
            		seccion2_2_9.add(new Paragraph("\n"));
                } else {
                    String strExplosivos = "<p style=\"text-align: justify;\">No requiere uso de explosivos</p>";
                    Paragraph pExplosivos = ReportUtil.parseHtmlParagraph(strExplosivos);
                    seccion2_2_9.add(pExplosivos);
                    seccion2_2_9.add(new Paragraph("\n"));
                }
            }
            // Cierre de TABLA: 2.2.9 "EXPLOSIVOS"
            System.out.println("\nDespués de tabla: Explosivos.\n");

            
            
            //====================================== REPORTE 2.2.10 RESIDUOS (2.2.13) =============================================================00         

            Chunk sec2_2_10Title = new Chunk("Generación, Manejo y Disposición de Residuos Sólidos, Líquidos y Emisiones a la Atmósfera.", TITULO2);
            Section seccion2_2_10 = seccion2_2.addSection(new Paragraph(sec2_2_10Title));
            sec2_2_10Title.setLocalDestination(seccion2_2_10.getTitle().getContent());
            seccion2_2_10.setIndentationLeft(10);
            seccion2_2_10.add(new Paragraph("\n"));
            
            int totalColumnas = 4;
            PdfPTable tblResiduos = new PdfPTable(totalColumnas);
            tblResiduos.setWidthPercentage((float) 100);
            float[] tblResiduosColumnWidths = new float[] {7f, 25f, 30f, 38};
            tblResiduos.setWidths(tblResiduosColumnWidths);
            
            PdfPCell titTipoContaminante = new PdfPCell(new Phrase("Tipo", TITULO4));
            PdfPCell titERD_Nombre = new PdfPCell(new Phrase("Emisiones, residuos o descargas", TITULO4));
            PdfPCell titCantidad = new PdfPCell(new Phrase("Cantidad", TITULO4));
			PdfPCell titFuenteEmisora = new PdfPCell(new Phrase("Fuente Emisora", TITULO4));
			PdfPCell titDescEmiResDes = new PdfPCell(new Phrase("Descripción de emisiones, residuos o descargas", TITULO4));
			PdfPCell titManejoDispos = new PdfPCell(new Phrase("Manejo/disposición de los residuos o emisiones", TITULO4));
            
            String sqlResiduos = "WITH REGS_SECUENCIALES AS ("
        		+ "SELECT CP.ETAPA_ID, TRIM(CE.ETAPA_DESCRIPCION) AS NOMBRE_ETAPA, TRIM(CT.TIPO_CONTAMINANTE_DESCRIPCION) AS TIPO, TRIM(CC.CONTAMINANTE_NOMBRE) AS CONTAMINANTE_NOMBRE"
                + ", DESC_EMI_RESI_DES, CONTAMINANTE_CANTIDAD || ' ' || CU.CTUN_DESC AS CANTIDAD, CP.FUENTE_EMISORA, CP.OBSERVACIONES"
                + " FROM CONTAMINANTE_PROYECTO CP, CAT_ETAPA CE, CAT_CONTAMINANTE CC, CAT_TIPO_CONTAMINANTE CT, CATALOGOS.CAT_UNIDAD_MEDIDA CU"
                + " WHERE CP.CONTAMINANTE_EMISIONES = CC.CONTAMINANTE_ID AND CP.ETAPA_ID = CE.ETAPA_ID"
                + " AND CP.CONTAMINANTE_TIPO = CT.TIPO_CONTAMINANTE_ID AND CP.CTUN_CLAVE = CU.CTUN_CLVE"
                + " AND CP.FOLIO_PROYECTO = '" + folio + "' AND CP.SERIAL_PROYECTO = " + serial
                + " ORDER BY CP.ETAPA_ID, CP.CONTAMINANTE_ID)"
                + " SELECT ROWNUM AS SECUENCIAL, rs.* FROM REGS_SECUENCIALES rs";

            //Statement statementResiduos = con.createStatement();
            ResultSet rsResiduos = st2.executeQuery(sqlResiduos);
            etapaAnt = 0;
            
            while (rsResiduos.next()) {
            	etapaActual = Integer.parseInt(rsResiduos.getString("ETAPA_ID"));
            	String etapa = rsResiduos.getString("NOMBRE_ETAPA");
            	
            	// Si existe etapa anterior
            	if (etapaAnt != 0) {
            		
            		// Si se encuentra etapa distinta de la anterior
            		if (etapaActual != etapaAnt) {
            			
            			// Actualiza valor de etapa anterior
            			etapaAnt = etapaActual;
            			
            			// Muestra tabla de Residuos para la etapa anterior encontrada
            			seccion2_2_10.add(tblResiduos);
            			seccion2_2_10.add(new Paragraph("\n"));
            			
            			// Muestra título de etapa encontrada
            			Chunk sec2_2_10_Etapa = new Chunk(notNullFilter(etapa), TITULO3);
                		Section seccion2_2_10_Etapa = seccion2_2_10.addSection(new Paragraph(sec2_2_10_Etapa));
                    	sec2_2_10_Etapa.setLocalDestination(seccion2_2_10_Etapa.getTitle().getContent());
                    	seccion2_2_10_Etapa.setIndentationLeft(10);
                    	seccion2_2_10_Etapa.add(new Paragraph("\n"));
            			
            			// Se crea una nueva tabla para agregar datos de la etapa encontrada
            			tblResiduos = new PdfPTable(totalColumnas);
            			tblResiduos.setWidthPercentage((float) 100);
                        tblResiduos.setWidths(tblResiduosColumnWidths);
            		}
            	}
            	else {
            		
            		// Actualiza valor de etapa anterior
        			etapaAnt = etapaActual;
            		
        			// Muestra título de primera Etapa encontrada
        			Chunk sec2_2_10_Etapa = new Chunk(notNullFilter(etapa), TITULO3);
            		Section seccion2_2_10_Etapa = seccion2_2_10.addSection(new Paragraph(sec2_2_10_Etapa));
                	sec2_2_10_Etapa.setLocalDestination(seccion2_2_10_Etapa.getTitle().getContent());
                	seccion2_2_10_Etapa.setIndentationLeft(10);
                	seccion2_2_10_Etapa.add(new Paragraph("\n"));
            	}

            	String descEmiResDes = rsResiduos.getString("DESC_EMI_RESI_DES");
            	PdfPCell valResiduoId = new PdfPCell(new Phrase("" + notNullFilter(rsResiduos.getString("SECUENCIAL")), TITULO3));
                PdfPCell valorTipo = new PdfPCell(new Phrase("" + notNullFilter(rsResiduos.getString("TIPO"))));
                PdfPCell valERD_Nombre = new PdfPCell(new Phrase("" + notNullFilter(rsResiduos.getString("CONTAMINANTE_NOMBRE"))));
                PdfPCell valCantidad = new PdfPCell(new Phrase("" + notNullFilter(rsResiduos.getString("CANTIDAD"))));
                PdfPCell valFuenteEmisora = new PdfPCell(new Phrase("" + notNullFilter(rsResiduos.getString("FUENTE_EMISORA"))));
                PdfPCell valManejoDispos = new PdfPCell(new Phrase("" + notNullFilter(rsResiduos.getString("OBSERVACIONES"))));
                
                // Valor "#"
                if (descEmiResDes != null) {
                	valResiduoId.setRowspan(8);
                }
                else {
                	valResiduoId.setRowspan(6);
                }
                valResiduoId.setHorizontalAlignment(Element.ALIGN_CENTER);
                tblResiduos.addCell(valResiduoId);

                // P R I M E R A   F I L A
                // Título "Tipo"
                tblResiduos.addCell(titTipoContaminante);
                // Título "Emisiones, Residuos o Descargas"
                tblResiduos.addCell(titERD_Nombre);
                // Título "Cantidad"
    			tblResiduos.addCell(titCantidad);
    			
    			// S E G U N D A  F I L A
                // Valor Tipo
                tblResiduos.addCell(valorTipo);
                // Valor Emisiones, Residuos o Descargas
                tblResiduos.addCell(valERD_Nombre);
                // Valor Cantidad
				tblResiduos.addCell(valCantidad);
				
				// T E R C E R A  F I L A
				titFuenteEmisora.setColspan(totalColumnas);
				tblResiduos.addCell(titFuenteEmisora);
                
                // C U A R T A  F I L A
                valFuenteEmisora.setColspan(totalColumnas);
                valFuenteEmisora.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                tblResiduos.addCell(valFuenteEmisora);
                
                if (descEmiResDes != null) {
                	PdfPCell valDescEmiResDes = new PdfPCell(new Phrase(descEmiResDes));
                	
                	// Q U I N T A  F I L A
    				titDescEmiResDes.setColspan(totalColumnas);
                    tblResiduos.addCell(titDescEmiResDes);
                    
                    // S E X T A  F I L A
                    valDescEmiResDes.setColspan(totalColumnas);
                    valDescEmiResDes.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    tblResiduos.addCell(valDescEmiResDes);
                }
                
				// S E P T I M A  F I L A
                titManejoDispos.setColspan(totalColumnas);
                tblResiduos.addCell(titManejoDispos);
				
				// O C T A V A  F I L A
                valManejoDispos.setColspan(totalColumnas);
				valManejoDispos.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
				tblResiduos.addCell(valManejoDispos);
            }
            rsResiduos.close();
            
            // Muestra tabla de Residuos para la última etapa encontrada
            seccion2_2_10.add(tblResiduos);
            seccion2_2_10.add(new Paragraph("\n"));
            
            // Cierre de TABLA: "RESIDUOS"
            System.out.println("\nDespués de tabla: Generación, Manejo y Disposición de Residuos Sólidos, Líquidos y Emisiones a la Atmósfera.\n");
 
            

            //====================================== POSIBLES ACCIDENTES AMBIENTALES =============================================================

            // 2.2.11 Accidentes (2.2.14)
            if (ReportUtil.muestraApartado(claveTramite, shSector, "2-1-8-0")) {
                Chunk sec2_2_11Title = new Chunk("Posibles accidentes ambientales.", TITULO2);
                Section seccion2_2_11 = seccion2_2.addSection(new Paragraph(sec2_2_11Title));
                sec2_2_11Title.setLocalDestination(seccion2_2_11.getTitle().getContent());
                seccion2_2_11.setIndentationLeft(10);
                seccion2_2_11.add(new Paragraph("\n"));

                String sql2211 = "SELECT TIPO_ACCIDENTE, EFECTOS_SECUNDARIOS FROM ACCIDENTES_PROYECTO A WHERE A.FOLIO_PROYECTO = '" + folio + "' AND SERIAL_PROYECTO = " + serial;
                PdfPTable tabAcc = ReportUtil.buildTable(con, sql2211, new String[]{"TIPO_ACCIDENTE", "EFECTOS_SECUNDARIOS"}, new String[]{"Tipo de accidente", "Efectos secundarios, medidas de seguridad y plan de contigencia"});
                seccion2_2_11.add(tabAcc);
                seccion2_2_11.add(new Paragraph("\n"));

            }
            content.add(chapter2);
            chapterList.add(chapter2);
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Capitulo 3">
            // Capitulo 3.
            Chunk chapTitle3 = new Chunk("Vinculación con los ordenamientos jurídicos aplicables en materia ambiental y, en su caso, con la regulación de uso de suelo.", TITULO1);
            Chapter chapter3 = new Chapter(new Paragraph(chapTitle3), 3);
            chapTitle3.setLocalDestination(chapter3.getTitle().getContent());

            //====================================== REPORTE 3.1. LEYES FEDERALES Y/O ESTATALES =============================================================         
            
            Chunk sec3_1Title = new Chunk("Leyes Federales y/o Estatales", TITULO2);
            Section seccion3_1 = chapter3.addSection(new Paragraph(sec3_1Title));
            sec3_1Title.setLocalDestination(seccion3_1.getTitle().getContent());
            seccion3_1.setIndentationLeft(10);
            seccion3_1.add(new Paragraph("\n"));
            seccion3_1.add(new Paragraph("\n"));

            //====================================== REPORTE 3.1.1. LEYES FEDERALES =========================================================================
            
            Chunk sec3_1_1Title = new Chunk("Federales", TITULO2);
            Section seccion3_1_1 = seccion3_1.addSection(new Paragraph(sec3_1_1Title));
            sec3_1_1Title.setLocalDestination(seccion3_1_1.getTitle().getContent());
            seccion3_1_1.setIndentationLeft(10);
            seccion3_1_1.add(new Paragraph("\n"));

            //SE CREA LA ESTRUCTURA DE LA TABLA DEL PDF CON LOS ENCABEZADOS.
            
            PdfPTable tab3_1_1 = new PdfPTable(5);
            tab3_1_1.setWidthPercentage((float) 100);
            float[] tblLeyFedColumnWidths = new float[] {30f, 20f, 15f, 15f, 20f};
            tab3_1_1.setWidths(tblLeyFedColumnWidths);            
            
            PdfPCell titLeyFed = new PdfPCell(new Phrase("Ley", TITULO3));
            PdfPCell titUltimaActualizacionLeyFed = new PdfPCell(new Phrase("Última Actualización", TITULO3));
            PdfPCell titArticuloLeyFed = new PdfPCell(new Phrase("Artículo", TITULO3));
            PdfPCell titFraccionIncisoLeyFed = new PdfPCell(new Phrase("Fracción e Inciso", TITULO3));
            PdfPCell titVinculacionLeyFed = new PdfPCell(new Phrase("Vinculación", TITULO3));
            
            String sql3_1_1 = "SELECT LF.*, AP.FILENAME || '.' || AP.EXTENSION AS NOMBRE_ARCHIVO"
        		+ " FROM ("
        		+ "SELECT E.LEY_FE_ID, C.LEY_NOMBRE, E.FECHA_ULTIMA_ACTUALIZACION AS FECHA_ULTIMA_ACT, E.LEY_FE_ARTICULO, E.LEY_FE_FRACCION AS LEY_FRACCION"
        		+ ", E.LEY_FE_INCISO AS LEY_INCISO, E.LEY_FE_VINCULACION AS LEY_VINCULACION, E.ID_ARCHIVO_PROGRAMA"
        		+ " FROM LEY_FED_EST_PROYECTO E, CAT_LEY C"
        		+ " WHERE C.LEY_ID = E.LEY_ID AND E.FOLIO_PROYECTO = " + folio + " AND E.SERIAL_PROYECTO = " + serial + " AND E.LEY_FE_BANDERA = 'F'"
        		+ ") LF"
        		+ " LEFT JOIN ARCHIVOS_PROYECTO AP ON LF.ID_ARCHIVO_PROGRAMA = AP.SEQ_ID"
        		+ " ORDER BY LF.LEY_FE_ID";

            //SE EJECUTA EL QUERY PARA CONSULTAR LOS REGISTROS DE LA LEY FEDERAL
            ResultSet rsCriteriosLeyesFederales = st2.executeQuery(sql3_1_1);

            if (rsCriteriosLeyesFederales != null) {
                while (rsCriteriosLeyesFederales.next()) {
                    PdfPCell valNombreLeyFed = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosLeyesFederales.getString("LEY_NOMBRE"))));
                    PdfPCell valUltimaActLeyFed = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosLeyesFederales.getString("FECHA_ULTIMA_ACT"))));
                    PdfPCell valArticuloLeyFed = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosLeyesFederales.getString("LEY_FE_ARTICULO"))));
                    
                    String fraccionLeyFed = notNullFilter(rsCriteriosLeyesFederales.getString("LEY_FRACCION"));
                    String incisoLeyFed = notNullFilter(rsCriteriosLeyesFederales.getString("LEY_INCISO"));
                    
                    if (!incisoLeyFed.isEmpty()) {
                    	fraccionLeyFed += ", " + incisoLeyFed;
                    }
                    
                    PdfPCell valFraccionLeyFed = new PdfPCell(new Phrase(fraccionLeyFed));
                    PdfPCell valVinculacionLeyFed = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosLeyesFederales.getString("LEY_VINCULACION"))));
                    
                    PdfPCell ligaArchivo = new PdfPCell(new Phrase("Sin adjunto", PARRAFO));
                    ligaArchivo.setHorizontalAlignment(Element.ALIGN_CENTER);
                    String id = rsCriteriosLeyesFederales.getString("ID_ARCHIVO_PROGRAMA");
                    if (null != id){
                        String filename = rsCriteriosLeyesFederales.getString("NOMBRE_ARCHIVO");
                        if (null != filename) {
                        	ligaArchivo = generarCeldaArchivo(filename, url, id.trim());
                        }
                	}
                    
                    // Primera fila: Titulos
                    tab3_1_1.addCell(titLeyFed);
                    tab3_1_1.addCell(titUltimaActualizacionLeyFed);
                    tab3_1_1.addCell(titArticuloLeyFed);
                    tab3_1_1.addCell(titFraccionIncisoLeyFed);
                    tab3_1_1.addCell(new PdfPCell(new Phrase("Adjunto", TITULO3)));
                    
                    
                    //Valores de la tabla de leyes federales
                    tab3_1_1.addCell(valNombreLeyFed);
                    tab3_1_1.addCell(valUltimaActLeyFed);
                    tab3_1_1.addCell(valArticuloLeyFed);
                    tab3_1_1.addCell(valFraccionLeyFed);
                    tab3_1_1.addCell(ligaArchivo);
                    
                    titVinculacionLeyFed.setColspan(5);
                    tab3_1_1.addCell(titVinculacionLeyFed);

                    valVinculacionLeyFed.setColspan(5);
                    valVinculacionLeyFed.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    tab3_1_1.addCell(valVinculacionLeyFed);

                }
            }
            
            seccion3_1_1.add(tab3_1_1);
            seccion3_1_1.add(new Paragraph("\n"));

            //====================================== REPORTE 3.1.2. LEYES ESTATALES =============================================================
            
            Chunk sec3_1_2Title = new Chunk("Estatales.", TITULO2);
            Section seccion3_1_2 = seccion3_1.addSection(new Paragraph(sec3_1_2Title));
            sec3_1_2Title.setLocalDestination(seccion3_1_2.getTitle().getContent());
            seccion3_1_2.setIndentationLeft(10);
            seccion3_1_2.add(new Paragraph("\n"));
            
            //SE CREA LA ESTRUCTURA DE LA TABLA DEL PDF CON LOS ENCABEZADOS.
            
            PdfPTable tab3_1_2 = new PdfPTable(5);
            tab3_1_2.setWidthPercentage((float) 100);
            float[] tblLeyEstColumnWidths = new float[] {30f, 20f, 15f, 15f, 20f};
            tab3_1_2.setWidths(tblLeyEstColumnWidths);            
            
            PdfPCell titLeyEst = new PdfPCell(new Phrase("Ley", TITULO3));
            PdfPCell titUltimaActualizacionLeyEst = new PdfPCell(new Phrase("Última Actualización", TITULO3));
            PdfPCell titArticuloLeyEst = new PdfPCell(new Phrase("Artículo", TITULO3));
            PdfPCell titFraccionIncisoLeyEst = new PdfPCell(new Phrase("Fracción e Inciso", TITULO3));
            PdfPCell titVinculacionLeyEst = new PdfPCell(new Phrase("Vinculación", TITULO3));
            
            String sql3_1_2 = "SELECT LE.*, AP.FILENAME || '.' || AP.EXTENSION AS NOMBRE_ARCHIVO"
        		+ " FROM ("
        		+ "SELECT E.LEY_FE_ID, E.LEY_FE_DESCRIP, E.FECHA_ULTIMA_ACTUALIZACION AS FECHA_ULTIMA_ACT, E.LEY_FE_ARTICULO AS LEY_ARTICULO"
            	+ ", E.LEY_FE_FRACCION AS LEY_FRACCION, E.LEY_FE_INCISO AS LEY_INCISO, E.LEY_FE_VINCULACION AS LEY_VINCULACION, E.ID_ARCHIVO_PROGRAMA"
        		+ " FROM LEY_FED_EST_PROYECTO E"
            	+ " WHERE E.FOLIO_PROYECTO = " + folio + " AND E.SERIAL_PROYECTO = " + serial + " AND E.LEY_FE_BANDERA = 'E'"
            	+ " ) LE"
            	+ " LEFT JOIN ARCHIVOS_PROYECTO AP ON LE.ID_ARCHIVO_PROGRAMA = AP.SEQ_ID"
            	+ " ORDER BY LE.LEY_FE_ID";
				

          //SE EJECUTA EL QUERY PARA CONSULTAR LOS CRITERIOS DE LA LEY ESTATAL PARA MOSTRAR SUS DATOS EN EL REPORTE.
            ResultSet rsCriteriosLeyesEstatales = st2.executeQuery(sql3_1_2);
            //SI EXISTEN ESTATALES PARA EL PROYECTO.
            if (rsCriteriosLeyesEstatales != null) {
                while (rsCriteriosLeyesEstatales.next()) {
                    PdfPCell valNombreLeyEst = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosLeyesEstatales.getString("LEY_FE_DESCRIP"))));
                    PdfPCell valUltimaActLeyEst = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosLeyesEstatales.getString("FECHA_ULTIMA_ACT"))));
                    PdfPCell valArticuloLeyEst = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosLeyesEstatales.getString("LEY_ARTICULO"))));
                    PdfPCell valVinculacionLeyEst = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosLeyesEstatales.getString("LEY_VINCULACION"))));
                    
                    String fraccionLeyEst = notNullFilter(rsCriteriosLeyesEstatales.getString("LEY_FRACCION"));
                    String incisoLeyEst = notNullFilter(rsCriteriosLeyesEstatales.getString("LEY_INCISO"));
                    
                    if (!incisoLeyEst.isEmpty()) {
                    	fraccionLeyEst +=  ", " + incisoLeyEst;
                    }
                    
                    PdfPCell valFraccionLeyEst = new PdfPCell(new Phrase(fraccionLeyEst));
                    
                    PdfPCell ligaArchivo = new PdfPCell(new Phrase("Sin adjunto", PARRAFO));
                    ligaArchivo.setHorizontalAlignment(Element.ALIGN_CENTER);
                    String id = rsCriteriosLeyesEstatales.getString("ID_ARCHIVO_PROGRAMA");
                    if (null != id){
                        String filename = rsCriteriosLeyesEstatales.getString("NOMBRE_ARCHIVO");
                        if (null != filename) {
                        	ligaArchivo = generarCeldaArchivo(filename, url, id.trim());
                        }
                	}
                    
                    // Primera fila: Titulos
                    tab3_1_2.addCell(titLeyEst);
                    tab3_1_2.addCell(titUltimaActualizacionLeyEst);
                    tab3_1_2.addCell(titArticuloLeyEst);
                    tab3_1_2.addCell(titFraccionIncisoLeyEst);
                    tab3_1_2.addCell(new PdfPCell(new Phrase("Adjunto", TITULO3)));
                    
                    //Valores de la tabla de leyes Estatales
                    tab3_1_2.addCell(valNombreLeyEst);
                    tab3_1_2.addCell(valUltimaActLeyEst);
                    tab3_1_2.addCell(valArticuloLeyEst);
                    tab3_1_2.addCell(valFraccionLeyEst);
                    tab3_1_2.addCell(ligaArchivo);

                    titVinculacionLeyEst.setColspan(5);
                    tab3_1_2.addCell(titVinculacionLeyEst);

                    valVinculacionLeyEst.setColspan(5);
                    valVinculacionLeyEst.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    tab3_1_2.addCell(valVinculacionLeyEst);
                }
            }

            seccion3_1_2.add(tab3_1_2);
            seccion3_1_2.add(new Paragraph("\n"));
            System.out.println("\nDespués de tabla: Leyes estatales.\n");

           
            
            //====================================== REPORTE 3.2. REGLAMENTOS =================================================================================         
            
            //PERMITE MOSTRAR EL TITULO DE LA SECCION DEL REPORTE DE REGLAMENTOS
            Chunk sec3_2Title = new Chunk("Reglamentos.", TITULO2);
            //SE ASIGNA EL TITULO COMO UNA SECCION DEL REPORTE
            Section seccion3_2 = chapter3.addSection(new Paragraph(sec3_2Title));
            sec3_2Title.setLocalDestination(seccion3_2.getTitle().getContent());
            seccion3_2.setIndentationLeft(10);
            //SE AGREGAN SALTOS DE LINEA PARA SEPARAR EL REPORTE
            seccion3_2.add(new Paragraph("\n"));

            
            //SE CREA LA ESTRUCTURA DE LA TABLA DEL PDF CON LOS ENCABEZADOS.
            
            PdfPTable tblReglamentos = new PdfPTable(4);
            tblReglamentos.setWidthPercentage((float) 100);
            float[] tblReglamentosColumnWidths = new float[] {30f, 20f, 25f, 25f};
            tblReglamentos.setWidths(tblReglamentosColumnWidths);
            
            
            PdfPCell titLey = new PdfPCell(new Phrase("Reglamento", TITULO3));
            PdfPCell titUltimaActualizacion = new PdfPCell(new Phrase("Última Actualización", TITULO3));
            PdfPCell titArticulo = new PdfPCell(new Phrase("Artículo, Fracción e Inciso", TITULO3));
            PdfPCell titVinculacion = new PdfPCell(new Phrase("Vinculación", TITULO3));
            PdfPCell titDescripcion = new PdfPCell(new Phrase("Descripción", TITULO3));
            
            
            String sql32 = "SELECT R.*, AP.FILENAME || '.' || AP.EXTENSION AS NOMBRE_ARCHIVO"
	    		+ " FROM ("
        		+ "SELECT A.REGLAMENTO_PROY_ID"
	            + ", CASE A.REGLAMENTO_ID"
	            + " WHEN 9999 THEN A.REGLAMENTO_DESCRIPCION"
	            + " ELSE B.REGLAMENTO_DESCRIPCION END REGLAMENTO_DESCRIPCION"
	            + ", REGLAMENTO_FECHA_PUBLICACION, REGLAMENTO_ARTICULO, REGLAMENTO_VICULACION, A.ID_ARCHIVO_PROYECTO"
	            + " FROM REGLAMENTO_PROYECTO A, CAT_REGLAMENTO B"
	            + " WHERE A.REGLAMENTO_ID = B.REGLAMENTO_ID"
	            + " AND A.FOLIO_PROYECTO = '" + folio + "' AND A.SERIAL_PROYECTO = " + serial 
	            + ") R"
	            + " LEFT JOIN ARCHIVOS_PROYECTO AP ON R.ID_ARCHIVO_PROYECTO = AP.SEQ_ID"
	            + " ORDER BY R.REGLAMENTO_PROY_ID";
            
            //SE EJECUTA EL QUERY PARA CONSULTAR LOS CRITERIOS DEL REGLAMENTO PARA MOSTRAR SUS DATOS EN EL REPORTE.
            ResultSet rsCriteriosReglamentos = st2.executeQuery(sql32);
            //SI EXISTEN REGLAMENTOS PARA EL PROYECTO.
            if (rsCriteriosReglamentos != null) {            	
            	//MIENTRAS EXISTAN REGISTROS.
                while (rsCriteriosReglamentos.next()) {
                    PdfPCell valLey = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosReglamentos.getString("REGLAMENTO_DESCRIPCION"))));
                    PdfPCell valUltimaAct = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosReglamentos.getString("REGLAMENTO_FECHA_PUBLICACION"))));
                    PdfPCell valArticulo = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosReglamentos.getString("REGLAMENTO_ARTICULO"))));
                    PdfPCell valVinculacion = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosReglamentos.getString("REGLAMENTO_VICULACION"))));
                    PdfPCell valDescripcion = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosReglamentos.getString("REGLAMENTO_DESCRIPCION"))));
                    
                    PdfPCell ligaArchivo = new PdfPCell(new Phrase("Sin adjunto", PARRAFO));
                    ligaArchivo.setHorizontalAlignment(Element.ALIGN_CENTER);
                    String id = rsCriteriosReglamentos.getString("ID_ARCHIVO_PROYECTO");
                    if (null != id){
                        String filename = rsCriteriosReglamentos.getString("NOMBRE_ARCHIVO");
                        if (null != filename) {
                        	ligaArchivo = generarCeldaArchivo(filename, url, id.trim());
                        }
                	}
                    
                    // Primera fila: Titulos
                    tblReglamentos.addCell(titLey);
                    tblReglamentos.addCell(titUltimaActualizacion);
                    tblReglamentos.addCell(titArticulo);
                    tblReglamentos.addCell(new PdfPCell(new Phrase("Adjunto", TITULO3)));
                    
                    // Segunda fila: Valores
                    tblReglamentos.addCell(valLey);
                    tblReglamentos.addCell(valUltimaAct);
                    tblReglamentos.addCell(valArticulo);
                    tblReglamentos.addCell(ligaArchivo);
                    
                    // Tercera fila: "VINCULACION"
                    titVinculacion.setColspan(4);
                    tblReglamentos.addCell(titVinculacion);
                    
                    // Cuarta Fila: Valor de VINCULACION
                    valVinculacion.setColspan(4);
                    valVinculacion.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    tblReglamentos.addCell(valVinculacion);
                    
                    // Quinta Fila: "DESCRIPCION"
                    titDescripcion.setColspan(4);
                    tblReglamentos.addCell(titDescripcion);

                    // Quinta Fila: Valor de DESCRIPCION
                    valDescripcion.setColspan(4);
                    valDescripcion.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    tblReglamentos.addCell(valDescripcion);
                }
                
                rsCriteriosReglamentos.close();
                
                //SE AGREGA LA TABLA CON SU INFORMACION DE LOS REGLAMENTOS EN LA SECCION CORRESPONDIENTE
                seccion3_2.add(new Paragraph("\n"));               
                seccion3_2.add(tblReglamentos);
                seccion3_2.add(new Paragraph("\n"));
                seccion3_2.add(new Paragraph("\n"));
            }
            // Cierre de TABLA: 3.2 "REGLAMENTOS"
            System.out.println("\nDespués de tabla: Reglamentos.\n");
            
            
            
            //====================================== REPORTE 3.3. NORMAS =============================================================        
            

            Chunk sec3_3Title = new Chunk("Normas.", TITULO2);
            Section seccion3_3 = chapter3.addSection(new Paragraph(sec3_3Title));
            sec3_3Title.setLocalDestination(seccion3_3.getTitle().getContent());
            seccion3_3.setIndentationLeft(10);
            seccion3_3.add(new Paragraph("\n"));
            
            
            //SE CREA LA ESTRUCTURA DE LA TABLA DEL PDF CON LOS ENCABEZADOS.
            String normaNombre = "Sin Nombre";
            String claveNorma = "";
            String fechaNorma = "";
            String fechaNormap = "";
            
            Statement stSNorma = con.createStatement();
            
            String sql3_3 = "SELECT FL.*, AP.FILENAME || '.' || AP.EXTENSION AS NOMBRE_ARCHIVO"
      		+ " FROM (SELECT"
            + " CASE A.NORMA_ID"
            + " WHEN 9999 THEN A.NORMA_DESCRIPCION"
            + " ELSE B.NORMA_NOMBRE || ' - ' || B.NORMA_NOMBRECORTO END AS NORMA_NOMBRE"
            + ", A.NORMA_PROY_ID, A.NORMA_FECH_PUBLIC, A.ID_ARCHIVO_PROGRAMA AS ID_ARCHIVO_PROGRAMA "
            + ", TO_CHAR(B.NORMA_FECHAPUBLICACION, 'mm/dd/yyyy') AS NORMA_FECHAPUBLICACION "
            + " FROM NORMA_PROYECTO A, CAT_NORMA B WHERE A.NORMA_ID = B.NORMA_ID AND A.FOLIO_PROYECTO = '" + folio + "' AND A.SERIAL_PROYECTO = " + serial
            + " ORDER BY NORMA_PROY_ID) FL"
      		+ " LEFT JOIN ARCHIVOS_PROYECTO AP ON FL.ID_ARCHIVO_PROGRAMA = AP.SEQ_ID"
      		+ " ORDER BY FL.NORMA_PROY_ID"; 
            
            ResultSet rsSNorma = stSNorma.executeQuery(sql3_3);
            
            while (rsSNorma.next()) {
            	normaNombre = notNullFilter(rsSNorma.getString("NORMA_NOMBRE"));
            	claveNorma = notNullFilter(rsSNorma.getString("NORMA_PROY_ID"));
                seccion3_3.add(new Paragraph(new Chunk(normaNombre, TITULO3)));
                seccion3_3.add(new Chunk("Fecha publicación: ", TITULO4));
                fechaNormap = notNullFilter(rsSNorma.getString("NORMA_FECHAPUBLICACION"));
                seccion3_3.add(new Chunk(fechaNormap));
                seccion3_3.add(new Chunk("\n"));
                seccion3_3.add(new Chunk("Fecha última actualización: ", TITULO4));
                fechaNorma = notNullFilter(rsSNorma.getString("NORMA_FECH_PUBLIC"));
                seccion3_3.add(new Chunk(fechaNorma));
            
                String idArchivoProyecto = rsSNorma.getString("ID_ARCHIVO_PROGRAMA");
                
                if (idArchivoProyecto != null) {
                	seccion3_3.add(new Paragraph(new Chunk("ADJUNTO: SI" )));					
	
                    String filename = rsSNorma.getString("NOMBRE_ARCHIVO");
    
                    if (null != filename) {
                  
                        String urlCompleta = url.concat("faces/modarchivos/visor.xhtml?idt=").concat(idArchivoProyecto).concat("&tipo=general");
                        
                        if (null != filename) {
                        	Chunk chk = new Chunk(filename, PARRAFO_SUB);
                            chk.setAnchor(urlCompleta);
                            System.out.println("URL de Adjunto por Registro: " + urlCompleta);
                            Paragraph p = new Paragraph();
                            p.add(chk);
                        	seccion3_3.add(new Paragraph(p));					

                        }                    
                    
                    }
                	  
                } else {
					seccion3_3.add(new Paragraph(new Chunk("ADJUNTO: NO" )));
					seccion3_3.add(new Paragraph(new Chunk("Sin adjunto" )));
				}
                
                seccion3_3.add(new Paragraph("\n"));
                
                PdfPTable tblNormas = null;
//                for (String claveNormaObtenida : clavesNormaObtenidos) {
                	
                    String sql33 = "SELECT C.NORMA_NUMERAL AS NORMA_NUMERAL, C.NORMA_REQUISITO AS NORMA_REQUISITO, C.NORMA_VINCULACION AS NORMA_VINCULACION "
                            + "FROM NUMERAL C "
                            + "WHERE  C.NORMA_PROY_ID = " + claveNorma + " AND C.FOLIO_PROYECTO = '" + folio + "' AND C.SERIAL_PROYECTO = " + serial + " ";				
    			
                    //SE EJECUTA EL QUERY PARA CONSULTAR LOS CRITERIOS DE LAS NORMAS PARA MOSTRAR SUS DATOS EN EL REPORTE.
                    ResultSet rsCriteriosNormas = st2.executeQuery(sql33);
                    
                        	//MIENTRAS EXISTAN REGISTROS.
                            while (rsCriteriosNormas.next()) {
                            	
                            	tblNormas = new PdfPTable(2);
                                tblNormas.setWidthPercentage((float) 100);
                                float[] tblNormasColumnWidths = new float[] {30f, 70f};
                                tblNormas.setWidths(tblNormasColumnWidths);           
                                
                                PdfPCell titNumeralNormas = new PdfPCell(new Phrase("Numeral", TITULO3));
                                PdfPCell titRequisitoNormas = new PdfPCell(new Phrase("Requisito", TITULO3));
                                PdfPCell titVinculacionNormas = new PdfPCell(new Phrase("Vinculación", TITULO3));
                            	
                            	String normaNumeral = rsCriteriosNormas.getString("NORMA_NUMERAL");
                            	String normaRequisito = rsCriteriosNormas.getString("NORMA_REQUISITO");
                            	String normaVinculacion = rsCriteriosNormas.getString("NORMA_VINCULACION");
                            	
                                PdfPCell valNormaNumeral = new PdfPCell(new Phrase("" + notNullFilter(normaNumeral)));
                                PdfPCell valNormaRequisito = new PdfPCell(new Phrase("" + notNullFilter(normaRequisito)));
                                PdfPCell valNormaVinculacion = new PdfPCell(new Phrase("" + notNullFilter(normaVinculacion)));
                                
                                // Primera fila: Titulo, valor
                                tblNormas.addCell(titNumeralNormas);
                                tblNormas.addCell(titRequisitoNormas);
                                
                                // Segunda fila: Titulo, valor
                                tblNormas.addCell(valNormaNumeral);
                                tblNormas.addCell(valNormaRequisito);                                 

                                // Tercera fila: Titulo, valor
                                tblNormas.addCell(titVinculacionNormas);
                                valNormaVinculacion.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                                tblNormas.addCell(valNormaVinculacion);

                                // SE AGREGA LA TABLA CON SU INFORMACION DE LAS NORMAS EN LA SECCION CORRESPONDIENTE
                                seccion3_3.add(tblNormas);                    
                                seccion3_3.add(Chunk.NEWLINE);

                         
                    }
                    
                    rsCriteriosNormas.close();

            }
                
//                
//                claveNorma = notNullFilter(rsSNorma.getString("NORMA_PROY_ID"));
//                clavesNormaObtenidos.add(claveNorma);
//            }
            
            rsSNorma.close();
            seccion3_3.add(new Paragraph("\n"));
            seccion3_3.add(new Paragraph("\n"));
            // Cierre de TABLA: 3.3 "NORMAS"
            System.out.println("\nDespués de tabla: Normas.\n");
            

            
            // 3.4 POET
            Chunk sec3_4Title = new Chunk("Programa de ordenamiento ecológico territorial.", TITULO2);
            Section seccion3_4 = chapter3.addSection(new Paragraph(sec3_4Title));
            sec3_4Title.setLocalDestination(seccion3_4.getTitle().getContent());
            seccion3_4.setIndentationLeft(10);
            seccion3_4.add(new Paragraph("\n"));

            //String sql34 = "SELECT POETM_TIPO, POETM_NOMBRE_INSTRUMENTO, POETM_NOMBRE_UGA, POETM_POLITICA_AMBIENTAL, POETM_USO_PREDOMINANTE, POETM_CRITERIO,"
            	//	+ " CASE POETM_COMPATIBLE WHEN 'S' THEN 'SI' ELSE 'NO' END AS POETM_COMPATIBLE, POETM_VINCULACION, ARCHIVOS_PROYECTO.FILENAME AS ARCHIVO"
            	//	+ " FROM POETM_PROYECTO LEFT JOIN ARCHIVOS_PROYECTO ON ARCHIVOS_PROYECTO.SEQ_ID = POETM_PROYECTO.ID_ARCHIVO_PROYECTO WHERE   POETM_PROYECTO.FOLIO_PROYECTO = '"+folio+"' AND POETM_PROYECTO.SERIAL_PROYECTO ="+serial+"";
          //  PdfPTable tabPOETm = ReportUtil.buildTableVinculacion(con, sql34, new String[]{"POETM_TIPO", "POETM_NOMBRE_UGA", "POETM_POLITICA_AMBIENTAL", "POETM_USO_PREDOMINANTE", "POETM_CRITERIO", "POETM_COMPATIBLE", "POETM_NOMBRE_INSTRUMENTO", "POETM_VINCULACION", "ARCHIVO"}, new String[]{"Tipo", "Número y/o nombre de la UAB/UGA", "Política ambiental", "Uso", "Criterios", "¿Es compatible con los usos?", "Nombre del instrumento", "Vinculación", "Archivo"});
         //   seccion3_4.add(tabPOETm);
           // seccion3_4.add(new Paragraph("\n"));
            
            ////=========================================================================================================================================
            PdfPTable tabPOETm = new PdfPTable(5);
            tabPOETm.setWidthPercentage((float) 100);
            float[] tabPOETmColumnWidths = new float[] {20f, 20f, 20f, 20f, 20f};
            tabPOETm.setWidths(tabPOETmColumnWidths);
            
            //  "Vinculación", "Archivo"
            PdfPCell titTipo = new PdfPCell(new Phrase("TIPO", TITULO3));
            PdfPCell titNumero = new PdfPCell(new Phrase("Número y/o nombre de la UAB/UGA", TITULO3));
            PdfPCell titPolitica = new PdfPCell(new Phrase("Política ambiental", TITULO3));
            PdfPCell titUso = new PdfPCell(new Phrase("Uso", TITULO3));
            PdfPCell titCrit = new PdfPCell(new Phrase("Criterios", TITULO3));
            PdfPCell titCompatible = new PdfPCell(new Phrase("¿Es compatible con los usos?", TITULO3));
            PdfPCell titinstrumento = new PdfPCell(new Phrase("Nombre del instrumento", TITULO3));
            PdfPCell titVincula = new PdfPCell(new Phrase("Vinculación", TITULO3));
            
            
            String sql34 = "SELECT POETM_TIPO, POETM_NOMBRE_INSTRUMENTO, POETM_NOMBRE_UGA, POETM_POLITICA_AMBIENTAL, POETM_USO_PREDOMINANTE, POETM_CRITERIO, "
            	       + "  CASE POETM_COMPATIBLE WHEN 'S' THEN 'SI' ELSE 'NO' END AS POETM_COMPATIBLE, POETM_VINCULACION, ARCHIVOS_PROYECTO.FILENAME ||'.'|| ARCHIVOS_PROYECTO.EXTENSION AS ARCHIVO, POETM_PROYECTO.ID_ARCHIVO_PROYECTO as ID"
            		   + "  FROM POETM_PROYECTO LEFT JOIN ARCHIVOS_PROYECTO ON ARCHIVOS_PROYECTO.SEQ_ID = POETM_PROYECTO.ID_ARCHIVO_PROYECTO WHERE   POETM_PROYECTO.FOLIO_PROYECTO = '"+folio+"' AND POETM_PROYECTO.SERIAL_PROYECTO ="+serial+"";
            
            
            ResultSet rsPOET = st2.executeQuery(sql34);
            
            if (rsPOET != null) {            	
            	//MIENTRAS EXISTAN REGISTROS.
                while (rsPOET.next()) {
                	 
                    PdfPCell valTipo = new PdfPCell(new Phrase("" + notNullFilter(rsPOET.getString("POETM_TIPO"))));
                    PdfPCell valNumero = new PdfPCell(new Phrase("" + notNullFilter(rsPOET.getString("POETM_NOMBRE_UGA"))));
                    PdfPCell valPolitica = new PdfPCell(new Phrase("" + notNullFilter(rsPOET.getString("POETM_POLITICA_AMBIENTAL"))));
                    PdfPCell valUso = new PdfPCell(new Phrase("" + notNullFilter(rsPOET.getString("POETM_USO_PREDOMINANTE"))));
                    PdfPCell valCrit = new PdfPCell(new Phrase("" + notNullFilter(rsPOET.getString("POETM_CRITERIO"))));
                    PdfPCell valCompatible = new PdfPCell(new Phrase("" + notNullFilter(rsPOET.getString("POETM_COMPATIBLE"))));
                    PdfPCell valInstrumento = new PdfPCell(new Phrase("" + notNullFilter(rsPOET.getString("POETM_NOMBRE_INSTRUMENTO"))));
                    PdfPCell valVincula = new PdfPCell(new Phrase("" + notNullFilter(rsPOET.getString("POETM_VINCULACION"))));
                    
                    PdfPCell ligaArchivo = new PdfPCell(new Phrase("Sin adjunto", PARRAFO));
                    ligaArchivo.setHorizontalAlignment(Element.ALIGN_CENTER);
                    String id = rsPOET.getString("ID");
                    if (null != id){
                        String filename = rsPOET.getString("ARCHIVO");
                        if (null != filename) {
                        	ligaArchivo = generarCeldaArchivo(filename, url, id.trim());
                        }
                	}
                    
                    // Primera fila: Titulos
                    tabPOETm.addCell(titTipo);
                    tabPOETm.addCell(titNumero);
                    tabPOETm.addCell(titUso);
                    tabPOETm.addCell(titCrit);
                    tabPOETm.addCell(new PdfPCell(new Phrase("Adjunto", TITULO3)));
                    
                    // Segunda fila: Valores
                    tabPOETm.addCell(valTipo);
                    tabPOETm.addCell(valNumero);
                    tabPOETm.addCell(valUso);
                    tabPOETm.addCell(valCrit);
                    tabPOETm.addCell(ligaArchivo);
                    
                    // Tercera fila: Vinculacion
                    titPolitica.setColspan(5);
                    tabPOETm.addCell(titPolitica);
                    
                    // Cuarta Fila
                    valPolitica.setColspan(5);
                    tabPOETm.addCell(valPolitica);
                    
                     //Quinta Fila
                    titCompatible.setColspan(5);
                    tabPOETm.addCell(titCompatible);

                    // Sexta Fila
                    valCompatible.setColspan(5);
                    tabPOETm.addCell(valCompatible);
                    //----------
                    //Septima Fila
                    titinstrumento.setColspan(5);
                    tabPOETm.addCell(titinstrumento);

                    // Octava Fila
                    valInstrumento.setColspan(5);
                    tabPOETm.addCell(valInstrumento);
                    
                    //Quinta Fila
                    titVincula.setColspan(5);
                    tabPOETm.addCell(titVincula);

                    // Quinta Fila
                    valVincula.setColspan(5);
                    valVincula.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    tabPOETm.addCell(valVincula);
                    
                }
                
                rsPOET.close();
                
                
                seccion3_4.add(new Paragraph("\n"));               
                seccion3_4.add(tabPOETm);
                seccion3_4.add(new Paragraph("\n"));	
            }
            // Cierre de TABLA: 3.4 "POET"
            System.out.println("\nDespués de tabla: POET.\n");
            ////==========================================================================================================================================

            
            
            // 3.5 ANP
            Chunk sec3_5Title = new Chunk("Área natural protegida.", TITULO2);
            Section seccion3_5 = chapter3.addSection(new Paragraph(sec3_5Title));
            sec3_5Title.setLocalDestination(seccion3_5.getTitle().getContent());
            seccion3_5.setIndentationLeft(10);
            seccion3_5.add(new Paragraph("\n"));

            String idAnp = "";  
            String tipoAnp = "";
            String nombreAnp = "";
            String categManejoAnp = "";          
            String fechaDecreto = "";
            String fechaProgManejo = "";
            String idDecreto;
            String idPrograma;

            Statement stSAnp = con.createStatement();
            String qryAnp = "SELECT ANP_ID, ANP_TIPO, ANP_NOMBRE, ANP_CATEGORIA_MANEJO"
        		+ ", TO_CHAR(ANP_ULTIMO_DECRETO, 'DD-MON-YYYY') ANP_ULTIMO_DECRETO, TO_CHAR(ANP_FECHA_MANEJO, 'DD-MON-YYYY') ANP_FECHA_MANEJO"
        		+ ", ID_ARCHIVO_DECRETO AS DECRETO_ID, ID_ARCHIVO_PROGRAMA AS PROGRAMA_ID"
        		+ " FROM ANP_PROYECTO"
        		+ " WHERE FOLIO_PROYECTO = '" + folio + "' AND SERIAL_PROYECTO = " + serial
        		+ " ORDER BY ANP_ID";
            ResultSet rsSAnp = stSAnp.executeQuery(qryAnp);
            //ResultSet rsSAnp = stSAnp.executeQuery("SELECT ANP_TIPO, ANP_NOMBRE, ANP_CATEGORIA_MANEJO, TO_CHAR(ANP_ULTIMO_DECRETO, 'DD-MON-YYYY') as fecha1, TO_CHAR(ANP_FECHA_MANEJO, 'DD-MON-YYYY') as fecha2, ANP_ID, (SELECT A.FILENAME ||'.'|| A.EXTENSION FROM ARCHIVOS_PROYECTO A, ANP_PROYECTO B WHERE A.SEQ_ID = B.ID_ARCHIVO_DECRETO AND B.FOLIO_PROYECTO='"+folio+"') AS DECRETO , (SELECT A.FILENAME ||'.'|| A.EXTENSION FROM ARCHIVOS_PROYECTO A, ANP_PROYECTO B WHERE A.SEQ_ID = B.ID_ARCHIVO_PROGRAMA AND B.FOLIO_PROYECTO='"+folio+"') AS PROGRAMA, ID_ARCHIVO_DECRETO AS DECRETO_ID, ID_ARCHIVO_PROGRAMA AS PROGRAMA_ID FROM ANP_PROYECTO WHERE FOLIO_PROYECTO='"+folio+"' AND SERIAL_PROYECTO="+serial+"");
            
            while (rsSAnp.next()) {
            	idAnp = notNullFilter(rsSAnp.getString("ANP_ID"));
                tipoAnp = notNullFilter(rsSAnp.getString("ANP_TIPO"));
                nombreAnp = notNullFilter(rsSAnp.getString("ANP_NOMBRE"));
                categManejoAnp = notNullFilter(rsSAnp.getString("ANP_CATEGORIA_MANEJO"));
               
                fechaDecreto = notNullFilter(rsSAnp.getString("ANP_ULTIMO_DECRETO"));
                fechaProgManejo = notNullFilter(rsSAnp.getString("ANP_FECHA_MANEJO"));
                
                idDecreto = notNullFilter(rsSAnp.getString("DECRETO_ID"));
                idPrograma = notNullFilter(rsSAnp.getString("PROGRAMA_ID"));
                
                Paragraph pANP = new Paragraph();
                pANP.add(new Chunk("Tipo: ", TITULO4));
                pANP.add(new Chunk(tipoAnp));
                pANP.add(Chunk.NEWLINE);
                pANP.add(new Chunk("ANP: ", TITULO4));
                pANP.add(new Chunk(nombreAnp));
                pANP.add(Chunk.NEWLINE);
                pANP.add(new Chunk("Categoría de Manejo: ", TITULO4));
                pANP.add(new Chunk(categManejoAnp));
                pANP.add(Chunk.NEWLINE);                
                pANP.add(new Chunk("Fecha decreto: ", TITULO4));
                pANP.add(new Chunk(fechaDecreto));
                pANP.add(Chunk.NEWLINE);
                pANP.add(new Chunk("Documento decreto: ", TITULO4));
                
                if (!idDecreto.isEmpty()) {
                	Statement stArchivoDecreto = con.createStatement();
                	String qryArchivoDecreto = "SELECT FILENAME || '.' || EXTENSION AS NOMBRE_ARCHIVO FROM ARCHIVOS_PROYECTO WHERE SEQ_ID = " + idDecreto;
                	ResultSet rsArchivoDecreto = stArchivoDecreto.executeQuery(qryArchivoDecreto);
                	
                	String filename = "";
                	if (rsArchivoDecreto.next()) {
                		filename = notNullFilter(rsArchivoDecreto.getString("NOMBRE_ARCHIVO"));
                	}
                	rsArchivoDecreto.close();
                	stArchivoDecreto.close();
                	
                    if (null != filename && !filename.isEmpty()) {
                        String urlCompleta = url.concat("faces/modarchivos/visor.xhtml?idt=").concat(idDecreto).concat("&tipo=general");                        
                        if (null != filename) {
                        	Chunk chk = new Chunk(filename, PARRAFO_SUB);
                            chk.setAnchor(urlCompleta);
                            System.out.println("URL de Adjunto por Registro: " + urlCompleta);
                            pANP.add(chk);					
                        }                                        
                    }
                	  
                } 
                else 
                { 
                	pANP.add(new Chunk("Sin adjunto" ));
			    }
                
                pANP.add(Chunk.NEWLINE);
                pANP.add(new Chunk("Fecha manejo: ", TITULO4));
                pANP.add(new Chunk(fechaProgManejo));
                pANP.add(Chunk.NEWLINE);
                pANP.add(new Chunk("Documento manejo: ", TITULO4));
                
                if (!idPrograma.isEmpty()) {
                	Statement stArchivoProgManejo = con.createStatement();
                	String qryArchivoProgManejo = "SELECT FILENAME || '.' || EXTENSION AS NOMBRE_ARCHIVO FROM ARCHIVOS_PROYECTO WHERE SEQ_ID = " + idPrograma;
                	ResultSet rsArchivoProgManejo = stArchivoProgManejo.executeQuery(qryArchivoProgManejo);
                     
                    String filename = "";
                    if (rsArchivoProgManejo.next()) {
                    	filename = notNullFilter(rsArchivoProgManejo.getString("NOMBRE_ARCHIVO"));
                    }
                    rsArchivoProgManejo.close();
                    stArchivoProgManejo.close();
                    
                    if (null != filename && !filename.isEmpty()) {                  
                        String urlCompleta = url.concat("faces/modarchivos/visor.xhtml?idt=").concat(idPrograma).concat("&tipo=general");                        
                        if (null != filename) {
                        	Chunk chk = new Chunk(filename, PARRAFO_SUB);
                            chk.setAnchor(urlCompleta);
                            System.out.println("URL de Adjunto por Registro: " + urlCompleta);
                            pANP.add(chk);					
                        }                                        
                    }
                	  
                } 
                else 
                { 
                	pANP.add(new Chunk("Sin adjunto" ));
			    }
                seccion3_5.add(pANP);
                seccion3_5.add(new Paragraph("\n"));
                
                
                //Tabla uno
                seccion3_5.add(new Paragraph(new Chunk("Vinculación con el Decreto ", TITULO3)));
                
                PdfPTable tab1 = new PdfPTable(3);
                tab1.setWidthPercentage((float) 100);
                float[] tab1ColumnWidths = new float[] {5f, 25f, 70f};
                tab1.setWidths(tab1ColumnWidths);
                // seccion3_5.add(new Paragraph("\n"));
                
                // Primera fila: Titulos
                tab1.addCell("No.");
                tab1.addCell("Artículo");
                tab1.addCell("Vinculación");
                
                String sql351 = "WITH REGS_SECUENCIALES AS ("
                		+ "SELECT ANP_ART_NOM_REGLA, ANP_ART_NOM_VINCULACION"
                        + " FROM ARTICULO_REGLA_ANP C"
                        + " WHERE C.ANP_ID = " + idAnp + " AND C.FOLIO_PROYECTO = '" + folio + "' AND C.SERIAL_PROYECTO = " + serial + " AND C.ANP_ARTICULO_REGLA = 'A'"
                        + " ORDER BY ANP_ART_REG_ID"
                        + ")"
                        + " SELECT ROWNUM AS SECUENCIAL, rs.* FROM REGS_SECUENCIALES rs";                
                
                ResultSet rsTab1 = st2.executeQuery(sql351);
                
                while (rsTab1.next())
                {
                	PdfPCell valNo = new PdfPCell(new Phrase("" + notNullFilter(rsTab1.getString("SECUENCIAL"))));
                    PdfPCell valArticulo = new PdfPCell(new Phrase("" + notNullFilter(rsTab1.getString("ANP_ART_NOM_REGLA"))));
                    PdfPCell valVinculacion = new PdfPCell(new Phrase("" + notNullFilter(rsTab1.getString("ANP_ART_NOM_VINCULACION"))));
                
                    // Segunda fila: Valores
                    tab1.addCell(valNo);
                    tab1.addCell(valArticulo);
                    tab1.addCell(valVinculacion);        
                }
                rsTab1.close();
            
                seccion3_5.add(new Paragraph("\n"));
                seccion3_5.add(tab1);
                seccion3_5.add(new Paragraph("\n"));
                
                
                //Tabla dos
                seccion3_5.add(new Paragraph(new Chunk("Vinculación con el Programa de Manejo", TITULO3)));
                
                PdfPTable tab2 = new PdfPTable(3);
                tab2.setWidthPercentage((float) 100);
                float[] tab2ColumnWidths = new float[] {5f, 25f, 70f};
                tab2.setWidths(tab2ColumnWidths);
                
                // Primera fila: Titulos
                tab2.addCell("No.");
                tab2.addCell("Artículo");
                tab2.addCell("Vinculación");
                            
                String sql352 = "WITH REGS_SECUENCIALES AS ("
                		+ "SELECT ANP_ART_REG_ID, ANP_ID, ANP_ART_NOM_REGLA, ANP_ART_NOM_VINCULACION"
                        + " FROM ARTICULO_REGLA_ANP C"
                        + " WHERE  C.ANP_ID = " + idAnp + " AND C.FOLIO_PROYECTO = '" + folio + "' AND C.SERIAL_PROYECTO = " + serial + " AND C.ANP_ARTICULO_REGLA = 'R'"
                        + " ORDER BY ANP_ART_REG_ID"
                        + ")"
                        + " SELECT ROWNUM AS SECUENCIAL, rs.* FROM REGS_SECUENCIALES rs";
                
                ResultSet rsTab2 = st2.executeQuery(sql352);
                
                while (rsTab2.next()) 
                {
                	PdfPCell valNo1 = new PdfPCell(new Phrase("" + notNullFilter(rsTab2.getString("SECUENCIAL"))));
                    PdfPCell valArticulo1 = new PdfPCell(new Phrase("" + notNullFilter(rsTab2.getString("ANP_ART_NOM_REGLA"))));
                    PdfPCell valVinculacion1 = new PdfPCell(new Phrase("" + notNullFilter(rsTab2.getString("ANP_ART_NOM_VINCULACION"))));
                	
                    // Segunda fila: Valores
                    tab2.addCell(valNo1);
                    tab2.addCell(valArticulo1);
                    tab2.addCell(valVinculacion1);                        
                }
                rsTab2.close();
            
                seccion3_5.add(new Paragraph("\n"));
                seccion3_5.add(tab2);
                seccion3_5.add(new Paragraph("\n"));                            
            }
            seccion3_5.add(new Paragraph("\n"));
            stSAnp.close();
            rsSAnp.close();
            // Cierre de TABLA: 3.5 "Área natural protegida"
            System.out.println("\nDespués de tabla: Área natural protegida.\n");

            
            //====================================== REPORTE 3.6. PDU =================================================================================
            

            Chunk sec3_6Title = new Chunk("Planes y/o programas de desarrollo urbano.", TITULO2);
            Section seccion3_6 = chapter3.addSection(new Paragraph(sec3_6Title));
            sec3_6Title.setLocalDestination(seccion3_6.getTitle().getContent());
            seccion3_6.setIndentationLeft(10);
            seccion3_6.add(new Paragraph("\n")); 

            //SE CREA LA ESTRUCTURA DE LA TABLA DEL PDF CON LOS ENCABEZADOS.
            
            PdfPTable tabPDU = new PdfPTable(1);
            tabPDU.setWidthPercentage((float) 100);
            //float[] tblPDUColumnWidths = new float[] {5f, 95f};
            //tabPDU.setWidths(tblPDUColumnWidths);
            
            /*PdfPCell titComponentePDU = new PdfPCell(new Phrase("Componente", TITULO3));
            PdfPCell titNombrePlanPDU = new PdfPCell(new Phrase("Nombre del Plan o Programa", TITULO3));
            PdfPCell titFechaPubliPDU = new PdfPCell(new Phrase("Fecha de Publicación", TITULO3));
            PdfPCell titUsosPDU = new PdfPCell(new Phrase("Usos", TITULO3));
            PdfPCell titVinculacionPDU = new PdfPCell(new Phrase("Vinculación", TITULO3));*/

            
            String sql36 = "SELECT ID_ARCHIVO_PROGRAMA, PDUM_ID, PDUM_COMP, PDUM_NOMBRE, PDUM_FECHA_PUBLICACION, PDUM_USOS"
        		+ ", TRIM(PDUM_VINCULACION) PDUM_VINCULACION, AP.FILENAME || '.' || AP.EXTENSION AS NOMBRE_ARCHIVO"
            	+ ", PDU.PDUM_CLAVE_USOS , PDU.PDUM_COS, PDU.PDUM_CUS  "
	    		+ " FROM PDUM_PROYECTO PDU"
        		+ " LEFT JOIN ARCHIVOS_PROYECTO AP ON PDU.ID_ARCHIVO_PROGRAMA = AP.SEQ_ID"
	    		+ " WHERE PDU.FOLIO_PROYECTO = '" + folio + "' AND PDU.SERIAL_PROYECTO = " + serial
	    		+ " ORDER BY PDU.PDUM_ID";

            //SE EJECUTA EL QUERY PARA CONSULTAR LOS CRITERIOS DEL PDU PARA MOSTRAR SUS DATOS EN EL REPORTE.
            ResultSet rsCriteriosPDU = st2.executeQuery(sql36);
            //SI EXISTEN PDU PARA EL PROYECTO.
            if (rsCriteriosPDU != null) {            	
            	//MIENTRAS EXISTAN REGISTROS.
                while (rsCriteriosPDU.next()) {
                    /*PdfPCell valIdPDU = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosPDU.getString("PDUM_ID"))));
                    PdfPCell valComponentePDU = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosPDU.getString("PDUM_COMP"))));
                    PdfPCell valNombrePDU = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosPDU.getString("PDUM_NOMBRE"))));
                    PdfPCell valFechaPubliPDU = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosPDU.getString("PDUM_FECHA_PUBLICACION"))));
                    PdfPCell valUsosPDU = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosPDU.getString("PDUM_USOS"))));
                    PdfPCell valVinculacionPDU = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosPDU.getString("PDUM_VINCULACION"))));
                    
                    PdfPCell ligaArchivo = new PdfPCell(new Phrase("Sin adjunto", PARRAFO));
                    String id = rsCriteriosPDU.getString("ID_ARCHIVO_PROGRAMA");
                    if (null != id){
                        String filename = rsCriteriosPDU.getString("NOMBRE_ARCHIVO");
                        if (null != filename) {
                        	ligaArchivo = generarCeldaArchivo(filename, url, id.trim());
                        }
                	}

                    // "Componente"
                    tabPDU.addCell(titComponentePDU);
                    
                    // Valor de "Componente"
                    tabPDU.addCell(valComponentePDU);
                    
                    // "Nombre del Plan o Programa"
                    tabPDU.addCell(titNombrePlanPDU);
                    
                    // Valor de "Nombre del Plan o Programa"
                    tabPDU.addCell(valNombrePDU);
                    
                    // "Fecha de Publicación"
                    tabPDU.addCell(titFechaPubliPDU);
                    
                    // Valor de "Fecha de Publicación"
                    tabPDU.addCell(valFechaPubliPDU);
                    
                    // "Usos"
                    tabPDU.addCell(titUsosPDU);
                    
                    // Valor de "Usos"
                    tabPDU.addCell(valUsosPDU);
                    
                    // Visualización del archivo adjunto
                    tabPDU.addCell(ligaArchivo);
                    
                    // "Vinculación"
                    tabPDU.addCell(titVinculacionPDU);
                    
                    // Valor de "Vinculación"
                    tabPDU.addCell(valVinculacionPDU);*/
Paragraph pPDU = new Paragraph();
                	
                	Chunk titComponentePDU = new Chunk("Componente: ", TITULO3);
                	Chunk titNombrePlanPDU = new Chunk("Nombre del Plan o Programa: ", TITULO3);
                	Chunk titFechaPubliPDU = new Chunk("Fecha de Publicación: ", TITULO3);
                	Chunk titClausosPDU = new Chunk("Clave de usos: ", TITULO3);
                	Chunk titUsosPDU = new Chunk("Usos: ", TITULO3);
                	Chunk titCOSPDU = new Chunk("Coeficiente de Ocupación del Suelo (COS): ", TITULO3);
                	Chunk titCUSPDU = new Chunk("Coeficiente de Utilización del Suelo (CUS): ", TITULO3);
                	Chunk titVinculacionPDU = new Chunk("Vinculación", TITULO3);
                	
                	Paragraph valComponentePDU = new Paragraph(notNullFilter(rsCriteriosPDU.getString("PDUM_COMP")));
                	Paragraph valNombrePlanPDU = new Paragraph(notNullFilter(rsCriteriosPDU.getString("PDUM_NOMBRE")));
                	Paragraph valFechaPubliPDU = new Paragraph(notNullFilter(rsCriteriosPDU.getString("PDUM_FECHA_PUBLICACION")));
                	Paragraph valUsosPDU = new Paragraph(notNullFilter(rsCriteriosPDU.getString("PDUM_USOS")));
                	Paragraph valClausosPDU = new Paragraph(notNullFilter(rsCriteriosPDU.getString("PDUM_CLAVE_USOS")));
                	Paragraph valCOSPDU = new Paragraph(notNullFilter(rsCriteriosPDU.getString("PDUM_COS")));
                	Paragraph valCUSPDU = new Paragraph(notNullFilter(rsCriteriosPDU.getString("PDUM_CUS")));
                	Paragraph valVinculacionPDU = new Paragraph(notNullFilter(rsCriteriosPDU.getString("PDUM_VINCULACION")));
                	
                	
                	pPDU.add(titComponentePDU);
                	pPDU.add(valComponentePDU);
                	
                	pPDU.add(titNombrePlanPDU);
                	pPDU.add(valNombrePlanPDU);

                	pPDU.add(titFechaPubliPDU);
                	pPDU.add(valFechaPubliPDU);

                	pPDU.add(titClausosPDU);
                	pPDU.add(valClausosPDU);

                	pPDU.add(titUsosPDU);
                	pPDU.add(valUsosPDU);
                	
                	pPDU.add(titCOSPDU);
                	valCOSPDU.setAlignment(Element.ALIGN_JUSTIFIED);
                	pPDU.add(valCOSPDU);
                	
                	pPDU.add(titCUSPDU);
                	valCUSPDU.setAlignment(Element.ALIGN_JUSTIFIED);
                	pPDU.add(valCUSPDU);
                	
                	pPDU.add(titVinculacionPDU);
                	valVinculacionPDU.setAlignment(Element.ALIGN_JUSTIFIED);
                	pPDU.add(valVinculacionPDU);                	
                	pPDU.add(Chunk.NEWLINE);
                	pPDU.add(Chunk.NEWLINE);
                	pPDU.add(Chunk.NEWLINE);
                	
                	// Se agrega párrafo a la sección
                	seccion3_6.add(pPDU);
                	
                }
            }
            
            //seccion3_6.add(tabPDU);
            seccion3_6.add(new Paragraph("\n"));
            // Cierre de TABLA: 3.6 "PDU"
            System.out.println("\nDespués de tabla: Planes y/o programas de desarrollo urbano (PDU).\n");

            
            //====================================== REPORTE 3.7. ACUERDOS =================================================================================

            Chunk sec3_7Title = new Chunk("Acuerdos, convenios y tratados internacionales en materia ambiental.", TITULO2);
            Section seccion3_7 = chapter3.addSection(new Paragraph(sec3_7Title));
            sec3_7Title.setLocalDestination(seccion3_7.getTitle().getContent());
            seccion3_7.setIndentationLeft(10);
            seccion3_7.add(new Paragraph("\n")); 

            //SE CREA LA ESTRUCTURA DE LA TABLA DEL PDF CON LOS ENCABEZADOS.
                        
            PdfPTable tabConvenios = new PdfPTable(2);
            tabConvenios.setWidthPercentage((float) 100);
            float[] tblAcuerdosColumnWidths = new float[] {80f, 40};
            tabConvenios.setWidths(tblAcuerdosColumnWidths);            
            
            PdfPCell titConvAcuerdo = new PdfPCell(new Phrase("Acuerdo, convenios, tratados o internacionales", TITULO3));
            PdfPCell titFechAcuerdo = new PdfPCell(new Phrase("Fecha de publicación", TITULO3));
            PdfPCell titVinculacionAcuerdo = new PdfPCell(new Phrase("Vinculación", TITULO3));

            String sql37 = "SELECT CASE A.CONVENIO_ID WHEN 9999 THEN A.CONVENIO_DESCRIPCION ELSE C.CONVENIO_DESCRIPCION END AS CONVENIO_DESCRIPCION, "
                    + "CONVENIO_VINCULACION, CONVENIO_FECHA_PUBLICACION "
                    + "FROM CONVENIO_PROY A, CAT_CONVENIOS C "
                    + "WHERE A.CONVENIO_ID = C.CONVENIO_ID AND A.FOLIO_PROYECTO = '" + folio + "' AND A.SERIAL_PROYECTO = " + serial + " ";

            
            //SE EJECUTA EL QUERY PARA CONSULTAR LOS CRITERIOS DEL PDU PARA MOSTRAR SUS DATOS EN EL REPORTE.
            ResultSet rsCriteriosAcuerdos = st2.executeQuery(sql37);
            //SI EXISTEN PDU PARA EL PROYECTO.
            if (rsCriteriosAcuerdos != null) {            	
            	//MIENTRAS EXISTAN REGISTROS.
                while (rsCriteriosAcuerdos.next()) {
                    PdfPCell valConvAcuerdo = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosAcuerdos.getString("CONVENIO_DESCRIPCION"))));
                    PdfPCell valfechaAcuerdo = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosAcuerdos.getString("CONVENIO_FECHA_PUBLICACION"))));
                    PdfPCell valVinculacionAcuerdo = new PdfPCell(new Phrase("" + notNullFilter(rsCriteriosAcuerdos.getString("CONVENIO_VINCULACION"))));
                    
                    // Primera fila: Titulos
                    tabConvenios.addCell(titConvAcuerdo);
                    tabConvenios.addCell(titFechAcuerdo);
                    
                    //Segunda fila: Valores
                    
                    tabConvenios.addCell(valConvAcuerdo);
                    tabConvenios.addCell(valfechaAcuerdo);
                    
                    titVinculacionAcuerdo.setColspan(2);
                    tabConvenios.addCell(titVinculacionAcuerdo);
                    
                    valVinculacionAcuerdo.setColspan(2);
                    tabConvenios.addCell(valVinculacionAcuerdo);
                    
                }
            }
            
            seccion3_7.add(tabConvenios);
            seccion3_7.add(new Paragraph("\n"));                     
            

            // 3.8 Otras Disposiciones
            String oDisposicionAplica = rsProy.getString("PROY_ODISPOSICION_APLICA");
            if (oDisposicionAplica != null && oDisposicionAplica.compareTo("S") == 0) {
                Chunk sec3_8Title = new Chunk("Otras disposiciones.", TITULO2);
                Section seccion3_8 = chapter3.addSection(new Paragraph(sec3_8Title));
                sec3_8Title.setLocalDestination(seccion3_8.getTitle().getContent());
                seccion3_8.setIndentationLeft(10);
                seccion3_8.add(new Paragraph("\n"));

                Statement stSOdisposicion = con.createStatement();

                ResultSet rsSODisposicion = stSOdisposicion.executeQuery("SELECT DISPOSICION_NOMBRE, DISPOSICION_VINCULACION"
                        + " FROM DISPOSICION_PROYECTO A "
                        + " WHERE A.FOLIO_PROYECTO = '" + folio + "' AND A.SERIAL_PROYECTO = " + serial + " ");
                
                while (rsSODisposicion.next()) {
                    seccion3_8.add(new Paragraph(new Chunk("" + notNullFilter(rsSODisposicion.getString("DISPOSICION_NOMBRE")), TITULO4)));
                    Paragraph pOtrasDisposiciones = new Paragraph("" + notNullFilter(rsSODisposicion.getString("DISPOSICION_VINCULACION")));
                    pOtrasDisposiciones.setAlignment(Element.ALIGN_JUSTIFIED);
                    seccion3_8.add(new Paragraph(pOtrasDisposiciones));
                    seccion3_8.add(new Paragraph("\n"));
                }
                stSOdisposicion.close();

                // Agrega párrafo a la sección
                seccion3_8.add(new Paragraph("\n"));
                
                // Muestra tabla de Adjuntos
                PdfPTable tablaArchsOtrasDispos = generarTablaArchivos(folio, serial, url, (short) 3, (short) 8, (short) -1, (short) -1);
                seccion3_8.add(tablaArchsOtrasDispos);
                seccion3_8.add(new Paragraph("\n"));
            }

            content.add(chapter3);
            chapterList.add(chapter3);
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Capitulo 4">
            // Capitulo 4
            Chunk chapTitle4 = new Chunk("Descripción del sistema ambiental y señalamiento de la problemática ambiental, detectada en el área de influencia del proyecto.", TITULO1);
            Chapter chapter4 = new Chapter(new Paragraph(chapTitle4), 4);
            chapTitle4.setLocalDestination(chapter4.getTitle().getContent());

            // 4.1
            Chunk sec4_1_0Title = new Chunk("Delimitación del sistema ambiental", TITULO2);
            Section seccion4_1_0 = chapter4.addSection(new Paragraph(sec4_1_0Title));
            sec4_1_0Title.setLocalDestination(seccion4_1_0.getTitle().getContent());
            seccion4_1_0.setIndentationLeft(10);
            seccion4_1_0.add(new Paragraph("\n"));

            // 4.1.1
            Chunk sec4_1_1Title = new Chunk("Delimitación del sistema ambiental", TITULO2);
            Section seccion4_1_1 = seccion4_1_0.addSection(new Paragraph(sec4_1_1Title));
            sec4_1_1Title.setLocalDestination(seccion4_1_1.getTitle().getContent());
            seccion4_1_1.setIndentationLeft(10);
            seccion4_1_1.add(new Paragraph("\n"));
            seccion4_1_1.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("DESC_DELIM_SISTEM_AMB")) + "</p>"));
            seccion4_1_1.add(new Paragraph("\n"));
            
            // Muestra tabla de Adjuntos
            // CAPITULO_ID='4' AND SUBCAPITULO_ID='1' AND SECCION_ID='1'";
            PdfPTable tblArchsDelSistAmb = generarTablaArchivos(folio, serial, url, (short) 4, (short) 1, (short) 1, (short) -1);
            seccion4_1_1.add(tblArchsDelSistAmb);
            seccion4_1_1.add(new Paragraph("\n"));

            // 4.1.2
            Chunk sec4_1_2Title = new Chunk("Delimitación del área de influencia", TITULO2);
            Section seccion4_1_2 = seccion4_1_0.addSection(new Paragraph(sec4_1_2Title));
            sec4_1_2Title.setLocalDestination(seccion4_1_2.getTitle().getContent());
            seccion4_1_2.setIndentationLeft(10);
            seccion4_1_2.add(new Paragraph("\n"));
            seccion4_1_2.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("DESC_DELIM_AREA_INFLU")) + "</p>"));
            seccion4_1_2.add(new Paragraph("\n"));
            
            // Muestra tabla de Adjuntos
            // AND CAPITULO_ID='4' AND SUBCAPITULO_ID='1' AND SECCION_ID='2'";
            PdfPTable tblArchsDelAreaInf = generarTablaArchivos(folio, serial, url, (short) 4, (short) 1, (short) 2, (short) -1);
            seccion4_1_2.add(tblArchsDelAreaInf);
            seccion4_1_2.add(new Paragraph("\n"));

            // 4.1.3
            Chunk sec4_1_3Title = new Chunk("Delimitación del Sitio del Proyecto", TITULO2);
            Section seccion4_1_3 = seccion4_1_0.addSection(new Paragraph(sec4_1_3Title));
            sec4_1_3Title.setLocalDestination(seccion4_1_3.getTitle().getContent());
            seccion4_1_3.setIndentationLeft(10);
            seccion4_1_3.add(new Paragraph("\n"));
            seccion4_1_3.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("DESC_DELIM_SIT_PROY")) + "</p>"));
            seccion4_1_3.add(new Paragraph("\n"));
            
            // Muestra tabla de Adjuntos
            // CAPITULO_ID='4' AND SUBCAPITULO_ID='1' AND SECCION_ID='3'";
            PdfPTable tblArchsDelSitProy = generarTablaArchivos(folio, serial, url, (short) 4, (short) 1, (short) 3, (short) -1);
            seccion4_1_3.add(tblArchsDelSitProy);
            seccion4_1_3.add(new Paragraph("\n"));

            // 4.2
            Chunk sec4_2_0Title = new Chunk("Aspectos abióticos.", TITULO2);
            Section seccion4_2_0 = chapter4.addSection(new Paragraph(sec4_2_0Title));
            sec4_2_0Title.setLocalDestination(seccion4_2_0.getTitle().getContent());
            seccion4_2_0.setIndentationLeft(10);
            seccion4_2_0.add(new Paragraph("\n"));

            // 4.2.1
            Chunk sec4_2_1Title = new Chunk("Clima y Fenómenos Meteorológicos", TITULO2);
            Section seccion4_2_1 = seccion4_2_0.addSection(new Paragraph(sec4_2_1Title));
            sec4_2_1Title.setLocalDestination(seccion4_2_1.getTitle().getContent());
            seccion4_2_1.setIndentationLeft(10);
            seccion4_2_1.add(new Paragraph("\n"));
            String sql4_2_1 = "SELECT CLIMA_TIPO, DES_TEM, DESC_PREC FROM CLIMAS WHERE CLIMAS.NUM_FOLIO = '" + folio + "' AND CLIMAS.VERSION = " + serial + " GROUP BY COMP, CLIMA_TIPO, DES_TEM, DESC_PREC";
            PdfPTable tabClimFe = ReportUtil.buildTable(con, sql4_2_1, new String[]{"CLIMA_TIPO", "DES_TEM", "DESC_PREC"}, new String[]{"Clave Climatológica", "Agrupación/Temp", "Precipitación"});
            seccion4_2_1.add(tabClimFe);
            seccion4_2_1.add(new Paragraph("\n"));
            
            Chunk sec_ClimaFenMet_Observ_Title = new Chunk("Observaciones", TITULO2);
            Section seccion_ClimaFenMet_Observ = seccion4_2_1.addSection(new Paragraph(sec_ClimaFenMet_Observ_Title));
            sec_ClimaFenMet_Observ_Title.setLocalDestination(seccion_ClimaFenMet_Observ.getTitle().getContent());
            seccion_ClimaFenMet_Observ.setIndentationLeft(10);
            seccion_ClimaFenMet_Observ.add(new Paragraph("\n"));
            seccion_ClimaFenMet_Observ.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("CLIMA_FEN_MET_OBSERV")) + "</p>"));
            seccion_ClimaFenMet_Observ.add(new Paragraph("\n"));
            seccion_ClimaFenMet_Observ.add(new Paragraph("\n"));
            
            Chunk sec_CambioClim_Title = new Chunk("Cambio Climático", TITULO2);
            Section secccion_CambClim = seccion4_2_1.addSection(new Paragraph(sec_CambioClim_Title));
            sec_CambioClim_Title.setLocalDestination(secccion_CambClim.getTitle().getContent());
            secccion_CambClim.setIndentationLeft(10);
            secccion_CambClim.add(new Paragraph("\n"));
            secccion_CambClim.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("TEMP_FEN_MET_DESCRIP")) + "</p>"));
            secccion_CambClim.add(new Paragraph("\n"));
            secccion_CambClim.add(new Paragraph("\n"));
            
            // Muestra tabla de Adjuntos
            // CAPITULO_ID='4' AND SUBCAPITULO_ID='2' AND SECCION_ID='1'";
            PdfPTable tblArchsClimaFenMet = generarTablaArchivos(folio, serial, url, (short) 4, (short) 2, (short) 1, (short) -1);
            seccion4_2_1.add(tblArchsClimaFenMet);
            seccion4_2_1.add(new Paragraph("\n"));
            

            // 4.2.2
            Chunk sec4_2_2Title = new Chunk("Geología y Geomorfología", TITULO2);
            Section seccion4_2_2 = seccion4_2_0.addSection(new Paragraph(sec4_2_2Title));
            sec4_2_2Title.setLocalDestination(seccion4_2_2.getTitle().getContent());
            seccion4_2_2.setIndentationLeft(10);
            seccion4_2_2.add(new Paragraph("\n"));
            seccion4_2_2.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("DESC_GEOS")) + "</p>"));

            // 4.2.3
            Chunk sec4_2_3Title = new Chunk("Suelos", TITULO2);
            Section seccion4_2_3 = seccion4_2_0.addSection(new Paragraph(sec4_2_3Title));
            sec4_2_3Title.setLocalDestination(seccion4_2_3.getTitle().getContent());
            seccion4_2_3.setIndentationLeft(10);
            seccion4_2_3.add(new Paragraph("\n"));
            seccion4_2_3.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("DESC_SUELO")) + "</p>"));
            seccion4_2_3.add(new Paragraph("\n"));
            
            // Muestra tabla de Adjuntos
            // CAPITULO_ID='4' AND SUBCAPITULO_ID='2' AND SECCION_ID='1' AND APARTADO_ID = '3'";
            PdfPTable tblArchsSuelos = generarTablaArchivos(folio, serial, url, (short) 4, (short) 2, (short) 1, (short) 3);
            seccion4_2_3.add(tblArchsSuelos);
            seccion4_2_3.add(new Paragraph("\n"));
            seccion4_2_3.add(new Paragraph("\n"));
            System.out.println("\nDespués de: Suelos.\n");

            // 4.2.4
            Chunk sec4_2_4Title = new Chunk("Hidrología superficial", TITULO2);
            Section seccion4_2_4 = seccion4_2_0.addSection(new Paragraph(sec4_2_4Title));
            sec4_2_4Title.setLocalDestination(seccion4_2_4.getTitle().getContent());
            seccion4_2_4.setIndentationLeft(10);
            seccion4_2_4.add(new Paragraph("\n"));
            String sql4_2_4 = "SELECT M.CUE_HID AS CUE_HID, M.SUBC_HID AS SUBC_HID, M.NOM_MIC AS NOM_MIC  FROM MICROCUENCA M WHERE M.NUM_FOLIO = '" + folio + "' AND M.VERSION = " + serial + "  GROUP BY M.CUE_HID, M.SUBC_HID, M.NOM_MIC";
            PdfPTable tabHidroSub = ReportUtil.buildTable(con, sql4_2_4, new String[]{"CUE_HID", "SUBC_HID", "NOM_MIC"}, new String[]{"Cuenca", "Subcuenca", "Microcuenca"});
            seccion4_2_4.add(tabHidroSub);
            seccion4_2_4.add(new Paragraph("\n"));
            System.out.println("\nDespués de: Hidrología superficial.\n");
            
            /**
             * TABLA: CUERPOS DE AGUA
             */
            Chunk sec_HSupTitle_CuerposAgua = new Chunk("Cuerpos de agua", TITULO2);
            Section seccion_HSup_CAgua = seccion4_2_4.addSection(new Paragraph(sec_HSupTitle_CuerposAgua));
            sec_HSupTitle_CuerposAgua.setLocalDestination(seccion_HSup_CAgua.getTitle().getContent());
            seccion_HSup_CAgua.setIndentationLeft(10);
            seccion_HSup_CAgua.add(new Paragraph("\n"));
            
            PdfPTable tblCuerposAgua = new PdfPTable(3);
            tblCuerposAgua.setWidthPercentage((float) 100);
            float[] tblCAguaColumnWidths = new float[] {50f, 32f, 18f};
            tblCuerposAgua.setWidths(tblCAguaColumnWidths);
            
            PdfPCell titCAgua_Nombre = new PdfPCell(new Phrase("Nombre", TITULO3));
            PdfPCell titCAgua_Tipo = new PdfPCell(new Phrase("Tipo", TITULO3));
            PdfPCell titCAgua_Distancia = new PdfPCell(new Phrase("Distancia al \nproyecto (m)", TITULO3));
            
            // Primera fila: Títulos
            titCAgua_Nombre.setHorizontalAlignment(Element.ALIGN_CENTER);
            titCAgua_Nombre.setVerticalAlignment(Element.ALIGN_MIDDLE);
            tblCuerposAgua.addCell(titCAgua_Nombre);
            
            titCAgua_Tipo.setHorizontalAlignment(Element.ALIGN_CENTER);
            titCAgua_Tipo.setVerticalAlignment(Element.ALIGN_MIDDLE);
            tblCuerposAgua.addCell(titCAgua_Tipo);
            
            titCAgua_Distancia.setHorizontalAlignment(Element.ALIGN_CENTER);
            tblCuerposAgua.addCell(titCAgua_Distancia);
            
            String sqlCuerposAgua = "WITH REGS_SECUENCIALES AS ("
				+ "SELECT CA.CUERPOS_AGUA_NOMBRE, CL.CLASIFICACION_B, CA.CUERPOS_AGUA_DISTANCIA"
				+ " FROM CUERPOS_AGUA_PROYECTO CA, CAT_CLASIFICACION_B CL"
				+ " WHERE CA.CUERPOS_AGUA_TIPO = CL.CLASIFICACION_B_ID"
				+ " AND FOLIO_PROYECTO = '" + folio + "' AND SERIAL_PROYECTO = " + serial
				+ " ORDER BY CUERPOS_AGUA_ID)"
				+ "SELECT ROWNUM AS SECUENCIAL, rs.* FROM REGS_SECUENCIALES rs";
            
            ResultSet rsCuerposAgua = st2.executeQuery(sqlCuerposAgua);
            
            while (rsCuerposAgua.next()){
                PdfPCell valNombre = new PdfPCell(new Phrase("" + notNullFilter(rsCuerposAgua.getString("CUERPOS_AGUA_NOMBRE"))));
                PdfPCell valTipo = new PdfPCell(new Phrase("" + notNullFilter(rsCuerposAgua.getString("CLASIFICACION_B"))));
                PdfPCell valDistancia = new PdfPCell(new Phrase("" + notNullFilter(rsCuerposAgua.getString("CUERPOS_AGUA_DISTANCIA"))));
                
                // Fila con valores
                tblCuerposAgua.addCell(valNombre);
                tblCuerposAgua.addCell(valTipo);
                valDistancia.setHorizontalAlignment(Element.ALIGN_CENTER);
                tblCuerposAgua.addCell(valDistancia);
            }
            rsCuerposAgua.close();
            seccion4_2_4.add(tblCuerposAgua);
            seccion4_2_4.add(new Paragraph("\n"));
            // Cierre de TABLA: "CUERPOS DE AGUA"
            System.out.println("\nDespués de tabla: Cuerpos de agua.\n");

            /**
             * CUADRO DE TEXTO ENRIQUECIDO: HIDROLOGIA SUPERFICIAL - DESCRIPCION
             */
            Chunk sec_HSup_Descripcion_Title = new Chunk("Descripción", TITULO2);
            Section seccion_HSup_Descr = seccion4_2_4.addSection(new Paragraph(sec_HSup_Descripcion_Title));
            sec_HSup_Descripcion_Title.setLocalDestination(seccion_HSup_Descr.getTitle().getContent());
            seccion_HSup_Descr.setIndentationLeft(10);
            seccion4_2_4.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("HIDRO_SUP_OBSERVA")) + "</p>"));
            seccion4_2_4.add(new Paragraph("\n"));
            seccion4_2_4.add(new Paragraph("\n"));

            
            // 4.2.5
            Chunk sec4_2_5Title = new Chunk("Hidrología subterránea", TITULO2);
            Section seccion4_2_5 = seccion4_2_0.addSection(new Paragraph(sec4_2_5Title));
            sec4_2_5Title.setLocalDestination(seccion4_2_5.getTitle().getContent());
            seccion4_2_5.setIndentationLeft(10);
            seccion4_2_5.add(new Paragraph("\n"));
            String sql4_2_5 = "SELECT CLV_ACUI, NOM_ACUI, DESC_DISPO, FECHA_DOF, SOBREEXP FROM ACUIFEROS A WHERE A.NUM_FOLIO = '" + folio + "' AND A.VERSION = " + serial + " GROUP BY CLV_ACUI, NOM_ACUI, DESC_DISPO, FECHA_DOF, SOBREEXP ";
            PdfPTable tabHidroS = ReportUtil.buildTable(con, sql4_2_5, new String[]{"CLV_ACUI", "NOM_ACUI", "DESC_DISPO", "FECHA_DOF", "SOBREEXP"}, new String[]{"Clave del Acuífero", "Nombre del Acuífero", "Disponibilidad", "Fecha D.O.F.", "¿Sobre explotado?"});
            seccion4_2_5.add(tabHidroS);
            seccion4_2_5.add(new Paragraph("\n"));
            
            Chunk sec_HSub_Observ_Title = new Chunk("Observaciones", TITULO2);
            Section seccion_HSub_Observ = seccion4_2_5.addSection(new Paragraph(sec_HSub_Observ_Title));
            sec_HSub_Observ_Title.setLocalDestination(seccion_HSub_Observ.getTitle().getContent());
            seccion_HSub_Observ.setIndentationLeft(10);
            seccion_HSub_Observ.add(new Paragraph("\n"));
            seccion_HSub_Observ.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("HIDRO_SUBT_OBSERVA")) + "</p>"));
            seccion_HSub_Observ.add(new Paragraph("\n"));
            seccion_HSub_Observ.add(new Paragraph("\n"));
            

            // 4.2.9
            Chunk sec4_2_9Title = new Chunk("Estudios Especiales", TITULO2);
            Section seccion4_2_9 = seccion4_2_0.addSection(new Paragraph(sec4_2_9Title));
            sec4_2_9Title.setLocalDestination(seccion4_2_9.getTitle().getContent());
            seccion4_2_9.setIndentationLeft(10);
            seccion4_2_9.add(new Paragraph("\n"));

            String sql429 = "SELECT FOLIO_SERIAL, SERIAL_PROYECTO, ESTUDIO_DESCRIPCION, EE.ESTUDIO_ID AS ESTUDIO_ID, ANEXO_NOMBRE as ANEXO_ESTUDIO,"
                    + "'" + url + "' AS URL "
                    + " FROM ESTUDIOS_ESP_PROY EE, CAT_EST_ESP CT"
                    + " WHERE EE.ESTUDIO_ESPECIAL = CT.ESTUDIO_ID"
                    + " AND EE.FOLIO_SERIAL = '" + folio + "' AND EE.SERIAL_PROYECTO = " + serial + " ";
            PdfPTable tabEstudios = ReportUtil.buildTable(con, sql429, new String[]{"ESTUDIO_DESCRIPCION", "ANEXO_ESTUDIO"}, new String[]{"Estudio", "Adjunto"});
            seccion4_2_9.add(tabEstudios);
            seccion4_2_9.add(new Paragraph("\n"));
            
            // 4.2.6
            Chunk sec4_2_6Title = new Chunk("Tipo de vegetación", TITULO2);
            Section seccion4_2_6 = seccion4_2_0.addSection(new Paragraph(sec4_2_6Title));
            sec4_2_6Title.setLocalDestination(seccion4_2_6.getTitle().getContent());
            seccion4_2_6.setIndentationLeft(10);
            seccion4_2_6.add(new Paragraph("\n"));
            //String sql4_2_6 = "SELECT suelo_Componente A,suelo_Descripcion B,suelo_Tipo_Ecov C,suelo_Tipo_Gen D,suelo_Fase_Vs E,suelo_Area_Sigeia F, CASE suelo_Valida_Resultado WHEN 'S' THEN 'SI' ELSE 'NO' END G,suelo_Cat_Uso H,suelo_Diagnostico I FROM SUELO_VEGETACION_PROYECTO e  WHERE FOLIO_PROYECTO = '" + folio + "' and SERIAL_PROYECTO = " + serial + " ORDER BY suelo_Componente";
            //PdfPTable tblCap4TipoVeg = ReportUtil.buildTableVinculacion(con, sql4_2_6, new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I"}, new String[]{"Componente", "Descripción", "Grupo de vegetación", "Tipo de vegetación o Uso del suelo", "Fase de vegetación secundaria)", "Superficie de incidencia (m2)", "Superficie de incidencia (m2)", "Uso suelo", "Diagnóstico"});

            /**
             * La tabla fue creada y llenada previamente con la del capítulo 2, ya que la información se toma de las mismas tablas,
             * la única diferencia es que esta tabla lleva la información capturada para el "Diagnóstico"
             */
            seccion4_2_6.add(tblCap4TipoVeg);
            seccion4_2_6.add(new Paragraph("\n"));

            // 4.2.7
            Chunk sec4_2_7Title = new Chunk("Flora", TITULO2);
            Section seccion4_2_7 = seccion4_2_0.addSection(new Paragraph(sec4_2_7Title));
            sec4_2_7Title.setLocalDestination(seccion4_2_7.getTitle().getContent());
            seccion4_2_7.setIndentationLeft(10);
            seccion4_2_7.add(new Paragraph("\n"));

            //String sql427p = "SELECT c.flora_proyid, c.flora_Grupo, c.flora_Familia, c.flora_Nom_Cient, c.flora_Nom_Comun, c.flora_endemico, c.flora_cat_nom59, c.id_archivo_programa FROM FLORA_PROYECTO c  WHERE FOLIO_PROYECTO = '" + folio + "' and SERIAL_PROYECTO = " + serial + "";
            String sql427p="SELECT FL.*, AP.FILENAME || '.' || AP.EXTENSION AS NOMBRE_ARCHIVO, AP.SEQ_ID AS ID"
            		+ "   FROM ("
            		+ "         SELECT c.FLORA_CLASE AS CLASE, c.flora_Familia AS FAMILIA, c.flora_Nom_Cient AS NOMBRE_CIENTIFICO, c.flora_Nom_Comun AS NOMBRE_COMUN"
            		+ "          , c.flora_cat_nom59 AS CAT_59, c.flora_endemico AS ENDEMICO,  c.flora_cites AS CITES, c.id_archivo_programa"
            		+ " FROM FLORA_PROYECTO c  WHERE FOLIO_PROYECTO = '"+folio+"' and SERIAL_PROYECTO = "+serial
            		+ " ) FL"
            		+ " LEFT JOIN ARCHIVOS_PROYECTO AP ON FL.ID_ARCHIVO_PROGRAMA = AP.SEQ_ID ORDER BY NOMBRE_CIENTIFICO";
            ResultSet rsFlora = st2.executeQuery(sql427p);
            
            PdfPTable tabFlora = new PdfPTable(8);
            tabFlora.setWidthPercentage((float) 100);
            float[] tabFloraColumnWidths = new float[] {15f, 15f, 15f, 15f, 15f, 15f, 15f, 15f};
            tabFlora.setWidths(tabFloraColumnWidths);
            
            PdfPCell titCitesFlora = new PdfPCell(new Phrase("Clase", TITULO4));
            PdfPCell titPresenciaFlora = new PdfPCell(new Phrase("Familia", TITULO4));
            PdfPCell titRegistroFlora = new PdfPCell(new Phrase("Nombre científico", TITULO4));
            PdfPCell titAbundanciaFlora = new PdfPCell(new Phrase("Nombre común", TITULO4));
            PdfPCell titDensidadFlora = new PdfPCell(new Phrase("Categoría NOM-059-2010", TITULO4));
            PdfPCell titDistribucionFlora = new PdfPCell(new Phrase("Endémico", TITULO4));
            PdfPCell titIndFlora = new PdfPCell(new Phrase("CITES", TITULO4));
            PdfPCell titAdjuntoFlora = new PdfPCell(new Phrase("Adjunto", TITULO4));
            
         // Primera fila: Titulos
            tabFlora.addCell(titCitesFlora);
            tabFlora.addCell(titPresenciaFlora);
            tabFlora.addCell(titRegistroFlora);
            tabFlora.addCell(titAbundanciaFlora);
            tabFlora.addCell(titDensidadFlora);
            tabFlora.addCell(titDistribucionFlora);
            tabFlora.addCell(titIndFlora);
            tabFlora.addCell(titAdjuntoFlora);
            
            while (rsFlora.next()) {
                 
            	PdfPCell valclase = new PdfPCell(new Phrase("" + notNullFilter(rsFlora.getString("CLASE"))));
                PdfPCell valfamilia = new PdfPCell(new Phrase("" + notNullFilter(rsFlora.getString("FAMILIA"))));
                PdfPCell valnom_cient = new PdfPCell(new Phrase("" + notNullFilter(rsFlora.getString("NOMBRE_CIENTIFICO"))));
                PdfPCell valnom_com = new PdfPCell(new Phrase("" + notNullFilter(rsFlora.getString("NOMBRE_COMUN"))));
                PdfPCell valcat59 = new PdfPCell(new Phrase("" + notNullFilter(rsFlora.getString("CAT_59"))));
                PdfPCell valendemico = new PdfPCell(new Phrase("" + notNullFilter(rsFlora.getString("ENDEMICO"))));
                PdfPCell valcites = new PdfPCell(new Phrase("" + notNullFilter(rsFlora.getString("CITES"))));
                
                
                PdfPCell valadjunto = new PdfPCell(new Phrase("Sin adjunto", PARRAFO));
                valadjunto.setHorizontalAlignment(Element.ALIGN_CENTER);
                String id = rsFlora.getString("ID");
                if (null != id){
                    String filename = rsFlora.getString("NOMBRE_ARCHIVO");
                    if (null != filename) {
                    	valadjunto = generarCeldaArchivo(filename, url, id.trim());
                    }
            	}
                
                tabFlora.addCell(valclase);
                tabFlora.addCell(valfamilia);
                tabFlora.addCell(valnom_cient);
                tabFlora.addCell(valnom_com);
                tabFlora.addCell(valcat59);
                tabFlora.addCell(valendemico);
                tabFlora.addCell(valcites);
                
                tabFlora.addCell(valadjunto);                        
            }
            seccion4_2_7.add(tabFlora);
            rsFlora.close();
            seccion4_2_7.add(new Paragraph("\n"));
            
            String sql427_1p="SELECT c.FLORA_NOM_CIENT as NOM_CIENT, CASE c.flora_presencia_sa WHEN 'N' THEN 'NO' ELSE 'SI' END AS FLORA_PRESENCIA, c.flora_registro AS FLORA_REGISTRO,"
            		+ " c.flora_abundancia AS FLORA_ABUNDANCIA, c.flora_densidad AS FLORA_DENSIDAD, c.flora_distribucion AS FLORA_DISTRIBUCION, c.flora_num_indv_afec AS FLORA_NUM_INDV_AFEC "
            		+ " FROM FLORA_PROYECTO c "
            		+ " WHERE c.FOLIO_PROYECTO = '"+folio+"' and c.SERIAL_PROYECTO = "+serial+" ORDER BY NOM_CIENT";
            ResultSet rsFlora_1 = st2.executeQuery(sql427_1p);
            
            PdfPTable tabFlora_1 = new PdfPTable(7);
            tabFlora_1.setWidthPercentage((float) 100);
            float[] tabFloraColumnWidths_1 = new float[] {15f, 15f, 15f, 15f, 15f, 15f, 15f};
            tabFlora_1.setWidths(tabFloraColumnWidths_1);
            
            PdfPCell titNom_cient = new PdfPCell(new Phrase("Nombre científico", TITULO4));
            PdfPCell titPresencia = new PdfPCell(new Phrase("Presencia", TITULO4));
            PdfPCell titRegistro = new PdfPCell(new Phrase("Registro", TITULO4));
            PdfPCell titAbundancia = new PdfPCell(new Phrase("Abundancia", TITULO4));
            PdfPCell titDensidad = new PdfPCell(new Phrase("Densidad", TITULO4));
            PdfPCell titDistribucion = new PdfPCell(new Phrase("Distribución", TITULO4));
            PdfPCell titInd = new PdfPCell(new Phrase("No. de individuos a afectar", TITULO4));
            
         // Primera fila: Titulos
            tabFlora_1.addCell(titNom_cient);
            tabFlora_1.addCell(titPresencia);
            tabFlora_1.addCell(titRegistro);
            tabFlora_1.addCell(titAbundancia);
            tabFlora_1.addCell(titDensidad);
            tabFlora_1.addCell(titDistribucion);
            tabFlora_1.addCell(titInd);
            
            while (rsFlora_1.next()) {
            	PdfPCell valclase1 = new PdfPCell(new Phrase("" + notNullFilter(rsFlora_1.getString("NOM_CIENT"))));
                PdfPCell valfamilia1 = new PdfPCell(new Phrase("" + notNullFilter(rsFlora_1.getString("FLORA_PRESENCIA"))));
                PdfPCell valnom_cient1 = new PdfPCell(new Phrase("" + notNullFilter(rsFlora_1.getString("FLORA_REGISTRO"))));
                PdfPCell valnom_com1 = new PdfPCell(new Phrase("" + notNullFilter(rsFlora_1.getString("FLORA_ABUNDANCIA"))));
                PdfPCell valcat591 = new PdfPCell(new Phrase("" + notNullFilter(rsFlora_1.getString("FLORA_DENSIDAD"))));
                PdfPCell valendemico1 = new PdfPCell(new Phrase("" + notNullFilter(rsFlora_1.getString("FLORA_DISTRIBUCION"))));
                PdfPCell valcites1 = new PdfPCell(new Phrase("" + notNullFilter(rsFlora_1.getString("FLORA_NUM_INDV_AFEC"))));                
                
                if(rsFlora_1.getString("FLORA_REGISTRO")!=null || rsFlora_1.getString("FLORA_ABUNDANCIA")!=null || rsFlora_1.getString("FLORA_DENSIDAD")!=null 
            		|| rsFlora_1.getString("FLORA_DISTRIBUCION")!= null || rsFlora_1.getString("FLORA_NUM_INDV_AFEC")!=null)
                {
                	tabFlora_1.addCell(valclase1);
                    tabFlora_1.addCell(valfamilia1);
                    tabFlora_1.addCell(valnom_cient1);
                    tabFlora_1.addCell(valnom_com1);
                    tabFlora_1.addCell(valcat591);
                    tabFlora_1.addCell(valendemico1);
                    tabFlora_1.addCell(valcites1);
                }
                
                                        
            }
            seccion4_2_7.add(tabFlora_1);
            rsFlora_1.close();
            
            
            seccion4_2_7.add(new Paragraph("\n"));
            System.out.println("Imprimiendo Flora y Fauna");
            //CAPITULO_ID='4' AND SUBCAPITULO_ID='2' AND SECCION_ID='2' AND APARTADO_ID = '2'" ;
            
//===========================================================================================================================================
            // 4.2.8
            Chunk sec4_2_8Title = new Chunk("Fauna", TITULO2);
            Section seccion4_2_8 = seccion4_2_0.addSection(new Paragraph(sec4_2_8Title));
            sec4_2_8Title.setLocalDestination(seccion4_2_8.getTitle().getContent());
            seccion4_2_8.setIndentationLeft(10);
            seccion4_2_8.add(new Paragraph("\n"));

            //String sql428p = "SELECT c.fauna_proyid, c.fauna_Grupo, c.fauna_Familia, c.fauna_Nom_Cient, c.fauna_Nom_Comun, c.fauna_endemico, c.fauna_cat_nom59, c.id_archivo_programa "
            	//			+ "FROM FAUNA_PROYECTO c  WHERE FOLIO_PROYECTO = '" + folio + "' and SERIAL_PROYECTO = " + serial + "";
            String sql428p = "SELECT FL.*, AP.FILENAME || '.' || AP.EXTENSION AS NOMBRE_ARCHIVO, AP.SEQ_ID AS ID"
            		+ " FROM ("
            		+ " SELECT c.FAUNA_CLASE as CLASE, c.fauna_Familia as FAMILIA, c.fauna_Nom_Cient AS CIENTIFICO, c.fauna_Nom_Comun AS COMUN,  c.fauna_cat_nom59 AS NOM_59, c.FAUNA_CITES AS CITES, c.fauna_endemico AS ENDEMICO,  c.id_archivo_programa"
            		+ " FROM FAUNA_PROYECTO c  WHERE FOLIO_PROYECTO = '"+folio+"' and SERIAL_PROYECTO ="+serial
            		+ "	) FL"
            		+ "	LEFT JOIN ARCHIVOS_PROYECTO AP ON FL.ID_ARCHIVO_PROGRAMA = AP.SEQ_ID ORDER BY CIENTIFICO";
            ResultSet rsFauna = st2.executeQuery(sql428p);
            
            PdfPTable tabFauna = new PdfPTable(8);
            tabFauna.setWidthPercentage((float) 100);
            float[] tabFaunaColumnWidths = new float[] {8f, 12f, 14f, 13f, 15f, 18f, 10f, 10f};
            tabFauna.setWidths(tabFaunaColumnWidths);
            
            PdfPCell titCitesFauna = new PdfPCell(new Phrase("Clase", TITULO4));
            PdfPCell titPresenciaFauna = new PdfPCell(new Phrase("Familia", TITULO4));
            PdfPCell titRegistroFauna = new PdfPCell(new Phrase("Nombre científico", TITULO4));
            PdfPCell titAbundanciaFauna = new PdfPCell(new Phrase("Nombre común", TITULO4));
            PdfPCell titDensidadFauna = new PdfPCell(new Phrase("Categoría NOM-059-2010", TITULO4));
            PdfPCell titDistribucionFauna = new PdfPCell(new Phrase("Endémico", TITULO4));
            PdfPCell titIndFauna = new PdfPCell(new Phrase("CITES", TITULO4));
            PdfPCell titAdjuntoFauna = new PdfPCell(new Phrase("Adjunto", TITULO4));
            
         // Primera fila: Titulos
            tabFauna.addCell(titCitesFauna);
            tabFauna.addCell(titPresenciaFauna);
            tabFauna.addCell(titRegistroFauna);
            tabFauna.addCell(titAbundanciaFauna);
            tabFauna.addCell(titDensidadFauna);
            tabFauna.addCell(titDistribucionFauna);
            tabFauna.addCell(titIndFauna);
            tabFauna.addCell(titAdjuntoFauna);
            
            while (rsFauna.next()) 
            {
            	PdfPCell valclase = new PdfPCell(new Phrase("" + notNullFilter(rsFauna.getString("CLASE"))));
                PdfPCell valfamilia = new PdfPCell(new Phrase("" + notNullFilter(rsFauna.getString("FAMILIA"))));
                PdfPCell valnom_cient = new PdfPCell(new Phrase("" + notNullFilter(rsFauna.getString("CIENTIFICO"))));
                PdfPCell valnom_com = new PdfPCell(new Phrase("" + notNullFilter(rsFauna.getString("COMUN"))));
                PdfPCell valcat59 = new PdfPCell(new Phrase("" + notNullFilter(rsFauna.getString("NOM_59"))));
                PdfPCell valendemico = new PdfPCell(new Phrase("" + notNullFilter(rsFauna.getString("ENDEMICO"))));
                PdfPCell valcites = new PdfPCell(new Phrase("" + notNullFilter(rsFauna.getString("CITES"))));
                
                PdfPCell valadjunto = new PdfPCell(new Phrase("Sin adjunto", PARRAFO));
                valadjunto.setHorizontalAlignment(Element.ALIGN_CENTER);
                String id = rsFauna.getString("ID");
                if (null != id){
                    String filename = rsFauna.getString("NOMBRE_ARCHIVO");
                    if (null != filename) {
                    	valadjunto = generarCeldaArchivo(filename, url, id.trim());
                    }
            	}
                
                tabFauna.addCell(valclase);
                tabFauna.addCell(valfamilia);
                tabFauna.addCell(valnom_cient);
                tabFauna.addCell(valnom_com);
                tabFauna.addCell(valcat59);
                tabFauna.addCell(valendemico);
                tabFauna.addCell(valcites);
                tabFauna.addCell(valadjunto); 
            }
            seccion4_2_8.add(tabFauna);
            rsFauna.close();
            seccion4_2_8.add(new Paragraph("\n"));
            
            String sql428_1p="SELECT c.fauna_Nom_Cient AS CIENTIFICO,CASE c.fauna_presencia_sa WHEN 'N' THEN 'NO' ELSE 'SI' END AS FAUNA_PRESENCIA_SA, c.fauna_registro AS FAUNA_REGISTRO,"
            		+ " c.fauna_abundancia AS FAUNA_ABUNDANCIA, c.fauna_densidad AS FAUNA_DENSIDAD, c.fauna_distribucion AS FAUNA_DISTRIBUCION, c.fauna_num_indv_afec AS FAUNA_NUM_INDV_AFEC"
            		+ " FROM FAUNA_PROYECTO c"
            		+ " WHERE c.FOLIO_PROYECTO = '14730' and c.SERIAL_PROYECTO =1 ORDER BY CIENTIFICO";
            ResultSet rsFauna_1 = st2.executeQuery(sql428_1p);
            
            PdfPTable tabFauna_1 = new PdfPTable(7);
            tabFauna_1.setWidthPercentage((float) 100);
            float[] tabFaunaColumnWidths_1 = new float[] {15f, 15f, 15f, 15f, 15f, 15f, 15f};
            tabFauna_1.setWidths(tabFaunaColumnWidths_1);
            
            PdfPCell titNomcient = new PdfPCell(new Phrase("Nombre científico", TITULO4));
            PdfPCell titPresenci = new PdfPCell(new Phrase("Presencia", TITULO4));
            PdfPCell titRegistr = new PdfPCell(new Phrase("Registro", TITULO4));
            PdfPCell titAbundanci = new PdfPCell(new Phrase("Abundancia", TITULO4));
            PdfPCell titDensida = new PdfPCell(new Phrase("Densidad", TITULO4));
            PdfPCell titDistribucio = new PdfPCell(new Phrase("Distribución", TITULO4));
            PdfPCell titIndi = new PdfPCell(new Phrase("No. de individuos a afectar", TITULO4));
            
         // Primera fila: Titulos
            tabFauna_1.addCell(titNomcient);
            tabFauna_1.addCell(titPresenci);
            tabFauna_1.addCell(titRegistr);
            tabFauna_1.addCell(titAbundanci);
            tabFauna_1.addCell(titDensida);
            tabFauna_1.addCell(titDistribucio);
            tabFauna_1.addCell(titIndi);
                       
            while (rsFauna_1.next()) {
            	PdfPCell valclase1 = new PdfPCell(new Phrase("" + notNullFilter(rsFauna_1.getString("CIENTIFICO"))));
                PdfPCell valfamilia1 = new PdfPCell(new Phrase("" + notNullFilter(rsFauna_1.getString("FAUNA_PRESENCIA_SA"))));
                PdfPCell valnom_cient1 = new PdfPCell(new Phrase("" + notNullFilter(rsFauna_1.getString("FAUNA_REGISTRO"))));
                PdfPCell valnom_com1 = new PdfPCell(new Phrase("" + notNullFilter(rsFauna_1.getString("FAUNA_ABUNDANCIA"))));
                PdfPCell valcat591 = new PdfPCell(new Phrase("" + notNullFilter(rsFauna_1.getString("FAUNA_DENSIDAD"))));
                PdfPCell valendemico1 = new PdfPCell(new Phrase("" + notNullFilter(rsFauna_1.getString("FAUNA_DISTRIBUCION"))));
                PdfPCell valcites1 = new PdfPCell(new Phrase("" + notNullFilter(rsFauna_1.getString("FAUNA_NUM_INDV_AFEC"))));                
                
                if(rsFauna_1.getString("FAUNA_REGISTRO")!=null || rsFauna_1.getString("FAUNA_ABUNDANCIA")!=null || rsFauna_1.getString("FAUNA_DENSIDAD")!=null
            		|| rsFauna_1.getString("FAUNA_DISTRIBUCION")!=null || rsFauna_1.getString("FAUNA_NUM_INDV_AFEC")!=null)
                {
	                tabFauna_1.addCell(valclase1);
	                tabFauna_1.addCell(valfamilia1);
	                tabFauna_1.addCell(valnom_cient1);
	                tabFauna_1.addCell(valnom_com1);
	                tabFauna_1.addCell(valcat591);
	                tabFauna_1.addCell(valendemico1);
	                tabFauna_1.addCell(valcites1);	
                }
            }
            seccion4_2_8.add(tabFauna_1);
            rsFauna_1.close();
            
            
            seccion4_2_8.add(new Paragraph("\n"));
            System.out.println("Imprimiendo Flora y Fauna");
                        
            
            // CAPITULO_ID='4' AND SUBCAPITULO_ID='2' AND SECCION_ID='2' AND APARTADO_ID = '3'" ;
//===========================================================================================================================================
            
            // 4.2.10
            Chunk sec4_2_10Title = new Chunk("Análisis Biológico y Ecológico", TITULO2);
            Section seccion4_2_10 = seccion4_2_0.addSection(new Paragraph(sec4_2_10Title));
            sec4_2_10Title.setLocalDestination(seccion4_2_10.getTitle().getContent());
            seccion4_2_10.setIndentationLeft(10);
            seccion4_2_10.add(new Paragraph("\n"));
            seccion4_2_10.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("ANALISISBIO_VEG")) + "</p>"));

            // 4.3.1
            Chunk sec4_3_1Title = new Chunk("Paisaje", TITULO2);
            Section seccion4_3_1 = chapter4.addSection(new Paragraph(sec4_3_1Title));
            sec4_3_1Title.setLocalDestination(seccion4_3_1.getTitle().getContent());
            seccion4_3_1.setIndentationLeft(10);
            seccion4_3_1.add(new Paragraph("\n"));
            seccion4_3_1.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("DESC_PAISAJE_PROY")) + "</p>"));
            seccion4_3_1.add(new Paragraph("\n"));
            seccion4_3_1.add(new Paragraph("\n"));
            
            // Muestra tabla de Adjuntos
            // CAPITULO_ID='4' AND SUBCAPITULO_ID='3' AND SECCION_ID='0'";
            PdfPTable tblArchsPaisaje = generarTablaArchivos(folio, serial, url, (short) 4, (short) 3, (short) 0, (short) -1);
            seccion4_3_1.add(tblArchsPaisaje);
            seccion4_3_1.add(new Paragraph("\n"));
            

            // 4.4
            Chunk sec4_4_0Title = new Chunk("Medio Socioeconómico", TITULO2);
            Section seccion4_4_0 = chapter4.addSection(new Paragraph(sec4_4_0Title));
            sec4_4_0Title.setLocalDestination(seccion4_4_0.getTitle().getContent());
            seccion4_4_0.setIndentationLeft(10);
            seccion4_4_0.add(new Paragraph("\n"));

            // 4.4.1
            Chunk sec4_4_1Title = new Chunk("Dinámica Población", TITULO2);
            Section seccion4_4_1 = seccion4_4_0.addSection(new Paragraph(sec4_4_1Title));
            sec4_4_1Title.setLocalDestination(seccion4_4_1.getTitle().getContent());
            seccion4_4_1.setIndentationLeft(10);
            seccion4_4_1.add(new Paragraph("\n"));
            seccion4_4_1.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("MEDIO_SOCIOPROY")) + "</p>"));
            seccion4_4_1.add(new Paragraph("\n"));

            // 4.4.2
            Chunk sec4_4_2Title = new Chunk("Localidades Ubicadas dentro del Sistema Ambiental del Proyecto. Serie IV 2010, INEGI", TITULO2);
            Section seccion4_4_2 = seccion4_4_0.addSection(new Paragraph(sec4_4_2Title));
            sec4_4_2Title.setLocalDestination(seccion4_4_2.getTitle().getContent());
            seccion4_4_2.setIndentationLeft(10);
            seccion4_4_2.add(new Paragraph("\n"));
            String sql442 = "SELECT L.NOM_LOC AS NOM_LOC, L.NOM_ENT AS NOM_ENT, L.NOM_MUN AS NOM_MUN, L.POBMAS AS POBMAS, L.POBFEM AS POBFEM, L.TOTVIVIEND AS TOTVIVIEND, L.POBTOT2 AS POBTOT2, L.NOMTIPO AS NOMTIPO, L.TIPOLOC AS TIPOLOC, L.AMBITO AS AMBITO, L.POB_INDI AS POB_INDI "
                    + "FROM DGIRA_MIAE2.LOCALID_INDIGENAS L WHERE L.NUM_FOLIO = '" + folio + "' AND L.VERSION = " + serial + " "
                    + "GROUP BY L.NOM_LOC, L.NOM_ENT, L.NOM_MUN, L.POBMAS, L.POBFEM, L.TOTVIVIEND, L.POBTOT2, L.NOMTIPO, L.TIPOLOC, L.AMBITO, L.POB_INDI  ORDER BY L.NOM_ENT, L.NOM_MUN ";
            PdfPTable tabMedioSocioeconomico = ReportUtil.buildTableVinculacion(con, sql442, new String[]{"NOM_LOC", "NOM_ENT", "NOM_MUN", "POBMAS", "POBFEM", "TOTVIVIEND", "POBTOT2", "NOMTIPO", "AMBITO", "POB_INDI", "TIPOLOC"}, new String[]{"Localidad", "Entidad\n Federativa", "Nombre\n Municipio", "Población\n Masculina", "Población\n Femenina", "Total\n Viviendas", "Población\n Total", "Mun.Pob.Ind.", "Ámbito", "Población \n Indígena", "Tipo Localidad"});
            seccion4_4_2.add(tabMedioSocioeconomico);
            seccion4_4_2.add(new Paragraph("\n"));

            // 4.5.1
            Chunk sec4_5_1Title = new Chunk("Diagnóstico Ambiental", TITULO2);
            Section seccion4_5_1 = chapter4.addSection(new Paragraph(sec4_5_1Title));
            sec4_5_1Title.setLocalDestination(seccion4_5_1.getTitle().getContent());
            seccion4_5_1.setIndentationLeft(10);
            seccion4_5_1.add(new Paragraph("\n"));
            seccion4_5_1.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("DESC_DIAGAMB")) + "</p>"));
            seccion4_5_1.add(new Paragraph("\n"));
            
            // Muestra tabla de Adjuntos
            // CAPITULO_ID='4' AND SUBCAPITULO_ID='5' AND SECCION_ID = '0' AND APARTADO_ID = '0' ";
            PdfPTable tblArchsDiagnosticoAmb = generarTablaArchivos(folio, serial, url, (short) 4, (short) 5, (short) 0, (short) 0);
            seccion4_5_1.add(tblArchsDiagnosticoAmb);
            seccion4_5_1.add(new Paragraph("\n"));

            content.add(chapter4);
            chapterList.add(chapter4);
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Capitulo 5">
            // Capitulo 5
            Chunk chapTitle5 = new Chunk("Identificación, Descripción y Evaluación de los impactos ambientales.", TITULO1);
            Chapter chapter5 = new Chapter(new Paragraph(chapTitle5), 5);
            chapTitle5.setLocalDestination(chapter5.getTitle().getContent());

            // 5.1
            Chunk sec5_1Title = new Chunk("Metodología para Identificar y Evaluar los Impactos Ambientales", TITULO2);
            Section seccion5_1 = chapter5.addSection(new Paragraph(sec5_1Title));
            sec5_1Title.setLocalDestination(seccion5_1.getTitle().getContent());
            seccion5_1.setIndentationLeft(10);
            seccion5_1.add(new Paragraph("\n"));

            // 5.1.1
            Chunk sec5_1_1Title = new Chunk("Metodología para Identificar y Evaluar los Impactos Ambientales", TITULO3);
            Section seccion5_1_1 = seccion5_1.addSection(new Paragraph(sec5_1_1Title));
            sec5_1_1Title.setLocalDestination(seccion5_1_1.getTitle().getContent());
            seccion5_1_1.setIndentationLeft(10);
            seccion5_1_1.add(new Paragraph("\n"));
            seccion5_1_1.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("DESC_METUTIL")) + "</p>"));
            seccion5_1_1.add(new Paragraph("\n"));
            
            // Muestra tabla de Adjuntos
            PdfPTable tblArchsMetIdentEvImpAmb = generarTablaArchivos(folio, serial, url, (short) 5, (short) 1, (short) -1, (short) -1);
            seccion5_1_1.add(tblArchsMetIdentEvImpAmb);
            seccion5_1_1.add(new Paragraph("\n"));

            // 5.1.2
            Chunk sec5_1_2Title = new Chunk("Justificación para todos los formatos capturados", TITULO3);
            Section seccion5_1_2 = seccion5_1.addSection(new Paragraph(sec5_1_2Title));
            sec5_1_2Title.setLocalDestination(seccion5_1_2.getTitle().getContent());
            seccion5_1_2.setIndentationLeft(10);
            seccion5_1_2.add(new Paragraph("\n"));
            seccion5_1_2.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("PROY_JUSTMET_UTIL")) + "</p"));
            seccion5_1_2.add(new Paragraph("\n"));

            // Muestra tabla de Adjuntos para la última etapa encontrada
        	// AND CAPITULO_ID='5' AND SUBCAPITULO_ID='1' AND SECCION_ID = '0' AND APARTADO_ID = '0' ";
    		PdfPTable tblArchivosJustificacion = generarTablaArchivos(folio, serial, url, (short) 5, (short) 1, (short) 0, (short) 0);
    		seccion5_1_2.add(tblArchivosJustificacion);
    		seccion5_1_2.add(new Paragraph("\n"));
    		seccion5_1_2.add(new Paragraph("\n"));

            // 5.2
            Chunk sec5_2Title = new Chunk("Resultados de Evaluación de los Impactos Ambientales", TITULO2);
            Section seccion5_2 = chapter5.addSection(new Paragraph(sec5_2Title));
            sec5_2Title.setLocalDestination(seccion5_2.getTitle().getContent());
            seccion5_2.setIndentationLeft(10);
            seccion5_2.add(new Paragraph("\n"));
            String sql5_2 = "SELECT ce.etapa_descripcion A,c.impact_indent B,c.impact_Desc C,c.impac_Carac D,c.impac_Eva E,cti.tipo_impacto_descripcion F,c.impac_Indic G FROM Impac_Amb_Proyecto c, cat_etapa ce, cat_tipo_impacto cti WHERE c.etapa_id = ce.etapa_id and cti.tipo_impacto_id = c.impacto_Id and FOLIO_PROYECTO = '" + folio + "' and SERIAL_PROYECTO = " + serial + "";
            PdfPTable tabResEva = ReportUtil.buildTableVinculacion(con, sql5_2, new String[]{"A", "B", "F", "G", "C", "D", "E"}, new String[]{"Etapa", "Impacto identificado", "Tipo de impacto", "Indicador", "Descripción", "Caracterización", "Evaluación sobre la medida propuesta"});
            seccion5_2.add(tabResEva);
            seccion5_2.add(new Paragraph("\n"));
            
            // Muestra tabla de Adjuntos
            // CAPITULO_ID='5' AND SUBCAPITULO_ID='2' AND SECCION_ID='0'";
            PdfPTable tblArchsResEvalImpAmb = generarTablaArchivos(folio, serial, url, (short) 5, (short) 2, (short) 0, (short) -1);
            seccion5_2.add(tblArchsResEvalImpAmb);
            seccion5_2.add(new Paragraph("\n"));
            

            content.add(chapter5);
            chapterList.add(chapter5);
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Capitulo 6">
            // Capitulo 6
            Chunk chapTitle6 = new Chunk("Medidas Preventivas y de Mitigación de los Impactos Ambientales.", TITULO1);
            Chapter chapter6 = new Chapter(new Paragraph(chapTitle6), 6);
            chapTitle6.setLocalDestination(chapter6.getTitle().getContent());
            chapter6.add(new Paragraph("\n"));

            // 6.1
            Chunk sec6_1Title = new Chunk("Medidas Preventivas y de Mitigación de los Impactos Ambientales", TITULO2);
            Section seccion6_1 = chapter6.addSection(new Paragraph(sec6_1Title));
            sec6_1Title.setLocalDestination(seccion6_1.getTitle().getContent());
            seccion6_1.setIndentationLeft(10);
            seccion6_1.add(new Paragraph("\n"));
            String sql6_1 = "SELECT c.prev_Impactonom A,c.prev_Med_Prop B, ce.etapa_descripcion C,c.prec_Rec_Nece D,c.prev_Efic_Camb E FROM Med_Prev_Impact_Proy c, cat_etapa ce WHERE c.etapa_id = ce.etapa_id and FOLIO_PROYECTO = '" + folio + "' and SERIAL_PROYECTO = " + serial + " order by c.etapa_id ";
            PdfPTable tabMedPrev = ReportUtil.buildTableVinculacion(con, sql6_1, new String[]{"C", "A", "D", "E", "B"}, new String[]{"Etapa", "Impacto", "Recursos Necesarios", "Indicadores de eficiencia ambiental", "Medida(s) Propuesta(s)"});
            seccion6_1.add(tabMedPrev);
            seccion6_1.add(new Paragraph("\n"));
            
            // Muestra tabla de Adjuntos
            // CAPITULO_ID='6' AND SUBCAPITULO_ID='1' AND SECCION_ID='0'";
            PdfPTable tblArchsMedPrevMitImpAmb = generarTablaArchivos(folio, serial, url, (short) 6, (short) 1, (short) 0, (short) -1);
            seccion6_1.add(tblArchsMedPrevMitImpAmb);
            seccion6_1.add(new Paragraph("\n"));
            

            content.add(chapter6);
            chapterList.add(chapter6);
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Capitulo 7">
            // Capitulo 7
            Chunk chapTitle7 = new Chunk("Pronósticos Ambientales y en su caso, Evaluación de Alternativas.", TITULO1);
            Chapter chapter7 = new Chapter(new Paragraph(chapTitle7), 7);
            chapTitle7.setLocalDestination(chapter7.getTitle().getContent());

            // 7.1
            Chunk sec7_1Title = new Chunk("Pronósticos Ambientales y en su caso Evaluación de Alternativas", TITULO2);
            Section seccion7_1 = chapter7.addSection(new Paragraph(sec7_1Title));
            sec7_1Title.setLocalDestination(seccion7_1.getTitle().getContent());
            seccion7_1.setIndentationLeft(10);
            seccion7_1.add(new Paragraph("\n"));

            // 7.1.1
            Chunk sec7_1_1Title = new Chunk("Escenario sin proyecto", TITULO2);
            Section seccion7_1_1 = seccion7_1.addSection(new Paragraph(sec7_1_1Title));
            sec7_1_1Title.setLocalDestination(seccion7_1_1.getTitle().getContent());
            seccion7_1_1.setIndentationLeft(10);
            seccion7_1_1.add(new Paragraph("\n"));
            seccion7_1_1.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("ESCE_SIN_PROY")) + "</p>"));
            seccion7_1_1.add(new Paragraph("\n"));

            // 7.1.2
            Chunk sec7_1_2Title = new Chunk("Escenario con proyecto", TITULO2);
            Section seccion7_1_2 = seccion7_1.addSection(new Paragraph(sec7_1_2Title));
            sec7_1_2Title.setLocalDestination(seccion7_1_2.getTitle().getContent());
            seccion7_1_2.setIndentationLeft(10);
            seccion7_1_2.add(new Paragraph("\n"));
            seccion7_1_2.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("ESCE_CON_PROY")) + "</p>"));
            seccion7_1_2.add(new Paragraph("\n"));

            // 7.1.3
            Chunk sec7_1_3Title = new Chunk("Escenario con proyecto y medidas", TITULO2);
            Section seccion7_1_3 = seccion7_1.addSection(new Paragraph(sec7_1_3Title));
            sec7_1_3Title.setLocalDestination(seccion7_1_3.getTitle().getContent());
            seccion7_1_3.setIndentationLeft(10);
            seccion7_1_3.add(new Paragraph("\n"));
            seccion7_1_3.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("ESCE_CON_PROY_MED")) + "</p>"));
            seccion7_1_3.add(new Paragraph("\n"));

            // 7.1.4
            Chunk sec7_1_4Title = new Chunk("Pronóstico ambiental", TITULO2);
            Section seccion7_1_4 = seccion7_1.addSection(new Paragraph(sec7_1_4Title));
            sec7_1_4Title.setLocalDestination(seccion7_1_3.getTitle().getContent());
            seccion7_1_4.setIndentationLeft(10);
            seccion7_1_4.add(new Paragraph("\n"));
            seccion7_1_4.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("PRONOS_AMB")) + "</p>"));
            seccion7_1_4.add(new Paragraph("\n"));

            // 7.2.1
            Chunk sec7_2_1Title = new Chunk("Evaluación de alternativas, en su caso", TITULO2);
            Section seccion7_2_1 = chapter7.addSection(new Paragraph(sec7_2_1Title));
            sec7_2_1Title.setLocalDestination(seccion7_2_1.getTitle().getContent());
            seccion7_2_1.setIndentationLeft(10);
            seccion7_2_1.add(new Paragraph("\n"));
            seccion7_2_1.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("EVA_ALTERNATIVAS")) + "</p>"));
            seccion7_2_1.add(new Paragraph("\n"));

            // 7.2.2
            Chunk sec7_2_2Title = new Chunk("Conclusión", TITULO2);
            Section seccion7_2_2 = chapter7.addSection(new Paragraph(sec7_2_2Title));
            sec7_2_2Title.setLocalDestination(seccion7_2_2.getTitle().getContent());
            seccion7_2_2.setIndentationLeft(10);
            seccion7_2_2.add(new Paragraph("\n"));
            seccion7_2_2.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("CONCLUSION_PROY")) + "</p>"));
            seccion7_2_2.add(new Paragraph("\n"));

            content.add(chapter7);
            chapterList.add(chapter7);
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="Capitulo 8">
            // Capitulo 8
            Chunk chapTitle8 = new Chunk("Identificación de los Instrumentos Metodológicos y Elementos Técnicos que Sustentan la Información señalada en las Fracciones Anteriores.", TITULO1);
            Chapter chapter8 = new Chapter(new Paragraph(chapTitle8), 8);
            chapTitle8.setLocalDestination(chapter8.getTitle().getContent());

            // 8.1
            Chunk sec8_1Title = new Chunk("Otros anexos", TITULO2);
            Section seccion8_1 = chapter8.addSection(new Paragraph(sec8_1Title));
            sec8_1Title.setLocalDestination(seccion8_1.getTitle().getContent());
            seccion8_1.setIndentationLeft(10);
            seccion8_1.add(new Paragraph("\n"));
            
            // Muestra tabla de Adjuntos
            // CAPITULO_ID = 8 and SUBCAPITULO_ID = 1
            PdfPTable tblOtrosAnexos = generarTablaArchivos(folio, serial, url, (short) 8, (short) 1, (short) -1, (short) -1);
            seccion8_1.add(tblOtrosAnexos);
            seccion8_1.add(new Paragraph("\n"));

            // 8.2
            Chunk sec8_2Title = new Chunk("Fotografías", TITULO2);
            Section seccion8_2 = chapter8.addSection(new Paragraph(sec8_2Title));
            sec8_2Title.setLocalDestination(seccion8_2.getTitle().getContent());
            seccion8_2.setIndentationLeft(10);
            seccion8_2.add(new Paragraph("\n"));
            
            // Muestra tabla de Adjuntos
            // CAPITULO_ID = 8 and SUBCAPITULO_ID = 2
            PdfPTable tblArchsFotografias = generarTablaArchivos(folio, serial, url, (short) 8, (short) 2, (short) -1, (short) -1);
            seccion8_2.add(tblArchsFotografias);
            seccion8_2.add(new Paragraph("\n"));

            // 8.3
            Chunk sec8_3Title = new Chunk("Videos", TITULO2);
            Section seccion8_3 = chapter8.addSection(new Paragraph(sec8_3Title));
            sec8_3Title.setLocalDestination(seccion8_3.getTitle().getContent());
            seccion8_3.setIndentationLeft(10);
            seccion8_3.add(new Paragraph("\n"));
            
            // Muestra tabla de Adjuntos
            // CAPITULO_ID = 8 and SUBCAPITULO_ID = 3
            PdfPTable tblArchsVideos = generarTablaArchivos(folio, serial, url, (short) 8, (short) 3, (short) -1, (short) -1);
            seccion8_3.add(tblArchsVideos);
            seccion8_3.add(new Paragraph("\n"));

            /**
             * CONTENIDO: 8.4 "GLOSARIO"
             */
            Chunk sec8_4Title = new Chunk("Glosario de términos", TITULO2);
            Section seccion8_4 = chapter8.addSection(new Paragraph(sec8_4Title));
            sec8_4Title.setLocalDestination(seccion8_4.getTitle().getContent());
            seccion8_4.setIndentationLeft(10);
            seccion8_4.add(new Paragraph("\n"));
            // Muestra en un párrafo el contenido registrado para Glosario
            seccion8_4.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("PROY_GLOSARIO")) + "</p>"));
            seccion8_4.add(new Paragraph("\n"));

            // 8.5
            Chunk sec8_5Title = new Chunk("Bibliografía", TITULO2);
            Section seccion8_5 = chapter8.addSection(new Paragraph(sec8_5Title));
            sec8_5Title.setLocalDestination(seccion8_5.getTitle().getContent());
            seccion8_5.setIndentationLeft(10);
            seccion8_5.add(new Paragraph("\n"));
            seccion8_5.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("BIBLIO_PROY")) + "</p>"));
            seccion8_5.add(new Paragraph("\n"));

            // 8.6
            Chunk sec8_6Title = new Chunk("Resumen Manifestación de Impacto Ambiental", TITULO2);
            Section seccion8_6 = chapter8.addSection(new Paragraph(sec8_6Title));
            sec8_6Title.setLocalDestination(seccion8_6.getTitle().getContent());
            seccion8_6.setIndentationLeft(10);
            seccion8_6.add(new Paragraph("\n"));
            seccion8_6.add(ReportUtil.parseHtmlParagraph("<p style=\"text-align: justify;\">" + notNullFilter(rsProy.getString("MANIFESTACION_RES")) + "</p>"));
            seccion8_6.add(new Paragraph("\n"));

            content.add(chapter8);
            chapterList.add(chapter8);
            //</editor-fold>

            // Capitulo ER
            //<editor-fold defaultstate="collapsed" desc="Estudio de Riesgo">
            try {
                if (rsProy.getString("ESTUDIO_RIESGO_ID") != null && rsProy.getString("ESTUDIO_RIESGO_ID").compareTo("1") == 0) {
                    Chunk chapTitleER = new Chunk("Estudio de riesgo", TITULO1);
                    Chapter chapterER = new Chapter(new Paragraph(chapTitleER), 9);
                    chapTitleER.setLocalDestination(chapterER.getTitle().getContent());
                    
                    Statement stEr = con.createStatement();
                    ResultSet rsEr = stEr.executeQuery("SELECT * FROM Estudio_Riesgo_Proyecto e WHERE folio_Proyecto = '" + folio + "' and serial_Proyecto = " + serial);
                    
                    if (rsEr.next()) {
                        // E.1
                        Chunk secE_1Title = new Chunk("Datos del Responsable del Estudio de Riesgo", TITULO2);
                        Section seccionE_1 = chapterER.addSection(new Paragraph(secE_1Title));
                        secE_1Title.setLocalDestination(seccionE_1.getTitle().getContent());
                        seccionE_1.setIndentationLeft(10);
                        seccionE_1.add(new Paragraph("\n"));

                        seccionE_1.add(new Chunk("Nombre o razón social de la empresa responsable de elaborar el estudio de riesgo: ", TITULO4));
                        seccionE_1.add(new Chunk(notNullFilter(rsEr.getString("estudio_Nom_Resp_Ela").trim())));
                        seccionE_1.add(Chunk.NEWLINE);
                        seccionE_1.add(new Chunk("RFC: ", TITULO4));
                        seccionE_1.add(new Chunk(notNullFilter(rsEr.getString("estudio_Rfc_Resp_Ela"))));
                        seccionE_1.add(Chunk.NEWLINE);
                        seccionE_1.add(new Chunk("Nombre del representante legal: ", TITULO4));
                        seccionE_1.add(new Chunk(notNullFilter(rsEr.getString("estudio_Nom_Rpte_Legal").trim())));
                        seccionE_1.add(Chunk.NEWLINE);
                        seccionE_1.add(new Chunk("RFC del representante legal: ", TITULO4));
                        seccionE_1.add(new Chunk(notNullFilter(rsEr.getString("estudio_Rfc_Rpte"))));
                        seccionE_1.add(new Chunk("\n"));

                        // E.2
                        Chunk secE_2Title = new Chunk("Escenarios de los Riesgos Ambientales Relacionados con el Proyecto", TITULO2);
                        Section seccionE_2 = chapterER.addSection(new Paragraph(secE_2Title));
                        secE_2Title.setLocalDestination(seccionE_2.getTitle().getContent());
                        seccionE_2.setIndentationLeft(10);
                        seccionE_2.add(new Paragraph("\n"));

                        // E.2.1
                        Chunk secE_2_1Title = new Chunk("Bases de Diseño", TITULO2);
                        Section seccionE_2_1 = seccionE_2.addSection(new Paragraph(secE_2_1Title));
                        secE_2_1Title.setLocalDestination(seccionE_2_1.getTitle().getContent());
                        seccionE_2_1.setIndentationLeft(10);
                        seccionE_2_1.add(new Paragraph("\n"));
                        Paragraph pE2_1 = ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Desc_Bases_Dise")));
                        seccionE_2_1.add(new Paragraph(pE2_1));
                        seccionE_2_1.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                		// CAPITULO_ID='10' AND SUBCAPITULO_ID='2' AND SECCION_ID='1' AND APARTADO_ID='0'";
                        PdfPTable tblArchsBasesDiseno = generarTablaArchivos(folio, serial, url, (short) 10, (short) 2, (short) 1, (short) 0);
                        seccionE_2_1.add(tblArchsBasesDiseno);
                        seccionE_2_1.add(new Paragraph("\n"));

                        // E.2.2
                        Section seccionE_2_2 = ReportUtil.creaSeccion("Proyecto Civil", seccionE_2);
                        seccionE_2_2.add(new Paragraph("\n"));
                        seccionE_2_2.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Proyecto_Civil"))));
                        seccionE_2_2.add(new Paragraph("\n"));
                    	
                        // Muestra tabla de Adjuntos
                        // AND CAPITULO_ID='10' AND SUBCAPITULO_ID='2' AND SECCION_ID='1' AND APARTADO_ID='1'";
                        PdfPTable tblArchsProyCivil = generarTablaArchivos(folio, serial, url, (short) 10, (short) 2, (short) 1, (short) 1);
                        seccionE_2_2.add(tblArchsProyCivil);
                        seccionE_2_2.add(new Paragraph("\n"));

                        // E.2.3
                        Section seccionE_2_3 = ReportUtil.creaSeccion("Proyecto Mecánico", seccionE_2);
                        seccionE_2_3.add(new Paragraph("\n"));
                        seccionE_2_3.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Proyecto_Mecanico"))));
                        seccionE_2_3.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='2' AND SECCION_ID='1' AND APARTADO_ID='2'";
                        PdfPTable tblArchsProyMecanico = generarTablaArchivos(folio, serial, url, (short) 10, (short) 2, (short) 1, (short) 2);
                        seccionE_2_3.add(tblArchsProyMecanico);
                        seccionE_2_3.add(new Paragraph("\n"));

                        // E.2.4
                        Section seccionE_2_4 = ReportUtil.creaSeccion("Proyecto Sistema Contra Incendio", seccionE_2);
                        seccionE_2_4.add(new Paragraph("\n"));
                        seccionE_2_4.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("ESTUDIO_PROYECTO_CINCENDIO"))));
                        seccionE_2_4.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='2' AND SECCION_ID='1' AND APARTADO_ID='3'";
                        PdfPTable tblArchsSistContraIncendio = generarTablaArchivos(folio, serial, url, (short) 10, (short) 2, (short) 1, (short) 3);
                        seccionE_2_4.add(tblArchsSistContraIncendio);
                        seccionE_2_4.add(new Paragraph("\n"));

                        // E.2.5
                        Section seccionE_2_5 = ReportUtil.creaSeccion("Descripción Detallada del Proceso", seccionE_2);
                        seccionE_2_5.add(new Paragraph("\n"));
                        seccionE_2_5.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("ESTUDIO_DETALLADA_PROCESO"))));
                        seccionE_2_5.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='2' AND SECCION_ID='2' AND APARTADO_ID='0'";
                        PdfPTable tblArchsDescrDetProceso = generarTablaArchivos(folio, serial, url, (short) 10, (short) 2, (short) 2, (short) 0);
                        seccionE_2_5.add(tblArchsDescrDetProceso);
                        seccionE_2_5.add(new Paragraph("\n"));

                        // E.2.6
                        //Section seccionE_2_6 = ReportUtil.creaSeccion("Hojas de Seguridad", seccionE_2);
                        
                        //============================================================================================
                        Section seccionE_2_6 = ReportUtil.creaSeccion("Hojas de Seguridad", seccionE_2);
                        
                        PdfPTable hs = new PdfPTable(5);
                        hs.setWidthPercentage((float) 100);
                        float[] hsColumnWidths = new float[] {5f, 23f, 22f, 20f, 20f};
                        hs.setWidths(hsColumnWidths);
                        
                        //  "Vinculación", "Archivo"
                        PdfPCell titNume = new PdfPCell(new Phrase("#", TITULO3));
                        PdfPCell titEtapa = new PdfPCell(new Phrase("Etapa", TITULO3));
                        PdfPCell titTipoSus = new PdfPCell(new Phrase("Tipo de sustancia", TITULO3));
                        PdfPCell titCant = new PdfPCell(new Phrase("Cantidad almacenada", TITULO3));
                        PdfPCell titAdjunto = new PdfPCell(new Phrase("Adjunto", TITULO3));
                     // Primera fila: Titulos
                        titNume.setHorizontalAlignment(Element.ALIGN_CENTER);
                        hs.addCell(titNume);
                        hs.addCell(titEtapa);
                        hs.addCell(titTipoSus);
                        hs.addCell(titCant);
                        titAdjunto.setHorizontalAlignment(Element.ALIGN_CENTER);
                        hs.addCell(titAdjunto);
                        
                        String sqlHS = " WITH algo AS ( SELECT  ETAPA_DESCRIPCION AS ETAPA, SUSTANCIA_DESCRIPCION AS SUSTANCIA, SUSTANCIA_CANTIDAD_ALMACENADA || ' ' || CTUN_DESC AS CANTIDAD"
                        		+ " , AP.SEQ_ID AS ID, AP.FILENAME || '.' || AP.EXTENSION AS ARCHIVO"
                        		+ "   FROM ("
                        		+ "   SELECT B.ETAPA_ID, FOLIO_PROYECTO, SERIAL_PROYECTO, SUST_PROY_ID, ID_ARCHIVO_PROGRAMA, ETAPA_DESCRIPCION,"
                        		+ "   CASE A.SUSTANCIA_ID WHEN 9999 THEN SUSTANCIA_PROMOVENTE ELSE SUSTANCIA_DESCRIPCION END AS SUSTANCIA_DESCRIPCION,"
                        		+ "   SUSTANCIA_CANTIDAD_ALMACENADA,"
                        		+ "   (SELECT CTUN_DESC FROM CATALOGOS.CAT_UNIDAD_MEDIDA D WHERE C.SUSTANCIA_UNIDAD = D.CTUN_CLVE) AS CTUN_DESC"
                        		+ "   FROM SUSTANCIA_PROYECTO A, CAT_ETAPA B, CAT_SUSTANCIA_ALTAM_RIESGOSA C"
                        		+ "   WHERE A.ETAPA_ID = B.ETAPA_ID AND A.SUSTANCIA_ID = C.SUSTANCIA_ID"
                        		+ "   AND A.FOLIO_PROYECTO = '"+folio+"' AND A.SERIAL_PROYECTO ="+serial
                        		+ "   ) SUST LEFT JOIN ARCHIVOS_PROYECTO AP ON SUST.ID_ARCHIVO_PROGRAMA = AP.SEQ_ID"
                        		+ "   ORDER BY SUST.ETAPA_ID, SUST_PROY_ID ) select rownum as NUMERAL, a.* from algo a";
                        
                        ResultSet rsHS = st2.executeQuery(sqlHS);
                        
                        if (rsHS != null) {            	
                        	//MIENTRAS EXISTAN REGISTROS.
                            while (rsHS.next()) {
                            	
                            	PdfPCell valNum = new PdfPCell(new Phrase("" + notNullFilter(rsHS.getString("NUMERAL"))));
                                PdfPCell valEtapa = new PdfPCell(new Phrase("" + notNullFilter(rsHS.getString("ETAPA"))));
                                PdfPCell valTipoSus = new PdfPCell(new Phrase("" + notNullFilter(rsHS.getString("SUSTANCIA"))));
                                PdfPCell valCant = new PdfPCell(new Phrase("" + notNullFilter(rsHS.getString("CANTIDAD"))));
                                PdfPCell ligaArchivo = new PdfPCell(new Phrase("Sin adjunto", PARRAFO));
                                
                                ligaArchivo.setHorizontalAlignment(Element.ALIGN_CENTER);
                                String id = rsHS.getString("ID");
                                if (null != id){
                                    String filename = rsHS.getString("ARCHIVO");
                                    if (null != filename) {
                                    	ligaArchivo = generarCeldaArchivo(filename, url, id.trim());
                                    }
                            	}                               
                                // Segunda fila: Valores
                                valNum.setHorizontalAlignment(Element.ALIGN_CENTER);
                                hs.addCell(valNum);
                                hs.addCell(valEtapa);
                                hs.addCell(valTipoSus);
                                hs.addCell(valCant);
                                hs.addCell(ligaArchivo);
                            }
                            
                            rsHS.close();
                            
                            
                            seccionE_2_6.add(new Paragraph("\n"));               
                            seccionE_2_6.add(hs);
                            seccionE_2_6.add(new Paragraph("\n"));	
                        }
                        // Cierre de TABLA: HOJA DE SEGURIDAD"
                        System.out.println("\nDespués de tabla: HOJA DE SEGURIDAD.\n");
                        ////==========================================================================================================================================

                        // E.2.7
                        Section seccionE_2_7 = ReportUtil.creaSeccion("Almacenamiento", seccionE_2);
                        seccionE_2_7.add(new Paragraph("\n"));
                        seccionE_2_7.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Almacenamiento"))));
                        seccionE_2_7.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='2' AND SECCION_ID='2' AND APARTADO_ID='2'";
                        PdfPTable tblArchsUbFisProy = generarTablaArchivos(folio, serial, url, (short) 10, (short) 2, (short) 2, (short) 2);
                        seccionE_2_7.add(tblArchsUbFisProy);
                        seccionE_2_7.add(new Paragraph("\n"));

                        // E.2.8
                        Section seccionE_2_8 = ReportUtil.creaSeccion("Equipos de Proceso y Auxiliares", seccionE_2);
                        seccionE_2_8.add(new Paragraph("\n"));
                        seccionE_2_8.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Proceso_Auxiliar"))));
                        seccionE_2_8.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='2' AND SECCION_ID='2' AND APARTADO_ID='3'";
                        PdfPTable tblArchsEqProcesoAux = generarTablaArchivos(folio, serial, url, (short) 10, (short) 2, (short) 2, (short) 3);
                        seccionE_2_8.add(tblArchsEqProcesoAux);
                        seccionE_2_8.add(new Paragraph("\n"));
                        
                        // E.2.9
                        Section seccionE_2_9 = ReportUtil.creaSeccion("Pruebas de verificación", seccionE_2);
                        seccionE_2_9.add(new Paragraph("\n"));
                        seccionE_2_9.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Pruebas_Verificacion"))));
                        seccionE_2_9.add(new Paragraph("\n"));

                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='2' AND SECCION_ID='2' AND APARTADO_ID='4'";
                        PdfPTable tblArchsPruebasVerif = generarTablaArchivos(folio, serial, url, (short) 10, (short) 2, (short) 2, (short) 4);
                        seccionE_2_9.add(tblArchsPruebasVerif);
                        seccionE_2_9.add(new Paragraph("\n"));

                        // E.2.10
                        Section seccionE_2_10 = ReportUtil.creaSeccion("Condiciones de Operación", seccionE_2);
                        seccionE_2_10.add(new Paragraph("\n"));
                        seccionE_2_10.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Operacion"))));
                        seccionE_2_10.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='2' AND SECCION_ID='3' AND APARTADO_ID='0'";
                        PdfPTable tblArchsCondicionesOper = generarTablaArchivos(folio, serial, url, (short) 10, (short) 2, (short) 3, (short) 0);
                        seccionE_2_10.add(tblArchsCondicionesOper);
                        seccionE_2_10.add(new Paragraph("\n"));

                        // E.2.11
                        Section seccionE_2_11 = ReportUtil.creaSeccion("Especificación del Cuarto de Control", seccionE_2);
                        seccionE_2_11.add(new Paragraph("\n"));
                        seccionE_2_11.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Cuarto_Control"))));
                        seccionE_2_11.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='2' AND SECCION_ID='3' AND APARTADO_ID='1'";
                        PdfPTable tblArchsEspecCuartoControl = generarTablaArchivos(folio, serial, url, (short) 10, (short) 2, (short) 3, (short) 1);
                        seccionE_2_11.add(tblArchsEspecCuartoControl);
                        seccionE_2_11.add(new Paragraph("\n"));

                        // E.2.12
                        Section seccionE_2_12 = ReportUtil.creaSeccion("Sistema de Aislamiento", seccionE_2);
                        seccionE_2_12.add(new Paragraph("\n"));
                        seccionE_2_12.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Sistema_Aislamiento"))));
                        seccionE_2_12.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='2' AND SECCION_ID='3' AND APARTADO_ID='2'";
                        PdfPTable tblArchsSistAislam = generarTablaArchivos(folio, serial, url, (short) 10, (short) 2, (short) 3, (short) 2);
                        seccionE_2_12.add(tblArchsSistAislam);
                        seccionE_2_12.add(new Paragraph("\n"));

                        // E.2.13
                        Section seccionE_2_13 = ReportUtil.creaSeccion("Análisis y Evaluación de Riesgos", seccionE_2);

                        // E.2.13.1
                        Section seccionE_2_13_1 = ReportUtil.creaSeccion("Antecedentes de Accidentes e Incidentes", seccionE_2_13);
                        seccionE_2_13_1.add(new Paragraph("\n"));
                        seccionE_2_13_1.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Antecedentes_Acci_Inci"))));
                        seccionE_2_13_1.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='2' AND SECCION_ID='4' AND APARTADO_ID='1'";
                        PdfPTable tblArchsAntecAcciInci = generarTablaArchivos(folio, serial, url, (short) 10, (short) 2, (short) 4, (short) 1);
                        seccionE_2_13_1.add(tblArchsAntecAcciInci);
                        seccionE_2_13_1.add(new Paragraph("\n"));

                        // E.2.13.2
                        Section seccionE_2_13_2 = ReportUtil.creaSeccion("Metodologías de Identificación y Jerarquización de los Riesgos", seccionE_2_13);
                        seccionE_2_13_2.add(new Paragraph("\n"));
                        seccionE_2_13_2.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Int_Jerar_Riesgo"))));
                        seccionE_2_13_2.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='2' AND SECCION_ID='4' AND APARTADO_ID='2'";
                        PdfPTable tblArchsMetIdentJerRiesgos = generarTablaArchivos(folio, serial, url, (short) 10, (short) 2, (short) 4, (short) 2);
                        seccionE_2_13_2.add(tblArchsMetIdentJerRiesgos);
                        seccionE_2_13_2.add(new Paragraph("\n"));

                        content.add(chapterER);
                        chapterList.add(chapterER);

                        // E.3
                        Section seccionE_3 = ReportUtil.creaSeccion("Descripción de las Medidas de Protección en Torno de las instalaciones", chapterER);

                        // E.3.1
                        Section seccionE_3_1 = ReportUtil.creaSeccion("Radios Potenciales de Afectación", seccionE_3);
                        seccionE_3_1.add(new Paragraph("\n"));
                        seccionE_3_1.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Radios_Potenciales"))));
                        seccionE_3_1.add(new Paragraph("\n"));

                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='3' AND SECCION_ID='1' AND APARTADO_ID='0'";
                        PdfPTable tblArchsRadPotAfect = generarTablaArchivos(folio, serial, url, (short) 10, (short) 3, (short) 1, (short) 0);
                        seccionE_3_1.add(tblArchsRadPotAfect);
                        seccionE_3_1.add(new Paragraph("\n"));

                        // E.3.2
                        Section seccionE_3_2 = ReportUtil.creaSeccion("Interacciones de Riesgo", seccionE_3);
                        seccionE_3_2.add(new Paragraph("\n"));
                        seccionE_3_2.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Interacciones_Riesgo"))));
                        seccionE_3_2.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                        // AND CAPITULO_ID='10' AND SUBCAPITULO_ID='3' AND SECCION_ID='2' AND APARTADO_ID='0'";
                        PdfPTable tblArchsInteracRiesgo = generarTablaArchivos(folio, serial, url, (short) 10, (short) 3, (short) 2, (short) 0);
                        seccionE_3_2.add(tblArchsInteracRiesgo);
                        seccionE_3_2.add(new Paragraph("\n"));

                        // E.3.3
                        Section seccionE_3_3 = ReportUtil.creaSeccion("Efectos Sobre el Área de Influencia", seccionE_3);
                        seccionE_3_3.add(new Paragraph("\n"));
                        seccionE_3_3.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Efectos_Area_Influ"))));
                        seccionE_3_3.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='3' AND SECCION_ID='3' AND APARTADO_ID='0'";
                        PdfPTable tblArchsEfectosAreaInf = generarTablaArchivos(folio, serial, url, (short) 10, (short) 3, (short) 3, (short) 0);
                        seccionE_3_3.add(tblArchsEfectosAreaInf);
                        seccionE_3_3.add(new Paragraph("\n"));

                        // E.4
                        Section seccionE_4 = ReportUtil.creaSeccion("Señalamiento de las Medidas de Seguridad y Preventivas en Materia Ambiental", chapterER);

                        // E.4.1
                        Section seccionE_4_1 = ReportUtil.creaSeccion("Recomendaciones Técnico - Operativas", seccionE_4);
                        seccionE_4_1.add(new Paragraph("\n"));
                        seccionE_4_1.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Recomendaciones_Tec_Op"))));
                        seccionE_4_1.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='4' AND SECCION_ID='1' AND APARTADO_ID='0'";
                        PdfPTable tblArchsRecomTecOper = generarTablaArchivos(folio, serial, url, (short) 10, (short) 4, (short) 1, (short) 0);
                        seccionE_4_1.add(tblArchsRecomTecOper);
                        seccionE_4_1.add(new Paragraph("\n"));

                        // E.4.2
                        Section seccionE_4_2 = ReportUtil.creaSeccion("Sistemas de Seguridad", seccionE_4);
                        seccionE_4_2.add(new Paragraph("\n"));
                        seccionE_4_2.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Sistemas_Seguridad"))));
                        seccionE_4_2.add(new Paragraph("\n"));

                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='4' AND SECCION_ID='1' AND APARTADO_ID='1'";
                        PdfPTable tblArchsSistSeguridad = generarTablaArchivos(folio, serial, url, (short) 10, (short) 4, (short) 1, (short) 1);
                        seccionE_4_2.add(tblArchsSistSeguridad);
                        seccionE_4_2.add(new Paragraph("\n"));

                        // E.4.3
                        Section seccionE_4_3 = ReportUtil.creaSeccion("Medidas Preventivas", seccionE_4);
                        seccionE_4_3.add(new Paragraph("\n"));
                        seccionE_4_3.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Medidas_Preventivas"))));
                        seccionE_4_3.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='4' AND SECCION_ID='1' AND APARTADO_ID='2'";
                        PdfPTable tblArchsMedidasPrev = generarTablaArchivos(folio, serial, url, (short) 10, (short) 4, (short) 1, (short) 2);
                        seccionE_4_3.add(tblArchsMedidasPrev);
                        seccionE_4_3.add(new Paragraph("\n"));

                        // E.5
                        Section seccionE_5 = ReportUtil.creaSeccion("Resumen", chapterER);

                        // E.5.1
                        Section seccionE_5_1 = ReportUtil.creaSeccion("Conclusiones del Estudio de Riesgo Ambiental", seccionE_5);
                        seccionE_5_1.add(new Paragraph("\n"));
                        seccionE_5_1.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Senala_Conclusion"))));
                        seccionE_5_1.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='5' AND SECCION_ID='1' AND APARTADO_ID='0'";
                        PdfPTable tblArchsConclusSistAmb = generarTablaArchivos(folio, serial, url, (short) 10, (short) 5, (short) 1, (short) 0);
                        seccionE_5_1.add(tblArchsConclusSistAmb);
                        seccionE_5_1.add(new Paragraph("\n"));
                        
                        // E.5.2
                        Section seccionE_5_2 = ReportUtil.creaSeccion("Resumen de la Situación General que Presenta el proyecto en Materia de Riesgo Ambiental", seccionE_5);
                        seccionE_5_2.add(new Paragraph("\n"));
                        seccionE_5_2.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Resumen_Situacion"))));
                        seccionE_5_2.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='5' AND SECCION_ID='2' AND APARTADO_ID='0'";
                        PdfPTable tblArchsResSitGralMRA = generarTablaArchivos(folio, serial, url, (short) 10, (short) 5, (short) 2, (short) 0);
                        seccionE_5_2.add(tblArchsResSitGralMRA);
                        seccionE_5_2.add(new Paragraph("\n"));

                        // E.5.3
                        Section seccionE_5_3 = ReportUtil.creaSeccion("Informe Técnico", seccionE_5);
                        seccionE_5_3.add(new Paragraph("\n"));
                        seccionE_5_3.add(ReportUtil.parseHtmlParagraph(notNullFilter(rsEr.getString("estudio_Informe_Tecnico"))));
                        seccionE_5_3.add(new Paragraph("\n"));
                        
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID='10' AND SUBCAPITULO_ID='5' AND SECCION_ID='3' AND APARTADO_ID='0'";
                        PdfPTable tblArchsInformeTecnico = generarTablaArchivos(folio, serial, url, (short) 10, (short) 5, (short) 3, (short) 0);
                        seccionE_5_3.add(tblArchsInformeTecnico);
                        seccionE_5_3.add(new Paragraph("\n"));

                        // E.6
                        Section seccionE_6 = ReportUtil.creaSeccion("Instrumentos Metodológicos y Elementos Técnicos", chapterER);

                        // E.6.1
                        Section seccionE_6_1 = ReportUtil.creaSeccion("Planos de Localización y fotografías", seccionE_6);
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID = 10 and SUBCAPITULO_ID = 6 AND SECCION_ID = 1
                        PdfPTable tblArchsPlanosLocFot = generarTablaArchivos(folio, serial, url, (short) 10, (short) 6, (short) 1, (short) -1);
                        seccionE_6_1.add(tblArchsPlanosLocFot);
                        seccionE_6_1.add(new Paragraph("\n"));

                        // E.6.2
                        Section seccionE_6_2 = ReportUtil.creaSeccion("Planos de Localización y fotografías", seccionE_6);
                        // Muestra tabla de Adjuntos
                        // CAPITULO_ID = 10 and SUBCAPITULO_ID = 6 AND SECCION_ID = 2
                        PdfPTable tblArchsIMETOtrosAnexos = generarTablaArchivos(folio, serial, url, (short) 10, (short) 6, (short) 2, (short) -1);
                        seccionE_6_2.add(tblArchsIMETOtrosAnexos);
                        seccionE_6_2.add(new Paragraph("\n"));

                    } else {
                        Chunk secERTitle = new Chunk("Estudio de riesgo.", TITULO2);
                        Section seccionER = chapterER.addSection(new Paragraph(secERTitle));
                        seccionER.setIndentationLeft(10);
                        seccionER.add(new Paragraph("\n"));

                        String strER = "<p style=\"text-align: justify;\">No existe información capturada</p>";
                        Paragraph pER = ReportUtil.parseHtmlParagraph(strER);
                        seccionER.add(pER);
                        seccionER.add(new Paragraph("\n"));
                        
                        content.add(chapterER);
                        chapterList.add(chapterER);
                    }
                }
            } catch (NullPointerException nu) {
                nu.printStackTrace();
            }
            //</editor-fold>
            content.close();

            String[] datos = {"" + notNullFilter(rsProy.getString("PROY_NOMBRE")), ssector, ssubSector, srama};
            Document document = new Document(PageSize.A4, 50, 50, 70, 50);
            PdfWriter writer = PdfWriter.getInstance(document, ouBs);
            IndexEvent indexEvent = new IndexEvent(datos);
            writer.setPageEvent(indexEvent);

            document.open();

            Chapter indexChapter = new Chapter("Indice", -1);
            indexChapter.setNumberDepth(-1);

            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage((float) 100);
            table.setWidths(new float[]{90f, 10f});

            for (Map.Entry<String, Integer> index : event.index.entrySet()) {
                if (index.getKey().indexOf("Capitulo") > 1) {
                    PdfPCell tmp1 = new PdfPCell(new Phrase("\n"));
                    PdfPCell tmp2 = new PdfPCell(new Phrase("\n"));
                    tmp1.setBorder(Rectangle.NO_BORDER);
                    tmp2.setBorder(Rectangle.NO_BORDER);
                    table.addCell(tmp1);
                    table.addCell(tmp2);
                }

                PdfPCell left = new PdfPCell(new Phrase(index.getKey()));
                left.setBorder(Rectangle.NO_BORDER);

                Chunk pageno = new Chunk(index.getValue() + "");
                pageno.setLocalGoto(index.getKey());

                PdfPCell right = new PdfPCell(new Phrase(pageno));
                right.setHorizontalAlignment(Element.ALIGN_RIGHT);
                right.setBorder(Rectangle.NO_BORDER);
                table.addCell(left);
                table.addCell(right);
            }
            indexChapter.add(table);
            document.add(indexChapter);

            // add content chapter
            for (Chapter c : chapterList) {
                document.add(c);
                indexEvent.body = true;
            }
            document.close();

            //<editor-fold defaultstate="collapsed" desc="Carátula">
            // Caratula
            Document caratula = new Document(PageSize.A4, 50, 50, 70, 50);
            PdfWriter caratulaWriter = PdfWriter.getInstance(caratula, cBs);
            caratula.open();

            Image img = Image.getInstance(ReportFactory.class.getResource("/mx/gob/semarnat/mia/report/logoSEMARNAT_hoz.png"));
            img.scalePercent(40f);
            img.setAbsolutePosition(10, 780);

            //Imagen
            caratulaWriter.getDirectContent().addImage(img);

            //Trámite
            Paragraph CaratulaTramite = new Paragraph();
            CaratulaTramite.setAlignment(Element.ALIGN_CENTER);
            Chunk TituloTramite = new Chunk("TRÁMITE:", TITULO_CARATULA);
            Chunk TituloTramite2 = new Chunk(nombreTramite, TITULO_CARATULA2);

            CaratulaTramite.add(TituloTramite);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(TituloTramite2);

            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);

            //Proyecto
            Chunk TituloProyecto = new Chunk("PROYECTO: ", TITULO_CARATULA);
            Chunk TituloProyecto2 = new Chunk("" + notNullFilter(rsProy.getString("PROY_NOMBRE")), TITULO_CARATULA2);

            CaratulaTramite.add(TituloProyecto);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(TituloProyecto2);

            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);

            //Clave del Proyecto y Número de Bitácora
            if (serial >= 3) {
                Chunk cClaveProyecto = new Chunk("CLAVE: ", TITULO_CARATULA);
                Chunk cClaveProyecto2 = new Chunk(ClaveProyecto, TITULO_CARATULA2);

                CaratulaTramite.add(cClaveProyecto);
                CaratulaTramite.add(cClaveProyecto2);

                CaratulaTramite.add("       ");

                Chunk cBitacoraProyecto = new Chunk("BITÁCORA: ", TITULO_CARATULA);
                Chunk cBitacoraProyecto2 = new Chunk(Bitacora, TITULO_CARATULA2);

                CaratulaTramite.add(cBitacoraProyecto);
                CaratulaTramite.add(cBitacoraProyecto2);

                CaratulaTramite.add(Chunk.NEWLINE);
                CaratulaTramite.add(Chunk.NEWLINE);
                CaratulaTramite.add(Chunk.NEWLINE);
                CaratulaTramite.add(Chunk.NEWLINE);
            }

            //Promovente            
            Chunk cPromovente = new Chunk("PROMOVENTE: ", TITULO_CARATULA);
            Chunk cPromovente2 = new Chunk(nombrePromovente, TITULO_CARATULA2);

            CaratulaTramite.add(cPromovente);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(cPromovente2);

            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);

            //Clave del Sector y Subsector
            Chunk cSectorProyecto = new Chunk("SECTOR: ", TITULO_CARATULA);
            Chunk cSectorProyecto2 = new Chunk(ssector, TITULO_CARATULA2);

            CaratulaTramite.add(cSectorProyecto);
            CaratulaTramite.add(cSectorProyecto2);

            CaratulaTramite.add("         ");

            Chunk cSubsectorProyecto = new Chunk("SUBSECTOR: ", TITULO_CARATULA);
            Chunk cSubsectorProyecto2 = new Chunk(ssubSector, TITULO_CARATULA2);

            CaratulaTramite.add(cSubsectorProyecto);
            CaratulaTramite.add(cSubsectorProyecto2);

            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);

            //Rama y tipo
            Chunk cRamaProyecto = new Chunk("RAMA: ", TITULO_CARATULA);
            Chunk cramaProyecto2 = new Chunk(srama, TITULO_CARATULA2);

            CaratulaTramite.add(cRamaProyecto);
            CaratulaTramite.add(cramaProyecto2);

            CaratulaTramite.add("         ");

            Chunk cTipoProyecto = new Chunk("TIPO: ", TITULO_CARATULA);
            Chunk cTipoProyecto2 = new Chunk(sTipoProyecto, TITULO_CARATULA2);

            CaratulaTramite.add(cTipoProyecto);
            CaratulaTramite.add(cTipoProyecto2);

            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);

            //Ubicación
            Chunk cUbicacionProyecto = new Chunk("UBICACIÓN: ", TITULO_CARATULA);
            CaratulaTramite.add(Chunk.NEWLINE);
            CaratulaTramite.add(Chunk.NEWLINE);

            CaratulaTramite.add(cUbicacionProyecto);

            caratula.add(CaratulaTramite);

            caratula.add(Chunk.NEWLINE);

            caratula.add(tableUniFis);

            Paragraph CaratulaTramite2 = new Paragraph();

            CaratulaTramite2.add(Chunk.NEWLINE);
            CaratulaTramite2.add(Chunk.NEWLINE);

            //Fecha de ingreso
            Chunk cFechaIngreso = new Chunk("Fecha de ingreso en SEMARNAT: ", TITULO_CARATULA);
            Chunk cFechaIngreso2 = new Chunk(FechaRegistro, TITULO_CARATULA2);

            CaratulaTramite2.add(Chunk.NEWLINE);
            CaratulaTramite2.add(Chunk.NEWLINE);

            CaratulaTramite2.add(cFechaIngreso);
            CaratulaTramite2.add(Chunk.NEWLINE);
            CaratulaTramite2.add(Chunk.NEWLINE);
            CaratulaTramite2.add(cFechaIngreso2);

            CaratulaTramite2.setAlignment(Element.ALIGN_CENTER);

            caratula.add(CaratulaTramite2);

            caratula.close();
            //</editor-fold>
        }

        rsProy.close();
        st.close();
        con.close();

        List<InputStream> pdfs = new LinkedList<InputStream>();
        pdfs.add(new ByteArrayInputStream(cBs.toByteArray()));
        pdfs.add(new ByteArrayInputStream(ouBs.toByteArray()));

        ByteArrayOutputStream cOut = new ByteArrayOutputStream();
        ReportUtil.juntaPdf(pdfs, cOut);

        return cOut.toByteArray();
    }
    
    /**
     * Genera Celda con hipervínculo para un archivo
     */
    public PdfPCell generarCeldaArchivo(String nombreArchivo, String url, String id) {
        PdfPCell ligaArchivo = new PdfPCell();
        String urlCompleta = url.concat("faces/modarchivos/visor.xhtml?idt=").concat(id).concat("&tipo=general");
        
        if (null != nombreArchivo) {
        	Chunk chk = new Chunk(nombreArchivo, PARRAFO_SUB);
            chk.setAnchor(urlCompleta);
            Paragraph p = new Paragraph();
            p.add(chk);
            p.setAlignment(Element.ALIGN_CENTER);
            ligaArchivo.addElement(p);
        }
        
        return ligaArchivo;
    }
    
    /**
     * Genera Tabla de Archivos
     */
    public PdfPTable generarTablaArchivos(String folio, Short serial, String url, Short capitulo, Short subcapitulo, Short seccion, Short apartado)
    		throws DocumentException, IOException, ClassNotFoundException, SQLException {
    	
    	int total = 0;
    	ResultSet rsArchivos;
    	
        PdfPTable tablaArchivos = new PdfPTable(4);
        tablaArchivos.setWidthPercentage((float) 100);
        float[] tblColumnWidths = new float[] {5f, 25f, 35f, 35};
        tablaArchivos.setWidths(tblColumnWidths);
        
        // Fila con Títulos
        PdfPCell titArchivoId = new PdfPCell(new Phrase("#", TITULO3));
        PdfPCell titNombre = new PdfPCell(new Phrase("Nombre", TITULO3));
        PdfPCell titDescripcion = new PdfPCell(new Phrase("Descripción", TITULO3));
        PdfPCell titLigaArchivo = new PdfPCell(new Phrase("Anexo", TITULO3));
        PdfPCell titSinRegistros = new PdfPCell(new Phrase("Sin registros.", TITULO4));
        
        titArchivoId.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablaArchivos.addCell(titArchivoId);
        titNombre.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablaArchivos.addCell(titNombre);
        titDescripcion.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablaArchivos.addCell(titDescripcion);
        titLigaArchivo.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablaArchivos.addCell(titLigaArchivo);
        
        try
        {
        	Connection con = getCon();
        	Statement stArchivos = con.createStatement();
        	
        	String qryTotalRegistros = "SELECT COUNT(*) TOTAL FROM ARCHIVOS_PROYECTO"
        			+ " WHERE FOLIO_SERIAL = '" + folio + "' AND SERIAL_PROYECTO = " + serial;
        	
        	String qryArchivos = "WITH REGS_SECUENCIALES AS ("
        			+ "SELECT SEQ_ID, NOMBRE, DESCRIPCION, FILENAME"
        			+ " FROM ARCHIVOS_PROYECTO"
        			+ " WHERE FOLIO_SERIAL = '" + folio + "' AND SERIAL_PROYECTO = " + serial;
        	
        	String qryCapituloSubcapitulo = " AND CAPITULO_ID = " + capitulo + " AND SUBCAPITULO_ID = " + subcapitulo;;
        	
        	if (null != capitulo && null != subcapitulo) {
        		qryTotalRegistros = qryTotalRegistros + " " + qryCapituloSubcapitulo;
        		qryArchivos = qryArchivos + " " + qryCapituloSubcapitulo;
        		
        		if (null != seccion && seccion != -1) {
            		qryTotalRegistros = qryTotalRegistros + " AND SECCION_ID = " + seccion;
            		qryArchivos = qryArchivos + " AND SECCION_ID = " + seccion;
            	}
            	
            	if (null != apartado && apartado != -1) {
            		qryTotalRegistros = qryTotalRegistros + " AND APARTADO_ID = " + apartado;
            		qryArchivos = qryArchivos + " AND APARTADO_ID = " + apartado;
            	}
        	}
        	
        	rsArchivos = stArchivos.executeQuery(qryTotalRegistros);
        	if (rsArchivos.next()) {
        		total = rsArchivos.getInt("TOTAL");
        	}
        	rsArchivos.close();
        	
        	if (total > 0) {
            	qryArchivos = qryArchivos + " ORDER BY ID) SELECT ROWNUM AS SECUENCIAL, rs.* FROM REGS_SECUENCIALES rs";
            	
            	rsArchivos = stArchivos.executeQuery(qryArchivos);
            	
            	while (rsArchivos.next()){
            		PdfPCell valArchivoId = new PdfPCell(new Phrase("" + notNullFilter(rsArchivos.getString("SECUENCIAL"))));
                    PdfPCell valNombrePromov = new PdfPCell(new Phrase("" + notNullFilter(rsArchivos.getString("NOMBRE"))));
                    PdfPCell valDescrPromov = new PdfPCell(new Phrase("" + notNullFilter(rsArchivos.getString("DESCRIPCION"))));
                    PdfPCell ligaArchivo = new PdfPCell();
                    
                    String vAnexo = rsArchivos.getString("FILENAME");
                    String urlCompleta = url.concat("faces/modarchivos/visor.xhtml?idt=");
                    
                    if (null != vAnexo) {
                    	Chunk chk = new Chunk(vAnexo, PARRAFO_SUB);

                    	urlCompleta = urlCompleta.concat(rsArchivos.getString("SEQ_ID").concat("&tipo=general"));
                    	chk.setAnchor(urlCompleta);
                    	
                        Paragraph p = new Paragraph();
                        p.add(chk);
                        p.setAlignment(Element.ALIGN_CENTER);
                        ligaArchivo.addElement(p);                    
                    }
                    else {
                    	vAnexo = "Sin adjunto";
                    }
                    
                    // Filas con valores
                    valArchivoId.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tablaArchivos.addCell(valArchivoId);
                    tablaArchivos.addCell(valNombrePromov);
                    tablaArchivos.addCell(valDescrPromov);
                    ligaArchivo.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tablaArchivos.addCell(ligaArchivo);
            	}
            	rsArchivos.close();
            	stArchivos.close();
        	}
        	else {
        		// Crear tabla con etiqueta: "Sin registros"
        		titSinRegistros.setColspan(4);
        		tablaArchivos.addCell(titSinRegistros);
        	}
        	
        }
        catch(SQLException sqlE){
        	sqlE.printStackTrace();
        }
        
        return tablaArchivos;
    }
    
    public PdfPTable generarTablaInversionEtapas(Connection con, String sql) throws SQLException, DocumentException {
        Statement st = con.createStatement();
        ResultSet rsInversion = st.executeQuery(sql);
        
        //====================================== TABLA DE INVERSIÓN Y EMPLEOS POR ETAPAS =========================================
        
        PdfPTable tabla = new PdfPTable(5);
        tabla.setWidthPercentage((float) 100);
        
        float[] tablaColumnWidths = new float[] {27f, 24f, 17f, 18f, 14f};
        tabla.setWidths(tablaColumnWidths);
        
        if (rsInversion.next()){
            
            // Primera fila: Títulos
            tabla.addCell(new PdfPCell(new Phrase("Etapas", TITULO3)));
            tabla.addCell(new PdfPCell(new Phrase("Inversión", TITULO3)));
            tabla.addCell(new PdfPCell(new Phrase("Empleos Temporales", TITULO3)));
            tabla.addCell(new PdfPCell(new Phrase("Empleos Permanentes", TITULO3)));
            tabla.addCell(new PdfPCell(new Phrase("Total Empleos", TITULO3)));
            
            
            // Segunda fila: Preparación del Sitio
            Paragraph pInversionPS = new Paragraph(new Chunk("$ " + notNullFilter(rsInversion.getString("INVERSION_PREPARACION")), TITULO3));
            pInversionPS.setAlignment(Element.ALIGN_RIGHT);
            
            tabla.addCell(new PdfPCell(new Phrase("Preparación del Sitio", TITULO3)));
            tabla.addCell(new PdfPCell(pInversionPS));
            tabla.addCell(new PdfPCell(new Phrase(rsInversion.getString("EMPLEOS_TEMPORALES_PREP"), TITULO3)));
            tabla.addCell(new PdfPCell(new Phrase(rsInversion.getString("EMPLEOS_PERMANENTES_PREP"), TITULO3)));
            tabla.addCell(new PdfPCell(new Phrase("" + notNullFilter(rsInversion.getString("TOTAL_EMPLEOS_PREP")), TITULO3)));
            
            
            // Tercera fila: Construcción
            Paragraph pInversionC = new Paragraph(new Chunk("$ " + notNullFilter(rsInversion.getString("INVERSION_CONSTRUCCION")), TITULO3));
            pInversionC.setAlignment(Element.ALIGN_RIGHT);
            
            tabla.addCell(new PdfPCell(new Phrase("Construcción", TITULO3)));
            tabla.addCell(new PdfPCell(pInversionC));
            tabla.addCell(new PdfPCell(new Phrase(rsInversion.getString("EMPLEOS_TEMPORALES_CONS"), TITULO3)));
            tabla.addCell(new PdfPCell(new Phrase(rsInversion.getString("EMPLEOS_PERMANENTES_CONS"), TITULO3)));
            tabla.addCell(new PdfPCell(new Phrase("" + notNullFilter(rsInversion.getString("TOTAL_EMPLEOS_CONS")), TITULO3)));
            
            
            // Cuarta fila: Operación y Mantenimiento
            Paragraph pInversionOM = new Paragraph(new Chunk("$ " + notNullFilter(rsInversion.getString("INVERSION_OPERACION")), TITULO3));
            pInversionOM.setAlignment(Element.ALIGN_RIGHT);
            
            tabla.addCell(new PdfPCell(new Phrase("Operación y Mantenimiento", TITULO3)));
            tabla.addCell(new PdfPCell(pInversionOM));
            tabla.addCell(new PdfPCell(new Phrase(rsInversion.getString("EMPLEOS_TEMPORALES_OPER"), TITULO3)));
            tabla.addCell(new PdfPCell(new Phrase(rsInversion.getString("EMPLEOS_PERMANENTES_OPER"), TITULO3)));
            tabla.addCell(new PdfPCell(new Phrase("" + notNullFilter(rsInversion.getString("TOTAL_EMPLEOS_OPER")), TITULO3)));
            
            
            // Quinta fila: Abandono
            Paragraph pInversionA = new Paragraph(new Chunk("$ " + notNullFilter(rsInversion.getString("INVERSION_ABANDONO")), TITULO3));
            pInversionA.setAlignment(Element.ALIGN_RIGHT);
            
            tabla.addCell(new PdfPCell(new Phrase("Abandono", TITULO3)));
            tabla.addCell(new PdfPCell(pInversionA));
            tabla.addCell(new PdfPCell(new Phrase(rsInversion.getString("EMPLEOS_TEMPORALES_ABAN"), TITULO3)));
            tabla.addCell(new PdfPCell(new Phrase(rsInversion.getString("EMPLEOS_PERMANENTES_ABAN"), TITULO3)));
            tabla.addCell(new PdfPCell(new Phrase("" + notNullFilter(rsInversion.getString("TOTAL_EMPLEOS_ABAN")), TITULO3)));
            
            
            // Sexta fila: SubTotal Inversión
            Paragraph pSubtotalInversion = new Paragraph(new Chunk("$ " + rsInversion.getString("SUBTOTAL_INVERSION"), TITULO3));
            PdfPCell celfil6_col3 = new PdfPCell();
            celfil6_col3.setColspan(3);
            
            tabla.addCell(new PdfPCell(new Phrase("SubTotal Inversión", TITULO3)));
            tabla.addCell(pSubtotalInversion);
            tabla.addCell(celfil6_col3);
            
            
            // Séptima fila: Total de Empleos
            String totalEmpleosTemp = rsInversion.getString("TOTAL_EMPLEOS_TEMP");
            Integer iTotalEmpleosTemp = Integer.parseInt(rsInversion.getString("TOTAL_EMPLEOS_TEMP"));
            String totalEmpleosPerm = rsInversion.getString("TOTAL_EMPLEOS_PERM");
            Integer iTotalEmpleosPerm = Integer.parseInt(rsInversion.getString("TOTAL_EMPLEOS_PERM"));
            Integer iTotalEmpleos = iTotalEmpleosTemp + iTotalEmpleosPerm;
            PdfPCell celfil7_col2 = new PdfPCell();
            
            tabla.addCell(new PdfPCell(new Phrase("Total de Empleos", TITULO3)));
            tabla.addCell(celfil7_col2);
            tabla.addCell(new PdfPCell(new Phrase(totalEmpleosTemp, TITULO3)));
            tabla.addCell(new PdfPCell(new Phrase(totalEmpleosPerm, TITULO3)));
            tabla.addCell(new PdfPCell(new Phrase("" + iTotalEmpleos, TITULO3)));
            
            
            // Octava fila: Medidas de Mitigación
            String strMedMitigacion = rsInversion.getString("MEDIDAS_MITIGACION");
            Paragraph pMedidasMitigacion = new Paragraph();
            
            PdfPCell celfil8_col3 = new PdfPCell();
            celfil8_col3.setColspan(3);
            
            if (null != strMedMitigacion) {
            	pMedidasMitigacion = new Paragraph(new Chunk("$ " + strMedMitigacion, TITULO3));
            }
            tabla.addCell(new PdfPCell(new Phrase("Medidas de Mitigación", TITULO3)));
            tabla.addCell(pMedidasMitigacion);
            tabla.addCell(celfil8_col3);
            
            
            // Novena fila: Total de la Inversión
            Paragraph pTotalInversion = new Paragraph(new Chunk("$ " + rsInversion.getString("TOTAL_INVERSION"), TITULO3));
            PdfPCell celfil9_col3 = new PdfPCell();
            celfil9_col3.setColspan(3);
            
            tabla.addCell(new PdfPCell(new Phrase("Total de la Inversión", TITULO3)));
            tabla.addCell(pTotalInversion);
            tabla.addCell(celfil9_col3);
        }

        rsInversion.close();
        st.close();
        return tabla;
    }

    public static void main(String[] args) throws Exception {
        ReportFactory r = new ReportFactory();
//       byte[] pdf =  r.pdf("3881", (short) 1);
//        byte[] pdf = r.pdf("2336", (short) 1);
        byte[] pdf = r.pdf("4931", (short) 1, "url");

        FileOutputStream fos = new FileOutputStream("C:\\Users\\mauricio\\Documents\\NetBeansProjects\\test1" + new java.util.Date().getTime() + ".pdf");
        fos.write(pdf);
        fos.close();

//        r.pdf("3823", (short) 1);
    }

    private String notNullFilter(String cadena) {
        if (cadena == null || cadena.compareToIgnoreCase("null") == 0) {
            return "";
        } else {
            return cadena;
        }
    }
}