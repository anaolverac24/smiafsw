/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.report;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Map;
import static mx.gob.semarnat.mia.report.ReportFactory.TITULO2;

/**
 *
 * @author mauricio
 */
public class ReportUtil {

    public static final Font TITULO4 = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK);
    public static final Font PARRAFO_SUB = new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE, BaseColor.BLACK);

    public static Section creaSeccion(String titulo, Section parent) {
        Chunk secTitle = new Chunk(titulo, TITULO2);
        Section seccion = parent.addSection(new Paragraph(secTitle));
        secTitle.setLocalDestination(seccion.getTitle().getContent());
        seccion.setIndentationLeft(10);
        seccion.add(new Paragraph("\n"));

        return seccion;
    }

    public static PdfPTable buildTable(Connection con, String sql, String[] col, String[] tit, Map<String, Object> opciones) throws SQLException, DocumentException {
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);

        PdfPTable table = new PdfPTable(col.length);
        table.setWidthPercentage((float) 100);
        //table.setKeepTogether(true);
//        table.setHeaderRows(1);

        if (opciones != null && !opciones.isEmpty() && opciones.containsKey("widths")) {
            float[] f = (float[]) opciones.get("widths");
            table.setWidths(f);
        }

        for (String s : tit) {
            PdfPCell cell = new PdfPCell(new Phrase(s, TITULO4));
            table.addCell(cell);
        }

        while (rs.next()) {
            for (String s : col) {
                PdfPCell cell = new PdfPCell(new Phrase(rs.getString(s)));
                cell.setHorizontalAlignment(Paragraph.ALIGN_JUSTIFIED);
                table.addCell(cell);
            }
        }

        rs.close();
        st.close();
        return table;
    }

    public static PdfPTable buildTable(Connection con, String sql, String[] col, String[] tit) throws SQLException {
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);

        PdfPTable table = new PdfPTable(col.length);
        table.setWidthPercentage((float) 100);
        //table.setKeepTogether(true);
//        table.setHeaderRows(1);
        for (String s : tit) {
            PdfPCell cell = new PdfPCell(new Phrase(s, TITULO4));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
        }

        String vAnexo = "";

        while (rs.next()) {
            for (String s : col) {
                vAnexo = "Sin adjunto";
                if (s != null && s.compareTo("ANEXO_NOMBRE") == 0) {
                    if (rs.getString("ANEXO_NOMBRE") != null) {
                        vAnexo = rs.getString("ANEXO_NOMBRE");
                    }
                    Chunk chk = new Chunk(vAnexo, PARRAFO_SUB);
                    chk.setAnchor(rs.getString("URL").concat("faces/modarchivos/visor.xhtml?idt=".
                            concat(rs.getString("SUSTANCIA_ANEXO_ID") + "").concat("&tipo=sustancia")));
                    PdfPCell cell = new PdfPCell();
                    Paragraph p = new Paragraph();
                    p.add(chk);
                    p.setAlignment(Element.ALIGN_CENTER);
                    cell.addElement(p);
                    table.addCell(cell);
                } else if (s != null && s.compareTo("ANEXO_ESTUDIO") == 0) {

                    if (rs.getString("ANEXO_ESTUDIO") != null) {
                        vAnexo = rs.getString("ANEXO_ESTUDIO");
                    }

                    Chunk chk = new Chunk(vAnexo, PARRAFO_SUB);
                    String ruta = rs.getString("URL");
                    ruta += "faces/modarchivos/visor.xhtml?idt=";
                    ruta += rs.getString("ESTUDIO_ID");
                    ruta += "&folio=";
                    ruta += rs.getString("FOLIO_SERIAL");
                    ruta += "&serial=";
                    ruta += rs.getString("SERIAL_PROYECTO");
                    ruta += "&tipo=estudio";
                    chk.setAnchor(ruta);
//                    chk.setAnchor(rs.getString("URL").concat("faces/modarchivos/visor.xhtml?idt=".
//                            concat(rs.getString("ESTUDIO_ID") + "").concat("&tipo=estudio")));
                    PdfPCell cell = new PdfPCell();
                    Paragraph p = new Paragraph();
                    p.add(chk);
                    p.setAlignment(Element.ALIGN_CENTER);
                    cell.addElement(p);
                    table.addCell(cell);
                } else {
                    PdfPCell cell = new PdfPCell(new Phrase(rs.getString(s)));
                    cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    table.addCell(cell);
                }
            }
        }

        rs.close();
        st.close();
        return table;
    }

    public static PdfPTable buildTableVinculacion(Connection con, String sql, String[] col, String[] tit) throws SQLException {
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);

        String TituloVinculacion = "";
        String TituloInstrumento = "";
        String TituloDescripcion = "";
        String TituloCaracterizacion = "";
        String EvaluacionSobreMedida = "";
        String TituloMedidaPropuesta = "";
        Integer vColumnas = 0;

        for (String s : tit) {
            if (s.toUpperCase().contains("VINCULACIÓN")) {
                TituloVinculacion = s;
                vColumnas += 1;
            } else if (s.toUpperCase().contains("INSTRUMENTO")) {
                TituloInstrumento = s;
                vColumnas += 1;
            } else if (s.toUpperCase().contains("NOMBRE DEL PLAN O PROGRAMA")) {
                TituloInstrumento = s;
                vColumnas += 1;
            } else if (s.toUpperCase().contains("DIAGNÓSTICO")) {
                TituloInstrumento = s;
                vColumnas += 1;
            } else if (s.toUpperCase().contains("TIPO LOCALIDAD")) {
                TituloInstrumento = s;
                vColumnas += 1;
            } else if (s.toUpperCase().contains("DESCRIPCIÓN")) {
                TituloDescripcion = s;
                vColumnas += 1;
            } else if (s.toUpperCase().contains("CARACTERIZACIÓN")) {
                TituloCaracterizacion = s;
                vColumnas += 1;
            } else if (s.toUpperCase().contains("EVALUACIÓN SOBRE LA MEDIDA PROPUESTA")) {
                EvaluacionSobreMedida = s;
                vColumnas += 1;
            } else if (s.toUpperCase().contains("MEDIDA(S) PROPUESTA(S)")) {
                TituloMedidaPropuesta = s;
                vColumnas += 1;
            }
        }

        PdfPTable table = new PdfPTable(col.length - vColumnas);
        table.setWidthPercentage((float) 100);

        for (String s : tit) {
            PdfPCell cellTit = new PdfPCell(new Phrase(s, TITULO4));
            if (!s.toUpperCase().contains("VINCULACIÓN") && !s.toUpperCase().contains("INSTRUMENTO")
                    && !s.toUpperCase().contains("NOMBRE DEL PLAN O PROGRAMA") && !s.toUpperCase().contains("DIAGNÓSTICO")
                    && !s.toUpperCase().contains("TIPO LOCALIDAD") && !s.toUpperCase().contains("DESCRIPCIÓN")
                    && !s.toUpperCase().contains("CARACTERIZACIÓN") && !s.toUpperCase().contains("EVALUACIÓN SOBRE LA MEDIDA PROPUESTA")
                    && !s.toUpperCase().contains("MEDIDA(S) PROPUESTA(S)")) {
                table.addCell(cellTit);
            }
        }

        int vContador = 0;
        while (rs.next()) {
            vContador = 0;
            for (String s : col) {
                if (tit[vContador].toUpperCase().contains("VINCULACIÓN")) {
                    table.addCell(new PdfPCell(new Phrase(TituloVinculacion, TITULO4)));
                    PdfPCell cell = new PdfPCell(new Phrase(rs.getString(s)));
                    cell.setColspan(vContador - 1);
                    cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    table.addCell(cell);
                } else if (tit[vContador].toUpperCase().contains("INSTRUMENTO")) {
                    table.addCell(new PdfPCell(new Phrase(TituloInstrumento, TITULO4)));
                    PdfPCell cell = new PdfPCell(new Phrase(rs.getString(s)));
                    cell.setColspan(vContador - 1);
                    cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    table.addCell(cell);
                } else if (tit[vContador].toUpperCase().contains("NOMBRE DEL PLAN O PROGRAMA")) {
                    table.addCell(new PdfPCell(new Phrase(TituloInstrumento, TITULO4)));
                    PdfPCell cell = new PdfPCell(new Phrase(rs.getString(s)));
                    cell.setColspan(vContador - 1);
                    cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    table.addCell(cell);
                } else if (tit[vContador].toUpperCase().contains("DIAGNÓSTICO")) {
                    table.addCell(new PdfPCell(new Phrase(TituloInstrumento, TITULO4)));
                    PdfPCell cell = new PdfPCell(new Phrase(rs.getString(s)));
                    cell.setColspan(vContador - 1);
                    cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    table.addCell(cell);
                } else if (tit[vContador].toUpperCase().contains("TIPO LOCALIDAD")) {
                    table.addCell(new PdfPCell(new Phrase(TituloInstrumento, TITULO4)));
                    PdfPCell cell = new PdfPCell(new Phrase(rs.getString(s)));
                    cell.setColspan(vContador - 1);
                    cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    table.addCell(cell);
                } else if (tit[vContador].toUpperCase().contains("DESCRIPCIÓN")) {
                    table.addCell(new PdfPCell(new Phrase(TituloDescripcion, TITULO4)));
                    PdfPCell cell = new PdfPCell(new Phrase(rs.getString(s)));
                    cell.setColspan(vContador - 1);
                    cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    table.addCell(cell);
                } else if (tit[vContador].toUpperCase().contains("CARACTERIZACIÓN")) {
                    table.addCell(new PdfPCell(new Phrase(TituloCaracterizacion, TITULO4)));
                    PdfPCell cell = new PdfPCell(new Phrase(rs.getString(s)));
                    cell.setColspan(vContador - 1);
                    cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    table.addCell(cell);
                } else if (tit[vContador].toUpperCase().contains("EVALUACIÓN SOBRE LA MEDIDA PROPUESTA")) {
                    table.addCell(new PdfPCell(new Phrase(EvaluacionSobreMedida, TITULO4)));
                    PdfPCell cell = new PdfPCell(new Phrase(rs.getString(s)));
                    cell.setColspan(vContador - 1);
                    cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    table.addCell(cell);
                } else if (tit[vContador].toUpperCase().contains("MEDIDA(S) PROPUESTA(S)")) {
                    table.addCell(new PdfPCell(new Phrase(TituloMedidaPropuesta, TITULO4)));
                    PdfPCell cell = new PdfPCell(new Phrase(rs.getString(s)));
                    cell.setColspan(vContador - 1);
                    cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    table.addCell(cell);
                } else {
                    PdfPCell cell = new PdfPCell(new Phrase(rs.getString(s)));
                    cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
                    table.addCell(cell);
                }
                vContador += 1;
            }
        }

        rs.close();
        st.close();
        return table;
    }

    public static PdfPTable buildTableAnexoGeneral(Connection con, String sql, String[] col, String[] tit, String url) throws SQLException, BadElementException {
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);

        PdfPTable table = new PdfPTable(col.length - 2);
        //table.setWidthPercentage((float) 100);
        //table.setKeepTogether(true);

        try {
            Image ImgAdjuntos = Image.getInstance(ReportFactory.class.getResource("/mx/gob/semarnat/mia/report/logos/Adjuntos.png"));
            ImgAdjuntos.scalePercent(7f);

            table.addCell(new PdfPCell(new Phrase("")));

            for (String s : tit) {
                PdfPCell cell = new PdfPCell(new Phrase(s, TITULO4));
                table.addCell(cell);
            }

            while (rs.next()) {
                //table.addCell(CellAdjunto);
                Chunk chk = new Chunk(rs.getString(col[1]), PARRAFO_SUB);
                chk.setAnchor(url.concat("faces/modarchivos/visor.xhtml?idt=".
                        concat(rs.getString(col[0]) + "").concat("&tipo=" + col[2])));
                PdfPCell cell = new PdfPCell();
                Paragraph p = new Paragraph();
                p.add(ImgAdjuntos);
                p.add(" ");
                p.add(chk);
                p.setAlignment(Element.ALIGN_CENTER);
                cell.addElement(p);
                table.addCell(new Paragraph(rs.getString("ANEXO_DESCRIPCIÓN")));
                table.addCell(cell);
            }
        } catch (Exception ep) {
            System.out.println("No se encuentra el archivo para los anexos");
            ep.printStackTrace();
        }
        rs.close();
        st.close();
        return table;
    }

    public static PdfPTable buildUrlTable(String url, Connection con, String sql, String[] col, String[] tit) throws SQLException {
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        String urlAnexo;

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage((float) 100);
        //table.setKeepTogether(true);
//        table.setHeaderRows(1);
        for (String s : tit) {
            PdfPCell cell = new PdfPCell(new Phrase(s, TITULO4));
            table.addCell(cell);
        }

        while (rs.next()) {
            Anchor liga = new Anchor(rs.getString(col[0]));
            urlAnexo = url.concat("faces/modarchivos/visor.xhtml?idt=").concat(rs.getString(col[1]) + "").concat("&tipo=" + col[3]);
            //liga.setReference(rs.getString(2));
            liga.setReference(urlAnexo);
            Phrase p = new Phrase();
            p.add(liga);

            PdfPCell cell = new PdfPCell(p);
            PdfPCell descripcion = new PdfPCell(new Phrase(rs.getString(col[2])));
            
            table.addCell(descripcion);
            table.addCell(cell);
            
        }

        rs.close();
        st.close();
        return table;
    }

    public static Paragraph parseHtmlParagraph(String html) {
        if (html == null || html.isEmpty()) {
            return new Paragraph("");
        }

        Paragraph pa = new Paragraph();
        try {
            List<Element> tl = HTMLWorker.parseToList(new StringReader(html), null);
            for (Element e : tl) {
                pa.add(e);
            }
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
        return pa;
    }

    public static void juntaPdf(List<InputStream> list, OutputStream outputStream)
            throws DocumentException, IOException {
        Document document = new Document();
        PdfCopy copy = new PdfCopy(document, outputStream);
        document.open();
        int n;
        for (InputStream in : list) {
            System.out.println("in");
            PdfReader reader = new PdfReader(in);

//            for (int i = 1; i <= reader.getNumberOfPages(); i++) {
            n = reader.getNumberOfPages();
            // loop over the pages in that document
            for (int page = 0; page < n;) {
//                System.out.println("pagina " + page);
                copy.addPage(copy.getImportedPage(reader, ++page));
            }
            copy.freeReader(reader);
            reader.close();
//            }
        }
        outputStream.flush();
        document.close();
        outputStream.close();
    }
    
    public static boolean muestraApartado(String tipoTramite, short sector, String seccion) {
        boolean bRes = true;
        
        if ( seccion.compareTo("2-1-1-1") == 0) {//Justificacion
            bRes = false;
        }
        
        if ( tipoTramite != null ) {
            if (tipoTramite.compareToIgnoreCase("MG") == 0 || tipoTramite.compareToIgnoreCase("DL") == 0) {
                if ( seccion.compareTo("2-1-1-1") == 0) {//Justificacion
                    bRes = true;
                }
                if ( seccion.compareTo("2-2-4-0") == 0) {//Información Biotecnológica de las especies a cultivar
                    bRes = false;
                } 
                if ( seccion.compareTo("2-2-3-0") == 0) {//Descripción de Obras Principales del Proyecto
                    bRes = false;
                } 
                if ( seccion.compareTo("2-1-8-0") == 0) {//Posibles Accidentes Ambientales
                    bRes = false;
                } 
                if ( seccion.compareTo("2-2-5-0|2-2-6-0") == 0 ) {//Insumos
                    bRes = true;
                }

            } else {
                if ( seccion.compareTo("2-2-5-0|2-2-6-0") == 0 ) {//Insumos
                    bRes = false;
                }            
            }

            //if (tipoTramite.compareToIgnoreCase("MP") == 0 || tipoTramite.compareToIgnoreCase("DM") == 0) {
                if ( seccion.compareTo("2-2-3-0") == 0) {//Descripción de Obras Principales del Proyecto
                    bRes = false;
                }
                if ( seccion.compareTo("2-2-4-0") == 0) {//Información Biotecnológica de las especies a cultivar
                    bRes = false;
                } 
                if ( seccion.compareTo("2-2-6-0|2-2-7-0") == 0 ) {//Utilización Explosivos
                    bRes = false;
                }            
                if ( seccion.compareTo("2-1-8-0") == 0) {//Posibles Accidentes Ambientales
                    bRes = false;
                } 

                switch (sector) {
                    case 6:
                        if ( seccion.compareTo("2-2-3-0") == 0) {//Descripción de Obras Principales del Proyecto
                            bRes = true;
                        }
                        break;
                    case 7:
                        break;
                    case 4:
                        if ( seccion.compareTo("2-2-4-0") == 0) {//Información Biotecnológica de las especies a cultivar
                            bRes = true;
                        } 
                        break;
                    case 3:
                        if ( seccion.compareTo("2-1-8-0") == 0) {//Posibles Accidentes Ambientales
                            bRes = true;
                        } 
                }

                if (sector == 6 || sector == 7 ) {
                    if ( seccion.compareTo("2-2-5-0|2-2-6-0") == 0 ) {//Insumos
                        bRes = true;
                    }            
                }
                if (sector == 1 || sector == 3 || sector == 5 || sector == 7 || sector == 8 || sector == 9 || sector == 10 || sector == 11 || sector == 12) {
                    if ( seccion.compareTo("2-2-6-0|2-2-7-0") == 0 ) {//Utilización Explosivos
                        bRes = true;
                    }            
                }
            //}
        }
        return bRes;
    }
    
    public static String porcentajeInversion(int tipo, Integer federal, Integer estatal, Integer municipal, Integer privada) {
        NumberFormat df = new DecimalFormat("#00.00");    
        
        double f = federal != null ? federal.doubleValue() : 0.0;
        double e = estatal != null ? estatal.doubleValue() : 0.0;
        double m = municipal != null ? municipal.doubleValue() : 0.0;
        double p = privada != null ? privada.doubleValue() : 0.0;
        
        double t = e + f + m + p;
        double out;
        
        switch(tipo) {
            case 1: //Federal
               out = ((double)(f / t) * 100); 
               break;
            case 2: //Estatal
               out = ((e / t) * 100); 
               break;
            case 3: //Mpal
               out = ((m / t) * 100); 
               break;
            case 4:
               out = ((p / t) * 100); 
               break;
            default:
                out = 0.0;
        }
        return ("" + df.format(out));
    }
}