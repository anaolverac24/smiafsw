/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.report;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.awt.Color;
import mx.gob.semarnat.mia.model.AnexosProyecto;
import mx.gob.semarnat.mia.model.sinatec.Vexdatosusuario;
import mx.gob.semarnat.mia.util.DBConnectionFactory;
import mx.gob.semarnat.mia.view.SIGEIAView;

/**
 *
 * @author Humberto
 */
public class ReportePagoDerechos {
    public static final Font TITULO1 = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.BLACK);
    public static final Font TITULO2 = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.BLACK);
    public static final Font TITULO3 = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.BLACK);
    public static final Font TITULO4 = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK);
    public static final Font TITULO_CARATULA = new Font(Font.FontFamily.HELVETICA, 20, Font.BOLD);
    public static final Font TITULO_CARATULA2 = new Font(Font.FontFamily.COURIER, 14, Font.NORMAL);

    public static final Font PARRAFO = new Font(Font.FontFamily.HELVETICA, 8);
    public static final Font PARRAFO_TITULO = new Font(Font.FontFamily.HELVETICA, 12);
    public static final Font PARRAFO_NEGRITA = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);
    public static final Font PARRAFO_SUB = new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE, BaseColor.BLACK);
    
    /**
     * EventListner for Content
     */
    private static class ContentEvent extends PdfPageEventHelper {

        private int page;
        Map<String, Integer> index = new LinkedHashMap<String, Integer>();

        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            page++;
        }

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            try {
                System.out.println("end");
                Image img = Image.getInstance(this.getClass().getResource("logoSEMARNAT_hoz.png"));
                img.setAbsolutePosition(400, 780);
                //writer.getDirectContent().addImage(img);
            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onChapter(PdfWriter writer, Document document,
                float paragraphPosition, Paragraph title
        ) {
            index.put(title.getContent(), page);
        }

        @Override
        public void onSection(PdfWriter writer, Document document,
                float paragraphPosition, int depth, Paragraph title
        ) {
            onChapter(writer, document, paragraphPosition, title);
        }
    }
    
    /**
     * EventListner for Index
     */
    private static class IndexEvent extends PdfPageEventHelper {

        private final String[] datos;
        private int page;
        private boolean body;
        protected PdfPTable header;                

        public IndexEvent(String[] datos) {
            this.datos = datos;
        }

        @Override
        public void onEndPage(PdfWriter writer, Document document) {            
            // set page number on content
            if (body) {
                page++;
                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER,
                        new Phrase(page + ""), (document.right() + document.left()) / 2, document.bottom() - 18, 0);
            }
        }
    }
    
    public Connection getCon() throws ClassNotFoundException, SQLException {
        return DBConnectionFactory.getConnection();
    }
    
    public byte[] pdf(String folio, Short serial, String url) throws DocumentException, IOException, ClassNotFoundException, SQLException {
        ByteArrayOutputStream ouBs = new ByteArrayOutputStream();
        ByteArrayOutputStream cBs = new ByteArrayOutputStream();                

        System.out.println("Folio |" + folio + "|");

        Connection con = getCon();
        Statement st = con.createStatement();
        ResultSet rsProy = st.executeQuery("SELECT VP.ID_CRITERIO AS ID_CRITERIO, CD.CRITERIO_DESCRIPCION AS CRITERIO_DESCRIPCION, CV.DESCRIPCION_VALOR AS DESCRIPCION_VALOR, VP.CRITERIO_VALOR_USUARIO AS CRITERIO_VALOR_USUARIO " +
                                            "FROM DGIRA_MIAE2.CRITERIO_VALORES_PROYECTO VP " +
                                            "INNER JOIN  DGIRA_MIAE2.CAT_DERECHOS_CRITERIOS CD ON VP.ID_CRITERIO = CD.CRITERIO_ID " +
                                            "INNER JOIN DGIRA_MIAE2.CAT_DERECHOS_CRITERIOSVALORES CV ON VP.ID_CRITERIO = CV.ID_CRITERIO AND VP.ID_VALOR = CV.VALOR " +
                                            "WHERE FOLIO_PROYECTO = '" + folio + "' AND SERIAL_PROYECTO = " + serial +" ORDER BY VP.ID_CRITERIO");         
        Document Documento = new Document(PageSize.A4, 50, 50, 50, 50);
        Documento.setMargins(48, 48, 100, 70);
        PdfWriter contentWriter = PdfWriter.getInstance(Documento, ouBs);
        ContentEvent event = new ContentEvent();
        contentWriter.setPageEvent(event);

        Documento.open();

        try {
                Image img = Image.getInstance(ReportFactory.class.getResource("/mx/gob/semarnat/mia/report/logoSEMARNAT_hoz.png"));
                img.scalePercent(40f);
                img.setAbsolutePosition(20, 760);
                String t1 = "Subsecretaria de Gestión para la Protección Ambiental";
                String t2 = "Dirección General de Impacto y Riesgo Ambiental";
                String t3 = "Cálculo de Pago de Derechos";


                contentWriter.getDirectContent().addImage(img);                                
                contentWriter.getDirectContent().beginText();
                contentWriter.getDirectContent().setFontAndSize(BaseFont.createFont(), 12);
                contentWriter.getDirectContent().showTextAligned(0, t1, 220, 810, 0);
                contentWriter.getDirectContent().showTextAligned(0, t2, 235, 790, 0);
                contentWriter.getDirectContent().showTextAligned(0, t3, 285, 770, 0);
                contentWriter.getDirectContent().setFontAndSize(BaseFont.createFont(), 9);                                

                contentWriter.getDirectContent().endText();
            } catch (Exception e) {
                System.out.println("Error en ruta de imagen");
                e.printStackTrace();
            }
        
        //Criterios del Promovente
        PdfPTable tableCriterios = new PdfPTable(4);
        tableCriterios.setWidthPercentage((float) 100);
        
        Paragraph tIdCriterio   = new Paragraph(new Chunk("Id Criterio", TITULO4));
        Paragraph tCriterio     = new Paragraph(new Chunk("Criterio", TITULO4));
        Paragraph tRespuesta    = new Paragraph(new Chunk("Respuesta", TITULO4));
        Paragraph tValor        = new Paragraph(new Chunk("Valor", TITULO4));
        Paragraph tTotalValores = new Paragraph(new Chunk("Total Valores", TITULO4));

        tIdCriterio.setAlignment(Element.ALIGN_CENTER);
        tCriterio.setAlignment(Element.ALIGN_CENTER);
        tRespuesta.setAlignment(Element.ALIGN_CENTER);
        tValor.setAlignment(Element.ALIGN_CENTER);

        PdfPCell uh1 = new PdfPCell(tIdCriterio);
        PdfPCell uh2 = new PdfPCell(tCriterio);
        PdfPCell uh3 = new PdfPCell(tRespuesta);
        PdfPCell uh4 = new PdfPCell(tValor);
        
        PdfPCell tVal = new PdfPCell(tTotalValores);

        uh1.setHorizontalAlignment(Element.ALIGN_CENTER);
        uh2.setHorizontalAlignment(Element.ALIGN_CENTER);
        uh3.setHorizontalAlignment(Element.ALIGN_CENTER);
        uh4.setHorizontalAlignment(Element.ALIGN_CENTER);
        
        tVal.setHorizontalAlignment(Element.ALIGN_RIGHT);
        
        tableCriterios.addCell(uh1);
        tableCriterios.addCell(uh2);
        tableCriterios.addCell(uh3);
        tableCriterios.addCell(uh4);
        
        float[] columnWidths = new float[] {13f, 61f, 13f, 13f};
        tableCriterios.setWidths(columnWidths);

        
        int vTotal = 0;
        while (rsProy.next()) {
            PdfPCell dIdCriterio  = new PdfPCell(new Paragraph(new Chunk(rsProy.getString("ID_CRITERIO"),PARRAFO)));
            PdfPCell dDescripcion = new PdfPCell(new Paragraph(new Chunk(rsProy.getString("CRITERIO_DESCRIPCION"),PARRAFO)));
            PdfPCell dRespuesta   = new PdfPCell(new Paragraph(new Chunk(rsProy.getString("DESCRIPCION_VALOR"),PARRAFO)));
            PdfPCell dValor       = new PdfPCell(new Paragraph(new Chunk(rsProy.getString("CRITERIO_VALOR_USUARIO"),PARRAFO)));
            
            dIdCriterio.setHorizontalAlignment(Element.ALIGN_CENTER);
            dDescripcion.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            dRespuesta.setHorizontalAlignment(Element.ALIGN_CENTER);
            dValor.setHorizontalAlignment(Element.ALIGN_CENTER);
            
            tableCriterios.addCell(dIdCriterio);
            tableCriterios.addCell(dDescripcion);
            tableCriterios.addCell(dRespuesta);
            tableCriterios.addCell(dValor);
            vTotal += Integer.parseInt(rsProy.getString("CRITERIO_VALOR_USUARIO"));            
        }                               
        
        tVal.setColspan(3);
        tableCriterios.addCell(tVal);
        
        PdfPCell tTotalValor  = new PdfPCell(new Paragraph(String.valueOf(vTotal)));
        tTotalValor.setHorizontalAlignment(Element.ALIGN_CENTER);
        tableCriterios.addCell(tTotalValor);
                
        Paragraph TituloValores = new Paragraph(new Chunk("Valores del promovente", TITULO2));
        TituloValores.setAlignment(Element.ALIGN_CENTER);
        Documento.add(TituloValores);
        Documento.add(new Paragraph(Chunk.NEWLINE));
        
        Documento.add(tableCriterios);
        
        rsProy.close();
        
        //Tabla de valores criterios
        ResultSet rsValores = st.executeQuery("SELECT ID_NIVEL, MONTO, YEAR, LIMITE_INFERIOR, LIMITE_SUPERIOR, ID_ESTUDIO "
                                            + "FROM DGIRA_MIAE2.CAT_DERECHOS WHERE ID_ESTUDIO = (SELECT VCCLAVE_TRAMITE "
                                            + "FROM SINATEC.VEXDATOSTRAMITE VX WHERE VX.BGTRAMITEID = " + folio + ") ");
                        
        Documento.add(new Paragraph(Chunk.NEWLINE));
        Documento.add(new Paragraph(Chunk.NEWLINE));
        
        PdfPTable tableValores = new PdfPTable(4);
        tableValores.setWidthPercentage((float) 100);
        
        Paragraph tNivel      = new Paragraph(new Chunk("Nivel", TITULO4));
        Paragraph tLinferior  = new Paragraph(new Chunk("Límite Inferior", TITULO4));
        Paragraph tLsuperior  = new Paragraph(new Chunk("Límite Superior", TITULO4));
        Paragraph tMonto      = new Paragraph(new Chunk("Monto", TITULO4));
        Paragraph tMontoPagar = new Paragraph(new Chunk("Total Monto a Pagar", TITULO4));

        tNivel.setAlignment(Element.ALIGN_CENTER);
        tLinferior.setAlignment(Element.ALIGN_CENTER);
        tLsuperior.setAlignment(Element.ALIGN_CENTER);
        tMonto.setAlignment(Element.ALIGN_CENTER);

        PdfPCell mh1 = new PdfPCell(tNivel);
        PdfPCell mh2 = new PdfPCell(tLinferior);
        PdfPCell mh3 = new PdfPCell(tLsuperior);
        PdfPCell mh4 = new PdfPCell(tMonto);
        
        PdfPCell tMontop = new PdfPCell(tMontoPagar);

        mh1.setHorizontalAlignment(Element.ALIGN_CENTER);
        mh2.setHorizontalAlignment(Element.ALIGN_CENTER);
        mh3.setHorizontalAlignment(Element.ALIGN_CENTER);
        mh4.setHorizontalAlignment(Element.ALIGN_CENTER);
        
        tMontop.setHorizontalAlignment(Element.ALIGN_RIGHT);
        
        tableValores.addCell(mh1);
        tableValores.addCell(mh2);
        tableValores.addCell(mh3);
        tableValores.addCell(mh3);
        
        //float[] columnWidths = new float[] {13f, 61f, 13f, 13f};
        //tableCriterios.setWidths(columnWidths);

         
        String vMontoPagar = "";
        while (rsValores.next()) {
            PdfPCell dIdNivel         = new PdfPCell(new Paragraph(new Chunk(rsValores.getString("ID_NIVEL"),PARRAFO)));
            PdfPCell dLimite_Inferior = new PdfPCell(new Paragraph(new Chunk(rsValores.getString("LIMITE_INFERIOR"),PARRAFO)));
            PdfPCell dLimite_Superior = new PdfPCell(new Paragraph(new Chunk(rsValores.getString("LIMITE_SUPERIOR"),PARRAFO)));
            PdfPCell dMonto           = new PdfPCell(new Paragraph(new Chunk(rsValores.getString("MONTO"),PARRAFO)));
            
            dIdNivel.setHorizontalAlignment(Element.ALIGN_CENTER);
            dLimite_Inferior.setHorizontalAlignment(Element.ALIGN_CENTER);
            dLimite_Superior.setHorizontalAlignment(Element.ALIGN_CENTER);
            dMonto.setHorizontalAlignment(Element.ALIGN_RIGHT);
            
            tableValores.addCell(dIdNivel);
            tableValores.addCell(dLimite_Inferior);
            tableValores.addCell(dLimite_Superior);
            tableValores.addCell(dMonto);    
                        
            if ((rsValores.getInt("LIMITE_INFERIOR") <= vTotal) && (vTotal <= rsValores.getInt("LIMITE_SUPERIOR")))
                vMontoPagar = rsValores.getString("MONTO");
            
        }   
                           
        
        tMontop.setColspan(3);
        tableValores.addCell(tMontop);
        
        Documento.add(new Paragraph(Chunk.NEWLINE));
        Documento.add(new Paragraph(Chunk.NEWLINE));
        
        
        PdfPCell dMontoPagar         = new PdfPCell(new Paragraph(new Chunk(vMontoPagar,PARRAFO)));
        dMontoPagar.setHorizontalAlignment(Element.ALIGN_RIGHT);
        
        tableValores.addCell(dMontoPagar);
        
        Paragraph TituloMontos = new Paragraph(new Chunk("Monto del pago", TITULO2));
        TituloMontos.setAlignment(Element.ALIGN_CENTER);
        Documento.add(TituloMontos);
        Documento.add(new Paragraph(Chunk.NEWLINE));
        
        Documento.add(tableValores);
        
        rsValores.close();
        st.close();        
        Documento.close();
        
        List<InputStream> pdfs = new LinkedList<InputStream>();      
        pdfs.add(new ByteArrayInputStream(ouBs.toByteArray()));

        ByteArrayOutputStream cOut = new ByteArrayOutputStream();
        ReportUtil.juntaPdf(pdfs, cOut);

        return cOut.toByteArray();
    }
}
