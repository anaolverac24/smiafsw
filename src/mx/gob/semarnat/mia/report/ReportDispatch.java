/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.report;

import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mauricio
 */
@SuppressWarnings("serial")
public class ReportDispatch extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/pdf");
        OutputStream responseOutputStream = response.getOutputStream();
        try {
            String folio        = request.getParameter("folio");
            String serial       = request.getParameter("serial");
            String vTipoReporte = request.getParameter("TipoReporte");
            
            System.out.println("PDF");
            System.out.println("Folio" + folio);
            System.out.println("Serial" + serial);
            	
            // configuracion_pool
            System.out.println("Ini");
            byte[] pdf;
            if (vTipoReporte.compareTo("PDF")==0){
                ReportFactory r = new ReportFactory();
                //pdf = r.pdf(folio, (short) Integer.parseInt(serial),"http://localhost:8090/ManifestacionImpactoAmbiental/");
                //pdf = r.pdf(folio, (short) Integer.parseInt(serial), "http://appsdev.semarnat.gob.mx:8080/ManifestacionImpactoAmbiental/"); // DESARROLLO
                pdf = r.pdf(folio, (short) Integer.parseInt(serial), "https://mia.semarnat.gob.mx:8443/ManifestacionImpactoAmbiental/"); // PRODUCCION
            }else { //if(vTipoReporte.compareTo("PAGO")==0){                        
                ReportePagoDerechos r = new ReportePagoDerechos();
                //pdf = r.pdf(folio, (short) Integer.parseInt(serial),"http://app2.semarnat.gob.mx:8080/ManifestacionImpactoAmbiental/");
                //pdf = r.pdf(folio, (short) Integer.parseInt(serial),"http://appsdev.semarnat.gob.mx:8080/ManifestacionImpactoAmbiental/"); // DESARROLLO
                pdf = r.pdf(folio, (short) Integer.parseInt(serial),"https://mia.semarnat.gob.mx:8443/ManifestacionImpactoAmbiental/"); // PRODUCCION
            }
            System.out.println("Fin");

            for (byte b : pdf) {
                responseOutputStream.write(b);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            responseOutputStream.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
