/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.converter;

import mx.gob.semarnat.mia.model.CatServicio;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import mx.gob.semarnat.mia.dao.EntityFactory;

/**
 *
 * @author mauricio
 */
@ManagedBean(name = "catServicioConverterBean")
//@FacesConverter(value = "catServicioConverter")
@FacesConverter(forClass=CatServicio.class)
public class CatServicioConverter implements Converter {

//    public static EntityManagerFactory emf = Persistence.createEntityManagerFactory("DGIRA_MIAE");
    public static EntityManagerFactory emf = EntityFactory.getMiaMF();

    @Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
        EntityManager em = emf.createEntityManager();
        Object ob = em.find(CatServicio.class, new Short(value));
        em.close();

        return ob;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if(o==null){
        	return"";
        }else{
        	return ((CatServicio) o).getServicioId()+ "";
        }
    	
    }

}
