/**
 * 
 */
package mx.gob.semarnat.mia.converter;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import mx.gob.semarnat.mia.dao.VegFaseDAO;
import mx.gob.semarnat.mia.model.VegFase;

/**
 * @author César
 *
 */
@ManagedBean(name = "catVegFaseConverterBean")
//@FacesConverter(value = "catNormaConverter")
@FacesConverter(forClass=VegFase.class)
public class CatVegFaseConverter implements Converter {

	/**
	 * Dao
	 */
	VegFaseDAO vegDAO;
	
	@Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
        vegDAO = new VegFaseDAO();
        Object ob = vegDAO.find(Integer.parseInt(value));

        return ob;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
    	if (o == null){
        	return ""; 
        }
        return ((VegFase) o).getIdVegFase()+ "";
    }
}
