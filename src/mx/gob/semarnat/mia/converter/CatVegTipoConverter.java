/**
 * 
 */
package mx.gob.semarnat.mia.converter;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import mx.gob.semarnat.mia.dao.VegTipoDAO;
import mx.gob.semarnat.mia.model.VegTipo;

/**
 * @author César
 *
 */
@ManagedBean(name = "catVegTipoConverterBean")
//@FacesConverter(value = "catNormaConverter")
@FacesConverter(forClass=VegTipo.class)
public class CatVegTipoConverter implements Converter {

	/**
	 * Dao
	 */
	VegTipoDAO vegDAO;
	
	@Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
        vegDAO = new VegTipoDAO();
        Object ob = vegDAO.find(Integer.parseInt(value));

        return ob;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
    	if (o == null){
        	return ""; 
        }
        return ((VegTipo) o).getIdVegTipo()+ "";
    }
}
