/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.converter;

import mx.gob.semarnat.mia.model.CatCriterio;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.lf5.viewer.categoryexplorer.CategoryImmediateEditor;

import mx.gob.semarnat.mia.dao.EntityFactory;
import mx.gob.semarnat.mia.model.CatClasificacion;
import mx.gob.semarnat.mia.model.CatContaminante;
import mx.gob.semarnat.mia.model.CatTipoImpacto;

/**
 *
 * @author mauricio
 */
@ManagedBean(name = "catTipoImpactoConverterBean")
//@FacesConverter(value = "catTipoImpactoConverter")
@FacesConverter(forClass=CatTipoImpacto.class)
public class CatTipoImpactoConverter implements Converter {

    public static EntityManagerFactory emf = EntityFactory.getMiaMF();

    @Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
        EntityManager em = emf.createEntityManager();
        Object ob = em.find(CatTipoImpacto.class, new Short(value));
        em.close();

        return ob;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if(o==null){
        	return"";
        }
        return ((CatTipoImpacto) o).getTipoImpactoId() + "";
    }

}
