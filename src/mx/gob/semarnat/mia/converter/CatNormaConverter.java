/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.converter;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import mx.gob.semarnat.mia.model.CatNorma;
import mx.gob.semarnat.mia.view.capitulo1.SelectItemsBaseConverter;
import mx.gob.semarnat.mia.view.to.EtapaProyectoTO;

/**
 *
 * @author mauricio
 */
@ManagedBean(name = "catNormaConverterBean")
public class CatNormaConverter extends SelectItemsBaseConverter {

    /* (non-Javadoc)
     * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof CatNorma) || ((CatNorma) value).getNormaId() == 0) {
            return null;
        }

        return String.valueOf(((CatNorma) value).getNormaId() );
    }
}
