/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.converter;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import mx.gob.semarnat.mia.dao.EntityFactory;
import mx.gob.semarnat.mia.model.CatClasificacion;
import mx.gob.semarnat.mia.model.CatPdu;
import mx.gob.semarnat.mia.model.CatPoet;

/**
 *
 * @author Rengerden
 */
@ManagedBean(name = "catPoetConverterBean")
//@FacesConverter(value = "catPoetConverter")
@FacesConverter(forClass=CatPoet.class)
public class CatPoetConverter implements Converter {
	
	public static EntityManagerFactory emf = EntityFactory.getMiaMF();

    @Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
        EntityManager em = emf.createEntityManager();
        //Object ob = em.find(CatPoet.class, new Short(value));
        Object ob = value;
        ob.toString();
        em.close();

        return ob;
    }
    
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
    	if (o == null){
    		return "";
    	}
        //return ((CatPoet) o).getPoetId()+ "";
    	return o.toString();
    }

}
