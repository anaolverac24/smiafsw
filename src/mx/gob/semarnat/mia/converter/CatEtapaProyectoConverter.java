/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.converter;

import mx.gob.semarnat.mia.model.CatCriterio;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import mx.gob.semarnat.mia.dao.EntityFactory;
import mx.gob.semarnat.mia.model.CatEtapa;
import mx.gob.semarnat.mia.model.EtapaProyecto;
import mx.gob.semarnat.mia.model.EtapaProyectoPK;

/**
 *
 * @author mauricio
 */
@ManagedBean(name = "catEtapaProyectoConverterBean")
@FacesConverter(value = "catEtapaProyectoConverter")
public class CatEtapaProyectoConverter implements Converter {

    public static EntityManagerFactory emf = EntityFactory.getMiaMF();

    @Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
        EntityManager em = emf.createEntityManager();
//        Object ob = em.find(EtapaProyecto.class, new Short(value));
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");

        Object ob = em.find(EtapaProyecto.class, new EtapaProyectoPK(folioProyecto, serialProyecto, new Short(value)));
        em.close();

        return ob;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        return ((EtapaProyecto) o).getEtapaProyectoPK().getEtapaId() + "";
    }

}
