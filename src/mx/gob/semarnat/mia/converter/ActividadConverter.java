package mx.gob.semarnat.mia.converter;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import mx.gob.semarnat.mia.dao.EntityFactory;
import mx.gob.semarnat.mia.model.ActividadEtapa;


@ManagedBean(name = "actividadConverterBean")
// @FacesConverter(value = "actividadConverter")
@FacesConverter(forClass=ActividadEtapa.class)
public class ActividadConverter implements Converter {

    public static EntityManagerFactory emf = EntityFactory.getMiaMF();

    @Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
        EntityManager em = emf.createEntityManager();
        Object ob = em.find(ActividadEtapa.class, new Short(value));
        // Object ob = em.find(ActividadEtapa.class, new ActividadEtapaPK("12195", (short)1, (short)1, (short)2));
        em.close();

        return ob;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
    	if (o == null){
    		return "";
    	}
        return ((ActividadEtapa) o).getActividadEtapaPK().getActividadEtapaId() + "";
    }

}

