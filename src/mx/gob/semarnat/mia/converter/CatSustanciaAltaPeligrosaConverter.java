/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.converter;

import mx.gob.semarnat.mia.model.CatCriterio;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import mx.gob.semarnat.mia.dao.EntityFactory;
import mx.gob.semarnat.mia.model.CatSustanciaAltamRiesgosa;

/**
 *
 * @author mauricio
 */
@ManagedBean(name = "catSustanciaAltaPeligrosaConverterBean")
@FacesConverter(value = "catSustanciaAltaPeligrosaConverter")
public class CatSustanciaAltaPeligrosaConverter implements Converter {

    public static EntityManagerFactory emf = EntityFactory.getMiaMF();

    @Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
        EntityManager em = emf.createEntityManager();
        Object ob = em.find(CatSustanciaAltamRiesgosa.class, new Short(value));
        em.close();

        return ob;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        return ((CatSustanciaAltamRiesgosa) o).getSustanciaId()+ "";
    }

}
