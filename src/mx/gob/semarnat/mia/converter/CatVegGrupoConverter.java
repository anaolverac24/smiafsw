/**
 * 
 */
package mx.gob.semarnat.mia.converter;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import mx.gob.semarnat.mia.dao.VegGrupoDAO;
import mx.gob.semarnat.mia.model.VegGrupo;

/**
 * @author César
 *
 */
@ManagedBean(name = "catVegGrupoConverterBean")
//@FacesConverter(value = "catNormaConverter")
@FacesConverter(forClass=VegGrupo.class)
public class CatVegGrupoConverter implements Converter {

	/**
	 * Dao
	 */
	VegGrupoDAO vegDAO;
	
	@Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
        vegDAO = new VegGrupoDAO();
        Object ob = vegDAO.find(Integer.parseInt(value));

        return ob;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
    	if (o == null){
        	return ""; 
        }
        return ((VegGrupo) o).getIdVegGrupo()+ "";
    }
}
