/**
 * 
 */
package mx.gob.semarnat.mia.Enum;

/**
 * @author dpaniagua.
 *
 */
public enum FaunaClaseEnum {
	AVES("Aves"),
	
	AMPHIBIAREPTILIA("AmphibiaReptilia"),
	
	MAMIFERO("Mammalia"),
	
	PECES("ActinopterygiiChondrichthyes");
	
	private String claseFauna;
	
	private FaunaClaseEnum(String claseFauna) {
		this.claseFauna = claseFauna;
	}

	/**
	 * @return the claseFauna
	 */
	public String getClaseFauna() {
		return claseFauna;
	}

	/**
	 * @param claseFauna the claseFauna to set
	 */
	public void setClaseFauna(String claseFauna) {
		this.claseFauna = claseFauna;
	}
}
