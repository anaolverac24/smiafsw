/**
 * 
 */
package mx.gob.semarnat.mia.Enum;

/**
 * @author Cesar
 *
 */
public enum EnumConstantes {
	
	CLAVEINSERCCIONSIGEIA(0),
	
	CLAVEINSERCCIONPROMOVENTE(1);
	
	/**
	 * Valor de la constante
	 */
	private int clave;
	
	/**
	 * Constructor
	 * @param clave
	 */
	private EnumConstantes(int clave) {
		this.clave = clave;
	}

	/**
	 * @return the clave
	 */
	public int getClave() {
		return clave;
	}

	/**
	 * @param clave the clave to set
	 */
	public void setClave(int clave) {
		this.clave = clave;
	}

}
