/**
 * 
 */
package mx.gob.semarnat.mia.Enum;

/**
 * @author dpaniagua.
 *
 */
public enum ArchivosAnexosEnum {

	CONCLUSIONES("CONCLUSIONES"),
	
	SITUACIONGENERAL("SITUACIONGENERAL"),
	
	BASESDISENIO("BASESDISENIO"),
	
	INFORMETECNICO("INFORMETECNICO"),
	
	CLIMAFENOMENOS("CLIMAFENOMENOS"),
	
	SUELOS("SUELOS"),
	
	PROYECTOCIVIL("PROYECTOCIVIL"),
	
	PROYECTOMECANICO("PROYECTOMECANICO"),
	
	PROYECTOINCENDIO("PROYECTOINCENDIO"),
	
	DESCRIPCIONDETALLADA("DESCRIPCIONDETALLADA"),
	
	HOJASSEGURIDAD("HOJASSEGURIDAD"),
	
	ALMACENAMIENTO("ALMACENAMIENTO"),
	
	EQUIPOSAUX("EQUIPOSAUX"),
	
	PRUEBASVERIFICACION("PRUEBASVERIFICACION"),
	
	CONDICIONES("CONDICIONES"),
	
	ESPECIFICACION("ESPECIFICACION"),
	
	AISLAMIENTO("AISLAMIENTO"),
	
	ANTECEDENTESACCIDENTES("ANTECEDENTESACCIDENTES"),
	
	METODOLOGIAS("METODOLOGIAS"),
	
	DELIMITACIONSA("DELIMITACIONSA"),
	
	DELIMITACIONAI("DELIMITACIONAI"),
	
	DELIMITACIONSP("DELIMITACIONSP"),
	
	RADIOS("RADIOS"),
	
	INTERACCIONES("INTERACCIONES"),
	
	EFECTOS("EFECTOS"),
	
	RECOMENDACIONES("RECOMENDACIONES"),
	
	SISTEMAS("SISTEMAS"),
	
	MEDIDAS("MEDIDAS"),
	
	PLANOS("PLANOS"),
	
	OTROS("OTROS");
		
	private String subCapSeleccionado;
	/**
	 * 
	 * @param subCapSeleccionado
	 */
	private ArchivosAnexosEnum(String subCapSeleccionado) {
		this.subCapSeleccionado = subCapSeleccionado;
	}

	/**
	 * @return the subCapSeleccionado
	 */
	public String getSubCapSeleccionado() {
		return subCapSeleccionado;
	}

	/**
	 * @param subCapSeleccionado the subCapSeleccionado to set
	 */
	public void setSubCapSeleccionado(String subCapSeleccionado) {
		this.subCapSeleccionado = subCapSeleccionado;
	}
}
