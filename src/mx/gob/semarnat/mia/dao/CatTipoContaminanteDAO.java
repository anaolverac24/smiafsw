/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.io.Serializable;

import javax.persistence.EntityManager;

import mx.gob.semarnat.mia.model.CatTipoContaminante;

/**
 *
 * @author César
 */
public class CatTipoContaminanteDAO extends AbstractBaseDAO<CatTipoContaminante> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -677144299020599399L;
	/**
	 * EntityManager
	 */
	EntityManager mia = EntityFactory.getSigeia().createEntityManager();

	/**
	 * Instantiates a new ReiaProyObrasDAO impl.
	 */
	public CatTipoContaminanteDAO() {
		super(CatTipoContaminante.class);
	}

	/**
	 * Returns the EntityManager
	 * 
	 * @return Em
	 */
	@Override
	protected EntityManager getEntityManager() {
		return this.mia;
	}

	
}
