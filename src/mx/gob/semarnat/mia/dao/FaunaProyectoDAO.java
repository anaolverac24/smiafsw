/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.FaunaProyecto;
import mx.gob.semarnat.mia.model.FloraProyecto;
import mx.gob.semarnat.mia.model.VwAnimaliaFauna;

/**
 * @author dpaniagua.
 *
 */
public class FaunaProyectoDAO {
	/**
	 * Permite consultar la animalia fauna segun el nombre cientifico seleccionado.
	 * @param nombre cientifico de la fauna.
	 * @return la fauna del proyecto.
	 */
    public VwAnimaliaFauna getAnimaliaFauna(String nombre) {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        Query q = mia.createQuery("FROM VwAnimaliaFauna WHERE animaliaNombreCientifico = :nombre");
        q.setParameter("nombre", nombre);
        VwAnimaliaFauna animaliaFauna = (VwAnimaliaFauna) q.getSingleResult();
        return animaliaFauna;
    }
    
    /**
     * Permite obtener el valor max de la fauna
     * @param folio del proyecto.
     * @param serial del proyecto.
     * @return el valor maximo de la fauna
     */
    public Integer getMaxFauna() {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        Query q = mia.createQuery("SELECT max(a.faunaProyectoPK.faunaProyid) FROM FaunaProyecto a ");
        return (Integer) q.getSingleResult();
    }    
    
    /**
     * Permite obtener el sequencial maximos de la fauna.
     * @param folio del proyecto.
     * @param serial del proyecto.
     * @return
     */
    public Short getMaxIdFaunaSEQ(String folio, short serial) {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        Query q = mia.createQuery("SELECT max(a.idFaunaSeq) FROM FaunaProyecto a where a.faunaProyectoPK.folioProyecto = :folio and a.faunaProyectoPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Short) q.getSingleResult();
    } 
    
    /**
     * Permite guardar el nuevo registro de fauna.
     * @param object fauna a guardar.
     */
    public void guardarFauna(Object object) throws Exception {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        mia.getTransaction().begin();
        mia.persist(object);
        mia.getTransaction().commit();
    }
    
    /**
     * Permite obtener la lista de fauna del proyecto con su serial.
     * @param folio del proyecto.
     * @param serial del proyecto.
     * @return la lista de la fauna del proyecto.
     */
    public List<FaunaProyecto> getFaunaProyecto(String folio, Short serial) {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        Query q = mia.createQuery("SELECT e FROM FaunaProyecto e WHERE e.faunaProyectoPK.folioProyecto = :folio and e.faunaProyectoPK.serialProyecto = :serial order by e.faunaProyectoPK.faunaProyid");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }
    /**
     * Permite consultar la fauna seleccionada para editar.
     * @param idFaunaSeqSeleccionada a editar.
     * @return la fauna a consultar.
     */
    public FaunaProyecto consultarFaunaProyectoSeleccionada(String folio, Short serial, Short idFaunaSeqSeleccionada) {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        Query q = mia.createQuery("FROM FaunaProyecto e WHERE e.faunaProyectoPK.folioProyecto = :folio and e.faunaProyectoPK.serialProyecto = :serial and e.idFaunaSeq = :idFaunaSeq");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("idFaunaSeq", idFaunaSeqSeleccionada);
        return (FaunaProyecto) q.getSingleResult();
    }

    /**
     * Permite actualizar la fauna seleccionada.
     * @param object
     */
	public void actualizarFauna(Object object) throws Exception {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        mia.getTransaction().begin();
        mia.merge(object);
        mia.getTransaction().commit();
	}     
	
    /**
     * Permite eliminar la fauna seleccionada.
     * @param idFaunaSeq a eliminar.
     * @return 1 en caso de ser exitoso.
     */
    public int eliminarFauna(Short idFaunaSeq) throws Exception {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        mia.getTransaction().begin();
        Query q = mia.createQuery("DELETE FROM FaunaProyecto WHERE idFaunaSeq = :idFaunaSeq");
        q.setParameter("idFaunaSeq", idFaunaSeq);
        int eliminados = q.executeUpdate();
        mia.getTransaction().commit();
        
        return eliminados;
    } 
}
