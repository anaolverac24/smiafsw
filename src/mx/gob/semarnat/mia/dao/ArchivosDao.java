/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import static mx.gob.semarnat.mia.dao.VisorDao.emf;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.AnexosProyecto;
import mx.gob.semarnat.mia.model.CatParametro;
import mx.gob.semarnat.mia.model.EspecificacionAnexo;
import mx.gob.semarnat.mia.model.EstudiosEspProy;
import mx.gob.semarnat.mia.model.SustanciaAnexo;

/**
 *
 * @author efrainc
 */
public class ArchivosDao 
{
    
    //Consulta de union para hacer la consulta de los campos para la tabla de resumen por proyecto
    
    //Variable para almacenar archivo
	
	
	private String nombreArchivo;
	
	 //	Setter y getter para almacenar el nombre del archivo
    public String getNombreArchivo() {
		return nombreArchivo;
	}


	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	//////////////////////////////
	
	
	  public Long conteoAdjuntos(String folio, short serial, String nombre) {
	        BigDecimal dRes = BigDecimal.ZERO;
	        String strNombre = null;
	        boolean bNombre = nombre != null;
	        if (bNombre) {
	            strNombre = "%" + nombre;
	        }
	        try {
	            EntityManager em = emf.createEntityManager();
	            Query q = em.createNativeQuery(
				"select sum(conteo) from " + 
				"( " + 
				"    select count (*) as conteo  " + 
				"        from anexos_proyecto AP  " + 
				"        where AP.folio_proyecto = :folio  " + 
				"          and AP.serial_proyecto = :serial  " + 
				(bNombre ? "          and AP.anexo_nombre like :nombre " : "") + 
				"    union all " + 
				"    select count (*) as conteo  " + 
				"        from especificacion_anexo EA  " + 
				"        where EA.folio_proyecto = :folio  " + 
				"          and EA.serial_proyecto = :serial  " + 
	                        (bNombre ? "          and EA.espec_anexo_nombre like :nombre " : "") + 
				"    union all " + 
				"    select count (*) as conteo  " + 
				"        from sustancia_anexo SA  " + 
				"        where SA.folio_proyecto = :folio  " + 
				"          and SA.serial_proyecto = :serial  " + 
	                        (bNombre ? "          and SA.anexo_nombre like :nombre " : "") +
				"    union all " + 
				"    select count (*) as conteo  " + 
				"        from estudios_esp_proy EE  " + 
				"        where EE.folio_serial = :folio  " + 
				"          and EE.serial_proyecto = :serial  " +
	                        "          and EE.anexo_nombre is not null " +                         
	                        (bNombre ? "          and EE.anexo_nombre like :nombre " : "") +                                
				") "            
	            );
	            
	            q.setParameter("serial", serial);
	            q.setParameter("folio", folio);
	            if (bNombre) {
	                q.setParameter("nombre", strNombre);
	            }
	            dRes = (BigDecimal) q.getSingleResult();
	            em.close();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return dRes.longValue();
	    }    

	
	
	 public Object getObject(Class <?> tipo, Object id){
	        return emf.createEntityManager().find(tipo, id);
	    }
	
	 public short numRegAnexoEst(String folio,Short serial) {
	        int id = 0;
	        try{
	            EntityManager em = emf.createEntityManager();
	            Query q = em.createQuery("SELECT max(s.estudiosEspProyPK.estudioId) FROM EstudiosEspProy s WHERE s.estudiosEspProyPK.folioSerial = :folio AND s.estudiosEspProyPK.serialProyecto = :serial");
	            q.setParameter("folio",folio);
	            q.setParameter("serial",serial);
	            id = (Short)q.getSingleResult();
	            em.close();
	        }catch(Exception e){
	            e.printStackTrace();
	        }
	        return (short) id;
	    }

	 public short numRegAnexoEspe(String folioProyecto, short serialProyecto) {
	        short r = 0;
	                try {
	            EntityManager em = emf.createEntityManager();
	            Query q = em.createQuery("SELECT max(a.especificacionAnexoPK.especificacionId) FROM EspecificacionAnexo a Where a.especificacionAnexoPK.folioProyecto = :folio and a.especificacionAnexoPK.serialProyecto = :serial");
	            q.setParameter("folio", folioProyecto);
	            q.setParameter("serial", serialProyecto);
	            
	            r = (Short) q.getSingleResult();
	            em.close();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return (short) r;
	    }

	  public short numRegAnexo(short capituloId, short subcapituloId, short seccionId, short apartadoId, String folioProyecto, short serialProyecto) {
	        int r = 0;
	        try {
	            EntityManager em = emf.createEntityManager();
	            Query q = em.createQuery("SELECT max(a.anexosProyectoPK.anexoId) FROM AnexosProyecto a");
	            r = (Short) q.getSingleResult();
	            em.close();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return (short) r;
	    }
	    
	  
	  public short numRegAnexoSust() {
	        int id = 0;
	        try{
	            EntityManager em = emf.createEntityManager();
	            Query q = em.createQuery("SELECT max(s.sustanciaAnexoPK.sustanciaAnexoId) FROM SustanciaAnexo s");
	            id = (Short)q.getSingleResult();
	            em.close();
	        }catch(Exception e){
	            e.printStackTrace();
	        }
	        return (short) id;
	    }
	 
	 
	 ////////////////////////////////////////

	public List<Object[]> ObtenerResumen(String folio ) {
        

        try {
            EntityManager em = emf.createEntityManager();
            Query q = em.createNativeQuery("SELECT * FROM("
            		+ "SELECT CAPITULO_ID, SECCION_ID, NOMBRE, DESCRIPCION, TAMANIO, RESERVADO, URL FROM ARCHIVOS_PROYECTO WHERE FOLIO_SERIAL =:folio "
            		+ "UNION ALL "
            		+ "SELECT CAPITULO_ID, SECCION_ID, ANEXO_NOMBRE, ANEXO_DESCRIPCION, ANEXO_TAMANIO, RESERVADO, ANEXO_URL FROM ESTUDIOS_ESP_PROY WHERE FOLIO_SERIAL =:folio)");            
            
            q.setParameter("folio", folio);
             return q.getResultList();
            //em.close();
            
            
            //System.out.println("Mensaje Resumen");
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error"+ e.getMessage());
            return null; 
        }
		
        
       
    }
    
    
    //Consulta todos los registros de la tabla de anexosProyecto
     public static List<AnexosProyecto> getAnexos(short capituloId, short subcapituloId, short seccionId, short apartadoId, String folioProyecto) {
        List<AnexosProyecto> r = new ArrayList<AnexosProyecto>();
        try {
            EntityManager em = emf.createEntityManager();
            Query q = em.createQuery("SELECT a FROM AnexosProyecto a WHERE a.anexosProyectoPK.apartadoId = :apartado and a.anexosProyectoPK.capituloId = :capitulo and a.anexosProyectoPK.folioProyecto = :folio and a.anexosProyectoPK.seccionId = :seccion and a.anexosProyectoPK.subcapituloId = :subcapitulo");
            q.setParameter("apartado", apartadoId);
            q.setParameter("capitulo", capituloId);
            q.setParameter("folio", folioProyecto);
            
            q.setParameter("seccion", seccionId);
            q.setParameter("subcapitulo", subcapituloId);
            r = q.getResultList();
            em.close();
        } catch (Exception e) {

        }

        return r;

    }
   
     //Consulta todos los registros de la tabla EstudiosProyecto
    public static List<EstudiosEspProy> getEstudiosEspProy(String folioProyecto, Short serialProyecto, Short estudiosProyId){
        EntityManager em = emf.createEntityManager();
        List<EstudiosEspProy> e;
            if(estudiosProyId!=null){
                Query q = em.createQuery("SELECT s FROM EstudiosEspProy s WHERE s.estudiosEspProyPK.folioSerial = :folio AND s.estudiosEspProyPK.serialProyecto = :serial AND s.estudiosEspProyPK.estudioId = :id");
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                q.setParameter("id", estudiosProyId);
                e = q.getResultList();
            }else{
                Query q = em.createQuery("SELECT s FROM EstudiosEspProy s WHERE s.estudiosEspProyPK.folioSerial = :folio AND s.estudiosEspProyPK.serialProyecto :serial");
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                e = q.getResultList();
            }
        return e;
    }
    
    //Consulta todos los registros de la tabla Especificaciones anexos 
    public static List<EspecificacionAnexo> getEspecifiacionesAnexos(String folio, String especifiacionId, Short serialProeycto, Short normaId) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT a FROM EspecificacionAnexo a WHERE a.especificacionAnexoPK.folioProyecto = :folio and a.especificacionAnexoPK.especificacionId = :especificacion and a.especificacionAnexoPK.serialProyecto = :serial and a.especificacionAnexoPK.normaId = :norma");
        q.setParameter("folio", folio);
        q.setParameter("especificacion", especifiacionId);
        q.setParameter("serial", serialProeycto);
        q.setParameter("norma", normaId);
        List l = q.getResultList();

        em.close();
        return l;
    }

   //Consuta de tabla sustancias anexos
     public static List<SustanciaAnexo> getSustanciaAnexos(String folioProyecto, Short serialProyecto, Short sustanciaProyId) {
        EntityManager em = emf.createEntityManager();
        List<SustanciaAnexo> hs;
            if(sustanciaProyId!=null){
                Query q = em.createQuery("SELECT s FROM SustanciaAnexo s WHERE s.sustanciaAnexoPK.folioProyecto = :folio AND s.sustanciaAnexoPK.serialProyecto = :serial AND s.sustanciaAnexoPK.sustanciaProyId= :id");
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                q.setParameter("id", sustanciaProyId);
                hs = q.getResultList();
            }else{
                Query q = em.createQuery("SELECT s FROM SustanciaAnexo s WHERE s.sustanciaAnexoPK.folioProyecto = :folio AND s.sustanciaAnexoPK.serialProyecto= :serial");
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                hs = q.getResultList();
            }
            return hs;
    }
    
     
     //temp/share/allusers/FOLIOpROYECTO/NOMBREARCHIVO
     
     public String ObtieneRuta()
     {
     	EntityManager em = EntityFactory.getMiaMF().createEntityManager();
         Query q = em.createQuery("SELECT a FROM CatParametro a WHERE a.parDescripcion = 'ADJUNTOS'");
         CatParametro c = (CatParametro) q.getSingleResult();
         
         return c.getParRuta();
     }
     
     
     
     
     
     
    
}
