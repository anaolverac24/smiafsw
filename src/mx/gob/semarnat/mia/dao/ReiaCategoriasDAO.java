/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.ReiaCategorias;
import mx.gob.semarnat.mia.model.ReiaObras;

/**
 *
 * @author César
 */
public class ReiaCategoriasDAO extends AbstractBaseDAO<ReiaCategorias> {
    
    /**
     * EntityManager
     */
    EntityManager mia=EntityFactory.getSigeia().createEntityManager();

    /**
    * Instantiates a new ReiaCategoriasDAO impl.
    */
    public ReiaCategoriasDAO() {
        super(ReiaCategorias.class);
    }

    /**
     * Returns the EntityManager
     * @return Em
     */
    @Override
    protected EntityManager getEntityManager() {
        return this.mia;
    }
    
    
    public List<ReiaCategorias> findByEstatus(Long estatus){
    	List<ReiaCategorias> rtrn=null;
        try{
            Query q = null;	
            q = mia.createNamedQuery("ReiaCategorias.findByEstatusFracionNotNull");
            q.setParameter("estatus", estatus);                   
            rtrn = q.getResultList();

        }catch(PersistenceException px){
            px.printStackTrace();
        }
        return rtrn;    
    }
    
}
