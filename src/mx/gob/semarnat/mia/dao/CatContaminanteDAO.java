/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.CatContaminante;

/**
 *
 * @author César
 */
public class CatContaminanteDAO extends AbstractBaseDAO<CatContaminante> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -677144299020599399L;
	/**
	 * EntityManager
	 */
	EntityManager mia = EntityFactory.getSigeia().createEntityManager();

	/**
	 * Instantiates a new ReiaProyObrasDAO impl.
	 */
	public CatContaminanteDAO() {
		super(CatContaminante.class);
	}

	/**
	 * Returns the EntityManager
	 * 
	 * @return Em
	 */
	@Override
	protected EntityManager getEntityManager() {
		return this.mia;
	}


	public List<CatContaminante> findByIdTipo(short idTipo) {
		List<CatContaminante> rtn = new ArrayList<CatContaminante>();
		try {
			Query q = null;
			q = mia.createNamedQuery("CatContaminante.findByIdTipo");
			q.setParameter("idTipo", idTipo);
			rtn = q.getResultList();

		} catch (PersistenceException px) {
			px.printStackTrace();
		}
		return rtn;

	}
}
