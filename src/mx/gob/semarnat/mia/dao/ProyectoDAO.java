/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import javax.persistence.EntityManager;

import mx.gob.semarnat.mia.model.Proyecto;

/**
 *
 * @author César
 */
public class ProyectoDAO extends AbstractBaseDAO<Proyecto> {
    
    /**
     * EntityManager
     */
    EntityManager mia=EntityFactory.getSigeia().createEntityManager();

    /**
     * Instantiates a new ReiaProyObrasDAO impl.
     */
    public ProyectoDAO() {
        super(Proyecto.class);
    }
    
    /**
     * Returns the EntityManager
     * @return Em
     */
    @Override
    protected EntityManager getEntityManager() {
       return this.mia;
    }
    
    
   
    
}
