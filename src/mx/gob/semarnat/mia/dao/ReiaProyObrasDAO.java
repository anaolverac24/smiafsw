/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.ReiaProyObras;

/**
 *
 * @author César
 */
public class ReiaProyObrasDAO extends AbstractBaseDAO<ReiaProyObras> {
    
    /**
     * EntityManager
     */
    EntityManager mia=EntityFactory.getSigeia().createEntityManager();

    /**
     * Instantiates a new ReiaProyObrasDAO impl.
     */
    public ReiaProyObrasDAO() {
        super(ReiaProyObras.class);
    }
    
    /**
     * Returns the EntityManager
     * @return Em
     */
    @Override
    protected EntityManager getEntityManager() {
       return this.mia;
    }
    
    
    @SuppressWarnings("unchecked")
	public List<ReiaProyObras> findByFolioEstatusObra(String folio, Long estatus) {
        List<ReiaProyObras> rtrn=null;
        try{
            Query q = null;	
            q = mia.createNamedQuery("ReiaProyObras.findByFolioEstatusObra");
            q.setParameter("folio", folio);
            q.setParameter("estatus", estatus);
            rtrn = q.getResultList();

        }catch(PersistenceException px){
            px.printStackTrace();
        }
    	return rtrn;
    }
    
    public List<ReiaProyObras> findByFolioObrasCapturadas(String folio) {
    	 List<ReiaProyObras> rtrn=null;
         try{
             Query q = null;	
             q = mia.createNamedQuery("ReiaProyObras.findByFolioObrasCapturadas");
             q.setParameter("folio", folio);
             rtrn = q.getResultList();

         }catch(PersistenceException px){
             px.printStackTrace();
         }
     	return rtrn;
    }
    
    /**
     * Busca las ReiaProyObras por su Folio y categoria
     * @param folio
     * @return lista de obras
     */
    public List<ReiaProyObras> findByFolioCategoria(String folio, Long idCategoria){
    	List<ReiaProyObras> listaObras=null;
        try{
            Query q = null;	
            q = mia.createNamedQuery("ReiaProyObras.findByCategoria");
            q.setParameter("folio", folio);
            q.setParameter("idCategoria", idCategoria);
            listaObras = q.getResultList();

        }catch(PersistenceException px){
            px.printStackTrace();
        }
    	return listaObras;
    }

    public int count(String folio, Long estatus) {
        Query q = null;	
        q = mia.createNamedQuery("ReiaProyObras.findByFolioEstatusObraCount");
        q.setParameter("folio", folio);
        q.setParameter("estatus", estatus);        
        return ((Long) q.getSingleResult()).intValue();
    }
    
    /**
     * Cuenta los registros de obras por folio
     * @param folio
     * @return numero de registros
     */
    public int countByFolio(String folio) {
        Query q = null;	
        q = mia.createNamedQuery("ReiaProyObras.findByFolioCount");
        q.setParameter("folio", folio);   
        return ((Long) q.getSingleResult()).intValue();
    }

   @SuppressWarnings("unchecked")
public List<ReiaProyObras> findRange(int[] range,String folio) {
       Query q = null;	
       q = mia.createNamedQuery("ReiaProyObras.findByFolioObrasCapturadas");
       q.setParameter("folio", folio);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    
}
