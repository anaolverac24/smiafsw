/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.AccidentesProyecto;
import mx.gob.semarnat.mia.model.ActividadEtapa;
import mx.gob.semarnat.mia.model.AnexosProyecto;
import mx.gob.semarnat.mia.model.AnpProyecto;
import mx.gob.semarnat.mia.model.ArchivosProyecto;
import mx.gob.semarnat.mia.model.ArticuloReglaAnp;
import mx.gob.semarnat.mia.model.AvanceProyecto;
import mx.gob.semarnat.mia.model.AvanceProyectoPK;
import mx.gob.semarnat.mia.model.CaracPartProy;
import mx.gob.semarnat.mia.model.CatApartado;
import mx.gob.semarnat.mia.model.CatCapitulo;
import mx.gob.semarnat.mia.model.CatClasificacion;
import mx.gob.semarnat.mia.model.CatClasificacionB;
import mx.gob.semarnat.mia.model.CatContaminante;
import mx.gob.semarnat.mia.model.CatConvenios;
import mx.gob.semarnat.mia.model.CatCriterio;
import mx.gob.semarnat.mia.model.CatEstEsp;
import mx.gob.semarnat.mia.model.CatEtapa;
import mx.gob.semarnat.mia.model.CatLey;
import mx.gob.semarnat.mia.model.CatNaturaleza;
import mx.gob.semarnat.mia.model.CatNorma;
import mx.gob.semarnat.mia.model.CatObra;
import mx.gob.semarnat.mia.model.CatPoet;
import mx.gob.semarnat.mia.model.CatReferencia;
import mx.gob.semarnat.mia.model.CatReglamento;
import mx.gob.semarnat.mia.model.CatSeccion;
import mx.gob.semarnat.mia.model.CatServicio;
import mx.gob.semarnat.mia.model.CatSubcapitulo;
import mx.gob.semarnat.mia.model.CatSustanciaAltamRiesgosa;
import mx.gob.semarnat.mia.model.CatTemporalidad;
import mx.gob.semarnat.mia.model.CatTipoContaminante;
import mx.gob.semarnat.mia.model.CatTipoImpacto;
import mx.gob.semarnat.mia.model.ClimaProyecto;
import mx.gob.semarnat.mia.model.Climas;
import mx.gob.semarnat.mia.model.ComentarioProyecto;
import mx.gob.semarnat.mia.model.ContaminanteProyecto;
import mx.gob.semarnat.mia.model.ConvenioProy;
//import mx.gob.semarnat.mia.model.ExplosivoActividadEtapa;
import mx.gob.semarnat.mia.model.CriterioValoresProyecto;
import mx.gob.semarnat.mia.model.CriteriosProyecto;
import mx.gob.semarnat.mia.model.CuerposAguaProyecto;
import mx.gob.semarnat.mia.model.DisposicionProyecto;
import mx.gob.semarnat.mia.model.EstudioRiesgoProyecto;
import mx.gob.semarnat.mia.model.EstudiosEspProy;
import mx.gob.semarnat.mia.model.EstudiosEspProyPK;
import mx.gob.semarnat.mia.model.EtapaProyecto;
import mx.gob.semarnat.mia.model.EvaluacionProyecto;
import mx.gob.semarnat.mia.model.ExplosivoActividadEtapa;
import mx.gob.semarnat.mia.model.FaunaProyecto;
import mx.gob.semarnat.mia.model.FloraProyecto;
import mx.gob.semarnat.mia.model.GlosarioProyecto;
import mx.gob.semarnat.mia.model.ImpacAmbProyecto;
import mx.gob.semarnat.mia.model.InfobioProyecto;
import mx.gob.semarnat.mia.model.InsumosProyecto;
import mx.gob.semarnat.mia.model.InversionEtapas;
import mx.gob.semarnat.mia.model.LeyFedEstProyecto;
import mx.gob.semarnat.mia.model.MedPrevImpactProy;
import mx.gob.semarnat.mia.model.NormaProyecto;
import mx.gob.semarnat.mia.model.Numeral;
import mx.gob.semarnat.mia.model.PdumProyecto;
import mx.gob.semarnat.mia.model.PoetmProyecto;
import mx.gob.semarnat.mia.model.PrediocolinProy;
import mx.gob.semarnat.mia.model.ProySubsectorGuias;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.ProyectoPK;
import mx.gob.semarnat.mia.model.ReglamentoProyecto;
import mx.gob.semarnat.mia.model.RepLegalProyecto;
import mx.gob.semarnat.mia.model.RepLegalProyectoPK;
import mx.gob.semarnat.mia.model.RespTecProyecto;
import mx.gob.semarnat.mia.model.ServicioProyecto;
import mx.gob.semarnat.mia.model.SueloVegetacionProyecto;
import mx.gob.semarnat.mia.model.SustanciaProyecto;
import mx.gob.semarnat.mia.model.VwAnimaliaFauna;
import mx.gob.semarnat.mia.model.VwPlantaeVegetacion;
import mx.gob.semarnat.mia.model.catalogos.CatTipoAsen;
import mx.gob.semarnat.mia.model.catalogos.CatUnidadMedida;
import mx.gob.semarnat.mia.model.catalogos.CatVialidad;
import mx.gob.semarnat.mia.model.catalogos.RamaProyecto;
import mx.gob.semarnat.mia.model.catalogos.SectorProyecto;
import mx.gob.semarnat.mia.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.mia.model.catalogos.TipoProyecto;
import mx.gob.semarnat.mia.model.sinatec.Vexdatosusuario;
import mx.gob.semarnat.mia.model.sinatec.Vexdatosusuariorep;
import mx.gob.semarnat.mia.util.Contaminanteu;
import mx.gob.semarnat.mia.util.Log;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mauricio
 */
public class MiaDao implements Serializable {
	
    EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
    EntityManager cat = EntityFactory.getCatalogos().createEntityManager();
    EntityManager sin = EntityFactory.getSinatec().createEntityManager();

    public static final short MIA_SERIAL_PROYECTO_INFO_ADICIONAL = 3;
    
    private static final Logger logger = LoggerFactory.getLogger(MiaDao.class);
    
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

    public Proyecto proyecto(String folio, short serial) {
        return (Proyecto) mia.find(Proyecto.class, new ProyectoPK(folio, serial));
    }

    public String getRegistrosLista(String folio, short serial, String lista) {
        Query q = mia.createNativeQuery("SELECT count(*) FROM " + lista + " WHERE " + lista + ".folio_Proyecto = :folio and " + lista + ".serial_Proyecto = :serial");

        if (lista.equals("ESTUDIOS_ESP_PROY")) {
            q = mia.createNativeQuery("SELECT count(*) FROM " + lista + " WHERE " + lista + ".folio_Serial = :folio and " + lista + ".serial_Proyecto = :serial");

        }
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        List l = q.getResultList();
        System.out.println("l.get(0)" + l.get(0));
        return "" + l.get(0);
    }

    public String getCampoProyecto(String folio, short serial, String campo) {
        Query q = mia.createQuery("SELECT p." + campo + " FROM Proyecto p WHERE p.proyectoPK.folioProyecto = :folio and p.proyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getSingleResult().toString();
    }

    public Proyecto proyecto(String folio) {
        Query q1 = mia.createQuery("SELECT p FROM Proyecto p WHERE p.proyectoPK.folioProyecto = :folio");
        q1.setParameter("folio", folio);
        if (q1.getResultList().isEmpty()) {
            return (Proyecto) mia.find(Proyecto.class, new ProyectoPK(folio, (short) 1));
        } else {
            Query q = mia.createQuery("SELECT p FROM Proyecto p WHERE p.proyectoPK.folioProyecto = :folio AND p.proyectoPK.serialProyecto = "
                    + " (SELECT MAX(p1.proyectoPK.serialProyecto) FROM Proyecto p1 WHERE p1.proyectoPK.folioProyecto = :folio)");
            q.setParameter("folio", folio);
            return (Proyecto) q.getSingleResult();
        }
    }

    public List<String> getImpactos(String folio, short serial) {
        Query q = mia.createQuery("SELECT a.impactIndent FROM ImpacAmbProyecto a WHERE a.impacAmbProyectoPK.folioProyecto = :folio and a.impacAmbProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }
    
    public List<ArticuloReglaAnp> getArticuloReglaAnp(String folio, short serial, String tipo) {
        Query q = mia.createQuery("SELECT r FROM ArticuloReglaAnp r WHERE r.articuloReglaAnpPK.folioProyecto = :folio and r.articuloReglaAnpPK.serialProyecto = :serial and r.anpArticuloRegla = :tipo");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("tipo", tipo);
        return q.getResultList();
    }   
    
    public RepLegalProyecto getRepLegalCurp(String folio, short serial, String curp) {
        Query q = mia.createQuery("SELECT r FROM RepLegalProyecto r WHERE r.repLegalProyectoPK.folioProyecto = :folio and r.repLegalProyectoPK.serialProyecto = :serial and r.repLegalProyectoPK.representanteCurp = :curp");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("curp", curp);
        return (RepLegalProyecto) q.getResultList().get(0);
    }

    public RepLegalProyecto getRepLegal(String folio, short serial, String rfc) {
        return (RepLegalProyecto) mia.find(RepLegalProyecto.class, new RepLegalProyectoPK(folio, serial, rfc));
    }

    public List<RepLegalProyecto> getListSelRepLegal(String folio, short serial) {
        Query q = mia.createQuery("SELECT r FROM RepLegalProyecto r WHERE r.repLegalProyectoPK.folioProyecto = :folio and r.repLegalProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public RepLegalProyecto getSelRepLegal(String folio, short serial) {
        Query q = mia.createQuery("SELECT r FROM RepLegalProyecto r WHERE r.repLegalProyectoPK.folioProyecto = :folio and r.repLegalProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return (RepLegalProyecto) q.getResultList().get(0);
    }

    public RespTecProyecto getRespTec(String folio, short serial) {
        Query q = mia.createQuery("SELECT r FROM RespTecProyecto r WHERE r.respTecProyectoPK.folioProyecto = :folio and r.respTecProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return (RespTecProyecto) q.getResultList().get(0);
    }

    public void eliminarResTec(String folio, short serial) {
        mia.getTransaction().begin();
        Query q = mia.createQuery("DELETE FROM RespTecProyecto r WHERE r.respTecProyectoPK.folioProyecto = :folio and r.respTecProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        System.out.println("Eliminados : " + q.executeUpdate());
        mia.getTransaction().commit();
    }

    public void elminarRepLegal(String folio, short serial) {
        mia.getTransaction().begin();
        Query q = mia.createQuery("DELETE FROM RepLegalProyecto r WHERE r.repLegalProyectoPK.folioProyecto = :folio and r.repLegalProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        System.out.println("Eliminados : " + q.executeUpdate());
        mia.getTransaction().commit();
    }

    public ActividadEtapa getActividad(String folio, short serial, short etapaId, short actividadEtapaId) {
        Query q = mia.createQuery("SELECT a FROM ActividadEtapa a where a.actividadEtapaPK.folioProyecto = :folio and a.actividadEtapaPK.serialProyecto =:serial and  a.actividadEtapaPK.etapaId = :etapaId and  a.actividadEtapaPK.actividadEtapaId = :actividadEtapaId");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        q.setParameter("etapaId", etapaId);
        q.setParameter("actividadEtapaId", actividadEtapaId);
        return (ActividadEtapa) q.getSingleResult();
    }
    
    public ArchivosProyecto getArchivoEtapa(String folio, Short serial, short capituloId, short subcapituloId, short seccionId, short apartadoId, short id) {
        Query q = mia.createQuery("SELECT e FROM ArchivosProyecto e WHERE e.folioSerial = :folio and e.serialProyecto = :serial and e.capituloId = :capituloId and e.subCapituloId = :subcapituloId and e.seccionId = :seccionId and e.apartadoId = :apartadoId and e.id = :id");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        q.setParameter("capituloId", capituloId);
        q.setParameter("subcapituloId", subcapituloId);
        q.setParameter("seccionId", seccionId);
        q.setParameter("apartadoId", apartadoId);
        q.setParameter("id", id);
        return (ArchivosProyecto) q.getSingleResult();
    }
    
    public short getMaxActividadEtapaProyecto(String folio, Short serial, Short etapaId) {
        Short r = 0;
        try {
            Query q = mia.createQuery("SELECT max(e.actividadEtapaPK.actividadEtapaId) FROM ActividadEtapa e WHERE e.actividadEtapaPK.folioProyecto = :folio and e.actividadEtapaPK.serialProyecto = :serial and e.actividadEtapaPK.etapaId = :etapaId");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("etapaId", etapaId);
            r = (Short) q.getSingleResult();
        } catch (Exception e) {

        }
        if (r == null) {
            r = 0;
        }
        r++;
        return r;
    }
    
    public short getMaxArchivoEtapaProyecto(String folio, Short serial, Short capituloId, Short subcapituloId, Short seccionId, Short apartadoId) {
        Short r = 0;
        try {
            Query q = mia.createQuery("SELECT max(e.id) FROM ArchivosProyecto e WHERE e.folioSerial = :folio and e.serialProyecto = :serial and e.capituloId = :capituloId and e.subCapituloId = :subcapituloId and e.seccionId = :seccionId and e.apartadoId = :apartadoId");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("capituloId", capituloId);
            q.setParameter("subcapituloId", subcapituloId);
            q.setParameter("seccionId", seccionId);
            q.setParameter("apartadoId", apartadoId);
            r = (Short) q.getSingleResult();
        } catch (Exception e) {
        }
        if (r == null) {
            r = 0;
        }
        r++;
        return r;
    }
    
    public short getMaxArchivoDisposicion(String folio, Short serial, Short capituloId, Short subcapituloId, Short seccionId, Short apartadoId) {
        Short r = 0;
        try {
            Query q = mia.createQuery("SELECT max(e.id) FROM ArchivosProyecto e WHERE e.folioSerial = :folio and e.serialProyecto = :serial and e.capituloId = :capituloId and e.subCapituloId = :subcapituloId and e.seccionId = :seccionId and e.apartadoId = :apartadoId");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("capituloId", capituloId);
            q.setParameter("subcapituloId", subcapituloId);
            q.setParameter("seccionId", seccionId);
            q.setParameter("apartadoId", apartadoId);
            r = (Short) q.getSingleResult();
        } catch (Exception e) {
        }
        if (r == null) {
            r = 0;
        }
        r++;
        return r;
    }

    public EtapaProyecto getEtapaProyecto(String folio, Short serial, Short etapaId) {
        Query q = mia.createQuery("SELECT e FROM EtapaProyecto e WHERE e.etapaProyectoPK.folioProyecto = :folio and e.etapaProyectoPK.serialProyecto = :serial and e.etapaProyectoPK.etapaId = :etapaId");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("etapaId", etapaId);
        return (EtapaProyecto) q.getSingleResult();
    }

    public List<EtapaProyecto> getEtapaProyecto(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM EtapaProyecto e WHERE e.etapaProyectoPK.folioProyecto = :folio and e.etapaProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    //AAOC
    public List<EtapaProyecto> getEtapaProyectoUtils(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM EtapaProyecto e WHERE e.etapaProyectoPK.folioProyecto = :folio and e.etapaProyectoPK.serialProyecto = :serial and e.meses>0 and e.anios>0 and e.semanas>0 ");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    @SuppressWarnings("unchecked")
	public List<MedPrevImpactProy> getMedPrevImpactProy(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM MedPrevImpactProy e WHERE e.medPrevImpactProyPK.folioProyecto = :folio and e.medPrevImpactProyPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }
    
	public MedPrevImpactProy getMedPrevImpactProy(String folio, Short serial, Short id) {
        Query q = mia.createQuery("SELECT e FROM MedPrevImpactProy e WHERE e.medPrevImpactProyPK.folioProyecto = :folio and e.medPrevImpactProyPK.serialProyecto = :serial and e.medPrevImpactProyPK.medPrevImpactId = :id");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("id", id);
        return (MedPrevImpactProy) q.getSingleResult();
    }

    @SuppressWarnings("unchecked")
	public List<ActividadEtapa> getActividadesEtapa(String folio, Short serial, Short etapaId) {
    	try{
	        Query q = mia.createQuery("SELECT e FROM ActividadEtapa e WHERE e.actividadEtapaPK.folioProyecto = :folio and e.actividadEtapaPK.serialProyecto = :serial and e.actividadEtapaPK.etapaId = :etapaId");
	        q.setParameter("folio", folio);
	        q.setParameter("serial", serial);
	        q.setParameter("etapaId", etapaId);
	        return q.getResultList();
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return new ArrayList<ActividadEtapa>();
    }
    
    @SuppressWarnings("unchecked")
	public List<ArchivosProyecto> getArchivosProyecto(String folio, Short serial, Short capituloId, Short subcapituloId, Short seccionId, Short apartadoId) {
    	try{
	        Query q = mia.createQuery("SELECT e FROM ArchivosProyecto e "
	        						+ "WHERE e.folioSerial = :folio and e.serialProyecto = :serial "
	        						+ "and e.capituloId = :capituloId "
	        						+ "and e.subCapituloId = :subcapituloId and e.seccionId = :seccionId "
	        						+ "and e.apartadoId = :apartadoId order by e.id");
	        q.setParameter("folio", folio);
	        q.setParameter("serial", serial);
	        q.setParameter("capituloId", capituloId);
	        q.setParameter("subcapituloId", subcapituloId);
	        q.setParameter("seccionId", seccionId);
	        q.setParameter("apartadoId", apartadoId);
	        return q.getResultList();
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return new ArrayList<ArchivosProyecto>();
    }

    @SuppressWarnings("unchecked")
	public List<ActividadEtapa> getActividadesEtapa(String folio, Short serial) {
    	try{
        Query q = mia.createQuery("SELECT e FROM ActividadEtapa e WHERE e.actividadEtapaPK.folioProyecto = :folio and e.actividadEtapaPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    	return new ArrayList<ActividadEtapa>();
    }

    public List<AnpProyecto> getAnpProyecto(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM AnpProyecto e WHERE e.anpProyectoPK.folioProyecto = :folio and e.anpProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }
    
    /**
     * Busca un AnpProyecto por su ID
     * @param anpId
     * @return AnpProyecto encontrado
     */
    public AnpProyecto getAnpProyectoById(String folio, short serial, short anpId) {
    	Query q = mia.createQuery("SELECT p FROM AnpProyecto p WHERE p.anpProyectoPK.folioProyecto = :folio AND p.anpProyectoPK.serialProyecto = :serial AND p.anpProyectoPK.anpId = :anpId");
    	q.setParameter("folio", folio);
    	q.setParameter("serial", serial);
    	q.setParameter("anpId", anpId);
    	List<AnpProyecto> anps = q.getResultList();
    	if (anps != null && !anps.isEmpty()) {
    		return anps.get(0);
    	}
    	return null;
    }
    
    /**
     * Busca un ArticuloReglaAnp por su ID
     * @param anpId
     * @return ArticuloReglaAnp encontrado
     */
    public ArticuloReglaAnp getArticuloRegla(String folio, short serial, String tipo, short articuloId) {
    	Query q = mia.createQuery("SELECT r FROM ArticuloReglaAnp r WHERE r.articuloReglaAnpPK.folioProyecto = :folio and r.articuloReglaAnpPK.serialProyecto = :serial and r.anpArticuloRegla = :tipo and r.articuloReglaAnpPK.anpArtRegId = :articuloId");
    	q.setParameter("folio", folio);
    	q.setParameter("serial", serial);
    	q.setParameter("tipo", tipo);
    	q.setParameter("articuloId", articuloId);
    	List<ArticuloReglaAnp> anps = q.getResultList();
    	if (anps != null && !anps.isEmpty()) {
    		return anps.get(0);
    	}
    	return null;
    }

    public List<InfobioProyecto> getInfoBio(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM InfobioProyecto e WHERE e.infobioProyectoPK.folioProyecto = :folio and e.infobioProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public InfobioProyecto getInfoBio(String folio, Short serial, Short id) {
        Query q = mia.createQuery("SELECT e FROM InfobioProyecto e WHERE e.infobioProyectoPK.folioProyecto = :folio and e.infobioProyectoPK.serialProyecto = :serial and e.infobioProyectoPK.infoBioId = :id");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("id", id);
        return (InfobioProyecto) q.getResultList().get(0);
    }

    public List<InsumosProyecto> getInsumos(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM InsumosProyecto e WHERE e.insumosProyectoPK.folioSerial = :folio and e.insumosProyectoPK.serialProyecto = :serial order by e.insumosProyectoPK.insumosId");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public short getMaxInsumo(String folio, Short serial) {
        Query q = mia.createQuery("SELECT max(e.insumosProyectoPK.insumosId) FROM InsumosProyecto e WHERE e.insumosProyectoPK.folioSerial = :folio and e.insumosProyectoPK.serialProyecto = :serial ");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        Short s = (Short) q.getSingleResult();
        if (s == null) {
            s = 0;
        }
        s++;
        return s;
    }

    public InsumosProyecto getInsumo(short insumoId) {
		Query q=mia.createQuery("SELECT a FROM InsumosProyecto a WHERE a.insumosProyectoPK.insumosId=:insumo");
		q.setParameter("insumo", insumoId);
		return (InsumosProyecto)q.getSingleResult();
	}	
    
    public InsumosProyecto getInsumo(String folio, short serial, short etapaId, short insumoId){
    	Query q=mia.createQuery("SELECT a FROM InsumosProyecto a WHERE a.insumosProyectoPK.folioSerial = :folio and a.insumosProyectoPK.serialProyecto = :serial and "
    							+ "a.catEtapa.etapaId=:etapa and a.insumosProyectoPK.insumosId=:insumo");
    	q.setParameter("folio", folio);
    	q.setParameter("serial", serial);
    	q.setParameter("etapa", etapaId);
    	q.setParameter("insumo", insumoId);
    	return (InsumosProyecto)q.getSingleResult();
    }
    
    public short getMaxSustancia(String folio, Short serial) {
    	try{
	        Query q = mia.createQuery("SELECT max(e.sustanciaProyectoPK.sustProyId) FROM SustanciaProyecto e WHERE e.sustanciaProyectoPK.folioProyecto = :folio and e.sustanciaProyectoPK.serialProyecto = :serial ");
	        q.setParameter("folio", folio);
	        q.setParameter("serial", serial);
	        return (Short) q.getSingleResult();
    	}catch(NullPointerException ex){    		
    	}
    	return 0;
    }

    
    public SustanciaProyecto getSustancia(String folio, short serial, short cid) {
        Query q = mia.createQuery("SELECT c FROM SustanciaProyecto c where c.sustanciaProyectoPK.folioProyecto = :folio and c.sustanciaProyectoPK.serialProyecto =:serial and c.sustanciaProyectoPK.sustProyId = :cid");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("cid", cid);
        return (SustanciaProyecto) q.getSingleResult();
    }
    
    public List<SustanciaProyecto> getSustancias(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM SustanciaProyecto e WHERE e.sustanciaProyectoPK.folioProyecto = :folio and e.sustanciaProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public List<ContaminanteProyecto> getContaminantes(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM ContaminanteProyecto e WHERE e.contaminanteProyectoPK.folioProyecto = :folio and e.contaminanteProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public List<Contaminanteu> getContaminantesU(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM ContaminanteProyecto e WHERE e.contaminanteProyectoPK.folioProyecto = :folio and e.contaminanteProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public List<AccidentesProyecto> getAccidente(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM AccidentesProyecto e WHERE e.accidentesProyectoPK.folioProyecto = :folio and e.accidentesProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }
    
    public AccidentesProyecto getAccidente(String folio, Short serial, short id) {
        Query q = mia.createQuery("SELECT e FROM AccidentesProyecto e WHERE e.accidentesProyectoPK.folioProyecto = :folio and e.accidentesProyectoPK.serialProyecto = :serial and e.accidentesProyectoPK.accidenteId = :id");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("id", id);
        return (AccidentesProyecto) q.getSingleResult();
    }

    public short getMaxContaminante(String folio, Short serial) {
        Query q = mia.createQuery("SELECT max(e.contaminanteProyectoPK.contaminanteId) FROM ContaminanteProyecto e WHERE e.contaminanteProyectoPK.folioProyecto = :folio and e.contaminanteProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return (Short) q.getSingleResult();
    }

    public short getMaxArticulo(String folio, Short serial) {
        Query q = mia.createQuery("SELECT max(e.articuloReglaAnpPK.anpId) FROM ArticuloReglaAnp e WHERE e.articuloReglaAnpPK.folioProyecto = :folio and e.articuloReglaAnpPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return (Short) q.getSingleResult();
    }

    public short getMaxAccidente(String folio, Short serial) {
        Short max = 0;
        try {
            Query q = mia.createQuery("SELECT max(e.accidentesProyectoPK.accidenteId) FROM AccidentesProyecto e WHERE e.accidentesProyectoPK.folioProyecto = :folio and e.accidentesProyectoPK.serialProyecto = :serial");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            max = (Short) q.getSingleResult();
        } catch (Exception e) {
            max = 0;
        }
        if (max == null) {
            max = 0;
        }
        return max;
    }

    public String getAyuda(int vClaveTramite, short nsub, short capitulo, short subCapitulo, short seccion, short apartado) {
        String ayuda = "Mi Ayuda";

        //--Consulta para el capítulo--
        if (subCapitulo == -1 && seccion == -1 && apartado == -1) {
            System.out.println("capitulo " + capitulo);
            System.out.println("nsub " + nsub);
            Query q = mia.createQuery("SELECT c from CatCapitulo c where c.catCapituloPK.idTramite = :cTramite and c.catCapituloPK.capituloId = :cap and c.catCapituloPK.nsub = :nsub ");
            q.setParameter("cTramite", vClaveTramite);
            q.setParameter("cap", capitulo);
            q.setParameter("nsub", nsub);
            CatCapitulo c = (CatCapitulo) q.getSingleResult();
            ayuda = c.getCapituloAyuda();
        }

        //--Consulta para el SubCapítulo--
        if (subCapitulo > -1 && seccion == -1 && apartado == -1) {
            System.out.println("subcapitulo");
            Query q = mia.createQuery("SELECT c from CatSubcapitulo c where c.catSubcapituloPK.idTramite = :cTramite and  c.catSubcapituloPK.capituloId = :cap and c.catSubcapituloPK.nsub = :nsub and c.catSubcapituloPK.subcapituloId = :sub");
            q.setParameter("cTramite", vClaveTramite);
            q.setParameter("cap", capitulo);
            q.setParameter("nsub", nsub);
            q.setParameter("sub", subCapitulo);
            CatSubcapitulo c = (CatSubcapitulo) q.getSingleResult();
            ayuda = c.getSupcapituloAyuda();
        }

        //--Consulta para el Sección--
        if (subCapitulo > -1 && seccion > -1 && apartado == -1) {
            System.out.println("Seccion - -- --");
            Query q = mia.createQuery("SELECT c from CatSeccion c where c.catSeccionPK.idTramite = :cTramite and c.catSeccionPK.capituloId = :cap and c.catSeccionPK.nsub = :nsub and c.catSeccionPK.subcapituloId = :sub and c.catSeccionPK.seccionId = :sec");
            q.setParameter("cTramite", vClaveTramite);
            q.setParameter("cap", capitulo);
            q.setParameter("nsub", nsub);
            q.setParameter("sub", subCapitulo);
            q.setParameter("sec", seccion);
            CatSeccion c = (CatSeccion) q.getSingleResult();
            ayuda = c.getSeccionAyuda();
        }

        //--Consulta para el Apartado--
        if (subCapitulo > -1 && seccion > -1 && apartado > -1) {
            System.out.println("Apartado");
            Query q = mia.createQuery("SELECT c from CatApartado c where c.catApartadoPK.idTramite = :cTramite and c.catApartadoPK.capituloId = :cap and c.catApartadoPK.nsub = :nsub and c.catApartadoPK.subcapituloId = :sub and c.catApartadoPK.seccionId = :sec and c.catApartadoPK.apartadoId = :apar");
            q.setParameter("cTramite", vClaveTramite);
            q.setParameter("cap", capitulo);
            q.setParameter("nsub", nsub);
            q.setParameter("sub", subCapitulo);
            q.setParameter("sec", seccion);
            q.setParameter("apar", apartado);
            CatApartado c = (CatApartado) q.getSingleResult();
            ayuda = c.getApartadoAyuda();
            System.out.println(c.getApartadoAyuda());
        }
        mia.clear();
        return ayuda;
    }

    //<editor-fold defaultstate="collapsed" desc="Consulta de CatDerechos">
    /**
     *
     * @param valor
     * @return
     */
    public List<Object[]> tablab(String valor) {
        //Query q = emfBitacora.createNativeQuery("select D.ID_NIVEL, D.MONTO, D.LIMITE_INFERIOR, D.LIMITE_SUPERIOR from CAT_DERECHOS D where D.ID_ESTUDIO =:VALOR");
        Query q = mia.createQuery("select a.catDerechosPK.idNivel, a.monto,a.limiteInferior, a.limiteSuperior from CatDerechos a where a.catDerechosPK.idEstudio =:valor");
        q.setParameter("valor", valor);
        List<Object[]> l = q.getResultList();
        return l;
    }//</editor-fold>

    public void guardar(Object object) throws Exception {
        try {
            mia.getTransaction().begin();
            mia.persist(object);
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

    
    public void persist(Object object) throws Exception {
        try {
            System.out.println("Iniciando Transaccion!");            
            mia.getTransaction().begin();
            System.out.println("Persistiendo datos!");
            mia.persist(object);
            System.out.println("Commit!");
            mia.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Rollback!");
            mia.getTransaction().rollback();
            throw e;
        } finally {
            //System.out.println("Cerrando conexión!");
            //mia.close();
        }
    }

    public void merge(Object object) throws Exception {
        try {
            mia.getTransaction().begin();
            object = mia.merge(object);
            mia.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

   public void eliminarServicios(List<ServicioProyecto>lista,Proyecto proyecto) throws Exception{
        try {
            
            mia.getTransaction().begin();
            ServicioProyecto sp=null;
            Short idt=0;
            
            if(!lista.isEmpty()){
                for (ServicioProyecto servicio:lista) {
                     idt=servicio.getServicioProyectoPK().getServicioProyId();
                     sp = new ServicioProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),idt );
                     mia.detach(sp);
                }
                mia.getTransaction().commit();
            }
        } catch (Exception e) {
             mia.getTransaction().rollback();
            throw e;
        }
    }
    public void eliminar(Object object) throws Exception {
        try {
            mia.getTransaction().begin();
            mia.detach(object);
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

    public void eliminarActividadEtapa(String folio, Short serial, Short eta, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM ActividadEtapa c where c.actividadEtapaPK.folioProyecto = :folio and c.actividadEtapaPK.serialProyecto = :serial and c.actividadEtapaPK.actividadEtapaId = :spi and c.actividadEtapaPK.etapaId = :etapa");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("spi", id);
            q.setParameter("etapa", eta);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    public short countExplosivos(String folio, short serial, short etapaId, short actividadEtapaId){
    	Query q = mia.createQuery("SELECT COUNT(a) FROM ExplosivoActividadEtapa a where a.explosivoActividadEtapaPK.folioProyecto = :folio and a.explosivoActividadEtapaPK.serialProyecto =:serial and  a.explosivoActividadEtapaPK.etapaId = :etapaId and  a.explosivoActividadEtapaPK.actividadEtapaId = :actividadEtapaId");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("etapaId", etapaId);
        q.setParameter("actividadEtapaId", actividadEtapaId);
        Long res = (Long) q.getSingleResult();
        return res.shortValue();
    }
    
    public void eliminarActividades(String folio, short serial, short etapaId, short actividadEtapaId) throws Exception {
    	System.out.println("Eliminando");
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM ActividadEtapa a where a.actividadEtapaPK.folioProyecto = :folio and a.actividadEtapaPK.serialProyecto =:serial and  a.actividadEtapaPK.etapaId = :etapaId and  a.actividadEtapaPK.actividadEtapaId = :actividadEtapaId");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);            
            q.setParameter("etapaId", etapaId);
            q.setParameter("actividadEtapaId", actividadEtapaId);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    public void eliminarArchivosEtapas(String folio, short serial, short capituloId, short subcapituloId, short seccionId, short apartadoId, short id, short seqId) throws Exception {
    	System.out.println("Eliminando");
        try {
            mia.getTransaction().begin();
            
            //Eliminacion de Archivos en FileSystem
            Query q1 = mia.createQuery("SELECT a FROM ArchivosProyecto a WHERE a.seqId = :seqId");
            q1.setParameter("seqId", seqId);
            ArchivosProyecto archivoProy = (ArchivosProyecto) q1.getSingleResult();
        	eliminaArchivoRemoto(archivoProy.getUrl());
        	System.out.println("Archivo eliminado del sistema: " + archivoProy.getUrl());
        	
            Query q = mia.createQuery("DELETE FROM ArchivosProyecto e WHERE e.folioSerial = :folio and e.serialProyecto = :serial and e.capituloId = :capituloId and e.subCapituloId = :subcapituloId and e.seccionId = :seccionId and e.apartadoId = :apartadoId and e.id = :id");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);            
            q.setParameter("capituloId", capituloId);
            q.setParameter("subcapituloId", subcapituloId);
            q.setParameter("seccionId", seccionId);
            q.setParameter("apartadoId", apartadoId);
            q.setParameter("id", id);
            q.executeUpdate();
            mia.getTransaction().commit();
            
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

    public void eliminarServicio(short servicioproyid) throws Exception {
        try {
            mia.getTransaction().begin();
            //Query q = mia.createQuery("DELETE FROM ServicioProyecto c where c.servicioProyectoPK.folioProyecto = :folio and c.servicioProyectoPK.serialProyecto = :serial and c.servicioProyectoPK.servicioProyId = :spi and c.servicioProyectoPK.etapaId = :etapa");
            Query q = mia.createQuery("DELETE FROM ServicioProyecto c where c.servicioProyectoPK.servicioProyId = :servicioproyid");
//            q.setParameter("folio", folio);
//            q.setParameter("serial", serial);
//            q.setParameter("spi", id);
//            q.setParameter("etapa", eta);
            q.setParameter("servicioproyid", servicioproyid);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    public void eliminarServicioByFolio(String folio,short serial, short servicioproyid) throws Exception {
        try {
            mia.getTransaction().begin();
            //Query q = mia.createQuery("DELETE FROM ServicioProyecto c where c.servicioProyectoPK.folioProyecto = :folio and c.servicioProyectoPK.serialProyecto = :serial and c.servicioProyectoPK.servicioProyId = :spi and c.servicioProyectoPK.etapaId = :etapa");
            Query q = mia.createQuery("DELETE FROM ServicioProyecto c where c.servicioProyectoPK.folioProyecto =:folio and c.servicioProyectoPK.serialProyecto =:serial and c.servicioProyectoPK.servicioProyId = :servicioproyid");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
//            q.setParameter("spi", id);
//            q.setParameter("etapa", eta);
            q.setParameter("servicioproyid", servicioproyid);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

    public void eliminarInfoBiotec(String folioProyecto, Short serialProyecto, Short infoBioId) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM InfobioProyecto i where i.infobioProyectoPK.folioProyecto = :folioProyecto and i.infobioProyectoPK.serialProyecto = :serialProyecto and i.infobioProyectoPK.infoBioId = :infoBioId");
            q.setParameter("folioProyecto", folioProyecto);
            q.setParameter("serialProyecto", serialProyecto);
            q.setParameter("infoBioId", infoBioId);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
            mia.close();
        }
    }

    public void eliminarMedPrevImpactProy(String folio, Short serial, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM MedPrevImpactProy c where c.medPrevImpactProyPK.folioProyecto = :folio and c.medPrevImpactProyPK.serialProyecto = :serial and c.medPrevImpactProyPK.medPrevImpactId = :cid");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

    public void eliminarImpacto(String folio, Short serial, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM ImpacAmbProyecto c where c.impacAmbProyectoPK.folioProyecto = :folio and c.impacAmbProyectoPK.serialProyecto = :serial and c.impacAmbProyectoPK.impacAmbproyId = :cid");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

    public void eliminarCriterio(String folio, Short serial, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM CriteriosProyecto c where c.criteriosProyectoPK.folioProyecto = :folio and c.criteriosProyectoPK.serialProyecto = :serial and c.criteriosProyectoPK.criterioProyId = :cid");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    public void eliminarCriterio(short criterioproyid) throws Exception {
    	System.out.println("Eliminando");
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM CriteriosProyecto c where c.criteriosProyectoPK.criterioProyId = :criteriosproyid");
            q.setParameter("criteriosproyid", criterioproyid);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

    public void eliminarArticuloReglaAnp(String folio, Short serial, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM ArticuloReglaAnp c where c.articuloReglaAnpPK.folioProyecto = :folio and c.articuloReglaAnpPK.serialProyecto = :serial and c.articuloReglaAnpPK.anpArtRegId = :cid");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

    public void eliminarPdumProyecto(String folio, Short serial, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM PdumProyecto c where c.pdumProyectoPK.folioProyecto = :folio and c.pdumProyectoPK.serialProyecto = :serial and c.pdumProyectoPK.pdumId =:cid");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    /**
     * Actualiza el campo proyPduAplica del proyecto
     * @param folioProyecto
     * @param serialProyecto
     * @param proyPduAplica
     * @throws Exception
     */
    public void actualizarPDURespuestaAplica(String folioProyecto, short serialProyecto, String proyPduAplica) throws Exception {
    	try {
    		System.out.println("Actualizando respuesta, fol: " + folioProyecto + " serial: " + serialProyecto);
    		mia.getTransaction().begin();
            Query q = mia.createQuery("UPDATE Proyecto SET proyPduAplica = :proyPduAplica "
                    + "where proyectoPK.folioProyecto = :folio "
                    + "and proyectoPK.serialProyecto = :serial ");
            q.setParameter("proyPduAplica", proyPduAplica);
            q.setParameter("folio", folioProyecto);
            q.setParameter("serial", serialProyecto);
            q.executeUpdate();
            System.out.println("Update terminado");
            mia.getTransaction().commit();
		} catch (Exception e) {
			mia.getTransaction().rollback();
			throw e;
		}
    }
    
    /**
     * Actualiza el campo proyAnpAplica del proyecto
     * @param folioProyecto
     * @param serialProyecto
     * @param proyAnpAplica
     * @throws Exception
     */
    public void actualizarANPRespuestaAplica(String folioProyecto, short serialProyecto, String proyAnpAplica) throws Exception {
    	try {
    		System.out.println("Actualizando respuesta, fol: " + folioProyecto + " serial: " + serialProyecto);
    		mia.getTransaction().begin();
            Query q = mia.createQuery("UPDATE Proyecto SET proyAnpAplica = :proyAnpAplica "
                    + "where proyectoPK.folioProyecto = :folio "
                    + "and proyectoPK.serialProyecto = :serial ");
            q.setParameter("proyAnpAplica", proyAnpAplica);
            q.setParameter("folio", folioProyecto);
            q.setParameter("serial", serialProyecto);
            q.executeUpdate();
            System.out.println("Update terminado");
            mia.getTransaction().commit();
		} catch (Exception e) {
			mia.getTransaction().rollback();
			throw e;
		}
    }
    
    /**
     * Elimina un AnpProyecto por su id
     * @param folio
     * @param serial
     * @param id AnpProyecto
     * @throws Exception
     */
    public void eliminarAnpProyecto(String folio, Short serial, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM AnpProyecto c where c.anpProyectoPK.folioProyecto = :folio and c.anpProyectoPK.serialProyecto = :serial and c.anpProyectoPK.anpId =:cid");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    /**
     * Elimina un ArticuloReglaAnp por su id
     * @param folio
     * @param serial
     * @param id
     * @throws Exception
     */
    public void eliminarAnpProyectoRegla(String folio, Short serial, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM ArticuloReglaAnp c where c.articuloReglaAnpPK.folioProyecto = :folio and c.articuloReglaAnpPK.serialProyecto = :serial and c.articuloReglaAnpPK.anpArtRegId = :cid");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    /**
     * Busca un registro de Otra Disposición para un Folio y Serial específicos
     * @param folio Folio del Trámite/Proyecto
     * @param serial Serial del Trámite/Proyecto
     * @param disposicionId
     * @return
     */
    public DisposicionProyecto getDisposicion(String folio, Short serial, short disposicionId) {
    	Query q = mia.createQuery("SELECT d FROM DisposicionProyecto d WHERE d.disposicionProyectoPK.folioProyecto=:folio" + 
    			" AND d.disposicionProyectoPK.serialProyecto=:serial AND d.disposicionProyectoPK.disposicionProyId=:dispId");
    	q.setParameter("folio", folio);
        q.setParameter("serial", serial);
    	q.setParameter("dispId", disposicionId);
    	
    	List<DisposicionProyecto> disposiciones = q.getResultList();
    	if (disposiciones != null && !disposiciones.isEmpty()) {
    		return disposiciones.get(0);
    	}
    	return null;
    }
    
    /**
     * Elimina un registro de Otra Disposición para un Folio y Serial específicos
     * @param folio Folio del Trámite/Proyecto
     * @param serial Serial del Trámite/Proyecto
     * @param disposicionId Identificador de registro de Otra Disposición
     * @throws Exception
     */
    public void eliminarOtraDisposicion(String folio, Short serial, Short disposicionId) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM DisposicionProyecto d WHERE d.disposicionProyectoPK.folioProyecto=:folio" + 
            		" AND d.disposicionProyectoPK.serialProyecto=:serial AND d.disposicionProyectoPK.disposicionProyId=:dispId");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("dispId", disposicionId);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
        }
    }

    public void eliminarCaracPartProy(String folio, Short serial, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM CaracPartProy c where c.caracPartProyPK.folioProyecto = :folio and c.caracPartProyPK.serialProyecto = :serial and c.caracPartProyPK.caracteristicaId = :cid");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

    public void eliminarInsumos(String folio, Short serial) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("SELECT c FROM InsumosProyecto c where c.insumosProyectoPK.folioSerial = :folio and c.insumosProyectoPK.serialProyecto = :serial");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            List<InsumosProyecto> i = q.getResultList();

            for (InsumosProyecto p : i) {
                mia.remove(p);
            }
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

    public void eliminarInsumo(String folio, Short serial, Short id, Short id2) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM InsumosProyecto c where c.insumosProyectoPK.folioSerial = :folio and c.insumosProyectoPK.serialProyecto = :serial and c.insumosProyectoPK.etapaId = :cid and c.insumosProyectoPK.insumosId = :cid2");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.setParameter("cid2", id2);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    public void eliminarSustancia(String folio, Short serial, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            
            //Adjunto
            Query q = mia.createNativeQuery("SELECT anexo_url FROM sustancia_anexo where folio_proyecto=:folio and serial_proyecto=:serial and sustancia_proy_id=:idt");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("idt", id);
            ArrayList<Object> lst = (ArrayList<Object>)q.getResultList();
            if (lst != null && !lst.isEmpty()){
                String anexoUrl = (String)lst.get(0);
                
                eliminaArchivoRemoto(anexoUrl);
            }
            
            //Sustancia
            q = mia.createQuery("DELETE FROM SustanciaProyecto c where c.sustanciaProyectoPK.folioProyecto = :folio and c.sustanciaProyectoPK.serialProyecto = :serial and c.sustanciaProyectoPK.sustProyId = :cid");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            
            
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    private void eliminaArchivoRemoto(String rutaArch) {
        try {
            File file = new File(rutaArch);
            Files.deleteIfExists(file.toPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // ** SINATEC
    public Vexdatosusuario getPromovente(String folio) {
        Query q = sin.createQuery("SELECT v FROM Vexdatosusuario v WHERE v.bgtramiteid = :folio");
        q.setParameter("folio", Integer.valueOf(folio));
        List<Vexdatosusuario> l = q.getResultList();

        return l.get(0);
    }
    
    public String obtenerEntidadPromovente(Vexdatosusuario promovente) {
        String entidad = "";
        Query qry = cat.createNativeQuery("SELECT EF.nombre_entidad_federativa FROM catalogos.entidad_federativa EF WHERE to_number(EF.id_entidad_federativa) = :idEntidad");
        qry.setParameter("idEntidad", Integer.parseInt(promovente.getVcidEntidadFederativaLk()));
        
        entidad = (String)qry.getSingleResult();
        
        return entidad;
        
    }

    public Vexdatosusuariorep getRepLeg(String curp) {
        Query q = sin.createQuery("SELECT v FROM Vexdatosusuariorep v where v.vccurp = :curp");
        q.setParameter("curp", curp);
        List l = q.getResultList();
        return (Vexdatosusuariorep) l.get(0);
    }

    public List<Vexdatosusuariorep> datosPrepLeg(Integer idTramite) {
        Query q = sin.createQuery("SELECT DISTINCT a FROM Vexdatosusuariorep a WHERE a.bgtramiteid = :idTramite");
        q.setParameter("idTramite", idTramite);
        List l = q.getResultList();

        return l;
    }

    public String tipotramite(Integer idTramite) {
        String tipoTramite = "";
        try {
            Query q = sin.createQuery("SELECT v.vcclaveTramite FROM Vexdatostramite v WHERE v.bgtramiteid= :idTramite");
            q.setParameter("idTramite", idTramite);

            tipoTramite = q.getSingleResult().toString();

        } catch (Exception e) {
            System.out.println("eror " + e);
        }
        return tipoTramite;
    }

    public String nombreSubsector(String folioProyecto) {
        String nombreSubsector = "";
        try {
            Query q = mia.createNativeQuery("SELECT NSUB.SUBSECTOR FROM DGIRA_MIAE2.PROYECTO P, CATALOGOS.SUBSECTOR_PROYECTO NSUB WHERE P.FOLIO_PROYECTO ='" + folioProyecto + "' AND P.NSUB = NSUB.NSUB");

            nombreSubsector = q.getSingleResult().toString();

        } catch (Exception e) {
            System.out.println("eror " + e);
        }
        return nombreSubsector;
    }

    //jsr: tengo una idea de lo que estoy haciendo: solo una.
    public String nombreRama(String folioProyecto) {
        String nombreRama = "";
        try {
            Query q = mia.createNativeQuery("SELECT NRAMA.RAMA FROM DGIRA_MIAE2.PROYECTO P, CATALOGOS.RAMA_PROYECTO NRAMA WHERE P.FOLIO_PROYECTO ='" + folioProyecto + "' AND P.NRAMA = NRAMA.NRAMA");
            nombreRama = q.getSingleResult().toString();

        } catch (Exception e) {
            System.out.println("eror " + e);
        }
        return nombreRama;
    }

    public String nombreTipoProy(String folioProyecto) {
        String nombreTipoProy = "";
        try {
            Query q = mia.createNativeQuery("SELECT NTIPO.TIPO_PROYECTO FROM DGIRA_MIAE2.PROYECTO P, CATALOGOS.TIPO_PROYECTO NTIPO WHERE P.FOLIO_PROYECTO ='" + folioProyecto + "' AND P.NTIPO = NTIPO.NTIPO");
            nombreTipoProy = q.getSingleResult().toString();

        } catch (Exception e) {
            System.out.println("eror " + e);
        }
        return nombreTipoProy;
    }
    
    
    public Integer claveTramite(Integer idTramite) {
        Integer claveTramite = 0;
        try {
            Query q = sin.createQuery("SELECT v.bgidTramiteLk FROM Vexdatostramite v WHERE v.bgtramiteid= :idTramite");
            q.setParameter("idTramite", idTramite);
            claveTramite = (Integer) q.getSingleResult();
        } catch (Exception e) {
            System.out.println("eror " + e);
        }
        return claveTramite;
    }

    // ** CATALOGOS
    public List<CatLey> getCatLeyEstatal() {
        Query q = mia.createQuery("SELECT c FROM CatLey c order by c.leyNombre");
        return q.getResultList();
    }

    public List<CatLey> getCatLeyFederal() {
        Query q = mia.createQuery("SELECT c FROM CatLey c order by c.leyNombre");
        return q.getResultList();
    }

    public List<CatTipoContaminante> getCatTipoContaminante() {
        Query q = mia.createQuery("SELECT c FROM CatTipoContaminante c order by c.tipoContaminanteDescripcion");
        return q.getResultList();
    }

    public List<CatTipoImpacto> getCatTipoImpacto() {
        Query q = mia.createQuery("SELECT c FROM CatTipoImpacto c order by c.tipoImpactoDescripcion");
        return q.getResultList();
    }

    public List<CatEtapa> getCatEtapaProyecto() {
        Query q = mia.createQuery("SELECT c FROM CatEtapa c order by c.etapaId");
        return q.getResultList();
    }

    public List<CatContaminante> getContaminantes(Short id) {
        Query q = mia.createQuery("SELECT c FROM CatContaminante c where c.tipoContaminanteId = :tipo order by c.contaminanteNombre");
        q.setParameter("tipo", new CatTipoContaminante(id));
        return q.getResultList();
    }

    public List<CatContaminante> getContaminantes() {
        Query q = mia.createQuery("SELECT c FROM CatContaminante c order by c.contaminanteNombre");
        return q.getResultList();
    }
    
    

    public CatVialidad getVialidad(String tipoVialidad) {
        return (CatVialidad) cat.find(CatVialidad.class, Short.valueOf(tipoVialidad));
    }

    public CatTipoAsen getTipoAsentamiento(String tipoAsen) {
        return (CatTipoAsen) cat.find(CatTipoAsen.class, Short.valueOf(tipoAsen));
    }

    public SectorProyecto getSectorProyecto(Short nsec) {
        return (SectorProyecto) cat.find(SectorProyecto.class, nsec);
    }

    public SubsectorProyecto getSubSector(Short nsub) {
        return (SubsectorProyecto) cat.find(SubsectorProyecto.class, nsub);
    }

    public List<SectorProyecto> getCatSector() {
        Query q = cat.createQuery("SELECT c FROM SectorProyecto c order by c.sector");
        return q.getResultList();
    }

    public List<SubsectorProyecto> getCatSubSector() {
        Query q = cat.createQuery("SELECT c FROM SubsectorProyecto c order by c.subsector");
        return q.getResultList();
    }

    public List<RamaProyecto> getCatRama(Short sub) {
        Query q = cat.createQuery("SELECT c FROM RamaProyecto c WHERE c.nsub = :sub order by c.rama");
        q.setParameter("sub", sub);
        return q.getResultList();
    }

    public List<TipoProyecto> getCatTipoProyecto(Short rama) {
        Query q = cat.createQuery("SELECT c FROM TipoProyecto c WHERE c.nrama = :rama order by c.tipoProyecto");
        q.setParameter("rama", rama);
        return q.getResultList();
    }

    public List<CatTipoAsen> getTipoAsentamientos() {
        Query q = cat.createQuery("SELECT C FROM CatTipoAsen C order by C.nombre");
        return q.getResultList();
    }

    public List<CatVialidad> getTipoVialidad() {
        Query q = cat.createQuery("SELECT C FROM CatVialidad C order by C.descripcion");
        return q.getResultList();
    }

    public List<CatCriterio> getCriterios() {
        Query q = mia.createQuery("SELECT C FROM CatCriterio C order by C.criterioNombre");
        return q.getResultList();
    }
    
    public List<CatEstEsp> getEstudiosEspeciales()
    {
    	 Query q = mia.createQuery("SELECT c FROM CatEstEsp c");
    	 return q.getResultList();
    }
    
    public List<CatPoet> getPoet() {
        Query q = mia.createQuery("SELECT P FROM CatPoet P order by P.poetId");
        return q.getResultList();
    }
    
//    public PoetmProyecto getPoet(String folio, short serial, short id) {
//        Query q = mia.createQuery("SELECT c FROM PoetmProyecto c where c.poetmProyectoPK.folioProyecto = :folio and c.poetmProyectoPK.serialProyecto = :serial and c.poetmProyectoPK.poetmId = :cid ");
//        q.setParameter("serial", serial);
//        q.setParameter("folio", folio);
//        q.setParameter("cid", id);
//        return (PoetmProyecto) q.getSingleResult();
//    }
    
    public PoetmProyecto getPoet(String folio, short serial, short id) {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
    	Query q = mia.createQuery("SELECT c FROM PoetmProyecto c where c.poetmProyectoPK.folioProyecto = :folio and c.poetmProyectoPK.serialProyecto = :serial and c.poetmProyectoPK.poetmId = :cid ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        q.setParameter("cid", id);
        return (PoetmProyecto) q.getSingleResult();
    }    

    public List<ImpacAmbProyecto> getImpacAmbProyecto(String folio, Short serial) {
    	EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        Query q = mia.createQuery("SELECT e FROM ImpacAmbProyecto e WHERE e.impacAmbProyectoPK.folioProyecto = :folio and e.impacAmbProyectoPK.serialProyecto = :serial order by e.impacAmbProyectoPK.impacAmbproyId");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }
    
    public ImpacAmbProyecto getImpacAmbProyecto(String folio, Short serial, Short id) {
    	EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        Query q = mia.createQuery("SELECT e FROM ImpacAmbProyecto e WHERE e.impacAmbProyectoPK.folioProyecto = :folio and e.impacAmbProyectoPK.serialProyecto = :serial and e.impacAmbProyectoPK.impacAmbproyId = :id");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("id", id);
        return (ImpacAmbProyecto) q.getSingleResult();
    }
    
    public Short getMaxInfoBio(String folio, short serial) {
        Query q = mia.createQuery("SELECT max(a.infobioProyectoPK.infoBioId) FROM InfobioProyecto a where a.infobioProyectoPK.folioProyecto = :folio and a.infobioProyectoPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Short) q.getSingleResult();
    }

    public Short getMaxCarac(String folio, short serial) {
        Query q = mia.createQuery("SELECT max(a.caracPartProyPK.caracteristicaId) FROM CaracPartProy a where a.caracPartProyPK.folioProyecto = :folio and a.caracPartProyPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Short) q.getSingleResult();
    }

    public Short getMaxCriterio(String folio, short serial) {
        Query q = mia.createQuery("SELECT max(a.criteriosProyectoPK.criterioProyId) FROM CriteriosProyecto a where a.criteriosProyectoPK.folioProyecto = :folio and a.criteriosProyectoPK.serialProyecto =:serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Short) q.getSingleResult();
    }
    
    public Short getMaxServicio(String folio, short serial) {
        Query q = mia.createQuery("SELECT max(a.servicioProyectoPK.servicioProyId) FROM ServicioProyecto a where a.servicioProyectoPK.folioProyecto = :folio and a.servicioProyectoPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Short) q.getSingleResult();
    }

    public Short getMaxPrediocolinProy(String folio, short serial) {
        Query q = mia.createQuery("SELECT max(a.prediocolinProyPK.predioColinId) FROM PrediocolinProy a where a.prediocolinProyPK.folioProyecto = :folio and a.prediocolinProyPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Short) q.getSingleResult();
    }
    
    public Short getMaxCuerpoAguaProyecto(String folio, short serial) {
        Query q = mia.createQuery("SELECT max(a.cuerposAguaProyectoPK.cuerposAguaId) FROM CuerposAguaProyecto a where a.cuerposAguaProyectoPK.folioProyecto = :folio and a.cuerposAguaProyectoPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Short) q.getSingleResult();
    }
    
    public PrediocolinProy getPrediocolinProy(String folio, short serial, short id) {
        Query q = mia.createQuery("SELECT a FROM PrediocolinProy a where a.prediocolinProyPK.folioProyecto = :folio and a.prediocolinProyPK.serialProyecto = :serial and a.prediocolinProyPK.predioColinId = :id ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        q.setParameter("id", id);
        return (PrediocolinProy) q.getSingleResult();
    }
    
    public CuerposAguaProyecto getCuerpoAgua(String folio, short serial, short id) {
        Query q = mia.createQuery("SELECT a FROM CuerposAguaProyecto a where a.cuerposAguaProyectoPK.folioProyecto = :folio and a.cuerposAguaProyectoPK.serialProyecto = :serial and a.cuerposAguaProyectoPK.cuerposAguaId = :id order by a.cuerposAguaProyectoPK.cuerposAguaId");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        q.setParameter("id", id);
        return (CuerposAguaProyecto) q.getSingleResult();
    }

    public List<CatSustanciaAltamRiesgosa> getSustanciaAltaRiesgosa() {
        Query q = mia.createQuery("SELECT c FROM CatSustanciaAltamRiesgosa c order by c.sustanciaDescripcion");
        return q.getResultList();
    }

    public List<CatServicio> getServicios() {
        Query q = mia.createQuery("SELECT c FROM CatServicio c order by c.servicioNombre");
        return q.getResultList();
    }

    public List<CatEtapa> getEtapa() {
        Query q = mia.createQuery("SELECT c FROM CatEtapa c order by c.etapaId");
        return q.getResultList();
    }

    public List<CatNaturaleza> getCatNaturaleza() {
        Query q = mia.createQuery("SELECT c FROM CatNaturaleza c order by c.naturalezaDescripcion");
        return q.getResultList();
    }

    public List<CatTemporalidad> getCatTemporalidad() {
        Query q = mia.createQuery("SELECT c FROM CatTemporalidad c order by c.temporalidadDescripcion");
        return q.getResultList();
    }

    public List<CatUnidadMedida> getCatUnidadMedida() {
        Query q = cat.createQuery("SELECT c FROM CatUnidadMedida c order by c.ctunDesc");
        return q.getResultList();
    }
    
    
    public CatUnidadMedida getCatUnidadMedidaPorClave(short ctunClve) {
        Query q = cat.createQuery("SELECT c FROM CatUnidadMedida c WHERE c.ctunClve = :ctunClve");
        q.setParameter("ctunClve", ctunClve);
        return (CatUnidadMedida)q.getSingleResult();
    }
    

    public CatUnidadMedida getCatUnidadMedida(Short ctunClve) {
   
    	CatUnidadMedida catUnidadMedida= new CatUnidadMedida();
        Query q = cat.createQuery("SELECT c FROM CatUnidadMedida c where c.ctunClve=:ctunClve order by c.ctunDesc");
        q.setParameter("ctunClve", ctunClve);
        catUnidadMedida=(CatUnidadMedida)q.getSingleResult();
        return catUnidadMedida;
    	
    }
    
    /*public List<CatEstEsp> getCatEstEsp(Short nsub) {
     Query q = mia.createQuery("SELECT e FROM CatEstudioEspSub c, CatEstEsp e WHERE c.catEstudioEspSubPK.estudioId = e.estudioId and c.catEstudioEspSubPK.nsub = :nsub");
     q.setParameter("nsub", nsub);
     return q.getResultList();
     }*/
    public List<CatEstEsp> getCatEstEsp() {
        Query q = mia.createQuery("SELECT e FROM CatEstEsp e order by e.estudioDescripcion");
        return q.getResultList();
    }

    public List<CriteriosProyecto> getCriterios(String folio, short serial) {
        Query q = mia.createQuery("SELECT a FROM CriteriosProyecto a where a.criteriosProyectoPK.folioProyecto = :folio and a.criteriosProyectoPK.serialProyecto =:serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return q.getResultList();
    }
    
    public List<InversionEtapas> getInversiones(String folio, short serial) {
        Query q = mia.createQuery("SELECT a FROM InversionEtapas a where a.inversionEtapasPK.folioProyecto = :folio and a.inversionEtapasPK.serialProyecto =:serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return q.getResultList();
    }
    
    public InversionEtapas getInversion(String folio, short serial, short inversionEtapaId) {
        Query q = mia.createQuery("SELECT a FROM InversionEtapas a where a.inversionEtapasPK.folioProyecto = :folio and a.inversionEtapasPK.serialProyecto =:serial and  a.inversionEtapasPK.inversionEtapaId = :inversionEtapaId");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        q.setParameter("inversionEtapaId", inversionEtapaId);
        return (InversionEtapas) q.getSingleResult();
    }
    
    public CriteriosProyecto getCriterio(String folio, short serial, short criterioProyId) {
        Query q = mia.createQuery("SELECT a FROM CriteriosProyecto a where a.criteriosProyectoPK.folioProyecto = :folio and a.criteriosProyectoPK.serialProyecto =:serial and  a.criteriosProyectoPK.criterioProyId = :criterioProyId");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        q.setParameter("criterioProyId", criterioProyId);
        return (CriteriosProyecto) q.getSingleResult();
    }

    public List<ServicioProyecto> getServicios(String folio, short serial) {
        Query q = mia.createQuery("SELECT a FROM ServicioProyecto a "
        							+ "where a.servicioProyectoPK.folioProyecto = :folio "
        							+ "and a.servicioProyectoPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return q.getResultList();
    }

    // cap3
    public List<LeyFedEstProyecto> getLeyesFed(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM LeyFedEstProyecto e WHERE e.leyFedEstProyectoPK.folioProyecto = :folio and e.leyFedEstProyectoPK.serialProyecto = :serial and e.leyFeBandera = 'F'");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public List<LeyFedEstProyecto> getLeyesEst(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM LeyFedEstProyecto e WHERE e.leyFedEstProyectoPK.folioProyecto = :folio and e.leyFedEstProyectoPK.serialProyecto = :serial and e.leyFeBandera = 'E'");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    @SuppressWarnings("unchecked")
	public List<ReglamentoProyecto> getReglamentos(String folio, Short serial) {
        try{
    	Query q = mia.createQuery("SELECT e FROM ReglamentoProyecto e WHERE e.reglamentoProyectoPK.folioProyecto = :folio and e.reglamentoProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
        }catch(Exception ex){
        	ex.printStackTrace();
        }
        return new ArrayList<ReglamentoProyecto>(0);
        
    }
    
    public ReglamentoProyecto consList(String folio, Short serial,Short id)
    {
    	Query q = mia.createNamedQuery("ReglamentoProyecto.findByReglamentoProyId");
    	q.setParameter("reglamentoProyId", id);
    	q.setParameter("folioProyecto", folio);
        q.setParameter("serialProyecto",serial);
    	return (ReglamentoProyecto) q.getSingleResult();    	 
    }
    
    public List<ReglamentoProyecto> getReglamento(String folio, Short serial, Short id) {
        Query q = mia.createQuery("SELECT e FROM ReglamentoProyecto e WHERE e.reglamentoProyectoPK.folioProyecto = :folio and e.reglamentoProyectoPK.serialProyecto = :serial and e.reglamentoId = :id");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("id", id);
        return q.getResultList();
    }

    
    
    
    public List<NormaProyecto> getNormas(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM NormaProyecto e WHERE e.normaProyectoPK.folioProyecto = :folio and e.normaProyectoPK.serialProyecto = :serial ORDER BY e.normaProyectoPK.normaProyId ");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }
    
    
    public NormaProyecto getNorma(String folio, short serial, short normaProyId) {
        Query q = mia.createQuery("SELECT a FROM NormaProyecto a where a.cnormaProyectoPK.folioProyecto = :folio and a.normaProyectoPK.serialProyecto =:serial and  a.normaProyectoPK.normaProyId = :normaProyId");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        q.setParameter("normaProyId", normaProyId);
        return (NormaProyecto) q.getSingleResult();
    }
    
    

    public List<Numeral> getNumerales(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM Numeral e WHERE e.numeralPK.folioProyecto = :folio and e.numeralPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public List<Numeral> getNumerales(String folio, Short serial, Short id) {
        Query q = mia.createQuery("SELECT e FROM Numeral e WHERE e.numeralPK.folioProyecto = :folio and e.numeralPK.serialProyecto = :serial and e.numeralPK.normaProyId = :cid");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("cid", id);
        return q.getResultList();
    }

    public List<ConvenioProy> getConvevios(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM ConvenioProy e WHERE e.convenioProyPK.folioProyecto = :folio and e.convenioProyPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public List<DisposicionProyecto> getDisposiciones(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM DisposicionProyecto e WHERE e.disposicionProyectoPK.folioProyecto = :folio and e.disposicionProyectoPK.serialProyecto = :serial order by e.disposicionProyectoPK.disposicionProyId");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public List<FloraProyecto> getFloraProyecto(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM FloraProyecto e WHERE e.floraProyectoPK.folioProyecto = :folio and e.floraProyectoPK.serialProyecto = :serial order by e.floraProyectoPK.floraProyid");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public List<FaunaProyecto> getFaunaProyecto(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM FaunaProyecto e WHERE e.faunaProyectoPK.folioProyecto = :folio and e.faunaProyectoPK.serialProyecto = :serial order by e.faunaProyectoPK.faunaProyid");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public List<PrediocolinProy> getPrediocolinProy(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM PrediocolinProy e WHERE e.prediocolinProyPK.folioProyecto = :folio and e.prediocolinProyPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

	public List<CatReglamento> getCatReglamento() {
        Query q = mia.createQuery("SELECT e FROM CatReglamento e order by e.reglamentoId");
        return q.getResultList();
    }
    
    
    //Obtiene la descrpcion de un reglamento del catalogo
    public String getDescripcionReglamentoString(Short id)
    {
    	 Query q = mia.createQuery("SELECT c.reglamentoDescripcion FROM CatReglamento c WHERE c.reglamentoId = :reglamentoId");
         q.setParameter("reglamentoId", id);
         String c= q.getResultList().toString();
    	return c;
    }
    
    
    public List<CatNorma> getCatNormas() {
        Query q = mia.createQuery("SELECT e FROM CatNorma e ORDER BY e.normaId");
        return q.getResultList();
    }

    public String getNormaDesc(Short id) {
        Query q = mia.createQuery("SELECT e.normaDescripcion FROM CatNorma e WHERE e.normaId = :id order by e.normaDescripcion");
        return (String) q.getSingleResult();
    }

    public List<CatClasificacion> getCatClasificacion() {
        Query q = mia.createQuery("SELECT e FROM CatClasificacion e order by e.clasificacionDescripcion");
        return q.getResultList();
    }
    
    public List<CatClasificacionB> getCatClasificacionB() {
        Query q = mia.createQuery("SELECT e FROM CatClasificacionB e order by e.clasificacionB");
        List<CatClasificacionB> cb = q.getResultList();
        return cb;
    }
    
    public List<CatClasificacionB> getCatClasificacionBById(Short id) {
        Query q = mia.createQuery("SELECT e FROM CatClasificacionB e WHERE e.clasificacionId = :id order by e.clasificacionB");
        q.setParameter("id", id);
        List<CatClasificacionB> cb = q.getResultList();
        return cb;
    }

    public List<CatReferencia> getCatReferencia() {
        Query q = mia.createQuery("SELECT e FROM CatReferencia e order by e.referenciaDescripcion");
        return q.getResultList();
    }

    public List<CatConvenios> getCatConvenios() {
        Query q = mia.createQuery("SELECT e FROM CatConvenios e ORDER by e.convenioDescripcion asc");
        return q.getResultList();
    }

    public Short getMaxLeyFedEst(String folio, short serial) {
        Query q = mia.createQuery("SELECT max(a.leyFedEstProyectoPK.leyFeId) FROM LeyFedEstProyecto a where a.leyFedEstProyectoPK.folioProyecto = :folio and a.leyFedEstProyectoPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Short) q.getSingleResult();
    }

    public Short getMaxConvenios(String folio, short serial) {
        Query q = mia.createQuery("SELECT max(a.convenioProyPK.convenioProyId) FROM ConvenioProy a where a.convenioProyPK.folioProyecto = :folio and a.convenioProyPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Short) q.getSingleResult();
    }

    public ConvenioProy getconvenio(short convenioId) {
		Query q = mia.createQuery("SELECT a FROM ConvenioProy a where a.convenioProyPK.convenioProyId=:convenioId");
		q.setParameter("convenioId", convenioId);
		
		return (ConvenioProy)q.getSingleResult();
	} 
    
    public ConvenioProy getconvenioByFolio(String folio, short serial, short convenioId) {
		Query q = mia.createQuery("SELECT a FROM ConvenioProy a where a.convenioProyPK.folioProyecto = :folio and a.convenioProyPK.serialProyecto = :serial and a.convenioProyPK.convenioProyId = :convenioId");
		q.setParameter("convenioId", convenioId);
		q.setParameter("folio", folio);
		q.setParameter("serial", serial);
		
		return (ConvenioProy)q.getSingleResult();
	} 
    
    public void eliminaConvenio(short convenioProyId) {
		try{ 
			mia.getTransaction().begin();
			Query q = mia.createQuery("DELETE FROM ConvenioProy a WHERE a.convenioProyPK.convenioProyId=:convenioId");
			q.setParameter("convenioId", convenioProyId);
			q.executeUpdate();
			mia.getTransaction().commit();
		}catch(Exception e){
			 mia.getTransaction().rollback();
	            throw e;
		}
	}
    
    public void eliminaConvenioByFolio(String folio, short serial, short convenioProyId) {
		try{ 
			mia.getTransaction().begin();
			Query q = mia.createQuery("DELETE FROM ConvenioProy a WHERE a.convenioProyPK.folioProyecto = :folio and a.convenioProyPK.serialProyecto = :serial and a.convenioProyPK.convenioProyId=:convenioProyId");
			q.setParameter("convenioProyId", convenioProyId);
			q.setParameter("folio", folio);
			q.setParameter("serial", serial);
			q.executeUpdate();
			mia.getTransaction().commit();
		}catch(Exception e){
			 mia.getTransaction().rollback();
	            throw e;
		}
	}
    
    public Short getMaxReglamento(String folio, short serial) {
        Query q = mia.createQuery("SELECT max(a.reglamentoProyectoPK.reglamentoProyId) FROM ReglamentoProyecto a where a.reglamentoProyectoPK.folioProyecto = :folio and a.reglamentoProyectoPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Short) q.getSingleResult();
    }

    public Short getMaxNorma(String folio, short serial) {
        Short max = 0;
        try {
            Query q = mia.createQuery("SELECT max(a.normaProyectoPK.normaProyId) FROM NormaProyecto a where a.normaProyectoPK.folioProyecto = :folio and a.normaProyectoPK.serialProyecto = :serial ");
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            max = (Short) q.getSingleResult();

        } catch (Exception e) {
            max = 0;
        }
        if (max == null) {
            max = 0;
        }
        max++;
        return max;
    }

    public Short getMaxImpacto(String folio, short serial) {
        Query q = mia.createQuery("SELECT max(a.impacAmbProyectoPK.impacAmbproyId) FROM ImpacAmbProyecto a where a.impacAmbProyectoPK.folioProyecto = :folio and a.impacAmbProyectoPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Short) q.getSingleResult();
    }

    public Short getMaxNumeral(String folio, short serial) {
        Short max = 0;
        try {
            Query q = mia.createQuery("SELECT max(a.numeralPK.numeralId) FROM Numeral a where a.numeralPK.folioProyecto = :folio and a.numeralPK.serialProyecto = :serial ");
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            max = (Short) q.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            max = 0;
        }
        if (max == null) {
            max = 0;
        }
        max++;
        return max;
    }

    public Short getMaxGlosario(String folio, short serial) {
        Query q = mia.createQuery("SELECT max(a.glosarioProyectoPK.glosproyId) FROM GlosarioProyecto a where a.glosarioProyectoPK.folioProyecto = :folio and a.glosarioProyectoPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Short) q.getSingleResult();
    }

    public Short getMaxDisposicion(String folio, short serial) {
    	Short maxId = 0;
    	
    	try
    	{
	    	Query q = mia.createQuery("SELECT max(a.disposicionProyectoPK.disposicionProyId) FROM DisposicionProyecto a where a.disposicionProyectoPK.folioProyecto = :folio and a.disposicionProyectoPK.serialProyecto = :serial ");
	        q.setParameter("serial", serial);
	        q.setParameter("folio", folio);
	        
	        maxId = (Short)q.getSingleResult();
	        maxId++;
    	}
    	catch(Exception e)
    	{
    	}
    	
    	if (null == maxId){
    		maxId = 1;
    	}
        
        return maxId;
    }

    public List<GlosarioProyecto> getGlosario(String folio, short serial) {
        Query q = mia.createQuery("SELECT a FROM GlosarioProyecto a where a.glosarioProyectoPK.folioProyecto = :folio and a.glosarioProyectoPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return q.getResultList();
    }

    public void eliminarLeyFedEst(String folio, Short serial, Short id, String tipo) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM LeyFedEstProyecto c where c.leyFedEstProyectoPK.folioProyecto = :folio and c.leyFedEstProyectoPK.serialProyecto = :serial and c.leyFedEstProyectoPK.leyFeId = :cid and c.leyFeBandera = :tipo ");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.setParameter("tipo", tipo);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

    public void eliminarGlosario(String folio, Short serial, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM GlosarioProyecto c where c.glosarioProyectoPK.folioProyecto = :folio and c.glosarioProyectoPK.serialProyecto = :serial and c.glosarioProyectoPK.glosproyId = :cid ");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

    public void eliminarPoetM(String folio, Short serial, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM PoetmProyecto c where c.poetmProyectoPK.folioProyecto = :folio and c.poetmProyectoPK.serialProyecto = :serial and c.poetmProyectoPK.poetmId = :cid ");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

    public void eliminarReglamento(String folio, Short serial, Short id)
    {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM ReglamentoProyecto c where c.reglamentoProyectoPK.folioProyecto = :folio and c.reglamentoProyectoPK.serialProyecto = :serial and c.reglamentoProyectoPK.reglamentoProyId = :cid ");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();           
            mia.getTransaction().commit();
             
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        }
    }

    public void eliminarPrediocolinProy(String folio, Short serial, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM PrediocolinProy c where c.prediocolinProyPK.folioProyecto = :folio and c.prediocolinProyPK.serialProyecto = :serial and c.prediocolinProyPK.predioColinId = :cid ");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        }
    }
    
    public void eliminarCuerpoAgua(String folio, Short serial, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM CuerposAguaProyecto c where c.cuerposAguaProyectoPK.folioProyecto = :folio and c.cuerposAguaProyectoPK.serialProyecto = :serial and c.cuerposAguaProyectoPK.cuerposAguaId = :cid ");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        }
    }
    
    public void eliminarNumerales(String folio, Short serial, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM Numeral c where c.numeralPK.folioProyecto  = :folio and c.numeralPK.serialProyecto = :serial and c.numeralPK.normaProyId = :cid ");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

    public void eliminarNumeral(String folio, Short serial, Short id, Short id2) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM Numeral c where c.numeralPK.folioProyecto = :folio and c.numeralPK.serialProyecto = :serial and c.numeralPK.normaProyId = :cid and c.numeralPK.numeralId = :cid2");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.setParameter("cid2", id2);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

    public void eliminarNorma(String folio, Short serial, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM NormaProyecto c where c.normaProyectoPK.folioProyecto = :folio and c.normaProyectoPK.serialProyecto = :serial and c.normaProyectoPK.normaProyId = :cid ");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

    public void eliminarFlora(String folio, Short serial, Integer id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM FloraProyecto c where c.floraProyectoPK.folioProyecto = :folio and c.floraProyectoPK.serialProyecto = :serial and c.floraProyectoPK.floraProyid = :cid ");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
/**
 * Metodo diseñado para borrar un registro de Accidente    
 * @param folio
 * @param serial
 * @param id
 * @throws Exception
 */
    public void eliminarAccidenteProyecto(String folio, Short serial, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM AccidentesProyecto e WHERE e.accidentesProyectoPK.folioProyecto = :folio and e.accidentesProyectoPK.serialProyecto = :serial and e.accidentesProyectoPK.accidenteId = :id");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("id", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        }
    }

    public VwPlantaeVegetacion getPlantaVegetacion(String nombre) {
        EntityManager m2 = EntityFactory.getMiaMF().createEntityManager();
        Query q = m2.createQuery("SELECT a from VwPlantaeVegetacion a WHERE a.plantaeNombreCientifico = '" + nombre + "'");
        List<VwPlantaeVegetacion> l = q.getResultList();
        System.out.println("resultado " + l.get(0));
        m2.close();

        return l.get(0);
    }

    public VwAnimaliaFauna getFauna(String nombre) {
        EntityManager m2 = EntityFactory.getMiaMF().createEntityManager();
        Query q = m2.createQuery("SELECT a from VwAnimaliaFauna a WHERE a.animaliaNombreCientifico = :nombre");
        q.setParameter("nombre", nombre);
        List<VwAnimaliaFauna> l = q.getResultList();
        m2.close();

        return l.get(0);
    }

    public List<String> getPlantaNombreCientifico(String nombre) {

        String pLetra = nombre.substring(0, 1);
        nombre = pLetra.toUpperCase() + nombre.substring(1).toLowerCase();
        System.out.println(nombre);
        Query q = mia.createNativeQuery("SELECT F.PLANTAE_NOMBRE_CIENTIFICO FROM DGIRA_MIAE2.VW_PLANTAE_VEGETACION F WHERE F.PLANTAE_NOMBRE_CIENTIFICO LIKE '%" + nombre + "%'");
        return q.getResultList();
    }

    /**
     * Permite obtener la lista de los nombres cientificos de la fauna seleccionada.
     * @param nombre a autocompletar
     * @param nombreClase seleccionada que no sea Anfibios o Reptiles
     * @return la lista de nombres cientificos
     */
    public List<String> getAnimaliaNombreCientifico(String nombre, String nombreClase) {

        String pLetra = nombre.substring(0, 1);
        nombre = pLetra.toUpperCase() + nombre.substring(1).toLowerCase();
        System.out.println(nombre);
        Query q = mia.createNativeQuery("SELECT F.ANIMALIA_NOMBRE_CIENTIFICO "
        							  + "FROM DGIRA_MIAE2.VW_ANIMALIA_FAUNA F "
        							  + "WHERE F.ANIMALIA_NOMBRE_CIENTIFICO LIKE '%" + nombre + "%' "
        							  + "AND F.ANIMALIA_CLASE = '"+nombreClase+"' ORDER BY F.ANIMALIA_NOMBRE_CIENTIFICO");
        return q.getResultList();
    }
    /**
     * Permite consultar la lista de nombres cientificos de anfibios y reptiles. 
     * @param nombre consultado
     * @return la lista de nombres cientificos.
     */
    public List<String> consultarAnimaliaNombreCientificoByAnfibioRept(String nombre) {
        String pLetra = nombre.substring(0, 1);
        nombre = pLetra.toUpperCase() + nombre.substring(1).toLowerCase();
        System.out.println(nombre);
        Query q = mia.createNativeQuery("SELECT F.ANIMALIA_NOMBRE_CIENTIFICO "
        							  + "FROM DGIRA_MIAE2.VW_ANIMALIA_FAUNA F "
        							  + "WHERE F.ANIMALIA_NOMBRE_CIENTIFICO LIKE '%" + nombre + "%' "
        							  + "AND F.ANIMALIA_CLASE IN ('Amphibia', 'Reptilia') ORDER BY F.ANIMALIA_NOMBRE_CIENTIFICO");
        return q.getResultList();		
	}

    /**
     * Permite consultar la lista de nombres cientificos de anfibios y reptiles. 
     * @param nombre consultado
     * @return la lista de nombres cientificos.
     */
    public List<String> consultarAnimaliaNombreCientificoByPeces(String nombre) {
        String pLetra = nombre.substring(0, 1);
        nombre = pLetra.toUpperCase() + nombre.substring(1).toLowerCase();
        System.out.println(nombre);
        Query q = mia.createNativeQuery("SELECT F.ANIMALIA_NOMBRE_CIENTIFICO "
        							  + "FROM DGIRA_MIAE2.VW_ANIMALIA_FAUNA F "
        							  + "WHERE F.ANIMALIA_NOMBRE_CIENTIFICO LIKE '%" + nombre + "%' "
        							  + "AND F.ANIMALIA_CLASE IN ('Actinopterygii', 'Chondrichthyes') ORDER BY F.ANIMALIA_NOMBRE_CIENTIFICO");
        return q.getResultList();		
	}

    public void eliminarFauna(String folio, Short serial, Integer id) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM FaunaProyecto c where c.faunaProyectoPK.folioProyecto = :folio and c.faunaProyectoPK.serialProyecto = :serial and c.faunaProyectoPK.faunaProyid = :cid ");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }

    public void eliminarEstudioEsp(String folio, Short serial, Short id) throws Exception {
        try {
            mia.getTransaction().begin();
            
            //Adjunto
            Query q = mia.createQuery("SELECT c.anexoUrl FROM EstudiosEspProy c where c.estudiosEspProyPK.folioSerial = :folio and c.estudiosEspProyPK.serialProyecto = :serial and c.estudiosEspProyPK.estudioId = :cid ");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            ArrayList<Object> lst = (ArrayList<Object>)q.getResultList();
            if (lst != null && !lst.isEmpty()){
                String anexoUrl = (String)lst.get(0);
                if (anexoUrl != null) {
                  //  eliminaArchivoRemoto(anexoUrl);
                }
            }            
            
            //Estudio especial
            q = mia.createQuery("DELETE FROM EstudiosEspProy c where c.estudiosEspProyPK.folioSerial = :folio and c.estudiosEspProyPK.serialProyecto = :serial and c.estudiosEspProyPK.estudioId = :cid ");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    public void eliminarTodosEstudioEsp(String folio, Short serial) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM EstudiosEspProy c where c.estudiosEspProyPK.folioSerial = :folio and c.estudiosEspProyPK.serialProyecto = :serial ");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    public void actualizarEstudioEspecial(EstudiosEspProyPK pkEstudioEsp, short estudioEspecial) {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("UPDATE EstudiosEspProy c SET estudioEspecial = :estudioEspecial "
                    + "where c.estudiosEspProyPK.folioSerial = :folio "
                    + "  and c.estudiosEspProyPK.serialProyecto = :serial "
                    + "  and c.estudiosEspProyPK.estudioId = :idEstudio ");
            q.setParameter("estudioEspecial", estudioEspecial);
            q.setParameter("folio", pkEstudioEsp.getFolioSerial());
            q.setParameter("serial", pkEstudioEsp.getSerialProyecto());
            q.setParameter("idEstudio", pkEstudioEsp.getEstudioId());
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            System.err.println("Ocurrió un error durante el guardado de Estudios Especiales");
            e.printStackTrace();
            //throw e;
        } finally {
//            mia.close();
        }
        
    }

    public List<PoetmProyecto> getPoetMProy(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM PoetmProyecto e  WHERE e.poetmProyectoPK.folioProyecto = :folio and e.poetmProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public Short getMaxPoetM(String folio, short serial) {
        Query q = mia.createQuery("SELECT max(a.poetmProyectoPK.poetmId) FROM PoetmProyecto a where a.poetmProyectoPK.folioProyecto = :folio and a.poetmProyectoPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Short) q.getSingleResult();
    }

    public Integer getMaxFlora(String folio, short serial) {
        Query q = mia.createQuery("SELECT max(a.floraProyectoPK.floraProyid) FROM FloraProyecto a where a.floraProyectoPK.folioProyecto = :folio and a.floraProyectoPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Integer) q.getSingleResult();
    }

    public Integer getMaxFauna(String folio, short serial) {
        Query q = mia.createQuery("SELECT max(a.faunaProyectoPK.faunaProyid) FROM FaunaProyecto a where a.faunaProyectoPK.folioProyecto = :folio and a.faunaProyectoPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Integer) q.getSingleResult();
    }

    
    public Short getMaxEstudiosEsp(String folio, short serial) 
    {
        Query q = mia.createQuery("SELECT max(a.estudiosEspProyPK.estudioId) FROM EstudiosEspProy a where a.estudiosEspProyPK.folioSerial = :folio and a.estudiosEspProyPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Short) q.getSingleResult();
    }

    
    public List<PdumProyecto> getPdum(String folio, short serial) 
    {
        Query q = mia.createQuery("SELECT a FROM PdumProyecto a where a.pdumProyectoPK.folioProyecto = :folio and a.pdumProyectoPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return q.getResultList();
    }
    
    /**
     * Busca un pdumProyecto por id
     * @param pdumId
     * @return objeto encontrado
     */
    public PdumProyecto getPdumProyecto(String folio, short serial, short pdumId) {
    	Query q = mia.createQuery("SELECT p FROM PdumProyecto p WHERE p.pdumProyectoPK.folioProyecto = :folio and p.pdumProyectoPK.serialProyecto = :serial and"
    			+ " p.pdumProyectoPK.pdumId = :pdumId");
    	q.setParameter("serial", serial);
        q.setParameter("folio", folio);
    	q.setParameter("pdumId", pdumId);
    	List<PdumProyecto> pdus = q.getResultList();
    	if (pdus != null && !pdus.isEmpty()) {
    		return pdus.get(0);
    	}
    	return null;
    }

    public List<SueloVegetacionProyecto> getSueloVegetacionProyecto(String folio, Short serial) {
    	EntityManager mia=EntityFactory.getMiaMF().createEntityManager();
        Query q = mia.createQuery("SELECT e FROM SueloVegetacionProyecto e  WHERE e.sueloVegetacionProyectoPK.folioProyecto = :folio and e.sueloVegetacionProyectoPK.serialProyecto = :serial ");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }
    
    public SueloVegetacionProyecto getSueloVegetacionProyecto(String folio, Short serial, short id) {
        Query q = mia.createQuery("SELECT e FROM SueloVegetacionProyecto e  WHERE e.sueloVegetacionProyectoPK.folioProyecto = :folio and e.sueloVegetacionProyectoPK.serialProyecto = :serial and e.sueloVegetacionProyectoPK.sueloVegProyId = :id");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("id", id);
        return (SueloVegetacionProyecto) q.getSingleResult();
    }

    public Short getMaxSueloVegetacionProyecto(String folio, short serial) {
        Short id = 0;
        try {
            Query q = mia.createQuery("SELECT max(a.sueloVegetacionProyectoPK.sueloVegProyId) FROM SueloVegetacionProyecto a where a.sueloVegetacionProyectoPK.folioProyecto = :folio and a.sueloVegetacionProyectoPK.serialProyecto = :serial ");
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            id = (Short) q.getSingleResult();
            id++;
        } catch (Exception e) {

        }
        if (id == null) {
            id = 1;
        }

        return id;
    }

    // Cap 4
    public List<EstudiosEspProy> getEstudiosEsp(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM EstudiosEspProy e  WHERE e.estudiosEspProyPK.folioSerial = :folio and e.estudiosEspProyPK.serialProyecto = :serial ");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public EstudiosEspProy getEstudiosEsp(String folio, Short serial, Short id) {
        EstudiosEspProy ele;
        List<EstudiosEspProy> lstEle;
        
        Query q = mia.createQuery("SELECT e FROM EstudiosEspProy e  WHERE e.estudiosEspProyPK.folioSerial = :folio and e.estudiosEspProyPK.serialProyecto = :serial  and e.estudiosEspProyPK.estudioId = :cid ");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("cid", id);
       
        lstEle = q.getResultList();
        if (lstEle != null && !lstEle.isEmpty() ) {
            ele = lstEle.get(0);
        } else {
            ele = null;
        }
        return ele;
    }

    public List<Climas> getClimas(String folio, Short serial, String cveProy) {
        Query q = mia.createQuery("SELECT e FROM Climas e  WHERE e.climasPK.numFolio = :folio and e.climasPK.version = :serial and e.climasPK.cveProy = :cveProy");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("cveProy", cveProy);
        return q.getResultList();
    }

    public List<ClimaProyecto> getClimaProyecto(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM ClimaProyecto e  WHERE e.climaProyectoPK.folioProyecto = :folio and e.climaProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public List<PdumProyecto> getPdumProyecto(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM PdumProyecto e  WHERE e.pdumProyectoPK.folioProyecto = :folio and e.pdumProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public Short getMaxPdumProyecto(String folio, short serial) {
        Short id = 0;
        try {
            Query q = mia.createQuery("SELECT max(e.pdumProyectoPK.pdumId) FROM PdumProyecto e  WHERE e.pdumProyectoPK.folioProyecto = :folio and e.pdumProyectoPK.serialProyecto = :serial");
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            id = (Short) q.getSingleResult();
            System.out.println("Max id encontrado: " + id);
            id++;
        } catch (Exception e) {

        }
        if (id == null) {
        	System.out.println("El id es nulo.");
            id = 1;
        }
        System.out.println("Id que se retorna: " + id);
        return id;
    }
    
    /**
     * Obtiene el id para el siguiente registro anp
     * @param folio
     * @param serial
     * @return id
     */
    public Short getMaxAnpProyecto(String folio, short serial) {
        Short id = 0;
        try {
            Query q = mia.createQuery("SELECT max(e.anpProyectoPK.anpId) FROM AnpProyecto e  WHERE e.anpProyectoPK.folioProyecto = :folio and e.anpProyectoPK.serialProyecto = :serial");
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            id = (Short) q.getSingleResult();
            id++;
        } catch (Exception e) {

        }
        if (id == null || id == 0) {
            id = 1;
        }

        return id;
    }
    
    /**
     * Obtiene el id para el siguiente registro ArticuloReglaAnp
     * @param folio
     * @param serial
     * @return id
     */
    public Short getMaxArticuloReglaAnpProyecto(String folio, short serial) {
        Short id = 0;
        try {
            Query q = mia.createQuery("SELECT max(e.articuloReglaAnpPK.anpArtRegId) FROM ArticuloReglaAnp e  WHERE e.articuloReglaAnpPK.folioProyecto = :folio and e.articuloReglaAnpPK.serialProyecto = :serial");
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            id = (Short) q.getSingleResult();
            id++;
        } catch (Exception e) {

        }
        if (id == null || id == 0) {
            id = 1;
        }

        return id;
    }

    public List<PoetmProyecto> getPoetmProyecto(String folio, Short serial) {
        Query q = mia.createQuery("SELECT e FROM PoetmProyecto e  WHERE e.poetmProyectoPK.folioProyecto = :folio and e.poetmProyectoPK.serialProyecto = :serial");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    public Short getMaxPoetmProyecto(String folio, short serial) {
        Short id = 0;
        try {
            Query q = mia.createQuery("SELECT max(e.poetmProyectoPK.poetmId) FROM PoetmProyecto e  WHERE e.poetmProyectoPK.folioProyecto = :folio and e.poetmProyectoPK.serialProyecto = :serial");
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            id = (Short) q.getSingleResult();
            id++;
        } catch (Exception e) {

        }
        if (id == null) {
            id = 0;
        }

        return id;
    }

    public Short getMaxMedPrevImpactProy(String folio, short serial) {
        Short id = 0;
        try {
            Query q = mia.createQuery("SELECT max(e.medPrevImpactProyPK.medPrevImpactId) FROM MedPrevImpactProy e  WHERE e.medPrevImpactProyPK.folioProyecto = :folio and e.medPrevImpactProyPK.serialProyecto = :serial");
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            id = (Short) q.getSingleResult();
            id++;
        } catch (Exception e) {

        }
        if (id == null) {
            id = 0;
        }

        return id;
    }

    public void guardarAvance(Proyecto proyecto, Short avance, String capitulo) {
        SinatecProcedure sp = new SinatecProcedure();
        System.out.println("actuaizar avance capitulo: " + capitulo);
        mia.getTransaction().begin();
//        AvanceProyecto ap = new AvanceProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        AvanceProyecto ap = (AvanceProyecto) mia.find(AvanceProyecto.class, new AvanceProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), new Short(capitulo)));
        if (ap == null) {
            ap = new AvanceProyecto(new AvanceProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), new Short(capitulo)));
            ap.setAvance(avance);
            mia.persist(ap);
        } else {
//            ap.setAvance(new Short("" + (avance + ap.getAvance())));
            ap.setAvance(avance);
            mia.merge(ap);
        }
        mia.getTransaction().commit();
        System.out.println("avance de SINATEC");
        sp.ejecutaSP(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()), Integer.parseInt(avanceProyecto(proyecto)));
    }

    public String avanceProyecto(Proyecto p) {
        Query q = mia.createQuery("SELECT sum(a.avance) FROM AvanceProyecto a where a.avanceProyectoPK.folioProyecto = :folio and a.avanceProyectoPK.serialProyeco = :serial");
        q.setParameter("folio", p.getProyectoPK().getFolioProyecto());
        q.setParameter("serial", p.getProyectoPK().getSerialProyecto());
        Object a = q.getSingleResult();

        if (a == null) {
            return "0";
        }
        return a.toString();
    }

    public static List<AnexosProyecto> getAnexos(short capituloId, short subcapituloId, short seccionId, short apartadoId, String folioProyecto, short serialProyecto) {
        List<AnexosProyecto> r = new ArrayList<AnexosProyecto>();
        try {
            if (capituloId != 0) {
                EntityManager em = EntityFactory.getMiaMF().createEntityManager();
                Query q = em.createQuery("SELECT a FROM AnexosProyecto a WHERE a.anexosProyectoPK.apartadoId = :apartado and a.anexosProyectoPK.capituloId = :capitulo and a.anexosProyectoPK.folioProyecto = :folio and a.anexosProyectoPK.serialProyecto = :serial and a.anexosProyectoPK.seccionId = :seccion and a.anexosProyectoPK.subcapituloId = :subcapitulo");
                q.setParameter("apartado", apartadoId);
                q.setParameter("capitulo", capituloId);
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                q.setParameter("seccion", seccionId);
                q.setParameter("subcapitulo", subcapituloId);
                r = q.getResultList();
                em.close();
            } else {
                //obtiene todos los adjuntos del folio
                EntityManager em = EntityFactory.getMiaMF().createEntityManager();
                Query q = em.createQuery("SELECT a FROM AnexosProyecto a WHERE a.anexosProyectoPK.folioProyecto = :folio and a.anexosProyectoPK.serialProyecto = :serial");
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                r = q.getResultList();
                em.close();
            }
        } catch (Exception e) {

        }

        return r;

    }

    public static int tamanioLista(List<?> l) {
        return l.size();
    }

    public List<Object[]> listaCriterios() {
        EntityManager em = EntityFactory.getMiaMF().createEntityManager();
        Query q = em.createNativeQuery("select C.CRITERIO_ORDEN, C.CRITERIO_DESCRIPCION, B.DESCRIPCION_VALOR, C.CRITERIO_IDESTATUS from DGIRA_MIAE2.CAT_DERECHOS_CRITERIOS C , DGIRA_MIAE2.CAT_DERECHOS_CRITERIOSVALORES B where B.ID_CRITERIO= C.CRITERIO_ID Order by C.CRITERIO_ORDEN");
        List<Object[]> l = q.getResultList();
        return l;
    }

    public List<?> lista_namedQuery(String qry, Object[] args, String[] paramName) {
        EntityManager em = EntityFactory.getMiaMF().createEntityManager();
        Query q = em.createNamedQuery(qry);
        for (int i = 0; i < args.length; i++) {
            q.setParameter(paramName[i], args[i]);
        }
        return q.getResultList();
    }

    public EvaluacionProyecto datosEvaProy(String numBita) {
        EntityManager em = EntityFactory.getMiaMF().createEntityManager();
        Query q = mia.createQuery("SELECT a FROM EvaluacionProyecto a WHERE a.bitacoraProyecto=:bitacora");
        q.setParameter("bitacora", numBita);
        em.close();
        Object o = null;
        try {
            o = q.getSingleResult();
        } catch (NoResultException e) {
            System.out.println("EROR: Folio no encontrado");
        } catch (Exception e) {
            System.out.println("" + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return (EvaluacionProyecto) o;
    }

    public List<CriterioValoresProyecto> getCriteriosProyecto(String folio, short serial) {
        Query q = mia.createQuery("SELECT a FROM CriterioValoresProyecto a WHERE a.criterioValoresProyectoPK.folioProyecto=:folio and a.criterioValoresProyectoPK.serialProyecto=:serial  ");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }

    //<editor-fold defaultstate="collapsed" desc="Consulta a CatDerechosCriteriosValores">
    /**
     * Consulta a CatDerechosCriteriosValores
     *
     * @param idCriterio
     * @param descValor
     * @return
     */
    public Integer valor(Integer idCriterio, String descValor) {
        EntityManager em = EntityFactory.getMiaMF().createEntityManager();
        BigInteger idCrit = new BigInteger(idCriterio.toString());
        Query q = em.createQuery("select c.valor from  CatDerechosCriteriosvalores c WHERE c.descripcionValor =:b AND c.catDerechosCriteriosvaloresPK.idCriterio =:a ");
        q.setParameter("b", descValor);
        q.setParameter("a", idCrit);
        BigInteger valor = (BigInteger) q.getSingleResult();

        return valor.intValue();

    }//</editor-fold> 

    public void elimina(Class<?> tipo, Object id) {
        EntityManager em = EntityFactory.getMiaMF().createEntityManager();
        EntityTransaction tx = em.getTransaction();
        try {
            Object o = em.find(tipo, id);
            if (o != null) {
                tx.begin();
                em.remove(o);
                tx.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (tx.isActive()) {
                tx.rollback();
            }
        }
//        em.close();
    }

    public Object busca(Class<?> tipo, Object id) {
        EntityManager em = EntityFactory.getMiaMF().createEntityManager();
        return em.find(tipo, id);
    }

    public EstudioRiesgoProyecto cargaEstudioRiesgo(Proyecto proyecto) {
        EstudioRiesgoProyecto e = null;

        System.out.println("Folio " + proyecto.getProyectoPK().getFolioProyecto());
        System.out.println("Serial " + proyecto.getProyectoPK().getFolioProyecto());

        try {
            Query q = mia.createQuery("SELECT e FROM EstudioRiesgoProyecto e WHERE e.estudioRiesgoProyectoPK.folioProyecto = :folio and e.estudioRiesgoProyectoPK.serialProyecto = :serial");
            q.setParameter("folio", proyecto.getProyectoPK().getFolioProyecto());
            q.setParameter("serial", proyecto.getProyectoPK().getSerialProyecto());
            e = (EstudioRiesgoProyecto) q.getSingleResult();
//            e = em.find(EstudioRiesgoProyecto.class, new EstudioRiesgoProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
        } catch (NoResultException err) {
            e = new EstudioRiesgoProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        }

        if (e == null) {
            e = new EstudioRiesgoProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

        }

        return e;
    }

    public static void main(String[] a) {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        EntityManager cat = EntityFactory.getCatalogos().createEntityManager();
//        Query q = mia.createQuery("SELECT e FROM ClimaProyecto e  WHERE e.climaProyectoPK.folioProyecto = :folio and e.climaProyectoPK.serialProyecto = :serial");
//        q.setParameter("folio", "3923");
//        q.setParameter("serial", (short)1);
//        List<ClimaProyecto> z = q.getResultList();
//
//        for (ClimaProyecto p:z){
//            System.out.println("" +p.getClaveClimatologica());
//        }
        Query q = mia.createQuery("SELECT e FROM CatEstudioEspSub c, CatEstEsp e WHERE c.catEstudioEspSubPK.estudioId = e.estudioId");
        List<Object> l = q.getResultList();
        for (Object o : l) {
            System.out.println("c " + o + " " + o.getClass().getName());
        }
    }

    public List<?> listado_like(Class<?> tipo, String atributeName, Object o) {

        System.out.println("tipo: " + tipo.getName());
        System.out.println("att: " + atributeName);
        System.out.println("obj: " + o.getClass().getName() + " " + o.toString());

        Query q = EntityFactory.getMiaMF().createEntityManager().createQuery("SELECT o FROM " + tipo.getName() + " o WHERE UPPER(o." + atributeName + ") like '" + o + "' ");

        return q.getResultList();
//        CriteriaBuilder cBuilder = em.getCriteriaBuilder();
//        CriteriaQuery<Object> cq = cBuilder.createQuery();
//        Root<?> r = cq.from(tipo);
//        cq.select(r);
//        Expression<String> path = r.get(atributeName);
//        cq.where(cBuilder.like(path, o.toString()));
//        return em.createQuery(cq).getResultList();
    }

    /**
     * Método que guarda en BD la bandera de sección modificada
     *
     * @param folio
     * @param capitulo
     * @param strSecciones
     */
    public void guardarSeccionModificada(String folio, short capitulo, String strSecciones) {
        String[] aSecciones, aUbica;
        Query q;
        String qry;
        Long lRes;
        ComentarioProyecto busqueda, cp;
        List<ComentarioProyecto> lstCp;
        short subCap, seccion, apartado;

        try {
            q = mia.createQuery("SELECT ss.bitacora FROM SinatSinatec ss WHERE to_char(ss.folio) = :folio");
            q.setParameter("folio", folio);
            List<String> lstBitacoras = (List<String>) q.getResultList();

            String bitacoraProy = (lstBitacoras != null && lstBitacoras.size() > 0) ? lstBitacoras.get(0) : "";

            qry = "SELECT c FROM ComentarioProyecto c "
                    + "WHERE c.comentarioProyectoPK.bitacoraProyecto = :bitacoraProy "
                    + "AND c.comentarioProyectoPK.capituloId = :capituloId "
                    + "AND c.comentarioProyectoPK.subcapituloId = :subcapituloId "
                    + "AND c.comentarioProyectoPK.seccionId = :seccionId "
                    + "AND c.comentarioProyectoPK.apartadoId = :apartadoId";
            
            if (strSecciones != null && !strSecciones.isEmpty()) {
            	mia.getTransaction().begin();
                aSecciones = strSecciones.split("\\|");
            	for (int idx = 0; idx < aSecciones.length; idx++) {
                    if (aSecciones[idx] != null && !aSecciones[idx].isEmpty()) {
                        aUbica = aSecciones[idx].split("-");
                        subCap = Short.parseShort(aUbica[0]);
                        seccion = Short.parseShort(aUbica[1]);
                        apartado = Short.parseShort(aUbica[2]);

                        q = mia.createQuery(qry);
                        q.setParameter("bitacoraProy", bitacoraProy);
                        q.setParameter("capituloId", capitulo);
                        q.setParameter("subcapituloId", subCap);
                        q.setParameter("seccionId", seccion);
                        q.setParameter("apartadoId", apartado);
                        lstCp = (List<ComentarioProyecto>) q.getResultList();

                        if (lstCp != null && lstCp.size() > 0) {
                            cp = lstCp.get(0);
                            cp.setSecModificada('1');
                            mia.merge(cp);
                        } else {
                            cp = new ComentarioProyecto(bitacoraProy, capitulo, subCap, seccion, apartado, BigInteger.valueOf(0));
                            cp.setSecModificada('1');
                            mia.persist(cp);
                        }
                        q = null;

                    }
                }

                mia.getTransaction().commit();
            }            
            System.out.println("Guardadas secciones modificadas");

        } catch (Exception e) {
            Log.error(folio, (short) 3, e.getMessage());
            System.out.println("Ocurrió un error durante la actualización de secciones modificadas");
            e.printStackTrace();
            if (mia.getTransaction().isActive()) {
                mia.getTransaction().rollback();
            }
        }
    }

    /**
     * Método que obtiene los comentarios correspondientes a un capitulo de un
     * folio
     *
     * @param folio
     * @param capitulo
     * @return <List>Lista de comentarios existentes por capitulo y folio</List>
     *
     * @author eescalona
     */
    public List<ComentarioProyecto> getComentarioProy(Integer folio, short capitulo) {
        short serialProyecto = MIA_SERIAL_PROYECTO_INFO_ADICIONAL;

        try {
            Query q = mia.createQuery("SELECT ss.bitacora FROM SinatSinatec ss, Proyecto p WHERE TO_CHAR(ss.folio) = p.proyectoPK.folioProyecto "
                    + "AND ss.folio = :folio AND p.proyectoPK.serialProyecto = :serialProyecto");
            q.setParameter("folio", folio);
            q.setParameter("serialProyecto", serialProyecto);
            List<String> lstBitacoras = (List<String>) q.getResultList();

            String bitacoraProy = (lstBitacoras != null && lstBitacoras.size() > 0) ? lstBitacoras.get(0) : "";
            System.out.println("bitacoraProy --> " + bitacoraProy);
            q = null;

            //String bitacoraProy = "09/MPW0010/12/14";
            q = mia.createQuery("SELECT r FROM ComentarioProyecto r WHERE r.comentarioProyectoPK.bitacoraProyecto = :bitacoraProy and r.comentarioProyectoPK.capituloId = :capitulo");
            q.setParameter("bitacoraProy", bitacoraProy);
            q.setParameter("capitulo", capitulo);
            return q.getResultList();

        } catch (Exception e) {
            System.out.println("Ocurrió un error durante la obtención de información adicional");
            e.printStackTrace();
            return null;
        }
    }

    public String obtenerBitacoraDeFolio(String folio) {
        Query q = mia.createQuery("SELECT ss.bitacora FROM SinatSinatec ss WHERE to_char(ss.folio) = :folio");
        q.setParameter("folio", folio);
        List<String> lstBitacoras = (List<String>) q.getResultList();

        String bitacora = (lstBitacoras != null && lstBitacoras.size() > 0) ? lstBitacoras.get(0) : "";

        return bitacora;
    }

    /**
     * Método que obtiene un listado de solo capitulos que tienen información
     * adicional y/o sección modificada
     *
     * @param bitacora
     * @return
     */
    public ArrayList<String[]> obtieneComenariosProyectoCapitulo(String bitacora) {
        ArrayList<String[]> lstRes = null;
        try {
            Query q = mia.createNativeQuery(
                    "select to_char(capitulo_id), to_char(max(info_adicional)) as info_adicional, to_char(max(sec_modificada)) as sec_modificada from "
                    + "( "
                    + "    select capitulo_id, subcapitulo_id, "
                    + "        case when info_adicional = '1' then 1 "
                    + "        else 0 end as info_adicional, "
                    + "        case when sec_modificada = '1' then 1 "
                    + "        else 0 end as sec_modificada   "
                    + "    from comentario_proyecto where bitacora_proyecto = '" + bitacora + "' "
                    + ") group by capitulo_id "
            );

            lstRes = (ArrayList<String[]>) q.getResultList();

        } catch (Exception e) {
            System.out.println("Ocurrió un error durante la obteción de comentariosProyecto " + e.toString());
            e.printStackTrace();
        }

        return lstRes;

    }

    /**
     * Método que obtiene un listado de capitulos y subcapitulos que tienen
     * información adicional y/o sección modificada
     *
     * @param bitacora
     * @return
     */
    public ArrayList<String[]> obtieneComenariosProyectoSubCap(String bitacora) {
        ArrayList<String[]> lstRes = null;
        try {
            Query q = mia.createNativeQuery(
                    "select capitulo_id || '_' || subcapitulo_id || '_' || seccion_id || '_' || apartado_id as ubicacion,  "
                    + "    to_char(max(info_adicional)) as info_adicional, to_char(max(sec_modificada)) as sec_modificada from "
                    + "( "
                    + "    select capitulo_id, subcapitulo_id, seccion_id, apartado_id, "
                    + "        case when info_adicional = '1' then 1 "
                    + "        else 0 end as info_adicional, "
                    + "        case when sec_modificada = '1' then 1 "
                    + "        else 0 end as sec_modificada   "
                    + "    from comentario_proyecto where bitacora_proyecto = '" + bitacora + "' "
                    + ") group by capitulo_id, subcapitulo_id, seccion_id, apartado_id "
            );

            lstRes = (ArrayList<String[]>) q.getResultList();

        } catch (Exception e) {
            System.out.println("Ocurrió un error durante la obteción de comentariosProyecto " + e.toString());
            e.printStackTrace();
        }

        return lstRes;

    }
    
    public boolean apartadosSinCapturar(String bitacora) {
        boolean bRes = false;
        String strRes;
        int iRes;
        BigDecimal bdRes;
        try {
            Query q = mia.createNativeQuery(
		"select count(*) from comentario_proyecto  " +
		"where bitacora_proyecto = :bitacora " +
		"  and info_adicional = 1 and (sec_modificada is null or sec_modificada <> 1)"
            );

            q.setParameter("bitacora", bitacora);
            bdRes = (BigDecimal) q.getSingleResult();
            
            if (bdRes.longValue()> 0) {
                bRes = true;//tiene apartados con información adicional pero sin seccion modificada (solicitados sin capturar)
            } else {
                bRes = false;//no tiene apartados
            }

        } catch (Exception e) {
            System.out.println("Ocurrió un error durante la obtención de comentariosProyecto " + e.toString());
            e.printStackTrace();
        }
        return bRes;
    }
    
    public List<AvanceProyecto> getAvanceCapERA(String folio, Short serial, short capitulo) {
        Query q = mia.createQuery("SELECT e FROM AvanceProyecto e WHERE e.avanceProyectoPK.folioProyecto=:folio and e.avanceProyectoPK.serialProyeco=:serial and e.avanceProyectoPK.capitulo=:cap");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("cap", capitulo);
        return q.getResultList();
    }
    
    public Long obtenerLoteProyecto(String folio, short serial) {
        Long lote = null;
        try {
            EntityManager em = EntityFactory.getMiaMF().createEntityManager();
            Query q = mia.createNativeQuery("select P.PROY_LOTE from proyecto P where P.folio_proyecto =:folio and p.serial_proyecto =:serial ");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            
            BigDecimal bdLote = (BigDecimal)q.getSingleResult();
            lote = bdLote.longValue();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return lote;
    }

    public ServicioProyecto getServicio(short idProy) {
		Query q = mia.createQuery("SELECT a FROM ServicioProyecto a where a.servicioProyectoPK.servicioProyId = :idProy");
		
		q.setParameter("idProy", idProy);

		return (ServicioProyecto) q.getSingleResult();
	}
    
    public ServicioProyecto getServicioByFolio(String folio, short serial, short idProy) {
		Query q = mia.createQuery("SELECT a FROM ServicioProyecto a where a.servicioProyectoPK.folioProyecto =:folio and a.servicioProyectoPK.serialProyecto =:serial and a.servicioProyectoPK.servicioProyId = :idProy");
		q.setParameter("folio", folio);
		q.setParameter("serial", serial);
		q.setParameter("idProy", idProy);

		return (ServicioProyecto) q.getSingleResult();
	}
	
    public List<CaracPartProy> getCaracPart(String folio, short serial) {
        Query q = mia.createQuery("SELECT r FROM CaracPartProy r WHERE r.caracPartProyPK.folioProyecto = :folio and r.caracPartProyPK.serialProyecto = :serial and r.estatusSigProm = '1' order by r.idCaracSeq");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        List<CaracPartProy> listaCarac = q.getResultList();
        return listaCarac;
    }
    
    /**
     * Permite buscar las caracteristicas particulares del promovente pero que contenga registros de SIGEIA.
     * @param folio
     * @param serial
     * @return la lista de registros de SIGEAI en caracteristicas particulares del Promovente.
     */
    public List<CaracPartProy> getCaracPartSIGEIAPROM(String folio, short serial) {
        Query q = mia.createQuery("SELECT r FROM CaracPartProy r WHERE r.caracPartProyPK.folioProyecto = :folio and r.caracPartProyPK.serialProyecto = :serial and r.estatusSigProm = '0' order by r.idCaracSeq");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        List<CaracPartProy> listaCarac = q.getResultList();
        return listaCarac;
    }    
    
    /**
     * Permite consultar las caracteristicas particulares del proyecto seleccionado para editar.
     * @param idCaracSeq
     * @return las caracteristicas de la obra del proyecto seleccionado.
     */
    public CaracPartProy getCaracPartPorObra(short idCaracSeq) {
        Query q = mia.createQuery("FROM CaracPartProy WHERE idCaracSeq = :idCaracSeq");
        q.setParameter("idCaracSeq", idCaracSeq);
        return (CaracPartProy) q.getSingleResult();
    }        
    
	
	    /**
     * Permite guardar las caracteristicas del proyecto.
     * @param caracPartProy a guardar.
     * @throws Exception en caso de error al guardar.
     */
    public void guardarCaracteristica(CaracPartProy caracPartProy) throws Exception {
    	
    	mia.getTransaction().begin();

    	short caracteristicaID = (short) (caracPartProy.getCaracPartProyPK().getCaracteristicaId() + 1);
    	
    	caracPartProy.getCaracPartProyPK().setCaracteristicaId(caracteristicaID);
    	
    	mia.persist(caracPartProy);
    	
//    	Query q = mia.createQuery("insert into CaracPartProy "
//    							+ " values (:folioProyecto,:serialProyecto,:caracteristicaId,:obraActividad,:naturalezaId,:temporalidad,:descripcion,:superficie,:ctunClve,:idCaracSeq,:nombreObra,:idObraActividad)");
//    	
//    	q.setParameter("folioProyecto", caracPartProy.getCaracPartProyPK().getFolioProyecto());
//    	q.setParameter("serialProyecto", caracPartProy.getCaracPartProyPK().getSerialProyecto());
//    	q.setParameter("caracteristicaId", caracPartProy.getCaracPartProyPK().getCaracteristicaId());
//    	q.setParameter("obraActividad", caracPartProy.getObraActividad().getObraDescripcion());
//    	q.setParameter("naturalezaId", caracPartProy.getNaturalezaId().getNaturalezaId());
//    	q.setParameter("temporalidad", caracPartProy.getTemporalidadId().getTemporalidadId());
//    	q.setParameter("descripcion", caracPartProy.getDescripcion());
//    	q.setParameter("superficie", caracPartProy.getSuperficie());
//    	q.setParameter("ctunClve", caracPartProy.getCtunClve());
//    	q.setParameter("idCaracSeq", "CARACPROY_SEQ.NEXTVAL");
//    	q.setParameter("nombreObra", caracPartProy.getNombreObra());
//    	q.setParameter("idObraActividad", caracPartProy.getObraActividad().getObraId());
    	
    	
        mia.getTransaction().commit();
        		
	} 
   
   
    /**
     * Se consulta el valor máximo de las caracteristicas para cargarlas.
     * @param folio del proyecto.
     * @param serial del proyecto.
     * @return el maximo valor de las caracteristicas.
     */
    public Short getMaxCaracteristica(String folio, short serial) throws Exception {
        Query q = mia.createQuery("SELECT max(cp.caracPartProyPK.caracteristicaId) FROM CaracPartProy cp where cp.caracPartProyPK.folioProyecto = :folio and cp.caracPartProyPK.serialProyecto =:serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Short) q.getSingleResult();
    }    
    /**
     * Se consulta el max valor de la sequencia obtenida.
     * @return el valor maximo de la sequencia
     */
    public Short consultarMAXCaracSequence() throws Exception {
        Query q = mia.createQuery("select max(idCaracSeq) from CaracPartProy");
        return (Short) q.getSingleResult();
	}
    

        
    /**
     * Permite eliminar la caracteristica particular del proyecto seleccionado.
     * @param folio
     * @param serial
     * @param idCaracSeq
     */
    public int eliminarCaracPartProySeleccionados(Short idCaracSeq) throws Exception {
    	mia.getTransaction().begin();
    	Query q = mia.createQuery("DELETE FROM CaracPartProy WHERE idCaracSeq = :caracteristicaID");
  
        q.setParameter("caracteristicaID", idCaracSeq);
        
        int eliminado = q.executeUpdate();
    	mia.getTransaction().commit();

        return eliminado;
	}
    
   
    @SuppressWarnings("unchecked")
	public List<ExplosivoActividadEtapa> obtenerPorProyecto2(String folio, short serial){    
    	Query q = mia.createQuery("SELECT e FROM ExplosivoActividadEtapa e");
        return q.getResultList();
    }
	
    /**
     * Permite consultar la ley federal por id.
     * @param leyId federal.
     * @return la ley federal consultada.
     */
    public CatLey consultarLeyFederal(short leyId) {
        Query q = mia.createQuery("FROM CatLey where leyId = " + leyId);
        return (CatLey) q.getSingleResult();
    }
    
    public CatReglamento consultarReglamento(short reglamentoId) {
        Query q = mia.createQuery("FROM CatReglamento where reglamentoId = " + reglamentoId);
        return (CatReglamento) q.getSingleResult();
    }
    
    public CatNorma consultarnorma(short normaId) {
        Query q = mia.createQuery("FROM CatNorma where normaId = " + normaId);
        return (CatNorma) q.getSingleResult();
    }

    

	public List<CatUnidadMedida> getUnidadesMedida() {
		String masa="masa";
		String volumen="volúmen";
		Query q = cat.createQuery("FROM CatUnidadMedida  where ctun_tipo=:masa or ctun_tipo=:volumen ");
		q.setParameter("masa", masa);
		q.setParameter("volumen", volumen);
	  return q.getResultList();
	}   
	
	/**
	 * Permite consultar la obra del proyecto..
	 * @param idObra
	 * @return la obra actividad.
	 */
	public CatObra consultarObra(int idObra) throws Exception {
        Query q = mia.createQuery("FROM CatObra where obraId = " + idObra);
        return (CatObra) q.getSingleResult();		
	}
	/**
	 * Permite consultar la naturaleza del proyecto.
	 * @param idNaturaleza
	 * @return la naturaleza del proyecto.
	 * @throws Exception en caso de error al consultar la naturaleza.
	 */
	public CatNaturaleza consultarNaturaleza(int idNaturaleza) throws Exception {
        Query q = mia.createQuery("FROM CatNaturaleza where naturalezaId = " + idNaturaleza);
        return (CatNaturaleza) q.getSingleResult();		
	}
	/**
	 * Permite consultar la temporalidad del proyecto.
	 * @param idTemporalidad
	 * @return la temporalidad del proyecto.
	 * @throws Exception en caso de error al consultar la temporalidad.
	 */
	public CatTemporalidad consultarTemporalidad(int idTemporalidad) throws Exception {
        Query q = mia.createQuery("FROM CatTemporalidad where temporalidadId = " + idTemporalidad);
        return (CatTemporalidad) q.getSingleResult();		
	}
	/**
	 * Permite consultar la lista de caracteristicas particulares del proyecto del promovente con su informacion de SIGEIA.
	 * @param folioProyecto
	 * @param serialProyecto
	 * @return
	 */
	public List<CaracPartProy> getCaracPartOrderBySIGEIA(String folioProyecto, short serialProyecto) {
        Query q = mia.createQuery("SELECT r FROM CaracPartProy r WHERE r.caracPartProyPK.folioProyecto = :folio and r.caracPartProyPK.serialProyecto = :serial "
        						+ "order by r.estatusSigProm");
        q.setParameter("folio", folioProyecto);
        q.setParameter("serial", serialProyecto);
        List<CaracPartProy> listaCarac = q.getResultList();
        return listaCarac;
	}

	//Método para obtener la Guía
	public List<ProySubsectorGuias> getUrlGuiaPdf(int subsec, int claveTramite) {
		Query q = mia.createQuery("SELECT c FROM ProySubsectorGuias c WHERE c.id.subsectorId=:subsec "
				                   + "and c.id.tramiteCve=:claveTramite");
		q.setParameter("subsec",(long)subsec);
		q.setParameter("claveTramite", String.valueOf(claveTramite) );
		return q.getResultList();
	}
	
	public List<ArchivosProyecto> getArchivos(String folioProyecto, short serialProyecto, short capitulo, short subcapitulo, short seccion, short apartado) {
        Query q = mia.createQuery("SELECT ap FROM ArchivosProyecto ap WHERE  folioSerial = :folioProyecto and serialProyecto = :serialProyecto and "
        						+ "capituloId = :capitulo and subCapituloId = :subcapitulo and seccionId = :seccion and ApartadoId = :apartado");
        
        
        q.setParameter("folioSerial", folioProyecto);
        q.setParameter("serialProyecto", serialProyecto);
        q.setParameter("capituloId", capitulo);
        q.setParameter("subCapituloId", subcapitulo);
        q.setParameter("seccionId", seccion);
        q.setParameter("ApartadoId", apartado);
        
        List<ArchivosProyecto> listaArchivos = q.getResultList();
        return listaArchivos;
	}

	public List<CuerposAguaProyecto> getCuerposAgua(String folioProyecto, short serialProyecto) {
        Query q = mia.createQuery("SELECT cap FROM CuerposAguaProyecto cap WHERE  cap.cuerposAguaProyectoPK.folioProyecto = :folioProyecto and cap.cuerposAguaProyectoPK.serialProyecto = :serialProyecto");
        
        
        q.setParameter("folioProyecto", folioProyecto);
        q.setParameter("serialProyecto", serialProyecto);
        
        List<CuerposAguaProyecto> listaCuerposAgua = q.getResultList();
        return listaCuerposAgua;
	}
	
	public Long conteoAdjuntos(String folio, short serial, String nombre) {
        BigDecimal dRes = BigDecimal.ZERO;
        String strNombre = null;
        boolean bNombre = nombre != null;
        if (bNombre) {
            strNombre = "%" + nombre;
        }
        try {
            
            EntityManager em = EntityFactory.getMiaMF().createEntityManager();
            Query q = em.createNativeQuery(
			"select sum(conteo) from " + 
			"( " +
                        "    select count (*) as conteo  " + 
			"        from anexos_proyecto AP  " + 
			"        where AP.folio_proyecto = :folio  " + 
			"          and AP.serial_proyecto = :serial  " + 
			(bNombre ? "          and AP.anexo_nombre like :nombre " : "") + 
			"    union all " + 
			"    select count (*) as conteo  " + 
			"        from archivos_proyecto AP2  " + 
			"        where AP2.folio_serial = :folio  " + 
			"          and AP2.serial_proyecto = :serial  " + 
                        (bNombre ? "          and AP2.filename like :nombre " : "") + 
			"    union all " + 
			"    select count (*) as conteo  " + 
			"        from estudios_esp_proy EE  " + 
			"        where EE.folio_serial = :folio  " + 
			"          and EE.serial_proyecto = :serial  " +
                        "          and EE.anexo_nombre is not null " +                         
                        (bNombre ? "          and EE.anexo_nombre like :nombre " : "") +                                
			") "            
            );
         
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            if (bNombre) {
                q.setParameter("nombre", strNombre);
            }
            dRes = (BigDecimal) q.getSingleResult();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dRes.longValue();
    }  
	
	public boolean buscaNombre(String folio, short serial, String nombre){
        int contador = 0;
		boolean existe = false;
        try {
        	EntityManager em = EntityFactory.getMiaMF().createEntityManager();
        	Query q1 = em.createQuery("SELECT a FROM AnexosProyecto a WHERE a.anexosProyectoPK.folioProyecto = :folio and a.anexosProyectoPK.serialProyecto = :serial and a.anexoNombre = :nombre");
            q1.setParameter("serial", serial);
            q1.setParameter("folio", folio);
            q1.setParameter("nombre", nombre);
            if(q1.getResultList().size() > 0){
            	contador ++;
            }
            
            Query q2 = em.createQuery(
        			"select AP2 from ArchivosProyecto AP2 where AP2.folioSerial = :folio and AP2.serialProyecto = :serial and AP2.fileName = :nombre");
            q2.setParameter("serial", serial);
            q2.setParameter("folio", folio);
            q2.setParameter("nombre", nombre);
            if(q2.getResultList().size() > 0){
            	contador ++;
            }
            
            Query q3 = em.createQuery(
        			"select EE from EstudiosEspProy EE where EE.estudiosEspProyPK.folioSerial = :folio and EE.estudiosEspProyPK.serialProyecto = :serial and EE.anexoNombre is not null and EE.anexoNombre = :nombre");   
            q3.setParameter("serial", serial);
            q3.setParameter("folio", folio);
            q3.setParameter("nombre", nombre);
            if(q3.getResultList().size() > 0){
            	contador ++;
            }
            
            if(contador > 0){
            	existe = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

		return existe;
	}
	
		
	public List<Object[]> listaResumen(String folio, Short serial) {
	    Query q = mia.createNativeQuery(" SELECT CAPITULO_ID AS IDCAP, SUBCAPITULO_ID AS SUBC, SECCION_ID AS IDSEC, SEQ_ID AS IDEST, FILENAME AS ESTNOM, DESCRIPCION AS ANDES, TAMANIO AS TAM, RESERVADO AS RES FROM ARCHIVOS_PROYECTO WHERE FOLIO_SERIAL =:folio AND SERIAL_PROYECTO =:serial "
			+ "	UNION "
			+ " SELECT CAPITULO_ID AS IDCAP,   0 AS SUBC,   SECCION_ID AS IDSEC, ESTUDIO_ID AS IDEST, ESTUDIO_ESP_NOMBRE_PROM AS ESTNOM, ANEXO_DESCRIPCION AS ANDES, ANEXO_TAMANIO AS TAM, RESERVADO AS RES FROM ESTUDIOS_ESP_PROY WHERE FOLIO_SERIAL =:folio AND SERIAL_PROYECTO =:serial ");
		q.setParameter("folio",folio );
		q.setParameter("serial", serial);
	    
	    List<Object[]> l = q.getResultList();
	    return l;
	}
	
	public int existePagoDerechos(String folio){
		 int resp = 0;
		 Query q = mia.createQuery("SELECT c FROM CriterioValoresProyecto c WHERE c.criterioValoresProyectoPK.folioProyecto = :folio");
		 q.setParameter("folio",folio);
		 if(q.getResultList().size()==3){
			 resp = q.getResultList().size();
		 }
		 return resp;
	 }
	
}