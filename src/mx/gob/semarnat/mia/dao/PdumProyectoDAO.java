/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.PdumProyecto;

/**
 * @author dpaniagua.
 *
 */
public class PdumProyectoDAO {
	
	/**
	 * Permite guardar un registro de PDUM del proyecto.
	 * @param object del pdum a guardar.
	 * @throws Exception en caso de error al guardar el registro.
	 */
    public void guardarPDUM(Object object) throws Exception {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
    	try {
            System.out.println("Iniciando Transacción!");            
            mia.getTransaction().begin();
            System.out.println("Persistiendo datos!");
            mia.persist(object);
            System.out.println("Commit!");
            mia.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Rollback!");
            mia.getTransaction().rollback();
            throw e;
        } finally {
            //System.out.println("Cerrando conexión!");
            //mia.close();
        }
    }
    
    /**
     * Permite consultar el listado de registros de PDUM del proyecto.
     * @param folio del proyecto.
     * @param serial del proyecto.
     * @return la lista de pdum proyecto.
     */
    public List<PdumProyecto> consultarPdumsProyecto(String folio, Short serial) throws Exception {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        Query q = mia.createQuery("SELECT e FROM PdumProyecto e  WHERE e.pdumProyectoPK.folioProyecto = :folio and e.pdumProyectoPK.serialProyecto = :serial order by e.claveOrigenRegistro");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }    
    
    /**
     * Permite consultar los registros de PDUM de SIGEIA.
     * @param folio del proyecto.
     * @param serial del proyecto.
     * @param cveProy clave del proyecto.
     * @return la lista de registros de PDUM de SIGEIA.
     */
    public List<Object[]> consultarPdumsSIGEIA(String folio, Short serial, String cveProy) {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
    	Query q = mia.createQuery("SELECT a.comp, a.descrip, a.usoclaspol, a.clvUsocp, a.instUrb, sum(a.area)  FROM InstrUrbanos a  WHERE a.instrUrbanosPK.numFolio = :fol and a.instrUrbanosPK.version = :clveProy group by a.comp, a.descrip, a.usoclaspol, a.clvUsocp, a.instUrb");
        q.setParameter("fol", folio);
        q.setParameter("clveProy", serial);
        //q.setParameter("cveProy", cveProy);
        return q.getResultList();
    }
    
    /**
     * Permite buscar un registro de PDUM del proyecto por id.
     * @param pdumId del registro a consultar.
     * @return objeto encontrado
     */
    public PdumProyecto consultarPdumPorId(String folio, short serial, short pdumId) {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
    	Query q = mia.createQuery("SELECT p FROM PdumProyecto p WHERE p.pdumProyectoPK.folioProyecto = :folio and p.pdumProyectoPK.serialProyecto = :serial and"
    			+ " p.pdumProyectoPK.pdumId = :pdumId");
    	q.setParameter("serial", serial);
        q.setParameter("folio", folio);
    	q.setParameter("pdumId", pdumId);
    	List<PdumProyecto> pdus = q.getResultList();
    	if (pdus != null && !pdus.isEmpty()) {
    		return pdus.get(0);
    	}
    	return null;
    }    
    
    /**
     * Permite eliminar el registro del PDUM del Proyecto seleccionado.
     * @param folio del proyecto.
     * @param serial del proyecto.
     * @param id del pdum seleccionado
     * @throws Exception en caso de error al eliminar el pdum.
     */
    public void eliminarPdumProyecto(String folio, Short serial, Short id) throws Exception {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM PdumProyecto c where c.pdumProyectoPK.folioProyecto = :folio and c.pdumProyectoPK.serialProyecto = :serial and c.pdumProyectoPK.pdumId =:cid");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    /**
     * Permite actualizar un registro del PDUM seleccionado.
     * @param object del registro a actualizar.
     * @throws Exception en caso de error al actualizar.
     */
    public void actualizarPDUMProyecto(Object object) throws Exception {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        try {
            mia.getTransaction().begin();
            object = mia.merge(object);
            mia.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    } 
    
    /**
     * Permite consultar El maximo valor del PDUM en la base de datos.
     * @param folio del proyecto.
     * @param serial del proyecto.
     * @return el valor maximo del PDUM.
     */
    public Short consultarMaxPdumProyecto(String folio, short serial) {
        Short id = 0;
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        try {
            Query q = mia.createQuery("SELECT max(e.pdumProyectoPK.pdumId) FROM PdumProyecto e  WHERE e.pdumProyectoPK.folioProyecto = :folio and e.pdumProyectoPK.serialProyecto = :serial");
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            id = (Short) q.getSingleResult();
            System.out.println("Max id encontrado: " + id);
            id++;
        } catch (Exception e) {

        }
        if (id == null) {
        	System.out.println("El id es nulo.");
            id = 1;
        }
        System.out.println("Id que se retorna: " + id);
        return id;
    }    
}
