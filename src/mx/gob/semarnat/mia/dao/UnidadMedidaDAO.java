/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.catalogos.CatUnidadMedida;

/**
 *
 * @author marcog
 */
@SuppressWarnings("unused")
public class UnidadMedidaDAO extends AbstractBaseDAO<CatUnidadMedida> {
    
    EntityManager mia=EntityFactory.getCatalogos().createEntityManager();

	/**
	 * Instantiates a new AsuntoDocDAO impl.
	 */
	public UnidadMedidaDAO() {
		super(CatUnidadMedida.class);
	}

    @Override
    protected EntityManager getEntityManager() {
       return this.mia;
    }

    public List<CatUnidadMedida> findByTipo(String tipo){
    	List<CatUnidadMedida>rtrn= new ArrayList<CatUnidadMedida>(); 
    	try{
            Query q = null;	
            q = mia.createNamedQuery("CatUnidadMedida.findByCtunTipo");
            q.setParameter("ctunTipo", tipo);                   
            rtrn = q.getResultList();

    	}catch(PersistenceException pe){
    		pe.printStackTrace();
    	}
    	return rtrn;
    }
    
    
   
    
}
