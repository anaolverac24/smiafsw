/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import javax.persistence.EntityManager;

import mx.gob.semarnat.mia.model.VegFase;

/**
 * @author Cesar
 *
 */
public class VegFaseDAO extends AbstractBaseDAO<VegFase> {

	/**
	 * EntityManager
	 */
	EntityManager mia=EntityFactory.getSigeia().createEntityManager();

	/**
	 * Constructor
	 */
	public VegFaseDAO() {
		super(VegFase.class);
	}
	
	/**
	 * getEntityManager
	 */
	@Override
    protected EntityManager getEntityManager() {
       return this.mia;
    }
}
