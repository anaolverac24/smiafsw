/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.ReglamentoProyecto;

/**
 * @author dpaniagua.
 *
 */
public class ReglamentoDAO {
	/**
	 * Permite guardar un nuevo reglamento.
	 * @param object reglamento a guardar.
	 */
	public void guardarReglamento(Object object) {
		EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        try {
            mia.getTransaction().begin();
            mia.persist(object);
            mia.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Rollback!");
            mia.getTransaction().rollback();
            throw e;
        } finally {
            //System.out.println("Cerrando conexión!");
            //mia.close();
        }		

	}

	/**
	 * Permite consultar la lista de Reglamentos.
	 * @param folio del proyecto.
	 * @param serial del proyecto.
	 * @return la lista de reglamentos.
	 */
	public List<ReglamentoProyecto> consultarReglamentos(String folio, Short serial) throws Exception {
		EntityManager mia = EntityFactory.getMiaMF().createEntityManager();

		Query q = mia.createQuery("SELECT e FROM ReglamentoProyecto e WHERE e.reglamentoProyectoPK.folioProyecto = :folio and e.reglamentoProyectoPK.serialProyecto = :serial");
		q.setParameter("folio", folio);
		q.setParameter("serial", serial);
		return q.getResultList();

	}
	
	/**
	 * Permite consultar un reglamento seleccionado para cargar un archivo.
	 * @param folio del proyecto.
	 * @param serial del proyecto.
	 * @param id del proyecto
	 * @return el reglamento seleccionado.
	 */
    public ReglamentoProyecto consultarReglamentoPorID(String folio, Short serial,Short id) {
		EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
    	Query q = mia.createNamedQuery("ReglamentoProyecto.findByReglamentoProyId");
    	q.setParameter("reglamentoProyId", id);
    	q.setParameter("folioProyecto", folio);
        q.setParameter("serialProyecto",serial);
    	return (ReglamentoProyecto) q.getSingleResult();    	 
    }	
	
	/**
	 * Permite eliminar uno o mas reglamentos seleccionados.
	 */
	public void eliminarReglamento(String folio, Short serial, Short id) {
		EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
		try {
        	mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM ReglamentoProyecto c where c.reglamentoProyectoPK.folioProyecto = :folio and c.reglamentoProyectoPK.serialProyecto = :serial and c.reglamentoProyectoPK.reglamentoProyId = :cid ");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("cid", id);
            q.executeUpdate();           
            mia.getTransaction().commit();
             
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        }		
	}
	
	
}
