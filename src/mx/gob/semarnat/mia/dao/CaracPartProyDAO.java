/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.CaracPartProy;
import mx.gob.semarnat.mia.model.CatNaturaleza;
import mx.gob.semarnat.mia.model.CatTemporalidad;
import mx.gob.semarnat.mia.model.catalogos.CatUnidadMedida;

/**
 * @author dpaniagua.
 *
 */
public class CaracPartProyDAO {
    EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
    EntityManager cat = EntityFactory.getCatalogos().createEntityManager();


	/**
	 * Permite actualizar la Caracteristica Particular del proyecto.
	 * @param caracPartProy
	 */
	public void actualizarCaracteristica(CaracPartProy caracPartProy) throws Exception {
		mia.getTransaction().begin();
		mia.merge(caracPartProy);
		mia.getTransaction().commit();
	}
	
    public List<CatNaturaleza> getCatNaturaleza() {
        Query q = mia.createQuery("SELECT c FROM CatNaturaleza c order by c.naturalezaDescripcion");
        return q.getResultList();
    }

    public List<CatTemporalidad> getCatTemporalidad() {
        Query q = mia.createQuery("SELECT c FROM CatTemporalidad c order by c.temporalidadDescripcion");
        return q.getResultList();
    }
    /**
     * Se consulta el max valor de la sequencia obtenida.
     * @return el valor maximo de la sequencia
     */
    public Short consultarMAXCaracSequence() throws Exception {
        Query q = mia.createQuery("select max(idCaracSeq) from CaracPartProy");
        return (Short) q.getSingleResult();
	}   
    
    /**
     * Se consulta el valor maximo de las caracteristicas para cargarlas.
     * @param folio del proyecto.
     * @param serial del proyecto.
     * @return el maximo valor de las caracteristicas.
     */
    public Short getMaxCaracteristica(String folio, short serial) throws Exception {
        Query q = mia.createQuery("SELECT max(cp.caracPartProyPK.caracteristicaId) FROM CaracPartProy cp where cp.caracPartProyPK.folioProyecto = :folio and cp.caracPartProyPK.serialProyecto =:serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Short) q.getSingleResult();
    }    

    /**
	 * Permite guardar las caracteristicas del proyecto.
	 * @param caracPartProy a guardar.
	 * @throws Exception en caso de error al guardar.
	 */
	public void guardarCaracteristica(CaracPartProy caracPartProy) throws Exception {
	    EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
		mia.getTransaction().begin();
		short caracteristicaID = (short) (caracPartProy.getCaracPartProyPK().getCaracteristicaId() + 1);
		caracPartProy.getCaracPartProyPK().setCaracteristicaId(caracteristicaID);
		mia.persist(caracPartProy);
		mia.getTransaction().commit();
	}  
	
    /**
     * Permite buscar las caracteristicas particulares del promovente pero que contenga registros de SIGEIA.
     * @param folio
     * @param serial
     * @return la lista de registros de SIGEAI en caracteristicas particulares del Promovente.
     */
    public List<CaracPartProy> getCaracPartSIGEIAPROM(String folio, short serial) {
        Query q = mia.createQuery("SELECT r FROM CaracPartProy r WHERE r.caracPartProyPK.folioProyecto = :folio and r.caracPartProyPK.serialProyecto = :serial and r.estatusSigProm = '0' order by r.idCaracSeq");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        List<CaracPartProy> listaCarac = q.getResultList();
        return listaCarac;
    }  	
    
    public List<CaracPartProy> getCaracPart(String folio, short serial) {
        Query q = mia.createQuery("SELECT r FROM CaracPartProy r WHERE r.caracPartProyPK.folioProyecto = :folio and r.caracPartProyPK.serialProyecto = :serial and r.estatusSigProm = '1' order by r.idCaracSeq");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        List<CaracPartProy> listaCarac = q.getResultList();
        return listaCarac;
    }  
    
	/**
	 * Permite consultar la lista de caracteristicas particulares del proyecto del promovente con su informacion de SIGEIA.
	 * @param folioProyecto
	 * @param serialProyecto
	 * @return
	 */
	public List<CaracPartProy> getCaracPartOrderBySIGEIA(String folioProyecto, short serialProyecto) {
        Query q = mia.createQuery("SELECT r FROM CaracPartProy r WHERE r.caracPartProyPK.folioProyecto = :folio and r.caracPartProyPK.serialProyecto = :serial "
        						+ "order by r.estatusSigProm");
        q.setParameter("folio", folioProyecto);
        q.setParameter("serial", serialProyecto);
        List<CaracPartProy> listaCarac = q.getResultList();
        return listaCarac;
	}    
	
	/**
	 * Permite consultar la naturaleza del proyecto.
	 * @param idNaturaleza
	 * @return la naturaleza del proyecto.
	 * @throws Exception en caso de error al consultar la naturaleza.
	 */
	public CatNaturaleza consultarNaturaleza(int idNaturaleza) throws Exception {
        Query q = mia.createQuery("FROM CatNaturaleza where naturalezaId = " + idNaturaleza);
        return (CatNaturaleza) q.getSingleResult();		
	}
	/**
	 * Permite consultar la temporalidad del proyecto.
	 * @param idTemporalidad
	 * @return la temporalidad del proyecto.
	 * @throws Exception en caso de error al consultar la temporalidad.
	 */
	public CatTemporalidad consultarTemporalidad(int idTemporalidad) throws Exception {
        Query q = mia.createQuery("FROM CatTemporalidad where temporalidadId = " + idTemporalidad);
        return (CatTemporalidad) q.getSingleResult();		
	}

    public CatUnidadMedida getCatUnidadMedida(Short ctunClve) {
    	   
    	CatUnidadMedida catUnidadMedida= new CatUnidadMedida();
        Query q = cat.createQuery("SELECT c FROM CatUnidadMedida c where c.ctunClve=:ctunClve order by c.ctunDesc");
        q.setParameter("ctunClve", ctunClve);
        catUnidadMedida=(CatUnidadMedida)q.getSingleResult();
        return catUnidadMedida;
    	
    }
}
