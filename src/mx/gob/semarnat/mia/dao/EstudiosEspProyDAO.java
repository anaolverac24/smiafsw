package mx.gob.semarnat.mia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.EstudiosEspProy;

public class EstudiosEspProyDAO extends AbstractBaseDAO<EstudiosEspProy> {
    EntityManager mia=EntityFactory.getMiaMF().createEntityManager();

	/**
	 * Instantiates a new AsuntoDocDAO impl.
	 */
	public EstudiosEspProyDAO() {
		super(EstudiosEspProy.class);
	}

	 @Override
	    protected EntityManager getEntityManager() {
	       return this.mia;
	    } 

	    public List<EstudiosEspProy> getEstudiosEsp(String folio, Short serial) {	       
	    	Query q = mia.createNamedQuery("EstudiosEspProy.findBySerialFolio");
	        q.setParameter("folio", folio);
	        q.setParameter("serial", serial);
	        return q.getResultList();
	    }
}
