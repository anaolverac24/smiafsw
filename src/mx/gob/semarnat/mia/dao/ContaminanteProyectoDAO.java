/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.ContaminanteProyecto;
import mx.gob.semarnat.mia.model.InfobioProyecto;

/**
 *
 * @author César
 */
public class ContaminanteProyectoDAO extends AbstractBaseDAO<ContaminanteProyecto> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6771442503020599399L;
	/**
	 * EntityManager
	 */
	EntityManager mia = EntityFactory.getSigeia().createEntityManager();

	/**
	 * Instantiates a new ReiaProyObrasDAO impl.
	 */
	public ContaminanteProyectoDAO() {
		super(ContaminanteProyecto.class);
	}

	/**
	 * Returns the EntityManager
	 * 
	 * @return Em
	 */
	@Override
	protected EntityManager getEntityManager() {
		return this.mia;
	}

	
	/**
	 * Busca las ContaminanteProyecto por su Folio y serial
	 * 
	 * @param folio
	 * @param serial
	 * @return lista de obras
	 */
	public List<ContaminanteProyecto> findByFolioSerial(String folio, short serial) {
		List<ContaminanteProyecto> listaInfo = null;
		try {
			Query q = null;
			q = mia.createNamedQuery("ContaminanteProyecto.findByFolioSerial");
			q.setParameter("folio", folio);
			q.setParameter("serial", serial);

			listaInfo = q.getResultList();

		} catch (PersistenceException px) {
			px.printStackTrace();
		}
		return listaInfo;
	}

	/**
	 * Cuenta los registros de obras por folio
	 * 
	 * @param folio
	 * @return numero de registros
	 */
	public int countByFolioSerial(String folio, short serial) {
		Query q = null;
		q = mia.createNamedQuery("ContaminanteProyecto.countByFolioSerial");
		q.setParameter("folio", folio);
		q.setParameter("serial", serial);
		return ((Long) q.getSingleResult()).intValue();
	}

	
	
	@SuppressWarnings("unchecked")
	public List<ContaminanteProyecto> findRangeByFolioSerial(int[] range, String folio, short serial) {
		List<ContaminanteProyecto> listaInfo = null;
		try {
			Query q = null;
			q = mia.createNamedQuery("ContaminanteProyecto.findByFolioSerial");
			q.setParameter("folio", folio);
			q.setParameter("serial", serial);
			q.setMaxResults(range[1] - range[0]);
			q.setFirstResult(range[0]);
			listaInfo = q.getResultList();
		} catch (PersistenceException pe) {
			pe.printStackTrace();
		}
		return listaInfo;
	}


	
	
	public int getLastByFolioSerial(String folio, short serial) {
		List<ContaminanteProyecto> listaInfo = null;
		int rtn=0;
		try {
			Query q = null;
			q = mia.createNamedQuery("ContaminanteProyecto.findByFolioSerialOrderBy");
			q.setParameter("folio", folio);
			q.setParameter("serial", serial);

			listaInfo = q.getResultList();
			if (listaInfo!=null && !listaInfo.isEmpty()){
				rtn=(int)listaInfo.get(0).getContaminanteProyectoPK().getContaminanteId();
			}
		} catch (PersistenceException px) {
			px.printStackTrace();
		}
		return rtn;
	}
}
