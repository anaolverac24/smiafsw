/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.NormaProyecto;
import mx.gob.semarnat.mia.model.Numeral;

/**
 *
 * @author 
 */
public class NumeralDAO extends AbstractBaseDAO<Numeral> {
    
    /**
     * EntityManager
     */
    EntityManager mia=EntityFactory.getSigeia().createEntityManager();

    /**
     * Instantiates a new ReiaProyObrasDAO impl.
     */
    public NumeralDAO() {
        super(Numeral.class);
    }
    
    /**
     * Returns the EntityManager
     * @return Em
     */
    @Override
    protected EntityManager getEntityManager() {
       return this.mia;
    }
    

    public short getMaxNumeral(String folioProyecto, short serialProyecto, short normaProyId){
    	short rtn=0;
    	Short rtnS=null;
        try{
            Query q = null;	
            q = mia.createNamedQuery("Numeral.findByFolioSerialProyecto");
            q.setParameter("folioProyecto", folioProyecto);
            q.setParameter("normaProyId", normaProyId);
            q.setParameter("serialProyecto", serialProyecto);
            
            rtnS =(Short) q.getSingleResult();

        }catch(Exception px){
            px.printStackTrace();
        }
        rtn=rtnS!=null?rtnS:0;
    	return ++rtn;
    }
   

    
    //findByFolioSerialProyectoNorma
    public void removeNumeral(String folioProyecto, short serialProyecto, short normaProyId){
    	List<Numeral> lNumeral= new ArrayList<Numeral> ();
        try{
            Query q = null;	
            q = mia.createNamedQuery("Numeral.findByFolioSerialProyectoNorma");
            q.setParameter("folioProyecto", folioProyecto);
            q.setParameter("normaProyId", normaProyId);
            q.setParameter("serialProyecto", serialProyecto);
            
            lNumeral = q.getResultList();
            if(lNumeral!=null && !lNumeral.isEmpty()){
            	for(Numeral numeral: lNumeral){
            		remove(numeral);
            	}
            }

        }catch(Exception px){
            px.printStackTrace();
        }
        
    }
    
    public void guardarNumeral(Object object) {
    	EntityManager mia=EntityFactory.getMiaMF().createEntityManager();
    	mia.getTransaction().begin();
    	mia.persist(object);
    	mia.getTransaction().commit();
	}
    
    public List<NormaProyecto> getNormas(String folio, Short serial) {
    	EntityManager mia=EntityFactory.getMiaMF().createEntityManager();
        Query q = mia.createQuery("SELECT e FROM NormaProyecto e WHERE e.normaProyectoPK.folioProyecto = :folio and e.normaProyectoPK.serialProyecto = :serial ORDER BY e.normaProyectoPK.normaProyId ");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }    
    
    //findByFolioSerialProyectoNorma
    public void eliminarNumeral(String folioProyecto, short serialProyecto, short normaProyId){
    	List<Numeral> lNumeral= new ArrayList<Numeral> ();
        try{
            Query q = null;	
        	EntityManager mia=EntityFactory.getMiaMF().createEntityManager();
            q = mia.createNamedQuery("Numeral.findByFolioSerialProyectoNorma");
            q.setParameter("folioProyecto", folioProyecto);
            q.setParameter("normaProyId", normaProyId);
            q.setParameter("serialProyecto", serialProyecto);
            
            lNumeral = q.getResultList();
            
            mia.getTransaction().begin();
            if(lNumeral!=null && !lNumeral.isEmpty()){
            	for(Numeral numeral: lNumeral){
            		mia.remove(mia.contains(numeral) ? numeral : mia.merge(numeral));
            	}
            }
            mia.getTransaction().commit();

            
        }catch(Exception px){
            px.printStackTrace();
        }
        
    } 
    
    /**
     * Permite eliminar un numeral seleccionado.
     * @param object a eliminar.
     */
    public void eliminarNumeralSeleccionado(Object object){
    	EntityManager mia=EntityFactory.getMiaMF().createEntityManager();
    	mia.getTransaction().begin();
    	mia.remove(mia.contains(object) ? object : mia.merge(object));
    	mia.getTransaction().commit();    	        
    }     
    
    /**
     * Permite eliminar una Norma.
     */
    public void eliminarNorma(Object object) {
    	EntityManager mia=EntityFactory.getMiaMF().createEntityManager();
    	mia.getTransaction().begin();
    	mia.remove(mia.contains(object) ? object : mia.merge(object));
    	mia.getTransaction().commit();
	}
    /**
     * Permite realizar la consulta de la lista de normas del proyecto.
     * @param folio
     * @param serial
     * @return la lista de normas.
     */
    public List<NormaProyecto> consultarNormas(String folio, Short serial) {
    	EntityManager mia=EntityFactory.getMiaMF().createEntityManager();
    	Query q = mia.createQuery("SELECT e FROM NormaProyecto e WHERE e.normaProyectoPK.folioProyecto = :folio and e.normaProyectoPK.serialProyecto = :serial ORDER BY e.normaProyectoPK.normaProyId ");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }   
    /**
     * Permite guardar la norma del proyecto.
     * @param object
     * @throws Exception en caso de error al guardar al norma.
     */
    public void guardarNorma(Object object) throws Exception {
    	EntityManager mia=EntityFactory.getMiaMF().createEntityManager();
        
    	try {
            System.out.println("Iniciando Transacción!");            
            mia.getTransaction().begin();
            System.out.println("Persistiendo datos!");
            mia.persist(object);
            System.out.println("Commit!");
            mia.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Rollback!");
            mia.getTransaction().rollback();
            throw e;
        } finally {
            //System.out.println("Cerrando conexión!");
            //mia.close();
        }
    }
    /**
     * Permite actualizar el numeral seleccionado.
     * @param object a actualizar.
     */
    public void editarNumeral(Object object) {
    	EntityManager mia=EntityFactory.getMiaMF().createEntityManager();
        
    	try {
            mia.getTransaction().begin();
            mia.merge(object);
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
            //System.out.println("Cerrando conexión!");
            //mia.close();
        }		
	}
}
