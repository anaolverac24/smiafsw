package mx.gob.semarnat.mia.dao;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import mx.gob.semarnat.mia.model.AnexosProyecto;
import mx.gob.semarnat.mia.model.ArchivosProyecto;
import mx.gob.semarnat.mia.model.CatEstEsp;
import mx.gob.semarnat.mia.model.CatParametro;
import mx.gob.semarnat.mia.model.EstudiosEspProy;
import mx.gob.semarnat.mia.model.EstudiosEspProyPK;
import mx.gob.semarnat.mia.util.ArchivoUtil;
import mx.gob.semarnat.mia.util.Log;
import mx.gob.semarnat.mia.view.ArchivoBase;

/**
 *
 * @author efrainc
 */
@ManagedBean
@ViewScoped
public class CargaBean extends ArchivoBase implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4132964446528256678L;

	private String nombre=" ";
	private String descr=" ";
	private String rutafin=" ";
	private long tam = 0;
	private boolean reservado;
	private MiaDao miaDao;
	private String msj;

	AnexosProyecto objeto = new AnexosProyecto();
	private String folioProyecto=" " ;
	private final VisorDao dao= new VisorDao();
	String Archivo =" ";
	String ruta =" ";

	//Atributos=

	short capituloId;
	short subcapituloId;
	short seccionId;
	short apartadoId;  
	short serialProyecto;

	private UploadedFile file;   
	
	private boolean bandera;

	private static final String RESERVADO_OK="1";
	private static final String RESERVADO_FALSE="0";
	
	private long indexRow = 0;


	//?=====================================================================================================================================================================
	//?=====================================================================================================================================================================
	private List<EstudiosEspProy> estudiosC = new ArrayList<EstudiosEspProy>();
	private List<EstudiosEspProy> estudiosS = new ArrayList<EstudiosEspProy>();
	private EstudiosEspProy estudiosArchivo = new EstudiosEspProy();
	private EstudiosEspProy estudiosAE;
	private CatEstEsp estEsp;
	private List<EstudiosEspProy> estudiosEsp = new ArrayList<EstudiosEspProy>();
	private List<CatEstEsp> catEstEsp = new ArrayList<CatEstEsp>();
	//?=====================================================================================================================================================================
	//?=====================================================================================================================================================================

	private EstudiosEspProyDAO estudiosEspProyDAO;
	public CargaBean(){
		//Carga proyecto especiales
		miaDao= new MiaDao();
		estudiosEspProyDAO= new EstudiosEspProyDAO();
		consultaEs();
		catEstEsp = miaDao.getCatEstEsp();
	} 


	public void addMessage() {
		String summary = reservado ? "Checked" : "Unchecked";
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));
	}


	public long getTam()
	{
		return tam;
	}

	public void setTam(long tam)
	{
		this.tam=tam;
	}





	public boolean isReservado() {
		return reservado;
	}



	public void setReservado(boolean reservado) {
		this.reservado = reservado;
	}



	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getRutafin() {
		return rutafin;
	}

	public void setRutafin(String rutafin) {
		this.rutafin = rutafin;
	}

	//Obtiene la primera ruta desde servidor
	private String Ruta()
	{

		EntityManager em = EntityFactory.getMiaMF().createEntityManager();
		Query q = em.createQuery("SELECT a FROM CatParametro a WHERE a.parDescripcion = 'ADJUNTOS'");
		CatParametro c = (CatParametro) q.getSingleResult();
		System.out.println(c.getParRuta().toString());
		return c.getParRuta().toString();
	}    

	//Crea el folder en servidor    
	private String CreaDir()
	{        
		FacesContext context = FacesContext.getCurrentInstance();
		this.folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");        
		// Crea toda la ruta folio lo recuperas desde session
		String Ruta = Ruta()+folioProyecto;
		ruta = Ruta + "/";
		// String Ruta = "c:/tmp/10344/";  // este solo debe habilitarse para pruebas locales  no para produccion
		context.getExternalContext().getSessionMap().put("archivoRuta", ruta);
		System.out.println(ruta);
		File destino = new File(ruta);//Crea un objeto
		//Revisa que exista si no lo crea
		if(!destino.exists())
		{
			destino.mkdirs();
			setRutafin(ruta);
			return ruta;      
		}
		else
		{
			return ruta;
		}
	}


	private String CreaDir2()
	{        
		FacesContext context = FacesContext.getCurrentInstance();
		this.folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");        
		//Crea toda la ruta folio lo recuperas desde session
		String Ruta = Ruta()+folioProyecto;
		//String Ruta = "c:/tmp/"+"10344";
		ruta = Ruta + "/";
		context.getExternalContext().getSessionMap().put("archivoRuta", ruta);
		System.out.println(ruta);
		File destino = new File(ruta);//Crea un objeto
		//Revisa que exista si no lo crea
		if(!destino.exists())
		{
			destino.mkdirs();
			setRutafin(ruta);
			return ruta;      
		}
		else
		{
			return ruta;
		}
	}






	/**
	 * Llama de la carga de archivo
	 * @param event
	 */
	public void handleFileUpload(FileUploadEvent event) {
		file = event.getFile();
		//        FacesMessage message = new FacesMessage("Archivo: ", file.getFileName() + " cargado correctamente.");
		//        FacesContext.getCurrentInstance().addMessage(null, message);
	}

	/**
	 * Escribe el archivo en el servidor
	 */
	private void crearArchivoEnServer(String nombreArchivo) {
		try {
			CreaDir();
			File targetFolder = new File(ruta);
			InputStream inputStream = file.getInputstream();
			OutputStream out = new FileOutputStream(new File(targetFolder, nombreArchivo));

			System.out.println("File name: " + nombreArchivo);
			System.out.println("Tamaño: " + file.getSize());           

			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = inputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			} 
			inputStream.close();
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	private void crearArchivoEnServerEst(String nombreArchivo) {
		try {
			CreaDir2();
			File targetFolder = new File(ruta);
			InputStream inputStream = file.getInputstream();
			OutputStream out = new FileOutputStream(new File(targetFolder, nombreArchivo));

			System.out.println("File name: " + file.getFileName());
			System.out.println("Tamaño: " + file.getSize());           

			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = inputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			} 
			inputStream.close();
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	//Prueba llamandolo desde la vista de prueba
	public void pruebaGuardar() throws Exception {
		capituloId =3;
		subcapituloId=3;
		seccionId=1;
		apartadoId=0;  
		serialProyecto=1;
		String reserv = "1";
		//crearArchivoEnServer();
		ArchivosProyecto archivoProyecto = new ArchivosProyecto("10344", serialProyecto, 
				capituloId, subcapituloId, seccionId, apartadoId); 
		GuardaAnexos(archivoProyecto);
	}

	/**
	 * Guarda el archivo cargado en la base de datos
	 * @param archivoProyecto
	 * @throws Exception 
	 */
	public boolean GuardaAnexos(ArchivosProyecto archivoProyecto) throws Exception {      
		boolean existe = false;
		try {
			if (this.file != null) {
			validarExtensionArchivo(this.file);
			String nombreArchivo = file.getFileName().substring(0, file.getFileName().lastIndexOf('.'));
			nombreArchivo = ArchivoUtil.adaptarReglaCadena(nombreArchivo);
			String nombre = nombreArchivo;
			String extension = FilenameUtils.getExtension(file.getFileName());
	        //Busca si el nombre de archivo no existe
	        Long conteo = miaDao.conteoAdjuntos(archivoProyecto.getFolioSerial(), archivoProyecto.getSerialProyecto(), nombre);
	        if (conteo > 0) {
	        	nombreArchivo = "[" + (conteo + 1) + "]" + nombre;
	        	nombreArchivo = ArchivoUtil.adaptarReglaCadena(nombreArchivo);
	        	//Valida que el nombre asignado despues del conteo no exista
	        	existe = miaDao.buscaNombre(archivoProyecto.getFolioSerial(), archivoProyecto.getSerialProyecto(), nombreArchivo);
	        	while (existe == true) {
	        		conteo++;	        		
	        		nombreArchivo = "[" + (conteo) + "]" + nombre;	        		
	        		nombreArchivo = ArchivoUtil.adaptarReglaCadena(nombreArchivo);
	        		existe = miaDao.buscaNombre(archivoProyecto.getFolioSerial(), archivoProyecto.getSerialProyecto(), nombreArchivo);
				}
	        }
			crearArchivoEnServer(nombreArchivo+"."+extension);
			//FacesContext context = FacesContext.getCurrentInstance();                  
			double bytes = file.getSize();
			double kilobytes = (bytes / 1024);
			archivoProyecto.setTamanio(new BigDecimal(kilobytes));
			archivoProyecto.setUrl(ruta + nombreArchivo+"."+extension);                    
			archivoProyecto.setReservado(reservado ? "1" : "0" );
			
//			String nombreArchivo = file.getFileName().substring(0, file.getFileName().lastIndexOf('.'));
			
			String fileName = nombreArchivo;
			archivoProyecto.setFileName(fileName); 
//			String extension = FilenameUtils.getExtension(archivoProyecto.getUrl());
			archivoProyecto.setExtension(extension);                                       
			VisorDao.persist(archivoProyecto);
			fileName = "";
			return bandera = true;
			}else {
				
				return bandera = false;
				
			}
			
		} catch (Exception e) {
			throw new Exception();
		}
	}
	/**
	 * Permite validar la extension y el contenido del archivo en caso de que se trate de cambiar
	 * manualmente la extension de un archivo (Ej. de doc a pdf).
	 * @param file a validar.
	 */
	public void validarExtensionArchivo(UploadedFile file) throws Exception {
		InputStream in = file.getInputstream();

		DataInputStream input = new DataInputStream(new BufferedInputStream(in));
		int fileSignature = input.readInt();
		System.out.println(fileSignature);

		String extensionFile = FilenameUtils.getExtension(file.getFileName());
		input.close();			

		if (extensionFile.equals("pdf")) {
			if (fileSignature != 0x25504446) {
				System.out.println("El archivo esta corrupto");
				throw new IOException();
			}
		} else if (extensionFile.equals("png")) {
			if (fileSignature != 0x89504E47) {
				System.out.println("El archivo esta corrupto");
				throw new IOException();					
			}
		} else if (extensionFile.equals("txt")) {
			if (fileSignature == 0x686F6C61) {
				System.out.println("El archivo esta corrupto");
				throw new IOException();					
			}
		} else if (extensionFile.equals("doc")) {
			System.out.println("Es un doc");
			if (fileSignature != 0xD0CF11E0) {
				System.out.println("El archivo esta corrupto");
				throw new IOException();					
			}
		} else if (extensionFile.equals("docx")) {
			System.out.println("Es un docx");
			if (fileSignature != 0x504B0304) {
				System.out.println("El archivo esta corrupto");
				throw new IOException();					
			}
		} else if (extensionFile.equals("gif")) {
			System.out.println("Es un gif");
			if (fileSignature != 0x47494638 || fileSignature != 0x47494638) {
				System.out.println("El archivo esta corrupto");
				throw new IOException();					
			}
		} else if (extensionFile.equals("jpg")) {
			System.out.println("Es un jpg o jpeg");
			if (fileSignature != 0xFFD8FFE0) {
				System.out.println("El archivo esta corrupto");
				throw new IOException();					
			}				
		} else if (extensionFile.equals("xls")) {
			System.out.println("Es un xls");
			if (fileSignature != 0xD0CF11E0) {
				System.out.println("El archivo esta corrupto");
				throw new IOException();					
			}				
		} else if (extensionFile.equals("xlsx")) {
			System.out.println("Es un xlsx");
			if (fileSignature != 0x504B0304) {
				System.out.println("El archivo esta corrupto");
				throw new IOException();					
			}				
		} else if (extensionFile.equals("dwg")) {
			System.out.println("Es un dwg");
			if (fileSignature != 0x41433130) {
				System.out.println("El archivo esta corrupto");
				throw new IOException();					
			}				
		}

	}


	public boolean guardaEstudios(EstudiosEspProy ee, String nom_prom, String desc, Short esId) {   
		boolean existe = false;
		if (this.file != null) {
			String nombreArchivo = file.getFileName().substring(0, file.getFileName().lastIndexOf('.'));
			nombreArchivo = ArchivoUtil.adaptarReglaCadena(nombreArchivo);
			String nombre = nombreArchivo;
			String extension = FilenameUtils.getExtension(file.getFileName());
	        //Busca si el nombre de archivo no existe
	        Long conteo = miaDao.conteoAdjuntos(ee.getEstudiosEspProyPK().getFolioSerial(), ee.getEstudiosEspProyPK().getSerialProyecto(), nombreArchivo);
	        if (conteo > 0) {
	        	nombreArchivo = "[" + (conteo + 1) + "]" + nombre;
	        	nombreArchivo = ArchivoUtil.adaptarReglaCadena(nombreArchivo);
	        	//Valida que el nombre asignado despues del conteo no exista
	        	existe = miaDao.buscaNombre(ee.getEstudiosEspProyPK().getFolioSerial(), ee.getEstudiosEspProyPK().getSerialProyecto(), nombreArchivo);
	        	while (existe == true) {
	        		conteo++;
	        		nombreArchivo = "[" + (conteo) + "]" + nombre;
	        		nombreArchivo = ArchivoUtil.adaptarReglaCadena(nombreArchivo);
	        		existe = miaDao.buscaNombre(ee.getEstudiosEspProyPK().getFolioSerial(), ee.getEstudiosEspProyPK().getSerialProyecto(), nombreArchivo);
				}
	        }	        
			crearArchivoEnServerEst(nombreArchivo+"."+extension);
			//FacesContext context = FacesContext.getCurrentInstance();    
			EstudiosEspProy esp= new EstudiosEspProy(new EstudiosEspProyPK(ee.getEstudiosEspProyPK().getFolioSerial(), ee.getEstudiosEspProyPK().getSerialProyecto(), ee.getEstudiosEspProyPK().getEstudioId()));
			double bytes = file.getSize();
			double kilobytes = (bytes / 1024);
			esp.setAnexoTamanio(new BigDecimal(kilobytes));
			esp.setAnexoUrl(ruta + nombreArchivo+"."+extension);                    
	
			esp.setCapituloId((short)4);
			esp.setSeccionId((short)1);
	
			esp.setEstudioEspecial(esId);
			esp.setNombrePromovente(nom_prom);
			esp.setAnexoDescripcion(desc);
			
			String fileName = nombreArchivo;
			esp.setAnexoNombre(fileName); 
//			String extension = FilenameUtils.getExtension(esp.getAnexoUrl());
			System.out.println(extension);
			esp.setAnexoExtension(extension);                                       
			//VisorDao.persist(esp);
	
			esp.setAnexoTamanio(esp.getAnexoTamanio().setScale(5, BigDecimal.ROUND_HALF_UP));
	
			esp.setReservado(reservado? RESERVADO_OK:RESERVADO_FALSE);
	
			try {
	
				estudiosEspProyDAO.create(esp);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return bandera = true;

		}else {
			
			return bandera = false;
			
		}
		

	}  





	/**
	 * @return the file
	 */
	public UploadedFile getFile() {
		return file;
	}



	/**
	 * @param file the file to set
	 */
	public void setFile(UploadedFile file) {
		this.file = file;
	}


	public MiaDao getMiaDao() {
		return miaDao;
	}


	public void setMiaDao(MiaDao miaDao) {
		this.miaDao = miaDao;
	}





	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void agregarEstudioEsp() {
		Short id = 0;
		try {
			id = miaDao.getMaxEstudiosEsp(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
			id++;
		} catch (Exception e2) {

		}
		if (id == null) {
			id = 1;
		}

		EstudiosEspProy ep = new EstudiosEspProy(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(),id);
		try {
			miaDao.merge(ep);
		} catch (Exception e3) {
			e3.printStackTrace();
		}
		estudiosEsp.add(ep);
	}


	public void eliminarEstudioEsp() {
		RequestContext reqcontEnv = RequestContext.getCurrentInstance();
		try {

			for(EstudiosEspProy e: estudiosS)
			{	            
				try {
					EstudiosEspProy ee =  estudiosEspProyDAO.find(new EstudiosEspProyPK(getProyecto().getProyectoPK().getFolioProyecto(), 
							getProyecto().getProyectoPK().getSerialProyecto(),e.getEstudiosEspProyPK().getEstudioId()));
//					//Eliminacion de Archivos en FileSystem
					eliminaArchivoRemotoEstudio(ee.getAnexoUrl());
					estudiosEspProyDAO.remove(ee);		                
				} catch (Exception e2) {
				}

			}
			consultaEs();
		} catch (Exception e) {
			Log.error(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), e.getMessage());
			e.printStackTrace();	            
		}

		RequestContext.getCurrentInstance().execute("dlgAvisoBorrado.show();");
		RequestContext.getCurrentInstance().update(":formEstudios:tablaDatos");
	}
	
    //Borra archivo desde file system de Estudios Especiales   
    private static void eliminaArchivoRemotoEstudio(String rutaArch) {
        try {
            File file = new File(rutaArch);
            Files.deleteIfExists(file.toPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


	public void nuevoEstudio()
	{	
		estEsp=null;
		reservado=Boolean.FALSE;
		estudiosAE= null;
		estudiosAE= new EstudiosEspProy ();
		file=null; 
	}

	public void consultaEs()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().get("userFolioProy");
		String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
		Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
		
		if (estudiosC==null){
			estudiosC= new ArrayList<EstudiosEspProy>();
		}else{
			estudiosC.clear();
		}

		
		if (estudiosS==null){
			estudiosS= new ArrayList<EstudiosEspProy>();
		}else{
			estudiosS.clear();
		}

		estudiosEspProyDAO= new EstudiosEspProyDAO();
		estudiosC = estudiosEspProyDAO.getEstudiosEsp(folioProyecto, serialProyecto);

		
		
		if(estudiosC.size() ==0)
		{
			System.out.println("Lista vacia: "+estudiosC.size());
		}
		else
		{
			System.out.println("TAMANAÑO DE LA LISTA DE ESTUDIOS: "+estudiosC.size());
		}
	}

	public void guardarEstudio()
	{
		if (!(estudiosAE!=null && estudiosAE.getEstudiosEspProyPK()!=null && estudiosAE.getEstudiosEspProyPK().getEstudioId()>0)){

			Short id = 0;
			try {
				id = miaDao.getMaxEstudiosEsp(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
				id++;
			} catch (Exception e2) {

			}
			if (id == null) {
				id = 1;
			}

			EstudiosEspProy ep = new EstudiosEspProy(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(),id);
			try 
			{

				bandera = guardaEstudios(ep, estudiosAE.getNombrePromovente(),estudiosAE.getAnexoDescripcion(), estEsp.getEstudioId() );
				
				if (bandera) {
					msj = "";
					consultaEs();
					RequestContext.getCurrentInstance().execute("modalEstudios.hide();");
					RequestContext.getCurrentInstance().execute("avisoDialog.show();");
				} else {
					msj = "Se requiere cargar un archivo";
				}
			} catch (Exception e3) {
				e3.printStackTrace();
			}

		}else{

			EstudiosEspProy entity=estudiosEspProyDAO.find(estudiosAE.getEstudiosEspProyPK());
			entity.setAnexoDescripcion(estudiosAE.getAnexoDescripcion());
			estudiosEspProyDAO.edit(entity);
			
			RequestContext.getCurrentInstance().execute("modalEstudios.hide();");
			RequestContext.getCurrentInstance().execute("avisoDialog.show();");
		}
	}


	/**
	 * @return the folioProyecto
	 */
	public String getFolioProyecto() {
		return folioProyecto;
	}


	/**
	 * @param folioProyecto the folioProyecto to set
	 */
	public void setFolioProyecto(String folioProyecto) {
		this.folioProyecto = folioProyecto;
	}


	/**
	 * @return the estudiosC
	 */
	public List<EstudiosEspProy> getEstudiosC() {
		return estudiosC;
	}


	/**
	 * @param estudiosC the estudiosC to set
	 */
	public void setEstudiosC(List<EstudiosEspProy> estudiosC) {
		this.estudiosC = estudiosC;
	}


	/**
	 * @return the estudiosS
	 */
	public List<EstudiosEspProy> getEstudiosS() {
		return estudiosS;
	}


	/**
	 * @param estudiosS the estudiosS to set
	 */
	public void setEstudiosS(List<EstudiosEspProy> estudiosS) {
		this.estudiosS = estudiosS;
	}


	/**
	 * @return the estudiosArchivo
	 */
	public EstudiosEspProy getEstudiosArchivo() {
		return estudiosArchivo;
	}


	/**
	 * @param estudiosArchivo the estudiosArchivo to set
	 */
	public void setEstudiosArchivo(EstudiosEspProy estudiosArchivo) {
		this.estudiosArchivo = estudiosArchivo;
	}


	/**
	 * @return the estudiosAE
	 */
	public EstudiosEspProy getEstudiosAE() {
		return estudiosAE;
	}


	/**
	 * @param estudiosAE the estudiosAE to set
	 */
	public void setEstudiosAE(EstudiosEspProy estudiosAE) {
		this.estudiosAE = estudiosAE;
	}


	/**
	 * @return the estEsp
	 */
	public CatEstEsp getEstEsp() {
		return estEsp;
	}


	/**
	 * @param estEsp the estEsp to set
	 */
	public void setEstEsp(CatEstEsp estEsp) {
		this.estEsp = estEsp;
	}


	/**
	 * @return the estudiosEsp
	 */
	public List<EstudiosEspProy> getEstudiosEsp() {
		return estudiosEsp;
	}


	/**
	 * @param estudiosEsp the estudiosEsp to set
	 */
	public void setEstudiosEsp(List<EstudiosEspProy> estudiosEsp) {
		this.estudiosEsp = estudiosEsp;
	}


	/**
	 * @return the catEstEsp
	 */
	public List<CatEstEsp> getCatEstEsp() {
		return catEstEsp;
	}


	/**
	 * @param catEstEsp the catEstEsp to set
	 */
	public void setCatEstEsp(List<CatEstEsp> catEstEsp) {
		this.catEstEsp = catEstEsp;
	}



	public void cargarEdicion(EstudiosEspProy estudiosEspProy){
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int rowIndex = 0;
		String pRowIndex = params.get("pRowIndex").toString();
		rowIndex = Integer.parseInt(pRowIndex);
		RequestContext.getCurrentInstance().reset(":fEliminar:eliminarEstudio");
		nuevoEstudio();
		estudiosAE=estudiosEspProy;
		reservado=estudiosAE.getReservado()!=null&&estudiosAE.getReservado().equals(RESERVADO_OK)?Boolean.TRUE:Boolean.FALSE;
		System.out.println("estudiosAE="+estudiosAE);
		estEsp=estudiosAE.getCatEstsEsp();
		indexRow = rowIndex;
	}



	public void cargarArchivo(String idEspS){
		String filePath = "";
		String ext = "";	        
		try {
			short idEsp=Short.parseShort(idEspS);
			EstudiosEspProy estudiosEspProy=estudiosEspProyDAO.find(new EstudiosEspProyPK(getProyecto().getProyectoPK().getFolioProyecto(),getProyecto().getProyectoPK().getSerialProyecto(),idEsp) );

			filePath = estudiosEspProy.getAnexoUrl();
			ext = estudiosEspProy.getAnexoExtension().toLowerCase().trim();   	
			File downloadFile = new File(filePath);

			String mimeType = "";
			String responseHeaderType = "";

			//Se valida si la extencion del archivo cumple para mostrar el contenido en el browser
			if (ext.equals("pdf") || ext.equals("jpg") || ext.equals("png") || ext.equals("gif")) {
				if (ext.equals("jpg") || ext.equals("png") || ext.equals("gif")) {
					mimeType = "image/" + ext;
				}
				if (ext.equals("pdf")) {
					mimeType = "application/pdf";
				}
				responseHeaderType = "inline";
			} else { 
				mimeType = "application/octet";
				responseHeaderType = "attachment";
			}          
			processRequest(mimeType, responseHeaderType, downloadFile.getPath(), estudiosEspProy.getAnexoNombre(), estudiosEspProy.getAnexoExtension());
			FacesContext.getCurrentInstance().responseComplete();
		} catch (IllegalStateException illegalException) {
			System.out.println("Excepcion de tipo IllegalStateException controloda.");
		} catch (Exception err) {
			System.out.println("Excepcion al ver el archivo: " + err.toString());            
		}	    
	}


	public String getMsj() {
		return msj;
	}


	public void setMsj(String msj) {
		this.msj = msj;
	}


	public boolean isBandera() {
		return bandera;
	}


	public void setBandera(boolean bandera) {
		this.bandera = bandera;
	}


	public long getIndexRow() {
		return indexRow;
	}


	public void setIndexRow(long indexRow) {
		this.indexRow = indexRow;
	}
}

