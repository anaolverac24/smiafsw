package mx.gob.semarnat.mia.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.HidrologiaProyecto;
import mx.gob.semarnat.mia.model.InfobioProyecto;

public class HidrologiaProyectoDAO extends AbstractBaseDAO<HidrologiaProyecto> {
    EntityManager mia=EntityFactory.getMiaMF().createEntityManager();

	/**
	 * Instantiates a new AsuntoDocDAO impl.
	 */
	public HidrologiaProyectoDAO() {
		super(HidrologiaProyecto.class);
	}

	 @Override
	    protected EntityManager getEntityManager() {
	       return this.mia;
	    } 

	 
	 public List<HidrologiaProyecto> getHidrologia(String folio, Short serial) {
			List<HidrologiaProyecto> listaInfo = null;
			try{
	        Query q = mia.createNamedQuery("HidrologiaProyecto.findByFolioSerial");
	        q.setParameter("folio", folio);
	        q.setParameter("serial", new BigDecimal(serial));
	        listaInfo= q.getResultList();
			} catch (PersistenceException pe) {
				pe.printStackTrace();
			}
			return listaInfo;
	    }
	 
	 public HidrologiaProyecto getHidrologia(String folio, Short serial, Short id) {
	        Query q = mia.createQuery("SELECT c FROM HidrologiaProyecto c where c.hidrologiaProyectoPK.folioProyecto = :folio and c.hidrologiaProyectoPK.serialProyecto = :serial and c.hidrologiaProyectoPK.id = :cid ");
	        q.setParameter("serial", serial);
	        q.setParameter("folio", folio);
	        q.setParameter("cid", id);
	        return (HidrologiaProyecto) q.getSingleResult();
	    }
	 
	 
	 
	 
		/**
		 * Cuenta los registros de obras por folio
		 * 
		 * @param folio
		 * @return numero de registros
		 */
		public int countHidrologia(String folio, Short serial) {
			Query q = null;
			q = mia.createNamedQuery("HidrologiaProyecto.countByFolioSerial");
			q.setParameter("folio", folio);
	        q.setParameter("serial", new BigDecimal(serial));
			return ((Long) q.getSingleResult()).intValue();
		}

		@SuppressWarnings("unchecked")
		public List<HidrologiaProyecto> findRangeHidrologia(int[] range, String folio, Short serial) {
			List<HidrologiaProyecto> listaInfo = null;
			try {
				Query q = null;
				q = mia.createNamedQuery("HidrologiaProyecto.findByFolioSerial");
				q.setParameter("folio", folio);
		        q.setParameter("serial", new BigDecimal(serial));
				q.setMaxResults(range[1] - range[0]);
				q.setFirstResult(range[0]);
				listaInfo = q.getResultList();
			} catch (PersistenceException pe) {
				pe.printStackTrace();
			}
			return listaInfo;
		}

}
