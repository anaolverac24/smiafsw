/*
 * @(#)AbstractBaseDAO.java 1.0 29/10/2014
 * 
 * Seguridad Tabasco
 */
package mx.gob.semarnat.mia.dao;

import java.util.List;

import javax.persistence.EntityManager;

/**
 * Clase abstracta que define los metodos de un dao.
 *
 * @author jponce
 * @param <T> the generic type
 */
public abstract class AbstractBaseDAO<T> {
	
	/** Entidad. */
	private Class<T> entityClass;

	/**
	 * Entity Manager.
	 *
	 * @return the entity manager
	 */
	protected abstract EntityManager getEntityManager();

	/**
	 * Constructor de la clase.
	 *
	 * @param entityClass the entity class
	 */
	public AbstractBaseDAO(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	/**
	 * Metodo insertar.
	 *
	 * @param entity the entity
	 */
	public void create(T entity) {
                getEntityManager().getTransaction().begin();
		getEntityManager().persist(entity);
                getEntityManager().getTransaction().commit();
	}

	/**
	 * Metodo editar o actualizar.
	 *
	 * @param entity the entity
	 */
	public void edit(T entity) {
                getEntityManager().getTransaction().begin();
		getEntityManager().merge(entity);
                getEntityManager().getTransaction().commit();

	}

	/**
	 * Metodo Eliminar.
	 *
	 * @param entity the entity
	 */
	public void remove(T entity) {
                getEntityManager().getTransaction().begin();
		getEntityManager().remove(entity);
                getEntityManager().getTransaction().commit();
	}

	/**
	 * Metodo de busqueda por Id.
	 *
	 * @param id the id
	 * @return the t
	 */
	public T find(Object id) {
		return getEntityManager().find(entityClass, id);
	}
	
	/**
	 * Metodo de busqueda por todos.
	 *
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		return getEntityManager().createQuery(
				"select object(o) from " + entityClass.getSimpleName()
						+ " as o").getResultList();
	}

	/**
	 * Flush.
	 */
	public void flush() {
		getEntityManager().flush();
	}


}
