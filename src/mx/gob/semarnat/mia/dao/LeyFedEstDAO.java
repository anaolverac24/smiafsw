/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.CatLey;
import mx.gob.semarnat.mia.model.LeyFedEstProyecto;

/**
 * @author dpaniagua.
 *
 */
public class LeyFedEstDAO {

	EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
	/**
	 * Permite eliminar el registro de la ley federal o estatal seleccionada.
	 * @param idLeySeq de la ley federal.
	 * @throws Exception en caso de error al eliminar.
	 */
	public int eliminarLeyFedEst(Short idLeySeq) throws Exception {
		try {
			EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
			mia.getTransaction().begin();
			Query q = mia.createQuery("DELETE FROM LeyFedEstProyecto where idLeySeq = :idLeySeq");
			q.setParameter("idLeySeq", idLeySeq);
			int leyesEliminadas = q.executeUpdate();
			mia.getTransaction().commit();

			return leyesEliminadas;
		} catch (Exception e) {
			mia.getTransaction().rollback();
			throw e;
		} finally {
		}
	}

	/**
	 * Permite consultar el valor maximos del idLeySeq del proyecto.
	 */
	public Short consultarMaxIdLeySeq() {
		EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
		Query query = mia.createQuery("select max(idLeySeq) from LeyFedEstProyecto");
		Short idMax = (Short) query.getSingleResult();
		return idMax;
	}
	/**
	 * Permite guardar la ley federal del proyecto.
	 * @param object ley federal.
	 * @throws Exception en caso de error al guardar la ley federal.
	 */
	public void guardarLeyFedProy(Object object) throws Exception {
		EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
		mia.getTransaction().begin();
		mia.persist(object);
		mia.getTransaction().commit();
	}
	/**
	 * Permite consultar la ley federal que se selecciono.
	 * @param idLeyFederal seleccionada.
	 * @return la ley federal.
	 */
	public CatLey consultarLeyPorId(short idLeyFederal) {
		EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
		Query q = mia.createQuery("FROM CatLey where leyId = " + idLeyFederal);
		return (CatLey) q.getSingleResult();
	}
	/**
	 * Permite consultar el maximo id de la ley del proyecto.
	 * @param folio del proyecto.
	 * @param serial del proyecto.
	 * @return el max id.
	 */
	public Short getMaxLeyFedEst() {
		EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
		Query q = mia.createQuery("SELECT max(a.leyFedEstProyectoPK.leyFeId) FROM LeyFedEstProyecto a");
		return (Short) q.getSingleResult();
	} 
	/**
	 * Permite consultar la ley por el id de la ley federal.
	 * @param idLeyFederal seleccionada.
	 * @return la ley federal.
	 */
	public LeyFedEstProyecto consultarLeyProyectoPorId(short idLeyFederal) {
		Query q = mia.createQuery("FROM LeyFedEstProyecto where idLeySeq = " + idLeyFederal);
		return (LeyFedEstProyecto) q.getSingleResult();
	}
	
	/**
	 * Permite consultar la ley por el id de la ley federal para la Carga de los Archivos.
	 * @param idLeyFederal seleccionada.
	 * @return la ley federal.
	 */
	public LeyFedEstProyecto consultarLeyProyectoArchivos(short idLeyFederal) {
		Query q = mia.createQuery("FROM LeyFedEstProyecto where idLeySeq = " + idLeyFederal);
		return (LeyFedEstProyecto) q.getSingleResult();
	}	

	/**
	 * Permite consultar la ley por el id de la ley federal.
	 * @param idLeyFederal seleccionada.
	 * @return la ley federal.
	 */
	public LeyFedEstProyecto consultarLeyFederalPorId(short idLeyFederal) {
		Query q = mia.createQuery("FROM LeyFedEstProyecto where idLeySeq = " + idLeyFederal);
		return (LeyFedEstProyecto) q.getSingleResult();
	}    
	/**
	 * Permite actualizar una ley estatal..
	 * @param object
	 */
	public void actualizarLeyFedProy(Object object) throws Exception {
		mia.getTransaction().begin();
		mia.merge(object);
		mia.getTransaction().commit();		
	}

	/**
	 * Permite actualizar una ley federal..
	 * @param object
	 */
	public void actualizarLeyFederalProy(Object object) throws Exception {
		mia.getTransaction().begin();
		mia.merge(object);
		mia.getTransaction().commit();

	}
	/**
	 * Permite consultar las leyes federales.
	 * @param folio del proyecto.
	 * @param serial del proyecto.
	 * @return la lista de leyes federales del proyecto.
	 */
    public List<LeyFedEstProyecto> getLeyesFed(String folio, Short serial) {
		EntityManager mia = EntityFactory.getMiaMF().createEntityManager();	
        Query q = mia.createQuery("SELECT e FROM LeyFedEstProyecto e WHERE e.leyFedEstProyectoPK.folioProyecto = :folio and e.leyFedEstProyectoPK.serialProyecto = :serial and e.leyFeBandera = 'F'");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }
    /**
     * Permite consultar las leyes estatales.
     * @param folio
     * @param serial
     * @return
     */
    public List<LeyFedEstProyecto> getLeyesEst(String folio, Short serial) {
		EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        Query q = mia.createQuery("SELECT e FROM LeyFedEstProyecto e WHERE e.leyFedEstProyectoPK.folioProyecto = :folio and e.leyFedEstProyectoPK.serialProyecto = :serial and e.leyFeBandera = 'E'");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    } 
}
