/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import mx.gob.semarnat.mia.model.ReiaObrasCondiciones;

/**
 *
 * @author César
 */
public class ReiaObrasCondicionesDAO extends AbstractBaseDAO<ReiaObrasCondiciones> {
    
    /**
     * EntityManager
     */
    EntityManager mia=EntityFactory.getSigeia().createEntityManager();
    
    /**
     * Instantiates a new ReiaObrasCondicionesDAO impl.
     */
    public ReiaObrasCondicionesDAO(){
        super(ReiaObrasCondiciones.class);
    }
    
    /**
     * Returns the EntityManager
     * @return Em
     */
    @Override
    protected EntityManager getEntityManager() {
       return this.mia;
    }
}
