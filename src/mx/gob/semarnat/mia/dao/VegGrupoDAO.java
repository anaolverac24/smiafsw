/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import javax.persistence.EntityManager;

import mx.gob.semarnat.mia.model.VegGrupo;

/**
 * @author Cesar
 *
 */
public class VegGrupoDAO extends AbstractBaseDAO<VegGrupo> {
	
	/**
	 * EntityManager
	 */
	EntityManager mia=EntityFactory.getSigeia().createEntityManager();

	/**
	 * Constructor
	 */
	public VegGrupoDAO() {
		super(VegGrupo.class);
	}
	
	/**
	 * getEntityManager
	 */
	@Override
    protected EntityManager getEntityManager() {
       return this.mia;
    }
}
