/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.FloraProyecto;
import mx.gob.semarnat.mia.model.VwPlantaeVegetacion;

/**
 * @author dpaniagua.
 */
public class FloraProyectoDAO {
    EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
    /**
     * Permite consultar la informacion de la flora segun el nombre cientifico.
     * @param nombre cientifico seleccionado.
     * @return la informacion de la flora.
     */
    public VwPlantaeVegetacion getPlantaVegetacion(String nombre) {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        Query q = mia.createQuery("FROM VwPlantaeVegetacion WHERE plantaeNombreCientifico = :nombre");
        q.setParameter("nombre", nombre);
        VwPlantaeVegetacion plantaVegetacion = (VwPlantaeVegetacion) q.getSingleResult();
        return plantaVegetacion;
    }

    /**
     * Permite guardar el nuevo registro de flora.
     * @param object flora a guardar.
     */
    public void guardarFlora(Object object) throws Exception {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        mia.getTransaction().begin();
        mia.persist(object);
        mia.getTransaction().commit();
    }    
    /**
     * Permite obtener el valor max de la flora
     * @param folio del proyecto.
     * @param serial del proyecto.
     * @return el valor maximo de la flora.s
     */
    public Integer getMaxFlora(String folio, short serial) {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        Query q = mia.createQuery("SELECT max(a.floraProyectoPK.floraProyid) FROM FloraProyecto a where a.floraProyectoPK.folioProyecto = :folio and a.floraProyectoPK.serialProyecto = :serial ");
        q.setParameter("serial", serial);
        q.setParameter("folio", folio);
        return (Integer) q.getSingleResult();
    }
    /**
     * Permite obtener el sequencial maximos de la flora.
     * @param folio del proyecto.
     * @param serial del proyecto.
     * @return
     */
    public Short getMaxIdFloraSEQ() {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        Query q = mia.createQuery("SELECT max(a.idFloraSeq) FROM FloraProyecto a");
        return (Short) q.getSingleResult();
    }    
    /**
     * Permite obtener la lista de flora del proyecto con su serial.
     * @param folio del proyecto.
     * @param serial del proyecto.
     * @return la lista de la flora del proyecto.
     */
    public List<FloraProyecto> getFloraProyecto(String folio, Short serial) {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        Query q = mia.createQuery("SELECT e FROM FloraProyecto e WHERE e.floraProyectoPK.folioProyecto = :folio and e.floraProyectoPK.serialProyecto = :serial order by e.floraProyectoPK.floraProyid ");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        return q.getResultList();
    }
    /**
     * Permite consultar la flora seleccionada para editar.
     * @param idFloraSeqSeleccionada a editar.
     * @return la flora a consultar.
     */
    public FloraProyecto consultarFloraProyectoSeleccionada(String folio, Short serial, Short idFloraSeqSeleccionada) {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        Query q = mia.createQuery("FROM FloraProyecto e WHERE e.floraProyectoPK.folioProyecto = :folio and e.floraProyectoPK.serialProyecto = :serial and e.idFloraSeq = :idFloraSeq");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("idFloraSeq", idFloraSeqSeleccionada);
        return (FloraProyecto) q.getSingleResult();
    }   
    
    /**
     * Permite actualizar la flora seleccionada.
     * @param object flora a actualizar.
     */
    public void actualizarFlora(Object object) throws Exception {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        mia.getTransaction().begin();
        mia.merge(object);
        mia.getTransaction().commit();
    }  
    /**
     * Permite eliminar la flora seleccionada.
     * @param idFloraSeq a eliminar.
     * @return 1 en caso de ser exitoso.
     */
    public int eliminarFlora(Short idFloraSeq) throws Exception {
        EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
        mia.getTransaction().begin();
        Query q = mia.createQuery("DELETE FROM FloraProyecto WHERE idFloraSeq = :idFloraSeq");
        q.setParameter("idFloraSeq", idFloraSeq);
        int eliminados = q.executeUpdate();
        mia.getTransaction().commit();
        
        return eliminados;
    }    
}
