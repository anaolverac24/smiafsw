/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.ReiaCondiciones;
import mx.gob.semarnat.mia.model.ReiaObrasCondiciones;

/**
 *
 * @author marcog
 */
public class ReiaCondicionesDAO extends AbstractBaseDAO<ReiaCondiciones> {
    
    EntityManager mia=EntityFactory.getSigeia().createEntityManager();

	/**
	 * Instantiates a new AsuntoDocDAO impl.
	 */
	public ReiaCondicionesDAO() {
		super(ReiaCondiciones.class);
	}
        
        
    public List<ReiaObrasCondiciones> findCondicionesPorObra(Long idObra) {
        List<ReiaObrasCondiciones> rtrn=null;
        try{
            Query q = null;	
            q = mia.createNamedQuery("ReiaObrasCondiciones.findCondicionByIdOb");
            q.setParameter("idObra", idObra);                   
            rtrn = q.getResultList();

        }catch(PersistenceException px){
            px.printStackTrace();
        }
        return rtrn;
    }

    


    
    
    @Override
    protected EntityManager getEntityManager() {
       return this.mia;
    }
        
}
