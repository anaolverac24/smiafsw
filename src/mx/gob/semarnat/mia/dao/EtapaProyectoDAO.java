/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.EtapaProyecto;

/**
 *
 * @author César
 */
public class EtapaProyectoDAO extends AbstractBaseDAO<EtapaProyecto> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -677144299020599399L;
	/**
	 * EntityManager
	 */
	EntityManager mia = EntityFactory.getSigeia().createEntityManager();

	/**
	 * Instantiates a new ReiaProyObrasDAO impl.
	 */
	public EtapaProyectoDAO() {
		super(EtapaProyecto.class);
	}

	/**
	 * Returns the EntityManager
	 * 
	 * @return Em
	 */
	@Override
	protected EntityManager getEntityManager() {
		return this.mia;
	}

//@NamedQuery(name = "EtapaProyecto.findByFolioSerial", query = "SELECT e FROM EtapaProyecto e WHERE e.etapaProyectoPK.folioProyecto=:folioProyecto AND e.etapaProyectoPK.serialProyecto=:serialProyecto")})	

	public  List<EtapaProyecto> findByFolioSerial(String folioProyecto, short serialProyecto){
		List<EtapaProyecto> listaEntity= new ArrayList<EtapaProyecto> ();
		try {
			Query q = null;
			q = mia.createNamedQuery("EtapaProyecto.findByFolioSerial");
			q.setParameter("folioProyecto", folioProyecto);
			q.setParameter("serialProyecto", serialProyecto);
			listaEntity = q.getResultList();

		} catch (PersistenceException px) {
			px.printStackTrace();
		}
		return listaEntity;
	}

}
