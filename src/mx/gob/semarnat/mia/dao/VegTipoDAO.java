/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.ReiaObras;
import mx.gob.semarnat.mia.model.VegTipo;

/**
 * @author Cesar
 *
 */
public class VegTipoDAO extends AbstractBaseDAO<VegTipo> {

	/**
	 * EntityManager
	 */
	EntityManager mia=EntityFactory.getSigeia().createEntityManager();

	/**
	 * Constructor
	 */
	public VegTipoDAO() {
		super(VegTipo.class);
	}
	
	/**
	 * Busca VegTipo por su nombre
	 * @param vegName
	 * @return VegTipo encontrado
	 */
	public VegTipo findByName(String vegName) {
		VegTipo rtrn=null;
        try{
            Query q = null;	
            q = mia.createNamedQuery("VegTipo.findByName");
            q.setParameter("vegName", vegName); 
            List<VegTipo> listaVeg = q.getResultList();
            if (listaVeg != null && !listaVeg.isEmpty()) {
            	rtrn = listaVeg.get(0);
            }            
        }catch(PersistenceException px){
            px.printStackTrace();
        }
        return rtrn;
	}
	
	/**
	 * getEntityManager
	 */
	@Override
    protected EntityManager getEntityManager() {
       return this.mia;
    }
}
