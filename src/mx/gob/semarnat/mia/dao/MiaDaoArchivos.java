package mx.gob.semarnat.mia.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;

 

import javax.persistence.Query;

import org.primefaces.event.FileUploadEvent;

import mx.gob.semarnat.mia.model.AnexosProyecto;
import mx.gob.semarnat.mia.model.EspecificacionAnexo;
import mx.gob.semarnat.mia.model.EstudiosEspProy;
import mx.gob.semarnat.mia.model.SustanciaAnexo;
import mx.gob.semarnat.mia.util.ConsultaUnion;



public class MiaDaoArchivos 
{
	
	private List<ConsultaUnion> ListaVisor;
	
	private List<AnexosProyecto> listaAnexos;
	
	
	
	public void init() throws Exception
	{
		System.out.println("Inicia método");
		ConsultaAnexos();
	}
	
	///Getter y setter de la lista de archivos
	  public List<ConsultaUnion> getListaVisor() {
		return ListaVisor;
	}

	public void setListaVisor(List<ConsultaUnion> listaVisor) {
		ListaVisor = listaVisor;
	}





	private EntityManager enMgr = PoolEntityManagers.getEmf().createEntityManager();
	  
	  //hace las consultas a la base 	  
	  //Hacemos cuatro listas para las 4 trablas
	  
	  //Tabla de anexosProyecto
	public List<AnexosProyecto> ConsultaAnexos() throws Exception {
        Query query = enMgr.createQuery("FROM CatNormaAsociacion");
        List<AnexosProyecto> catNormasAsociacion = (List<AnexosProyecto>)query.getResultList();
        return catNormasAsociacion;
    }
	
	//tABLA DE EstudiosEspProy
	public List<EstudiosEspProy> ConsultaEstudiosEspProy()
	{
		Query query = enMgr.createQuery("");
		List<EstudiosEspProy> espProys = (List<EstudiosEspProy>)query.getResultList();
		return espProys;
	}
	
	//tabla SustanciaAnexo
	public List<SustanciaAnexo>ConsultasSustanciasAnexos()
	{
		Query query = enMgr.createQuery("");
		List<SustanciaAnexo> sustanciaAnexos = (List<SustanciaAnexo>)query.getResultList();
		return sustanciaAnexos;		
	}
	
	//Tabla de EspecificacionAnexo
	
	public List<EspecificacionAnexo> ConsultaEspecificacionAnexo()
	{
		Query query = enMgr.createQuery("");
		List<EspecificacionAnexo> especificacionAnexos = (List<EspecificacionAnexo>)query.getResultList();
		return especificacionAnexos;
	}
	
	
	public String obtieneRuta()
	{
		Query query =enMgr.createQuery("SELECT A.ADJUNTOS FROM ");
		
		return "";
	}
	
	
	//Metodo para alta de archivos
	//INVOACADO DESDE EL MODAL DE PRIME
	 public void CargaArchivo(FileUploadEvent event) {
	        try {
	            File targetFolder = new File("/TEMP/share/allusers");
	            InputStream inputStream = event.getFile().getInputstream();
	            OutputStream out = new FileOutputStream(new File(targetFolder,
	                    event.getFile().getFileName()));
	            int read = 0;
	            byte[] bytes = new byte[1024];

	            while ((read = inputStream.read(bytes)) != -1) {
	                out.write(bytes, 0, read);
	            }
	            inputStream.close();
	            out.flush();
	            out.close();
	                        
	          // System.out.println(inputStream.toString()); // comentario de carga de acchivo......!!!!!!!!!!!!!!
	            
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    } 
	
	
}
