/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.Acuiferos;

/**
 *
 * @author marcog
 */
@SuppressWarnings("unused")
public class AcuiferosDAO extends AbstractBaseDAO<Acuiferos> {

	EntityManager mia = EntityFactory.getSigeia().createEntityManager();

	/**
	 * Instantiates a new AsuntoDocDAO impl.
	 */
	public AcuiferosDAO() {
		super(Acuiferos.class);
	}

	@SuppressWarnings("unchecked")
	public List<Acuiferos> findByFolioVersion(String folio, short serial) {
		List<Acuiferos> rtrn = null;
		try {
			Query q = null;
			q = mia.createNamedQuery("Acuiferos.findByFolioVersion");
			q.setParameter("serial", serial);
			q.setParameter("folio", folio);
			List<Object[]> listaObj = q.getResultList();

			if (listaObj != null) {
				rtrn = new ArrayList<Acuiferos>();
				for (Object[] arrayObj : listaObj) {
					Acuiferos acuiferos = new Acuiferos();
					acuiferos.setClvAcui((String) arrayObj[0]);
					acuiferos.setNomAcui((String) arrayObj[1]);
					acuiferos.setDescDispo((String) arrayObj[2]);
					acuiferos.setFechaDof((Date) arrayObj[3]);
					acuiferos.setSobreexp((String) arrayObj[4]);
					rtrn.add(acuiferos);
				}
			}

		} catch (PersistenceException px) {
			px.printStackTrace();
		}
		return rtrn;
	}

	/**
	 * Cuenta los registros
	 * 
	 * @param folio
	 * @return numero de registros
	 */
	public int countByFolioVersion(String folio, short serial) {
		int rtn=0;
		Query q = null;
		q = mia.createNamedQuery("Acuiferos.findByFolioVersion");
		q.setParameter("serial", serial);
		q.setParameter("folio", folio);
		List<Object[]> listaObj = q.getResultList();

		if (listaObj != null) {
			rtn=listaObj.size();
		}
		return rtn;
	}

	@SuppressWarnings("unchecked")
	public List<Acuiferos> findRangeFolioVersion(int[] range, String folio, short serial) {
		List<Acuiferos> rtrn = null;
		try {
			Query q = null;
			q = mia.createNamedQuery("Acuiferos.findByFolioVersion");
			q.setParameter("serial", serial);
			q.setParameter("folio", folio);
			q.setMaxResults(range[1] - range[0]);
			q.setFirstResult(range[0]);

			List<Object[]> listaObj = q.getResultList();

			if (listaObj != null) {
				rtrn = new ArrayList<Acuiferos>();
				for (Object[] arrayObj : listaObj) {
					Acuiferos acuiferos = new Acuiferos();
					acuiferos.setClvAcui((String) arrayObj[0]);
					acuiferos.setNomAcui((String) arrayObj[1]);
					acuiferos.setDescDispo((String) arrayObj[2]);
					acuiferos.setFechaDof((Date) arrayObj[3]);
					acuiferos.setSobreexp((String) arrayObj[4]);
					rtrn.add(acuiferos);
				}
			}

		} catch (PersistenceException px) {
			px.printStackTrace();
		}
		return rtrn;
	}

	@Override
	protected EntityManager getEntityManager() {
		return this.mia;
	}

}
