/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.io.Serializable;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author mauricio
 */
@SuppressWarnings("serial")
public class EntityFactory implements Serializable{

    private static EntityManagerFactory miaMF = Persistence.createEntityManagerFactory("DGIRA_MIAE");
    private static EntityManagerFactory sigeia = Persistence.createEntityManagerFactory("SIGEIA");
    

    public static EntityManagerFactory getSigeia() {
        return sigeia;
    }

    public static void setSigeia(EntityManagerFactory sigeia) {
        EntityFactory.sigeia = sigeia;
    }
    
    
    
    
    private static EntityManagerFactory catalogos = Persistence.createEntityManagerFactory("CATALOGOS");
    private static EntityManagerFactory sinatec = Persistence.createEntityManagerFactory("SINATEC");

    /**
     * @return the miaMF
     */
    public static EntityManagerFactory getMiaMF() {
        return miaMF;
    }

    /**
     * @param aMiaMF the miaMF to set
     */
    public static void setMiaMF(EntityManagerFactory aMiaMF) {
        miaMF = aMiaMF;
    }

    /**
     * @return the catalogos
     */
    public static EntityManagerFactory getCatalogos() {
        return catalogos;
    }

    /**
     * @param aCatalogos the catalogos to set
     */
    public static void setCatalogos(EntityManagerFactory aCatalogos) {
        catalogos = aCatalogos;
    }
    
    
    /**
     * @return the sinatec
     */
    public static EntityManagerFactory getSinatec() {
        return sinatec;
    }

    /**
     * @param aSinatec the sinatec to set
     */
    public static void setSinatec(EntityManagerFactory aSinatec) {
        sinatec = aSinatec;
    }
   

}
