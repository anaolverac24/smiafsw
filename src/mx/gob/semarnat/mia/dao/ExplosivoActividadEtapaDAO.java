package mx.gob.semarnat.mia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.ExplosivoActividadEtapa;
import mx.gob.semarnat.mia.model.ExplosivoActividadEtapaPK;


/**
*
* @author Paul Montoya
*/
public class ExplosivoActividadEtapaDAO  extends AbstractBaseDAO<ExplosivoActividadEtapa> {
    
    /**
     * EntityManager
     */
    // EntityManager mia=EntityFactory.getMiaMF().createEntityManager();
    EntityManager mia=EntityFactory.getSigeia().createEntityManager();

    /**
     * Instantiates a new ExplosivoActividadEtapaDAO impl.
     */
    public ExplosivoActividadEtapaDAO() {
        super(ExplosivoActividadEtapa.class);
    }
    
    /**
     * Returns the EntityManager
     * @return Em
     */
    @Override
    protected EntityManager getEntityManager() {
       return this.mia;
    }
    
    @SuppressWarnings("unchecked")
	public List<ExplosivoActividadEtapa> obtenerTodos() {
        List<ExplosivoActividadEtapa> rtrn=null;
        try{
            Query q = null;	
            q = mia.createNamedQuery("ExplosivoActividadEtapa.findAll");            
            rtrn = q.getResultList();
        }catch(PersistenceException px){
            px.printStackTrace();
        }
    	return rtrn;
    }
    
    
    public short getMaxExplosivoActividadEtapaProyecto(String folio, Short serial, Short etapaId, short actividadetapaid) {
        Short r = 0;
        try {
            Query q = mia.createQuery("SELECT max(e.explosivoActividadEtapaPK.explosivoEtapaId) FROM ExplosivoActividadEtapa e WHERE e.explosivoActividadEtapaPK.folioProyecto = :folio and e.explosivoActividadEtapaPK.serialProyecto = :serial and e.explosivoActividadEtapaPK.etapaId = :etapaId and e.explosivoActividadEtapaPK.actividadEtapaId = :actividadetapaid");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("etapaId", etapaId);
            q.setParameter("actividadetapaid", actividadetapaid);            
            r = (Short) q.getSingleResult();
        } catch (Exception e) {
        	e.printStackTrace();
        }
        if (r == null) {
            r = 0;
        }
        return r;
    }
    
    
    public long getMaxExplosivoConsecutivoPorProyecto(String folio, Short serial) {
        long r = 0;
        try {
            Query q = mia.createQuery("SELECT max(e.consecutivo) FROM ExplosivoActividadEtapa e WHERE e.explosivoActividadEtapaPK.folioProyecto = :folio and e.explosivoActividadEtapaPK.serialProyecto = :serial");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);                        
            r = (long) q.getSingleResult();
        } catch (Exception e) {
        	e.printStackTrace();
        	r = 0;
        }
        
        return r;
    }
    
    
    @SuppressWarnings("unchecked")
	public List<ExplosivoActividadEtapa> obtenerPorProyecto(String folio, short serial){
    	List<ExplosivoActividadEtapa> lista = null;
        try{
            Query q = null;	
            q = mia.createNamedQuery("ExplosivoActividadEtapa.findByFolioAndSerialProyecto");
            q.setParameter("folioProyecto", folio);
            q.setParameter("serialProyecto", serial);            
            lista = q.getResultList();

        }catch(PersistenceException px){
            px.printStackTrace();
        }
    	return lista;
    }
    
    public void eliminarExplosivoActividadEtapaHijo(String folio, short serial, short etapaId, short actividadEtapaId) throws Exception{
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM ExplosivoActividadEtapa c where c.explosivoActividadEtapaPK.folioProyecto = :folio and c.explosivoActividadEtapaPK.serialProyecto = :serial and c.explosivoActividadEtapaPK.etapaId = :etapaId and c.explosivoActividadEtapaPK.actividadEtapaId = :actividadEtapaId");
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);            
            q.setParameter("etapaId", etapaId);
            q.setParameter("actividadEtapaId", actividadEtapaId);            
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    public void eliminarExplosivoActividadEtapa(ExplosivoActividadEtapaPK explosivoActividadEtapaPK) throws Exception {
        try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM ExplosivoActividadEtapa c where c.explosivoActividadEtapaPK.folioProyecto = :folio and c.explosivoActividadEtapaPK.serialProyecto = :serial and c.explosivoActividadEtapaPK.etapaId = :etapa and c.explosivoActividadEtapaPK.actividadEtapaId = :actividadetapa and c.explosivoActividadEtapaPK.explosivoEtapaId = :id");
            q.setParameter("folio", explosivoActividadEtapaPK.getFolioProyecto());
            q.setParameter("serial", explosivoActividadEtapaPK.getSerialProyecto());
            q.setParameter("etapa", explosivoActividadEtapaPK.getEtapaId());
            q.setParameter("actividadetapa", explosivoActividadEtapaPK.getActividadEtapaId());
            q.setParameter("id", explosivoActividadEtapaPK.getExplosivoEtapaId());
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    public ExplosivoActividadEtapa getExplosivo(String folio, short serial, short explosivoEtapaId, short etapaId, short actividadEtapaId) {
        Query q = mia.createQuery("SELECT e FROM ExplosivoActividadEtapa e WHERE e.explosivoActividadEtapaPK.folioProyecto = :folio and e.explosivoActividadEtapaPK.serialProyecto =:serial and e.explosivoActividadEtapaPK.explosivoEtapaId =:explosivoEtapaId and e.explosivoActividadEtapaPK.etapaId =:etapaId and e.explosivoActividadEtapaPK.actividadEtapaId =:actividadEtapaId");
        q.setParameter("folio", folio);
        q.setParameter("serial", serial);
        q.setParameter("explosivoEtapaId", explosivoEtapaId);
        q.setParameter("etapaId", etapaId);
        q.setParameter("actividadEtapaId", actividadEtapaId);
        return (ExplosivoActividadEtapa) q.getSingleResult();
    }
    
    
    public void merge(Object object) throws Exception {
        try {
            mia.getTransaction().begin();
            object = mia.merge(object);
            mia.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    public void persist(Object object) throws Exception {
        try {
            System.out.println("Iniciando Transacción!");            
            mia.getTransaction().begin();
            System.out.println("Persistiendo datos!");
            mia.persist(object);
            System.out.println("Commit!");
            mia.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Rollback!");
            mia.getTransaction().rollback();
            throw e;
        } finally {
            System.out.println("Cerrando conexión!");
            //mia.close();
        }
    }

    

	
}
