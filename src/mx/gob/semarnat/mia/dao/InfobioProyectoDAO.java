/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.InfobioProyecto;
import mx.gob.semarnat.mia.model.catalogos.CatUnidadMedida;

/**
 *
 * @author César
 */
public class InfobioProyectoDAO extends AbstractBaseDAO<InfobioProyecto> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6771442503020599399L;
	/**
	 * EntityManager
	 */
	EntityManager mia = EntityFactory.getSigeia().createEntityManager();

	/**
	 * Instantiates a new ReiaProyObrasDAO impl.
	 */
	public InfobioProyectoDAO() {
		super(InfobioProyecto.class);
	}

	/**
	 * Returns the EntityManager
	 * 
	 * @return Em
	 */
	@Override
	protected EntityManager getEntityManager() {
		return this.mia;
	}

	/**
	 * Busca las ReiaProyObras por su Folio y categoria
	 * 
	 * @param folio
	 * @return lista de obras
	 */
	public List<InfobioProyecto> findByFolio(String folio) {
		List<InfobioProyecto> listaInfo = null;
		try {
			Query q = null;
			q = mia.createNamedQuery("InfobioProyecto.findByFolioProyecto");
			q.setParameter("folioProyecto", folio);

			listaInfo = q.getResultList();

		} catch (PersistenceException px) {
			px.printStackTrace();
		}
		return listaInfo;
	}
	
	/**
	 * Busca las ReiaProyObras por su Folio y categoria
	 * 
	 * @param folio
	 * @return lista de obras
	 */
	public InfobioProyecto consultarInfoBioPorId(String folio, short serial, short id) {
		InfobioProyecto infoBioConsultado = null;
		try {
			Query q = null;
			q = mia.createNamedQuery("InfobioProyecto.findInfoBioById");
			q.setParameter("folioProyecto", folio);
			q.setParameter("serialProyecto", serial);
			q.setParameter("infoBioId", id);

			infoBioConsultado = (InfobioProyecto) q.getSingleResult();

		} catch (PersistenceException px) {
			px.printStackTrace();
		}
		return infoBioConsultado;
	}
	
	/**
	 * Permite consultar el nombre de la unidad medida de la informacion biotecnologica a editar.
	 * @param nombreUnidad a editar
	 * @return la unidad medida seleccionada del catalogo.
	 */
	public CatUnidadMedida consultarCatUnidadMedidaByNombre(String nombreUnidad) {
		CatUnidadMedida catUnidadMedidadConsultado = null;
		try {
			Query q = null;
			q = mia.createQuery("from CatUnidadMedida where ctunDesc = :nombreUnidad");
			q.setParameter("nombreUnidad", nombreUnidad);

			catUnidadMedidadConsultado = (CatUnidadMedida) q.getSingleResult();

		} catch (PersistenceException px) {
			px.printStackTrace();
		}
		return catUnidadMedidadConsultado;
	}

	/**
	 * Cuenta los registros de obras por folio
	 * 
	 * @param folio
	 * @return numero de registros
	 */
	public int countByFolio(String folio) {
		Query q = null;
		q = mia.createNamedQuery("InfobioProyecto.countByFolioProyecto");
		q.setParameter("folioProyecto", folio);
		return ((Long) q.getSingleResult()).intValue();
	}

	@SuppressWarnings("unchecked")
	public List<InfobioProyecto> findByFolioRange(int[] range, String folio) {
		List<InfobioProyecto> listaInfo = null;
		try {
			Query q = null;
			q = mia.createNamedQuery("InfobioProyecto.findByFolioProyecto");
			q.setParameter("folioProyecto", folio);
			q.setMaxResults(range[1] - range[0]);
			q.setFirstResult(range[0]);
			listaInfo = q.getResultList();
		} catch (PersistenceException pe) {
			pe.printStackTrace();
		}
		return listaInfo;
	}

	public short getSequence() {
		short sequence = 0;
		try {
			Query q = mia.createNativeQuery("select INFOBIO_PROYECTO_SEQ.nextval as num from dual ");

			BigDecimal seq = (BigDecimal) q.getSingleResult();
			sequence=seq!=null?seq.shortValue():0;
		} catch (PersistenceException pe) {
			pe.printStackTrace();
		}
		return sequence;
	}

	public InfobioProyecto getByPK( short infoBioId) {
		InfobioProyecto infobioProyecto = null;
		try {
			Query q = null;
			q = mia.createNamedQuery("InfobioProyecto.findByInfoBioId");
			q.setParameter("infoBioId", infoBioId);
			infobioProyecto = (InfobioProyecto) q.getSingleResult();
		} catch (PersistenceException pe) {
			pe.printStackTrace();
		}
		return infobioProyecto;
	}
}
