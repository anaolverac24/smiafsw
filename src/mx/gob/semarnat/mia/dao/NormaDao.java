package mx.gob.semarnat.mia.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.CatLey;
import mx.gob.semarnat.mia.model.CatNorma;
import mx.gob.semarnat.mia.model.LeyFedEstProyecto;
import mx.gob.semarnat.mia.model.NormaProyecto;

public class NormaDao 
{
	 EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
	 
	 
	 public NormaProyecto consultarLeyProyectoPorId(short idnorma) {
	        Query q = mia.createQuery("FROM NormaProyecto where normaId = " + idnorma);
	        return (NormaProyecto) q.getSingleResult();
	    }
	 
	 
	 public int eliminarLeyFedEst(CatNorma idLeySeq) throws Exception {
	        try {
	            mia.getTransaction().begin();
	            Query q = mia.createQuery("DELETE FROM NormaProyecto where normaId = :idLeySeq");
	            q.setParameter("idLeySeq", idLeySeq);
	            int leyesEliminadas = q.executeUpdate();
	            mia.getTransaction().commit();
	            
	            return leyesEliminadas;
	        } catch (Exception e) {
	            mia.getTransaction().rollback();
	            throw e;
	        } finally {
	        }
	    }
	 
	 public void actualizarLeyFedProy(Object object) throws Exception {
	        mia.getTransaction().begin();
	        mia.merge(object);
	        mia.getTransaction().commit();		
		}
	 
	 public short consultarMaxIdLeySeq() {
			Query query = mia.createQuery("select max(normaId) from NormaProyecto");
			short idMax = (short) query.getSingleResult();
			return idMax;
		}
	 
	 public CatLey consultarLeyPorId(short idLeyFederal) {
	        Query q = mia.createQuery("FROM CatNorma where leyId = " + idLeyFederal);
	        return (CatLey) q.getSingleResult();
	    }
	 
}
