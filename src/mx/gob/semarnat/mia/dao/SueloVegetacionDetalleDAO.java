/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.SueloVegetacionDetalle;

/**
 * @author Cesar
 *
 */
public class SueloVegetacionDetalleDAO extends AbstractBaseDAO<SueloVegetacionDetalle>{

	/**
	 * EntityManager
	 */
	EntityManager mia=EntityFactory.getSigeia().createEntityManager();

	/**
	 * Constructor
	 */
	public SueloVegetacionDetalleDAO() {
		super(SueloVegetacionDetalle.class);
	}
	
	/**
	 * Obtiene la suma de las superficies capturadas en los detalles de suelo
	 * @param folioProyecto
	 * @param serialProyecto
	 * @param idSueloProyVegetacion
	 * @return suma de superficies
	 */
	public double obtenerSumaSuperficieActual(String folioProyecto,short serialProyecto, short idSueloProyVegetacion) {
		try {
			Query q = mia.createQuery("SELECT sum(s.superficie) FROM SueloVegetacionDetalle s where s.folioProyecto = :folio and"
					+ " s.serialProyecto = :serial and s.sueloVegProyId = :idSueloProyVeg");
	        q.setParameter("folio", folioProyecto);
	        q.setParameter("serial", serialProyecto);
	        q.setParameter("idSueloProyVeg", idSueloProyVegetacion);
	        return (double) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0.0;
	}
	
	/**
	 * getEntityManager
	 */
	@Override
    protected EntityManager getEntityManager() {
       return this.mia;
    }
}
