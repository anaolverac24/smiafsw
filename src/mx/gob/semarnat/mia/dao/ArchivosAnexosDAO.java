/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import java.io.File;
import java.nio.file.Files;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.ArchivosProyecto;

/**
 * @author dpaniagua.
 *
 */
public class ArchivosAnexosDAO {
	
    EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
	/**
	 * Permite consultar la lista de archivos por proyecto.
	 * @param folio del proyecto.
	 * @param serial del proyecto.
	 * @param capituloId
	 * @param subcapituloId
	 * @param seccionId
	 * @param apartadoId
	 * @return la lista de archivos del proyecto
	 */
	public List<ArchivosProyecto> consultarArchivosProyecto(String folio, Short serial, Short capituloId, Short subcapituloId, Short seccionId, Short apartadoId) {
        try {
            Query q = mia.createQuery("SELECT e FROM ArchivosProyecto e "
            						+ "WHERE e.folioSerial = :folio and e.serialProyecto = :serial "
            						+ "and e.capituloId = :capituloId "
            						+ "and e.subCapituloId = :subcapituloId "
            						+ "and e.seccionId = :seccionId "
            						+ "and e.apartadoId = :apartadoId "
            						+ "order by e.id");
            
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);
            q.setParameter("capituloId", capituloId);
            q.setParameter("subcapituloId", subcapituloId);
            q.setParameter("seccionId", seccionId);
            q.setParameter("apartadoId", apartadoId);
            List<ArchivosProyecto> archivosProyecto = q.getResultList();
            
            return archivosProyecto;
        } catch (Exception e) {
        	e.printStackTrace();
        	return null;
        }
    }
	
	/**
	 * Permite editar el archivo del anexo del proyecto.
	 * @param object a editar.
	 */
	public void editarArchivoProyecto(Object object) {
        try {
            mia.getTransaction().begin();
            object = mia.merge(object);
            mia.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }	
	/**
	 * Permite eliminar el/los archivos del proyecto seleccionado.
	 * @param folio
	 * @param serial
	 * @param capituloId
	 * @param subcapituloId
	 * @param seccionId
	 * @param apartadoId
	 * @param id
	 * @throws Exception en caso de error al eliminar el archivo anexo del proyecto.
	 */
    public int eliminarArchivoProyecto(String folio, short serial, short capituloId, short subcapituloId, 
    		short seccionId, short apartadoId, short id) throws Exception {

    	try {
            mia.getTransaction().begin();
            
            //Eliminacion de Archivos en FileSystem
            Query q1 = mia.createQuery("SELECT a FROM ArchivosProyecto a WHERE a.folioSerial = :folio and a.serialProyecto = :serial "
            		+ "and a.capituloId = :capituloId "
					+ "and a.subCapituloId = :subcapituloId "
					+ "and a.seccionId = :seccionId "
					+ "and a.apartadoId = :apartadoId and a.id = :id");
            q1.setParameter("folio", folio);
            q1.setParameter("serial", serial);  
            q1.setParameter("capituloId", capituloId);
            q1.setParameter("subcapituloId", subcapituloId);
            q1.setParameter("seccionId", seccionId);
            q1.setParameter("apartadoId", apartadoId);
            q1.setParameter("id", id);
            
            ArchivosProyecto archivoProy = (ArchivosProyecto) q1.getSingleResult();
        	eliminaArchivoRemoto(archivoProy.getUrl());
        	System.out.println("Archivo eliminado del sistema: " + archivoProy.getUrl());
            
            Query q = mia.createQuery("DELETE FROM ArchivosProyecto e "
            						+ "WHERE e.folioSerial = :folio and e.serialProyecto = :serial "
            						+ "and e.capituloId = :capituloId "
            						+ "and e.subCapituloId = :subcapituloId "
            						+ "and e.seccionId = :seccionId "
            						+ "and e.apartadoId = :apartadoId and e.id = :id");
            
            q.setParameter("folio", folio);
            q.setParameter("serial", serial);            
            q.setParameter("capituloId", capituloId);
            q.setParameter("subcapituloId", subcapituloId);
            q.setParameter("seccionId", seccionId);
            q.setParameter("apartadoId", apartadoId);
            q.setParameter("id", id);
            
            int registrosEliminados = q.executeUpdate();
            
            mia.getTransaction().commit();
            
            return registrosEliminados;
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
//            mia.close();
        }
    }
    
    //Borra archivo desde file system   
    private static void eliminaArchivoRemoto(String rutaArch) {
        try {
            File file = new File(rutaArch);
            Files.deleteIfExists(file.toPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
	/**
	 * Permite consultar el valor maximos del idLeySeq del proyecto.
	 */
	public Short consultarMaxIdSeq() {
		EntityManager mia = EntityFactory.getMiaMF().createEntityManager();
		Query query = mia.createQuery("select max(seqId) from ArchivosProyecto");
		Short idMax = (Short) query.getSingleResult();
		return idMax;
	}
}
