/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.CatEspeciesAcuiferos;
import mx.gob.semarnat.mia.model.InfobioProyecto;

/**
 *
 * @author César
 */
public class CatEspeciesAcuiferosDAO extends AbstractBaseDAO<CatEspeciesAcuiferos> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7415753444501562681L;
	/**
	 * EntityManager
	 */
	EntityManager mia = EntityFactory.getSigeia().createEntityManager();

	/**
	 * Instantiates a new ReiaProyObrasDAO impl.
	 */
	public CatEspeciesAcuiferosDAO() {
		super(CatEspeciesAcuiferos.class);
	}

	/**
	 * Returns the EntityManager
	 * 
	 * @return Em
	 */
	@Override
	protected EntityManager getEntityManager() {
		return this.mia;
	}

}
