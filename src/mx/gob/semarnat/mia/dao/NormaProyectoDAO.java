/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import javax.persistence.EntityManager;

import mx.gob.semarnat.mia.model.NormaProyecto;

/**
 *
 * @author 
 */
public class NormaProyectoDAO extends AbstractBaseDAO<NormaProyecto> {
    
    /**
     * EntityManager
     */
    EntityManager mia=EntityFactory.getSigeia().createEntityManager();

    /**
     * Instantiates a new ReiaProyObrasDAO impl.
     */
    public NormaProyectoDAO() {
        super(NormaProyecto.class);
    }
    
    /**
     * Returns the EntityManager
     * @return Em
     */
    @Override
    protected EntityManager getEntityManager() {
       return this.mia;
    }
    
    
   
    
}
