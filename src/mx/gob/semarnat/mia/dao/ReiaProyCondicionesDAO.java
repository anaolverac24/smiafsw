/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.ReiaCondiciones;
import mx.gob.semarnat.mia.model.ReiaObrasCondiciones;
import mx.gob.semarnat.mia.model.ReiaProyCondiciones;
import mx.gob.semarnat.mia.model.ReiaProyObras;

/**
 *
 * @author Cesar
 */
public class ReiaProyCondicionesDAO extends AbstractBaseDAO<ReiaProyCondiciones> {
    
    /**
     * EntityManager
     */
    EntityManager mia=EntityFactory.getSigeia().createEntityManager();

    /**
     * Instantiates a new ReiaProyCondicionesDAO impl.
     */
    public ReiaProyCondicionesDAO() {
        super(ReiaProyCondiciones.class);
    }
    
    /**
     * Busca las ReiaProyCondiciones por claveObraProy y idObra
     * @param claveObraProy
     * @param idObra
     * @return lista de ReiaProyCondiciones
     */
    public List<ReiaProyCondiciones> findByClaveObra(Long claveObraProy, Long idObra) {
    	List<ReiaProyCondiciones> listaCondicionesObra = null;
    	
    	ReiaCondicionesDAO reiaCondicionesDao = new ReiaCondicionesDAO();
    	List<ReiaObrasCondiciones> listaCondiciones = reiaCondicionesDao.findCondicionesPorObra(idObra);
    	
    	if (listaCondiciones != null) {
    		listaCondicionesObra = new ArrayList<ReiaProyCondiciones>();
    		for (ReiaObrasCondiciones reiaCondicion : listaCondiciones) {
    			ReiaProyCondiciones reiaProyCondiciones = this.findByClaveCondicion(claveObraProy, reiaCondicion.getIdCondicion().getIdCondicion());
				listaCondicionesObra.add(reiaProyCondiciones);
			}
    	}
    	
    	return listaCondicionesObra;    	
    }
    
    /**
     * Busca una ReiaProyCondiciones por claveObraProy y idCondicion
     * @param claveObraProy
     * @param idCondicion
     * @return ReiaProyCondiciones
     */
    public ReiaProyCondiciones findByClaveCondicion(Long claveObraProy, Long idCondicion) {
    	ReiaProyCondiciones rtrn=null;
        try{
            Query q = null;	
            q = mia.createNamedQuery("ReiaProyCondiciones.findByClaveCondicion");
            q.setParameter("claveObraProy", claveObraProy);
            q.setParameter("idCondicion", idCondicion);
            List<ReiaProyCondiciones> results = q.getResultList();
            if (results != null ){
            	rtrn = results.get(0);
            }            

        }catch(PersistenceException px){
            px.printStackTrace();
        }
    	return rtrn;
    }
    
    /**
     * Returns the EntityManager
     * @return Em
     */
    @Override
    protected EntityManager getEntityManager() {
       return this.mia;
    }

    /**
     * Busca en ReiaProyCondiciones la cantidad de condiciones de la Obra
     * @param claveObraProy
     * @return ReiaProyCondiciones
     */
	public Long countCondiciones(Long claveObraProy) {
		Query q = mia.createQuery("SELECT COUNT(r) FROM ReiaProyCondiciones r WHERE r.claveObraProy.clave = :claveObraProy");
        q.setParameter("claveObraProy", claveObraProy);
        Long res = (Long) q.getSingleResult();
        return res;
	}

	/**
     * Elimina en ReiaProyCondiciones la cantidad de condiciones de la Obra
     * @param claveObraProy
     * @return ReiaProyCondiciones
     */
	public void eliminarExplosivoActividadEtapaHijo(Long claveObraProy) {
		try {
            mia.getTransaction().begin();
            Query q = mia.createQuery("DELETE FROM ReiaProyCondiciones r WHERE r.claveObraProy.clave = :claveObraProy");
            q.setParameter("claveObraProy", claveObraProy);
            q.executeUpdate();
            mia.getTransaction().commit();
        } catch (Exception e) {
            mia.getTransaction().rollback();
            throw e;
        } finally {
        }
	}
	
}
