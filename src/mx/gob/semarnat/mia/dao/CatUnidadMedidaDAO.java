/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.catalogos.CatUnidadMedida;

/**
 * @author dpaniagua.
 *
 */
public class CatUnidadMedidaDAO {

    EntityManager cat = EntityFactory.getCatalogos().createEntityManager();	
	
    /**
     * Permite consultar las unidades de medida que se encuentren en M2, KM2 y HA.
     * @return la lista de Unidad de Medida.
     */
	public List<CatUnidadMedida> consultarUnidadesMedida() throws Exception {
        Query q = cat.createQuery("from CatUnidadMedida "
        						+ "where ctunClve in (54,57,56) "
        						+ "order by ctunClve");
        
        return q.getResultList();
	}
}
