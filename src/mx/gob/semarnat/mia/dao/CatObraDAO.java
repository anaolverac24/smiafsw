/**
 * 
 */
package mx.gob.semarnat.mia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import mx.gob.semarnat.mia.model.CatObra;

/**
 * @author dpaniagua.
 *
 */
public class CatObraDAO {
    EntityManager mia = EntityFactory.getMiaMF().createEntityManager();

    /**
     * Permite consultar la lista de obras de SIGEIA.
     * @return la lista de obras.
     * @throws Exception en caso de error al consultar la lista de obras actividad de SIGEIA.
     */
    public List<CatObra> consultarObraActividadSIGEIA() throws Exception {
		Query query = mia.createQuery("from CatObra order by obraDescripcion");
		List<CatObra> listaCatObra = query.getResultList();
		
		return listaCatObra;
	}
}
