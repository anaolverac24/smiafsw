/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;

import mx.gob.semarnat.mia.model.AnexosProyecto;
import mx.gob.semarnat.mia.model.ArchivosProyecto;
import mx.gob.semarnat.mia.model.EspecificacionAnexo;
import mx.gob.semarnat.mia.model.EstudiosEspProy;
import mx.gob.semarnat.mia.model.SustanciaAnexo;
import mx.gob.semarnat.mia.view.ArchivoBase;

/**
 *
 * @author efrainc
 */
@ManagedBean
public class ArchivoBean extends ArchivoBase {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3213037381217208435L;

	/**
     * Creates a new instance of ArchivosBean
     */
    public ArchivoBean() 
    {        
    }
    
    private ArchivosDao obj;    
    private List<Object[]> lista;
    private List<SustanciaAnexo> listaSustancia;
    private String folioProyecto;	
	private StreamedContent file;
	
	Short serialProyecto = (short) 0;
    Short normaId = (short) 0;

    short capituloId = (short) 0;
    short subcapituloId = (short) 0;
    short seccionId = (short) 0;
    short apartadoId = (short) 0;
    short sustanciaId = (short) 0;
    short estudioId = (short) 0;
	
	
	AnexosProyecto anexos;
	EstudiosEspProy estuproy;
	SustanciaAnexo  SusAnexos;
	
	
	 List<Map<String, String>> adjuntos = new ArrayList<Map<String, String>>();
    
    @PostConstruct
    public void init() {
        FacesContext context = FacesContext.getCurrentInstance();
        this.folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        ArchivosDao linkDao = new ArchivosDao();
        this.lista = linkDao.ObtenerResumen(folioProyecto);
        
    }
    
    public ArchivosDao getObj() {
        return obj;
    }

    public void setObj(ArchivosDao obj) {
        this.obj = obj;
    }

    public List<Object[]> getLista() {        
        return this.lista;
    }

    public void setLista(List<Object[]> lista) {
        this.lista = lista;
    }

    public List<SustanciaAnexo> getListaSustancia(String folioProyecto, Short serialProyecto, Short sustanciaProyId) {
        ArchivoBean linkDao  = new ArchivoBean();
        listaSustancia = linkDao.getListaSustancia(folioProyecto, serialProyecto, sustanciaProyId);
         return listaSustancia;
    }

    public void setListaAnexos(List<SustanciaAnexo> listaSustancia) {
        this.listaSustancia = listaSustancia;
    }
    
    
    
     
    
    //Carga archivo a file System       
    public void CargaArchivo(FileUploadEvent event) {	
        try {
        	
        	String ruta = obj.ObtieneRuta();      	
        	
            File targetFolder = new File(ruta);
            InputStream inputStream = event.getFile().getInputstream();
            OutputStream out = new FileOutputStream(new File(targetFolder,
                    event.getFile().getFileName()));
            String val = event.getFile().getFileName();
            
            String archivo[] = val.split("\\.");
            
            String nombre = archivo[0].toString();
            String extencion = archivo[1].toString();
            
            obj.setNombreArchivo(val);
            
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
           
            inputStream.close();
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    
    
    
    
    public StreamedContent prueba(String seqId) {
    	short id = Short.parseShort(seqId);
//    	return ver_archivo(id);
    	return null;
    }
    
    /**
     * Muestra un archivo en base a us id secuencial
     * @param SeqId del archivo en la BD
     */
    public void ver_archivo(short SeqId)
    {
    	System.out.println("ver_archivo... " + SeqId);
    	response = null;
        EntityManager em = EntityFactory.getMiaMF().createEntityManager();
        String filePath = "";
        String ext = "";
        
        Query q = em.createQuery("SELECT a FROM ArchivosProyecto a WHERE a.seqId = :seqId");
        q.setParameter("seqId", SeqId);
        
        try {
        	ArchivosProyecto e = (ArchivosProyecto) q.getSingleResult();
        	filePath = e.getUrl();
            ext = e.getExtension().toLowerCase().trim();   	
            File downloadFile = new File(filePath);
            
            String mimeType = "";
            String responseHeaderType = "";
            
            //Se valida si la extencion del archivo cumple para mostrar el contenido en el browser
            if (ext.equals("pdf") || ext.equals("jpg") || ext.equals("png") || ext.equals("gif")) {
            	if (ext.equals("jpg") || ext.equals("png") || ext.equals("gif")) {
            		mimeType = "image/" + ext;
		        }
		        if (ext.equals("pdf")) {
		        	  mimeType = "application/pdf";
		        }
		        responseHeaderType = "inline";
            } else { 
            	mimeType = "application/octet";
            	responseHeaderType = "attachment";
            }          
            processRequest(mimeType, responseHeaderType, downloadFile.getPath(), e.getFileName(), e.getExtension());
            FacesContext.getCurrentInstance().responseComplete();
        } catch (IllegalStateException illegalException) {
        	System.out.println("Excepcion de tipo IllegalStateException controloda.");
    	} catch (Exception err) {
            System.out.println("Excepcion al ver el archivo: " + err.toString());            
        }
    }
            
    //Eliminar Archivo    
    public void eliminar_archivo(String id, String tabla, String fol, String serie) throws IOException
    {
    	HttpServletRequest request; 
    	response = null;
        String idt = id;
        String tipo = tabla; 
        String folio = fol; 
        String serial = serie; 
        String nombre = ";";
        EntityManager em = EntityFactory.getMiaMF().createEntityManager();
        String filePath = "";
        String anexoUrl;
        
        
        if (tipo.equals("general")) {
            try {
                
                em.getTransaction().begin();

                //Obtención de la ruta para su eliminación en el sitio ftp
                Query q = em.createQuery("SELECT a.anexoUrl FROM AnexosProyecto a WHERE a.anexosProyectoPK.anexoId = :idt");
                q.setParameter("idt", Short.valueOf(idt));
                anexoUrl = (String)q.getSingleResult();
                eliminaArchivoRemoto(anexoUrl);

                //Eliminación en BD
                q = em.createQuery("DELETE FROM AnexosProyecto a WHERE a.anexosProyectoPK.anexoId = :idt");
                q.setParameter("idt", Short.valueOf(idt));
                q.executeUpdate();
                em.getTransaction().commit();
                em.close();
            } catch (Exception e) {
                System.out.println("Ocurrió un error durante la eliminación del archivo");
                e.printStackTrace();
            }
        } else if (tipo.equals("anexo_sustancia")) {
            try {
                
                em.getTransaction().begin();
                
                //Obtención de la ruta para su eliminación en el sitio ftp
                Query q = em.createQuery("SELECT a.anexoUrl FROM SustanciaAnexo a WHERE a.sustanciaAnexoPK.sustanciaAnexoId = :idt");
                q.setParameter("idt", Short.valueOf(idt));
                anexoUrl = (String)q.getSingleResult();
                eliminaArchivoRemoto(anexoUrl);

                //Eliminación en BD
                q = em.createQuery("DELETE FROM SustanciaAnexo a WHERE a.sustanciaAnexoPK.sustanciaAnexoId = :idt");
                q.setParameter("idt", Short.valueOf(idt));
                q.executeUpdate();
                em.getTransaction().commit();
                em.close();
            } catch (Exception e) {
                System.out.println("Ocurrió un error durante la eliminación del archivo");
                e.printStackTrace();
            }
        } else if (tipo.equals("anexo_estudio")) {
            try {
                 
                em.getTransaction().begin();
                
                //Obtención de la ruta para su eliminación en el sitio ftp
                Query q = em.createQuery("SELECT a.anexoUrl FROM EstudiosEspProy a "
                        + " WHERE a.estudiosEspProyPK.estudioId =:idt "
                        + "   AND a.estudiosEspProyPK.folioSerial =:folio "
                        + "   AND a.estudiosEspProyPK.serialProyecto =:serial ");
                q.setParameter("idt", Short.valueOf(idt));
                q.setParameter("folio", folio);
                q.setParameter("serial", Short.valueOf(serial));
                anexoUrl = (String)q.getSingleResult();
                eliminaArchivoRemoto(anexoUrl);

                //Eliminación en BD
                q = em.createQuery("UPDATE EstudiosEspProy a "
                        + " SET a.anexoUrl = null, a.anexoDescripcion = null, a.anexoNombre = null, a.anexoTamanio = null, a.anexoExtension = null "
                        + " WHERE a.estudiosEspProyPK.estudioId =:idt "
                        + "   AND a.estudiosEspProyPK.folioSerial =:folio "
                        + "   AND a.estudiosEspProyPK.serialProyecto =:serial ");
                q.setParameter("idt", Short.valueOf(idt));
                q.setParameter("folio", folio);
                q.setParameter("serial", Short.valueOf(serial));
                q.executeUpdate();
                em.getTransaction().commit();
                em.close();
            } catch (Exception e) {
                System.out.println("Ocurrió un error durante la eliminación del archivo");
                e.printStackTrace();
            }
        }

        
    }
           
    //Borra archivo desde file system   
    private static void eliminaArchivoRemoto(String rutaArch) {
        try {
            File file = new File(rutaArch);
            Files.deleteIfExists(file.toPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Elimina un ArchivosProyecto por su id consecutivo
     * @param seqId
     */
    public static void eliminarArchivoProyecto(short seqId) {
    	EntityManager em = EntityFactory.getMiaMF().createEntityManager();
    	
        Query q = em.createQuery("SELECT a FROM ArchivosProyecto a WHERE a.seqId = :seqId");
        q.setParameter("seqId", seqId);
        
        ArchivosProyecto archivoProy = (ArchivosProyecto) q.getSingleResult();
        try {
            em.getTransaction().begin();
            em.remove(archivoProy);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
    	System.out.println("Archivo eliminado de la BD.");
    	
    	eliminaArchivoRemoto(archivoProy.getUrl());
    	System.out.println("Archivo eliminado del sistema: " + archivoProy.getUrl());
    }

    
    public void GuardaBase(String tipo, String folioProyecto, short serialProyecto, String especificacionId, short normaId, short especificacionAnexoId, short capituloId, short subcapituloId, short seccionId, short apartadoId)
    {

		int i = 0;
		 
		for (Map<String, String> a : adjuntos) 
		{
			System.out.println("tipo " + tipo);
			
			System.out.println("guardando anexo_especificacion");
			short idanexo = obj.numRegAnexoEspe(folioProyecto , serialProyecto);
			EspecificacionAnexo tmp = new EspecificacionAnexo(folioProyecto, serialProyecto, especificacionId, normaId, idanexo);
			
			System.out.println("tmp " + tmp.getEspecificacionAnexoPK().toString());
			
			tmp.setEspecAnexoDescripcion(anexos.getAnexoDescripcion());
		
			tmp.setEspecAnexoUrl(obj.ObtieneRuta());
		
			String[] tnomext = obj.getNombreArchivo().split("\\.");
			
			tmp.setEspecAnexoExtension(tnomext[tnomext.length - 1]);
			tmp.setEspecAnexoNombre(anexos.getAnexoNombre());
			VisorDao.persist(tmp);		
			i++;
		}
	}

    
    public void ver_archivo2(short SeqId)
    {
    	System.out.println("ver_archivo... " + SeqId);
    	response = null;
        EntityManager em = EntityFactory.getMiaMF().createEntityManager();
        String filePath = "";
        String ext = "";
        
        Query q = em.createQuery("SELECT a FROM EstudiosEspProy a WHERE a.estudioId = :seqId");
        q.setParameter("seqId", SeqId);
        
        try {
        	EstudiosEspProy e = (EstudiosEspProy) q.getSingleResult();
        	filePath = e.getAnexoUrl();
            ext = e.getAnexoExtension().toLowerCase().trim();   	
            File downloadFile = new File(filePath);
            
            String mimeType = "";
            String responseHeaderType = "";
            
            //Se valida si la extencion del archivo cumple para mostrar el contenido en el browser
            if (ext.equals("pdf") || ext.equals("jpg") || ext.equals("png") || ext.equals("gif")) {
            	if (ext.equals("jpg") || ext.equals("png") || ext.equals("gif")) {
            		mimeType = "image/" + ext;
		        }
		        if (ext.equals("pdf")) {
		        	  mimeType = "application/pdf";
		        }
		        responseHeaderType = "inline";
            } else { 
            	mimeType = "application/octet";
            	responseHeaderType = "attachment";
            }          
            processRequest(mimeType, responseHeaderType, downloadFile.getPath(), e.getAnexoNombre(), e.getAnexoExtension());
            FacesContext.getCurrentInstance().responseComplete();
        } catch (IllegalStateException illegalException) {
        	System.out.println("Excepcion de tipo IllegalStateException controloda.");
    	} catch (Exception err) {
            System.out.println("Excepcion al ver el archivo: " + err.toString());            
        }
    }
       
    
    
    
	/**
	 * @return the file
	 */
	public StreamedContent getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(StreamedContent file) {
		this.file = file;
	}
}
