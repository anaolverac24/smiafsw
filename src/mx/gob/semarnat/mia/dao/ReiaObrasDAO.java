/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import mx.gob.semarnat.mia.model.ReiaObras;

/**
 *
 * @author marcog
 */
@SuppressWarnings("unused")
public class ReiaObrasDAO extends AbstractBaseDAO<ReiaObras> {
    
    EntityManager mia=EntityFactory.getSigeia().createEntityManager();

	/**
	 * Instantiates a new AsuntoDocDAO impl.
	 */
	public ReiaObrasDAO() {
		super(ReiaObras.class);
	}
        
        
    @SuppressWarnings("unchecked")
	public List<ReiaObras> obtenerObrasPorCategoria(Long categoria) {
        List<ReiaObras> rtrn=null;
        try{
            Query q = null;	
            q = mia.createNamedQuery("ReiaObras.getByCategoria");
            q.setParameter("categoria", categoria);                   
            rtrn = q.getResultList();

        }catch(PersistenceException px){
            px.printStackTrace();
        }
        return rtrn;
    }

    @Override
    protected EntityManager getEntityManager() {
       return this.mia;
    }
        
}
