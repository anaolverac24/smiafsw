/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import mx.gob.semarnat.mia.model.AnexosProyecto;
import mx.gob.semarnat.mia.model.CatCapitulo;
import mx.gob.semarnat.mia.model.CatSeccion;
import mx.gob.semarnat.mia.model.CatSubcapitulo;
import mx.gob.semarnat.mia.model.EspecificacionAnexo;
import mx.gob.semarnat.mia.model.EstudiosEspProy;
import mx.gob.semarnat.mia.model.SustanciaAnexo;

/**
 *
 * @author mauricio
 */
public class VisorDao implements Serializable {

    public static EntityManagerFactory emf = EntityFactory.getMiaMF();
    
    public Object getObject(Class <?> tipo, Object id){
        return emf.createEntityManager().find(tipo, id);
    }
    
    public static List<SustanciaAnexo> getSustanciaAnexos(String folioProyecto, Short serialProyecto, Short sustanciaProyId) {
        EntityManager em = emf.createEntityManager();
        List<SustanciaAnexo> hs;
            if(sustanciaProyId!=null){
                Query q = em.createQuery("SELECT s FROM SustanciaAnexo s WHERE s.sustanciaAnexoPK.folioProyecto = :folio AND s.sustanciaAnexoPK.serialProyecto = :serial AND s.sustanciaAnexoPK.sustanciaProyId= :id");
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                q.setParameter("id", sustanciaProyId);
                hs = q.getResultList();
            }else{
                Query q = em.createQuery("SELECT s FROM SustanciaAnexo s WHERE s.sustanciaAnexoPK.folioProyecto = :folio AND s.sustanciaAnexoPK.serialProyecto= :serial");
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                hs = q.getResultList();
            }
            return hs;
    }
    public short numRegAnexoSust() {
        int id = 0;
        try{
            EntityManager em = emf.createEntityManager();
            Query q = em.createQuery("SELECT max(s.sustanciaAnexoPK.sustanciaAnexoId) FROM SustanciaAnexo s");
            id = (Short)q.getSingleResult();
            em.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return (short) id;
    }
    public short numRegAnexoEst(String folio,Short serial) {
        int id = 0;
        try{
            EntityManager em = emf.createEntityManager();
            Query q = em.createQuery("SELECT max(s.estudiosEspProyPK.estudioId) FROM EstudiosEspProy s WHERE s.estudiosEspProyPK.folioSerial = :folio AND s.estudiosEspProyPK.serialProyecto = :serial");
            q.setParameter("folio",folio);
            q.setParameter("serial",serial);
            id = (Short)q.getSingleResult();
            em.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return (short) id;
    }
    public static void merge(Object object) {
        EntityManager em = EntityFactory.getMiaMF().createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
//            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
        } finally {
        }
    }
    public static void mergeEst(Object object) {
        EntityManager em = EntityFactory.getMiaMF().createEntityManager();
        try {
            em.getTransaction().begin();
            em.merge(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
//            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
        } finally {
        }
    }

    public short numRegAnexoEspe(String folioProyecto, short serialProyecto, String especifiacionId) {
        short r = 0;
                try {
            EntityManager em = emf.createEntityManager();
            Query q = em.createQuery("SELECT max(a.especificacionAnexoPK.especificacionId) FROM EspecificacionAnexo a Where a.especificacionAnexoPK.folioProyecto = :folio and a.especificacionAnexoPK.serialProyecto = :serial and a.especificacionAnexoPK.especificacionId = :especificacion");
            q.setParameter("folio", folioProyecto);
            q.setParameter("serial", serialProyecto);
            q.setParameter("especificacion", especifiacionId);
            r = (Short) q.getSingleResult();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (short) r;
    }

    public short numRegAnexo(short capituloId, short subcapituloId, short seccionId, short apartadoId, String folioProyecto, short serialProyecto) {
        int r = 0;
        try {
            EntityManager em = emf.createEntityManager();
            Query q = em.createQuery("SELECT max(a.id) FROM ArchivosProyecto a WHERE a.folioSerial = :folio ");// a WHERE a.anexosProyectoPK.apartadoId = :apartado and a.anexosProyectoPK.capituloId = :capitulo and a.anexosProyectoPK.folioProyecto = :folio and a.anexosProyectoPK.serialProyecto = :serial and a.anexosProyectoPK.seccionId = :seccion and a.anexosProyectoPK.subcapituloId = :subcapitulo");
//            q.setParameter("apartado", apartadoId);
//            q.setParameter("capitulo", capituloId);
              q.setParameter("folio", folioProyecto);
//            q.setParameter("serial", serialProyecto);
//            q.setParameter("seccion", seccionId);
//            q.setParameter("subcapitulo", subcapituloId);
            r = (Short) q.getSingleResult();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (short) r;
    }
    
    public ArrayList<Object[]> obtenerAdjuntos(String folio, short serial) {
        ArrayList<Object[]> lstAdjuntos = null;

        try {
            EntityManager em = emf.createEntityManager();
            Query q = em.createNativeQuery(
               "select * from " + 
			"( " +
                        "    select coalesce(to_char(AP2.descripcion), '') as descripcion, concat(concat(AP2.filename,'.'),AP2.extension) as nombre " + 
			"        from archivos_proyecto AP2  " + 
			"        where AP2.folio_serial = :folio  " + 
			"          and AP2.serial_proyecto = :serial  " + 
			"    union all " + 
			"    select coalesce(to_char(AP.anexo_descripcion), '') as descripcion, AP.anexo_nombre as nombre " + 
			"        from anexos_proyecto AP  " + 
			"        where AP.folio_proyecto = :folio  " + 
			"          and AP.serial_proyecto = :serial  " + 
			"    union all " + 
			"    select coalesce(to_char(EA.espec_anexo_descripcion), '') as descripcion, EA.espec_anexo_nombre as nombre " + 
			"        from especificacion_anexo EA  " + 
			"        where EA.folio_proyecto = :folio  " + 
			"          and EA.serial_proyecto = :serial  " + 
			"    union all " + 
			"    select coalesce(to_char(SA.anexo_desc), '') as descripcion, SA.anexo_nombre as nombre " + 
			"        from sustancia_anexo SA  " + 
			"        where SA.folio_proyecto = :folio  " + 
			"          and SA.serial_proyecto = :serial  " + 
			"    union all " + 
			"    select coalesce(to_char(EE.anexo_descripcion), '') as descripcion, concat(concat(EE.anexo_nombre,'.'),EE.anexo_extension) as nombre  " + 
			"        from estudios_esp_proy EE  " + 
			"        where EE.folio_serial = :folio  " + 
			"          and EE.serial_proyecto = :serial  " +
                        "          and EE.anexo_nombre is not null " + 
			") "            
            );
            
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            lstAdjuntos = (ArrayList<Object[]>)q.getResultList();
            em.close();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return lstAdjuntos;
    }
    
    public Long conteoAdjuntos(String folio, short serial, String nombre) {
        BigDecimal dRes = BigDecimal.ZERO;
        String strNombre = null;
        boolean bNombre = nombre != null;
        if (bNombre) {
            strNombre = "%" + nombre;
        }
        try {
            
            EntityManager em = emf.createEntityManager();
            Query q = em.createNativeQuery(
			"select sum(conteo) from " + 
			"( " +
                        "    select count (*) as conteo  " + 
			"        from especificacion_anexo EA  " + 
			"        where EA.folio_proyecto = :folio  " + 
			"          and EA.serial_proyecto = :serial  " + 
                        (bNombre ? "          and EA.espec_anexo_nombre like :nombre " : "") + 
			"    union all " +
			"    select count (*) as conteo  " + 
			"        from anexos_proyecto AP  " + 
			"        where AP.folio_proyecto = :folio  " + 
			"          and AP.serial_proyecto = :serial  " + 
			(bNombre ? "          and AP.anexo_nombre like :nombre " : "") + 
			"    union all " + 
			"    select count (*) as conteo  " + 
			"        from archivos_proyecto AP2  " + 
			"        where AP2.folio_serial = :folio  " + 
			"          and AP2.serial_proyecto = :serial  " + 
                        (bNombre ? "          and EA.espec_anexo_nombre like :nombre " : "") + 
			"    union all " + 
			"    select count (*) as conteo  " + 
			"        from sustancia_anexo SA  " + 
			"        where SA.folio_proyecto = :folio  " + 
			"          and SA.serial_proyecto = :serial  " + 
                        (bNombre ? "          and SA.anexo_nombre like :nombre " : "") +
			"    union all " + 
			"    select count (*) as conteo  " + 
			"        from estudios_esp_proy EE  " + 
			"        where EE.folio_serial = :folio  " + 
			"          and EE.serial_proyecto = :serial  " +
                        "          and EE.anexo_nombre is not null " +                         
                        (bNombre ? "          and EE.anexo_nombre like :nombre " : "") +                                
			") "            
            );
         
            q.setParameter("serial", serial);
            q.setParameter("folio", folio);
            if (bNombre) {
                q.setParameter("nombre", strNombre);
            }
            dRes = (BigDecimal) q.getSingleResult();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dRes.longValue();
    }    

    public static List<AnexosProyecto> getAnexos(short capituloId, short subcapituloId, short seccionId, short apartadoId, String folioProyecto, short serialProyecto) {
        List<AnexosProyecto> r = new ArrayList<AnexosProyecto>();
        try {
            EntityManager em = emf.createEntityManager();
            Query q = em.createQuery("SELECT a FROM AnexosProyecto a WHERE a.anexosProyectoPK.apartadoId = :apartado and a.anexosProyectoPK.capituloId = :capitulo and a.anexosProyectoPK.folioProyecto = :folio and a.anexosProyectoPK.serialProyecto = :serial and a.anexosProyectoPK.seccionId = :seccion and a.anexosProyectoPK.subcapituloId = :subcapitulo");
            q.setParameter("apartado", apartadoId);
            q.setParameter("capitulo", capituloId);
            q.setParameter("folio", folioProyecto);
            q.setParameter("serial", serialProyecto);
            q.setParameter("seccion", seccionId);
            q.setParameter("subcapitulo", subcapituloId);
            r = q.getResultList();
            em.close();
        } catch (Exception e) {

        }

        return r;

    }
    
    public static List<EstudiosEspProy> getEstudiosEspProy(String folioProyecto, Short serialProyecto, Short estudiosProyId){
        EntityManager em = emf.createEntityManager();
        List<EstudiosEspProy> e;
            if(estudiosProyId!=null){
                Query q = em.createQuery("SELECT s FROM EstudiosEspProy s WHERE s.estudiosEspProyPK.folioSerial = :folio AND s.estudiosEspProyPK.serialProyecto = :serial AND s.estudiosEspProyPK.estudioId = :id");
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                q.setParameter("id", estudiosProyId);
                e = q.getResultList();
            }else{
                Query q = em.createQuery("SELECT s FROM EstudiosEspProy s WHERE s.estudiosEspProyPK.folioSerial = :folio AND s.estudiosEspProyPK.serialProyecto :serial");
                q.setParameter("folio", folioProyecto);
                q.setParameter("serial", serialProyecto);
                e = q.getResultList();
            }
        return e;
    }

    public static List<EspecificacionAnexo> getEspecifiacionesAnexos(String folio, String especifiacionId, Short serialProeycto, Short normaId) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT a FROM EspecificacionAnexo a WHERE a.especificacionAnexoPK.folioProyecto = :folio and a.especificacionAnexoPK.especificacionId = :especificacion and a.especificacionAnexoPK.serialProyecto = :serial and a.especificacionAnexoPK.normaId = :norma");
        q.setParameter("folio", folio);
        q.setParameter("especificacion", especifiacionId);
        q.setParameter("serial", serialProeycto);
        q.setParameter("norma", normaId);
        List l = q.getResultList();

        em.close();
        return l;
    }

    public List<CatCapitulo> getCapitulos() {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT a FROM CatCapitulo a ");
        List l = q.getResultList();

        em.close();
        return l;
    }

    public List<AnexosProyecto> getAnexos(CatCapitulo cap, CatSubcapitulo sub, CatSeccion sec, Object ap) {
        String hql = "";

        if (sub == null) {
            sub = new CatSubcapitulo();
//            sub.setId(-1);
        }
        if (sec == null) {
            sec = new CatSeccion();
        }
//        if (ap == null) {
//            ap = new CatApartado();
//            ap.setId(-1);
//        }

//        if (ap == null || ap.getId() < 1) {
//            hql = "SELECT a FROM AnexoProyecto a where a.apartado.apartado.seccion.id = " + sec.getCatSeccionPK().getSeccionId();
//        }
        if (sec == null || sec.getCatSeccionPK().getSeccionId() < 1) {
//            hql = "SELECT a FROM AnexoProyecto a where a.apartado.apartado.seccion.subCapitulo.id = " + sub.getId();
        }

//        if (sub == null || sub.getId() < 1) {
//            hql = "SELECT a FROM AnexoProyecto a where a.apartado.apartado.seccion.subCapitulo.capitulo = " + cap.getCatCapituloPK().getCapituloId();
//        }
        if (cap == null || cap.getCatCapituloPK().getCapituloId() < 1) {
            hql = "SELECT a FROM AnexoProyecto a where a.apartado.apartado.seccion.subCapitulo.capitulo.id > 0 ";
        }

        if (hql.isEmpty()) {
//            hql = "SELECT a FROM AnexoProyecto a where a.apartado.apartado.id = " + ap.getId();
        }

        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery(hql);
        List l = q.getResultList();
        em.close();

        return l;
    }

    public List<AnexosProyecto> getAnexos(int capitulo, int subCapitulo, int seccion, int apartado) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT a FROM AnexoProyecto a where a.apartado.apartado.seccion.subCapitulo.id ");
        List l = q.getResultList();
        em.close();

        return l;
    }

    public List<CatSubcapitulo> getSubCapitulos(String idCapitulo) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT c FROM CatSubCapitulo c WHERE c.capitulo.id = " + idCapitulo);
        System.out.println("SELECT c FROM CatSubCapitulo c WHERE c.capitulo.id = " + idCapitulo);

        List l = q.getResultList();
        System.out.println("total " + l.size());
        em.close();

        return l;
    }

    public List<CatSeccion> getSecciones(String idSubCapitulo) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT c FROM CatSeccion c WHERE c.subCapitulo.id = " + idSubCapitulo);
        List l = q.getResultList();
        em.close();

        return l;
    }

//    public List<CatApartado> getApartados(String idSeccion) {
//        EntityManager em = emf.createEntityManager();
//        Query q = em.createQuery("SELECT c FROM CatApartado c WHERE c.seccion.id = " + idSeccion);
//        List l = q.getResultList();
//        em.close();
//
//        return l;
//    }
    public List<AnexosProyecto> getApartado(CatCapitulo cap, CatSubcapitulo sub, CatSeccion sec, Object ap) {

        EntityManager em = emf.createEntityManager();
        //       Query q = em.createQuery("SELECT ap FROM  ApartadoProyecto ap  WHERE ap.apartado.id in ( SELECT catAp.id FROM  CatApartado catAp where catAp.seccion )  " );

        Query q = em.createQuery("SELECT anexo FROM AnexoProyecto anexo");
        List l = q.getResultList();
        em.close();

        return l;
    }

    /**
     * Busca un registro por su id
     *
     * @param o Objeto a buscar
     * @param id id del registro
     * @return Object<t>
     */
    public Object getObject(Object o, String id) {
        EntityManager em = emf.createEntityManager();
        Object ob = em.find(o.getClass(), id);
        em.close();

        return ob;
    }

    public Object getObject(Object o, Integer id) {
        EntityManager em = emf.createEntityManager();
        Object ob = em.find(o.getClass(), id);
        em.close();

        return ob;
    }

    public static void persist(Object object) {
        EntityManager em = EntityFactory.getMiaMF().createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
//            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            em.getTransaction().rollback();
        } finally {
        }
    }

}
