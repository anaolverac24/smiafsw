/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import mx.gob.semarnat.mia.Enum.FaunaClaseEnum;
import mx.gob.semarnat.mia.dao.ArchivoBean;
import mx.gob.semarnat.mia.dao.ArchivosAnexosDAO;
import mx.gob.semarnat.mia.dao.CargaBean;
import mx.gob.semarnat.mia.dao.FaunaProyectoDAO;
import mx.gob.semarnat.mia.dao.FloraProyectoDAO;
import mx.gob.semarnat.mia.dao.ProyectoDAO;
import mx.gob.semarnat.mia.dao.SigeiaDao;
import mx.gob.semarnat.mia.model.Acuiferos;
import mx.gob.semarnat.mia.model.ArchivosProyecto;
import mx.gob.semarnat.mia.model.CatClasificacionB;
import mx.gob.semarnat.mia.model.CatEstEsp;
import mx.gob.semarnat.mia.model.ClimaProyecto;
import mx.gob.semarnat.mia.model.CuerposAguaProyecto;
import mx.gob.semarnat.mia.model.EstudiosEspProy;
import mx.gob.semarnat.mia.model.FaunaProyecto;
import mx.gob.semarnat.mia.model.FaunaProyectoPK;
import mx.gob.semarnat.mia.model.FloraProyecto;
import mx.gob.semarnat.mia.model.FloraProyectoPK;
import mx.gob.semarnat.mia.model.HidrologiaProyecto;
import mx.gob.semarnat.mia.model.HidrologiaProyectoId;
import mx.gob.semarnat.mia.model.LocalidIndigenas;
import mx.gob.semarnat.mia.model.Microcuenca;
import mx.gob.semarnat.mia.model.PrediocolinProy;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.SueloVegetacionProyecto;
import mx.gob.semarnat.mia.model.UsoSueloVeget;
import mx.gob.semarnat.mia.model.VwAnimaliaFauna;
import mx.gob.semarnat.mia.model.VwPlantaeVegetacion;
import mx.gob.semarnat.mia.model.catalogos.SectorProyecto;
import mx.gob.semarnat.mia.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.mia.util.Log;
import mx.gob.semarnat.mia.validacion.Validacion;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ramon Hidalgo
 */
@ManagedBean
@ViewScoped
public class Capitulo4View extends CapitulosComentarios implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(Capitulo4View.class);
	
	
    private String seccion;
    private String seccionFlora;
    private String seccionFauna;
    //private MiaDao    miaDao = new MiaDao();
    private SigeiaDao siDao = new SigeiaDao();
    private Proyecto proyecto = new Proyecto();
    private String sector = "";
    
    private Short valSelect;
    private String se = "";
    

    private List<ClimaProyecto> climas = new ArrayList<ClimaProyecto>();
    private List<Object[]> locInd = new ArrayList<Object[]>();
    private List<Microcuenca> microCuenca = new ArrayList<Microcuenca>();
    
    private List<HidrologiaProyecto> hidrologia = new ArrayList<HidrologiaProyecto>();
    private List<Object[]> microCuencaTmp = new ArrayList<Object[]>();
    
    private List<Object[]> microCuenca2 = new ArrayList<Object[]>();
    private List<Acuiferos> acuiferos = new ArrayList<Acuiferos>();
    private List<Object[]> acuiferos2 = new ArrayList<Object[]>();
    private List<UsoSueloVeget> usoSueloVeg = new ArrayList<UsoSueloVeget>();
    private List<LocalidIndigenas> socioe = new ArrayList<LocalidIndigenas>();
    private List<FloraProyecto> flora = new ArrayList<FloraProyecto>();
    private List<FaunaProyecto> fauna = new ArrayList<FaunaProyecto>();
    private List<EstudiosEspProy> estudiosEsp = new ArrayList<EstudiosEspProy>();
    private List<CatEstEsp> catEstEsp = new ArrayList<CatEstEsp>();
    private List<SueloVegetacionProyecto> sueloVegetacion = new ArrayList<SueloVegetacionProyecto>();
    private SueloVegetacionProyecto editTipoVegetacion = new SueloVegetacionProyecto();
    private long idSueloVegetacionSelected;
    private Integer claveTramite = 0;
    private short subsec = 0;
    private List<CuerposAguaProyecto> cuerposAgua = new ArrayList<CuerposAguaProyecto>();
    private List<CuerposAguaProyecto> selectedCuerposAgua = new ArrayList<CuerposAguaProyecto>();
    private CuerposAguaProyecto cuerpoAgua = new CuerposAguaProyecto();
    private CuerposAguaProyecto editCuerpoAgua = new CuerposAguaProyecto();
    private List<SelectItem> catClasificacionB = new ArrayList<SelectItem>();
    private String hidroSupObserva;
    
    /**
     * Numero secuencial del registro "Cuerpos de agua" a editar
     */
    private int rowIndexCAguaEditar;
    
  //?=====================================================================================================================================================================
  //?=====================================================================================================================================================================
	private List<EstudiosEspProy> estudiosC = new ArrayList<EstudiosEspProy>();
    private List<EstudiosEspProy> estudiosS = new ArrayList<EstudiosEspProy>();
    private EstudiosEspProy estudiosArchivo = new EstudiosEspProy();
    private EstudiosEspProy estudiosAE;
    private CatEstEsp estEsp;
    
    
    
    //?=====================================================================================================================================================================
    //?=====================================================================================================================================================================
    
    /**
     * Lista de la flora del proyecto.
     */
    private List<FloraProyecto> listaFloraSeleccionada = new ArrayList<FloraProyecto>();
    /**
     * Lista de la fauna del proyecto.
     */
    private List<FaunaProyecto> listaFaunaSeleccionada = new ArrayList<FaunaProyecto>();
    
    /**
     * Objeto que almacena la informacion de la flora del proyecto.
     */
    private FloraProyecto floraProyecto;
    
    /**
     * Objeto que almacena la informacion de la fauna del proyecto.
     */
    private FaunaProyecto faunaProyecto;    
    
    /**
     * Bandera que permite mostrar la edicion o guardado de la flora.
     */
	private boolean mostrarEdicionFlora;
    /**
     * Bandera que permite mostrar la edicion o guardado de la fauna.
     */
	private boolean mostrarEdicionFauna;	
	/**
	 * Objeto que permite realizar las transacciones a la base de datos para la flora.
	 */
	private FloraProyectoDAO floraProyectoDAO = new FloraProyectoDAO();
	/**
	 * Objeto que permite realizar las transacciones a la base de datos para la fauna.
	 */
	private FaunaProyectoDAO faunaProyectoDAO = new FaunaProyectoDAO();	
	/**
	 * Objeto que contiene toda la informacion de la vista de la base de datos del catalogo de flora.
	 */
	private VwPlantaeVegetacion vwPlantaeVegetacion;	
	/**
	 * Objeto que contiene toda la informacion de la vista de la base de datos del catalogo de fauna.
	 */	
	private VwAnimaliaFauna vwAnimaliaFauna;
	/**
	 * bandera que permite indicar si es endemico o no.
	 */
	private boolean banderaEndemico;
	/**
	 * Id de la flora seleccionada.
	 */
	private Short idFloraSeqSeleccionada;
	/**
	 * Id de la fauna seleccionada.
	 */
	private Short idFaunaSeqSeleccionada;
	/**
	 * Permite cargar el archivo de la fauna.
	 */
    private ArchivosProyecto archivoTemporalFauna = new ArchivosProyecto();
    /**
     * Permite cargar el archivo de la flora.
     */
    private ArchivosProyecto archivoTemporalFlora = new ArchivosProyecto();
    /**
     * Objeto para la cargar de los archivos del Capitulo 4.
     */
    private CargaBean cargaBean;
    /**
     * Fauna seleccionada del proyecto.
     */
	private FaunaProyecto faunaSeleccionado;
	/**
	 * Flora seleccionada del proyecto.
	 */
	private FloraProyecto floraSeleccionado;
	/**
	 * Permite mostrar el chechbox de Endemico para Flora.
	 */
	private boolean mostrarEndemicoFlora;
	/**
	 * Permite mostrar el chechbox de Endemico para Fauna..
	 */
	private boolean mostrarEndemicoFauna;	
	/**
	 * Obtiene el nombre de la clase de la fauna seleccionada.
	 */
	private String claseFaunaSeleccionada;


	private ArchivosAnexosDAO archivosAnexosDAO = new ArchivosAnexosDAO();
	
	private String msj;
	private boolean bandera;
	
	/**
     * Numero secuencial del registro a editar en tabla "Flora"
     */
    private int rowIndexFloraEditar;
    

	/**
     * Numero secuencial del registro a editar en tabla "Fauna"
     */
    private int rowIndexFaunaEditar;
	

    public Capitulo4View() {
    	logger.debug("CARGANDO LOG4J ================================================");
    	floraProyecto = new FloraProyecto();
    	faunaProyecto = new FaunaProyecto();
    	vwPlantaeVegetacion = new VwPlantaeVegetacion();  
    	vwAnimaliaFauna = new VwAnimaliaFauna();    	
    	faunaSeleccionado = new FaunaProyecto();
    	estudiosAE = new EstudiosEspProy();
    	cargaBean = new CargaBean();
    	archivoTemporalFauna = new ArchivosProyecto();
    	archivoTemporalFlora = new ArchivosProyecto();
    	
    	
        // Recupera proyecto de sesion
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
        claveTramite = (Integer) context.getExternalContext().getSessionMap().get("claveTramite");
        //Cargar Proyecto
        try {
            this.proyecto = miaDao.proyecto(folioProyecto, serialProyecto);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }

        if (proyecto == null) {
            this.proyecto = new Proyecto(folioProyecto, serialProyecto);
        }

        System.out.println("Folio " + folioProyecto);
        System.out.println("Serial " + serialProyecto);

        if (proyecto.getNsub() != null) {
            SubsectorProyecto su = miaDao.getSubSector(proyecto.getNsub());
            SectorProyecto sp = miaDao.getSectorProyecto(su.getNsec());
            sector = sp.getSector();
        }

        catEstEsp = miaDao.getCatEstEsp();

        //microCuenca = siDao.getMicrocuenca(folioProyecto, serialProyecto, proyecto.getClaveProyecto());
        //microCuenca2 = siDao.getMicrocuenca2(folioProyecto, serialProyecto, proyecto.getClaveProyecto());
        
        hidrologia = siDao.getHidrologia(folioProyecto, serialProyecto);
        
		catClasificacionB = new ArrayList<SelectItem>();
        List<CatClasificacionB> n = miaDao.getCatClasificacionBById((short) 2);
        for (CatClasificacionB c : n) {
            SelectItem s = new SelectItem(c, c.getClasificacionB());
            catClasificacionB.add(s);
        }
        setCatClasificacionB(catClasificacionB);
        
        cuerposAgua = miaDao.getCuerposAgua(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        hidroSupObserva = getProyecto().getHidroSuPObserva();
        
        //Si no existen registros en hidrologia se insertan desde Microcuenca
        if ( hidrologia == null || hidrologia.isEmpty() ) {
            HidrologiaProyecto hp;
            BigDecimal hidroId = BigDecimal.ZERO;
            microCuencaTmp = siDao.getMicrocuenca2(folioProyecto, serialProyecto, proyecto.getClaveProyecto());
            for (Object[] obj : microCuencaTmp) {
                hidroId = hidroId.add(BigDecimal.ONE);
                hp = new HidrologiaProyecto(
                        new HidrologiaProyectoId(hidroId, folioProyecto,new BigDecimal(serialProyecto)));
                hp.setHidroCuenca(obj[0].toString());
                hp.setHidroSubcuenca(obj[1].toString());
                hp.setHidroMicrocuenca(obj[2].toString());
                siDao.agrega(hp);
            }
        }
        
        //TODO

        //acuiferos = siDao.getAcuiferos(folioProyecto, serialProyecto, proyecto.getClaveProyecto());
        acuiferos2 = siDao.getAcuiferos2(folioProyecto, serialProyecto, proyecto.getClaveProyecto());

        locInd = siDao.getLocalidIndigenas(folioProyecto, serialProyecto, proyecto.getClaveProyecto());

        estudiosEsp = miaDao.getEstudiosEsp(folioProyecto, serialProyecto);

        climas = miaDao.getClimaProyecto(folioProyecto, serialProyecto);
        if (climas.isEmpty()) {
            List<Object[]> cli = siDao.getClimas2(folioProyecto, serialProyecto, proyecto.getClaveProyecto());
            for (Object[] c : cli) {
                ClimaProyecto cp = new ClimaProyecto(folioProyecto, serialProyecto);
                cp.setProyecto(proyecto);
                cp.setClaveClimatologica(c[0].toString());
                cp.setDesTem(c[1].toString());
                cp.setDescPrec(c[2].toString());
                //cp.setDescripcion(c[3].toString());
                cp.setGrupoMapa2(c[3].toString());
                try {
                    miaDao.merge(cp);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                climas.add(cp);
            }
        }

        flora = floraProyectoDAO.getFloraProyecto(folioProyecto, serialProyecto);
        fauna = miaDao.getFaunaProyecto(folioProyecto, serialProyecto);

        try {
            sueloVegetacion = miaDao.getSueloVegetacionProyecto(folioProyecto, serialProyecto);

            if (sueloVegetacion.isEmpty()) {

//                dimensionesUso = sinDao.usoSueloVegetacion(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                List<Object[]> d = siDao.usoSueloVegetacion2(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                System.out.println(d.size());
                for (Object[] u : d) {
                    Short id = miaDao.getMaxSueloVegetacionProyecto(folioProyecto, serialProyecto);

                    SueloVegetacionProyecto s = new SueloVegetacionProyecto(folioProyecto, serialProyecto, id);

                    System.out.println("--- Suelo Vegetación id --- " + id + " Folio " + folioProyecto + " serial " + serialProyecto);

                    System.out.println(u[0] + " --- " + u[1] + "  ---  " + u[2] + " --- " + u[3] + "  ---  " + u[4] + "  ---  " + u[5] + "  --   ");

                    s.setSueloComponente((String) u[0]);
                    s.setSueloDescripcion((String) u[1]);
                    s.setSueloTipoEcov((String) u[2]);
                    s.setSueloTipoGen((String) u[3]);
                    s.setSueloFaseVs((String) u[4]);
//                    String usoSuelo = (String) u[5];
                    Double numeral = (Double) u[5];
                    String usoSuelo = Double.toString(numeral);
                    System.out.println(u[5]);
                    usoSuelo = usoSuelo.replace(",", "");
                    usoSuelo = usoSuelo.trim();
                    s.setSueloAreaSigeia(Double.parseDouble(usoSuelo));
                    //s.setSueloSuperficeProm((Double) u[6]);

                    try {
                        miaDao.merge(s);
                    } catch (Exception ep) {
                        ep.printStackTrace();
                    }
                }
            }

            //Comentarios proyecto - eescalona
            super.setProyectoInfo((short) 4, folioProyecto, serialProyecto);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Carga proyecto especiales
        ;      
        
        

    }

  
    
    
    //============================================================================================================================================================
    
    public List<String> completarNombreCientifico(String query) {
        System.out.println("completarNombreCientifico " + query);
        List<String> r = miaDao.getPlantaNombreCientifico(query);

        return r;
    }

    /**
     * Permite consultar el nombre cientifico de la fauna por la clase que se haya seleccionado.
     * @param query
     * @param nombreClase
     * @return
     */
    public List<String> completarNombreCientificoFauna(String query) {
        System.out.println("completarNombreCientificoFauna: " + query);
        System.out.println("Clase Seleccionada: " + claseFaunaSeleccionada);
        
        List<String> listaFaunaAutocomplete = new ArrayList<String>();
        //Si se selecciona Anfibios y Reptiles
        if (claseFaunaSeleccionada.equals(FaunaClaseEnum.AMPHIBIAREPTILIA.getClaseFauna())) {
        	listaFaunaAutocomplete = miaDao.consultarAnimaliaNombreCientificoByAnfibioRept(query);
		} else if (claseFaunaSeleccionada.equals(FaunaClaseEnum.PECES.getClaseFauna())) {
        	listaFaunaAutocomplete = miaDao.consultarAnimaliaNombreCientificoByPeces(query);			
		} else {
	        listaFaunaAutocomplete = miaDao.getAnimaliaNombreCientifico(query, claseFaunaSeleccionada);
		}

        return listaFaunaAutocomplete;
    }
    /**
     * Permite limpiar los campos si se selecciona una clase diferente.
     */
    public void limpiarCamposClaseSeleccionada() {
		this.vwAnimaliaFauna = new VwAnimaliaFauna();
		this.faunaProyecto.setFaunaNomCient("");
	}
    
    public void consultarTipoVegetacion(){
    	Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idSueloVegetacion = params.get("id").toString();
		id = Integer.parseInt(idSueloVegetacion);
		System.out.println("Registro de Suelo: "+id);
		editTipoVegetacion = new SueloVegetacionProyecto();
		editTipoVegetacion = miaDao.getSueloVegetacionProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) id);
		idSueloVegetacionSelected = id;
    	
    }
    
    public void editarTipoVegetacion(){
    	try{
    		miaDao.persist(editTipoVegetacion);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditar.hide();modalMensajeEditar.show()");
    	} catch (Exception e) {
            e.printStackTrace();
        }
    	sueloVegetacion = miaDao.getSueloVegetacionProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    	
    }
    
    public void crearCuerpoAgua(){
    	cuerpoAgua = new CuerposAguaProyecto();
    	RequestContext.getCurrentInstance().reset("Agregar:pnlAgregar");
    }
    
    public void agregarCuerpoAgua(){
        int id = 0;
      try {
          id = miaDao.getMaxCuerpoAguaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
          id = id + 1;
      } catch (Exception e) {
          id = 1;
          //e.printStackTrace();
      }
      CuerposAguaProyecto cuerpoAguaProyecto = new CuerposAguaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) id);
      cuerpoAguaProyecto.setNombre(cuerpoAgua.getNombre());
      cuerpoAguaProyecto.setTipo(cuerpoAgua.getTipo());
      cuerpoAguaProyecto.setDistancia(cuerpoAgua.getDistancia());
      try {
          miaDao.persist(cuerpoAguaProyecto);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalAgregar.hide();modalMensaje.show()");
      } catch (Exception e) {
          e.printStackTrace();
      }
      cuerposAgua = miaDao.getCuerposAgua(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    }
    
    public void consultarCuerpoAgua(){
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idPredio = params.get("id").toString();
		id = Integer.parseInt(idPredio);
		editCuerpoAgua = new CuerposAguaProyecto();
		editCuerpoAgua = miaDao.getCuerpoAgua(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) id);
		
		// Numero secuencial a mostrar en "Editar registro #"
		rowIndexCAguaEditar = Integer.parseInt(params.get("prowindexCAgua").toString());
    }
    
    public void editarCuerpoAgua(){
    	CuerposAguaProyecto cuerposAguaProyecto = new CuerposAguaProyecto();
    	cuerposAguaProyecto = editCuerpoAgua;
        try {
            miaDao.merge(cuerposAguaProyecto);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditarCA.hide();modalMensajeEditarCuerpoAgua.show()");
        } catch (Exception e) {
            e.printStackTrace();
        }
        cuerposAgua = miaDao.getCuerposAgua(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    }
    
    public void eliminarCuerpoAgua(){
    	int id = 0;
    	for (CuerposAguaProyecto cuerposAguaProyecto : selectedCuerposAgua){
    		id = cuerposAguaProyecto.getCuerposAguaProyectoPK().getCuerposAguaId();
	        try {
		          miaDao.eliminarCuerpoAgua(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),(short) id);
		          selectedCuerposAgua = new ArrayList<CuerposAguaProyecto>();
		      } catch (Exception e2) {
		          e2.printStackTrace();
		      }
    	}
    	cuerposAgua = miaDao.getCuerposAgua(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("modalEliminarCuerpo.hide();modalMensajeEliminar.show()");
    }
    
	public void saveObs() {
		System.out.println("saveObs____________________________");
		ProyectoDAO proyecto = new ProyectoDAO();
		getProyecto().setHidroSuPObserva(hidroSupObserva);
		proyecto.edit(getProyecto());
	}

    public void onFloraNombreSelect(SelectEvent event) {
//        Object o = event.getObject();
        String o;
        Integer id = (Integer) event.getComponent().getAttributes().get("idt");
        System.out.println("idt " + id);
        for (FloraProyecto f : flora) {
            if (f.getFloraProyectoPK().getFloraProyid() == id) {
                o = f.getFloraNomCient();
                System.out.println("getFloraNomCient " + o);
                VwPlantaeVegetacion v = miaDao.getPlantaVegetacion(o);
                System.out.println("v " + v.getPlantaeNombreCientifico());

                f.setFloraFamilia(v.getPlantaeFamilia());
                f.setFloraCatNom59(v.getPlantaeCategoriaNom059());
                f.setFloraCites(v.getPlantaeCites());
                f.setFloraGrupo(v.getPlantaeReino());
                f.setFloraNomComun(v.getPlantaeNombreComun());
//                f.setFloraEndemico(v.getPlantaeNombreCientifico());
                //v.getPlantaeEspecie()
                //v.getPlantaeGenero()
                //v.getPlantaeOrden()
                //v.getPlantaeReino()
            }
        }
    }

    public void onFaunaNombreSelect(SelectEvent event) {
//        Object o = event.getObject();
        String o = "";
        Integer id = (Integer) event.getComponent().getAttributes().get("idt");
        System.out.println("idt " + id);
        for (FaunaProyecto f : fauna) {
            System.out.println("f.getFaunaProyectoPK().getFaunaProyid() " + f.getFaunaProyectoPK().getFaunaProyid());

            if (f.getFaunaProyectoPK().getFaunaProyid() == id) {
                System.out.println("Seleccionadn");
                o = f.getFaunaNomCient();
                VwAnimaliaFauna v = miaDao.getFauna(o);
                f.setFaunaFamilia((String) v.getAnimaliaFamilia());
                f.setFaunaCatNom59((String) v.getAnimaliaCategoriaNom059());
                f.setFaunaCites((String) v.getAnimaliaCites());
                f.setFaunaGrupo((String) v.getAnimaliaReino());
                f.setFaunaNomComun((String) v.getAnimaliaNombreComun());
//                f.setFaunaEndemico((String)v.getAnimaliaNombreCientifico());
            }
        }
    }

//    public void onFaunaNombreSelect(SelectEvent event) {
////        Object o = event.getObject();
//        String o = "";
//        Integer id = (Integer) event.getComponent().getAttributes().get("idt");
//        System.out.println("idt " + id);
//        for (FaunaProyecto f : fauna) {
//            if (f.getFaunaProyectoPK().getFaunaProyid() == id) {
//                o = f.getFaunaNomCient();
//                VwPlantaeVegetacion v = miaDao.getPlantaVegetacion(o);
//                f.setFaunaFamilia(v.getPlantaeFamilia());
//                f.setFaunaCatNom59(v.getPlantaeCategoriaNom059());
//                f.setFaunaCites(v.getPlantaeCites());
//                f.setFaunaGrupo(v.getPlantaeReino());
//                f.setFaunaNomComun(v.getPlantaeClase());
//                f.setFaunaEndemico(v.getPlantaeEndemico());
//                //v.getPlantaeEspecie()
//                //v.getPlantaeGenero()
//                //v.getPlantaeOrden()
//                //v.getPlantaeReino()
//            }
//        }
//    }
    public void agregarFlora() {
        Integer id = 0;
        try {
            id = miaDao.getMaxFlora(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            id++;
        } catch (Exception e) {

        }
        if (id == null) {
            id = 0;
        }
        FloraProyecto f = new FloraProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        try {
            miaDao.merge(f);
        } catch (Exception e) {
            e.printStackTrace();
        }
        flora.add(f);
    }

    public void agregarFauna() {
        Integer id = 0;
        try {
            id = miaDao.getMaxFauna(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            id++;
        } catch (Exception e) {

        }
        if (id == null) {
            id = 0;
        }

        FaunaProyecto f = new FaunaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        try {
            miaDao.merge(f);
        } catch (Exception e) {
            e.printStackTrace();
        }
        fauna.add(f);
    }

    public void eliminarFauna(ActionEvent e) {
        Integer id = (Integer) e.getComponent().getAttributes().get("idt");
        FaunaProyecto f = new FaunaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        try {
            miaDao.eliminarFauna(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        } catch (Exception e2) {

        }
        fauna.remove(f);
        //    fauna = miaDao.getFaunaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

    }

    public void eliminarFlora(ActionEvent e) {
        Integer id = (Integer) e.getComponent().getAttributes().get("idt");
//        FloraProyecto f = new FloraProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);

        try {
            miaDao.eliminarFlora(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        } catch (Exception e2) {

        }
//        flora.remove(f);
        flora = miaDao.getFloraProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

    }

    public void agregarEstudioEsp() {
        Short id = 0;
        try {
            id = miaDao.getMaxEstudiosEsp(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            id++;
        } catch (Exception e2) {

        }
        if (id == null) {
            id = 1;
        }

        EstudiosEspProy ep = new EstudiosEspProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),id);
        try {
            miaDao.merge(ep);
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        estudiosEsp.add(ep);
    }

    public void eliminarEstudioEsp() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        try {
            
            for(EstudiosEspProy e: estudiosS)
            {
            EstudiosEspProy ee = new EstudiosEspProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),e.getEstudioEspecial());
            try {
                miaDao.eliminarEstudioEsp(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),e.getEstudiosEspProyPK().getEstudioId());
            } catch (Exception e2) {
    
            }
            

            estudiosC.remove(ee);
            }
        } catch (Exception e) {
            Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e.getMessage());
            e.printStackTrace();
            reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.')");          
        }

    }
    
    public void guardar() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        // Cargar Proyecto
        Capitulo4View cap4 = new Capitulo4View();
     
        try {
        	miaDao.merge(proyecto);
			try {
				if (proyecto.getDescDelimSistemAmb() != null && proyecto.getDescDelimAreaInflu() != null
						&& proyecto.getDescDelimSitProy() != null) {
					try {
						miaDao.guardarAvance(proyecto, new Short("3"), "41");						
					} catch (Exception e) {
						System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
					}
				}
				if (cap4.proyecto.getClimaFenMetObserv() != null && cap4.proyecto.getTempFenMetDescrip() != null
						&& cap4.proyecto.getDescGeos() != null && cap4.proyecto.getDescSuelo() != null
						&& cap4.proyecto.getHidroSubtObserva() != null && cap4.proyecto.getHidroSuPObserva() != null
						&& cap4.proyecto.getAnalisisbioVeg() != null) {
					try {
						miaDao.guardarAvance(proyecto, new Short("2"), "42");
					} catch (Exception e) {
						System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
						if (cap4.proyecto.getClimaFenMetObserv() == null || cap4.proyecto.getTempFenMetDescrip() == null
								|| cap4.proyecto.getDescGeos() == null || cap4.proyecto.getDescSuelo() == null
								|| cap4.proyecto.getHidroSubtObserva() == null || cap4.proyecto.getHidroSuPObserva() == null
								|| cap4.proyecto.getAnalisisbioVeg() == null) {
							// reqcontEnv.execute("alert('Debe llenar todos los
							// cuadros de texto.')");
							reqcontEnv.execute("mensajes('Debe llenar todos los cuadros de texto.', 'danger')");
						}
					}
				}				
				if (proyecto.getDescPaisajeProy() != null) {
					try {
						miaDao.guardarAvance(proyecto, new Short("2"), "43");
					} catch (Exception e) {
						System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
					}
				}
				if (proyecto.getMedioSocioproy() != null) {
					try {
						miaDao.guardarAvance(proyecto, new Short("2"), "44");
					} catch (Exception e) {
						System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
					}
				}
				if (proyecto.getDescDiagamb() != null) {
					try {
						miaDao.guardarAvance(proyecto, new Short("2"), "45");						
					} catch (Exception e) {
						System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
					}
				}
			} catch (Exception e) {
			}

//            try {
//                Validacion v = new Validacion();
//                String msg = v.validar(seccion, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
//                if (!msg.isEmpty()) {
//                    return;
//                }
//            } catch (Exception err) {
//                err.printStackTrace();
//            }            

			reqcontEnv.execute("parent.actMenu();");
            //reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
            reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
            
            if (Integer.parseInt(miaDao.avanceProyecto(proyecto)) == 99) {
                //reqcontEnv.execute("alert('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.')");
            	reqcontEnv.execute("mensajes('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.', 'success')");
            }

            //Guardado de componentes en informacion adicional - eescalona
            super.guardarComentarios();

        } catch (Exception e) {
            Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e.getMessage());
            e.printStackTrace();
            //reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.')");
            reqcontEnv.execute("mensajes('No se ha podido guardar la información, Intente más tarde.', 'danger')");
        }
    }   
    
    /**
     * Permite abrir la pantalla modal para guardar un registro de flora.
     */
    public void abrirModalGuardarFlora() {
    	vwPlantaeVegetacion = new VwPlantaeVegetacion();
    	floraProyecto = new FloraProyecto(); 
    	
    	mostrarEdicionFlora = false;
    	
    	banderaEndemico = false;
    	mostrarEndemicoFlora = false;	
	}    
    
    public void abrirModalGuardarFauna() {
    	vwAnimaliaFauna = new VwAnimaliaFauna();
    	faunaProyecto = new FaunaProyecto();  
    	
    	mostrarEdicionFauna = false;
    	
    	mostrarEndemicoFauna = false;		
    	banderaEndemico = false;
	}
    
    /**
     * Permite abrir la pantalla modal para editar un registro de flora.
     */
    public void abrirModalEditarFlora(Short idFloraSeq, int rowIndexFlora) {
    	vwPlantaeVegetacion = new VwPlantaeVegetacion();
    	this.floraProyecto = new FloraProyecto();
    	    	
    	this.idFloraSeqSeleccionada = idFloraSeq;
    	
    	this.floraProyecto = floraProyectoDAO.consultarFloraProyectoSeleccionada(
    			proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),idFloraSeqSeleccionada);
    	
    	if (this.floraProyecto.getFloraEndemico().equals("SI")) {
			banderaEndemico = true;
		} else {
			banderaEndemico = false;
		}
    	
    	mostrarEdicionFlora = true;
    	
    	// Numero secuencial a mostrar en "Editar registro #"
		rowIndexFloraEditar = rowIndexFlora;
	}    
    
    /**
     * Permite abrir la pantalla modal para editar un registro de flora.
     */
    public void abrirModalEditarFauna(Short idFaunaSeq, int rowIndexFauna) {
    	this.faunaProyecto = new FaunaProyecto();
    	
    	this.idFaunaSeqSeleccionada = idFaunaSeq;
    	
    	this.faunaProyecto = faunaProyectoDAO.consultarFaunaProyectoSeleccionada(
    			proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idFaunaSeqSeleccionada);
    	
    	if (this.faunaProyecto.getFaunaEndemico().equals("SI")) {
			banderaEndemico = true;
		} else {
			banderaEndemico = false;
		}
    	
    	if (this.faunaProyecto.getFaunaClase().equals("Anfibios y Reptiles")) {
			this.claseFaunaSeleccionada = FaunaClaseEnum.AMPHIBIAREPTILIA.getClaseFauna();
		} else if(this.faunaProyecto.getFaunaClase().equals("Mamíferos")) {
			this.claseFaunaSeleccionada = FaunaClaseEnum.MAMIFERO.getClaseFauna();
		} else if (this.faunaProyecto.getFaunaClase().equals("Aves")) {
			this.claseFaunaSeleccionada = FaunaClaseEnum.AVES.getClaseFauna();			
		} else if (this.faunaProyecto.getFaunaClase().equals("Peces")) {
			this.claseFaunaSeleccionada = FaunaClaseEnum.PECES.getClaseFauna();			
		}
    	
    	mostrarEdicionFauna = true;
    	
    	// Numero secuencial a mostrar en "Editar registro #"
		rowIndexFaunaEditar = rowIndexFauna;
	}
    
	/**
     * Permite cargar los datos de la Flora segun el nombre científico seleccionado.
     * @param nombreCientificoSeleccionado
     */
    public void cargarInformacionFlora(String nombreCientificoSeleccionado) {
    	vwPlantaeVegetacion = new VwPlantaeVegetacion();
    	//Se consulta la informacion de la flora segun el nombre cientifico seleccionado.
		vwPlantaeVegetacion = floraProyectoDAO.getPlantaVegetacion(nombreCientificoSeleccionado);
		banderaEndemico = false;
		mostrarEndemicoFlora = true;
	}
    
    public void cargarInformacionFauna(String nombreCientificoSeleccionado) {
		vwAnimaliaFauna = new VwAnimaliaFauna();
    	//Se consulta la informacion de la fauna segun el nombre cientifico seleccionado.		
		vwAnimaliaFauna = faunaProyectoDAO.getAnimaliaFauna(nombreCientificoSeleccionado);
		banderaEndemico = false;
		mostrarEndemicoFauna = true;

    }
    /**
     * Permite guardar la flora nueva a registrar.
     */
    public void guardarFlora() {
    	FloraProyectoPK floraProyectoPK = new FloraProyectoPK();
    	floraProyectoPK.setFolioProyecto(proyecto.getProyectoPK().getFolioProyecto());
    	floraProyectoPK.setSerialProyecto(proyecto.getProyectoPK().getSerialProyecto());
    	
    	String folio = floraProyectoPK.getFolioProyecto();
    	short serial = floraProyectoPK.getSerialProyecto();
    	
    	Integer maxFlora = floraProyectoDAO.getMaxFlora(folio, serial);
    	
    	if (maxFlora != null) {
			floraProyectoPK.setFloraProyid(maxFlora + 1);
		} else {
    		floraProyectoPK.setFloraProyid(1);
		}
    	
    	
    	
    	floraProyecto.setFloraProyectoPK(floraProyectoPK);
    	
    	floraProyecto.setFloraCatNom59(vwPlantaeVegetacion.getPlantaeCategoriaNom059());
    	floraProyecto.setFloraFamilia(vwPlantaeVegetacion.getPlantaeFamilia());
    	floraProyecto.setFloraNomCient(vwPlantaeVegetacion.getPlantaeNombreCientifico());
    	floraProyecto.setFloraNomComun(vwPlantaeVegetacion.getPlantaeNombreComun());
    	floraProyecto.setFloraGrupo(vwPlantaeVegetacion.getPlantaeReino());
    	floraProyecto.setFloraClase(vwPlantaeVegetacion.getPlantaeClase());
    	floraProyecto.setFloraCites(vwPlantaeVegetacion.getPlantaeCites());
    	
    	Short maxFloraSeq = floraProyectoDAO.getMaxIdFloraSEQ();
    	
    	if (maxFloraSeq != null) {
			floraProyecto.setIdFloraSeq((short) (maxFloraSeq + 1));
		} else {
    		floraProyecto.setIdFloraSeq((short) 1);
		}
    	    	
    	if (banderaEndemico == true) {
			 floraProyecto.setFloraEndemico("SI");
		} else {
			 floraProyecto.setFloraEndemico("NO");
		}
    	
    	try {
    		//guardar el registro nuevo de la flora.
			floraProyectoDAO.guardarFlora(floraProyecto);
		
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("dlgAgregarFlora.hide(); dlgGuardadoExitoso.show();");			
			
			floraProyecto = new FloraProyecto();
			banderaEndemico = false;
			vwPlantaeVegetacion = new VwPlantaeVegetacion();
			mostrarEndemicoFlora = false;
			mostrarEndemicoFauna = false;
			
	        flora = floraProyectoDAO.getFloraProyecto(folio, serial);

    	} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    /**
     * Permite guardar la fauna del proyecto.
     */
    public void guardarFauna() {
    	FaunaProyectoPK faunaProyectoPK = new FaunaProyectoPK();
    	faunaProyectoPK.setFolioProyecto(proyecto.getProyectoPK().getFolioProyecto());
    	faunaProyectoPK.setSerialProyecto(proyecto.getProyectoPK().getSerialProyecto());
    	
    	String folio = faunaProyectoPK.getFolioProyecto();
    	short serial = faunaProyectoPK.getSerialProyecto();
    	
    	Integer maxFlora = faunaProyectoDAO.getMaxFauna();
    	
    	if (maxFlora != null) {
			faunaProyectoPK.setFaunaProyid(maxFlora + 1);
		} else {
    		faunaProyectoPK.setFaunaProyid(1);
		}
    	
    	
    	
    	faunaProyecto.setFaunaProyectoPK(faunaProyectoPK);
    	
    	faunaProyecto.setFaunaCatNom59(vwAnimaliaFauna.getAnimaliaCategoriaNom059());
    	faunaProyecto.setFaunaFamilia(vwAnimaliaFauna.getAnimaliaFamilia());
    	faunaProyecto.setFaunaNomCient(vwAnimaliaFauna.getAnimaliaNombreCientifico());
    	faunaProyecto.setFaunaNomComun(vwAnimaliaFauna.getAnimaliaNombreComun());
    	faunaProyecto.setFaunaGrupo(vwAnimaliaFauna.getAnimaliaReino());
    	faunaProyecto.setFaunaClase(vwAnimaliaFauna.getAnimaliaClase());
    	faunaProyecto.setFaunaCites(vwAnimaliaFauna.getAnimaliaCites());
    	
    	Short maxFloraSeq = faunaProyectoDAO.getMaxIdFaunaSEQ(folio, serial);
    	
    	if (maxFloraSeq != null) {
			faunaProyecto.setIdFaunaSeq((short) (maxFloraSeq + 1));
		} else {
    		faunaProyecto.setIdFaunaSeq((short) 1);
		}
    	    	
    	if (banderaEndemico == true) {
			 faunaProyecto.setFaunaEndemico("SI");
		} else {
			 faunaProyecto.setFaunaEndemico("NO");
		}
    	
    	try {
    		//guardar el registro nuevo de la fauna.
    		//Si la clase seleccionada es Anfibios y Reptiles
    		if (claseFaunaSeleccionada.equals(FaunaClaseEnum.AMPHIBIAREPTILIA.getClaseFauna())) {
				faunaProyecto.setFaunaClase("Anfibios y Reptiles");
			} else {
	    		//Si la clase seleccionada es Mamiferos
				if(claseFaunaSeleccionada.equals(FaunaClaseEnum.MAMIFERO.getClaseFauna())) {
					faunaProyecto.setFaunaClase("Mamíferos");
				}
				
	    		//Si la clase seleccionada es Aves				
				if (claseFaunaSeleccionada.equals(FaunaClaseEnum.AVES.getClaseFauna())) {
					faunaProyecto.setFaunaClase(claseFaunaSeleccionada);
				}
				
	    		//Si la clase seleccionada es Peces
				if (claseFaunaSeleccionada.equals(FaunaClaseEnum.PECES.getClaseFauna())) {
					faunaProyecto.setFaunaClase("Peces");
				}
			}
    		
			faunaProyectoDAO.guardarFauna(faunaProyecto);
		
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("dlgAgregarFauna.hide(); dlgGuardadoExitoso.show();");			
			
			faunaProyecto = new FaunaProyecto();
			banderaEndemico = false;
			vwAnimaliaFauna = new VwAnimaliaFauna();
			
	        fauna = faunaProyectoDAO.getFaunaProyecto(folio, serial);
	        
			mostrarEndemicoFlora = false;
			mostrarEndemicoFauna = false;

    	} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
    /**
     * Permite actualizar la flora seleccionada.
     */
    public void actualizarFlora() {
		try {
	    	if (banderaEndemico == true) {
				 floraProyecto.setFloraEndemico("SI");
			} else {
				 floraProyecto.setFloraEndemico("NO");
			}
			//Se actualizar la flora
	    	floraProyectoDAO.actualizarFlora(this.floraProyecto);
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("dlgAgregarFlora.hide(); dlgGuardadoExitoso.show();");			
			
			floraProyecto = new FloraProyecto();
			banderaEndemico = false;
			vwPlantaeVegetacion = new VwPlantaeVegetacion();
			
			String folio = proyecto.getProyectoPK().getFolioProyecto();
			Short serial = proyecto.getProyectoPK().getSerialProyecto();
	        flora = floraProyectoDAO.getFloraProyecto(folio, serial);			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    /**
     * Permite actualizar la fauna seleccionada.
     */
    public void actualizarFauna() {
		try {
	    	if (banderaEndemico == true) {
				 faunaProyecto.setFaunaEndemico("SI");
			} else {
				faunaProyecto.setFaunaEndemico("NO");
			}
	    	
    		//guardar el registro nuevo de la fauna.
    		//Si la clase seleccionada es Anfibios y Reptiles
    		if (claseFaunaSeleccionada.equals(FaunaClaseEnum.AMPHIBIAREPTILIA.getClaseFauna())) {
				faunaProyecto.setFaunaClase("Anfibios y Reptiles");
			} else {
	    		//Si la clase seleccionada es Mamiferos
				if(claseFaunaSeleccionada.equals(FaunaClaseEnum.MAMIFERO.getClaseFauna())) {
					faunaProyecto.setFaunaClase("Mamíferos");
				}
				
	    		//Si la clase seleccionada es Aves				
				if (claseFaunaSeleccionada.equals(FaunaClaseEnum.AVES.getClaseFauna())) {
					faunaProyecto.setFaunaClase(claseFaunaSeleccionada);
				}
				
	    		//Si la clase seleccionada es Peces
				if (claseFaunaSeleccionada.equals(FaunaClaseEnum.PECES.getClaseFauna())) {
					faunaProyecto.setFaunaClase(claseFaunaSeleccionada);
				}
			}
	    	
			//Se actualizar la fauna
	    	faunaProyectoDAO.actualizarFauna(this.faunaProyecto);
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("dlgAgregarFauna.hide(); dlgGuardadoExitoso.show();");			
			
			faunaProyecto = new FaunaProyecto();
			banderaEndemico = false;
			vwAnimaliaFauna = new VwAnimaliaFauna();
			
			String folio = proyecto.getProyectoPK().getFolioProyecto();
			Short serial = proyecto.getProyectoPK().getSerialProyecto();
	        fauna = faunaProyectoDAO.getFaunaProyecto(folio, serial);			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}    
    /**
     * Permite abrir al ventana de dialog de Elimninar 
     */
    public void abrirDialogEliminarFlora() {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("dlgEliminarFlora.show()");		
	}
    
    /**
     * Permite eliminar la(s) flora(s) seleccionadas.
     */
    public void eliminarFlora() {
    	int floraEliminada = 0;
    	if (listaFloraSeleccionada != null && listaFloraSeleccionada.size() > 0) {
    		for (FloraProyecto floraProyecto : listaFloraSeleccionada) {
    			try {
    				//Eliminacion de Archivos en FileSystem
        			if (floraProyecto.getIdArchivoProyecto() != null) {
        				ArchivoBean.eliminarArchivoProyecto(floraProyecto.getIdArchivoProyecto());
        			}
    				floraEliminada = floraProyectoDAO.eliminarFlora(floraProyecto.getIdFloraSeq());
    			} catch (Exception e) {
    				e.printStackTrace();
    				return;
    			}
    		}	
    		/**
    		 * Si se eliminaron leyes.
    		 */
    		if (floraEliminada > 0) {
        		//Se consulta de nuevo la lista para revisar los cambios afectados.
        		flora = floraProyectoDAO.getFloraProyecto(proyecto.getProyectoPK().getFolioProyecto(),
        				proyecto.getProyectoPK().getSerialProyecto());
        		
    			RequestContext context = RequestContext.getCurrentInstance();
    			context.execute("dlgEliminarFlora.hide(); dlgFloraEliminadaExitosa.show()");
			}
    		
    		listaFloraSeleccionada = new ArrayList<>();
		}	
	}
    
    /**
     * Permite eliminar la(s) fauna(s) seleccionadas.
     */
    public void eliminarFauna() {
    	int faunaEliminada = 0;
    	if (listaFaunaSeleccionada != null && listaFaunaSeleccionada.size() > 0) {
    		for (FaunaProyecto faunaProyecto : listaFaunaSeleccionada) {
    			try {
    				//Eliminacion de Archivos en FileSystem
        			if (faunaProyecto.getIdArchivoProyecto() != null) {
        				ArchivoBean.eliminarArchivoProyecto(faunaProyecto.getIdArchivoProyecto());
        			}
    				faunaEliminada = faunaProyectoDAO.eliminarFauna(faunaProyecto.getIdFaunaSeq());
    			} catch (Exception e) {
    				e.printStackTrace();
    				return;
    			}
    		}	
    		/**
    		 * Si se eliminaron leyes.
    		 */
    		if (faunaEliminada > 0) {
        		//Se consulta de nuevo la lista para revisar los cambios afectados.
        		fauna = faunaProyectoDAO.getFaunaProyecto(proyecto.getProyectoPK().getFolioProyecto(),
        				proyecto.getProyectoPK().getSerialProyecto());
        		
    			RequestContext context = RequestContext.getCurrentInstance();
    			context.execute("dlgEliminarFauna.hide(); dlgFaunaEliminadaExitosa.show()");
			}
    		
    		listaFaunaSeleccionada = new ArrayList<>();
		}	
	}  
    
    /**
     * Inicializa un nuevo archivo para ser cargado
     */
    public void iniciarArchivoFauna() {
    	archivoTemporalFauna = new ArchivosProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), 
    			(short) 4, (short) 2, (short) 2, (short) 3);
    	
    	Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		short idFaunaSeq = 0;
		String idFaunaParam = params.get("idFaunaSeq").toString();
		idFaunaSeq = Short.parseShort(idFaunaParam);
		
		this.faunaSeleccionado = faunaProyectoDAO.consultarFaunaProyectoSeleccionada(
				proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idFaunaSeq);
		
		cargaBean = new CargaBean();
		msj = " ";
    }
    
    /**
     * Inicializa un nuevo archivo para ser cargado
     */
    public void iniciarArchivoFlora() {
    	archivoTemporalFlora = new ArchivosProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), 
    			(short) 4, (short) 2, (short) 2, (short) 3);
    	
    	Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		short idFloraSeq = 0;
		String idFloraParam = params.get("idFloraSeq").toString();
		idFloraSeq = Short.parseShort(idFloraParam);
		
		this.floraSeleccionado = floraProyectoDAO.consultarFloraProyectoSeleccionada(
				proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idFloraSeq);
    
		cargaBean = new CargaBean();
		
		msj = " ";
    }    
    
    /**
     * Guarda el archivo pdf en la BD y el Servidor 
     */
    public void guardarArchivoFauna() {
    	try {
        	short id;
    		id = miaDao.getMaxArchivoEtapaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), 
    				archivoTemporalFauna.getCapituloId(), archivoTemporalFauna.getSubCapituloId(), archivoTemporalFauna.getSeccionId(), archivoTemporalFauna.getApartadoId());

    		archivoTemporalFauna.setId(id);
			
			bandera = cargaBean.GuardaAnexos(archivoTemporalFauna);
			if (bandera == true) {
			
			Short idSeq = archivosAnexosDAO.consultarMaxIdSeq();
			if (idSeq != null) {
				this.faunaSeleccionado.setIdArchivoProyecto((short) (idSeq));
			} else {
				this.faunaSeleccionado.setIdArchivoProyecto((short) 1);
			}
			msj = "";
//	    	this.faunaSeleccionado.setIdArchivoProyecto(archivoTemporalFauna.getSeqId());
	    	RequestContext requestContext = RequestContext.getCurrentInstance();
	    	requestContext.execute("modalAlerta.show(); modalSubirArchivoFauna.hide();");

			faunaProyectoDAO.actualizarFauna(faunaSeleccionado);
			fauna = faunaProyectoDAO.getFaunaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
			
			}else {
				msj = "Se requiere cargar un archivo";
				
			}
    	
    	} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}   	
    }
    
    /**
     * Guarda el archivo pdf en la BD y el Servidor 
     */
    public void guardarArchivoFlora() {
    	try {
        	short id;
    		id = miaDao.getMaxArchivoEtapaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), 
    				archivoTemporalFlora.getCapituloId(), archivoTemporalFlora.getSubCapituloId(), archivoTemporalFlora.getSeccionId(), archivoTemporalFlora.getApartadoId());

    		archivoTemporalFlora.setId(id);
			
			bandera = cargaBean.GuardaAnexos(archivoTemporalFlora);
			if (bandera == true) {
			
			Short idSeq = archivosAnexosDAO.consultarMaxIdSeq();
			if (idSeq != null) {
				this.floraSeleccionado.setIdArchivoProyecto((short) (idSeq));
			} else {
				this.floraSeleccionado.setIdArchivoProyecto((short) 1);
			}
//	    	this.floraSeleccionado.setIdArchivoProyecto(archivoTemporalFlora.getSeqId());
			msj = "";
	    	RequestContext requestContext = RequestContext.getCurrentInstance();
	    	requestContext.execute("modalAlertaFlora.show(); modalSubirArchivoFlora.hide();");
	    	
			}else {
				msj = "Se requiere cargar un archivo";
				
			}
    	} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}   	

    	try {
			floraProyectoDAO.actualizarFlora(floraSeleccionado);
			flora = floraProyectoDAO.getFloraProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }  

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the sector
     */
    public String getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(String sector) {
        this.sector = sector;
    }

    /**
     * @return the climas
     */
    public List<ClimaProyecto> getClimas() {
        return climas;
    }

    /**
     * @param climas the climas to set
     */
    public void setClimas(List<ClimaProyecto> climas) {
        this.climas = climas;
    }

    /**
     * @return the locInd
     */
    public List<Object[]> getLocInd() {
        return locInd;
    }

    /**
     * @param locInd the locInd to set
     */
    public void setLocInd(List<Object[]> locInd) {
        this.locInd = locInd;
    }

    /**
     * @return the microCuenca
     */
    public List<Microcuenca> getMicroCuenca() {
        return microCuenca;
    }

    /**
     * @param microCuenca the microCuenca to set
     */
    public void setMicroCuenca(List<Microcuenca> microCuenca) {
        this.microCuenca = microCuenca;
    }

    /**
     * @return the acuiferos
     */
    public List<Acuiferos> getAcuiferos() {
        return acuiferos;
    }

    /**
     * @param acuiferos the acuiferos to set
     */
    public void setAcuiferos(List<Acuiferos> acuiferos) {
        this.acuiferos = acuiferos;
    }

    /**
     * @return the usoSueloVeg
     */
    public List<UsoSueloVeget> getUsoSueloVeg() {
        return usoSueloVeg;
    }

    /**
     * @param usoSueloVeg the usoSueloVeg to set
     */
    public void setUsoSueloVeg(List<UsoSueloVeget> usoSueloVeg) {
        this.usoSueloVeg = usoSueloVeg;
    }

    /**
     * @return the socioe
     */
    public List<LocalidIndigenas> getSocioe() {
        return socioe;
    }

    /**
     * @param socioe the socioe to set
     */
    public void setSocioe(List<LocalidIndigenas> socioe) {
        this.socioe = socioe;
    }

    /**
     * @return the flora
     */
    public List<FloraProyecto> getFlora() {
        return flora;
    }

    /**
     * @param flora the flora to set
     */
    public void setFlora(List<FloraProyecto> flora) {
        this.flora = flora;
    }

    /**
     * @return the fauna
     */
    public List<FaunaProyecto> getFauna() {
        return fauna;
    }

    /**
     * @param fauna the fauna to set
     */
    public void setFauna(List<FaunaProyecto> fauna) {
        this.fauna = fauna;
    }

    /**
     * @return the estudiosEsp
     */
    public List<EstudiosEspProy> getEstudiosEsp() {
        return estudiosEsp;
    }

    /**
     * @param estudiosEsp the estudiosEsp to set
     */
    public void setEstudiosEsp(List<EstudiosEspProy> estudiosEsp) {
        this.estudiosEsp = estudiosEsp;
    }

    /**
     * @return the catEstEsp
     */
    public List<CatEstEsp> getCatEstEsp() {
        return catEstEsp;
    }

    /**
     * @param catEstEsp the catEstEsp to set
     */
    public void setCatEstEsp(List<CatEstEsp> catEstEsp) {
        this.catEstEsp = catEstEsp;
    }

    /**
     * @return the acuiferos2
     */
    public List<Object[]> getAcuiferos2() {
        return acuiferos2;
    }

    /**
     * @param acuiferos2 the acuiferos2 to set
     */
    public void setAcuiferos2(List<Object[]> acuiferos2) {
        this.acuiferos2 = acuiferos2;
    }

    /**
     * @return the sueloVegetacion
     */
    public List<SueloVegetacionProyecto> getSueloVegetacion() {
        return sueloVegetacion;
    }

    /**
     * @param sueloVegetacion the sueloVegetacion to set
     */
    public void setSueloVegetacion(List<SueloVegetacionProyecto> sueloVegetacion) {
        this.sueloVegetacion = sueloVegetacion;
    }

    /**
     * @return the claveTramite
     */
    public Integer getClaveTramite() {
        return claveTramite;
    }

    /**
     * @param claveTramite the claveTramite to set
     */
    public void setClaveTramite(Integer claveTramite) {
        this.claveTramite = claveTramite;
    }

    /**
     * @return the microCuenca2
     */
    public List<Object[]> getMicroCuenca2() {
        return microCuenca2;
    }

    /**
     * @param microCuenca2 the microCuenca2 to set
     */
    public void setMicroCuenca2(List<Object[]> microCuenca2) {
        this.microCuenca2 = microCuenca2;
    }

    /**
     * @return the seccion
     */
    public String getSeccion() {
        return seccion;
    }

    /**
     * @param seccion the seccion to set
     */
    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public List<HidrologiaProyecto> getHidrologia() {
        return hidrologia;
    }

    public void setHidrologia(List<HidrologiaProyecto> hidrologia) {
        this.hidrologia = hidrologia;
    }

	public SueloVegetacionProyecto getEditTipoVegetacion() {
		return editTipoVegetacion;
	}

	public void setEditTipoVegetacion(SueloVegetacionProyecto editTipoVegetacion) {
		this.editTipoVegetacion = editTipoVegetacion;
	}
	/**
	 * @return the listaFloraSeleccionada
	 */
	public List<FloraProyecto> getListaFloraSeleccionada() {
		return listaFloraSeleccionada;
	}

	/**
	 * @param listaFloraSeleccionada the listaFloraSeleccionada to set
	 */
	public void setListaFloraSeleccionada(List<FloraProyecto> listaFloraSeleccionada) {
		this.listaFloraSeleccionada = listaFloraSeleccionada;
	}

	/**
	 * @return the floraProyecto
	 */
	public FloraProyecto getFloraProyecto() {
		return floraProyecto;
	}

	/**
	 * @param floraProyecto the floraProyecto to set
	 */
	public void setFloraProyecto(FloraProyecto floraProyecto) {
		this.floraProyecto = floraProyecto;
	}

	/**
	 * @return the mostrarEdicionFlora
	 */
	public boolean isMostrarEdicionFlora() {
		return mostrarEdicionFlora;
	}

	/**
	 * @param mostrarEdicionFlora the mostrarEdicionFlora to set
	 */
	public void setMostrarEdicionFlora(boolean mostrarEdicionFlora) {
		this.mostrarEdicionFlora = mostrarEdicionFlora;
	}

	public long getIdSueloVegetacionSelected() {
		return idSueloVegetacionSelected;
	}

	public void setIdSueloVegetacion(long idSueloVegetacionSelected) {
		this.idSueloVegetacionSelected = idSueloVegetacionSelected;
	}

	/**
	 * @return the vwPlantaeVegetacion
	 */
	public VwPlantaeVegetacion getVwPlantaeVegetacion() {
		return vwPlantaeVegetacion;
	}

	/**
	 * @param vwPlantaeVegetacion the vwPlantaeVegetacion to set
	 */
	public void setVwPlantaeVegetacion(VwPlantaeVegetacion vwPlantaeVegetacion) {
		this.vwPlantaeVegetacion = vwPlantaeVegetacion;
	}

	/**
	 * @return the banderaEndemico
	 */
	public boolean isBanderaEndemico() {
		return banderaEndemico;
	}

	/**
	 * @param banderaEndemico the banderaEndemico to set
	 */
	public void setBanderaEndemico(boolean banderaEndemico) {
		this.banderaEndemico = banderaEndemico;
	}

	/**
	 * @return the idFloraSeqSeleccionada
	 */
	public Short getIdFloraSeqSeleccionada() {
		return idFloraSeqSeleccionada;
	}

	/**
	 * @param idFloraSeqSeleccionada the idFloraSeqSeleccionada to set
	 */
	public void setIdFloraSeqSeleccionada(Short idFloraSeqSeleccionada) {
		this.idFloraSeqSeleccionada = idFloraSeqSeleccionada;
	}

	/**
	 * @return the listaFaunaSeleccionada
	 */
	public List<FaunaProyecto> getListaFaunaSeleccionada() {
		return listaFaunaSeleccionada;
	}

	/**
	 * @param listaFaunaSeleccionada the listaFaunaSeleccionada to set
	 */
	public void setListaFaunaSeleccionada(List<FaunaProyecto> listaFaunaSeleccionada) {
		this.listaFaunaSeleccionada = listaFaunaSeleccionada;
	}

	/**
	 * @return the faunaProyecto
	 */
	public FaunaProyecto getFaunaProyecto() {
		return faunaProyecto;
	}

	/**
	 * @param faunaProyecto the faunaProyecto to set
	 */
	public void setFaunaProyecto(FaunaProyecto faunaProyecto) {
		this.faunaProyecto = faunaProyecto;
	}

	/**
	 * @return the mostrarEdicionFauna
	 */
	public boolean isMostrarEdicionFauna() {
		return mostrarEdicionFauna;
	}

	/**
	 * @param mostrarEdicionFauna the mostrarEdicionFauna to set
	 */
	public void setMostrarEdicionFauna(boolean mostrarEdicionFauna) {
		this.mostrarEdicionFauna = mostrarEdicionFauna;
	}

	/**
	 * @return the vwAnimaliaFauna
	 */
	public VwAnimaliaFauna getVwAnimaliaFauna() {
		return vwAnimaliaFauna;
	}

	/**
	 * @param vwAnimaliaFauna the vwAnimaliaFauna to set
	 */
	public void setVwAnimaliaFauna(VwAnimaliaFauna vwAnimaliaFauna) {
		this.vwAnimaliaFauna = vwAnimaliaFauna;
	}

	/**
	 * @return the idFaunaSeqSeleccionada
	 */
	public Short getIdFaunaSeqSeleccionada() {
		return idFaunaSeqSeleccionada;
	}

	/**
	 * @param idFaunaSeqSeleccionada the idFaunaSeqSeleccionada to set
	 */
	public void setIdFaunaSeqSeleccionada(Short idFaunaSeqSeleccionada) {
		this.idFaunaSeqSeleccionada = idFaunaSeqSeleccionada;
	}

	/**
	 * @return the seccionFlora
	 */
	public String getSeccionFlora() {
		return seccionFlora;
	}

	/**
	 * @param seccionFlora the seccionFlora to set
	 */
	public void setSeccionFlora(String seccionFlora) {
		this.seccionFlora = seccionFlora;
	}

	/**
	 * @return the seccionFauna
	 */
	public String getSeccionFauna() {
		return seccionFauna;
	}

	/**
	 * @param seccionFauna the seccionFauna to set
	 */
	public void setSeccionFauna(String seccionFauna) {
		this.seccionFauna = seccionFauna;
	}

	/**
	 * @return the archivoTemporalFauna
	 */
	public ArchivosProyecto getArchivoTemporalFauna() {
		return archivoTemporalFauna;
	}

	/**
	 * @param archivoTemporalFauna the archivoTemporalFauna to set
	 */
	public void setArchivoTemporalFauna(ArchivosProyecto archivoTemporalFauna) {
		this.archivoTemporalFauna = archivoTemporalFauna;
	}

	/**
	 * @return the cargaBean
	 */
	public CargaBean getCargaBean() {
		return cargaBean;
	}

	/**
	 * @param cargaBean the cargaBean to set
	 */
	public void setCargaBean(CargaBean cargaBean) {
		this.cargaBean = cargaBean;
	}

	/**
	 * @return the archivoTemporalFlora
	 */
	public ArchivosProyecto getArchivoTemporalFlora() {
		return archivoTemporalFlora;
	}

	/**
	 * @param archivoTemporalFlora the archivoTemporalFlora to set
	 */
	public void setArchivoTemporalFlora(ArchivosProyecto archivoTemporalFlora) {
		this.archivoTemporalFlora = archivoTemporalFlora;
	}

	/**
	 * @return the faunaSeleccionado
	 */
	public FaunaProyecto getFaunaSeleccionado() {
		return faunaSeleccionado;
	}

	/**
	 * @param faunaSeleccionado the faunaSeleccionado to set
	 */
	public void setFaunaSeleccionado(FaunaProyecto faunaSeleccionado) {
		this.faunaSeleccionado = faunaSeleccionado;
	}

	/**
	 * @return the floraSeleccionado
	 */
	public FloraProyecto getFloraSeleccionado() {
		return floraSeleccionado;
	}

	/**
	 * @param floraSeleccionado the floraSeleccionado to set
	 */
	public void setFloraSeleccionado(FloraProyecto floraSeleccionado) {
		this.floraSeleccionado = floraSeleccionado;
	}

	
	
 //?=====================================================================================================================================================================
 //?=====================================================================================================================================================================

	public void nuevoEstudio()
	{	
		estEsp=null;
		
		 
	}
	
	public void consultaEs()
	{
		estudiosC = miaDao.getEstudiosEsp(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		
		if(estudiosC.size() ==0)
		{
			System.out.println("Lista vacia: "+estudiosC.size());
		}
		else
		{
			System.out.println("TAMANAÑO DE LA LISTA DE ESTUDIOS: "+estudiosC.size());
		}
    }
	
	public void guardarEstudio()
	{
		Short id = 0;
        try {
            id = miaDao.getMaxEstudiosEsp(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            id++;
        } catch (Exception e2) {

        }
        if (id == null) {
            id = 1;
        }

        EstudiosEspProy ep = new EstudiosEspProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),id);
        try 
        {
       		 
    		cargaBean.guardaEstudios(ep, estudiosAE.getNombrePromovente(),estudiosAE.getAnexoDescripcion(), estEsp.getEstudioId());
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        estudiosC.add(ep);
		 
	}
	
	
	public List<EstudiosEspProy> getEstudiosC() 
	{
		return estudiosC;
	}

	public void setEstudiosC(List<EstudiosEspProy> estudiosC) 
	{
		this.estudiosC = estudiosC;
	}

	public List<EstudiosEspProy> getEstudiosS() {
		return estudiosS;
	}

	public void setEstudiosS(List<EstudiosEspProy> estudiosS) {
		this.estudiosS = estudiosS;
	}

	public EstudiosEspProy getEstudiosArchivo() {
		return estudiosArchivo;
	}
	
	public void setEstudiosArchivo(EstudiosEspProy estudiosArchivo) {
		this.estudiosArchivo = estudiosArchivo;
	}
	
	public EstudiosEspProy getEstudiosAE() {
		return estudiosAE;
	}
	
	public void setEstudiosAE(EstudiosEspProy estudiosAE) {
		this.estudiosAE = estudiosAE;
	}
	
	public Short getValSelect() {
		return valSelect;
	}
	
	public void setValSelect(Short valSelect) {
		this.valSelect = valSelect;
	}
	
	public String getSe() {
		return se;
	}
	
	public void setSe(String se) {
		this.se = se;
	}
	
	public CatEstEsp getEstEsp() {
		return estEsp;
	}
	
	public void setEstEsp(CatEstEsp estEsp) {
		this.estEsp = estEsp;
	}




	/**
	 * @return the mostrarEndemicoFlora
	 */
	public boolean isMostrarEndemicoFlora() {
		return mostrarEndemicoFlora;
	}




	/**
	 * @param mostrarEndemicoFlora the mostrarEndemicoFlora to set
	 */
	public void setMostrarEndemicoFlora(boolean mostrarEndemicoFlora) {
		this.mostrarEndemicoFlora = mostrarEndemicoFlora;
	}




	/**
	 * @return the mostrarEndemicoFauna
	 */
	public boolean isMostrarEndemicoFauna() {
		return mostrarEndemicoFauna;
	}




	/**
	 * @param mostrarEndemicoFauna the mostrarEndemicoFauna to set
	 */
	public void setMostrarEndemicoFauna(boolean mostrarEndemicoFauna) {
		this.mostrarEndemicoFauna = mostrarEndemicoFauna;
	}




	/**
	 * @return the claseFaunaSeleccionada
	 */
	public String getClaseFaunaSeleccionada() {
		return claseFaunaSeleccionada;
	}




	/**
	 * @param claseFaunaSeleccionada the claseFaunaSeleccionada to set
	 */
	public void setClaseFaunaSeleccionada(String claseFaunaSeleccionada) {
		this.claseFaunaSeleccionada = claseFaunaSeleccionada;
	}
	

	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla Flora
	 */
	public int getRowIndexFloraEditar() {
		return rowIndexFloraEditar;
	}

	/**
	 * Asigna valor a variable que guarda Numero secuencial de registro en tabla Flora
	 * @param rowIndexFloraEditar
	 */
	public void setRowIndexFloraEditar(int rowIndexFloraEditar) {
		this.rowIndexFloraEditar = rowIndexFloraEditar;
	}


	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla Fauna
	 */
	public int getRowIndexFaunaEditar() {
		return rowIndexFaunaEditar;
	}

	/**
	 * Asigna valor a variable que guarda Numero secuencial de registro en tabla Fauna
	 * @param rowIndexFaunaEditar
	 */
	public void setRowIndexFaunaEditar(int rowIndexFaunaEditar) {
		this.rowIndexFaunaEditar = rowIndexFaunaEditar;
	}



	public String getMsj() {
		return msj;
	}




	public void setMsj(String msj) {
		this.msj = msj;
	}




	public boolean isBandera() {
		return bandera;
	}




	public void setBandera(boolean bandera) {
		this.bandera = bandera;
	}

	public List<CuerposAguaProyecto> getCuerposAgua() {
		return cuerposAgua;
	}




	public void setCuerposAgua(List<CuerposAguaProyecto> cuerposAgua) {
		this.cuerposAgua = cuerposAgua;
	}




	public CuerposAguaProyecto getCuerpoAgua() {
		return cuerpoAgua;
	}




	public void setCuerpoAgua(CuerposAguaProyecto cuerpoAgua) {
		this.cuerpoAgua = cuerpoAgua;
	}




	public List<SelectItem> getCatClasificacionB() {
		return catClasificacionB;
	}




	public void setCatClasificacionB(List<SelectItem> catClasificacionB) {
		this.catClasificacionB = catClasificacionB;
	}




	public List<CuerposAguaProyecto> getSelectedCuerposAgua() {
		return selectedCuerposAgua;
	}




	public void setSelectedCuerposAgua(List<CuerposAguaProyecto> selectedCuerposAgua) {
		this.selectedCuerposAgua = selectedCuerposAgua;
	}




	public CuerposAguaProyecto getEditCuerpoAgua() {
		return editCuerpoAgua;
	}

	public void setEditCuerpoAgua(CuerposAguaProyecto editCuerpoAgua) {
		this.editCuerpoAgua = editCuerpoAgua;
	}

	public String getHidroSubtObserva() {
		return hidroSupObserva;
	}

	public void setHidroSubtObserva(String hidroSubtObserva) {
		this.hidroSupObserva = hidroSubtObserva;
	}	
	
	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla Cuerpos de agua
	 */
	public int getRowIndexCAguaEditar() {
		return rowIndexCAguaEditar;
	}

	/**
	 * Asigna valor a variable que guarda Numero secuencial de registro a editar en Cuerpos de agua
	 * @param rowIndexCAguaEditar
	 */
	public void setRowIndexCAguaEditar(int rowIndexCAguaEditar) {
		this.rowIndexCAguaEditar = rowIndexCAguaEditar;
	}
	
 //?=====================================================================================================================================================================
 //?=====================================================================================================================================================================
	

}
