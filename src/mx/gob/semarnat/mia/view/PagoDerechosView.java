/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import mx.gob.semarnat.mia.dao.MiaDao;
import mx.gob.semarnat.mia.model.CatDerechosCriteriosvalores;
import mx.gob.semarnat.mia.model.CatDerechosCriteriosvaloresPK;
import mx.gob.semarnat.mia.model.CriterioValoresProyecto;
import mx.gob.semarnat.mia.model.Proyecto;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Rodrigo
 */
@SuppressWarnings("serial")
@ManagedBean(name = "pagoDerView")
@ViewScoped
public class PagoDerechosView implements Serializable {

    private final MiaDao dao = new MiaDao();

    @SuppressWarnings({ "unchecked", "rawtypes" })
	private List<WizardHelper> lista = new ArrayList();
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private List<String> temp = new ArrayList();
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private List<Object[]> todosProySubD = new ArrayList();
    private Proyecto proyecto = new Proyecto();
    private String folioPagoDer = "";
    private short serialPagoDer = 0;
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private List<Object[]> niveles = new ArrayList();

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public PagoDerechosView() {
        // Recupera proyecto de sesion
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");

        //Cargar Proyecto
        try {
            this.proyecto = dao.proyecto(folioProyecto, serialProyecto);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }
        todosProySubD = dao.listaCriterios();
        folioPagoDer  = proyecto.getProyectoPK().getFolioProyecto();
        serialPagoDer = proyecto.getProyectoPK().getSerialProyecto();
        //System.out.println("Folio del pago de derechos:" + folioPagoDer + "---------->");
        List<CriterioValoresProyecto> criterios = (List<CriterioValoresProyecto>) dao.lista_namedQuery("CriterioValoresProyecto.findByFolioSerial", new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()}, new String[]{"folioProyecto", "serialProyecto"});
        int indice = 1, i = 1;
        String desc = "";
        CriterioValoresProyecto c;
        String id = todosProySubD.get(0)[0].toString();

        //<editor-fold desc="Carga de lista del monto de los criterios" defaultstate="collalpsed">
        int iFolioProyecto = Integer.parseInt(folioProyecto);
        String vClaveTramite = dao.tipotramite(iFolioProyecto);
        niveles = dao.tablab(vClaveTramite);
        //</editor-fold>

        //<editor-fold desc="Carga de lista criterios" defaultstate="collalpsed">
        for (Object[] o : todosProySubD) {
            if (!id.equals(o[0].toString()) || todosProySubD.size() == i) {
                if (todosProySubD.size() == i) {
                	desc = o[1].toString();
                    temp.add(o[2].toString());

                }
                if (criterios.isEmpty()) {
                    lista.add(new WizardHelper(indice, desc, temp));
                } else {
                    try {
                        String descripcion;
                        
                        c = (CriterioValoresProyecto) dao.lista_namedQuery("CriterioValoresProyecto.findByFolioSerialIdCriterio",
                                new Object[]{new BigInteger(id), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()},
                                new String[]{"idCriterio", "folioProyecto", "serialProyecto"}).get(0);
                        
                        descripcion = buscarDescripcionValor(c.getCatDerechosCriteriosvalores().getCatDerechosCriterios().getCatDerechosCriteriosvaloresList(), 
                                c.getCriterioValorUsuario().intValue());
                        lista.add(new WizardHelper(indice, desc, temp, descripcion , c.getCriterioValorUsuario().intValue(), c.getCriterioObservacionEva()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                temp = new ArrayList();
                id = o[0].toString();
                temp.add(o[2].toString());
                indice++;
            } else {
                temp.add(o[2].toString());
                desc = o[1].toString();
            }
            i++;
        }//</editor-fold>
    }

    @SuppressWarnings("unchecked")
	public void guarda() {
         RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        CriterioValoresProyecto cvp;
        //borra registros para evitar duplicados
        for (CriterioValoresProyecto cp : (List<CriterioValoresProyecto>) dao.lista_namedQuery("CriterioValoresProyecto.findByFolioSerial", new Object[]{proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()}, new String[]{"folioProyecto", "serialProyecto"})) {
            dao.elimina(CriterioValoresProyecto.class, cp.getCriterioValoresProyectoPK());
        }
        for (WizardHelper helper : lista) {
            cvp = new CriterioValoresProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),
                    new BigInteger(helper.id.toString()), new BigInteger(dao.valor(helper.id, helper.seleccion).toString()));
            cvp.setBitacoraProyecto(proyecto.getBitacoraProyecto());
            cvp.setCriterioValorUsuario(new BigInteger(dao.valor(helper.id, helper.seleccion).toString()));
            cvp.setProyecto(proyecto);
            cvp.setCriterioObservacionEva(helper.referencia);
            cvp.setCatDerechosCriteriosvalores((CatDerechosCriteriosvalores) dao.busca(CatDerechosCriteriosvalores.class, new CatDerechosCriteriosvaloresPK(new BigInteger(helper.id.toString()), new BigInteger(helper.valor.toString()))));
            try {
                dao.merge(cvp);
            } catch (Exception e) {
            }
        }
        proyecto.setProyValorCriterio(new Short(getSumatoria() + ""));

        try {
            dao.merge(proyecto);
            reqcontEnv.execute("alert('Su información ha sido guardada con éxito.');");
        } catch (Exception ex) {
            Logger.getLogger(PagoDerechosView.class.getName()).log(Level.SEVERE, null, ex);
            reqcontEnv.execute("alert('No se ha podido guardar la información, Intente mas tarde.')");
        }

    }
    
    private String buscarDescripcionValor(List<CatDerechosCriteriosvalores> lista, int valor) {
        String descripcion = "";
        
        for (CatDerechosCriteriosvalores criterio : lista) {
            if (criterio.getValor().intValue() == valor) {
                descripcion = criterio.getDescripcionValor();
                break;
            }
        }
        
        return descripcion;
    }

    //<editor-fold desc="listener" defaultstate="collapsed">
    public void seleccionaRespuesta() {
        for (WizardHelper n : lista) {
            if (n.seleccion != null) {
                n.valor = dao.valor(n.id, n.seleccion);
            }
        }

    }//</editor-fold>

    /**
     * @return the niveles
     */
    public List<Object[]> getNiveles() {
        return niveles;
    }

    /**
     * @param niveles the niveles to set
     */
    public void setNiveles(List<Object[]> niveles) {
        this.niveles = niveles;
    }

    /**
     * @return the serialPagoDer
     */
    public short getSerialPagoDer() {
        return serialPagoDer;
    }

    /**
     * @param serialPagoDer the serialPagoDer to set
     */
    public void setSerialPagoDer(short serialPagoDer) {
        this.serialPagoDer = serialPagoDer;
    }

    //<editor-fold desc="Clase WizardHelper para apoyo en vista" defaultstate="collapsed">
    public class WizardHelper implements Serializable {

        private Integer id;
        private String descripcion;
        private List<String> respuesta;
        private String seleccion;
        private Integer valor;
        private String referencia;

        public WizardHelper(Integer id, String descripcion, List<String> respuesta) {
            this.id = id;
            this.descripcion = descripcion;
            this.respuesta = respuesta;

        }

        public WizardHelper(Integer id, String descripcion, List<String> respuesta, String seleccion, Integer valor, String referencia) {
            this.id = id;
            this.descripcion = descripcion;
            this.respuesta = respuesta;
            this.seleccion = seleccion;
            this.valor = valor;
            this.referencia = referencia;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public List<String> getRespuesta() {
            return respuesta;
        }

        public void setRespuesta(List<String> respuesta) {
            this.respuesta = respuesta;
        }

        public String getSeleccion() {
            return seleccion;
        }

        public void setSeleccion(String seleccion) {
            this.seleccion = seleccion;
        }

        public String getReferencia() {
            return referencia;
        }

        public void setReferencia(String referencia) {
            this.referencia = referencia;
        }

        public Integer getValor() {
            return valor;
        }
    }//</editor-fold>

    //<editor-fold desc="Getters & Setters" defaultstate="collapsed">
    public int getSumatoria() {
        int s = 0;
        for (WizardHelper n : lista) {
            if (n.seleccion != null) {
                s += n.valor;
            }
        }
        return s;
    }

    public List<WizardHelper> getLista() {
        return lista;
    }

    public void setLista(List<WizardHelper> lista) {
        this.lista = lista;
    }

    public List<String> getTemp() {
        return temp;
    }

    public void setTemp(List<String> temp) {
        this.temp = temp;
    }

    public List<Object[]> getTodosProySubD() {
        return todosProySubD;
    }

    public void setTodosProySubD(List<Object[]> todosProySubD) {
        this.todosProySubD = todosProySubD;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    //</editor-fold>
    public String getFolioPagoDer() {
        return folioPagoDer;
    }

    public void setFolioPagoDer(String folioPagoDer) {
        this.folioPagoDer = folioPagoDer;
    }

}
