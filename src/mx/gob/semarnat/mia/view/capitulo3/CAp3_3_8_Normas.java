/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view.capitulo3;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;

import mx.gob.semarnat.mia.dao.ArchivoBean;
import mx.gob.semarnat.mia.dao.CargaBean;
import mx.gob.semarnat.mia.dao.NormaProyectoDAO;
import mx.gob.semarnat.mia.dao.NumeralDAO;
import mx.gob.semarnat.mia.model.ArchivosProyecto;
import mx.gob.semarnat.mia.model.CatNorma;
import mx.gob.semarnat.mia.model.NormaProyecto;
import mx.gob.semarnat.mia.model.NormaProyectoPK;
import mx.gob.semarnat.mia.model.Numeral;
import mx.gob.semarnat.mia.model.NumeralPK;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.validacion.Validacion;
import mx.gob.semarnat.mia.view.Cap;

/**
 *
 * @author mauricio
 * 
 * 
 */
@ManagedBean(name ="Capnorm")
@ViewScoped
public class CAp3_3_8_Normas extends Cap implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String seccion;
    private List<NormaProyecto> normas = new ArrayList<NormaProyecto>();
    private List<NormaProyecto> normasSeleccionadas= new ArrayList<NormaProyecto>();
    
    private short normaP;
    
    private List<CatNorma> catalogoNormas;
    private CatNorma catalogoNormasSeleccionada;
    
    
    private Proyecto proyecto = new Proyecto();
     
    private NormaProyecto norms;
    
    private NormaProyecto editNorma;
    
    private boolean mostrarCapturaNorma;
    
    private CargaBean cargaBean;
    
    private Numeral numeral;
    
    /*
     * Propiedades para mostrar al combo
     */
    
    private boolean mostrarNorma;    
    private boolean mostrarUltimaFecha;
    private CatNorma catnorm;
    
    private ArchivosProyecto archivoTemporal = new ArchivosProyecto();        
    
    private String normaDescripcion; 
    
    private Date fechaPublicacion;
    
    NormaProyectoDAO normaProyectoDAO;
    
    private String msj;
	private boolean bandera;
	
    public boolean isMostrarUltimaFecha() {
		return mostrarUltimaFecha;
	}

	public void setMostrarUltimaFecha(boolean mostrarUltimaFecha) {
		this.mostrarUltimaFecha = mostrarUltimaFecha;
	}

	public boolean isMostrarNorma() {
		return mostrarNorma;
	}

	public void setMostrarNorma(boolean mostrarNorma) {
		this.mostrarNorma = mostrarNorma;
	}

	/**
     * Permite mostrar el panel para guardar o editar una norma.
     */
    private boolean mostrarEdicion;
    
    /**
	 * Permite mostrar el catalogo de leyes Federales.
	 */
	
    
    private List<Numeral> numerales = new ArrayList<Numeral>();
    private Short idNormaSel = null;
    Short id = null;
    Short idNumeral = null;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public CAp3_3_8_Normas() {
    	catnorm = new CatNorma();
    	norms = new NormaProyecto();
    	normaProyectoDAO=new NormaProyectoDAO();
    	norms.setNormaId(catnorm);
    	catalogoNormas = miaDao.getCatNormas();
    	
        System.out.println(getProyecto().getProyectoPK().getFolioProyecto());
        System.out.println(getProyecto().getProyectoPK().getSerialProyecto());

        loadDataTable();
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        for (NormaProyecto norma : normas) {
        	System.out.println("EFR: in normas");
            try {
                if ( norma.getNormaFechPublic() != null) {
                    norma.setFechaPublicacion(sdf.parse(norma.getNormaFechPublic()));
                }
            } catch (Exception e) { e.printStackTrace(); }
        }
        
        cargaBean=new CargaBean ();
        super.setProyectoInfo((short) 3, getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
    }
    
    ///// 
   

    public String tieneNormasSeleccionadas(){
    	System.out.println("Comprobando Chechbox´s");
    	System.out.println(normasSeleccionadas.size()+" seleccionados");
    	if(!normasSeleccionadas.isEmpty()){
    		RequestContext context = RequestContext.getCurrentInstance();
    		System.out.println("Abriendo modal de confirmación");
    	    context.execute("eliminarNorma.show()");	    
    	}
    	System.out.println("Saliendo del IF");
    	return "SeleSitio";
    }
    
    public void editarNorma() {
    	NormaProyecto cp = new NormaProyecto();
    	cp = editNorma;
        try {
            miaDao.persist(cp);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditar.hide();modalAlertEditar.show()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    public void consultarNorma() {
    	System.out.println("Consultando");
    	@SuppressWarnings("rawtypes")
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idCriterio = params.get("id").toString();
		id = Integer.parseInt(idCriterio);
		System.out.println("Actualizando criterio: "+id);
		editNorma = new NormaProyecto();
		editNorma = miaDao.getNorma(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) id);
		
    }
    
    
    public void eliminaNorma()
    {
    	 try{
        for (NormaProyecto normasSeleccionado : normasSeleccionadas) {              
                   
            System.out.println(getProyecto().getProyectoPK().getFolioProyecto());
            
           miaDao.eliminarNumerales(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), normasSeleccionado.getNormaProyectoPK().getNormaProyId());
           miaDao.eliminarNorma(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), normasSeleccionado.getNormaProyectoPK().getNormaProyId());
           normas.remove(normasSeleccionado); 
        }   
    	 }
    	 catch(Exception e)
    	 {
    		 System.out.println(e.getMessage());
    	 }
       
    }
    
     
    
    
/////
    public void agregarNorma() {
    	
    	id = null;
    	System.out.println(id);
        if (id == null) {
            id = getMiaDao().getMaxNorma(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
        }        
        System.out.println(id);
        NormaProyecto n = new NormaProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
        try {
        	n.setNormaDescripcion(normaDescripcion);        	
        	n.setNormaFechPublic(sdf.format(fechaPublicacion));
        	n.setNormaId(catalogoNormasSeleccionada);
//        	normaProyectoDAO.create(n);
            //getMiaDao().merge(n);
        	NumeralDAO numeralDAO = new NumeralDAO();
        	numeralDAO.guardarNorma(n);
        	
        	normas = numeralDAO.consultarNormas(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
	
        	sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            for (NormaProyecto norma : normas) {
            	System.out.println("EFR: in normas");
                try {
                    if ( norma.getNormaFechPublic() != null) {
                        norma.setFechaPublicacion(sdf.parse(norma.getNormaFechPublic()));
                    }
                } catch (Exception e) { e.printStackTrace(); }
            }        	
			
			RequestContext context = RequestContext.getCurrentInstance();
			RequestContext.getCurrentInstance().reset(":fAgregar:pnlAgregar");
			context.execute("modalAgregar.hide();modalAlert.show()");
			normaDescripcion = null;
			fechaPublicacion = null;
			catalogoNormasSeleccionada =  null;
			
        } catch (Exception e) {
            e.printStackTrace();
        }

        
    }

    
    
    public void eliminarNorma(ActionEvent evt) {
        Short id = (Short) evt.getComponent().getAttributes().get("idt");
        NormaProyecto n = new NormaProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
        try {
            getMiaDao().eliminarNumerales(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
            getMiaDao().eliminarNorma(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
        } catch (Exception e) {
            e.printStackTrace();
           
        }

        normas.remove(n);
    }

    public void agregarNumeral() {
        if (idNumeral == null) {
            idNumeral = getMiaDao().getMaxNumeral(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
        }
        idNumeral++;

        Numeral num = new Numeral(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), idNormaSel, idNumeral);
        num.setNormaNumeral(" ");
        try {
            getMiaDao().merge(num);
        } catch (Exception e) {
            e.printStackTrace();
        }

        numerales = miaDao.getNumerales(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
    }

    public void eliminarNumeral(ActionEvent evt) {
        Short id = (Short) evt.getComponent().getAttributes().get("idt");
        Short id2 = (Short) evt.getComponent().getAttributes().get("idt2");
        Numeral num = new Numeral(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id, id2);
        try {
            getMiaDao().eliminarNumeral(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id, id2);
        } catch (Exception e) {
            e.printStackTrace();
        }

        numerales.remove(num);
    }

    
    public void guardarNumeral(){
    	NumeralDAO numeralDAO= new NumeralDAO ();
    	
    	Numeral numeral = null;
    	
    	if (this.numeral==null || this.numeral.getNumeralPK()==null || this.numeral.getNumeralPK().getFolioProyecto()==null){
	    	short nextNumeral=numeralDAO.getMaxNumeral(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(),  normaP);
	    	numeral = new Numeral( new NumeralPK(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(),  normaP, nextNumeral));
	    	numeral.setNormaVinculaciN(this.numeral.getNormaVinculaciN());
	    	numeral.setNormaRequisito(this.numeral.getNormaRequisito());
	    	numeral.setNormaNumeral(this.numeral.getNormaNumeral());
	    	RequestContext.getCurrentInstance().update(":fNormas");
	    	RequestContext.getCurrentInstance().execute("numeralesWin.hide();");
	    	RequestContext.getCurrentInstance().execute("modalAlert.show();");
	    	numeralDAO.guardarNumeral(numeral);
    	}else{
    		numeralDAO.editarNumeral(this.numeral);
    	  	RequestContext.getCurrentInstance().update(":fNormas");
	    	RequestContext.getCurrentInstance().execute("numeralesWin.hide();");
	    	RequestContext.getCurrentInstance().execute("modalAlertEditar.show();");
    	}
    	
    	normas = null;
    	normas = numeralDAO.consultarNormas(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
        
    	sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        for (NormaProyecto norma : normas) {
        	System.out.println("EFR: in normas");
            try {
                if ( norma.getNormaFechPublic() != null) {
                    norma.setFechaPublicacion(sdf.parse(norma.getNormaFechPublic()));
                }
            } catch (Exception e) { e.printStackTrace(); }
        }
    	
    	numeral = new Numeral();
    	
    }
    
    
    public void guardarNumerales() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        try {
            for (Numeral n : numerales) {
                getMiaDao().merge(n);
            }
            try {
                Validacion v = new Validacion();
                String msg = v.validar(seccion, getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
                if (!msg.isEmpty()) {
                    return;
                }
            } catch (Exception err) {
                err.printStackTrace();
            }
            reqcontEnv.execute("alert('Numerales guardados')");
        } catch (Exception ee) {
            reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.')");
            ee.printStackTrace();
        }
    }

    public void guardar() {
    	RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        try {
//            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
//            for (NormaProyecto n : normas) {
//                if ( n.getFechaPublicacion() != null ) {
//                    n.setNormaFechPublic(sdf.format(n.getFechaPublicacion()));
//                }
//                getMiaDao().merge(n);
//            }
//        try {
//                Validacion v = new Validacion();
//                String msg = v.validar(seccion, getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
//                if (!msg.isEmpty()) {
//                    return;
//                }
//            } catch (Exception err) {
//                err.printStackTrace();
//            }
//            if (normas.size() >= 1 && numerales.size() >= 1) {
            if (normas.size() >= 1) {
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                try {
                    getMiaDao().guardarAvance(getProyecto(), new Short("2"), "33");
                    reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
                } catch (Exception e) {
                    System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    reqcontEnv.execute("mensajes('No se ha podido guardar la información, Intente más tarde.', 'danger')");
                }
                //</editor-fold>

            }

            //super.guardarComentarios();

            //reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
            
        } catch (Exception ee) {
            //reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.')");
        	
            ee.printStackTrace();
        }
    }

    public void seleccionarNorma(ActionEvent evt) {
        Short idt = (Short) evt.getComponent().getAttributes().get("idt");
        this.idNormaSel = idt;
        numerales.clear();
        numerales = getMiaDao().getNumerales(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), idt);
        System.out.println("Norma seleccionada " + idt);
    }
    
    /**
     * Permite actualizar el dialog con el panel de guardado de norma.
     */
    public void abrirGuardarNorma() {
		mostrarEdicion = false;
	}    
    
    /**
     * Permite mostrar u ocultar el panel para ingresar una nueva norma.
     * @param idNormaSeleccionada
     */
    public void mostrarCapturaNorma(short idNormaSeleccionada) {
		if (idNormaSeleccionada != 9999) {
			mostrarCapturaNorma = false;
		} else {
			mostrarCapturaNorma = true;
		}
	}
    
    /**
	 * Genera un nuevo objeto actividad para realizar un alta
	 */
	public void crearNorma() {
		norms = new NormaProyecto();
		catalogoNormas= new ArrayList<CatNorma>();
		catalogoNormas = miaDao.getCatNormas();
		cargaBean.setFile(null);
	}

	 
	
    /**
     * @return the normas
     */
    public List<NormaProyecto> getNormas() throws ParseException {
        return normas;
    }

    /**
     * @param normas the normas to set
     */
    public void setNormas(List<NormaProyecto> normas) {
        this.normas = normas;
    }

    /**
     * @return the numerales
     */
    public List<Numeral> getNumerales() {
        return numerales;
        
    }

    /**
     * @param numerales the numerales to set
     */
    public void setNumerales(List<Numeral> numerales) {
        this.numerales = numerales;
    }

    /**
     * @return the idNormaSel
     */
    public Short getIdNormaSel() {
        return idNormaSel;
    }

    /**
     * @param idNormaSel the idNormaSel to set
     */
    public void setIdNormaSel(Short idNormaSel) {
        this.idNormaSel = idNormaSel;
    }

    /**
     * @return the seccion
     */
    public String getSeccion() {
        return seccion;
    }

    /**
     * @param seccion the seccion to set
     */
    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

	public List<NormaProyecto> getNormasSeleccionadas() {
		
		return normasSeleccionadas;
	}

	public void setNormasSeleccionadas(List<NormaProyecto> normas_Seleccionadas) {
		this.normasSeleccionadas = normas_Seleccionadas;
	}

	public boolean getMostrarEdicion() {
		return mostrarEdicion;
	}

	public void setMostrarEdicion(boolean mostrarEdicion) {
		this.mostrarEdicion = mostrarEdicion;
	}

	/**
	 * @return the norms
	 */
	public NormaProyecto getNorms() {
		return norms;
	}

	/**
	 * @param norms the norms to set
	 */
	public void setNorms(NormaProyecto norms) {
		this.norms = norms;
	}

	/**
	 * @return the mostrarCapturaNorma
	 */
	public boolean isMostrarCapturaNorma() {
		return mostrarCapturaNorma;
	}

	/**
	 * @param mostrarCapturaNorma the mostrarCapturaNorma to set
	 */
	public void setMostrarCapturaNorma(boolean mostrarCapturaNorma) {
		this.mostrarCapturaNorma = mostrarCapturaNorma;
	}

	/**
	 * @return the catalogoNormas
	 */
	public List<CatNorma> getCatalogoNormas() {
		return catalogoNormas;
	}

	/**
	 * @param catalogoNormas the catalogoNormas to set
	 */
	public void setCatalogoNormas(List<CatNorma> catalogoNormas) {
		this.catalogoNormas = catalogoNormas;
	}

	/**
	 * @return the catalogoNormasSeleccionada
	 */
	public CatNorma getCatalogoNormasSeleccionada() {
		return catalogoNormasSeleccionada;
	}

	/**
	 * @param catalogoNormasSeleccionada the catalogoNormasSeleccionada to set
	 */
	public void setCatalogoNormasSeleccionada(CatNorma catalogoNormasSeleccionada) {
		this.catalogoNormasSeleccionada = catalogoNormasSeleccionada;
	}

	/**
	 * @return the cargaBean
	 */
	public CargaBean getCargaBean() {
		return cargaBean;
	}

	/**
	 * @param cargaBean the cargaBean to set
	 */
	public void setCargaBean(CargaBean cargaBean) {
		this.cargaBean = cargaBean;
	}

	
	
	

	/**
	 * @return the normaDescripcion
	 */
	public String getNormaDescripcion() {
		return normaDescripcion;
	}

	/**
	 * @param normaDescripcion the normaDescripcion to set
	 */
	public void setNormaDescripcion(String normaDescripcion) {
		this.normaDescripcion = normaDescripcion;
	}

	/**
	 * @return the fechaPublicacion
	 */
	public Date getFechaPublicacion() {
		return fechaPublicacion;
	}

	/**
	 * @param fechaPublicacion the fechaPublicacion to set
	 */
	public void setFechaPublicacion(Date fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}
	
	public void guardarArchivo(){
		short id=0;
		if (cargaBean.getFile()!=null){
			archivoTemporal.setFolioSerial(getProyecto().getProyectoPK().getFolioProyecto());
			archivoTemporal.setSerialProyecto(getProyecto().getProyectoPK().getSerialProyecto());
			try {
		    	short idProy;
		    	idProy = miaDao.getMaxArchivoEtapaProyecto(archivoTemporal.getFolioSerial(), archivoTemporal.getSerialProyecto(), (short) 3, (short) 3, (short)0, (short) 0 );

				archivoTemporal.setId(idProy);
				archivoTemporal.setCapituloId((short)3);
				archivoTemporal.setSubCapituloId((short)3);
				bandera = cargaBean.GuardaAnexos(archivoTemporal);
				if (bandera == true) {
			
				id=archivoTemporal.getSeqId();
				
				if (id>0){
					//normaP
					NormaProyecto normaProyecto=normaProyectoDAO.find( new NormaProyectoPK(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), normaP));
					if(normaProyecto!=null){
						normaProyecto.setIdArchivoProyecto(new Short(id));
						normaProyectoDAO.edit(normaProyecto);
					}
					
				}			
				
				archivoTemporal = new ArchivosProyecto();
				archivoTemporal.setSeqId(id);
				
				msj = "";
				cargaBean = new CargaBean();
				RequestContext.getCurrentInstance().update(":fNormas:tablaDatos");
				RequestContext.getCurrentInstance().execute("modalSubirArchivo.hide();");
		    	RequestContext.getCurrentInstance().execute("modalAlert.show();");
				}else {
					msj = "Se requiere cargar un archivo";
					
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}else {
			msj = "Se requiere cargar un archivo";
			
		}
		
		NumeralDAO numeralDAO = new NumeralDAO();
		normas=null;
    	normas = numeralDAO.consultarNormas(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
		
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        for (NormaProyecto norma : normas) {
        	System.out.println("EFR: in normas");
            try {
                if ( norma.getNormaFechPublic() != null) {
                    norma.setFechaPublicacion(sdf.parse(norma.getNormaFechPublic()));
                }
            } catch (Exception e) { e.printStackTrace(); }
        }
		
		
		//:fNormas:tablaDatos
    	
	}

	/**
	 * @return the numeral
	 */
	public Numeral getNumeral() {
		return numeral;
	}

	/**
	 * @param numeral the numeral to set
	 */
	public void setNumeral(Numeral numeral) {
		this.numeral = numeral;
	}


	public void cargarNumeral(short normaProyecto){
		//		
		normaP=normaProyecto;
		numeral=null;
		numeral=new Numeral();
	}

	public void editarNumeral(Numeral numeral){
		System.out.println("editarNumeral");
		this.numeral=null;
		this.numeral=numeral;
		
		//RequestContext.getCurrentInstance().execute("eliminarNorma.show();");
	}

	/**
	 * @return the archivoTemporal
	 */
	public ArchivosProyecto getArchivoTemporal() {
		return archivoTemporal;
	}

	/**
	 * @param archivoTemporal the archivoTemporal to set
	 */
	public void setArchivoTemporal(ArchivosProyecto archivoTemporal) {
		this.archivoTemporal = archivoTemporal;
	}
	

	public void iniciarArchivo(short normaS) {
		archivoTemporal = new ArchivosProyecto();
		archivoTemporal = new ArchivosProyecto(getProyecto().getProyectoPK().getFolioProyecto(),getProyecto().getProyectoPK().getSerialProyecto(), 
				                               (short) 3, (short) 3, (short)0, (short) 0);
		normaP=normaS;
		msj = "";
		
		
	}

	public void deleteNormas(){		
		for (NormaProyecto  norma:normasSeleccionadas){
			//Eliminacion de Archivos en FileSystem
			if (norma.getIdArchivoProyecto() != null) {
				ArchivoBean.eliminarArchivoProyecto(norma.getIdArchivoProyecto());
			}
			NumeralDAO numeralDAO= new NumeralDAO();
			numeralDAO.eliminarNumeral(getProyecto().getProyectoPK().getFolioProyecto(),getProyecto().getProyectoPK().getSerialProyecto(),norma.getNormaProyectoPK().getNormaProyId());
			NormaProyecto  nor=normaProyectoDAO.find(norma.getNormaProyectoPK());
			numeralDAO.eliminarNorma((nor));
			
			normasSeleccionadas = new ArrayList<NormaProyecto>();
		}
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("eliminarNorma.hide();dlgAvisoBorrado.show()");
		
		NumeralDAO numeralDAO= new NumeralDAO();
		normas=null;
    	normas = numeralDAO.consultarNormas(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
    	
    	sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        for (NormaProyecto norma : normas) {
        	System.out.println("EFR: in normas");
            try {
                if ( norma.getNormaFechPublic() != null) {
                    norma.setFechaPublicacion(sdf.parse(norma.getNormaFechPublic()));
                }
            } catch (Exception e) { e.printStackTrace(); }
        }    	
    	
    	numeral= new Numeral();
	}
	
	
	public void deleteNumeral(Numeral num){
		NumeralDAO numeralDAO= new NumeralDAO();
		Numeral nume= numeralDAO.find(num.getNumeralPK());
//		numeralDAO.remove(nume);
		numeralDAO.eliminarNumeralSeleccionado(nume);

		normas=null;
    	normas = numeralDAO.consultarNormas(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
		
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        for (NormaProyecto norma : normas) {
        	System.out.println("EFR: in normas");
            try {
                if ( norma.getNormaFechPublic() != null) {
                    norma.setFechaPublicacion(sdf.parse(norma.getNormaFechPublic()));
                }
            } catch (Exception e) { e.printStackTrace(); }
        }
    	
		this.numeral = new Numeral();
		this.numeral = num;
	}
	
	public void cargaELiminar(){
		System.out.println("cargaELiminar");
			for (NormaProyecto  norma:normasSeleccionadas){
				System.out.println("norma"+norma);
			}
	}

	private void loadDataTable(){
		NumeralDAO numeralDAO= new NumeralDAO();
		normas=null;
    	normas = numeralDAO.consultarNormas(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
    	numeral= new Numeral();
	}
	
	public String getMsj() {
		return msj;
	}

	public void setMsj(String msj) {
		this.msj = msj;
	}

	public boolean isBandera() {
		return bandera;
	}

	public void setBandera(boolean bandera) {
		this.bandera = bandera;
	}
}
