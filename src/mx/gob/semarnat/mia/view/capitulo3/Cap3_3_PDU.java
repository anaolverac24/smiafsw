/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view.capitulo3;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.jws.soap.InitParam;
import javax.servlet.http.HttpServletRequest;

import mx.gob.semarnat.mia.Enum.EnumConstantes;
import mx.gob.semarnat.mia.dao.ArchivoBean;
import mx.gob.semarnat.mia.dao.ArchivosAnexosDAO;
import mx.gob.semarnat.mia.dao.CargaBean;
import mx.gob.semarnat.mia.dao.PdumProyectoDAO;
import mx.gob.semarnat.mia.dao.SigeiaDao;
import mx.gob.semarnat.mia.model.ArchivosProyecto;
import mx.gob.semarnat.mia.model.PduProyecto;
import mx.gob.semarnat.mia.model.PdumProyecto;
import mx.gob.semarnat.mia.model.PdumProyectoPK;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.validacion.Validacion;
import mx.gob.semarnat.mia.view.Cap;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mauricio
 */
@ManagedBean
@ViewScoped
public class Cap3_3_PDU extends Cap {

    /**
	 * Constantes
	 */
	private static final long serialVersionUID = -2581446667946289964L;
	private static final Logger LOGGER = LoggerFactory.getLogger(Cap3_3_PDU.class);
	
	private String seccion;
    private List<PdumProyecto> pdus = new ArrayList<PdumProyecto>();
    private List<PdumProyecto> pdusSeleccionados = new ArrayList<PdumProyecto>();
    private PdumProyecto pduSeleccionado = new PdumProyecto();
    private boolean datosIngresadosValidos;
    private boolean seleccionEdicion;
    private String headerText;
    private String alertText;
    private SigeiaDao siDao = new SigeiaDao();
    private ArchivosProyecto archivoTemporal = new ArchivosProyecto();    
    private CargaBean cargaBean;
    private String respuestaPregunta;
	private Boolean respuestaIncidePDU;
	private PdumProyectoDAO pdumProyectoDAO = new PdumProyectoDAO();

    
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    private ArchivosAnexosDAO archivosAnexosDAO = new ArchivosAnexosDAO(); 
    
    private String msj;
	private boolean bandera;
	
	/**
	  * Numero secuencial del registro PDU a editar
	  */
	private int rowIndexPDUEditar;
	

	public Cap3_3_PDU() { 
    	this.respuestaPregunta = "N";
    	this.cargaBean = new CargaBean();
        buscarPdus();
        headerText = "";
        alertText = "";
    }
    
	public void respuestaPDUSi(){
		this.respuestaIncidePDU = true;
		this.respuestaPregunta = "S";
	}

	public void respuestaPDUNo(){
		this.respuestaIncidePDU = false;
		this.respuestaPregunta = "N";
	}    
    
    private void buscarPdus() {
    	try {
			pdus = pdumProyectoDAO.consultarPdumsProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());

	        if (pdus == null || pdus.isEmpty()) {
	            //List<InstrUrbanos> u = siDao.getInstrUbr(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), getProyecto().getClaveProyecto());
	            List<Object[]> u = pdumProyectoDAO.consultarPdumsSIGEIA(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), getProyecto().getClaveProyecto());
	            for (Object[] t : u) {
	                Short id = pdumProyectoDAO.consultarMaxPdumProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
	                PdumProyecto p = new PdumProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
	                p.setPdumComp((String)t[0]);
	                p.setPdumClaveUsos((String)t[3]);
	                p.setPdumNombre((String)t[4]);
	                p.setPdumUsos((String)t[2]);
	                p.setDeSigeia(Boolean.TRUE);
	                p.setClaveOrigenRegistro(0);
	                try {
	                    pdumProyectoDAO.actualizarPDUMProyecto(p);
	                } catch (Exception e) {
	                    e.printStackTrace();
	                }
	                pdus.add(p);
	            }
	        } else {
	            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
	        
	            for (PdumProyecto pdu : pdus) {
	                try {
	                    if (pdu.getPdumFechaPublicacion() != null) {
	                        pdu.setFechaPublicacion(sdf.parse(pdu.getPdumFechaPublicacion()));
	                    }
	                } catch (Exception e) { e.printStackTrace(); }
	            }
	        }
	        
			this.respuestaIncidePDU = Boolean.FALSE;
			this.respuestaIncidePDU = getProyecto().getProyPduAplica()==null?Boolean.FALSE:(getProyecto().getProyPduAplica().equalsIgnoreCase("S")?Boolean.TRUE:Boolean.FALSE);
	        
	        //Comentarios proyecto - eescalona
	        super.setProyectoInfo((short)3, getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
    	} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}        
    }
    
    /**
     * Inicializa un nuevo objeto PDU
     */
    public void crearNuevoPDU() {
    	System.out.println("Creando nuevo PDU...");
    	this.pduSeleccionado = new PdumProyecto();
    	this.seleccionEdicion = false;
    	this.headerText = "Planes y/o programas de desarrollo urbano (PDU) - agregar registro";
    	this.alertText = " Registro guardado exitosamente.";
    }
    
    /**
     * Valida los datos requeridos del objeto PDU en el momento de la ventana modal
     */
    public void validarDatos() {
    	if (pduSeleccionado.getClaveOrigenRegistro() == null) {
        	if (pduSeleccionado.getPdumComp() == null || pduSeleccionado.getPdumNombre() == null || pduSeleccionado.getPdumUsos() == null
        			|| pduSeleccionado.getPdumClaveUsos() == null || pduSeleccionado.getPdumCos() == null || pduSeleccionado.getPdumCus() == null) {
        		this.datosIngresadosValidos = false;
        		LOGGER.debug("Validacion de campos requeridos no cumplida.");
        	} else {
        		this.datosIngresadosValidos = true;
        		LOGGER.debug("Validacion de campos requeridos cumplida.");
        		agregarPdu();   		   		
        	}			
		} else {
        		this.datosIngresadosValidos = true;
        		agregarPdu();   		   					
		}
    }
    
    /**
     * Cambia respuesta
     * @param res
     */
    public void cambiaRespuesta(String res) {
    	this.respuestaPregunta = res;
    }

    /**
     * Guarda un nuevo PDU en la base de datos
     */
    public void agregarPdu() {
    	LOGGER.debug("Guardando nuevo PDU");
    	if (!seleccionEdicion) {
    		short id = getMiaDao().getMaxPdumProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
            pduSeleccionado.setPdumProyectoPK(new PdumProyectoPK(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id));
            pduSeleccionado.setProyecto(getProyecto());
            pduSeleccionado.setClaveOrigenRegistro(EnumConstantes.CLAVEINSERCCIONPROMOVENTE.getClave());
            
            //Se asigna la zona horaria        
	        if (pduSeleccionado.getFechaPublicacion() != null) {
	        	sdf.setTimeZone(TimeZone.getTimeZone("UTC"));	        	
	        	pduSeleccionado.setPdumFechaPublicacion(sdf.format(pduSeleccionado.getFechaPublicacion()));
	        }
	        
	        try {    		
	        	pdumProyectoDAO.guardarPDUM(pduSeleccionado);        	
				pduSeleccionado = new PdumProyecto();
		        buscarPdus();
		        this.alertText = " Registro guardado exitosamente.";
		    	RequestContext context = RequestContext.getCurrentInstance();
				context.execute("modal.hide();modalAlerta.show()");
            } catch (Exception e) {
            	e.printStackTrace();
                System.out.println("Excepcion controlada: " + e.getMessage());
                System.out.println("Volviendo a intentar guardar pdu...");
            }
    	} else {
    		//Se asigna la zona horaria        
	        if (pduSeleccionado.getFechaPublicacion() != null) {
	        	sdf.setTimeZone(TimeZone.getTimeZone("UTC"));	        	
	        	pduSeleccionado.setPdumFechaPublicacion(sdf.format(pduSeleccionado.getFechaPublicacion()));
	        }
	        
	        try {    		
        		pdumProyectoDAO.actualizarPDUMProyecto(pduSeleccionado);            	
		        buscarPdus();
		    	RequestContext context = RequestContext.getCurrentInstance();
				context.execute("dlgEditarPDUM.hide();modalAlerta.show()");
				pduSeleccionado = new PdumProyecto();
				this.seleccionEdicion = false;

	        } catch (Exception e) {
                e.printStackTrace();
            }
    	} 
    }
    
    /**
     * Actualiza el objeto pdu seleccionado desde la vista
     */
    public void mostrarPduSeleccionado() {
    	Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		short pduId = 0;
		String idPredio = params.get("id").toString();
		pduId = Short.parseShort(idPredio);
		System.out.println("Buscando pdu con id: " + pduId);
		this.pduSeleccionado = pdumProyectoDAO.consultarPdumPorId(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), pduId);
		
		// Si existe una fecha para el registro seleccionado a editar.
        if (pduSeleccionado.getPdumFechaPublicacion() != null) {      	
			try {
				DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH); 
				Date fechaUltimaActualizacion = sdf.parse(pduSeleccionado.getPdumFechaPublicacion());
				pduSeleccionado.setFechaPublicacion(fechaUltimaActualizacion);
	
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	
        }
        
        // Número secuencial a mostrar en "Editar registro #"
        rowIndexPDUEditar = Integer.parseInt(params.get("prowindexPDU").toString());
		
		this.seleccionEdicion = true;
		this.alertText = " Registro actualizado exitosamente.";
    }
    
    /**
     * Elimina los pduSeleccionados
     */
    public void eliminarPdus() {
    	for (PdumProyecto pdumProyecto : pdusSeleccionados) {
    		PdumProyecto pduEliminar = pdumProyectoDAO.consultarPdumPorId(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), pdumProyecto.getPdumProyectoPK().getPdumId());
    		try {
    			System.out.println("Eliminando pdu con id: " + pduEliminar.getPdumProyectoPK().getPdumId());
    			//Eliminacion de archivo
    			if (pduEliminar.getIdArchivoProyecto() != null) {
    				ArchivoBean.eliminarArchivoProyecto(pduEliminar.getIdArchivoProyecto());
    			}
    			pdumProyectoDAO.eliminarPdumProyecto(pduEliminar.getPdumProyectoPK().getFolioProyecto(), pduEliminar.getPdumProyectoPK().getSerialProyecto(),
                		pduEliminar.getPdumProyectoPK().getPdumId()); 
                System.out.println("Pdu eliminado...");
                
                pdusSeleccionados = new ArrayList<PdumProyecto>();
            } catch (Exception e) {
                e.printStackTrace();
            }
		}
    	buscarPdus();
    	this.alertText = " Registro eliminado exitosamente.";
    }   
    
    /**
     * Inicializa un nuevo archivo para ser cargado
     */
    public void iniciarArchivo() {
    	archivoTemporal = new ArchivosProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), 
    			(short) 3, (short) 6, (short) 0, (short) 0);
    	Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		short pduId = 0;
		String id = params.get("id").toString();
		pduId = Short.parseShort(id);
		this.pduSeleccionado = getMiaDao().getPdumProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), pduId);
		this.seleccionEdicion = true;
		this.headerText = "Guardar archivo para el PDU #" + pduSeleccionado.getPdumProyectoPK().getPdumId();
		this.alertText = " Archivo guardado correctamente.";
		
		cargaBean = new CargaBean();
		msj = " ";
    }
    
    /**
     * Guarda el archivo pdf en la BD y el Servidor 
     */
    public void guardarArchivo() {
    	try {
        	short id;
    		id = miaDao.getMaxArchivoEtapaProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), 
    				archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(), archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());

    		archivoTemporal.setId(id);
	
			bandera = cargaBean.GuardaAnexos(archivoTemporal);
			if (bandera == true) {
			
			Short idSeq = archivosAnexosDAO .consultarMaxIdSeq();
			if (idSeq != null) {
				this.pduSeleccionado.setIdArchivoProyecto((short) (idSeq));
			} else {
				this.pduSeleccionado.setIdArchivoProyecto((short) 1);
			}
		
//	    	this.pduSeleccionado.setIdArchivoProyecto(archivoTemporal.getSeqId());
	    	agregarPdu();  	
	    	msj = "";
	    	cargaBean = new CargaBean();
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalSubirArchivo.hide();");
			
			this.alertText = "Archivo guardado correctamente.";
			
			}else {
				msj = "Se requiere cargar un archivo";
				
			}
	    	
    	} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    /**
     * Guarda el avance del proyecto
     */
    public void guardarAvance() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        try {
        	System.out.println("Guardando respuesta de la pregunta: " + respuestaPregunta);
			miaDao.actualizarPDURespuestaAplica(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), respuestaPregunta);
			
			//Si la respuesta seleccionada a la pregunta es SI
			if (respuestaPregunta.equals("S")) {
				//Verifica si hay registros pdus guardados.
	            if (pdus.size() >= 1) {
	                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
	                try {
	                    getMiaDao().guardarAvance(getProyecto(), new Short("2"), "36");

	                    System.out.println("validar " + seccion);
	                    Validacion v = new Validacion();
	                    String msg = v.validar(seccion, getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
	                    
	                    if (!msg.isEmpty()) {
	                        return;
	                    }

	                    //reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
	                    reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
	                    if (Integer.parseInt(getMiaDao().avanceProyecto(getProyecto())) == 99) {
	                      //  reqcontEnv.execute("alert('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.')");
	                        reqcontEnv.execute("mensajes('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.', 'success')");
	                    }

	                    //Guardado de componentes en información adicional - eescalona
	                    super.guardarComentarios(); 
	                } catch (Exception e) {
	                    System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
	                }
	                //</editor-fold>
	            }				
			} else {
                try {
                    getMiaDao().guardarAvance(getProyecto(), new Short("2"), "36");

                    System.out.println("validar " + seccion);
                    Validacion v = new Validacion();
                    String msg = v.validar(seccion, getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
                    
                    if (!msg.isEmpty()) {
                        return;
                    }

                    //reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
                    reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
                    if (Integer.parseInt(getMiaDao().avanceProyecto(getProyecto())) == 99) {
                      //  reqcontEnv.execute("alert('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.')");
                        reqcontEnv.execute("mensajes('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.', 'success')");
                    }

                    //Guardado de componentes en información adicional - eescalona
                    super.guardarComentarios();
                } catch (Exception e) {
                    System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                }				
			}       
            
        } catch (Exception e) {
            e.printStackTrace();
            //reqcontEnv.execute("alert('No se ha podido guardar la información, Intente mas tarde.')");
            reqcontEnv.execute("mensajes('No se ha podido guardar la información, Intente mas tarde.', 'danger')");
        }
    }
    
    /**
     * @return the pdus
     */
    public List<PdumProyecto> getPdus() {
        return pdus;
    }

    /**
     * @param pdus the pdus to set
     */
    public void setPdus(List<PdumProyecto> pdus) {
        this.pdus = pdus;
    }

    /**
     * @return the seccion
     */
    public String getSeccion() {
        return seccion;
    }

    /**
     * @param seccion the seccion to set
     */
    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

	/**
	 * @return the pdusSeleccionados
	 */
	public List<PdumProyecto> getPdusSeleccionados() {
		return pdusSeleccionados;
	}

	/**
	 * @param pdusSeleccionados the pdusSeleccionados to set
	 */
	public void setPdusSeleccionados(List<PdumProyecto> pdusSeleccionados) {
		this.pdusSeleccionados = pdusSeleccionados;
	}

	/**
	 * @return the pduSeleccionado
	 */
	public PdumProyecto getPduSeleccionado() {
		return pduSeleccionado;
	}

	/**
	 * @param pduSeleccionado the pduSeleccionado to set
	 */
	public void setPduSeleccionado(PdumProyecto pduSeleccionado) {
		this.pduSeleccionado = pduSeleccionado;
	}

	/**
	 * @return the datosIngresadosValidos
	 */
	public boolean isDatosIngresadosValidos() {
		return datosIngresadosValidos;
	}

	/**
	 * @param datosIngresadosValidos the datosIngresadosValidos to set
	 */
	public void setDatosIngresadosValidos(boolean datosIngresadosValidos) {
		this.datosIngresadosValidos = datosIngresadosValidos;
	}

	/**
	 * @return the seleccionEdicion
	 */
	public boolean isSeleccionEdicion() {
		return seleccionEdicion;
	}

	/**
	 * @param seleccionEdicion the seleccionEdicion to set
	 */
	public void setSeleccionEdicion(boolean seleccionEdicion) {
		this.seleccionEdicion = seleccionEdicion;
	}

	/**
	 * @return the headerText
	 */
	public String getHeaderText() {
		return headerText;
	}

	/**
	 * @param headerText the headerText to set
	 */
	public void setHeaderText(String headerText) {
		this.headerText = headerText;
	}

	/**
	 * @return the alertText
	 */
	public String getAlertText() {
		return alertText;
	}

	/**
	 * @param alertText the alertText to set
	 */
	public void setAlertText(String alertText) {
		this.alertText = alertText;
	}

	/**
	 * @return the cargaBean
	 */
	public CargaBean getCargaBean() {
		return cargaBean;
	}

	/**
	 * @param cargaBean the cargaBean to set
	 */
	public void setCargaBean(CargaBean cargaBean) {
		this.cargaBean = cargaBean;
	}

	/**
	 * @return the archivoTemporal
	 */
	public ArchivosProyecto getArchivoTemporal() {
		return archivoTemporal;
	}

	/**
	 * @param archivoTemporal the archivoTemporal to set
	 */
	public void setArchivoTemporal(ArchivosProyecto archivoTemporal) {
		this.archivoTemporal = archivoTemporal;
	}

	public String getMsj() {
		return msj;
	}

	public void setMsj(String msj) {
		this.msj = msj;
	}

	public boolean isBandera() {
		return bandera;
	}

	public void setBandera(boolean bandera) {
		this.bandera = bandera;
	}

	
	/**
	 * @return the respuestaIncidePDU
	 */
	public Boolean getRespuestaIncidePDU() {
		return respuestaIncidePDU;
	}

	
	/**
	 * @param respuestaIncidePDU the respuestaIncidePDU to set
	 */
	public void setRespuestaIncidePDU(Boolean respuestaIncidePDU) {
		this.respuestaIncidePDU = respuestaIncidePDU;
	}
	
	
	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla PDU
	 */
	public int getRowIndexPDUEditar() {
		return rowIndexPDUEditar;
	}

	
	/**
	 * Asigna valor a variable que guarda Numero secuencial de registro PDU a editar
	 * @param rowIndexPDUEditar
	 */
	public void setRowIndexPDUEditar(int rowIndexPDUEditar) {
		this.rowIndexPDUEditar = rowIndexPDUEditar;
	}
	
}
