/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view.capitulo3;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import mx.gob.semarnat.mia.Enum.EnumConstantes;
import mx.gob.semarnat.mia.dao.ArchivoBean;
import mx.gob.semarnat.mia.dao.CargaBean;
import mx.gob.semarnat.mia.dao.MiaDao;
import mx.gob.semarnat.mia.dao.SigeiaDao;
import mx.gob.semarnat.mia.model.AnpProyecto;
import mx.gob.semarnat.mia.model.AnpProyectoPK;
import mx.gob.semarnat.mia.model.ArchivosProyecto;
import mx.gob.semarnat.mia.model.ArticuloReglaAnp;
import mx.gob.semarnat.mia.model.ArticuloReglaAnpPK;
import mx.gob.semarnat.mia.model.ComentarioProyecto;
import mx.gob.semarnat.mia.model.PdumProyecto;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.validacion.Validacion;
import mx.gob.semarnat.mia.view.Cap;

import org.json.simple.parser.ParseException;
import org.primefaces.context.RequestContext;

/**
 *
 * @author mauricio
 */
@ManagedBean
@ViewScoped
public class Cap3_3_6 extends Cap {

    /**
	 * Constante 
	 */
	private static final long serialVersionUID = -9207973813449354474L;
	
	private String seccion;
    private MiaDao miaDao = new MiaDao();
    private final SigeiaDao siDao = new SigeiaDao();
    private List<AnpProyecto> anpProyectos = new ArrayList<AnpProyecto>();
    private List<ArticuloReglaAnp> vinculacionDecreto = new ArrayList<ArticuloReglaAnp>();
    private List<ArticuloReglaAnp> vinculacionPrograma = new ArrayList<ArticuloReglaAnp>();
    private List<AnpProyecto> anpProyectosSeleccionados = new ArrayList<AnpProyecto>();
    private List<ArticuloReglaAnp> vinculacionDecretoSeleccionados = new ArrayList<ArticuloReglaAnp>();
    private List<ArticuloReglaAnp> vinculacionProgramaSeleccionados = new ArrayList<ArticuloReglaAnp>();
    private Short idArticulo = null;
    private boolean respuestaIncideANP;
    private boolean datosIngresadosValidos;
    private boolean datosIngresadosArticuloValidos;
    private boolean seleccionEdicion;
    private boolean seleccionEdicionArticuloRegla;
    private AnpProyecto anpProyectoSeleccionado = new AnpProyecto();
    private ArticuloReglaAnp articuloReglaAnpSeleccionado = new ArticuloReglaAnp();
    private String headerText;
    private String alertText;
    private short idAnpSeleccionado;
    private ArchivosProyecto archivoTemporal = new ArchivosProyecto();
    private CargaBean cargaBean;
    private boolean archivoCargarEsDecreto;
    private String msj;
	private boolean bandera;
	
	/**
	  * Número secuencial del registro a editar en cualquiera de las 3 tablas ANP
	  */
	private int rowIndexANPEditar;


	/**
     * Constructor
     */
    public Cap3_3_6() {
    	this.cargaBean = new CargaBean();
    	try {
    		cargarAnps();
            cargarAnpsVinculacionDecreto();
            cargarAnpsVinculacionPrograma();
		} catch (Exception e) {
			e.printStackTrace();
		}     
        
        //Comentarios proyecto - eescalona
        super.setProyectoInfo((short)3, getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
        this.headerText = "";        
    }
    
    /**
     * Cambio de bandera para la respuesta ANP
     */
    public void respuestaANPNo() {
    	this.respuestaIncideANP = false;
    }
    
    /**
     * Cambio de bandera para la respuesta ANP
     */
    public void respuestaANPSi() {
    	this.respuestaIncideANP = true;
    }
    
    /**
     * Valida los datos ingresados del objeto anp en la modal actual
     */
    public void validarDatos() {
    	if (anpProyectoSeleccionado.getAnpTipo() == null && anpProyectoSeleccionado.getAnpNombre() == null 
    			&& anpProyectoSeleccionado.getAnpCategoriaManejo() == null && anpProyectoSeleccionado.getAnpUltimoDecreto() == null) {
    		this.datosIngresadosValidos = false;
    		System.out.println("Datos no validos para registrar ANP");
    	} else {
    		System.out.println("Datos validos para registrar ANP");
    		this.datosIngresadosValidos = true;
    		guardarAnp();
    	}
    }
    
    public void validarDatosArticuloRegla() {
    	if(articuloReglaAnpSeleccionado.getAnpArticuloRegla() == null && articuloReglaAnpSeleccionado.getAnpArtNomVinculacion() == null) {
    		this.datosIngresadosArticuloValidos = false;
    		System.out.println("Datos no validos para registrar ARTICULO");
    	} else {
    		System.out.println("Datos no validos para registrar ARTICULO");
    		this.datosIngresadosArticuloValidos = true;
    		guardaArticuloRegla();
    	}
    }

    /**
     * Crea un nuevo objeto ANP
     */
    public void crearNuevoANP() {   
    	System.out.println("Se inicializa un nuevo Objeto ANP");
    	this.anpProyectoSeleccionado = new AnpProyecto();
    	this.seleccionEdicion = false;
    	this.headerText = "Agregar ANP";
    	this.alertText = "Registro guardado exitosamente.";
    }
    
    /**
     * Crea un nuevo objeto ArticuloReglaAnp
     */
    public void crearNuevoArticuloReglaDecreto() {    	
    	System.out.println("Se inicializa un nuevo Objeto ARTICULO DECRETO");
    	this.articuloReglaAnpSeleccionado = new ArticuloReglaAnp();
    	this.articuloReglaAnpSeleccionado.setAnpArticuloRegla("A");
    	this.seleccionEdicionArticuloRegla = false;
    	this.headerText = "Agregar registro";
    	this.alertText = "Registro guardado exitosamente.";
    }
    
    /**
     * Crea un nuevo objeto ArticuloReglaAnp
     */
    public void crearNuevoArticuloReglaPrograma() { 
    	System.out.println("Se inicializa un nuevo Objeto ARTICULO PROGRAMA");
    	this.articuloReglaAnpSeleccionado = new ArticuloReglaAnp();
    	this.articuloReglaAnpSeleccionado.setAnpArticuloRegla("R");
    	this.seleccionEdicionArticuloRegla = false;
    	this.headerText = "Agregar registro";
    	this.alertText = "Registro guardado exitosamente.";
    }
    
    /**
     * Carga en la modal el AnpProyecto seleccionado
     */
    public void mostrarAnpSeleccionado() {
    	System.out.println("Se busca el anp seleccionado...");
    	Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		short anpId = 0;
		String idPredio = params.get("id").toString();
		anpId = Short.parseShort(idPredio);
		
		// Número secuencial a mostrar en "Editar registro #"
		rowIndexANPEditar = Integer.parseInt(params.get("prowindexANP").toString());
		
		this.anpProyectoSeleccionado = getMiaDao().getAnpProyectoById(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), anpId);
		this.seleccionEdicion = true;
		this.headerText = "Editar área Natural Protegida # " + rowIndexANPEditar;
		this.alertText = "Registro actualizado exitosamente.";
    }
    
    /**
     * Carga en la modal el ArticuloReglaAnp seleccionado
     */
    public void mostrarArticuloReglaSeleccionado(String tipoRegla) {
    	System.out.println("Se busca el articulo seleccionado...");
    	Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		short anpId = 0;
		String idPredio = params.get("id").toString();
		anpId = Short.parseShort(idPredio);
		
		// Número secuencial a mostrar en "Editar registro #"
		rowIndexANPEditar = Integer.parseInt(params.get("prowindexANP").toString());
				
		this.articuloReglaAnpSeleccionado = getMiaDao().getArticuloRegla(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), tipoRegla, anpId);
		this.seleccionEdicionArticuloRegla = true;
		this.headerText = "Editar registro # " + rowIndexANPEditar;
		this.alertText = "Registro actualizado exitosamente.";
    }
    
    /**
     * Guarda y actualiza un objeto ArticuloReglaAnp la bd
     */
    public void guardaArticuloRegla() {
    	System.out.println("Guardando el articulo...");
    	if (!seleccionEdicionArticuloRegla) {
    		short id = getMiaDao().getMaxArticuloReglaAnpProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
        	ArticuloReglaAnpPK pk = new ArticuloReglaAnpPK(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), idAnpSeleccionado, id);
        	articuloReglaAnpSeleccionado.setArticuloReglaAnpPK(pk);
        	
        	AnpProyecto anp = getMiaDao().getAnpProyectoById(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), idAnpSeleccionado);
        	articuloReglaAnpSeleccionado.setAnpProyecto(anp);	
        	
        	try {    		
        		getMiaDao().merge(articuloReglaAnpSeleccionado);            	
            } catch (Exception e) {
                e.printStackTrace();
            }
    	} else {    		
    		try {    		
        		getMiaDao().persist(articuloReglaAnpSeleccionado);            	
            } catch (Exception e) {
                e.printStackTrace();
            }
    	}    	
    	cargarAnps();
    	cargarAnpsVinculacionDecreto();
    	cargarAnpsVinculacionPrograma();
    }
    
    /**
     * Guarda y actualiza un objeto AnpProyectoen la bd
     */
    public void guardarAnp() {
    	System.out.println("Guardando el ANP...");
    	if (!seleccionEdicion) {    		
    		short id = getMiaDao().getMaxAnpProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
    		System.out.println("Insertando nuevo ANP con ID: " + id);
        	AnpProyectoPK pk = new AnpProyectoPK(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
        	anpProyectoSeleccionado.setAnpProyectoPK(pk);
        	anpProyectoSeleccionado.setClaveOrigenRegistro(EnumConstantes.CLAVEINSERCCIONPROMOVENTE.getClave());
        	
        	try {    		
        		getMiaDao().persist(anpProyectoSeleccionado);            	
            } catch (Exception e) {
                System.out.println("Excepcion controlada: " + e.getMessage());
                System.out.println("Volviendo a intentar guardar ANP...");
                try {
					getMiaDao().persist(anpProyectoSeleccionado);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
            }
    	} else {
    		System.out.println("Actualizando ANP");
    		try {    		
            	getMiaDao().merge(anpProyectoSeleccionado);        	
            } catch (Exception e) {
                e.printStackTrace();
            }   
    	}  	
    	RequestContext context = RequestContext.getCurrentInstance();
		RequestContext.getCurrentInstance().reset(":foDatosGen:pnlAnps");
		context.execute("modalANP.hide();modalAlerta.show()");
    	cargarAnps();
    }
    
    /**
     * Elimina los Proyectos Anps seleccionados
     */
    public void eliminarAnps() {
    	System.out.println("Eliminando ANP...");
    	for (AnpProyecto anpProyecto : anpProyectosSeleccionados) {
    		System.out.println("ANP id: " + anpProyecto.getAnpProyectoPK().getAnpId());
    		AnpProyecto anpEliminar = getMiaDao().getAnpProyectoById(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), anpProyecto.getAnpProyectoPK().getAnpId());
    		try { 
    			if (anpEliminar.getArticuloReglaAnpList() != null) {
    				System.out.println("Eliminando articulos hijos de ANP");
    				for (ArticuloReglaAnp anpReg : anpEliminar.getArticuloReglaAnpList()){
        				getMiaDao().eliminarAnpProyectoRegla(anpReg.getArticuloReglaAnpPK().getFolioProyecto(), anpReg.getArticuloReglaAnpPK().getSerialProyecto(),
        						anpReg.getArticuloReglaAnpPK().getAnpArtRegId());
        			}
    			}
    			//Eliminacion de archivos
    			if (anpEliminar.getIdArchivoDecreto() != null) {
    				ArchivoBean.eliminarArchivoProyecto(anpEliminar.getIdArchivoDecreto());
    			}
    			if (anpEliminar.getIdArchivoPrograma() != null) {
    				ArchivoBean.eliminarArchivoProyecto(anpEliminar.getIdArchivoPrograma());
    			}
    			getMiaDao().eliminarAnpProyecto(anpEliminar.getAnpProyectoPK().getFolioProyecto(), anpEliminar.getAnpProyectoPK().getSerialProyecto(), 
    					anpEliminar.getAnpProyectoPK().getAnpId());                
            } catch (Exception e) {
                System.out.println("Excepcion controlada: " + e.getMessage());
                System.out.println("Volviendo intentar a eliminar registro...");
                anpEliminar = getMiaDao().getAnpProyectoById(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), anpProyecto.getAnpProyectoPK().getAnpId());
        		try { 
        			if (anpEliminar.getArticuloReglaAnpList() != null) {
        				System.out.println("Eliminando articulos hijos de ANP");
        				for (ArticuloReglaAnp anpReg : anpEliminar.getArticuloReglaAnpList()){
            				getMiaDao().eliminarAnpProyectoRegla(anpReg.getArticuloReglaAnpPK().getFolioProyecto(), anpReg.getArticuloReglaAnpPK().getSerialProyecto(),
            						anpReg.getArticuloReglaAnpPK().getAnpArtRegId());
            			}
        			}   		
        			//Eliminacion de archivos
        			if (anpEliminar.getIdArchivoDecreto() != null) {
        				ArchivoBean.eliminarArchivoProyecto(anpEliminar.getIdArchivoDecreto());
        			}
        			if (anpEliminar.getIdArchivoPrograma() != null) {
        				ArchivoBean.eliminarArchivoProyecto(anpEliminar.getIdArchivoPrograma());
        			}
        			getMiaDao().eliminarAnpProyecto(anpEliminar.getAnpProyectoPK().getFolioProyecto(), anpEliminar.getAnpProyectoPK().getSerialProyecto(), 
        					anpEliminar.getAnpProyectoPK().getAnpId());
        		} catch (Exception e2) {
        			e2.printStackTrace();
        		}
            }
		}
    	cargarAnps();
    	cargarAnpsVinculacionDecreto();
    	cargarAnpsVinculacionPrograma();
    	this.alertText = "Registro eliminado exitosamente.";
    }
    
    /**
     * Elimina los Proyectos ArticuloReglaAnp decreto seleccionados
     */
    public void eliminarArticulosReglaDecreto() {
    	System.out.println("Eliminando articulos decreto...");
    	for (ArticuloReglaAnp anpProyecto : vinculacionDecretoSeleccionados) {
    		ArticuloReglaAnp anpEliminar = getMiaDao().getArticuloRegla(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), "A",anpProyecto.getArticuloReglaAnpPK().getAnpArtRegId());
    		try {    			
    			getMiaDao().eliminarAnpProyectoRegla(anpEliminar.getArticuloReglaAnpPK().getFolioProyecto(), anpEliminar.getArticuloReglaAnpPK().getSerialProyecto(),
    					anpEliminar.getArticuloReglaAnpPK().getAnpArtRegId());               
            } catch (Exception e) {
                e.printStackTrace();
            }
		}
    	cargarAnpsVinculacionDecreto();
    	this.alertText = "Registro eliminado exitosamente.";
    }
    
    /**
     * Elimina los ArticuloReglaAnp programa Anps seleccionados
     */
    public void eliminarArticulosReglaPrograma() {
    	System.out.println("Eliminando articulos programa...");
    	for (ArticuloReglaAnp anpProyecto : vinculacionProgramaSeleccionados) {
    		ArticuloReglaAnp anpEliminar = getMiaDao().getArticuloRegla(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), "R",anpProyecto.getArticuloReglaAnpPK().getAnpArtRegId());
    		try {    			
    			getMiaDao().eliminarAnpProyectoRegla(anpProyecto.getArticuloReglaAnpPK().getFolioProyecto(), anpProyecto.getArticuloReglaAnpPK().getSerialProyecto(),
    					anpEliminar.getArticuloReglaAnpPK().getAnpArtRegId());               
            } catch (Exception e) {
                e.printStackTrace();
            }
		}
    	cargarAnpsVinculacionPrograma();
    	this.alertText = "Registro eliminado exitosamente.";
    }
    
    /**
     * Guardar avance
     */
    public void guardarAvance() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        try {            
            try {
                System.out.println("validar " + seccion);
                Validacion v = new Validacion();
                String msg = v.validar(seccion, getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
                if (!msg.isEmpty()) {
                    return;
                }
            } catch (Exception err) {
                err.printStackTrace();
            }

            String respuestaPregunta = respuestaIncideANP ? "S" : "N";
			System.out.println("Actualizando la respuesta de la pregunta: " + respuestaPregunta);
			miaDao.actualizarANPRespuestaAplica(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), respuestaPregunta);
            System.out.println("Guardando avance...");
            miaDao.guardarAvance(getProyecto(), new Short("2"), "35");
            //reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
            reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
            
        } catch (Exception ee) {
            //reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.')");
        	reqcontEnv.execute("mensajes('No se ha podido guardar la información, Intente más tarde.', 'danger')");
        	
            ee.printStackTrace();
        }
        
        super.guardarComentarios();
    }
    
    /**
     * Carga la lista de proyectos anp
     */
    public void cargarAnps() {
        Proyecto proyecto = getProyecto();
        Short id = 0;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s"); 
        anpProyectos.clear();
        anpProyectosSeleccionados.clear();
        anpProyectos = getMiaDao().getAnpProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
        if (anpProyectos.isEmpty()) {
            List<Object[]> e = siDao.getAnpEstatal2(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            for (Object[] t : e) {
                AnpProyecto u = new AnpProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
                u.setAnpTipo("Estatal");
                u.setAnpCategoriaManejo((String)t[1]);
                u.setAnpNombre((String)t[0]);
                u.setClaveOrigenRegistro(0);
                try
                { 
                    u.setAnpFechaManejo(format.parse(t[2].toString()));                
                }catch (Exception tt) {
                    tt.printStackTrace();
                }
                try
                {
                    u.setAnpUltimoDecreto(format.parse(t[3].toString()));
                }catch (Exception tz) {
                    tz.printStackTrace();
                }                
                anpProyectos.add(u);
                id++;
            }

            List<Object[]> e2 = siDao.getAnpMuicipal2(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            for (Object[] t : e2) {
                AnpProyecto u = new AnpProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
                u.setAnpTipo("Municipal");
                u.setAnpCategoriaManejo((String)t[1]);
                u.setAnpNombre((String)t[0]);
                try
                { 
                    u.setAnpFechaManejo(format.parse(t[2].toString()));                
                }catch (Exception tt) {
                    tt.printStackTrace();
                }
                try
                {
                    u.setAnpUltimoDecreto(format.parse(t[3].toString()));
                }catch (Exception tz) {
                    tz.printStackTrace();
                } 
                anpProyectos.add(u);
                id++;
            }

            List<Object[]> e3 = siDao.getAnpFederal2(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            for (Object[] t : e3) {
                AnpProyecto u = new AnpProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
                u.setAnpTipo("Federal");
                u.setAnpCategoriaManejo((String)t[1]);
                u.setAnpNombre((String)t[0]);
                try
                { 
                    u.setAnpFechaManejo(format.parse(t[2].toString()));                
                }catch (Exception tt) {
                    tt.printStackTrace();
                }
                try
                {
                    u.setAnpUltimoDecreto(format.parse(t[3].toString()));
                }catch (Exception tz) {
                    tz.printStackTrace();
                } 
                anpProyectos.add(u);
                id++;
            }

            for (AnpProyecto a : anpProyectos) {
                try {
                    getMiaDao().merge(a);
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
            }

        }
    }
    
    /**
     * Carga loa lista de anps vinculacion decreto
     */
    public void cargarAnpsVinculacionDecreto(){
    	vinculacionDecreto = getMiaDao().getArticuloReglaAnp(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), "A");
    }
    
    /**
     * Carga loa lista de anps vinculacion programa
     */
    public void cargarAnpsVinculacionPrograma() {
    	vinculacionPrograma = getMiaDao().getArticuloReglaAnp(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), "R");
    }
    
    /**
     * Inicializa un nuevo archivo para ser cargado
     */
    public void iniciarArchivo() {
    	archivoTemporal = new ArchivosProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), 
    			(short) 3, (short) 5, (short) 0, (short) 0);
    	Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		short anpId = 0;
		String id = params.get("id").toString();
		anpId = Short.parseShort(id);
		String esArchivoDecreto = params.get("esArchivoDecreto").toString();
		this.archivoCargarEsDecreto = esArchivoDecreto.equals("true") ? true : false;
		this.anpProyectoSeleccionado = getMiaDao().getAnpProyectoById(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), anpId);
		this.seleccionEdicion = true;
		this.headerText = "Guardar archivo para el ANP #" + anpProyectoSeleccionado.getAnpProyectoPK().getAnpId();
		this.alertText = "Archivo guardado correctamente";
		msj=" ";
    }
    
    /**
     * Guarda el archivo pdf en la BD y el Servidor 
     */
    public void guardarArchivo() {
    	try {
        	short id;
    		id = miaDao.getMaxArchivoEtapaProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), 
    				archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(), archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());

    		archivoTemporal.setId(id);
			
			bandera = cargaBean.GuardaAnexos(archivoTemporal);
			if (bandera == true) {

	    	if (archivoCargarEsDecreto) {
	    		this.anpProyectoSeleccionado.setIdArchivoDecreto(archivoTemporal.getSeqId());
	    	} else {
	    		this.anpProyectoSeleccionado.setIdArchivoPrograma(archivoTemporal.getSeqId());
	    	}    	
	    	guardarAnp(); 
	    	msj = " ";
	    	cargaBean = new CargaBean();
	    	RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalSubirArchivo.hide()");
	    	
	    	
			}else {
				msj = "Se requiere cargar un archivo";
				
			}
    	} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    /**
     * @return the anpProyectos
     */
    public List<AnpProyecto> getAnpProyectos() {
        return anpProyectos;
    }

    /**
     * @param anpProyectos the anpProyectos to set
     */
    public void setAnpProyectos(List<AnpProyecto> anpProyectos) {
        this.anpProyectos = anpProyectos;
    }

    /**
     * @return the vinculacionDecreto
     */
    public List<ArticuloReglaAnp> getVinculacionDecreto() {
        return vinculacionDecreto;
    }

    /**
     * @param vinculacionDecreto the vinculacionDecreto to set
     */
    public void setVinculacionDecreto(List<ArticuloReglaAnp> vinculacionDecreto) {
        this.vinculacionDecreto = vinculacionDecreto;
    }

    /**
     * @return the vinculacionPrograma
     */
    public List<ArticuloReglaAnp> getVinculacionPrograma() {
        return vinculacionPrograma;
    }

    /**
     * @param vinculacionPrograma the vinculacionPrograma to set
     */
    public void setVinculacionPrograma(List<ArticuloReglaAnp> vinculacionPrograma) {
        this.vinculacionPrograma = vinculacionPrograma;
    }

    /**
     * @return the seccion
     */
    public String getSeccion() {
        return seccion;
    }

    /**
     * @param seccion the seccion to set
     */
    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

	/**
	 * @return the anpProyectosSeleccionados
	 */
	public List<AnpProyecto> getAnpProyectosSeleccionados() {
		return anpProyectosSeleccionados;
	}

	/**
	 * @param anpProyectosSeleccionados the anpProyectosSeleccionados to set
	 */
	public void setAnpProyectosSeleccionados(
			List<AnpProyecto> anpProyectosSeleccionados) {
		this.anpProyectosSeleccionados = anpProyectosSeleccionados;
	}

	/**
	 * @return the vinculacionDecretoSeleccionados
	 */
	public List<ArticuloReglaAnp> getVinculacionDecretoSeleccionados() {
		return vinculacionDecretoSeleccionados;
	}

	/**
	 * @param vinculacionDecretoSeleccionados the vinculacionDecretoSeleccionados to set
	 */
	public void setVinculacionDecretoSeleccionados(
			List<ArticuloReglaAnp> vinculacionDecretoSeleccionados) {
		this.vinculacionDecretoSeleccionados = vinculacionDecretoSeleccionados;
	}

	/**
	 * @return the vinculacionProgramaSeleccionados
	 */
	public List<ArticuloReglaAnp> getVinculacionProgramaSeleccionados() {
		return vinculacionProgramaSeleccionados;
	}

	/**
	 * @param vinculacionProgramaSeleccionados the vinculacionProgramaSeleccionados to set
	 */
	public void setVinculacionProgramaSeleccionados(
			List<ArticuloReglaAnp> vinculacionProgramaSeleccionados) {
		this.vinculacionProgramaSeleccionados = vinculacionProgramaSeleccionados;
	}

	/**
	 * @return the respuestaIncideANP
	 */
	public boolean isRespuestaIncideANP() {
		return respuestaIncideANP;
	}

	/**
	 * @param respuestaIncideANP the respuestaIncideANP to set
	 */
	public void setRespuestaIncideANP(boolean respuestaIncideANP) {
		this.respuestaIncideANP = respuestaIncideANP;
	}

	/**
	 * @return the headerText
	 */
	public String getHeaderText() {
		return headerText;
	}

	/**
	 * @param headerText the headerText to set
	 */
	public void setHeaderText(String headerText) {
		this.headerText = headerText;
	}

	/**
	 * @return the seleccionEdicion
	 */
	public boolean isSeleccionEdicion() {
		return seleccionEdicion;
	}

	/**
	 * @param seleccionEdicion the seleccionEdicion to set
	 */
	public void setSeleccionEdicion(boolean seleccionEdicion) {
		this.seleccionEdicion = seleccionEdicion;
	}

	/**
	 * @return the datosIngresadosValidos
	 */
	public boolean isDatosIngresadosValidos() {
		return datosIngresadosValidos;
	}

	/**
	 * @param datosIngresadosValidos the datosIngresadosValidos to set
	 */
	public void setDatosIngresadosValidos(boolean datosIngresadosValidos) {
		this.datosIngresadosValidos = datosIngresadosValidos;
	}

	/**
	 * @return the anpProyectoSeleccionado
	 */
	public AnpProyecto getAnpProyectoSeleccionado() {
		return anpProyectoSeleccionado;
	}

	/**
	 * @param anpProyectoSeleccionado the anpProyectoSeleccionado to set
	 */
	public void setAnpProyectoSeleccionado(AnpProyecto anpProyectoSeleccionado) {
		this.anpProyectoSeleccionado = anpProyectoSeleccionado;
	}

	/**
	 * @return the alertText
	 */
	public String getAlertText() {
		return alertText;
	}

	/**
	 * @param alertText the alertText to set
	 */
	public void setAlertText(String alertText) {
		this.alertText = alertText;
	}

	/**
	 * @return the datosIngresadosArticuloValidos
	 */
	public boolean isDatosIngresadosArticuloValidos() {
		return datosIngresadosArticuloValidos;
	}

	/**
	 * @param datosIngresadosArticuloValidos the datosIngresadosArticuloValidos to set
	 */
	public void setDatosIngresadosArticuloValidos(
			boolean datosIngresadosArticuloValidos) {
		this.datosIngresadosArticuloValidos = datosIngresadosArticuloValidos;
	}

	/**
	 * @return the seleccionEdicionArticuloRegla
	 */
	public boolean isSeleccionEdicionArticuloRegla() {
		return seleccionEdicionArticuloRegla;
	}

	/**
	 * @param seleccionEdicionArticuloRegla the seleccionEdicionArticuloRegla to set
	 */
	public void setSeleccionEdicionArticuloRegla(
			boolean seleccionEdicionArticuloRegla) {
		this.seleccionEdicionArticuloRegla = seleccionEdicionArticuloRegla;
	}

	/**
	 * @return the articuloReglaAnpSeleccionado
	 */
	public ArticuloReglaAnp getArticuloReglaAnpSeleccionado() {
		return articuloReglaAnpSeleccionado;
	}

	/**
	 * @param articuloReglaAnpSeleccionado the articuloReglaAnpSeleccionado to set
	 */
	public void setArticuloReglaAnpSeleccionado(
			ArticuloReglaAnp articuloReglaAnpSeleccionado) {
		this.articuloReglaAnpSeleccionado = articuloReglaAnpSeleccionado;
	}

	/**
	 * @return the idAnpSeleccionado
	 */
	public short getIdAnpSeleccionado() {
		return idAnpSeleccionado;
	}

	/**
	 * @param idAnpSeleccionado the idAnpSeleccionado to set
	 */
	public void setIdAnpSeleccionado(short idAnpSeleccionado) {
		this.idAnpSeleccionado = idAnpSeleccionado;
	}

	/**
	 * @return the archivoTemporal
	 */
	public ArchivosProyecto getArchivoTemporal() {
		return archivoTemporal;
	}

	/**
	 * @param archivoTemporal the archivoTemporal to set
	 */
	public void setArchivoTemporal(ArchivosProyecto archivoTemporal) {
		this.archivoTemporal = archivoTemporal;
	}

	/**
	 * @return the cargaBean
	 */
	public CargaBean getCargaBean() {
		return cargaBean;
	}

	/**
	 * @param cargaBean the cargaBean to set
	 */
	public void setCargaBean(CargaBean cargaBean) {
		this.cargaBean = cargaBean;
	}

	/**
	 * @return the archivoCargarEsDecreto
	 */
	public boolean isArchivoCargarEsDecreto() {
		return archivoCargarEsDecreto;
	}

	/**
	 * @param archivoCargarEsDecreto the archivoCargarEsDecreto to set
	 */
	public void setArchivoCargarEsDecreto(boolean archivoCargarEsDecreto) {
		this.archivoCargarEsDecreto = archivoCargarEsDecreto;
	}

	public String getMsj() {
		return msj;
	}

	public void setMsj(String msj) {
		this.msj = msj;
	}

	public boolean isBandera() {
		return bandera;
	}

	public void setBandera(boolean bandera) {
		this.bandera = bandera;
	}
	
	/**
	 * @return Numero secuencial (en la Vista) para un registro de cualquiera de las 3 tablas ANP
	 */
	public int getRowIndexANPEditar() {
		return rowIndexANPEditar;
	}

	/**
	 * Asigna valor a variable que guarda Numero secuencial de registro a editar en cualquiera de las 3 tablas ANP
	 * @param rowIndexANPEditar
	 */
	public void setRowIndexANPEditar(int rowIndexANPEditar) {
		this.rowIndexANPEditar = rowIndexANPEditar;
	}
	
}
