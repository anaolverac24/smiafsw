package mx.gob.semarnat.mia.view.capitulo3;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.gob.semarnat.mia.dao.CargaBean;
import mx.gob.semarnat.mia.model.ArchivosProyecto;
import mx.gob.semarnat.mia.model.DisposicionProyecto;
import mx.gob.semarnat.mia.model.DisposicionProyectoPK;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.view.Cap;

/**
 * @author edith hernandez
 *
 */
@ManagedBean
@ViewScoped
public class Cap3_OtrasDisp extends Cap { // implements Serializable 

	/**
	 * Constantes
	 */
	private static final long serialVersionUID = 1447776985188004953L;
	private static final Logger LOGGER = LoggerFactory.getLogger(Cap3_OtrasDisp.class);
	
	/**
	 * Lista de registros a mostrar en tabla de consulta
	 */
	private List<DisposicionProyecto> disposiciones = new ArrayList<DisposicionProyecto>();
	
	/**
	 * Lista de registros seleccionados para eliminar
	 */
	private List<DisposicionProyecto> regsSeleccionados = new ArrayList<DisposicionProyecto>();
	
	/**
	 * Registro seleccionado para edicion
	 */
	private DisposicionProyecto regSeleccionado = new DisposicionProyecto();
	
	/**
	 * Indica si los datos capturados son correctos o no
	 */
	private boolean datosValidos;
	
	/**
	 * Indica si se esta editando un registro o no
	 */
	private boolean seleccionEdicion;
	
	/**
	 * Indica si el proyecto requiere Otras disposiciones
	 */
	private boolean capDisposiciones = false;
	
	/**
	 * Titulo a mostrar en la ventana modal de agregar/editar registro
	 */
	private String titulo;
	
	/**
	 * Mensaje de aviso a mostrar en el cuerpo de ventanas modales
	 */
	private String aviso;
	
	/**
	 * Numero secuencial del registro "Otras disposiciones" a editar
	 */
	private int rowIndexDispEditar;
	
    // Vinculacion del registro Otras disposiciones a visualizar
    //private String vinculacion;

	
	/**
	 * Propiedades para la carga de archivos adjuntos
	 */
	private Short id = null;
    
    private ArchivosProyecto archivoTemporal;
	private ArchivosProyecto archivoEdicion;
    private CargaBean cargaBean;
    
    private List<ArchivosProyecto> archivosGeneral = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> archivosSeleccionados = new ArrayList<ArchivosProyecto>();
    private Proyecto proyecto = new Proyecto();
    
    private String msj;
	private boolean bandera;
	
	private long indexRow = 0;
	
	
	public Cap3_OtrasDisp(){
		
		try {
			this.proyecto = miaDao.proyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
		} catch (Exception e) {
			System.out.println("No encontrado");
		}
		
		buscarDisposiciones();
        
        this.cargaBean = new CargaBean();
		
		archivoTemporal = new ArchivosProyecto();
		archivoEdicion = new ArchivosProyecto();
		archivosGeneral = getMiaDao().getArchivosProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), (short) 3, (short) 8, (short) 0, (short) 0);
		
		// Requiere disposiciones
		if (proyecto.getProyOdisposicionAplica() != null && proyecto.getProyOdisposicionAplica().equals("S")) {
			capDisposiciones = true;
		}
	}
	
	
	/**
	 * Listener proyecto requeiere Otras disposiciones
	 */
	public void selDisposiciones() {
		if (proyecto.getProyOdisposicionAplica() != null) {
			if (proyecto.getProyOdisposicionAplica().equals("S")) {
				capDisposiciones = true;
			}
			if (proyecto.getProyOdisposicionAplica().equals("N")) {
				capDisposiciones = false;
			}
		}
	}

	
	/**
	 * Actualiza lista de disposiciones para el proyecto y serial específicos
	 */
	private void buscarDisposiciones() {
		String folioProy = getProyecto().getProyectoPK().getFolioProyecto();
		short serialProy = getProyecto().getProyectoPK().getSerialProyecto();
		
    	disposiciones = getMiaDao().getDisposiciones(folioProy, serialProy);
        LOGGER.debug("Se recuperaron registros de Otras Disposiciones: " + disposiciones.size());
        
        super.setProyectoInfo((short)3, folioProy, serialProy);
    }
	
	
	/**
     * Inicializa un objeto "Otras Disposiciones"
     */
    public void crearNuevoRegistro() {
    	this.regSeleccionado = new DisposicionProyecto();
    	this.seleccionEdicion = false;
    	this.titulo = "Otras disposiciones - agregar registro";
    	this.aviso = "Registro guardado exitosamente.";
    }
    
    
    /**
     * Valida los datos requeridos del registro "Otras disposiciones" en ventana modal
     */
    public void validarDatos() {
    	if (null == regSeleccionado.getDisposicionNombre() || null == regSeleccionado.getDisposicionVinculacion()) {
    		datosValidos = false;
    	} else {
    		datosValidos = true;
    		agregarRegistro();   		   		
    	}
    }

    
    /**
     * Guarda registro de Otra Disposicion en la base de datos
     */
    public void agregarRegistro() {
    	String folioProy = getProyecto().getProyectoPK().getFolioProyecto();
    	short serialProy = getProyecto().getProyectoPK().getSerialProyecto();
    	
    	if (!seleccionEdicion) {
    		short disposicionId = getMiaDao().getMaxDisposicion(folioProy, serialProy);
            regSeleccionado.setDisposicionProyectoPK(new DisposicionProyectoPK(folioProy, serialProy, disposicionId));
            regSeleccionado.setProyecto(getProyecto());
            System.out.println("Otras Disposiciones-Agregar. Folio=" + folioProy + ", Serial=" + serialProy + ", IdDisposicion=" + disposicionId);
    	}
        
        try {
        	getMiaDao().persist(regSeleccionado);
        	LOGGER.debug("Se guardó Nombre= {0}, Vinculacion= {1}" + regSeleccionado.getDisposicionNombre()
        			+ regSeleccionado.getDisposicionVinculacion());
        } catch (Exception e) {
            e.printStackTrace();
        }

        buscarDisposiciones();
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("modal.hide();modalAlerta.show()");
    }
    
    
    /**
     * Actualiza el objeto Disposicion seleccionado desde la vista
     */
    public void mostrarRegSeleccionado() {
    	String folioProy = getProyecto().getProyectoPK().getFolioProyecto();
		short serialProy = getProyecto().getProyectoPK().getSerialProyecto();
		short idDisposicion = 0;
		
    	Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		
		idDisposicion = Short.parseShort(params.get("id").toString());
		LOGGER.debug("Otra Disposicion a editar. Folio={0}, Serial={1}, IdDisposicion={2}"+ folioProy, serialProy, idDisposicion);
		this.regSeleccionado = getMiaDao().getDisposicion(folioProy, serialProy, idDisposicion);
		this.seleccionEdicion = true;
		
		// Numero secuencial a mostrar en "Editar registro #"
		rowIndexDispEditar = Integer.parseInt(params.get("prowindexDisp").toString());
		
		this.titulo = "Editar otras disposiciones: # " + rowIndexDispEditar;
		this.aviso = "Registro actualizado exitosamente.";
    }
    
    
    /**
     * Elimina los registros seleccionados
     */
    public void eliminarRegistros() {
    	
    	for (DisposicionProyecto disposicion : regsSeleccionados) {
    		String folioProy = getProyecto().getProyectoPK().getFolioProyecto();
    		short serialProy = getProyecto().getProyectoPK().getSerialProyecto();
    		short regId = disposicion.getDisposicionProyectoPK().getDisposicionProyId();
    		
    		try {
    			LOGGER.debug("Por eliminar: Folio={0}, Serial={1}, IdDisposicion={2}" + folioProy, serialProy, regId);
                getMiaDao().eliminarOtraDisposicion(folioProy, serialProy, regId);
                System.out.println("Se elimino: folio=" + folioProy + ", serial=" + serialProy + ", Id Disposicion=" + regId);
                		
            } catch (Exception e) {
                e.printStackTrace();
            }
		}
    	
    	this.aviso = "Registro(s) eliminado(s) exitosamente.";
    	buscarDisposiciones();
    }

	/**
	 * Inicializa un nuevo archivo para ser cargado
	 */
	public void iniciarArchivo() {
		archivoTemporal = new ArchivosProyecto();		
		RequestContext.getCurrentInstance().reset(":archivosDisposicion:pnlArchivo");
		archivoTemporal = new ArchivosProyecto(getProyecto().getProyectoPK().getFolioProyecto(),
				getProyecto().getProyectoPK().getSerialProyecto(), (short) 3, (short) 8, (short) 0, (short) 0);
		
		cargaBean = new CargaBean();
		msj = " ";
	}

	/**
	 * Guarda el archivo en la BD y el Servidor
	 */
	public void guardarArchivo() {
//		RequestContext.getCurrentInstance().reset(":archivosDisposicion:pnlArchivo");
//		archivoTemporal.setFolioSerial(getProyecto().getProyectoPK().getFolioProyecto());
//		archivoTemporal.setSerialProyecto(getProyecto().getProyectoPK().getSerialProyecto());
//		archivoTemporal.setCapituloId((short) 3);
//		archivoTemporal.setSubCapituloId((short) 8);
//		archivoTemporal.setSeccionId((short) 0);
//		archivoTemporal.setApartadoId((short) 0);
		id = getMiaDao().getMaxArchivoEtapaProyecto(getProyecto().getProyectoPK().getFolioProyecto(),
				getProyecto().getProyectoPK().getSerialProyecto(), archivoTemporal.getCapituloId(),
				archivoTemporal.getSubCapituloId(), archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());
		System.out.println("Registro: #" + id);
		archivoTemporal.setId(id);
		try {
			bandera = cargaBean.GuardaAnexos(archivoTemporal);
		if (bandera == true) {
		
			System.out.println("Fin del guardado de Archivos");
			RequestContext context = RequestContext.getCurrentInstance();
			System.out.println("Cerrando Modal Agregar y abriendo Modal Aviso");
			context.execute("modalSubirArchivo.hide();modalAlertArchivo.show()");
			archivosGeneral.add(archivoTemporal);	
			msj = "";
			cargaBean = new CargaBean();
		}else {
			msj = "Se requiere cargar un archivo";
			
		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void eliminaArchivos() throws Exception {
		System.out.println("Eliminación afirmativa");		
		for (ArchivosProyecto archivosSeleccionado : archivosSeleccionados) {
//			archivosGeneral.remove(archivosSeleccionado);			
			miaDao.eliminarArchivosEtapas(getProyecto().getProyectoPK().getFolioProyecto(),
					getProyecto().getProyectoPK().getSerialProyecto(), (short) 3, (short) 8, (short) 0, (short) 0, archivosSeleccionado.getId(), archivosSeleccionado.getSeqId());
		}
		
		archivosGeneral = getMiaDao().getArchivosProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), (short) 3, (short) 8, (short) 0, (short) 0);

	}
	
	public void tieneArchivosSeleccionados() {
		System.out.println(archivosSeleccionados.size() + " seleccionados");
		if (!archivosSeleccionados.isEmpty()) {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("eliminarArchivo.show()");
		}
	}
	
	public void consultarArchivo() {
		System.out.println("Consultando");
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idArchivo = params.get("id").toString();
		id = Integer.parseInt(idArchivo);
		int rowIndex = 0;
		String pRowIndex = params.get("pRowIndex").toString();
		rowIndex = Integer.parseInt(pRowIndex);
		
		
		System.out.println("Actualizando archivo: " + id);
		archivoEdicion = new ArchivosProyecto();
		archivoEdicion = miaDao.getArchivoEtapa(getProyecto().getProyectoPK().getFolioProyecto(),
				getProyecto().getProyectoPK().getSerialProyecto(), (short) 3, (short) 8, (short) 0,(short) 0, (short) id);
		System.out.println("Fin de la Consulta");
		indexRow = rowIndex;
	}
	
	public void editarArchivo() {
		ArchivosProyecto ar = new ArchivosProyecto();
		ar = archivoEdicion;
		try {
			miaDao.merge(ar);
			
			archivosGeneral = getMiaDao().getArchivosProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), (short) 3, (short) 8, (short) 0, (short) 0);

			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditarArchivo.hide();modalAlertEdArchivo.show()");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
    /**
     * Guarda avance del proyecto
     */
    public void guardarAvance() {
    	RequestContext reqcontEnv = RequestContext.getCurrentInstance();
    	
    	// Guardar avance del proyecto: Otras Disposiciones
		try {
			if (null != proyecto.getProyOdisposicionAplica()) {
				if (disposiciones.size() >= 1 && proyecto.getProyOdisposicionAplica().equals("S")) {
					try {
						miaDao.guardarAvance(proyecto, new Short("1"), "38");
						LOGGER.debug("Otras Disposiciones: Después de guardar avance.");
					} catch (Exception e) {
						LOGGER.debug("[ERR] Error al cargar avance en Otras disposiciones: " + e.getCause().getMessage());
					}
					reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
				} else if (proyecto.getProyOdisposicionAplica().equals("N")) {
					try {
						miaDao.guardarAvance(proyecto, new Short("1"), "38");
						LOGGER.debug("Otras Disposiciones: Después de guardar avance.");
					} catch (Exception e) {
						LOGGER.debug("[ERR] Error al cargar avance en Otras Disposiciones: " + e.getCause().getMessage());
					}
					reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
				} else {
					reqcontEnv.execute("mensajes('No se ha podido guardar la información, Intente más tarde.', 'danger')");
				}
			} else {
				reqcontEnv.execute("mensajes('No se ha podido guardar la información, Intente más tarde.', 'danger')");
			}
		} catch (NullPointerException ex) {
			System.out.println("Aun no ha sido capturado el apartado de otras disposiciones");
			reqcontEnv.execute("mensajes('No se ha podido guardar la información, Intente más tarde.', 'danger')");
		}
		
		if (Integer.parseInt(miaDao.avanceProyecto(proyecto)) == 99) {
			reqcontEnv.execute("mensajes('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.', 'success')");
        }
		
		// Guardado de componentes en información adicional - eescalona
        super.guardarComentarios();  
    }
    
    
    
    /**
	 * @return Lista de registros a mostrar en tabla de consulta
	 */
	public List<DisposicionProyecto> getDisposiciones() {
		return disposiciones;
	}

	/**
	 * @param disposiciones Lista de objetos del tipo DisposicionProyecto a asignar
	 */
	public void setDisposiciones(List<DisposicionProyecto> disposiciones) {
		this.disposiciones = disposiciones;
	}
	
	/**
	 * @return Lista de registros seleccionados para eliminacion
	 */
	public List<DisposicionProyecto> getRegsSeleccionados() {
		return regsSeleccionados;
	}

	/**
	 * @param regsSeleccionados Lista de objetos de tipo DisposicionProyecto a eliminar
	 */
	public void setRegsSeleccionados(List<DisposicionProyecto> regsSeleccionados) {
		this.regsSeleccionados = regsSeleccionados;
	}
	
	/**
	 * @return Registro seleccionado para edicion
	 */
	public DisposicionProyecto getRegSeleccionado() {
		return regSeleccionado;
	}

	/**
	 * @param regSeleccionado Objeto seleccionado para eliminacion
	 */
	public void setRegSeleccionado(DisposicionProyecto regSeleccionado) {
		this.regSeleccionado = regSeleccionado;
	}
	
	/**
	 * @return Indica si los datos capturados son correctos
	 */
	public boolean isDatosValidos() {
		return datosValidos;
	}

	/**
	 * @param datosValidos Valor de datos capturados validados
	 */
	public void setDatosValidos(boolean datosValidos) {
		this.datosValidos = datosValidos;
	}  
	
	/**
	 * @return Indica si el registro se va a editar
	 */
	public boolean isSeleccionEdicion() {
		return seleccionEdicion;
	}

	/**
	 * @param seleccionEdicion Valor que indica si el registro se va a editar
	 */
	public void setSeleccionEdicion(boolean seleccionEdicion) {
		this.seleccionEdicion = seleccionEdicion;
	}
	
	/**
	 * @return Titulo a mostrar en ventana modal
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * @param titulo Valor a colocar en titulo
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
    /**
     * @return Aviso a mostrar en cuerpo de ventana modal
     */
    public String getAviso() {
		return aviso;
	}

	/**
	 * @param aviso Valor a colocar en aviso
	 */
	public void setAviso(String aviso) {
		this.aviso = aviso;
	}
	
	/**
	 * @return Indica si el proyecto requiere Otras disposiciones
	 */
	public boolean isCapDisposiciones() {
		return capDisposiciones;
	}

	/**
	 * @param capDisposiciones Valor a colocar que indica si el proyecto requiere Otras disposiciones
	 */
	public void setCapDisposiciones(boolean capDisposiciones) {
		this.capDisposiciones = capDisposiciones;
	}
	
	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla Otras disposiciones
	 */
	public int getRowIndexDispEditar() {
		return rowIndexDispEditar;
	}

	/**
	 * Asigna valor a variable que guarda Numero secuencial de registro a editar en tabla Otras disposiciones
	 * @param rowIndexDispEditar
	 */
	public void setRowIndexDispEditar(int rowIndexDispEditar) {
		this.rowIndexDispEditar = rowIndexDispEditar;
	}
	
	/*
	public String getVinculacion() {
		return vinculacion;
	}

	public void setVinculacion(String vinculacion) {
		this.vinculacion = vinculacion;
	}*/
	
	/**
     * Guarda valor de Vinculacion del registro Otras disposiciones a editar
     * @param vinculacion
     *//*
    public void mostrarVinculacion(String vinculacion) {
    	LOGGER.debug("Otras disposiciones. Mostrar vinculación: " + vinculacion);
		this.vinculacion = vinculacion;
	}*/
	
	
	
	/**
	 * @return the archivoTemporal
	 */
	public ArchivosProyecto getArchivoTemporal() {
		return archivoTemporal;
	}

	/**
	 * @param archivoTemporal the archivoTemporal to set
	 */
	public void setArchivoTemporal(ArchivosProyecto archivoTemporal) {
		this.archivoTemporal = archivoTemporal;
	}

	/**
	 * @return the archivoEdicion
	 */
	public ArchivosProyecto getArchivoEdicion() {
		return archivoEdicion;
	}

	/**
	 * @param archivoEdicion the archivoEdicion to set
	 */
	public void setArchivoEdicion(ArchivosProyecto archivoEdicion) {
		this.archivoEdicion = archivoEdicion;
	}

	/**
	 * @return the cargaBean
	 */
	public CargaBean getCargaBean() {
		return cargaBean;
	}

	/**
	 * @param cargaBean the cargaBean to set
	 */
	public void setCargaBean(CargaBean cargaBean) {
		this.cargaBean = cargaBean;
	}

	/**
	 * @return the archivosGeneral
	 */
	public List<ArchivosProyecto> getArchivosGeneral() {
		return archivosGeneral;
	}

	/**
	 * @param archivosGeneral the archivosGeneral to set
	 */
	public void setArchivosGeneral(List<ArchivosProyecto> archivosGeneral) {
		this.archivosGeneral = archivosGeneral;
	}

	/**
	 * @return the archivosSeleccionados
	 */
	public List<ArchivosProyecto> getArchivosSeleccionados() {
		return archivosSeleccionados;
	}

	/**
	 * @param archivosSeleccionados the archivosSeleccionados to set
	 */
	public void setArchivosSeleccionados(List<ArchivosProyecto> archivosSeleccionados) {
		this.archivosSeleccionados = archivosSeleccionados;
	}

	public String getMsj() {
		return msj;
	}

	public void setMsj(String msj) {
		this.msj = msj;
	}

	public boolean isBandera() {
		return bandera;
	}

	public void setBandera(boolean bandera) {
		this.bandera = bandera;
	}

	public long getIndexRow() {
		return indexRow;
	}

	public void setIndexRow(long indexRow) {
		this.indexRow = indexRow;
	}
	
}