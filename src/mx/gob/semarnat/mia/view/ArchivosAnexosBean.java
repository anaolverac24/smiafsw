/**
 * 
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import mx.gob.semarnat.mia.dao.ArchivosAnexosDAO;
import mx.gob.semarnat.mia.dao.CargaBean;
import mx.gob.semarnat.mia.dao.MiaDao;
import mx.gob.semarnat.mia.model.ArchivosProyecto;
import mx.gob.semarnat.mia.model.Proyecto;

/**
 * @author dpaniagua.
 *
 */
@ManagedBean
@ViewScoped
public class ArchivosAnexosBean implements Serializable {
	/**
	 * Clave del tramite del proyecto.
	 */
	private Integer claveTramite;
	/**
	 * Proyecto del promovente.
	 */
	private Proyecto proyecto;
	/**
	 * Permite ocupar las transacciones de MiaDAO.
	 */
    private MiaDao miaDao = new MiaDao();
    /**
     * Permite obtener el valor maximo de la etapa del proyecto.
     */
	private Short id;
	/**
	 * Archivo anexo del proyecto.
	 */
	private ArchivosProyecto archivoTemporal;
	/**
	 * Archivo anexo del proyecto para programas generales.
	 */
	private ArchivosProyecto archivoTemporalPrograma;
	/**
	 * Permite realizar la carga del archivo del anexo.
	 */
	private CargaBean cargaBean;
	/**
	 * Lista que permite mostrar los Archivos del proyecto.
	 */
	private List<ArchivosProyecto> archivosGeneral = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista que permite mostrar los Archivos del proyecto para el programa general.
	 */
	private List<ArchivosProyecto> archivosGeneralPrograma = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de archivos seleccionados de la tabla.
	 */
	private List<ArchivosProyecto> listaArchivosSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de archivos para programa general de trabajo.
	 */
	private List<ArchivosProyecto> listaArchivosProgramaSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Archivo anexo a editar del capitulo.
	 */
	private ArchivosProyecto archivoEdicion;
	/**
	 * Folio del Proyecto
	 */
	private String folioProyecto;
	/**
	 * Serial del Proyecto.
	 */
	private short serialProyecto;	
	
	private ArchivosAnexosDAO archivosAnexosDAO;
	/**
	 * Capitulo en el que se encuentra la pantalla actual.
	 */
	private String capitulo;
	/**
	 * Subcapitulo del capitulo de la pantalla actual.
	 */
	private String subcapitulo;
	/**
	 * Seccion del subcapitulo de la pantalla actual.
	 */
	private String seccion;
	/**
	 * Apartado de la seccion de la pantalla actual.
	 */
	private String apartardo;
	/**
	 * id que construye el id del grid dinamico.
	 */
	private long indexRow = 0;
//	private String idGridAnexos;
	
	/**
	 * Lista para el resumen de los archivos contenidos en todo el proyecto
	 */
	private List listaresumen =new ArrayList<Object>();
	
	private String msj;
	private boolean bandera;


	@PostConstruct
	public void init() {
		archivoEdicion = new ArchivosProyecto();
		
		archivoTemporal = new ArchivosProyecto();
		archivoTemporalPrograma = new ArchivosProyecto();
		
		archivosGeneral = new ArrayList<ArchivosProyecto>();
		archivosGeneralPrograma = new ArrayList<ArchivosProyecto>();
		cargaBean = new CargaBean();
		proyecto = new Proyecto();
		
        FacesContext context = FacesContext.getCurrentInstance();
        this.folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        this.serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
        
        context.getExternalContext().getSessionMap().get("userFolioProy");        
        claveTramite = (Integer) context.getExternalContext().getSessionMap().get("claveTramite");
        //Cargar Proyecto
        try {
            this.proyecto = miaDao.proyecto(this.folioProyecto, this.serialProyecto);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }

        if (proyecto == null) {
            this.proyecto = new Proyecto(this.folioProyecto, this.serialProyecto);
        } 
        /**
         * Llenamos la lista desde la base de datos
         */
        listaresumen = miaDao.listaResumen(this.folioProyecto, this.serialProyecto);
        
	}
	
	/**
	 * Inicializa un nuevo archivo para ser cargado
	 */
	public void iniciarArchivo() {
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		archivoTemporal= new ArchivosProyecto();
		String cadenaCapitulo = params.get("capitulo").toString();
		String cadenaSubCapitulo = params.get("subcapitulo").toString();
		String cadenaSeccion = params.get("seccion").toString();
		String cadenaApartado = params.get("apartado").toString();
		
		this.capitulo = cadenaCapitulo;
		this.subcapitulo = cadenaSubCapitulo;
		this.seccion = cadenaSeccion;
		this.apartardo = cadenaApartado;
	
		short capitulo = Short.parseShort(cadenaCapitulo);
		short subCapitulo = Short.parseShort(cadenaSubCapitulo);
		short seccion = Short.parseShort(cadenaSeccion);
		short apartado = Short.parseShort(cadenaApartado);

		cargaBean = new CargaBean();
		
		archivoTemporal.setNombre("");
		archivoTemporal.setDescripcion("");
		archivoTemporal.setFolioSerial(this.folioProyecto);
		archivoTemporal.setSerialProyecto(this.serialProyecto);
		archivoTemporal.setCapituloId(capitulo);
		archivoTemporal.setSubCapituloId(subCapitulo);
		archivoTemporal.setSeccionId(seccion);
		archivoTemporal.setApartadoId(apartado);

		
		msj = "";


	}
	
	/**
	 * Inicializa un nuevo archivo para ser cargado para el programa general del trabajo.
	 */
	public void iniciarArchivoPrograma() {
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

		String cadenaCapitulo = params.get("capitulo").toString();
		String cadenaSubCapitulo = params.get("subcapitulo").toString();
		String cadenaSeccion = params.get("seccion").toString();
		String cadenaApartado = params.get("apartado").toString();
		
		this.capitulo = cadenaCapitulo;
		this.subcapitulo = cadenaSubCapitulo;
		this.seccion = cadenaSeccion;
		this.apartardo = cadenaApartado;
	
		short capitulo = Short.parseShort(cadenaCapitulo);
		short subCapitulo = Short.parseShort(cadenaSubCapitulo);
		short seccion = Short.parseShort(cadenaSeccion);
		short apartado = Short.parseShort(cadenaApartado);
		
		cargaBean = new CargaBean();
		
		archivoTemporalPrograma.setNombre("");
		archivoTemporalPrograma.setDescripcion("");
		archivoTemporalPrograma.setFolioSerial(this.folioProyecto);
		archivoTemporalPrograma.setSerialProyecto(this.serialProyecto);
		archivoTemporalPrograma.setCapituloId(capitulo);
		archivoTemporalPrograma.setSubCapituloId(subCapitulo);
		archivoTemporalPrograma.setSeccionId(seccion);
		archivoTemporalPrograma.setApartadoId(apartado);
msj = "";

	}	
	
	/**
	 * Guarda el archivo en la BD y el Servidor
	 */
	public void guardarArchivo() {
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		
		String capitulo = params.get("capitulo").toString();
		String subCapitulo = params.get("subcapitulo").toString();
		String seccion = params.get("seccion").toString();
		String apartado = params.get("apartado").toString();
//		
//		archivoTemporal = new ArchivosProyecto(this.folioProyecto, this.serialProyecto, 
//				Short.parseShort(capitulo), Short.parseShort(subCapitulo), Short.parseShort(seccion), Short.parseShort(apartado));
		
		id = miaDao.getMaxArchivoEtapaProyecto(this.folioProyecto, this.serialProyecto, 
				archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(), archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());

		archivoTemporal.setId(id);
		try {
			bandera = cargaBean.GuardaAnexos(archivoTemporal);
			if (bandera == true) {
			
			
			cargaBean = new CargaBean();
			this.archivoTemporal = new ArchivosProyecto();
			
			this.archivoTemporal.setCapituloId(Short.parseShort(this.capitulo));
			this.archivoTemporal.setSubCapituloId(Short.parseShort(this.subcapitulo));
			this.archivoTemporal.setSeccionId(Short.parseShort(this.seccion));
			this.archivoTemporal.setApartadoId(Short.parseShort(this.apartardo));
			
			msj = "";
			
			cargaBean = new CargaBean();

			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalSubirArchivo.hide();modalAlertArchivo.show()");
			archivosGeneral = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capitulo), 
					Short.parseShort(subCapitulo), Short.parseShort(seccion), Short.parseShort(apartado));
			}else {
				msj = "Se requiere cargar un archivo";
				
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Hubo un error en el contenido del archivo o al guardar", null));
			e.printStackTrace();
		}
	}
	
	/**
	 * Guarda el archivo en la BD y el Servidor
	 */
	public void guardarArchivoPrograma() {
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		
		String capitulo = params.get("capitulo").toString();
		String subCapitulo = params.get("subcapitulo").toString();
		String seccion = params.get("seccion").toString();
		String apartado = params.get("apartado").toString();
		
		id = miaDao.getMaxArchivoEtapaProyecto(this.folioProyecto, this.serialProyecto, 
				archivoTemporalPrograma.getCapituloId(), archivoTemporalPrograma.getSubCapituloId(), 
				archivoTemporalPrograma.getSeccionId(), archivoTemporalPrograma.getApartadoId());

		archivoTemporalPrograma.setId(id);
		try {
			bandera = cargaBean.GuardaAnexos(archivoTemporalPrograma);





			



			if (bandera == true) {
			
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("modalSubirArchivoPrograma.hide();modalAlertArchivoPrograma.show()");
				archivosGeneralPrograma = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capitulo), 
						Short.parseShort(subCapitulo), Short.parseShort(seccion), Short.parseShort(apartado));
				
				cargaBean = new CargaBean();
	
				this.archivoTemporalPrograma = new ArchivosProyecto();
				
				this.archivoTemporalPrograma.setCapituloId(Short.parseShort(capitulo));
				this.archivoTemporalPrograma.setSubCapituloId(Short.parseShort(subCapitulo));
				this.archivoTemporalPrograma.setSeccionId(Short.parseShort(seccion));
				this.archivoTemporalPrograma.setApartadoId(Short.parseShort(apartardo));
			
				msj = "";	
				
			} else {
				msj = "Se requiere cargar un archivo";	
			}
			
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Hubo un error en el contenido del archivo o al guardar", null));
			e.printStackTrace();
		}
	}	
	/**
	 * Permite consultar el archivo anexo a editar.
	 */
	public void consultarArchivo() {
		
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		int rowIndex = 0;
		String idArchivo = params.get("id").toString();
		String pRowIndex = params.get("pRowIndex").toString();
		id = Integer.parseInt(idArchivo);
		rowIndex = Integer.parseInt(pRowIndex);

		archivoEdicion = new ArchivosProyecto();
		archivoEdicion = miaDao.getArchivoEtapa(this.folioProyecto, this.serialProyecto, archivoTemporal.getCapituloId(), 
				archivoTemporal.getSubCapituloId(), archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId(), (short) id);
		indexRow = rowIndex;

	}
	/**
	 * Permite consultar el archivo anexo del programa general del trabajo a editar.
	 */
	public void consultarArchivoPrograma() {
		
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idArchivo = params.get("id").toString();
		id = Integer.parseInt(idArchivo);

		archivoEdicion = new ArchivosProyecto();
		archivoEdicion = miaDao.getArchivoEtapa(this.folioProyecto, this.serialProyecto, archivoTemporalPrograma.getCapituloId(), 
				archivoTemporalPrograma.getSubCapituloId(), archivoTemporalPrograma.getSeccionId(), archivoTemporalPrograma.getApartadoId(), (short) id);

	}
	
	/**
	 * Permite realizar la carga de los archivos anexos para las pantallas individuales.
	 * @param capitulo
	 * @param subCapitulo
	 * @param seccion
	 * @param apartado
	 */
	public void consultarDesgloseCapitulo(String capitulo,String subCapitulo,String seccion,String apartado) {	
		this.capitulo = capitulo;
		this.subcapitulo = subCapitulo;
		this.seccion = seccion;
		this.apartardo = apartado;
		
		archivoTemporal = new ArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capitulo), 
				Short.parseShort(subCapitulo), Short.parseShort(seccion), Short.parseShort(apartado));
		archivosAnexosDAO = new ArchivosAnexosDAO();
		archivosGeneral = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capitulo), 
				Short.parseShort(subCapitulo), Short.parseShort(seccion), Short.parseShort(apartado));

	}
	/**
	 * Permite realizar la carga de los archivos anexos para el programa general del proyecto.
	 * @param capitulo
	 * @param subCapitulo
	 * @param seccion
	 * @param apartado
	 */
	public void consultarDesgloseCapituloPrograma(String capitulo,String subCapitulo,String seccion,String apartado) {	
//		this.capitulo = capitulo;
//		this.subcapitulo = subCapitulo;
//		this.seccion = seccion;
//		this.apartardo = apartado;
		
		if (this.proyecto.getNsub() != null) {
			if (this.proyecto.getNsub() == 6) {
				seccion = "4";
			} else {
				seccion = "3";
			}
		}
		
		archivoTemporalPrograma = new ArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capitulo), 
				Short.parseShort(subCapitulo), Short.parseShort(seccion), Short.parseShort(apartado));
		archivosAnexosDAO = new ArchivosAnexosDAO();
		archivosGeneralPrograma = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capitulo), 
				Short.parseShort(subCapitulo), Short.parseShort(seccion), Short.parseShort(apartado));

	}	
	/**
	 * Permite realizar la edicion del archivo de anexo seleccionado.
	 */
	public void editarArchivo() {
		ArchivosProyecto archivoProyectoEditado = new ArchivosProyecto();
		archivoProyectoEditado = archivoEdicion;
		try {
			archivosAnexosDAO.editarArchivoProyecto(archivoProyectoEditado);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditarArchivo.hide();modalAlertEdArchivo.show()");

			archivosGeneral = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(this.capitulo), 
					Short.parseShort(this.subcapitulo), Short.parseShort(this.seccion), Short.parseShort(this.apartardo));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Permite realizar la edicion del archivo de anexo del programa general del trabajo seleccionado.
	 */
	public void editarArchivoPrograma() {
		ArchivosProyecto archivoProyectoEditado = new ArchivosProyecto();
		archivoProyectoEditado = archivoEdicion;
		try {
			archivosAnexosDAO.editarArchivoProyecto(archivoProyectoEditado);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditarArchivoPrograma.hide();modalAlertEdArchivoPrograma.show()");

			archivosGeneralPrograma = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, archivoProyectoEditado.getCapituloId(), 
					archivoProyectoEditado.getSubCapituloId(), archivoProyectoEditado.getSeccionId(), archivoProyectoEditado.getApartadoId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
	/**
	 * Permite eliminar el o los archivos anexos seleccionados.
	 */
	public void eliminarArchivos() {
		for (ArchivosProyecto archivosSeleccionado : listaArchivosSeleccionados) {
			try {
				archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(this.capitulo), 
						Short.parseShort(this.subcapitulo), Short.parseShort(this.seccion), Short.parseShort(this.apartardo), archivosSeleccionado.getId());
				
				archivosGeneral = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(this.capitulo), 
						Short.parseShort(this.subcapitulo), Short.parseShort(this.seccion), Short.parseShort(this.apartardo));
				
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("dlgEliminarArchivoAnexo.hide();modalAlertEArchivo.show()");
				
				listaArchivosSeleccionados = new ArrayList<ArchivosProyecto>();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Permite eliminar el o los archivos anexos del programa del trabajo seleccionado seleccionados.
	 */
	public void eliminarArchivosPrograma() {
		for (ArchivosProyecto archivosSeleccionado : listaArchivosProgramaSeleccionados) {
			try {
				archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosSeleccionado.getCapituloId(), 
						archivosSeleccionado.getSubCapituloId(), archivosSeleccionado.getSeccionId(), archivosSeleccionado.getApartadoId(), archivosSeleccionado.getId());
				
				archivosGeneralPrograma = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, archivosSeleccionado.getCapituloId(), 
						archivosSeleccionado.getSubCapituloId(), archivosSeleccionado.getSeccionId(), archivosSeleccionado.getApartadoId());
				
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("dlgEliminarArchivoProgramaAnexo.hide();modalAlertEArchivoPrograma.show()");				

				listaArchivosProgramaSeleccionados = new ArrayList<ArchivosProyecto>();

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}	
	
	/**
	 * Permite abrir el dialog para eliminar archivos.
	 */
	public void abrirModalEliminarArchivos() {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("dlgEliminarArchivoAnexo.show()");
	}
	/**
	 * Permite abrir el dialog para eliminar archivos.
	 */
	public void abrirModalEliminarArchivosPrograma() {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("dlgEliminarArchivoProgramaAnexo.show()");
	}	
	
	/**
	 * @return the claveTramite
	 */
	public Integer getClaveTramite() {
		return claveTramite;
	}

	/**
	 * @param claveTramite the claveTramite to set
	 */
	public void setClaveTramite(Integer claveTramite) {
		this.claveTramite = claveTramite;
	}

	/**
	 * @return the proyecto
	 */
	public Proyecto getProyecto() {
		return proyecto;
	}

	/**
	 * @param proyecto the proyecto to set
	 */
	public void setProyecto(Proyecto proyecto) {
		this.proyecto = proyecto;
	}

	/**
	 * @return the id
	 */
	public Short getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Short id) {
		this.id = id;
	}

	/**
	 * @return the archivoTemporal
	 */
	public ArchivosProyecto getArchivoTemporal() {
		return archivoTemporal;
	}

	/**
	 * @param archivoTemporal the archivoTemporal to set
	 */
	public void setArchivoTemporal(ArchivosProyecto archivoTemporal) {
		this.archivoTemporal = archivoTemporal;
	}

	/**
	 * @return the cargaBean
	 */
	public CargaBean getCargaBean() {
		return cargaBean;
	}

	/**
	 * @param cargaBean the cargaBean to set
	 */
	public void setCargaBean(CargaBean cargaBean) {
		this.cargaBean = cargaBean;
	}

	/**
	 * @return the archivosGeneral
	 */
	public List<ArchivosProyecto> getArchivosGeneral() {
		return archivosGeneral;
	}

	/**
	 * @param archivosGeneral the archivosGeneral to set
	 */
	public void setArchivosGeneral(List<ArchivosProyecto> archivosGeneral) {
		this.archivosGeneral = archivosGeneral;
	}

	/**
	 * @return the listaArchivosSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosSeleccionados() {
		return listaArchivosSeleccionados;
	}

	/**
	 * @param listaArchivosSeleccionados the listaArchivosSeleccionados to set
	 */
	public void setListaArchivosSeleccionados(List<ArchivosProyecto> listaArchivosSeleccionados) {
		this.listaArchivosSeleccionados = listaArchivosSeleccionados;
	}

	/**
	 * @return the archivoEdicion
	 */
	public ArchivosProyecto getArchivoEdicion() {
		return archivoEdicion;
	}

	/**
	 * @param archivoEdicion the archivoEdicion to set
	 */
	public void setArchivoEdicion(ArchivosProyecto archivoEdicion) {
		this.archivoEdicion = archivoEdicion;
	}

	/**
	 * @return the folioProyecto
	 */
	public String getFolioProyecto() {
		return folioProyecto;
	}

	/**
	 * @param folioProyecto the folioProyecto to set
	 */
	public void setFolioProyecto(String folioProyecto) {
		this.folioProyecto = folioProyecto;
	}

	/**
	 * @return the serialProyecto
	 */
	public short getSerialProyecto() {
		return serialProyecto;
	}

	/**
	 * @param serialProyecto the serialProyecto to set
	 */
	public void setSerialProyecto(short serialProyecto) {
		this.serialProyecto = serialProyecto;
	}

	/**
	 * @return the capitulo
	 */
	public String getCapitulo() {
		return capitulo;
	}

	/**
	 * @param capitulo the capitulo to set
	 */
	public void setCapitulo(String capitulo) {
		this.capitulo = capitulo;
	}

	/**
	 * @return the subcapitulo
	 */
	public String getSubcapitulo() {
		return subcapitulo;
	}

	/**
	 * @param subcapitulo the subcapitulo to set
	 */
	public void setSubcapitulo(String subcapitulo) {
		this.subcapitulo = subcapitulo;
	}

	/**
	 * @return the seccion
	 */
	public String getSeccion() {
		return seccion;
	}

	/**
	 * @param seccion the seccion to set
	 */
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	/**
	 * @return the apartardo
	 */
	public String getApartardo() {
		return apartardo;
	}

	/**
	 * @param apartardo the apartardo to set
	 */
	public void setApartardo(String apartardo) {
		this.apartardo = apartardo;
	}

	/**
	 * @return the listaArchivosProgramaSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosProgramaSeleccionados() {
		return listaArchivosProgramaSeleccionados;
	}

	/**
	 * @param listaArchivosProgramaSeleccionados the listaArchivosProgramaSeleccionados to set
	 */
	public void setListaArchivosProgramaSeleccionados(List<ArchivosProyecto> listaArchivosProgramaSeleccionados) {
		this.listaArchivosProgramaSeleccionados = listaArchivosProgramaSeleccionados;
	}

	/**
	 * @return the archivoTemporalPrograma
	 */
	public ArchivosProyecto getArchivoTemporalPrograma() {
		return archivoTemporalPrograma;
	}

	/**
	 * @param archivoTemporalPrograma the archivoTemporalPrograma to set
	 */
	public void setArchivoTemporalPrograma(ArchivosProyecto archivoTemporalPrograma) {
		this.archivoTemporalPrograma = archivoTemporalPrograma;
	}

	/**
	 * @return the archivosGeneralPrograma
	 */
	public List<ArchivosProyecto> getArchivosGeneralPrograma() {
		return archivosGeneralPrograma;
	}

	/**
	 * @param archivosGeneralPrograma the archivosGeneralPrograma to set
	 */
	public void setArchivosGeneralPrograma(List<ArchivosProyecto> archivosGeneralPrograma) {
		this.archivosGeneralPrograma = archivosGeneralPrograma;
	}

	public String getMsj() {
		return msj;
	}

	public void setMsj(String msj) {
		this.msj = msj;
	}

	public boolean isBandera() {
		return bandera;
	}

	public void setBandera(boolean bandera) {
		this.bandera = bandera;
	}

	public long getIndexRow() {
		return indexRow;
	}

	public void setIndexRow(long indexRow) {
		this.indexRow = indexRow;
	}
	/**
	 * @return the idGridAnexos
	 */
//	public String getIdGridAnexos() {
//		return idGridAnexos = this.capitulo + "" + this.subcapitulo + "" + this.seccion + "" +this.apartardo ;
//	}
	
	
	public List getListaresumen() {
		return listaresumen;
	}

	public void setListaresumen(List listaresumen) {
		this.listaresumen = listaresumen;
	}
	
}
