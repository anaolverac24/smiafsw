/**
 * 
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;

import mx.gob.semarnat.mia.Enum.ArchivosAnexosEnum;
import mx.gob.semarnat.mia.dao.ArchivosAnexosDAO;
import mx.gob.semarnat.mia.dao.CargaBean;
import mx.gob.semarnat.mia.dao.MiaDao;
import mx.gob.semarnat.mia.model.ArchivosProyecto;
import mx.gob.semarnat.mia.model.Proyecto;

/**
 * @author dpaniagua.
 *
 */
@ManagedBean
@ViewScoped
public class ArchivosAnexosCap4Bean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3250565180406880931L;
	/**
	 * Clave del tramite del proyecto.
	 */
	private Integer claveTramite;
	/**
	 * Proyecto del promovente.
	 */
	private Proyecto proyecto;
	/**
	 * Permite ocupar las transacciones de MiaDAO.
	 */
    private MiaDao miaDao = new MiaDao();
    /**
     * Permite obtener el valor maximo de la etapa del proyecto.
     */
	private Short id;
	/**
	 * Archivo anexo del proyecto.
	 */
	private ArchivosProyecto archivoTemporal;
	/**
	 * Permite realizar la carga del archivo del anexo.
	 */
	private CargaBean cargaBean;
	/**
	 * Lista que permite mostrar los Archivos del proyecto.
	 */
	private List<ArchivosProyecto> archivosGeneral = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de archivos seleccionados de la tabla.
	 */
	private List<ArchivosProyecto> listaArchivosSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Archivo anexo a editar del capitulo.
	 */
	private ArchivosProyecto archivoEdicion;
	/**
	 * Folio del Proyecto
	 */
	private String folioProyecto;
	/**
	 * Serial del Proyecto.
	 */
	private short serialProyecto;	
	/**
	 * Seccion del Capitulo 4 Seleccionada.	
	 */
	private String seccionSeleccionada;
	/**
	 * id que construye el id del grid dinamico.
	 */
	private long indexRow = 0;
	
	private ArchivosAnexosDAO archivosAnexosDAO;
	
	
	// ============================== DATOS CAPITULO 4 : CLIMA Y FENOMENOS METEOROLOGICOS ============================== //
	/**
	 * Archivo anexo a editar de Clima.
	 */
	private ArchivosProyecto archEditClima;
	/**
	 * Archivo anexo para Clima.
	 */
	private ArchivosProyecto archTempClima;
	/**
	 * Lista de archivos seleccionados de la tabla de Clima.
	 */
	private List<ArchivosProyecto> listaSeleccionadosClima = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista Seleccionada de registros anexos de Clima
	 */
	private List<ArchivosProyecto> archivosGeneralClima;
	
	// ============================== DATOS CAPITULO 4 : SUELO =========================================================
	/**
	 * Lista de archivos seleccionados de la tabla de Suelos.
	 */
	private List<ArchivosProyecto> listaSeleccionadosSuelo = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista Seleccionada de registros anexos de Suelo
	 */
	private List<ArchivosProyecto> archivosGeneralSuelo;

	// ============================== DATOS CAPITULO 4 : Delimitacion =========================================================
	/**
	 * Lista de registros de Anexos de Delimitacion del Sistema Ambiental
	 */
	private List<ArchivosProyecto> archivosGeneralDelimitacionSA;
	/**
	 * Lista de archivos seleccionados de la tabla de DelimitacionSA
	 */
	private List<ArchivosProyecto> listaSeleccionadosDSA = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de registros de Anexos de Delimitacion del area de Influencia
	 */
	private List<ArchivosProyecto> archivosGeneralDelimitacionAI;
	/**
	 * Lista de archivos seleccionados de la tabla de DelimitacionAI
	 */
	private List<ArchivosProyecto> listaSeleccionadosDAI = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de registros de Anexos de Delimitacion del Sitio del Proyecto
	 */
	private List<ArchivosProyecto> archivosGeneralDelimitacionSP;
	/**
	 * Lista de archivos seleccionados de la tabla de DelimitacionSP
	 */
	private List<ArchivosProyecto> listaSeleccionadosDSP = new ArrayList<ArchivosProyecto>();
	
	private String capitulo;
	private String subCapitulo;
	private String seccion;
	private String apartado;
	private String msj;
	private boolean bandera;

	@PostConstruct
	public void init() {
		archivoEdicion = new ArchivosProyecto();
		archivoTemporal = new ArchivosProyecto();
//		archivosGeneral = new ArrayList<ArchivosProyecto>();
		cargaBean = new CargaBean();
		proyecto = new Proyecto();
		archTempClima = new ArchivosProyecto();
		
        FacesContext context = FacesContext.getCurrentInstance();
        this.folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        this.serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
		
        context.getExternalContext().getSessionMap().get("userFolioProy");        
        claveTramite = (Integer) context.getExternalContext().getSessionMap().get("claveTramite");
        //Cargar Proyecto
        try {
            this.proyecto = miaDao.proyecto(this.folioProyecto, this.serialProyecto);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }

        if (proyecto == null) {
            this.proyecto = new Proyecto(this.folioProyecto, this.serialProyecto);
        }           
        
	}
	
	/**
	 * Inicializa un nuevo archivo para ser cargado
	 */
	public void iniciarArchivo(String subCapituloSeleccionado) {
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		
		archivoTemporal = new ArchivosProyecto();
		
		String cadenaCapitulo = params.get("capitulo").toString();
		String cadenaSubCapitulo = params.get("subcapitulo").toString();
		String cadenaSeccion = params.get("seccion").toString();
		String cadenaApartado = params.get("apartado").toString();
	
		short capitulo = Short.parseShort(cadenaCapitulo);
		short subCapitulo = Short.parseShort(cadenaSubCapitulo);
		short seccion = Short.parseShort(cadenaSeccion);
		short apartado = Short.parseShort(cadenaApartado);
		
		archivoTemporal.setFolioSerial(this.folioProyecto);
		archivoTemporal.setSerialProyecto(this.serialProyecto);
		archivoTemporal.setCapituloId(capitulo);
		archivoTemporal.setSubCapituloId(subCapitulo);
		archivoTemporal.setSeccionId(seccion);
		archivoTemporal.setApartadoId(apartado);
		seccionSeleccionada = subCapituloSeleccionado;
		cargaBean = new CargaBean();
		
		msj = "";
	}
	
	/**
	 * Guarda el archivo en la BD y el Servidor
	 */
	public void guardarArchivo() {
//		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		
//		String capitulo = params.get("capitulo").toString();
//		String subCapitulo = params.get("subcapitulo").toString();
//		String seccion = params.get("seccion").toString();
//		String apartado = params.get("apartado").toString();
//		
//		archivoTemporal = new ArchivosProyecto(this.folioProyecto, this.serialProyecto, 
//				Short.parseShort(capitulo), Short.parseShort(subCapitulo), Short.parseShort(seccion), Short.parseShort(apartado));
		
		id = miaDao.getMaxArchivoEtapaProyecto(this.folioProyecto, this.serialProyecto, 
				archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(), archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());

		archivoTemporal.setId(id);
		try {
			bandera = cargaBean.GuardaAnexos(archivoTemporal);
			if (bandera == true) {
			
			archivosGeneral.add(archivoTemporal);
			
			//Si se selecciono la seccion de CONCLUSIONES PARA GUARDAR
			if (seccionSeleccionada.equals(ArchivosAnexosEnum.CLIMAFENOMENOS.getSubCapSeleccionado())) {	
				archivosGeneralClima = miaDao.getArchivosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(),
						archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());
			}else if (seccionSeleccionada.equals(ArchivosAnexosEnum.SUELOS.getSubCapSeleccionado())) {
				archivosGeneralSuelo = miaDao.getArchivosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(),
						archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());
			}else if (seccionSeleccionada.equals(ArchivosAnexosEnum.DELIMITACIONSA.getSubCapSeleccionado())) {
				archivosGeneralDelimitacionSA = miaDao.getArchivosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(),
						archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());
			}else if (seccionSeleccionada.equals(ArchivosAnexosEnum.DELIMITACIONAI.getSubCapSeleccionado())) {
				archivosGeneralDelimitacionAI = miaDao.getArchivosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(),
						archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());
			}else if (seccionSeleccionada.equals(ArchivosAnexosEnum.DELIMITACIONSP.getSubCapSeleccionado())) {
				archivosGeneralDelimitacionSP = miaDao.getArchivosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(),
						archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());
			}		
			
			msj = "";
			cargaBean = new CargaBean();
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalSubirArchivo.hide();modalAlertArchivo.show()");
		}else {
			msj = "Se requiere cargar un archivo";
			RequestContext.getCurrentInstance().update("formArchivosDisposicion:pnlArchivo");
			
			
		}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Hubo un error en el contenido del archivo o al guardar", null));
			
			e.printStackTrace();
		}
	}
	
	/**
	 * Permite consultar el registro a editar segun la seccion en la que se encuentre.
	 * @param capitulo
	 * @param subCapitulo
	 * @param seccion
	 * @param apartado
	 */
	public void consultarArchivo(Short capitulo, Short subCapitulo, Short seccion, Short apartado) {
		
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		int rowIndex = 0;
		String idArchivo = params.get("id").toString();
		id = Integer.parseInt(idArchivo);
		String pRowIndex = params.get("pRowIndex").toString();
		rowIndex = Integer.parseInt(pRowIndex);
		
		this.seccionSeleccionada = params.get("actualizarArchivo").toString();

		archivoEdicion = new ArchivosProyecto();
		archivoEdicion = miaDao.getArchivoEtapa(this.folioProyecto, this.serialProyecto, capitulo, subCapitulo, seccion, apartado, (short) id);
		indexRow = rowIndex;
	}	
	
	/**
	 * Permite realizar la edicion del archivo de anexo seleccionado.
	 */
	public void editarArchivo() throws Exception {
		ArchivosProyecto archivoProyectoEditado = new ArchivosProyecto();
		archivoProyectoEditado = archivoEdicion;

		try {
			archivosAnexosDAO.editarArchivoProyecto(archivoProyectoEditado);
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.CLIMAFENOMENOS.getSubCapSeleccionado())) {
				archivosGeneralClima = miaDao.getArchivosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(),
						archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());
				
			} else if(this.seccionSeleccionada.equals(ArchivosAnexosEnum.SUELOS.getSubCapSeleccionado())) {
				archivosGeneralSuelo = miaDao.getArchivosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(),
						archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());
				
			} else if(this.seccionSeleccionada.equals(ArchivosAnexosEnum.DELIMITACIONSA.getSubCapSeleccionado())) {
				archivosGeneralDelimitacionSA = miaDao.getArchivosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(),
						archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());
				
			} else if(this.seccionSeleccionada.equals(ArchivosAnexosEnum.DELIMITACIONAI.getSubCapSeleccionado())) {
				archivosGeneralDelimitacionAI = miaDao.getArchivosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(),
						archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());
				
			} else if(this.seccionSeleccionada.equals(ArchivosAnexosEnum.DELIMITACIONSP.getSubCapSeleccionado())) {
				archivosGeneralDelimitacionSP = miaDao.getArchivosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(),
						archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());
				
			} 
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditarArchivo.hide();modalAlertEdArchivo.show()");
		} catch (Exception e) {
			// TODO: handle exception
		}

	}	
	
	/**
	 * Permite consultar la lista de Archivos Proyectos para la seccion de climas.
	 * @param capituloClima
	 * @param subCapituloClima
	 * @param seccionClima
	 * @param apartadoClima
	 * @return la lista de archivos proyecto.
	 */
	public void consultarDesgloseClimaCargaPagina(String capituloClima,String subCapituloClima,String seccionClima,String apartadoClima) {	
		
		archivoTemporal = new ArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloClima), 
				Short.parseShort(subCapituloClima), Short.parseShort(seccionClima), Short.parseShort(apartadoClima));
		archivosAnexosDAO = new ArchivosAnexosDAO();
		archivosGeneralClima = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloClima), 
				Short.parseShort(subCapituloClima), Short.parseShort(seccionClima), Short.parseShort(apartadoClima));

	}
	
	public List<ArchivosProyecto> consultarDesgloseClima(String capituloClima,String subCapituloClima,String seccionClima,String apartadoClima) {	
		
		archivoTemporal = new ArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloClima), 
				Short.parseShort(subCapituloClima), Short.parseShort(seccionClima), Short.parseShort(apartadoClima));
		archivosAnexosDAO = new ArchivosAnexosDAO();
		archivosGeneralClima = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloClima), 
				Short.parseShort(subCapituloClima), Short.parseShort(seccionClima), Short.parseShort(apartadoClima));
		
		return archivosGeneralClima;

	}	
	
	/**
	 * Permite consultar la lista de Archivos Proyectos para la seccion de Suelo.
	 * @param capituloSuelo
	 * @param subCapituloSuelo
	 * @param seccionSuelo
	 * @param apartadoSuelo
	 * @return la lista de archivos proyecto.
	 */
	public void consultarDesgloseSueloCargaPagina(String capituloSuelo,String subCapituloSuelo,String seccionSuelo,String apartadoSuelo) {	
		
		archivoTemporal = new ArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloSuelo), 
				Short.parseShort(subCapituloSuelo), Short.parseShort(seccionSuelo), Short.parseShort(apartadoSuelo));
		archivosAnexosDAO = new ArchivosAnexosDAO();
		archivosGeneralSuelo = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloSuelo), 
				Short.parseShort(subCapituloSuelo), Short.parseShort(seccionSuelo), Short.parseShort(apartadoSuelo));

	}
	
	public List<ArchivosProyecto> consultarDesgloseSuelo(String capituloSuelo,String subCapituloSuelo,String seccionSuelo,String apartadoSuelo) {	
		
		archivoTemporal = new ArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloSuelo), 
				Short.parseShort(subCapituloSuelo), Short.parseShort(seccionSuelo), Short.parseShort(apartadoSuelo));
		archivosAnexosDAO = new ArchivosAnexosDAO();
		archivosGeneralSuelo = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloSuelo), 
				Short.parseShort(subCapituloSuelo), Short.parseShort(seccionSuelo), Short.parseShort(apartadoSuelo));
		
		return archivosGeneralSuelo;

	}
	
	/**
	 * Permite consultar la lista de Archivos Proyectos para la seccion de Desglose del Sistema Ambiental.
	 * @param capituloDSA
	 * @param subCapituloDSA
	 * @param seccionDSA
	 * @param apartadoDSA
	 * @return la lista de archivos proyecto.
	 */
	public void consultarDesgloseDSACargaPagina(String capituloDSA,String subCapituloDSA,String seccionDSA,String apartadoDSA) {	
		
		archivoTemporal = new ArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloDSA), 
				Short.parseShort(subCapituloDSA), Short.parseShort(seccionDSA), Short.parseShort(apartadoDSA));
		archivosAnexosDAO = new ArchivosAnexosDAO();
		archivosGeneralDelimitacionSA = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloDSA), 
				Short.parseShort(subCapituloDSA), Short.parseShort(seccionDSA), Short.parseShort(apartadoDSA));
	}
	
	public List<ArchivosProyecto> consultarDesgloseDSA(String capituloDSA,String subCapituloDSA,String seccionDSA,String apartadoDSA) {	
		
		archivoTemporal = new ArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloDSA), 
				Short.parseShort(subCapituloDSA), Short.parseShort(seccionDSA), Short.parseShort(apartadoDSA));
		archivosAnexosDAO = new ArchivosAnexosDAO();
		archivosGeneralDelimitacionSA = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloDSA), 
				Short.parseShort(subCapituloDSA), Short.parseShort(seccionDSA), Short.parseShort(apartadoDSA));
		
		return archivosGeneralDelimitacionSA;

	}	
	
	/**
	 * Permite consultar la lista de Archivos Proyectos para la seccion de Desglose de Area de Influencia.
	 * @param capituloDAI
	 * @param subCapituloDAI
	 * @param seccionDAI
	 * @param apartadoDAI
	 * @return la lista de archivos proyecto.
	 */
	public void consultarDesgloseDAICargaPagina(String capituloDAI,String subCapituloDAI,String seccionDAI,String apartadoDAI) {	
		
		archivoTemporal = new ArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloDAI), 
				Short.parseShort(subCapituloDAI), Short.parseShort(seccionDAI), Short.parseShort(apartadoDAI));
		archivosAnexosDAO = new ArchivosAnexosDAO();
		archivosGeneralDelimitacionAI = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloDAI), 
				Short.parseShort(subCapituloDAI), Short.parseShort(seccionDAI), Short.parseShort(apartadoDAI));

	}
	
	public List<ArchivosProyecto> consultarDesgloseDAI(String capituloDAI,String subCapituloDAI,String seccionDAI,String apartadoDAI) {	
		
		archivoTemporal = new ArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloDAI), 
				Short.parseShort(subCapituloDAI), Short.parseShort(seccionDAI), Short.parseShort(apartadoDAI));
		archivosAnexosDAO = new ArchivosAnexosDAO();
		archivosGeneralDelimitacionAI = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloDAI), 
				Short.parseShort(subCapituloDAI), Short.parseShort(seccionDAI), Short.parseShort(apartadoDAI));
		
		return archivosGeneralDelimitacionAI;

	}	
	
	/**
	 * Permite consultar la lista de Archivos Proyectos para la seccion de Desglose del Sitio del Proyecto.
	 * @param capituloDSP
	 * @param subCapituloDSP
	 * @param seccionDSP
	 * @param apartadoDSP
	 * @return la lista de archivos proyecto.
	 */
	public void consultarDesgloseDSPCargaPagina(String capituloDSP,String subCapituloDSP,String seccionDSP,String apartadoDSP) {	
		
		archivoTemporal = new ArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloDSP), 
				Short.parseShort(subCapituloDSP), Short.parseShort(seccionDSP), Short.parseShort(apartadoDSP));
		archivosAnexosDAO = new ArchivosAnexosDAO();
		archivosGeneralDelimitacionSP = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloDSP), 
				Short.parseShort(subCapituloDSP), Short.parseShort(seccionDSP), Short.parseShort(apartadoDSP));
	}
	
	public List<ArchivosProyecto> consultarDesgloseDSP(String capituloDSP,String subCapituloDSP,String seccionDSP,String apartadoDSP) {	
		
		archivoTemporal = new ArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloDSP), 
				Short.parseShort(subCapituloDSP), Short.parseShort(seccionDSP), Short.parseShort(apartadoDSP));
		archivosAnexosDAO = new ArchivosAnexosDAO();
		archivosGeneralDelimitacionSP = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capituloDSP), 
				Short.parseShort(subCapituloDSP), Short.parseShort(seccionDSP), Short.parseShort(apartadoDSP));
		
		return archivosGeneralDelimitacionSP;

	}	
	
	/**
	 * Permite abrir la pantalla modal de eliminar archivo de la seccion seleccionada.
	 */
	public void abrirModalEliminarArchivo() {
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		
		this.seccionSeleccionada = params.get("eliminarArchivo").toString();
		
		this.setCapitulo(params.get("capitulo").toString());
		this.setSubCapitulo(params.get("subcapitulo").toString());
		this.setSeccion(params.get("seccion").toString());
		this.setApartado(params.get("apartado").toString());
		
		if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.CLIMAFENOMENOS.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaSeleccionadosClima;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.SUELOS.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaSeleccionadosSuelo;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.DELIMITACIONSA.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaSeleccionadosDSA;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.DELIMITACIONAI.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaSeleccionadosDAI;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.DELIMITACIONSP.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaSeleccionadosDSP;
		} 
	}
	
	/**
	 * Permite eliminar el o los archivos anexos seleccionados.
	 */
	public void eliminarArchivosAnexosProyecto() {
		try {		
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.CLIMAFENOMENOS.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaSeleccionadosClima) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());
				}				

				archivosGeneralClima = miaDao.getArchivosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(),
						archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());
				
				listaSeleccionadosClima = new ArrayList<ArchivosProyecto>();
			}

			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.SUELOS.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaSeleccionadosSuelo) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}	
				
				archivosGeneralSuelo = miaDao.getArchivosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(),
						archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());
			
				listaSeleccionadosSuelo = new ArrayList<ArchivosProyecto>();
			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.DELIMITACIONSA.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaSeleccionadosDSA) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}	
				
				archivosGeneralDelimitacionSA = miaDao.getArchivosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(),
						archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());
			
				listaSeleccionadosDSA = new ArrayList<ArchivosProyecto>();

			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.DELIMITACIONAI.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaSeleccionadosDAI) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}	
				
				archivosGeneralDelimitacionAI = miaDao.getArchivosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(),
						archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());
			
				listaSeleccionadosDAI = new ArrayList<ArchivosProyecto>();
			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.DELIMITACIONSP.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaSeleccionadosDSP) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}	
				
				archivosGeneralDelimitacionSP = miaDao.getArchivosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(),
						archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());

				listaSeleccionadosDSP = new ArrayList<ArchivosProyecto>();

			}
			
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("dlgEliminarArchivoAnexo.hide();modalAlertEArchivo.show()");				

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/**
	 * Permite eliminar el o los archivos anexos seleccionados.
	 */
	public List<ArchivosProyecto> eliminarArchivos(ArchivosProyecto archivosSeleccionado, String capitulo, String subCapitulo, String seccion, String apartado) {
		try {
			archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capitulo), 
					Short.parseShort(subCapitulo), Short.parseShort(seccion), Short.parseShort(apartado), archivosSeleccionado.getId());

			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("dlgEliminarArchivoAnexo.hide();modalAlertEdArchivo.show()");				
			
			archivosGeneral = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capitulo), 
					Short.parseShort(subCapitulo), Short.parseShort(seccion), Short.parseShort(apartado));

			listaArchivosSeleccionados = new ArrayList<ArchivosProyecto>();
			
			return archivosGeneral;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}
	
	/**
	 * Permite abrir el dialog para eliminar archivos.
	 */
	public void abrirModalEliminarArchivos() {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("dlgEliminarArchivoAnexo.show()");
	}
	/**
	 * @return the claveTramite
	 */
	public Integer getClaveTramite() {
		return claveTramite;
	}

	/**
	 * @param claveTramite the claveTramite to set
	 */
	public void setClaveTramite(Integer claveTramite) {
		this.claveTramite = claveTramite;
	}

	/**
	 * @return the proyecto
	 */
	public Proyecto getProyecto() {
		return proyecto;
	}

	/**
	 * @param proyecto the proyecto to set
	 */
	public void setProyecto(Proyecto proyecto) {
		this.proyecto = proyecto;
	}

	/**
	 * @return the id
	 */
	public Short getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Short id) {
		this.id = id;
	}

	/**
	 * @return the archivoTemporal
	 */
	public ArchivosProyecto getArchivoTemporal() {
		return archivoTemporal;
	}

	/**
	 * @param archivoTemporal the archivoTemporal to set
	 */
	public void setArchivoTemporal(ArchivosProyecto archivoTemporal) {
		this.archivoTemporal = archivoTemporal;
	}

	/**
	 * @return the cargaBean
	 */
	public CargaBean getCargaBean() {
		return cargaBean;
	}

	/**
	 * @param cargaBean the cargaBean to set
	 */
	public void setCargaBean(CargaBean cargaBean) {
		this.cargaBean = cargaBean;
	}

	/**
	 * @return the archivosGeneral
	 */
	public List<ArchivosProyecto> getArchivosGeneral() {
		return archivosGeneral;
	}

	/**
	 * @param archivosGeneral the archivosGeneral to set
	 */
	public void setArchivosGeneral(List<ArchivosProyecto> archivosGeneral) {
		this.archivosGeneral = archivosGeneral;
	}

	/**
	 * @return the listaArchivosSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosSeleccionados() {
		return listaArchivosSeleccionados;
	}

	/**
	 * @param listaArchivosSeleccionados the listaArchivosSeleccionados to set
	 */
	public void setListaArchivosSeleccionados(List<ArchivosProyecto> listaArchivosSeleccionados) {
		this.listaArchivosSeleccionados = listaArchivosSeleccionados;
	}

	/**
	 * @return the archivoEdicion
	 */
	public ArchivosProyecto getArchivoEdicion() {
		return archivoEdicion;
	}

	/**
	 * @param archivoEdicion the archivoEdicion to set
	 */
	public void setArchivoEdicion(ArchivosProyecto archivoEdicion) {
		this.archivoEdicion = archivoEdicion;
	}

	/**
	 * @return the folioProyecto
	 */
	public String getFolioProyecto() {
		return folioProyecto;
	}

	/**
	 * @param folioProyecto the folioProyecto to set
	 */
	public void setFolioProyecto(String folioProyecto) {
		this.folioProyecto = folioProyecto;
	}

	/**
	 * @return the serialProyecto
	 */
	public short getSerialProyecto() {
		return serialProyecto;
	}

	/**
	 * @param serialProyecto the serialProyecto to set
	 */
	public void setSerialProyecto(short serialProyecto) {
		this.serialProyecto = serialProyecto;
	}

	/**
	 * @return the archivosGeneralClima
	 */
	public List<ArchivosProyecto> getArchivosGeneralClima() {
		return archivosGeneralClima;
	}

	/**
	 * @param archivosGeneralClima the archivosGeneralClima to set
	 */
	public void setArchivosGeneralClima(List<ArchivosProyecto> archivosGeneralClima) {
		this.archivosGeneralClima = archivosGeneralClima;
	}

	/**
	 * @return the archivosGeneralSuelo
	 */
	public List<ArchivosProyecto> getArchivosGeneralSuelo() {
		return archivosGeneralSuelo;
	}

	/**
	 * @param archivosGeneralSuelo the archivosGeneralSuelo to set
	 */
	public void setArchivosGeneralSuelo(List<ArchivosProyecto> archivosGeneralSuelo) {
		this.archivosGeneralSuelo = archivosGeneralSuelo;
	}

	public List<ArchivosProyecto> getListaSeleccionadosClima() {
		return listaSeleccionadosClima;
	}

	public void setListaSeleccionadosClima(List<ArchivosProyecto> listaSeleccionadosClima) {
		this.listaSeleccionadosClima = listaSeleccionadosClima;
	}

	public List<ArchivosProyecto> getListaSeleccionadosSuelo() {
		return listaSeleccionadosSuelo;
	}

	public void setListaSeleccionadosSuelo(List<ArchivosProyecto> listaSeleccionadosSuelo) {
		this.listaSeleccionadosSuelo = listaSeleccionadosSuelo;
	}

	public ArchivosProyecto getArchTempClima() {
		return archTempClima;
	}

	public void setArchTempClima(ArchivosProyecto archTempClima) {
		this.archTempClima = archTempClima;
	}

	public ArchivosProyecto getArchEditClima() {
		return archEditClima;
	}

	public void setArchEditClima(ArchivosProyecto archEditClima) {
		this.archEditClima = archEditClima;
	}

	public String getSeccionSeleccionada() {
		return seccionSeleccionada;
	}

	public void setSeccionSeleccionada(String seccionSeleccionada) {
		this.seccionSeleccionada = seccionSeleccionada;
	}

	public String getCapitulo() {
		return capitulo;
	}

	public void setCapitulo(String capitulo) {
		this.capitulo = capitulo;
	}

	public String getSubCapitulo() {
		return subCapitulo;
	}

	public void setSubCapitulo(String subCapitulo) {
		this.subCapitulo = subCapitulo;
	}

	public String getSeccion() {
		return seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	public String getApartado() {
		return apartado;
	}

	public void setApartado(String apartado) {
		this.apartado = apartado;
	}

	public List<ArchivosProyecto> getArchivosGeneralDelimitacionSA() {
		return archivosGeneralDelimitacionSA;
	}

	public void setArchivosGeneralDelimitacionSA(List<ArchivosProyecto> archivosGeneralDelimitacionSA) {
		this.archivosGeneralDelimitacionSA = archivosGeneralDelimitacionSA;
	}

	public List<ArchivosProyecto> getArchivosGeneralDelimitacionAI() {
		return archivosGeneralDelimitacionAI;
	}

	public void setArchivosGeneralDelimitacionAI(List<ArchivosProyecto> archivosGeneralDelimitacionAI) {
		this.archivosGeneralDelimitacionAI = archivosGeneralDelimitacionAI;
	}

	public List<ArchivosProyecto> getArchivosGeneralDelimitacionSP() {
		return archivosGeneralDelimitacionSP;
	}

	public void setArchivosGeneralDelimitacionSP(List<ArchivosProyecto> archivosGeneralDelimitacionSP) {
		this.archivosGeneralDelimitacionSP = archivosGeneralDelimitacionSP;
	}

	public List<ArchivosProyecto> getListaSeleccionadosDSA() {
		return listaSeleccionadosDSA;
	}

	public void setListaSeleccionadosDSA(List<ArchivosProyecto> listaSeleccionadosDSA) {
		this.listaSeleccionadosDSA = listaSeleccionadosDSA;
	}

	public List<ArchivosProyecto> getListaSeleccionadosDAI() {
		return listaSeleccionadosDAI;
	}

	public void setListaSeleccionadosDAI(List<ArchivosProyecto> listaSeleccionadosDAI) {
		this.listaSeleccionadosDAI = listaSeleccionadosDAI;
	}

	public List<ArchivosProyecto> getListaSeleccionadosDSP() {
		return listaSeleccionadosDSP;
	}

	public void setListaSeleccionadosDSP(List<ArchivosProyecto> listaSeleccionadosDSP) {
		this.listaSeleccionadosDSP = listaSeleccionadosDSP;
	}

	public String getMsj() {
		return msj;
	}

	public void setMsj(String msj) {
		this.msj = msj;
	}

	public boolean isBandera() {
		return bandera;
	}

	public void setBandera(boolean bandera) {
		this.bandera = bandera;
	}

	public long getIndexRow() {
		return indexRow;
	}

	public void setIndexRow(long indexRow) {
		this.indexRow = indexRow;
	}
	
}
