/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.ViewHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import mx.gob.semarnat.mia.dao.MiaDao;
import mx.gob.semarnat.mia.dao.SinatecProcedure;
import mx.gob.semarnat.mia.model.ProySubsectorGuias;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.RepLegalProyecto;
import mx.gob.semarnat.mia.model.RespTecProyecto;
import mx.gob.semarnat.mia.model.RespTecProyectoPK;
import mx.gob.semarnat.mia.model.TramiteSubsector;
import mx.gob.semarnat.mia.model.catalogos.RamaProyecto;
import mx.gob.semarnat.mia.model.catalogos.SectorProyecto;
import mx.gob.semarnat.mia.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.mia.model.catalogos.TipoProyecto;
import mx.gob.semarnat.mia.model.sinatec.Vexdatosusuario;
import mx.gob.semarnat.mia.model.sinatec.Vexdatosusuariorep;
import mx.gob.semarnat.mia.util.Log;
import mx.gob.semarnat.mia.util.RepLegalu;

import org.primefaces.context.RequestContext;

/**
 *
 * @author mauricio
 */
@ManagedBean
@ViewScoped
public class Capitulo1View implements Serializable {

	//GuiasPdf
	List<ProySubsectorGuias>guiasPdf=new ArrayList<ProySubsectorGuias>();
	List<String>urlPdfValidad=new ArrayList<String>();
	private boolean hayGuias=true;
	
	
	//obtiene subsector y folio para determinar la guia pdf a mostrar
	private MiaDao miaDao = new MiaDao();
    private Proyecto proyecto = new Proyecto();
    private String sector = "";
    private String subSector = "";
    private String rama = "";
    private String tipo = "";

    private static final SinatecProcedure sp = new SinatecProcedure();

    // ** catalogos
    private List<SubsectorProyecto> subSectorCat = miaDao.getCatSubSector();
    private List<RamaProyecto> ramaCat = new ArrayList<RamaProyecto>();
    private List<TipoProyecto> tipoCat = new ArrayList<TipoProyecto>();
    // ** Promovente
    private Vexdatosusuario promovente = new Vexdatosusuario();
    private String vialidad = "";
    private String nomVialidad = "";
    private String asentamiento = "";
    private Boolean datPromovente = false;
    private Boolean msgPromovente = false;
    private String entidadPromovente = "";
    
    // ** RepLegal
    private List<Vexdatosusuariorep> representanes = new ArrayList<Vexdatosusuariorep>();
    private RepLegalProyecto repLegal = new RepLegalProyecto();
    private List<SelectItem> representantes = new ArrayList<SelectItem>();
    private List<RepLegalu> repLegalu = new ArrayList<RepLegalu>();
    private String selCurp = "";
    // ** REEIA
    private Boolean editarREEIA = false;
    private Boolean disablREEIA = false;
    private String mismo = "";
    private RespTecProyecto repEEIA = new RespTecProyecto();

    private Boolean disa = false;
    private Integer claveTramite = 0;
    private short subsec = 0;
    private short nrama = 0;
    int pagoDer = 0;


    /**
     * Creates a new instance of Capitulo1View
     */
    public Capitulo1View() {
    	System.out.println("Capitulo1View");    	
        // Recupera proyecto de sesion
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
        claveTramite = (Integer) context.getExternalContext().getSessionMap().get("claveTramite");
        subsec = (Short) context.getExternalContext().getSessionMap().get("subsector");
        subSector = miaDao.nombreSubsector(folioProyecto);
        rama = miaDao.nombreRama(folioProyecto);
        tipo = miaDao.nombreTipoProy(folioProyecto);
        
        //Cargar Proyecto
        try {
            this.proyecto = miaDao.proyecto(folioProyecto, serialProyecto);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }

        if (proyecto == null) {
            this.proyecto = new Proyecto(folioProyecto, serialProyecto);
            proyecto.setEstatusProyecto("A");
            proyecto.setClaveProyecto("0");
        }

        System.out.println("Folio: " + folioProyecto);
        System.out.println("Serial: " + serialProyecto);
        System.out.println("SubSector: " + subSector );
        System.out.println("Rama: " + rama);

        if (proyecto.getNsub() != null) {
            ramaCat = miaDao.getCatRama(proyecto.getNsub());
            SubsectorProyecto su = miaDao.getSubSector(proyecto.getNsub());
            SectorProyecto sectorProyT = miaDao.getSectorProyecto(su.getNsec());
            sector = sectorProyT.getSector();
            proyecto.setNsec(sectorProyT.getNsec());
        }

        if (proyecto.getNrama() != null) {
            tipoCat = miaDao.getCatTipoProyecto(proyecto.getNrama());
        }

        if (proyecto.getNsub() != null && proyecto.getNrama() != null) {
            if (proyecto.getNsub() > 0 && proyecto.getNrama() > 0) {
                disa = true;
            }
        }
        // promovente
        try {
            promovente = miaDao.getPromovente(proyecto.getProyectoPK().getFolioProyecto());
            nomVialidad = promovente.getVcnombrevialidad();
            vialidad = miaDao.getVialidad(promovente.getBgcveTipoVialLk().toString()).getDescripcion();
            asentamiento = miaDao.getTipoAsentamiento(promovente.getBgcveTipoAsenLk().toString()).getNombre();
            entidadPromovente = miaDao.obtenerEntidadPromovente(promovente);
            datPromovente = true;
            msgPromovente = false;

            representanes = miaDao.datosPrepLeg(Integer.valueOf(proyecto.getProyectoPK().getFolioProyecto()));
            try {
                repLegal = miaDao.getSelRepLegal(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

                for (Vexdatosusuariorep r : representanes) {
                    if (repLegal.getRepLegalProyectoPK().getRepresentanteCurp().equals(r.getVccurp())) {
                        r.setSelected(true);
                    }
                }
            } catch (Exception e) {
                System.out.println("Erro en rep legal" + e.getLocalizedMessage());
            }
            for (Vexdatosusuariorep r : representanes) {
                RepLegalu r2 = new RepLegalu();
                r2.setVexdatosusuariorep(r);
                RepLegalProyecto r3 = null;
                try {
                    r3 = miaDao.getRepLegalCurp(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), r.getVccurp());
                } catch (Exception e3) {

                }
                if (r3 == null) {
                    r3 = new RepLegalProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), r.getVccurp());
//                    r3.setRfc(sector);
//                    r3.setRepresentanteCurp(r.getVccurp());
                }
                r2.setRepLegal(r3);
                repLegalu.add(r2);
            }

        } catch (Exception e) {
            datPromovente = false;
            msgPromovente = true;

            System.out.println("Promovente no encontrado");
            e.printStackTrace();
        }

        try {
            for (RepLegalProyecto r : miaDao.getListSelRepLegal(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto())) {
                SelectItem s = new SelectItem(r.getRepLegalProyectoPK().getRepresentanteCurp(), r.getRepLegalProyectoPK().getRepresentanteCurp());
                representantes.add(s);
            }

            if (proyecto.getProyRespTecIgualLegal() != null) {
                try {
                    repEEIA = miaDao.getRespTec(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                } catch (Exception e) {e.printStackTrace();}
                
                disablREEIA = true;
                editarREEIA = true;
                mismo = "" + proyecto.getProyRespTecIgualLegal();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        guiasPdf=miaDao.getUrlGuiaPdf(new Integer(subsec),claveTramite);
        for (ProySubsectorGuias g : guiasPdf) {
			//System.out.println(g.getId().getUrlGuia());
			if(g.getId().getUrlGuia()!=null && g.getId().getUrlGuia().startsWith("http://") && g.getId().getUrlGuia().endsWith(".pdf")){
				urlPdfValidad.add("'"+g.getId().getUrlGuia()+"'");
			}
		}
        
        pagoDer = miaDao.existePagoDerechos(proyecto.getProyectoPK().getFolioProyecto());
    }

    
    public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


    public Boolean getTipoPerMor() {
        return promovente.getVctipopersona().equals("MOR");
    }

    public Boolean getTipoPerFis() {
        return promovente.getVctipopersona().equals("FIS");

    }

    public void selRepLegal() {
        if (repLegal != null) {
            try {
                Vexdatosusuariorep r = miaDao.getRepLeg(repLegal.getRepLegalProyectoPK().getRepresentanteCurp());
                repEEIA.setRespTecApellido1(r.getVcapellido1());
                repEEIA.setRespTecApellido2(r.getVcapellido2());
                repEEIA.setRespTecCurp(r.getVccurp());
                repEEIA.setRespTecNombre(r.getVcnombre());
                repEEIA.setRespTecRfc("");

                try {
                    RepLegalProyecto r2 = miaDao.getRepLegalCurp(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), r.getVccurp());
                    repEEIA.setRespTecRfc(r2.getRfc());

                } catch (Exception e44) {
                    e44.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void selSubSector() {
        sector = "";
        ramaCat = new ArrayList<RamaProyecto>();
        tipoCat = new ArrayList<TipoProyecto>();
        proyecto.setNrama(null);
        proyecto.setNtipo(null);
        if (proyecto.getNsub() != null) {
            ramaCat = miaDao.getCatRama(proyecto.getNsub());

            SubsectorProyecto su = miaDao.getSubSector(proyecto.getNsub());
            SectorProyecto sp = miaDao.getSectorProyecto(su.getNsec());
            sector = sp.getSector();
        }
    }

    public void selRama() {
        if (proyecto.getNrama() != null) {
            tipoCat = miaDao.getCatTipoProyecto(proyecto.getNrama());
        }
    }

    // ** REEIA
    public void selMismo() {
        if (mismo != null || !mismo.isEmpty()) {
            if (mismo.equals("S")) {
                repEEIA = new RespTecProyecto();
                editarREEIA = true;
            } else {
                editarREEIA = false;
                repEEIA = new RespTecProyecto();
            }
        }
    }

    public void guardar() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        try {
            miaDao.guardar(proyecto);

            if (proyecto.getNsub() != null && proyecto.getNrama() != null) {
                if (proyecto.getNsub() > 0 && proyecto.getNrama() > 0) {
                    disa = true;
                }
            }

            reqcontEnv.execute("parent.actMenu();");
            reqcontEnv.execute("parent.actualizaMenu();");
            //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
            try {
                miaDao.guardarAvance(proyecto, new Short("4"), "11");
                sp.ejecutaSP(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()), Integer.parseInt(miaDao.avanceProyecto(proyecto)));
                if (Integer.parseInt(miaDao.avanceProyecto(proyecto)) == 99) {
                    reqcontEnv.execute("alert('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.')");
                }
            } catch (Exception e) {
                Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e.getMessage());
//                System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
            }
            //</editor-fold>

            reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success');");
        } catch (Exception e) {
            Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e.getMessage());
//            e.printStackTrace();
            reqcontEnv.execute("mensajes('No se ha podido guardar la información, intente mas tarde.', 'danger')");
        }
    }

    public void guardarGral() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        try {
            for (Vexdatosusuariorep v : representanes) {
                if (v.getSelected()) {
//                    if (proyecto.getRepLegalProyectoList() == null) {
//                        proyecto.setRepLegalProyectoList(new ArrayList<RepLegalProyecto>());
//                    }
//                    miaDao.elminarRepLegal(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
//                    proyecto.getRepLegalProyectoList().clear();
//                    repLegal = new RepLegalProyecto();
//                    repLegal.setProyecto(proyecto);
//                    repLegal.setRfc("");
////                    repLegal.setRepresentanteCurp(v.getVccurp());
//                    repLegal.setRepLegalProyectoPK(new RepLegalProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), v.getVccurp()));
//                    miaDao.merge(repLegal);
                }
            }

            for (RepLegalu u : repLegalu) {
//                if (u.getCRepLegal().getRfc() != null && !u.getRepLegal().getRfc().isEmpty() ){
                miaDao.merge(u.getRepLegal());
//                }
            }

            miaDao.guardar(proyecto);
            //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
            try {
                miaDao.guardarAvance(proyecto, new Short("2"), "12");
                sp.ejecutaSP(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()), Integer.parseInt(miaDao.avanceProyecto(proyecto)));
                if (Integer.parseInt(miaDao.avanceProyecto(proyecto)) == 99) {
                    reqcontEnv.execute("alert('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.')");
                }
            } catch (Exception e) {
                Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e.getMessage());
//    System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
            }
            //</editor-fold>

            reqcontEnv.execute("parent.actMenu();");
            reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success');");

        } catch (Exception e) {
            Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e.getMessage());
//e.printStackTrace();
            reqcontEnv.execute("mensajes('No se ha podido guardar la información, intente mas tarde.', 'danger')");
        }
    }

    public Boolean getVerCombo() {
        return (editarREEIA.booleanValue() == disablREEIA.booleanValue());
    }

    public void guardarREEIA() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        try {
            miaDao.eliminarResTec(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

            repEEIA.setRespTecProyectoPK(new RespTecProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
            miaDao.merge(repEEIA);

            if (msgPromovente) {
                proyecto.setProyRespTecIgualLegal('N');
            } else {
                proyecto.setProyRespTecIgualLegal(mismo.charAt(0));
            }
            miaDao.guardar(proyecto);
            reqcontEnv.execute("parent.actMenu();");
            reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success');");
            //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
            try {
                miaDao.guardarAvance(proyecto, new Short("2"), "13");
                if (Integer.parseInt(miaDao.avanceProyecto(proyecto)) == 99) {
                    reqcontEnv.execute("mensajes('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.', 'success')");
                }
            } catch (Exception e) {
                System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
            }
            //</editor-fold>

        } catch (Exception e) {
            Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e.getMessage());
//          e.printStackTrace();
            reqcontEnv.execute("mensajes('No se ha podido guardar la información, intente mas tarde.', 'danger')");
        }
    }
        
    //Método para abrir una guía PDF
    public void abreGuia(){
    	for(int i=0; i<urlPdfValidad.size();i++){
    		RequestContext.getCurrentInstance().execute("window.open('" + urlPdfValidad.get(i) + "')");
    	}
    }
    

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the subSectorCat
     */
    public List<SubsectorProyecto> getSubSectorCat() {
        return subSectorCat;
    }

    /**
     * @param subSectorCat the subSectorCat to set
     */
    public void setSubSectorCat(List<SubsectorProyecto> subSectorCat) {
        this.subSectorCat = subSectorCat;
    }

    /**
     * @return the ramaCat
     */
    public List<RamaProyecto> getRamaCat() {
        return ramaCat;
    }

    /**
     * @param ramaCat the ramaCat to set
     */
    public void setRamaCat(List<RamaProyecto> ramaCat) {
        this.ramaCat = ramaCat;
    }

    /**
     * @return the tipoCat
     */
    public List<TipoProyecto> getTipoCat() {
        return tipoCat;
    }

    /**
     * @param tipoCat the tipoCat to set
     */
    public void setTipoCat(List<TipoProyecto> tipoCat) {
        this.tipoCat = tipoCat;
    }

    /**
     * @return the sector
     */
    public String getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(String sector) {
        this.sector = sector;
    }

    /**
     * @return the editarREEIA
     */
    public Boolean getEditarREEIA() {
        return editarREEIA;
    }

    /**
     * @param editarREEIA the editarREEIA to set
     */
    public void setEditarREEIA(Boolean editarREEIA) {
        this.editarREEIA = editarREEIA;
    }

    /**
     * @return the mismo
     */
    public String getMismo() {
        return mismo;
    }

    /**
     * @param mismo the mismo to set
     */
    public void setMismo(String mismo) {
        this.mismo = mismo;
    }

    /**
     * @return the repEEIA
     */
    public RespTecProyecto getRepEEIA() {
        return repEEIA;
    }

    /**
     * @param repEEIA the repEEIA to set
     */
    public void setRepEEIA(RespTecProyecto repEEIA) {
        this.repEEIA = repEEIA;
    }

    /**
     * @return the promovente
     */
    public Vexdatosusuario getPromovente() {
        return promovente;
    }

    /**
     * @param promovente the promovente to set
     */
    public void setPromovente(Vexdatosusuario promovente) {
        this.promovente = promovente;
    }

    /**
     * @return the vialidad
     */
    public String getVialidad() {
        return vialidad;
    }

    /**
     * @param vialidad the vialidad to set
     */
    public void setVialidad(String vialidad) {
        this.vialidad = vialidad;
    }

    /**
     * @return the asentamiento
     */
    public String getAsentamiento() {
        return asentamiento;
    }

    /**
     * @param asentamiento the asentamiento to set
     */
    public void setAsentamiento(String asentamiento) {
        this.asentamiento = asentamiento;
    }

    /**
     * @return the datPromovente
     */
    public Boolean getDatPromovente() {
        return datPromovente;
    }

    /**
     * @param datPromovente the datPromovente to set
     */
    public void setDatPromovente(Boolean datPromovente) {
        this.datPromovente = datPromovente;
    }

    /**
     * @return the msgPromovente
     */
    public Boolean getMsgPromovente() {
        return msgPromovente;
    }

    /**
     * @param msgPromovente the msgPromovente to set
     */
    public void setMsgPromovente(Boolean msgPromovente) {
        this.msgPromovente = msgPromovente;
    }

    /**
     * @return the representanes
     */
    public List<Vexdatosusuariorep> getRepresentanes() {
        return representanes;
    }

    /**
     * @param representanes the representanes to set
     */
    public void setRepresentanes(List<Vexdatosusuariorep> representanes) {
        this.representanes = representanes;
    }

    /**
     * @return the repLegal
     */
    public RepLegalProyecto getRepLegal() {
        return repLegal;
    }

    /**
     * @param repLegal the repLegal to set
     */
    public void setRepLegal(RepLegalProyecto repLegal) {
        this.repLegal = repLegal;
    }

    /**
     * @return the disablREEIA
     */
    public Boolean getDisablREEIA() {
        return disablREEIA;
    }

    /**
     * @param disablREEIA the disablREEIA to set
     */
    public void setDisablREEIA(Boolean disablREEIA) {
        this.disablREEIA = disablREEIA;
    }

    /**
     * @return the representantes
     */
    public List<SelectItem> getRepresentantes() {
        return representantes;
    }

    /**
     * @param representantes the representantes to set
     */
    public void setRepresentantes(List<SelectItem> representantes) {
        this.representantes = representantes;
    }

    /**
     * @return the selCurp
     */
    public String getSelCurp() {
        return selCurp;
    }

    /**
     * @param selCurp the selCurp to set
     */
    public void setSelCurp(String selCurp) {
        this.selCurp = selCurp;
    }

    /**
     * @return the repLegalu
     */
    public List<RepLegalu> getRepLegalu() {
        return repLegalu;
    }

    /**
     * @param repLegalu the repLegalu to set
     */
    public void setRepLegalu(List<RepLegalu> repLegalu) {
        this.repLegalu = repLegalu;
    }

    /**
     * @return the subSector
     */
    public String getSubSector() {
        return subSector;
    }

    /**
     * @param subSector the subSector to set
     */
    public void setSubSector(String subSector) {
        this.subSector = subSector;
    }

    /**
     * @return the disa
     */
    public Boolean getDisa() {
        return disa;
    }

    /**
     * @param disa the disa to set
     */
    public void setDisa(Boolean disa) {
        this.disa = disa;
    }

    public Integer getClaveTramite() {
        return claveTramite;
    }

    public void setClaveTramite(Integer claveTramite) {
        this.claveTramite = claveTramite;
    }

    public short getSubsec() {
        return subsec;
    }

    public void setSubsec(short subsec) {
        this.subsec = subsec;
    }

    /**
	 * @return the nrama
	 */
	public short getNrama() {
		return nrama;
	}

	/**
	 * @param nrama the nrama to set
	 */
	public void setNrama(short nrama) {
		this.nrama = nrama;
	}

	/**
     * @return the nomVialidad
     */
    public String getNomVialidad() {
        return nomVialidad;
    }

    /**
     * @param nomVialidad the nomVialidad to set
     */
    public void setNomVialidad(String nomVialidad) {
        this.nomVialidad = nomVialidad;
    }

    public String getEntidadPromovente() {
        return entidadPromovente;
    }


	public List<ProySubsectorGuias> getGuiasPdf() {
		
		
		return guiasPdf;
	}


	public void setGuiasPdf(List<ProySubsectorGuias> guiasPdf) {
		this.guiasPdf = guiasPdf;
	}


	public boolean isHayGuias() {
		System.out.println("Id Subsector= "+subsec);
    	System.out.println("Clave de tramite= "+claveTramite);
    	System.out.println("Existen "+guiasPdf.size()+" guias");
    	
    	if(urlPdfValidad.isEmpty()){
			hayGuias=true;
		}else{
			hayGuias=false;
		}
		
		return hayGuias;
	}


	public void setHayGuias(boolean hayGuias) {
		this.hayGuias = hayGuias;
	}

    
	public String getRama() {
		return rama;
	}
    
    
	public void setRama(String rama) {
		this.rama = rama;
	}

	public List<String> getUrlPdfValidad() {
		return urlPdfValidad;
	}

	public void setUrlPdfValidad(List<String> urlPdfValidad) {
		this.urlPdfValidad = urlPdfValidad;
	}

	public int getPagoDer() {
		return pagoDer;
	}

	public void setPagoDer(int pagoDer) {
		this.pagoDer = pagoDer;
	}
   
}