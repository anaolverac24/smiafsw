/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.FileOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import javax.faces.application.ViewHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import mx.gob.semarnat.mia.dao.EntityFactory;
import mx.gob.semarnat.mia.dao.MiaDao;
import mx.gob.semarnat.mia.dao.SigeiaDao;
import mx.gob.semarnat.mia.dao.SinatecProcedure;
import mx.gob.semarnat.mia.dao.VisorDao;
import mx.gob.semarnat.mia.model.ArchivosProyecto;
import mx.gob.semarnat.mia.model.CatParametro;
import mx.gob.semarnat.mia.model.EvaluacionProyecto;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.SustanciaProyecto;
import mx.gob.semarnat.mia.model.sinatec.Vexdatosusuario;
import mx.gob.semarnat.mia.report.ReportFactory;

import org.primefaces.context.RequestContext;

import Token.DecToken;

/**
 *
 * @author mauricio
 */
@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class PrincipalView implements Serializable {

    private Boolean estatusProyecto = true;
    private final MiaDao miaDao = new MiaDao();
    private final VisorDao visorDao = new VisorDao();
    private final SigeiaDao sinDao = new SigeiaDao();
    private String info = "";
    private String tipoTramite = "";
    private String nombreSubsector = "";
    private Vexdatosusuario promovente = new Vexdatosusuario();
    private String nombrePromovente = "";
    private Proyecto proy;
    private Integer claveTramite = 0;
    private Short subsector = 0;
    private String activePage = "";
    private String userFolioProy;
    private short sustanciasExceden = 0;  // sustancias riesgosas que tiene el proyecto que exceden el limite
    
    private HashMap<String, boolean[]> mapInfoAdicional;   
    
    private String msgCierreTramite;
    
    private boolean existenDatosGenerales;
    private boolean reia;
    @SuppressWarnings("unused")
	public PrincipalView() {
        System.out.println("Princial View");
        
        FacesContext context = FacesContext.getCurrentInstance();

        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String q = req.getQueryString();        
        

        
        //Primero se desencripta el folio del proyecto
                                       
        try
        {
            DecToken decToken = new DecToken ();            
            String vFolioEncriptado = q.substring(q.indexOf("folioProy")+"folioProy".length()+1);
                    
            userFolioProy = decToken.DecryptCentralToken(vFolioEncriptado);

            System.out.println("desencriptado--->    " + userFolioProy);
            
            
        }catch (Exception msg){
            //msg.printStackTrace();
        }
        
        // configuracion_pool
        // comentar para URL encriptada (se encripta siempre que va a produccion pero hay casos que tambien cuando va a desarrollo [preguntar para este caso como debe deployarse])
        // descomentar para URL normal ( sin encriptar - osea en el browser sería normal y no un chorizo encriptado  [normalmente en desarrollo] se manda asi)
//        try{
//        	userFolioProy = q.substring(q.indexOf("folioProy")+"folioProy".length()+1);
//        }catch(NullPointerException nu){
//            userFolioProy= (String)   context.getExternalContext().getSessionMap().get("folioProyecto");
//        }
        
        // Busca proyecto
        //Proyecto proyecto = miaDao.proyecto(userFolioProy, (short) 1);
        Proyecto proyecto = miaDao.proyecto(userFolioProy);
        proy = proyecto;
                
        
        // Si no existe crearlo
        if (proyecto == null) {
        	reia=Boolean.TRUE;
            proyecto = new Proyecto(userFolioProy, (short) 1, "0");
            proyecto.setEstatusProyecto("A");
            proyecto.setClaveProyecto("0");
            SinatecProcedure sp = new SinatecProcedure();
//           long lote = sp.getLote(Integer.parseInt(proyecto.getProyectoPK().getFolioProyecto()), "Proyecto_" + proyecto.getProyectoPK().getFolioProyecto());
//            if (lote != -1) {
//                proyecto.setProyLote(lote);
//            }
//            try {
//                miaDao.guardar(proyecto);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        }else{
        	reia=!(proyecto.getProyNombre()!=null && proyecto.getProyNombre().length()>0 && proyecto.getNsub()!=null );
        }

        //Agrega el estatus y el número de proyecto si son nulos
        System.out.println("Estatus y Clave del Proyecto");
        if ((proyecto.getEstatusProyecto() == null) || (proyecto.getEstatusProyecto() == " ")) {
            proyecto.setEstatusProyecto("A");
        }

        if ((proyecto.getClaveProyecto() == " ") || (proyecto.getClaveProyecto() == null)) {
            proyecto.setClaveProyecto("0");
        }

        System.out.println(proyecto.getEstatusProyecto() + "  <--->  " + proyecto.getClaveProyecto());

        if (proyecto.getNsub() != null) {
            info = "" + proyecto.getNsub();

        }                

        //bandera que determinara la funcionalidad entre MIA REGIONAL o MIA PARTICULAR
        tipoTramite = miaDao.tipotramite(Integer.valueOf(userFolioProy));
        nombreSubsector = miaDao.nombreSubsector(userFolioProy);

        if (miaDao.getPromovente(proyecto.getProyectoPK().getFolioProyecto()) != null) {
            promovente = miaDao.getPromovente(proyecto.getProyectoPK().getFolioProyecto());
            nombrePromovente = promovente.getVcnombre() + " " + promovente.getVcapellidopaterno() + " " + promovente.getVcapellidomaterno();
        }

        if (proyecto.getNsub() == null) {
            proyecto.setNsub((short) 1);
        }
        System.out.println("tipoTramite " + tipoTramite);
        claveTramite = miaDao.claveTramite(Integer.valueOf(userFolioProy));
        subsector = proyecto.getNsub();
        
        System.out.println("claveTramite " + claveTramite + " subsector " + subsector);
       
        context.getExternalContext().getSessionMap().put("userFolioProy", userFolioProy);
        context.getExternalContext().getSessionMap().put("folioProyecto", proyecto.getProyectoPK().getFolioProyecto());
        context.getExternalContext().getSessionMap().put("serialProyecto", proyecto.getProyectoPK().getSerialProyecto());
        context.getExternalContext().getSessionMap().put("subsector", proyecto.getNsub());
        

        //Se asigna el trámite 64 por si no encuentra el trámite.
        if (claveTramite == 0) {
            claveTramite = 64;
        }

        context.getExternalContext().getSessionMap().put("claveTramite", claveTramite);
        System.out.println("tipoTramite ----> -----> -----> " + tipoTramite);
        
        //Indicación de Información adicional en el menú
        String bitacora = miaDao.obtenerBitacoraDeFolio(proyecto.getProyectoPK().getFolioProyecto());
        mapInfoAdicional = generaMapaInfoAdicional(bitacora);
        
        //Verifica si ya fueron capturados los datos generales del proyecto
        if (proyecto.getProyNombre() != null && proyecto.getNsub() != null && 
                proyecto.getNrama() != null && proyecto.getNtipo() != null) {
            existenDatosGenerales = true;
        } else {
            existenDatosGenerales = false;
        }         
        this.activePage = "index.xhtml";
        
        
        
        
        sustanciasExceden = 0;
        // Sustancias
        for (SustanciaProyecto s : miaDao.getSustancias(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto())) {
              
        	if(s.getExcedeLimite().equalsIgnoreCase("1")){
        		sustanciasExceden++;
        	}           
        }
           

    }
    
    /**
     * 
     * @param bitacora
     * @return 
     * 
     * Mapa:
     * Capitulo-Subcapitulo --> existe info adicional, hay modificacion de info
     *          "2-1"       -->         true,                   false
     */
    @SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	public HashMap<String,boolean[]> generaMapaInfoAdicional(String bitacora) {
        HashMap<String,boolean[]> mapRes = new HashMap();
        Integer idx;
        ArrayList<String[]> lstInfo;
        boolean[] bRes;
        
        lstInfo = miaDao.obtieneComenariosProyectoCapitulo(bitacora);
        lstInfo.addAll(miaDao.obtieneComenariosProyectoSubCap(bitacora));
        for (Object[] reg : lstInfo) {
            bRes = new boolean[2];
            bRes[0] = ((String)reg[1]).compareTo("1") == 0;
            bRes[1] = ((String)reg[2]).compareTo("1") == 0;
           
            mapRes.put( "cap_" + reg[0], bRes);
        }
        
        return mapRes;
    }
    

    public String getEsTRid() {
        try {
            if (proy.getEstudioRiesgoId() == null) {

                return "";
            }

            if (proy.getProySustanciaAplica().equals("S") && proy.getEstudioRiesgoId() == 1) {
                return "1";
            }
            
        } catch (Exception e) {
            return "0";
        }
        
        return "0"; 
    }

    public void actProyInfo() {
        Proyecto proyecto = miaDao.proyecto(proy.getProyectoPK().getFolioProyecto(), proy.getProyectoPK().getSerialProyecto());
        proy = proyecto;
    }

    //<editor-fold desc="Getters & Setters" defaultstate="collapsed">
    /**
     * @return the info
     */
    public String getInfo() {
        return info;
    }

    /**
     * @param info the info to set
     */
    public void setInfo(String info) {
        this.info = info;
    }

    public String getTipoTramite() {
        return tipoTramite;
    }

    public void setTipoTramite(String tipoTramite) {
        this.tipoTramite = tipoTramite;
    }
    
     public String getActivePage() {
        return activePage;
    }

    public void setActivePage(String activePage) {
        this.activePage = activePage;
    }//</editor-fold>
    
    //<editor-fold desc="Método para cambiar de pagina" defaultstate="collapsed">
    public void cambiarPagina(){
        FacesContext  param = FacesContext.getCurrentInstance();
        this.activePage= (String) param.getExternalContext().getSessionMap().get("destino");
    }
    //</editor-fold>

    //<editor-fold desc="Verifica si el trámite esta al 99%" defaultstate="collapsed">
    public Boolean getFinTramite() {
        try {
            return Integer.parseInt(miaDao.avanceProyecto(getProy())) == 99;
        } catch (Exception e) {
            return false;
        }
    }//</editor-fold>

    //<editor-fold desc="Obtener el porcentaje de avance del proyecto" defaultstate="collapsed">
    public int getAvance() {
        if (getProy() != null) {
            String a = miaDao.avanceProyecto(getProy());
            return Integer.parseInt(a);
        }
        return 0;
    }//</editor-fold>

    //<editor-fold desc="Método para finalizar trámite" defaultstate="collapsed">
    @Deprecated
    public void _finaliza() throws Exception {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        String vFolio = getProy().getProyectoPK().getFolioProyecto();
        short vSerial = getProy().getProyectoPK().getSerialProyecto();
        String vBitacoraProyecto = getProy().getBitacoraProyecto();
        SinatecProcedure sp = new SinatecProcedure();
        String vMensaje = "";

        System.out.println("--Folio trámite--" + vFolio + "  serial  " + vSerial);
        
        if (vSerial == 3) {
            vMensaje = sp.solicitudCarga(Integer.parseInt(vFolio), "FINALIZA_TRAMITE", vBitacoraProyecto, null);
        }

        Long LoteProyecto = null;
        int noArch = 0;
        EvaluacionProyecto EvaProy = miaDao.datosEvaProy(vBitacoraProyecto);

        if (vSerial == 1) {
            LoteProyecto = getProy().getProyLote();
            //noArch = MiaDao.tamanioLista(MiaDao.getAnexos(new Short("0"), new Short("0"), new Short("0"), new Short("0"), vFolio, vSerial));
            noArch = visorDao.conteoAdjuntos(vFolio, vSerial, null).intValue();
        } else if (vSerial == 3) {
            LoteProyecto = EvaProy.getEvaLoteInfoAdicional();
            noArch = 0;
        }

        System.out.println("--Cerrando trámite--" + vMensaje);

        System.out.println("Serial " + vSerial + "Bitacora Proyecto " + vBitacoraProyecto + "lote 2 ---> " + LoteProyecto);

        if (LoteProyecto != null) {

            //ejecuta SP cuando completa el trámite            
            int i = sp.sp_fin(LoteProyecto, noArch);
//            pdf = true;
            System.out.println(i == 0 ? "--Procedimiento OK--" : "Error al ejecutar el procedimiento");
            if (i == 0) {
                miaDao.guardarAvance(getProy(), new Short("1"), "0");
                getProy().setEstatusProyecto("C");
                miaDao.merge(getProy());
                //Proceso de captura completo
                reqcontEnv.execute("alert('Ha completado el registro de datos de la Manifestación de Impacto Ambiental, ahora puede firmar su trámite')");
            } else {
                reqcontEnv.execute("alert('No se ha completado el registro, intente de nuevo más tarde')");
            }
        }
    }//</editor-fold>

	//<editor-fold desc="Metodo para finalizar tramite" defaultstate="collapsed">
    public void finaliza() throws Exception {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        String vFolio = getProy().getProyectoPK().getFolioProyecto();
        short vSerial = getProy().getProyectoPK().getSerialProyecto();
        String vBitacoraProyecto = getProy().getBitacoraProyecto();
        SinatecProcedure sp = new SinatecProcedure();
        String vMensaje = "";

        System.out.println("--Folio tramite--" + vFolio + "  serial  " + vSerial);        
        //Validacion de existencia de adjuntos
        int noArch = 0;
        noArch = visorDao.conteoAdjuntos(vFolio, vSerial, null).intValue();
        System.out.println("No. de Archivos ANTES de generar Resumen de MIA en PDF--" + noArch);
        if (noArch > 0) {
            //Generacion de reporte de resumen
            System.out.println("-------Generación de reporte de resumen--------");
            generaReporteResumen(vFolio, vSerial);
            Long LoteProyecto = null;

            Integer vContador = 0;
            
            if (vSerial == 1) {
                //Aviso al Sinatec de los archivos que se adjuntaron
                ArrayList<Object[]> lstAdjuntos = visorDao.obtenerAdjuntos(vFolio, vSerial);
                int idx = 0;
                Integer iLote = null; //0; //
                for (Object[] adjunto : lstAdjuntos) {
                    System.out.println("-------Procedimeinto SINATEC PSTFOLIODOCSATLOTE_RG Serial 1--------");
                    sp.solicitudCarga(Integer.parseInt(vFolio), adjunto[0] != null ? (String)adjunto[0] : "", (String)adjunto[1], iLote);
                    if (iLote == null) {
                        Long lLote = miaDao.obtenerLoteProyecto(vFolio, vSerial);
                        iLote = lLote != null ? lLote.intValue() : null;
                        System.out.println("Lote:  " + iLote );
                    }
                    System.out.println("Adjunto ---> " + idx + ": Archivo: " + adjunto[0] != null ? (String)adjunto[0] : "" + " Descripción: " + adjunto[1] != null ? (String)adjunto[1] : "" );
                    vContador += 1;
                    idx++;
                }
                
                LoteProyecto = new Long(iLote);//getProy().getProyLote();
                //se realiza de nuevo el conteo de archivos, para tomar en cuenta el archivo de resumen de PDF
                //de la MIA que se adjunto en el procedimeinto generaReporteResumen(vFolio, vSerial);
                noArch = visorDao.conteoAdjuntos(vFolio, vSerial, null).intValue();
            } else if (vSerial == 3) {
                System.out.println("-------Procedimeinto SINATEC PSTFOLIODOCSATLOTE_RG Serial 3--------");
                vMensaje = sp.solicitudCarga(Integer.parseInt(vFolio), "FINALIZA_TRAMITE", vBitacoraProyecto, null);
                EvaluacionProyecto EvaProy = miaDao.datosEvaProy(vBitacoraProyecto);
                LoteProyecto = EvaProy.getEvaLoteInfoAdicional();
                noArch = 0;
            }
            System.out.println("No. de Archivos DESPUES de generar Resumen de MIA en PDF--" + noArch);
            System.out.println("--Cerrando tramite--" + vMensaje);

            System.out.println("Serial " + vSerial + "Bitacora Proyecto " + vBitacoraProyecto + "lote 2 ---> " + LoteProyecto);
        
            System.out.println("-------Procedimeinto SINATEC PSTFOLIODOCSATLOTEFIN_RG--------");
            int i = sp.sp_fin(LoteProyecto, noArch);
            System.out.println(i == 0 ? "--Procedimiento OK--" : "Error al ejecutar el procedimiento Número de archivos reportados " + vContador + " archivos contabilizados " + noArch);
            if (i == 0) {
            	competenciaEval(vFolio);
                miaDao.guardarAvance(getProy(), new Short("1"), "0");
                getProy().setEstatusProyecto("C");
                miaDao.merge(getProy());
                //Proceso de captura completo
                reqcontEnv.execute("alert('Ha completado el registro de datos de la Manifestación de Impacto Ambiental, ahora puede firmar su trámite')");
                System.out.println("Se ha completado el registro de datos de la Manifestación de Impacto Ambiental");
            } else {
                reqcontEnv.execute("alert('No se ha completado el registro, intente de nuevo más tarde')");
                System.out.println("No se ha completado el registro de datos de la Manifestación de Impacto Ambiental");
            }

        } else {
            reqcontEnv.execute("alert('El tramite no cuenta con ningun adjunto, no es posible cerrarlo')");
            System.out.println("El tramite no cuenta con ningun adjunto, no es posible cerrarlo");
        }
    }//</editor-fold>
    
    @SuppressWarnings("unused")
	public void simulacroGeneraReporte() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        String vFolio = getProy().getProyectoPK().getFolioProyecto();
        short vSerial = getProy().getProyectoPK().getSerialProyecto();
        
        generaReporteResumen(vFolio, vSerial);

    }
    private void generaReporteResumen(String folio, short serial) {
        try {
            CatParametro c = new CatParametro();
            CatParametro d = new CatParametro();
            c = getCaracPdf("ADJUNTOS");
            System.out.println(c.getParRuta());
            ReportFactory r = new ReportFactory();
            byte[] pdf;
            pdf = r.pdf(folio, serial,"http://appsdev.semarnat.gob.mx:8080/ManifestacionImpactoAmbiental/"); // DESARROLLO
            //pdf = r.pdf(folio, serial,"https://mia.semarnat.gob.mx:8443/ManifestacionImpactoAmbiental/"); // PRODUCCION
            
            String UPLOAD_DIRECTORY = c.getParRuta();
            String nombreArchivo = "";
            String nombreBD = "";
            String descripcion = "";
            //String UPLOAD_DIRECTORY = "C:/wars/";
            if(serial==1){
            	c = getCaracPdf("PDF_INGRESO_MIA");
            	nombreArchivo = c.getParRuta()+".pdf";
            	nombreBD = c.getParRuta();
            	d = getCaracPdf("DESCRIPCION_PDF_INGRESO_MIA");
            	descripcion = d.getParRuta();
            }else{
            	c = getCaracPdf("PDF_INFOADIC_MIA");
            	nombreArchivo = c.getParRuta()+".pdf";
            	nombreBD = c.getParRuta();
            	d = getCaracPdf("DESCRIPCION_PDF_INGRESO_MIA");
            	descripcion = d.getParRuta();
            }
            System.out.println("Nombre del archivo: "+nombreArchivo);
            System.out.println("Descripcion del archivo: "+descripcion);
            String ruta = UPLOAD_DIRECTORY + folio + "/" + nombreArchivo;
            System.out.println(pdf.length);
            double bytes = pdf.length;
			double kilobytes = (bytes / 1024);
            Short idAnexo = visorDao.numRegAnexo((short)0, (short)0, (short)0, (short)0, folio, serial);
            ArchivosProyecto infoFinal = new ArchivosProyecto(folio,serial,(short)0, (short)0, (short)0, (short)0);
            infoFinal.setId(idAnexo);
            infoFinal.setNombre(nombreBD);
            infoFinal.setDescripcion(descripcion);
            infoFinal.setFileName(nombreBD);
            infoFinal.setUrl(ruta);
            infoFinal.setTamanio(new BigDecimal(kilobytes));
            infoFinal.setExtension("pdf");
            infoFinal.setReservado("0");
            VisorDao.persist(infoFinal);
            
            FileOutputStream fos = new FileOutputStream(ruta);
            fos.write(pdf);
            fos.close();
        
        } catch (Exception e) {
            System.out.println("Ocurrió un error durante la generación del reporte de resumen");
            e.printStackTrace();
        }
    }
    
    private CatParametro getCaracPdf(String parDescripcion){
    	EntityManager em = EntityFactory.getMiaMF().createEntityManager();
        Query q = em.createQuery("SELECT a FROM CatParametro a WHERE a.parDescripcion = '"+ parDescripcion +"'");
        CatParametro c = (CatParametro) q.getSingleResult();
        return c;
    }
    
    /*Metodo para definir unidad responsable de la evaluación de proyecto, obedeciendo las siguientes condiciones:
     * - Si el proyecto es una MIA - P mod. A y entra en alguno de los siguientes supuestos:
     *   * El número de estados afectados es mayor o igual a 2
     *   * El promovente es una entidad de gobierno
     *   * Debido a las sustancias reportadas requiere un estudio de riesgo
     *  Deberá ser evaluado por la entidad 09 de lo contrario tomará el valor del estado más afectado.
     *  - Los proyectos MIA - P mod. B y MIA - R (no importando la modalidad) deberán ser evaluados en la entidad 09*/
    private void competenciaEval(String folio){
    	int numEstados = sinDao.soloEstadosAfectados(getProy().getProyectoPK().getFolioProyecto(),getProy().getClaveProyecto(), getProy().getProyectoPK().getSerialProyecto());
    	tipoTramite = miaDao.tipotramite(Integer.valueOf(folio));
    	promovente = miaDao.getPromovente(folio);
    	if(!tipoTramite.isEmpty()){
    		if( tipoTramite.equalsIgnoreCase("MP")){
        		if(numEstados>=2 || promovente.getBtentidadgobierno().intValue()==1 || getProy().getEstudioRiesgoId()==1){
        			proy.setProyCompetencia("09");
        			System.out.println("La evaluación del trámite es responsabilidad de la DGIRA");
        		}
        		else{
        			proy.setProyCompetencia(getProy().getProyEntAfectado());
    				System.out.println("La evaluación es responsabilidad de la delegación");
        		}
    		}else{
    			proy.setProyCompetencia("09");
    			System.out.println("La evaluación del trámite es responsabilidad de la DGIRA");
    		}
    	}else{
    		System.out.println("no pude evaluar el tipo de tramite");
    	}
    	try {
			miaDao.merge(proy);
		} catch (Exception e) {
			System.out.println("Ocurrió un error al almacenar la unidad de gestión");
            e.printStackTrace();
		}
    }

    /**
     * @return the proy
     */
    public Proyecto getProy() {
        return proy;
    }

    public String getNombreSubsector() {
        return nombreSubsector;
    }

    public void setNombreSubsector(String nombreSubsector) {
        this.nombreSubsector = nombreSubsector;
    }

    public String getNombrePromovente() {
        return nombrePromovente;
    }

    public void setNombrePromovente(String nombrePromovente) {
        this.nombrePromovente = nombrePromovente;
    }

    /**
     * @return the estatusProyecto
     */
    public Boolean getEstatusProyecto() {
        return estatusProyecto;
    }

    /**
     * @param estatusProyecto the estatusProyecto to set
     */
    public void setEstatusProyecto(Boolean estatusProyecto) {
        this.estatusProyecto = estatusProyecto;
    }

    public HashMap<String, boolean[]> getMapInfoAdicional() {
        return mapInfoAdicional;
    }

    public void setMapInfoAdicional(HashMap<String, boolean[]> mapInfoAdicional) {
        this.mapInfoAdicional = mapInfoAdicional;
    }

    public String getMsgCierreTramite() {
         
        String bitacora = miaDao.obtenerBitacoraDeFolio(proy.getProyectoPK().getFolioProyecto());
        if ( miaDao.apartadosSinCapturar(bitacora) ) {
            msgCierreTramite = "Existen apartados que fueron solicitados pero no han sido capturados, su información se enviará y ya no podrá modificarla";
        } else {
            msgCierreTramite = "Sus datos se enviarán y ya no podrán modificarse";
        }
        
        return msgCierreTramite;
    }
    
    public boolean isExistenDatosGenerales() {
        return existenDatosGenerales;
    }
    
    public void refresh() {
        FacesContext context = FacesContext.getCurrentInstance();
        String viewId = context.getViewRoot().getViewId();
        ViewHandler handler = context.getApplication().getViewHandler();
        UIViewRoot root = handler.createView(context, viewId);
        root.setViewId(viewId);
        context.setViewRoot(root);        
    }

	/**
	 * @return the reia
	 */
	public boolean isReia() {
		return reia;
	}

	/**
	 * @param reia the reia to set
	 */
	public void setReia(boolean reia) {
		this.reia = reia;
	}

	public short getSustanciasExceden() {
		return sustanciasExceden;
	}

	public void setSustanciasExceden(short sustanciasExceden) {
		this.sustanciasExceden = sustanciasExceden;
	}

	
    
    
}
