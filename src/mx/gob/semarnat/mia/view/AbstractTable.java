/*
 * @(#)AbstractBaseDAO.java 1.0 29/10/2014
 * 
 * Seguridad Tabasco
 */
package mx.gob.semarnat.mia.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.component.UIData;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;

import mx.gob.semarnat.mia.view.capitulo1.PaginationHelper;

/**
 * Clase abstracta que define los metodos de un dao.
 *
 * @author jponce
 * @param <T> the generic type
 */
public abstract class AbstractTable<T>  extends Cap {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5916336934431541810L;
	
	protected static final int UNO= 1;
	protected static final int CERO= 0;
	
	
	List<T> listTable;
	
	/****************************/
	/****************************/
    boolean selectAllRow;
    
    protected UIData dataTable;	
 

	/**
	 * @return the reiaProyObrasDataTable
	 */
	public UIData getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable the reiaProyObrasDataTable to set
	 */
	public void setDataTable(UIData dataTable) {
		this.dataTable = dataTable;
	}


	protected void loadListaPaginas() {
		int numpag=listTable!=null && !listTable.isEmpty()?listTable.size():CERO;
		listaPaginas= new ArrayList<Integer>();
		int numPags=numpag/NUM_MAX_PAG;
		numPags=numPags+(numpag%NUM_MAX_PAG>CERO?UNO:CERO);
		 for(int i=CERO;i<numPags;i++){
			 listaPaginas.add(i);
		 }
	}

	/**
	 * @return the selectAllRow
	 */
	public boolean isSelectAllRow() {
		return selectAllRow;
	}

	/**
	 * @param selectAllRow the selectAllRow to set
	 */
	public void setSelectAllRow(boolean selectAllRow) {
		this.selectAllRow = selectAllRow;
	}




	private Map<T,Boolean> rowSelected = new HashMap<T, Boolean>(){

		private static final long serialVersionUID = -336083889678124999L;

		@Override
		public Boolean get(Object object) {
			if (isSelectAllRow()){
				rowSelected.put((T) object , Boolean.TRUE);
				return Boolean.TRUE;
			}
			if (!containsKey(object)){
				return Boolean.FALSE;	
			}
			return super.get(object);
		};

	};
    
	
	
	
    /**
	 * @return the caractTOSelected
	 */
	public Map<T, Boolean> getRowSelected() {
		return rowSelected;
	}


	public void selectAllRowListener(ValueChangeEvent ae){
		System.out.println("******************************* init selectAllRowListener");
		selectAllRow = ((Boolean) ae.getNewValue()).booleanValue();
		if (!selectAllRow){
			clearEntitiesSelected();
		}
		System.out.println("******************************* end selectAllRowListener");
	}
    
    
    public void clearEntitiesSelected(){
		selectAllRow = false;
		rowSelected.clear();
	}
    
    public List<T> getSelectedRow(){
		if (isSelectAllRow()){
			return listTable;
		}
		final List<T> result = new ArrayList<T>();
	    final Iterator<?> iterator = rowSelected.keySet().iterator();
	    while (iterator.hasNext()) {
	    	T key = (T) iterator.next();
	    	if (rowSelected.get(key)){	
	    		result.add(key);
	    	}
	    }
	    
		return result;
	}

	public void selectRowListener(ValueChangeEvent event){
		System.out.println("******************************* init selectRowListener");
 		selectAllRow = false;
 		rowSelected.put((T)dataTable.getRowData(), (Boolean) event.getNewValue());
	}
		
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	

	protected static final int NUM_MAX_PAG=10;
	protected PaginationHelper pagination;
	protected int selectedItemIndex;
	@SuppressWarnings("rawtypes")
	protected DataModel dtmdl = null;
	
	abstract  PaginationHelper getPagination() ;
	
	
	@SuppressWarnings("rawtypes")
	public DataModel getdtmdl() {
        if (dtmdl == null) {
            dtmdl = getPagination().createPageDataModel();
        }
        return dtmdl;
    }
    private void recreateModel() {
        dtmdl = null;
    }

    @SuppressWarnings("unused")
	private void recreatePagination() {
        pagination = null;
    }

    @SuppressWarnings("unused")
	abstract void updateCurrentItem() ;
    
    public void next() {    	
        getPagination().nextPage();
        recreateModel();
    }

    public void inPage(int ini){
    	getPagination().setPage(ini);
        recreateModel();
    }
    
    public void previous() {
        getPagination().previousPage();
        recreateModel();
    }
    
    
    private List<Integer> listaPaginas;


	/**
	 * @return the listaPaginas
	 */
	public List<Integer> getListaPaginas() {
		return listaPaginas;
	}

	/**
	 * @param listaPaginas the listaPaginas to set
	 */
	public void setListaPaginas(List<Integer> listaPaginas) {
		this.listaPaginas = listaPaginas;
	}
	
    
}
