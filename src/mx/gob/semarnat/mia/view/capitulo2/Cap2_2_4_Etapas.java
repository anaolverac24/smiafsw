/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view.capitulo2;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import mx.gob.semarnat.mia.dao.CargaBean;
import mx.gob.semarnat.mia.dao.ExplosivoActividadEtapaDAO;
import mx.gob.semarnat.mia.model.ActividadEtapa;
import mx.gob.semarnat.mia.model.ArchivosProyecto;
import mx.gob.semarnat.mia.view.Cap;

import org.primefaces.context.RequestContext;

/**
 *
 * @author EdgarSC
 */
@ManagedBean(name = "cap2Etapas")
@ViewScoped
public class Cap2_2_4_Etapas extends Cap {

	private ExplosivoActividadEtapaDAO explosivoDao = new ExplosivoActividadEtapaDAO();
	private List<ActividadEtapa> actividadesPrepara = new ArrayList<ActividadEtapa>();
	private List<ActividadEtapa> activPrep_seleccionados = new ArrayList<ActividadEtapa>();
	private List<ActividadEtapa> actividadesConstru = new ArrayList<ActividadEtapa>();
	private List<ActividadEtapa> activCons_seleccionados = new ArrayList<ActividadEtapa>();
	private List<ActividadEtapa> actividadesOperaci = new ArrayList<ActividadEtapa>();
	private List<ActividadEtapa> activOper_seleccionados = new ArrayList<ActividadEtapa>();
	private List<ActividadEtapa> actividadesAbandon = new ArrayList<ActividadEtapa>();
	private List<ActividadEtapa> activAban_seleccionados = new ArrayList<ActividadEtapa>();
	private Boolean verPreparacion = false;
	private Boolean verConstruccion = false;
	private Boolean verOperacion = false;
	private Boolean verAbandono = false;
	private Boolean verMsgEtapa = false;
	private final String folioProyecto;
	private final Short serialProyecto;
	private Short id = null;

	private ActividadEtapa selectedActividad;
	private ActividadEtapa editActividad;

	private ArchivosProyecto archivoTemporal;
	private ArchivosProyecto archivoEdicion;
    private CargaBean cargaBean;
    
    private List<ArchivosProyecto> archivosPrepara = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> archivosPrep_seleccionados = new ArrayList<ArchivosProyecto>();
    
    private List<ArchivosProyecto> archivosConstru = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> archivosCons_seleccionados = new ArrayList<ArchivosProyecto>();
    
    private List<ArchivosProyecto> archivosOperaci = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> archivosOper_seleccionados = new ArrayList<ArchivosProyecto>();
    
    private List<ArchivosProyecto> archivosAbandon = new ArrayList<ArchivosProyecto>();
    private List<ArchivosProyecto> archivosAban_seleccionados = new ArrayList<ArchivosProyecto>();
    
    
    boolean explosivosPrep;
    boolean explosivosCons;
    boolean explosivosOper;
    boolean explosivosAban;
    
    private String msj;
	private boolean bandera;
	private long indexRow = 0;
	
	// Sección del folio, dependiendo del subsector
	Short seccion;
	
	// Id secuencial del registro a editar
	int rowIndexEditar;
	

	/**
	 * Creates a new instance of Cap2_2_4_Etapas
	 */
	public Cap2_2_4_Etapas() {
		System.out.println("Capitulo 2_2_4 View");
		Short subsector = getProyecto().getNsub();
		
		seccion = (subsector == 4 || subsector == 6) ? (short)5 : (short)4;
		
		selectedActividad = new ActividadEtapa();
		editActividad = new ActividadEtapa();
		this.cargaBean = new CargaBean();
		
		archivoTemporal = new ArchivosProyecto();
		archivoEdicion = new ArchivosProyecto();

		folioProyecto = this.getProyecto().getProyectoPK().getFolioProyecto();
		serialProyecto = this.getProyecto().getProyectoPK().getSerialProyecto();

		actividadesPrepara = getMiaDao().getActividadesEtapa(folioProyecto, serialProyecto, (short) 1);
		actividadesConstru = getMiaDao().getActividadesEtapa(folioProyecto, serialProyecto, (short) 2);
		actividadesOperaci = getMiaDao().getActividadesEtapa(folioProyecto, serialProyecto, (short) 3);
		actividadesAbandon = getMiaDao().getActividadesEtapa(folioProyecto, serialProyecto, (short) 4);
		
		archivosPrepara = getMiaDao().getArchivosProyecto(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 1);
		archivosConstru = getMiaDao().getArchivosProyecto(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 2);
		archivosOperaci = getMiaDao().getArchivosProyecto(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 3);
		archivosAbandon = getMiaDao().getArchivosProyecto(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 4);

		verPreparacion = true;
		verConstruccion = true;
		verOperacion = true;
		verAbandono = true;
	}

	public void agregarEtapa(ActionEvent evt) {
		System.out.println("Agregando Actividad");
		String tipo = (String) evt.getComponent().getAttributes().get("tipo");
		Short idEtapa = 1;
		System.out.println("Entrando en los IF´s");
		if (tipo.equals("preparacion")) {
			idEtapa = 1;
		}
		if (tipo.equals("construccion")) {
			idEtapa = 2;
		}
		if (tipo.equals("operacion")) {
			idEtapa = 3;
		}
		if (tipo.equals("abandono")) {
			idEtapa = 4;
		}

		System.out.println("Calculando ID anterior");
		System.out.println("Datos Nuevos: " + selectedActividad.getActividadNombre() + ", "
				+ selectedActividad.getActividadDescripcion() + ", " + idEtapa);
		if (id == null) {
			id = getMiaDao().getMaxActividadEtapaProyecto(folioProyecto, serialProyecto, idEtapa);
			System.out.println("Registro: #" + id);
		} else {
			id++;
			System.out.println("Registro: #" + id);
		}
		System.out.println("Inicializando Actividad");
		ActividadEtapa ae = new ActividadEtapa(folioProyecto, serialProyecto, idEtapa, id);
		ae.setActividadNombre(selectedActividad.getActividadNombre());
		ae.setActividadDescripcion(selectedActividad.getActividadDescripcion());
		System.out.println("IF´s para determinar el tipo de etapa");
		if (tipo.equals("preparacion")) {
			System.out.println("Actividad Etapa de tipo Preparación");
			try {
				System.out.println("MiaDao Persist!");
				miaDao.persist(ae);
				selectedActividad = new ActividadEtapa();
				RequestContext context = RequestContext.getCurrentInstance();
				System.out.println("Cerrando Modal Agregar y abriendo Modal Aviso");
				context.execute("modalAgregarP.hide();modalAlertP.show()");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Actividad Preparación Add");
			actividadesPrepara.add(ae);
		}
		if (tipo.equals("construccion")) {
			System.out.println("Actividad Etapa de tipo Construcción");			
			try {
				System.out.println("MiaDao Persist!");
				miaDao.persist(ae);
				selectedActividad = new ActividadEtapa();
				RequestContext context = RequestContext.getCurrentInstance();
				System.out.println("Cerrando Modal Agregar y abriendo Modal Aviso");
				context.execute("modalAgregarC.hide();modalAlertC.show()");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Actividad Construcción Add");
			actividadesConstru.add(ae);
		}
		if (tipo.equals("operacion")) {
			System.out.println("Actividad Etapa de tipo Operación");			
			try {
				System.out.println("MiaDao Persist!");
				miaDao.persist(ae);
				selectedActividad = new ActividadEtapa();
				RequestContext context = RequestContext.getCurrentInstance();
				System.out.println("Cerrando Modal Agregar y abriendo Modal Aviso");
				context.execute("modalAgregarO.hide();modalAlertO.show()");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Actividad Operación Add");
			actividadesOperaci.add(ae);
		}
		if (tipo.equals("abandono")) {
			System.out.println("Actividad Etapa de tipo Abandono");			
			try {
				System.out.println("MiaDao Persist!");
				miaDao.persist(ae);
				selectedActividad = new ActividadEtapa();
				RequestContext context = RequestContext.getCurrentInstance();
				System.out.println("Cerrando Modal Agregar y abriendo Modal Aviso");
				context.execute("modalAgregarA.hide();modalAlertA.show()");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Actividad Abandono Add");
			actividadesAbandon.add(ae);
		}		
		System.out.println("Fin");
	}

	/**
	 * Editar una actividad preparación ya existente
	 */
	public void consultarActividadPrep() {	    
		System.out.println("Consultando actividad Prep.Sitio");
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idActividadPrep = params.get("id").toString();
		id = Integer.parseInt(idActividadPrep);
		System.out.println("Actualizando actividad: " + id);
		editActividad = new ActividadEtapa();
		
		// Número secuencial a mostrar en "Editar registro #"
		rowIndexEditar = Integer.parseInt(params.get("pRowIndexActPS").toString());
		
		editActividad = miaDao.getActividad(folioProyecto, serialProyecto, (short) 1, (short) id);
		System.out.println("Fin de la Consulta actividad Prep.Sitio");
	}
	
	public void consultarActividadCons() {
		System.out.println("Consultando");
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idActividadCons = params.get("id").toString();
		id = Integer.parseInt(idActividadCons);
		System.out.println("Actualizando actividad: " + id);
		editActividad = new ActividadEtapa();
		
		// Número secuencial a mostrar en "Editar registro #"
		rowIndexEditar = Integer.parseInt(params.get("pRowIndexActC").toString());
		
		editActividad = miaDao.getActividad(folioProyecto, serialProyecto, (short) 2, (short) id);
		System.out.println("Fin de la Consulta");
	}
	
	public void consultarActividadOper() {
		System.out.println("Consultando");
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idActividadOper = params.get("id").toString();
		id = Integer.parseInt(idActividadOper);
		System.out.println("Actualizando actividad: " + id);
		editActividad = new ActividadEtapa();
		
		// Número secuencial a mostrar en "Editar registro #"
		rowIndexEditar = Integer.parseInt(params.get("pRowIndexActOM").toString());
		
		editActividad = miaDao.getActividad(folioProyecto, serialProyecto, (short) 3, (short) id);
		System.out.println("Fin de la Consulta");
	}
	
	public void consultarActividadAban() {
		System.out.println("Consultando");
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idActividadAban = params.get("id").toString();
		id = Integer.parseInt(idActividadAban);
		System.out.println("Actualizando actividad: " + id);
		editActividad = new ActividadEtapa();
		
		// Número secuencial a mostrar en "Editar registro #"
		rowIndexEditar = Integer.parseInt(params.get("pRowIndexActA").toString());
		
		editActividad = miaDao.getActividad(folioProyecto, serialProyecto, (short) 4, (short) id);
		System.out.println("Fin de la Consulta");
	}

	public void editarActividadPrep() {
		ActividadEtapa ae = new ActividadEtapa();
		ae = editActividad;
		try {
			miaDao.persist(ae);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditarAP.hide();modalAlertEAP.show()");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void editarActividadCons() {
		ActividadEtapa ae = new ActividadEtapa();
		ae = editActividad;
		try {
			miaDao.persist(ae);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditarAC.hide();modalAlertEAC.show()");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void editarActividadOper() {
		ActividadEtapa ae = new ActividadEtapa();
		ae = editActividad;
		try {
			miaDao.persist(ae);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditarAO.hide();modalAlertEAO.show()");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void editarActividadAban() {
		ActividadEtapa ae = new ActividadEtapa();
		ae = editActividad;
		try {
			miaDao.persist(ae);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditarAA.hide();modalAlertEAA.show()");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void eliminarEtapa(ActionEvent evt) {
		Short id = (Short) evt.getComponent().getAttributes().get("idt");
		String tipo = (String) evt.getComponent().getAttributes().get("tipo");
		Short idEtapa = 1;

		if (tipo.equals("preparacion")) {
			idEtapa = 1;
		}
		if (tipo.equals("construccion")) {
			idEtapa = 2;
		}
		if (tipo.equals("operacion")) {
			idEtapa = 3;
		}
		if (tipo.equals("abandono")) {
			idEtapa = 4;
		}

		ActividadEtapa ae = new ActividadEtapa(folioProyecto, serialProyecto, idEtapa, id);

		try {
			getMiaDao().eliminarActividadEtapa(folioProyecto, serialProyecto, idEtapa, id);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (tipo.equals("preparacion")) {
			actividadesPrepara.remove(ae);
		}
		if (tipo.equals("construccion")) {
			actividadesConstru.remove(ae);
		}
		if (tipo.equals("operacion")) {
			actividadesOperaci.remove(ae);
		}
		if (tipo.equals("abandono")) {
			actividadesAbandon.remove(ae);
		}
	}

	public String eliminaActividadesPrep() throws Exception {
		System.out.println("Eliminación Afirmativa");
		short explosivos = 0;
		for (ActividadEtapa activPrepSeleccionado : activPrep_seleccionados) {
			actividadesPrepara.remove(activPrepSeleccionado);
			explosivos = miaDao.countExplosivos(folioProyecto, serialProyecto, (short) 1, activPrepSeleccionado.getActividadEtapaPK().getActividadEtapaId());
			System.out.println("El Registro tiene: " + explosivos + " hijos");			
			if(explosivos != 0){
				explosivoDao.eliminarExplosivoActividadEtapaHijo(folioProyecto, serialProyecto, (short) 1, activPrepSeleccionado.getActividadEtapaPK().getActividadEtapaId());
			}
			miaDao.eliminarActividades(folioProyecto, serialProyecto, (short) 1, activPrepSeleccionado.getActividadEtapaPK().getActividadEtapaId());
		}
		return "etapaProyecto";
	}		
	
	public String eliminaActividadesCons() throws Exception {
		System.out.println("Eliminación Afirmativa");
		short explosivos = 0;
		for (ActividadEtapa activConsSeleccionado : activCons_seleccionados) {
			actividadesPrepara.remove(activConsSeleccionado);
			explosivos = miaDao.countExplosivos(folioProyecto, serialProyecto, (short) 2, activConsSeleccionado.getActividadEtapaPK().getActividadEtapaId());
			System.out.println("El Registro tiene: " + explosivos + " hijos");
			if(explosivos != 0){
				explosivoDao.eliminarExplosivoActividadEtapaHijo(folioProyecto, serialProyecto, (short) 2, activConsSeleccionado.getActividadEtapaPK().getActividadEtapaId());
			}
			miaDao.eliminarActividades(folioProyecto, serialProyecto, (short) 2, activConsSeleccionado.getActividadEtapaPK().getActividadEtapaId());
		}
		return "etapaProyecto";
	}
	
	public String eliminaActividadesOper() throws Exception {
		System.out.println("Eliminación Afirmativa");
		short explosivos = 0;
		for (ActividadEtapa activOperSeleccionado : activOper_seleccionados) {
			actividadesPrepara.remove(activOperSeleccionado);
			explosivos = miaDao.countExplosivos(folioProyecto, serialProyecto, (short) 3, activOperSeleccionado.getActividadEtapaPK().getActividadEtapaId());
			System.out.println("El Registro tiene: " + explosivos + " hijos");
			if(explosivos != 0){
				explosivoDao.eliminarExplosivoActividadEtapaHijo(folioProyecto, serialProyecto, (short) 3, activOperSeleccionado.getActividadEtapaPK().getActividadEtapaId());
			}
			miaDao.eliminarActividades(folioProyecto, serialProyecto, (short) 3, activOperSeleccionado.getActividadEtapaPK().getActividadEtapaId());
		}
		return "etapaProyecto";
	}
	
	public String eliminaActividadesAban() throws Exception {
		System.out.println("Eliminación Afirmativa");
		short explosivos = 0;
		for (ActividadEtapa activAbanSeleccionado : activAban_seleccionados) {
			actividadesPrepara.remove(activAbanSeleccionado);
			explosivos = miaDao.countExplosivos(folioProyecto, serialProyecto, (short) 4, activAbanSeleccionado.getActividadEtapaPK().getActividadEtapaId());
			System.out.println("El Registro tiene: " + explosivos + " hijos");
			if(explosivos != 0){
				explosivoDao.eliminarExplosivoActividadEtapaHijo(folioProyecto, serialProyecto, (short) 4, activAbanSeleccionado.getActividadEtapaPK().getActividadEtapaId());
			}
			miaDao.eliminarActividades(folioProyecto, serialProyecto, (short) 4, activAbanSeleccionado.getActividadEtapaPK().getActividadEtapaId());
		}
		return "etapaProyecto";
	}

	public String tieneActividadesPrepSeleccionados() {
		System.out.println("Comprobando Chechbox´s");
		System.out.println(activPrep_seleccionados.size() + " seleccionados");
		if (!activPrep_seleccionados.isEmpty()) {			
			short explosivos = 0;
			for (ActividadEtapa activPrepSeleccionado : activPrep_seleccionados) {
				explosivos = miaDao.countExplosivos(folioProyecto, serialProyecto, (short) 1, activPrepSeleccionado.getActividadEtapaPK().getActividadEtapaId());
				System.out.println("Los registros tienen: " + explosivos + " hijos");
				if(explosivos > 0){
					break;
				}
			}
			if (explosivos > 0) {
				this.explosivosPrep = true;
			} else {
				this.explosivosPrep = false;
			}
			System.out.println("Los registros tienen hijos: " + this.explosivosPrep);
			RequestContext context = RequestContext.getCurrentInstance();
			System.out.println("Abriendo modal de confirmación");
			context.execute("eliminarActivPrep.show()");
		}
		System.out.println("Saliendo del IF");
		return "etapaProyecto";
	}
	
	public String tieneActividadesConsSeleccionados() {
		System.out.println("Comprobando Chechboxs");
		System.out.println(activCons_seleccionados.size() + " seleccionados");
		
		if (!activCons_seleccionados.isEmpty()) {
			short explosivos = 0;
			for (ActividadEtapa activConsSeleccionado : activCons_seleccionados) {
				explosivos = miaDao.countExplosivos(folioProyecto, serialProyecto, (short) 2, activConsSeleccionado.getActividadEtapaPK().getActividadEtapaId());
				System.out.println("Los registros tienen: " + explosivos + " hijos");
				if(explosivos > 0){
					break;
				}
			}
			if (explosivos > 0) {
				this.explosivosCons = true;
			} else {
				this.explosivosCons = false;
			}
			System.out.println("Los registros tienen hijos: " + this.explosivosCons);
			RequestContext context = RequestContext.getCurrentInstance();
			System.out.println("Abriendo modal de confirmación");
			context.execute("eliminarActivCons.show()");
		}
		System.out.println("Saliendo del IF");
		return "etapaProyecto";
	}
	
	public String tieneActividadesOperSeleccionados() {
		System.out.println("Comprobando Chechbox´s");
		System.out.println(activOper_seleccionados.size() + " seleccionados");
		if (!activOper_seleccionados.isEmpty()) {
			short explosivos = 0;
			for (ActividadEtapa activOperSeleccionado : activOper_seleccionados) {
				explosivos = miaDao.countExplosivos(folioProyecto, serialProyecto, (short) 3, activOperSeleccionado.getActividadEtapaPK().getActividadEtapaId());
				System.out.println("Los registros tienen: " + explosivos + " hijos");
				if(explosivos > 0){
					break;
				}
			}
			if (explosivos > 0) {
				this.explosivosOper = true;
			} else {
				this.explosivosOper = false;
			}
			System.out.println("Los registros tienen hijos: " + this.explosivosOper);
			RequestContext context = RequestContext.getCurrentInstance();
			System.out.println("Abriendo modal de confirmación");
			context.execute("eliminarActivOper.show()");
		}
		System.out.println("Saliendo del IF");
		return "etapaProyecto";
	}
	
	public String tieneActividadesAbanSeleccionados() {
		System.out.println("Comprobando Chechbox´s");
		System.out.println(activAban_seleccionados.size() + " seleccionados");
		if (!activAban_seleccionados.isEmpty()) {
			short explosivos = 0;
			for (ActividadEtapa activAbanSeleccionado : activAban_seleccionados) {
				explosivos = miaDao.countExplosivos(folioProyecto, serialProyecto, (short) 4, activAbanSeleccionado.getActividadEtapaPK().getActividadEtapaId());
				System.out.println("Los registros tienen: " + explosivos + " hijos");
				if(explosivos > 0){
					break;
				}
			}
			if (explosivos > 0) {
				this.explosivosAban = true;
			} else {
				this.explosivosAban = false;
			}
			System.out.println("Los registros tienen hijos: " + this.explosivosAban);
			RequestContext context = RequestContext.getCurrentInstance();
			System.out.println("Abriendo modal de confirmación");
			context.execute("eliminarActivAban.show()");
		}
		System.out.println("Saliendo del IF");
		return "etapaProyecto";
	}
	
	public String eliminaArchivosPrep() throws Exception {
		System.out.println("Eliminación Afirmativa");		
		for (ArchivosProyecto archivosPrepSeleccionado : archivosPrep_seleccionados) {
			archivosPrepara.remove(archivosPrepSeleccionado);			
			miaDao.eliminarArchivosEtapas(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 1, archivosPrepSeleccionado.getId(), archivosPrepSeleccionado.getSeqId());
		}
		archivosPrepara = getMiaDao().getArchivosProyecto(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 1);
		return "etapaProyecto";
	}
	
	public String tieneArchivosPrepSeleccionados() {
		System.out.println("Comprobando Chechbox´s");
		System.out.println(archivosPrep_seleccionados.size() + " seleccionados");
		if (!archivosPrep_seleccionados.isEmpty()) {
			RequestContext context = RequestContext.getCurrentInstance();
			System.out.println("Abriendo modal de confirmación");
			context.execute("eliminarArchPrep.show()");
		}
		System.out.println("Saliendo del IF");
		return "etapaProyecto";
	}
	
	public void consultarArchivoPrep() {
		System.out.println("Consultando");
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idArchivoPrep = params.get("id").toString();
		id = Integer.parseInt(idArchivoPrep);
		int rowIndex = 0;
		String pRowIndex = params.get("pRowIndex").toString();
		rowIndex = Integer.parseInt(pRowIndex);
		System.out.println("Actualizando archivo: " + id);
		archivoEdicion = new ArchivosProyecto();
		archivoEdicion = miaDao.getArchivoEtapa(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion,(short) 1, (short) id);
		System.out.println("Fin de la Consulta");
		indexRow = rowIndex;
	}
	
	public void editarArchivoPrep() {
		ArchivosProyecto ar = new ArchivosProyecto();
		ar = archivoEdicion;
		try {
			miaDao.persist(ar);
			
			archivosPrepara = getMiaDao().getArchivosProyecto(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 1);
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditarArchP.hide();modalAlertEdArchP.show()");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String eliminaArchivosCons() throws Exception {
		System.out.println("Eliminación Afirmativa");		
		for (ArchivosProyecto archivosConsSeleccionado : archivosCons_seleccionados) {
			archivosConstru.remove(archivosConsSeleccionado);			
			miaDao.eliminarArchivosEtapas(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 2, archivosConsSeleccionado.getId(), archivosConsSeleccionado.getSeqId());
		}
		archivosConstru = getMiaDao().getArchivosProyecto(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 2);
		return "etapaProyecto";
	}
	
	public String tieneArchivosConsSeleccionados() {
		System.out.println("Comprobando Chechbox´s");
		System.out.println(archivosCons_seleccionados.size() + " seleccionados");
		if (!archivosCons_seleccionados.isEmpty()) {
			RequestContext context = RequestContext.getCurrentInstance();
			System.out.println("Abriendo modal de confirmación");
			context.execute("eliminarArchCons.show()");
		}
		System.out.println("Saliendo del IF");
		return "etapaProyecto";
	}
	
	public void consultarArchivoCons() {
		System.out.println("Consultando");
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idArchivoCons = params.get("id").toString();
		id = Integer.parseInt(idArchivoCons);
		int rowIndex = 0;
		String pRowIndex = params.get("pRowIndex").toString();
		rowIndex = Integer.parseInt(pRowIndex);
		System.out.println("Actualizando archivo: " + id);
		archivoEdicion = new ArchivosProyecto();
		archivoEdicion = miaDao.getArchivoEtapa(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion,(short) 2, (short) id);
		System.out.println("Fin de la Consulta");
		indexRow = rowIndex;
	}
	
	public void editarArchivoCons() {
		ArchivosProyecto ar = new ArchivosProyecto();
		ar = archivoEdicion;
		try {
			miaDao.persist(ar);
			archivosConstru = getMiaDao().getArchivosProyecto(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 2);
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditarArchC.hide();modalAlertEdArchC.show()");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String eliminaArchivosOper() throws Exception {
		System.out.println("Eliminación Afirmativa");		
		for (ArchivosProyecto archivosOperSeleccionado : archivosOper_seleccionados) {
			archivosOperaci.remove(archivosOperSeleccionado);			
			miaDao.eliminarArchivosEtapas(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 3, archivosOperSeleccionado.getId(), archivosOperSeleccionado.getSeqId());
		}
		archivosOperaci = getMiaDao().getArchivosProyecto(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 3);
		return "etapaProyecto";
	}
	
	public String tieneArchivosOperSeleccionados() {
		System.out.println("Comprobando Chechbox´s");
		System.out.println(archivosOper_seleccionados.size() + " seleccionados");
		if (!archivosOper_seleccionados.isEmpty()) {
			RequestContext context = RequestContext.getCurrentInstance();
			System.out.println("Abriendo modal de confirmación");
			context.execute("eliminarArchOper.show()");
		}
		System.out.println("Saliendo del IF");
		return "etapaProyecto";
	}
	
	public void consultarArchivoOper() {
		System.out.println("Consultando");
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idArchivoOper = params.get("id").toString();
		id = Integer.parseInt(idArchivoOper);
		int rowIndex = 0;
		String pRowIndex = params.get("pRowIndex").toString();
		rowIndex = Integer.parseInt(pRowIndex);
		System.out.println("Actualizando archivo: " + id);
		archivoEdicion = new ArchivosProyecto();
		archivoEdicion = miaDao.getArchivoEtapa(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 3, (short) id);
		System.out.println("Fin de la Consulta");
		indexRow = rowIndex;
	}
	
	public void editarArchivoOper() {
		ArchivosProyecto ar = new ArchivosProyecto();
		ar = archivoEdicion;
		try {
			miaDao.persist(ar);
			
			archivosOperaci = getMiaDao().getArchivosProyecto(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 3);
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditarArchO.hide();modalAlertEdArchO.show()");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String eliminaArchivosAban() throws Exception {
		System.out.println("Eliminación Afirmativa");		
		for (ArchivosProyecto archivosAbanSeleccionado : archivosAban_seleccionados) {
			archivosAbandon.remove(archivosAbanSeleccionado);			
			miaDao.eliminarArchivosEtapas(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 4, archivosAbanSeleccionado.getId() ,archivosAbanSeleccionado.getSeqId());
		}
		archivosAbandon = getMiaDao().getArchivosProyecto(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 4);
		return "etapaProyecto";
	}
	
	public String tieneArchivosAbanSeleccionados() {
		System.out.println("Comprobando Chechbox´s");
		System.out.println(archivosAban_seleccionados.size() + " seleccionados");
		if (!archivosAban_seleccionados.isEmpty()) {
			RequestContext context = RequestContext.getCurrentInstance();
			System.out.println("Abriendo modal de confirmación");
			context.execute("eliminarArchAban.show()");
		}
		System.out.println("Saliendo del IF");
		return "etapaProyecto";
	}
	
	public void consultarArchivoAban() {
		System.out.println("Consultando");
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idArchivoAban = params.get("id").toString();
		id = Integer.parseInt(idArchivoAban);
		int rowIndex = 0;
		String pRowIndex = params.get("pRowIndex").toString();
		rowIndex = Integer.parseInt(pRowIndex);
		System.out.println("Actualizando archivo: " + id);
		archivoEdicion = new ArchivosProyecto();
		archivoEdicion = miaDao.getArchivoEtapa(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 4, (short) id);
		System.out.println("Fin de la Consulta");
		indexRow = rowIndex;
	}
	
	public void editarArchivoAban() {
		ArchivosProyecto ar = new ArchivosProyecto();
		ar = archivoEdicion;
		try {
			miaDao.persist(ar);
			
			archivosAbandon = getMiaDao().getArchivosProyecto(folioProyecto, serialProyecto, (short) 2, (short) 2, seccion, (short) 4);
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditarArchA.hide();modalAlertEdArchA.show()");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void guardar() {
		RequestContext reqcontEnv = RequestContext.getCurrentInstance();
		try {
//			for (ActividadEtapa p : actividadesPrepara) {
//				getMiaDao().merge(p);
//			}
//			for (ActividadEtapa p : actividadesConstru) {
//				getMiaDao().merge(p);
//			}
//			for (ActividadEtapa p : actividadesOperaci) {
//				getMiaDao().merge(p);
//			}
//			for (ActividadEtapa p : actividadesAbandon) {
//				getMiaDao().merge(p);
//			}

			if (actividadesPrepara.size() >= 1 || actividadesConstru.size() >= 1 || actividadesOperaci.size() >= 1
					|| actividadesAbandon.size() >= 1) {
				// <editor-fold desc="guardado de avance local y SINATEC"
				// defaultstate="collapsed">
				try {
					getMiaDao().guardarAvance(getProyecto(), new Short("2"), "212");
				} catch (Exception e) {
					System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
				}
				// </editor-fold>
			}

			reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
		} catch (Exception e) {
			reqcontEnv.execute("mensajes('No se ha podido guardar la información, intente más tarde.', 'danger')");
			e.printStackTrace();
		}
	}
	
	/**
     * Inicializa un nuevo archivo para ser cargado
     */
    public void iniciarArchivo() {
    	archivoTemporal = new ArchivosProyecto();
    	Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		short apartadoId = 0;
		String id = params.get("id").toString();
		apartadoId = Short.parseShort(id);
		
		if (apartadoId == 1) {
			RequestContext.getCurrentInstance().reset("archivosPrep:pnlArchP");
		}
		if (apartadoId == 2) {
			RequestContext.getCurrentInstance().reset("archivosCons:pnlArchC");
		}
		if (apartadoId == 3) {
			RequestContext.getCurrentInstance().reset("archivosOper:pnlArchO");
		}
		if (apartadoId == 4) {
			RequestContext.getCurrentInstance().reset("archivosAban:pnlArchA");
		}		
    	archivoTemporal = new ArchivosProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), 
    			(short) 2, (short) 2, seccion, apartadoId); 
    	
    	cargaBean = new CargaBean();
    	
    	msj = "";
    }
    
    /**
     * Guarda el archivo pdf en la BD y el Servidor 
     */
	public void guardarArchivo(ActionEvent evt) {
		String tipo = (String) evt.getComponent().getAttributes().get("tipo");
		id = getMiaDao().getMaxArchivoEtapaProyecto(folioProyecto, serialProyecto, archivoTemporal.getCapituloId(),
				archivoTemporal.getSubCapituloId(), archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());
		System.out.println("Registro: #" + id);
		archivoTemporal.setId(id);
		try {
			bandera = cargaBean.GuardaAnexos(archivoTemporal);
			if (bandera == true) {
		
			System.out.println("Fin del guardado de Archivos");
			RequestContext context = RequestContext.getCurrentInstance();
			System.out.println("Cerrando Modal Agregar y abriendo Modal Aviso");				
			if (tipo.equals("preparacion")) {
				context.execute("modalSubirArchivoPrep.hide();modalAlertArchP.show()");
				archivosPrepara.add(archivoTemporal);
			}
			if (tipo.equals("construccion")) {
				context.execute("modalSubirArchivoCons.hide();modalAlertArchC.show()");
				archivosConstru.add(archivoTemporal);
			}
			if (tipo.equals("operacion")) {
				context.execute("modalSubirArchivoOper.hide();modalAlertArchO.show()");
				archivosOperaci.add(archivoTemporal);
			}
			if (tipo.equals("abandono")) {
				context.execute("modalSubirArchivoAban.hide();modalAlertArchA.show()");
				archivosAbandon.add(archivoTemporal);
			}		
			cargaBean = new CargaBean();
			msj = " ";
			}else {
				msj = "Se requiere cargar un archivo";
				
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(":archivosPrep:msgAnexoPrep", new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Hubo un error al guardar el archivo o no se ha seleccionado", null));
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @return the actividadesPrepara
	 */
	public List<ActividadEtapa> getActividadesPrepara() {
		return actividadesPrepara;
	}

	/**
	 * @param actividadesPrepara
	 *            the actividadesPrepara to set
	 */
	public void setActividadesPrepara(List<ActividadEtapa> actividadesPrepara) {
		this.actividadesPrepara = actividadesPrepara;
	}

	/**
	 * @return the actividadesConstru
	 */
	public List<ActividadEtapa> getActividadesConstru() {
		return actividadesConstru;
	}

	/**
	 * @param actividadesConstru
	 *            the actividadesConstru to set
	 */
	public void setActividadesConstru(List<ActividadEtapa> actividadesConstru) {
		this.actividadesConstru = actividadesConstru;
	}

	/**
	 * @return the actividadesOperaci
	 */
	public List<ActividadEtapa> getActividadesOperaci() {
		return actividadesOperaci;
	}

	/**
	 * @param actividadesOperaci
	 *            the actividadesOperaci to set
	 */
	public void setActividadesOperaci(List<ActividadEtapa> actividadesOperaci) {
		this.actividadesOperaci = actividadesOperaci;
	}

	/**
	 * @return the actividadesAbandon
	 */
	public List<ActividadEtapa> getActividadesAbandon() {
		return actividadesAbandon;
	}

	/**
	 * @param actividadesAbandon
	 *            the actividadesAbandon to set
	 */
	public void setActividadesAbandon(List<ActividadEtapa> actividadesAbandon) {
		this.actividadesAbandon = actividadesAbandon;
	}

	/**
	 * @return the verPreparacion
	 */
	public Boolean getVerPreparacion() {
		return verPreparacion;
	}

	/**
	 * @param verPreparacion
	 *            the verPreparacion to set
	 */
	public void setVerPreparacion(Boolean verPreparacion) {
		this.verPreparacion = verPreparacion;
	}

	/**
	 * @return the verConstruccion
	 */
	public Boolean getVerConstruccion() {
		return verConstruccion;
	}

	/**
	 * @param verConstruccion
	 *            the verConstruccion to set
	 */
	public void setVerConstruccion(Boolean verConstruccion) {
		this.verConstruccion = verConstruccion;
	}

	/**
	 * @return the verOperacion
	 */
	public Boolean getVerOperacion() {
		return verOperacion;
	}

	/**
	 * @param verOperacion
	 *            the verOperacion to set
	 */
	public void setVerOperacion(Boolean verOperacion) {
		this.verOperacion = verOperacion;
	}

	/**
	 * @return the verAbandono
	 */
	public Boolean getVerAbandono() {
		return verAbandono;
	}

	/**
	 * @param verAbandono
	 *            the verAbandono to set
	 */
	public void setVerAbandono(Boolean verAbandono) {
		this.verAbandono = verAbandono;
	}

	/**
	 * @return the verMsgEtapa
	 */
	public Boolean getVerMsgEtapa() {
		return verMsgEtapa;
	}

	/**
	 * @param verMsgEtapa
	 *            the verMsgEtapa to set
	 */
	public void setVerMsgEtapa(Boolean verMsgEtapa) {
		this.verMsgEtapa = verMsgEtapa;
	}

	/**
	 * @return the selectedActividad
	 */
	public ActividadEtapa getSelectedActividad() {
		return selectedActividad;
	}

	/**
	 * @param selectedActividad
	 *            the selectedActividad to set
	 */
	public void setSelectedActividad(ActividadEtapa selectedActividad) {
		this.selectedActividad = selectedActividad;
	}

	/**
	 * Genera un nuevo objeto actividad para realizar un alta
	 */
	public void crearActividad(ActionEvent evt) {
		String tipo = (String) evt.getComponent().getAttributes().get("tipo");
		System.out.println("Entrando en los IF´s");
		if (tipo.equals("preparacion")) {
			RequestContext.getCurrentInstance().reset("fAgregarActivPrep:pnlAgregarP");		
			selectedActividad = new ActividadEtapa();
			RequestContext.getCurrentInstance().execute("modalAgregarP.show();");
		}
		if (tipo.equals("construccion")) {
			RequestContext.getCurrentInstance().reset("fAgregarActivCons:pnlAgregarC");		
			selectedActividad = new ActividadEtapa();
			RequestContext.getCurrentInstance().execute("modalAgregarC.show();");
		}
		if (tipo.equals("operacion")) {
			RequestContext.getCurrentInstance().reset("fAgregarActivOper:pnlAgregarO");		
			selectedActividad = new ActividadEtapa();
			RequestContext.getCurrentInstance().execute("modalAgregarO.show();");
		}
		if (tipo.equals("abandono")) {
			RequestContext.getCurrentInstance().reset("fAgregarActivAban:pnlAgregarA");		
			selectedActividad = new ActividadEtapa();
			RequestContext.getCurrentInstance().execute("modalAgregarA.show();");
		}
	}

	/**
	 * Metodo postconstruct que se ejecuta al cargar la pagina
	 */
	@PostConstruct
	public void init() {
		selectedActividad = new ActividadEtapa();
		archivoTemporal = new ArchivosProyecto();
	}

	/**
	 * @return the activPrep_seleccionados
	 */
	public List<ActividadEtapa> getActivPrep_seleccionados() {
		return activPrep_seleccionados;
	}

	/**
	 * @param activPrep_seleccionados
	 *            the activPrep_seleccionados to set
	 */
	public void setActivPrep_seleccionados(List<ActividadEtapa> activPrep_seleccionados) {
		this.activPrep_seleccionados = activPrep_seleccionados;
	}

	/**
	 * @return the editActividad
	 */
	public ActividadEtapa getEditActividad() {
		return editActividad;
	}

	/**
	 * @param editActividad the editActividad to set
	 */
	public void setEditActividad(ActividadEtapa editActividad) {
		this.editActividad = editActividad;
	}
	

	/**
	 * @return the activCons_seleccionados
	 */
	public List<ActividadEtapa> getActivCons_seleccionados() {
		return activCons_seleccionados;
	}
	

	/**
	 * @param activCons_seleccionados the activCons_seleccionados to set
	 */
	public void setActivCons_seleccionados(List<ActividadEtapa> activCons_seleccionados) {
		this.activCons_seleccionados = activCons_seleccionados;
	}
	

	/**
	 * @return the activOper_seleccionados
	 */
	public List<ActividadEtapa> getActivOper_seleccionados() {
		return activOper_seleccionados;
	}
	

	/**
	 * @param activOper_seleccionados the activOper_seleccionados to set
	 */
	public void setActivOper_seleccionados(List<ActividadEtapa> activOper_seleccionados) {
		this.activOper_seleccionados = activOper_seleccionados;
	}
	

	/**
	 * @return the activAban_seleccionados
	 */
	public List<ActividadEtapa> getActivAban_seleccionados() {
		return activAban_seleccionados;
	}
	

	/**
	 * @param activAban_seleccionados the activAban_seleccionados to set
	 */
	public void setActivAban_seleccionados(List<ActividadEtapa> activAban_seleccionados) {
		this.activAban_seleccionados = activAban_seleccionados;
	}

	/**
	 * @return the folioProyecto
	 */
	public String getFolioProyecto() {
		return folioProyecto;
	}

	/**
	 * @return the serialProyecto
	 */
	public Short getSerialProyecto() {
		return serialProyecto;
	}

	/**
	 * @return the archivoTemporal
	 */
	public ArchivosProyecto getArchivoTemporal() {
		return archivoTemporal;
	}

	/**
	 * @param archivoTemporal the archivoTemporal to set
	 */
	public void setArchivoTemporal(ArchivosProyecto archivoTemporal) {
		this.archivoTemporal = archivoTemporal;
	}

	/**
	 * @return the cargaBean
	 */
	public CargaBean getCargaBean() {
		return cargaBean;
	}

	/**
	 * @param cargaBean the cargaBean to set
	 */
	public void setCargaBean(CargaBean cargaBean) {
		this.cargaBean = cargaBean;
	}

	/**
	 * @return the archivosPrepara
	 */
	public List<ArchivosProyecto> getArchivosPrepara() {
		return archivosPrepara;
	}

	/**
	 * @param archivosPrepara the archivosPrepara to set
	 */
	public void setArchivosPrepara(List<ArchivosProyecto> archivosPrepara) {
		this.archivosPrepara = archivosPrepara;
	}

	/**
	 * @return the archivosPrep_seleccionados
	 */
	public List<ArchivosProyecto> getArchivosPrep_seleccionados() {
		return archivosPrep_seleccionados;
	}

	/**
	 * @param archivosPrep_seleccionados the archivosPrep_seleccionados to set
	 */
	public void setArchivosPrep_seleccionados(List<ArchivosProyecto> archivosPrep_seleccionados) {
		this.archivosPrep_seleccionados = archivosPrep_seleccionados;
	}

	/**
	 * @return the archivoEdicion
	 */
	public ArchivosProyecto getArchivoEdicion() {
		return archivoEdicion;
	}

	/**
	 * @param archivoEdicion the archivoEdicion to set
	 */
	public void setArchivoEdicion(ArchivosProyecto archivoEdicion) {
		this.archivoEdicion = archivoEdicion;
	}

	/**
	 * @return the archivosConstru
	 */
	public List<ArchivosProyecto> getArchivosConstru() {
		return archivosConstru;
	}

	/**
	 * @param archivosConstru the archivosConstru to set
	 */
	public void setArchivosConstru(List<ArchivosProyecto> archivosConstru) {
		this.archivosConstru = archivosConstru;
	}

	/**
	 * @return the archivosCons_seleccionados
	 */
	public List<ArchivosProyecto> getArchivosCons_seleccionados() {
		return archivosCons_seleccionados;
	}

	/**
	 * @param archivosCons_seleccionados the archivosCons_seleccionados to set
	 */
	public void setArchivosCons_seleccionados(List<ArchivosProyecto> archivosCons_seleccionados) {
		this.archivosCons_seleccionados = archivosCons_seleccionados;
	}

	/**
	 * @return the archivosOperaci
	 */
	public List<ArchivosProyecto> getArchivosOperaci() {
		return archivosOperaci;
	}

	/**
	 * @param archivosOperaci the archivosOperaci to set
	 */
	public void setArchivosOperaci(List<ArchivosProyecto> archivosOperaci) {
		this.archivosOperaci = archivosOperaci;
	}

	/**
	 * @return the archivosOper_seleccionados
	 */
	public List<ArchivosProyecto> getArchivosOper_seleccionados() {
		return archivosOper_seleccionados;
	}

	/**
	 * @param archivosOper_seleccionados the archivosOper_seleccionados to set
	 */
	public void setArchivosOper_seleccionados(List<ArchivosProyecto> archivosOper_seleccionados) {
		this.archivosOper_seleccionados = archivosOper_seleccionados;
	}

	/**
	 * @return the archivosAbandon
	 */
	public List<ArchivosProyecto> getArchivosAbandon() {
		return archivosAbandon;
	}

	/**
	 * @param archivosAbandon the archivosAbandon to set
	 */
	public void setArchivosAbandon(List<ArchivosProyecto> archivosAbandon) {
		this.archivosAbandon = archivosAbandon;
	}

	/**
	 * @return the archivosAban_seleccionados
	 */
	public List<ArchivosProyecto> getArchivosAban_seleccionados() {
		return archivosAban_seleccionados;
	}

	/**
	 * @param archivosAban_seleccionados the archivosAban_seleccionados to set
	 */
	public void setArchivosAban_seleccionados(List<ArchivosProyecto> archivosAban_seleccionados) {
		this.archivosAban_seleccionados = archivosAban_seleccionados;
	}

	/**
	 * @return the explosivosPrep
	 */
	public boolean isExplosivosPrep() {
		return explosivosPrep;
	}

	/**
	 * @param explosivosPrep the explosivosPrep to set
	 */
	public void setExplosivosPrep(boolean explosivosPrep) {
		this.explosivosPrep = explosivosPrep;
	}

	/**
	 * @return the explosivosCons
	 */
	public boolean isExplosivosCons() {
		return explosivosCons;
	}

	/**
	 * @param explosivosCons the explosivosCons to set
	 */
	public void setExplosivosCons(boolean explosivosCons) {
		this.explosivosCons = explosivosCons;
	}

	/**
	 * @return the explosivosOper
	 */
	public boolean isExplosivosOper() {
		return explosivosOper;
	}

	/**
	 * @param explosivosOper the explosivosOper to set
	 */
	public void setExplosivosOper(boolean explosivosOper) {
		this.explosivosOper = explosivosOper;
	}

	/**
	 * @return the explosivosAban
	 */
	public boolean isExplosivosAban() {
		return explosivosAban;
	}

	/**
	 * @param explosivosAban the explosivosAban to set
	 */
	public void setExplosivosAban(boolean explosivosAban) {
		this.explosivosAban = explosivosAban;
	}

	public String getMsj() {
		return msj;
	}

	public void setMsj(String msj) {
		this.msj = msj;
	}

	public boolean isBandera() {
		return bandera;
	}

	public void setBandera(boolean bandera) {
		this.bandera = bandera;
	}
	
	public Short getSeccion() {
		return seccion;
	}

	public void setSeccion(Short seccion) {
		this.seccion = seccion;
	}
	
	public int getRowIndexEditar() {
		return rowIndexEditar;
	}

	public void setRowIndexEditar(int rowIndexEditar) {
		this.rowIndexEditar = rowIndexEditar;
	}

	public long getIndexRow() {
		return indexRow;
	}

	public void setIndexRow(long indexRow) {
		this.indexRow = indexRow;
	}

}
