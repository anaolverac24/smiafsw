/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view.capitulo2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import mx.gob.semarnat.mia.model.EtapaProyecto;
import mx.gob.semarnat.mia.model.InsumosProyecto;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.catalogos.CatUnidadMedida;
import mx.gob.semarnat.mia.view.CapitulosComentarios;

import org.primefaces.context.RequestContext;


@ManagedBean
@ViewScoped
public class Cap2_2_10_InsumosView extends CapitulosComentarios implements Serializable {

    //private MiaDao miaDao = new MiaDao();
    private Proyecto proyecto = new Proyecto();
    private Short    NSubSector = (short) 0;
    private Short    nuevoInsumoId = 0;
    private Integer  claveTramite = 0;

    private List<InsumosProyecto> insumosLista=new ArrayList<InsumosProyecto>();
    private List<InsumosProyecto> insumosSeleccionados=new ArrayList<InsumosProyecto>();
    private short insumoId = 0;
    private InsumosProyecto add=null;
    private InsumosProyecto edit=null;
    private List<CatUnidadMedida> catUnidadMedida ;
    private List<CatUnidadMedida> catMedida ;
    private List<EtapaProyecto> etapasProyecto;
    
    // Número secuencial del registro "Otros insumos" a editar
 	private int rowIndexInsumoEditar;


	/**
     * Creates a new instance of Cap2_2_10_InsumosView
     */
    public Cap2_2_10_InsumosView() {
        // Recupera proyecto de sesion
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
        
        claveTramite = (Integer) context.getExternalContext().getSessionMap().get("claveTramite");
//        etapasProyecto = new ArrayList<EtapaProyecto>();
//        etapasProyecto = miaDao.getEtapaProyecto(getProyecto().getProyectoPK().getFolioProyecto(),getProyecto().getProyectoPK().getSerialProyecto());
        //Cargar Proyecto
        try {
            this.proyecto = miaDao.proyecto(folioProyecto, serialProyecto);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }

        if (proyecto == null) {
            this.proyecto = new Proyecto(folioProyecto, serialProyecto);
        }

        if (proyecto.getNsub() != null) {
            NSubSector = proyecto.getNsub();
        }

        // Cargar Insumos
        insumosLista= miaDao.getInsumos(folioProyecto, serialProyecto);
        
        //Carga unidades de tipo masa y volumen
        catUnidadMedida=miaDao.getUnidadesMedida();
        catMedida=miaDao.getCatUnidadMedida();
        
        //Comentarios proyecto - eescalona
        super.setProyectoInfo((short)2, folioProyecto, serialProyecto);

    }

    public List<EtapaProyecto> getEtapasProyecto() {
		return etapasProyecto;
	}

	public void setEtapasProyecto(List<EtapaProyecto> etapasProyecto) {
		this.etapasProyecto = etapasProyecto;
	}

	public void creaInsumo(){
    	nuevoInsumoId = miaDao.getMaxInsumo(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    	
    	if(nuevoInsumoId==null || nuevoInsumoId==0){
    		nuevoInsumoId=1;
    	}
    	
    	add=new InsumosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),(short)1,nuevoInsumoId);
    	RequestContext context = RequestContext.getCurrentInstance();
		context.execute("modalAgregar.show()");
    }
    
    public void agregarInsumo() {
        try{
        	add.setCatEtapa(add.getCatEtapa());
        	add.getInsumosProyectoPK().setEtapaId(add.getCatEtapa().getEtapaId());
        	miaDao.persist(add);
	        insumosLista.add(add);
	        RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalAgregar.hide(),modalAvisoAgregado.show()");
        }catch(Exception e){
        	e.printStackTrace();
        }
   }

    public void tieneInsumosSeleccionados(){
    	if(!insumosSeleccionados.isEmpty()){
    		RequestContext context = RequestContext.getCurrentInstance();
    	    context.execute("modalEliminar.show()");
    	}
    }
    
    public void consultaInsumoId(){
    	@SuppressWarnings("rawtypes")
    	Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String insumo= params.get("idInsumo").toString();
		String etapa= params.get("idEtapa").toString();

		// Nímero secuencial a mostrar en "Editar registro #"
		rowIndexInsumoEditar = Integer.parseInt(params.get("prowindexInsumo").toString());
		
		insumoId=Short.parseShort(insumo);
		short idEtapa=Short.parseShort(etapa);
		
		edit=new InsumosProyecto();
		edit=miaDao.getInsumo(proyecto.getProyectoPK().getFolioProyecto(),proyecto.getProyectoPK().getSerialProyecto(),idEtapa,insumoId);
		
	}
    
    public void editarInsumo(){
    	try {
    		miaDao.persist(edit);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditar.hide();modalMensajeEditar.show()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void eliminarInsumos() throws Exception{
    	short etapaId=0;
    	short insumoId=0;
    	
    	if(!insumosSeleccionados.isEmpty()){
    		for(InsumosProyecto i: insumosSeleccionados){
    			etapaId=i.getCatEtapa().getEtapaId();
    			insumoId=i.getInsumosProyectoPK().getInsumosId();
    			System.out.println("Eliminando insumo:" + insumoId + "- Etapa:" + etapaId);
    			insumosLista.remove(i);
    			miaDao.eliminarInsumos(i.getInsumosProyectoPK().getFolioSerial(), i.getInsumosProyectoPK().getSerialProyecto());
    			
    		}
    	
    		RequestContext context = RequestContext.getCurrentInstance();
    		context.execute("modalAvisoEliminar.show();");
    	}
    }
  
    public String unidades(short id){
    	if(!insumosLista.isEmpty() && id>0){
    	 CatUnidadMedida unidad= miaDao.getCatUnidadMedida(id);
    	 return unidad.getCtunDesc();
    	}
    	return "";
    }
    
    public void guardar() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();

        try { 
            //Guardar nuevas
            //<editor-fold desc="Guardado Apartado 213 Otros Insumos" defaultstate="collapsed">
            try {
                 //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                    if (proyecto.getNsub().equals(new Short("6")) || proyecto.getNsub().equals(new Short("7")) || claveTramite == 66 || claveTramite == 67) {
                        if (insumosLista.size() >= 1) {
                            try {
                                miaDao.guardarAvance(proyecto, new Short("1"), "213");
                            } catch (Exception en) {
                                System.out.println("[ERR] Error al cargar avance: " + en.getCause().getMessage());
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
          
            //Guardado de componentes en información adicional - eescalona
            super.guardarComentarios();

            reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
        } catch (Exception e) {
            reqcontEnv.execute("mensajes('No se ha podido guardar la información, intente mas tarde.', 'success')");
            e.printStackTrace();
        }

    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the NSubSector
     */
    public Short getNSubSector() {
        return NSubSector;
    }

    /**
     * @param NSubSector the NSubSector to set
     */
    public void setNSubSector(Short NSubSector) {
        this.NSubSector = NSubSector;
    }

    /**
     * @return the insumosProyecto
     */
   
    public List<InsumosProyecto> getInsumosLista() {
		return insumosLista;
	}

	public void setInsumosLista(List<InsumosProyecto> insumosLista) {
		this.insumosLista = insumosLista;
	}

	public InsumosProyecto getAdd() {
		return add;
	}

	public void setAdd(InsumosProyecto add) {
		this.add = add;
	}

	public InsumosProyecto getEdit() {
		return edit;
	}

	public void setEdit(InsumosProyecto edit) {
		this.edit = edit;
	}

	public List<InsumosProyecto> getInsumosSeleccionados() {
		return insumosSeleccionados;
	}

	public void setInsumosSeleccionados(List<InsumosProyecto> insumosSeleccionados) {
		this.insumosSeleccionados = insumosSeleccionados;
	}

	public List<CatUnidadMedida> getCatUnidadMedida() {
		return catUnidadMedida;
	}

	public void setCatUnidadMedida(List<CatUnidadMedida> catUnidadMedida) {
		this.catUnidadMedida = catUnidadMedida;
	}

	public List<CatUnidadMedida> getCatMedida() {
		return catMedida;
	}

	public void setCatMedida(List<CatUnidadMedida> catMedida) {
		this.catMedida = catMedida;
	}
	
	/**
	 * @return Número secuencial (en la Vista) para un registro de tabla Otros insumos
	 */
	public int getRowIndexInsumoEditar() {
		return rowIndexInsumoEditar;
	}

	/**
	 * Asigna valor a variable que guarda Número secuencial de Otros insumos a editar
	 * @param rowIndexInsumoEditar
	 */
	public void setRowIndexInsumoEditar(int rowIndexInsumoEditar) {
		this.rowIndexInsumoEditar = rowIndexInsumoEditar;
	}
	
}
