/**
 * 
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import mx.gob.semarnat.mia.Enum.ArchivosAnexosEnum;
import mx.gob.semarnat.mia.dao.ArchivosAnexosDAO;
import mx.gob.semarnat.mia.dao.CargaBean;
import mx.gob.semarnat.mia.dao.MiaDao;
import mx.gob.semarnat.mia.model.ArchivosProyecto;
import mx.gob.semarnat.mia.model.Proyecto;

/**
 * @author dpaniagua.
 *
 */
@ManagedBean
@ViewScoped
public class ArchivosAnexosCap10Bean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3588621513710174185L;
	/**
	 * Clave del tramite del proyecto.
	 */
	private Integer claveTramite;
	/**
	 * Proyecto del promovente.
	 */
	private Proyecto proyecto;
	/**
	 * Permite ocupar las transacciones de MiaDAO.
	 */
    private MiaDao miaDao = new MiaDao();
    /**
     * Permite obtener el valor maximo de la etapa del proyecto.
     */
	private Short id;
	/**
	 * Archivo anexo del proyecto.
	 */
	private ArchivosProyecto archivoTemporal;
	/**
	 * Permite realizar la carga del archivo del anexo.
	 */
	private CargaBean cargaBean;
	/**
	 * Lista que permite mostrar los Archivos del proyecto.
	 */
	private List<ArchivosProyecto> archivosGeneral = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de archivos seleccionados de la tabla.
	 */
	private List<ArchivosProyecto> listaArchivosSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Archivo anexo a editar del capitulo.
	 */
	private ArchivosProyecto archivoEdicion;
	/**
	 * Folio del Proyecto
	 */
	private String folioProyecto;
	/**
	 * Serial del Proyecto.
	 */
	private short serialProyecto;	
	
	private ArchivosAnexosDAO archivosAnexosDAO;
	/**
	 * Capitulo de la seccion seleccionada.
	 */
	private String capitulo;
	/**
	 * SubCapitulo de la seccion seleccionada.
	 */
	private String subCapitulo;
	/**
	 * Seccion del capitulo.
	 */
	private String seccion;
	/**
	 * Apartado del capitulo.
	 */
	private String apartado;
	/**
	 * Capitulo de la pantalla actual.
	 */
	private String capituloUbicado;
	
	
	// ============================== DATOS CAPITULO 10 : RESUMEN ============================== //
	/**
	 * Seccion del Capitulo 10 Seleccionada.	
	 */
	private String seccionSeleccionada;
	/**
	 * Lista de Archivos de Conclusiones.
	 */
	private List<ArchivosProyecto> archivosGeneralConclusiones;
	/**
	 * Lista de Archivos de Situaciones Generales.
	 */
	private List<ArchivosProyecto> archivosGeneralSituacionGen;
	/**
	 * Lista de Archivos de Informe Tecnico.
	 */
	private List<ArchivosProyecto> archivosGeneralInformeTec;	
	/**
	 * Lista de los archivos seleccionados a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosConclusionesSeleccionados = new ArrayList<ArchivosProyecto>();
	
	private List<ArchivosProyecto> listaArchivosSituacionGenSeleccionados = new ArrayList<ArchivosProyecto>();

	private List<ArchivosProyecto> listaArchivosInformeTecSeleccionados = new ArrayList<ArchivosProyecto>();
	
	
	// ============================== DATOS CAPITULO 10 : ESCENARIOS DE LOS RIESGOS AMBIENTALES RELACIONADOS CON EL PROYECTO ================== //
	/**
	 * Lista de Archivos de Bases de Disenio.
	 */
	private List<ArchivosProyecto> archivosGeneralBases;
	/**
	 * Lista de Archivos de Proyecto Civil.
	 */
	private List<ArchivosProyecto> archivosGeneralCivil;
	/**
	 * Lista de Archivos de Proyecto Mecanico.
	 */
	private List<ArchivosProyecto> archivosGeneralMecanico;	
	/**
	 * Lista de Archivos de Proyecto Sistema Contra Incendios.
	 */
	private List<ArchivosProyecto> archivosGeneralIncendio;
	/**
	 * Lista de Archivos de Descripcion Detallada del Proyecto.
	 */
	private List<ArchivosProyecto> archivosGeneralDescripcion;
	/**
	 * Lista de Archivos de Almacenamiento.
	 */
	private List<ArchivosProyecto> archivosGeneralAlmacenamiento;
	/**
	 * Lista de Archivos de Equipos de Proceso y Auxiliares.
	 */
	private List<ArchivosProyecto> archivosGeneralEquipos;
	/**
	 * Lista de Archivos de Pruebas de Verificacion.
	 */
	private List<ArchivosProyecto> archivosGeneralPruebas;
	/**
	 * Lista de Archivos de Condiciones.
	 */
	private List<ArchivosProyecto> archivosGeneralCondiciones;
	/**
	 * Lista de Archivos de Especificacion.
	 */
	private List<ArchivosProyecto> archivosGeneralEspecificacion;
	/**
	 * Lista de Archivos de Aislamiento.
	 */
	private List<ArchivosProyecto> archivosGeneralAislamiento;	
	/**
	 * Lista de Archivos de Accidentes.
	 */
	private List<ArchivosProyecto> archivosGeneralAccidentes;
	/**
	 * Lista de Archivos de Metodologias.
	 */
	private List<ArchivosProyecto> archivosGeneralMetodologias;
	
	/**
	 * Lista de los archivos base seleccionados a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosBaseSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de los archivos proyecto civil seleccionados a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosCivilSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de los archivos proyecto mecanico seleccionados a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosMecanicoSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de los archivos proyecto sistema contra incendios seleccionados a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosIncendioSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de los archivos descripcion detallada del proyecto seleccionados a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosDescripcionSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de los archivos de almacenamiento seleccionados a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosAlmacenamientoSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de los archivos de especificacion seleccionados a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosEspecificacionSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de los archivos de aislamiento seleccionados a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosAislamientoSeleccionados = new ArrayList<ArchivosProyecto>();	

	

	/**
	 * Lista de los archivos de condiciones de operacion seleccionados a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosCondicionesSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de los archivos de equipos de proceso y auxiliares seleccionados a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosEquiposSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de los archivos de pruebas de verificacion seleccionados a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosPruebasSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de los archivos de accidentes seleccionados a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosAccidentesSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de los archivos de metodologias seleccionados a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosMetodologiasSeleccionados = new ArrayList<ArchivosProyecto>();
	// ============================== DATOS CAPITULO 10 : DESCRIPCION DE LAS MEDIDAS DE PROTECCION EN TORNO A LAS INSTALACIONES ================== //	
	
	
	/**
	 * Lista de Archivos de Proyecto Radios Potenciales de Afectacion.
	 */
	private List<ArchivosProyecto> archivosGeneralRadios;
	/**
	 * Lista de Archivos de Proyecto Interacciones de Riesgo.
	 */
	private List<ArchivosProyecto> archivosGeneralInteracciones;
	/**
	 * Lista de Archivos de Proyecto Efectos Sobre el Area de Influencia.
	 */
	private List<ArchivosProyecto> archivosGeneralEfectos;
	/**
	 * Lista de los archivos de Radios Potenciales a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosRadiosSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de los archivos de Interacciones de Riesgo a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosInteraccionesSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de los archivos de Efectos sobre el area de influencia a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosEfectosSeleccionados = new ArrayList<ArchivosProyecto>();
	
	// ============================== DATOS CAPITULO 10 : Senialamiento de las Medidas de Seguridad y Preventivas en Materia Ambiental ================== //
	
	/**
	 * Lista de Archivos de Proyecto Recomendaciones Tecnico/Operativas.
	 */
	private List<ArchivosProyecto> archivosGeneralRecomendaciones;
	/**
	 * Lista de Archivos de Proyecto Sistemas de Seguridad.
	 */
	private List<ArchivosProyecto> archivosGeneralSistemas;
	/**
	 * Lista de Archivos de Proyecto Medidas Preventivas.
	 */
	private List<ArchivosProyecto> archivosGeneralMedidas;
	/**
	 * Lista de los archivos de Recomendaciones Tecnico/Operativas a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosRecomendacionesSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de los archivos de Sistemas de Seguridad a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosSistemasSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de los archivos de Medidas Preventivas a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosMedidasSeleccionados = new ArrayList<ArchivosProyecto>();
	
	// ============================== DATOS CAPITULO 10 : Instrumentos Metodologicos y Elementos Tecnicos ================== //
	
	/**
	 * Lista de Archivos de Proyecto Planos de localizacion y Fotografias.
	 */
	private List<ArchivosProyecto> archivosGeneralPlanos;
	/**
	 * Lista de Archivos de Proyecto Otros Anexos.
	 */
	private List<ArchivosProyecto> archivosGeneralOtros;
	/**
	 * Lista de los archivos de Planos de localizacion y Fotografia a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosPlanosSeleccionados = new ArrayList<ArchivosProyecto>();
	/**
	 * Lista de los archivos de Otros Anexos a eliminar.
	 */
	private List<ArchivosProyecto> listaArchivosOtrosSeleccionados = new ArrayList<ArchivosProyecto>();
	
	private long indexRow = 0;
	
	private String msj;
	private boolean bandera;
	
	@PostConstruct
	public void init() {
		archivoEdicion = new ArchivosProyecto();
		archivoTemporal = new ArchivosProyecto();
		
		archivosGeneralConclusiones = new ArrayList<ArchivosProyecto>();
		archivosGeneralSituacionGen = new ArrayList<ArchivosProyecto>();
		archivosGeneralInformeTec = new ArrayList<ArchivosProyecto>();
		archivosGeneralBases = new ArrayList<ArchivosProyecto>();
		archivosGeneralCivil = new ArrayList<ArchivosProyecto>();
		archivosGeneralMecanico = new ArrayList<ArchivosProyecto>();
		archivosGeneralIncendio = new ArrayList<ArchivosProyecto>();
		archivosGeneralDescripcion = new ArrayList<ArchivosProyecto>();
		archivosGeneralAlmacenamiento = new ArrayList<ArchivosProyecto>();
		archivosGeneralEquipos = new ArrayList<ArchivosProyecto>();
		archivosGeneralPruebas = new ArrayList<ArchivosProyecto>();
		archivosGeneralCondiciones = new ArrayList<ArchivosProyecto>();
		archivosGeneralEspecificacion = new ArrayList<ArchivosProyecto>();
		archivosGeneralAislamiento = new ArrayList<ArchivosProyecto>();
		archivosGeneralAccidentes = new ArrayList<ArchivosProyecto>();
		archivosGeneralMetodologias = new ArrayList<ArchivosProyecto>();		
		archivosGeneralRadios = new ArrayList<ArchivosProyecto>();
		archivosGeneralInteracciones = new ArrayList<ArchivosProyecto>();
		archivosGeneralEfectos = new ArrayList<ArchivosProyecto>();
		archivosGeneralRecomendaciones = new ArrayList<ArchivosProyecto>();
		archivosGeneralSistemas = new ArrayList<ArchivosProyecto>();
		archivosGeneralMedidas = new ArrayList<ArchivosProyecto>();
		archivosGeneralPlanos = new ArrayList<ArchivosProyecto>();
		archivosGeneralOtros = new ArrayList<ArchivosProyecto>();
		
		cargaBean = new CargaBean();
		proyecto = new Proyecto();
		
        FacesContext context = FacesContext.getCurrentInstance();
        this.folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        this.serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
        
        context.getExternalContext().getSessionMap().get("userFolioProy");        
        claveTramite = (Integer) context.getExternalContext().getSessionMap().get("claveTramite");
        //Cargar Proyecto
        try {
            this.proyecto = miaDao.proyecto(this.folioProyecto, this.serialProyecto);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }

        if (proyecto == null) {
            this.proyecto = new Proyecto(this.folioProyecto, this.serialProyecto);
        }                    
	}
	
	/**
	 * Inicializa un nuevo archivo para ser cargado
	 */
	public void iniciarArchivo(String subCapituloSeleccionado) {
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

		String cadenaCapitulo = params.get("capitulo").toString();
		String cadenaSubCapitulo = params.get("subcapitulo").toString();
		String cadenaSeccion = params.get("seccion").toString();
		String cadenaApartado = params.get("apartado").toString();
	
		short capitulo = Short.parseShort(cadenaCapitulo);
		short subCapitulo = Short.parseShort(cadenaSubCapitulo);
		short seccion = Short.parseShort(cadenaSeccion);
		short apartado = Short.parseShort(cadenaApartado);
		
		archivoTemporal.setFolioSerial(this.folioProyecto);
		archivoTemporal.setSerialProyecto(this.serialProyecto);
		archivoTemporal.setCapituloId(capitulo);
		archivoTemporal.setSubCapituloId(subCapitulo);
		archivoTemporal.setSeccionId(seccion);
		archivoTemporal.setApartadoId(apartado);
		seccionSeleccionada = subCapituloSeleccionado;
		
		cargaBean = new CargaBean();
		
		msj = " ";
	}
	
	/**
	 * Permite abrir la pantalla modal de eliminar archivo de la seccion seleccionada.
	 */
	public void abrirModalEliminarArchivo() {
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		
		this.seccionSeleccionada = params.get("eliminarArchivo").toString();
		
		this.capitulo = params.get("capitulo").toString();
		this.subCapitulo = params.get("subcapitulo").toString();
		this.seccion = params.get("seccion").toString();
		this.apartado = params.get("apartado").toString();
		
		if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.CONCLUSIONES.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosConclusionesSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.SITUACIONGENERAL.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosSituacionGenSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.INFORMETECNICO.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosInformeTecSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.BASESDISENIO.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosBaseSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.PROYECTOCIVIL.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosCivilSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.PROYECTOMECANICO.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosMecanicoSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.PROYECTOINCENDIO.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosIncendioSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.DESCRIPCIONDETALLADA.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosDescripcionSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.ALMACENAMIENTO.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosAlmacenamientoSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.EQUIPOSAUX.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosEquiposSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.PRUEBASVERIFICACION.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosPruebasSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.CONDICIONES.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosCondicionesSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.ESPECIFICACION.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosEspecificacionSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.AISLAMIENTO.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosAislamientoSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.ANTECEDENTESACCIDENTES.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosAccidentesSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.METODOLOGIAS.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosMetodologiasSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.RADIOS.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosRadiosSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.INTERACCIONES.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosInteraccionesSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.EFECTOS.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosEfectosSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.RECOMENDACIONES.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosRecomendacionesSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.SISTEMAS.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosSistemasSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.MEDIDAS.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosMedidasSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.PLANOS.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosPlanosSeleccionados;
		} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.OTROS.getSubCapSeleccionado())) {
			this.listaArchivosSeleccionados = this.listaArchivosOtrosSeleccionados;
		}
	}
	
	/**
	 * Permite consultar el registro a editar segun la seccion en la que se encuentre.
	 * @param capitulo
	 * @param subCapitulo
	 * @param seccion
	 * @param apartado
	 */
	public void consultarArchivo(Short capitulo, Short subCapitulo, Short seccion, Short apartado) {
		
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idArchivo = params.get("id").toString();
		id = Integer.parseInt(idArchivo);
		int rowIndex = 0;
		String pRowIndex = params.get("pRowIndex").toString();
		rowIndex = Integer.parseInt(pRowIndex);
		
		this.seccionSeleccionada = params.get("actualizarArchivo").toString();

		archivoEdicion = new ArchivosProyecto();
		archivoEdicion = miaDao.getArchivoEtapa(this.folioProyecto, this.serialProyecto, capitulo, subCapitulo, seccion, apartado, (short) id);
		indexRow = rowIndex;

	}	

	/**
	 * Permite consulta la lista de archivos de los anexos de la seccion "Señalar las Conclusiones del Estudio de Riesgo Ambiental".
	 * @param capituloConclusiones
	 * @param subCapituloConclusiones
	 * @param seccionConclusiones
	 * @param apartadoConclusiones
	 * @return la lista de archivos de anexos.
	 */
	public void consultarDesgloseConclusionesCargaPagina(String capituloConclusiones,String subCapituloConclusiones, 
			String seccionConclusiones, String apartadoConclusiones) {	
		
		capituloUbicado = "5";
		
		this.archivosGeneralConclusiones = consultarArchivosAnexosProyecto(capituloConclusiones, subCapituloConclusiones, seccionConclusiones, apartadoConclusiones);
	}
	
	public List<ArchivosProyecto> consultarDesgloseConclusiones(String capituloConclusiones,String subCapituloConclusiones, 
			String seccionConclusiones, String apartadoConclusiones) {	
		
		capituloUbicado = "5";
		
		this.archivosGeneralConclusiones = consultarArchivosAnexosProyecto(capituloConclusiones, subCapituloConclusiones, seccionConclusiones, apartadoConclusiones);
		return this.archivosGeneralConclusiones;
	}	

	/**
	 * Permite consulta la lista de archivos de los anexos de la seccion "Hacer un Resumen de la Situacion General que Presenta el Proyecto en Materia de Riesgo Ambiental".
	 * @param capituloSituacionGen
	 * @param subCapituloSituacionGen
	 * @param seccionSituacionGen
	 * @param apartadoSituacionGen
	 * @return la lista de archivos de anexos.
	 */
	public void consultarDesgloseSituacionGenCargaPagina(String capituloSituacionGen, String subCapituloSituacionGen, 
			String seccionSituacionGen,String apartadoSituacionGen) {	
		
		this.archivosGeneralSituacionGen = consultarArchivosAnexosProyecto(capituloSituacionGen, subCapituloSituacionGen, seccionSituacionGen, apartadoSituacionGen);
	}
	
	public List<ArchivosProyecto> consultarDesgloseSituacionGen(String capituloSituacionGen, String subCapituloSituacionGen, 
			String seccionSituacionGen,String apartadoSituacionGen) {	
		
		this.archivosGeneralSituacionGen = consultarArchivosAnexosProyecto(capituloSituacionGen, subCapituloSituacionGen, seccionSituacionGen, apartadoSituacionGen);
		return this.archivosGeneralSituacionGen;

	}	
	
	/**
	 * Permite consulta la lista de archivos de los anexos de la seccion "Presentar el Informe Tecnico Debidamente Llenado"
	 * @param capituloInformeTec
	 * @param subCapituloInformeTec
	 * @param seccionInformeTec
	 * @param apartadoInformeTec
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseInformeTecCargaPagina(String capituloInformeTec, String subCapituloInformeTec, 
			String seccionInformeTec,String apartadoInformeTec) {		
		
		this.archivosGeneralInformeTec = consultarArchivosAnexosProyecto(capituloInformeTec, subCapituloInformeTec, seccionInformeTec, apartadoInformeTec);
	}
	
	public List<ArchivosProyecto> consultarDesgloseInformeTec(String capituloInformeTec, String subCapituloInformeTec, 
			String seccionInformeTec,String apartadoInformeTec) {		
		
		this.archivosGeneralInformeTec = consultarArchivosAnexosProyecto(capituloInformeTec, subCapituloInformeTec, seccionInformeTec, apartadoInformeTec);
		return this.archivosGeneralInformeTec;
	}	
	
	/**
	 * Permite consulta la lista de archivos de los anexos de la seccion "Bases de Disenio".
	 * @param capituloBases
	 * @param subCapituloBases
	 * @param seccionBases
	 * @param apartadoBases
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseBasesCargaPagina(String capituloBases, String subCapituloBases, 
			String seccionBases,String apartadoBases) {		
		
		capituloUbicado = "2";
		
		this.archivosGeneralBases = consultarArchivosAnexosProyecto(capituloBases, subCapituloBases, seccionBases, apartadoBases);
	}
	
	public List<ArchivosProyecto> consultarDesgloseBases(String capituloBases, String subCapituloBases, 
			String seccionBases,String apartadoBases) {		
		
		capituloUbicado = "2";
		
		this.archivosGeneralBases = consultarArchivosAnexosProyecto(capituloBases, subCapituloBases, seccionBases, apartadoBases);
		return this.archivosGeneralBases;
	}	
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Proyecto Civil".
	 * @param capituloCivil
	 * @param subCapituloCivil
	 * @param seccionCivil
	 * @param apartadoCivil
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseCivilCargaPagina(String capituloCivil, String subCapituloCivil, 
			String seccionCivil,String apartadoCivil) {		
				
		this.archivosGeneralCivil = consultarArchivosAnexosProyecto(capituloCivil, subCapituloCivil, seccionCivil, apartadoCivil);
	}
	
	public List<ArchivosProyecto> consultarDesgloseCivil(String capituloCivil, String subCapituloCivil, 
			String seccionCivil,String apartadoCivil) {		
				
		this.archivosGeneralCivil = consultarArchivosAnexosProyecto(capituloCivil, subCapituloCivil, seccionCivil, apartadoCivil);
		return this.archivosGeneralCivil;
	}	
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Proyecto Mecanico".
	 * @param capituloMecanico
	 * @param subCapituloMecanico
	 * @param seccionMecanico
	 * @param apartadoMecanico
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseMecanicoCargaPagina(String capituloMecanico, String subCapituloMecanico, 
			String seccionMecanico,String apartadoMecanico) {		
				
		this.archivosGeneralMecanico = consultarArchivosAnexosProyecto(capituloMecanico, subCapituloMecanico, seccionMecanico, apartadoMecanico);
	}
	
	public List<ArchivosProyecto> consultarDesgloseMecanico(String capituloMecanico, String subCapituloMecanico, 
			String seccionMecanico,String apartadoMecanico) {		
				
		this.archivosGeneralMecanico = consultarArchivosAnexosProyecto(capituloMecanico, subCapituloMecanico, seccionMecanico, apartadoMecanico);
		return this.archivosGeneralMecanico;
	}	
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Proyecto Sistema Contra Incendios".
	 * @param capituloIncendio
	 * @param subCapituloIncendio
	 * @param seccionIncendio
	 * @param apartadoIncendio
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseIncendioCargaPagina(String capituloIncendio, String subCapituloIncendio, 
			String seccionIncendio,String apartadoIncendio) {		
				
		this.archivosGeneralIncendio = consultarArchivosAnexosProyecto(capituloIncendio, subCapituloIncendio, seccionIncendio, apartadoIncendio);
	}
	
	public List<ArchivosProyecto> consultarDesgloseIncendio(String capituloIncendio, String subCapituloIncendio, 
			String seccionIncendio,String apartadoIncendio) {		
				
		this.archivosGeneralIncendio = consultarArchivosAnexosProyecto(capituloIncendio, subCapituloIncendio, seccionIncendio, apartadoIncendio);
		return this.archivosGeneralIncendio;
	}	
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Descripcion Detallada del Proyecto".
	 * @param capituloDescripcion
	 * @param subCapituloDescripcion
	 * @param seccionDescripcion
	 * @param apartadoDescripcion
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseDescripcionCargaPagina(String capituloDescripcion, String subCapituloDescripcion, 
			String seccionDescripcion,String apartadoDescripcion) {		
				
		this.archivosGeneralDescripcion = consultarArchivosAnexosProyecto(capituloDescripcion, subCapituloDescripcion, seccionDescripcion, apartadoDescripcion);
	}
	
	public List<ArchivosProyecto> consultarDesgloseDescripcion(String capituloDescripcion, String subCapituloDescripcion, 
			String seccionDescripcion,String apartadoDescripcion) {		
				
		this.archivosGeneralDescripcion = consultarArchivosAnexosProyecto(capituloDescripcion, subCapituloDescripcion, seccionDescripcion, apartadoDescripcion);
		return this.archivosGeneralDescripcion;
	}	
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Hojas de Seguridad".
	 * @param capituloHojas
	 * @param subCapituloHojas
	 * @param seccionHojas
	 * @param apartadoHojas
	 * @return la lista de archivos anexos.
	 */
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Almacenamiento".
	 * @param capituloAlmacenamiento
	 * @param subCapituloAlmacenamiento
	 * @param seccionAlmacenamiento
	 * @param apartadoAlmacenamiento
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseAlmacenamientoCargaPagina(String capituloAlmacenamiento, String subCapituloAlmacenamiento, 
			String seccionAlmacenamiento,String apartadoAlmacenamiento) {		
				
		this.archivosGeneralAlmacenamiento = consultarArchivosAnexosProyecto(capituloAlmacenamiento, subCapituloAlmacenamiento, seccionAlmacenamiento, apartadoAlmacenamiento);
	}
	
	public List<ArchivosProyecto> consultarDesgloseAlmacenamiento(String capituloAlmacenamiento, String subCapituloAlmacenamiento, 
			String seccionAlmacenamiento,String apartadoAlmacenamiento) {		
				
		this.archivosGeneralAlmacenamiento = consultarArchivosAnexosProyecto(capituloAlmacenamiento, subCapituloAlmacenamiento, seccionAlmacenamiento, apartadoAlmacenamiento);
		return this.archivosGeneralAlmacenamiento;
	}	
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Equipos de Proceso y Auxiliares".
	 * @param capituloEquipos
	 * @param subCapituloEquipos
	 * @param seccionEquipos
	 * @param apartadoEquipos
	 * @return lista de archivos anexos.
	 */
	public void consultarDesgloseEquiposCargaPagina(String capituloEquipos, String subCapituloEquipos, 
			String seccionEquipos,String apartadoEquipos) {		
				
		this.archivosGeneralEquipos = consultarArchivosAnexosProyecto(capituloEquipos, subCapituloEquipos, seccionEquipos, apartadoEquipos);
	}
	
	public List<ArchivosProyecto> consultarDesgloseEquipos(String capituloEquipos, String subCapituloEquipos, 
			String seccionEquipos,String apartadoEquipos) {		
				
		this.archivosGeneralEquipos = consultarArchivosAnexosProyecto(capituloEquipos, subCapituloEquipos, seccionEquipos, apartadoEquipos);
		return this.archivosGeneralEquipos;
	}	
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Pruebas de Verificacion".
	 * @param capituloPruebas
	 * @param subCapituloPruebas
	 * @param seccionPruebas
	 * @param apartadoPruebas
	 * @return lista de archivos anexos.
	 */
	public void consultarDesglosePruebasCargaPagina(String capituloPruebas, String subCapituloPruebas, 
			String seccionPruebas,String apartadoPruebas) {		
				
		this.archivosGeneralPruebas = consultarArchivosAnexosProyecto(capituloPruebas, subCapituloPruebas, seccionPruebas, apartadoPruebas);
	}
	
	public List<ArchivosProyecto> consultarDesglosePruebas(String capituloPruebas, String subCapituloPruebas, 
			String seccionPruebas,String apartadoPruebas) {		
				
		this.archivosGeneralPruebas = consultarArchivosAnexosProyecto(capituloPruebas, subCapituloPruebas, seccionPruebas, apartadoPruebas);
		return this.archivosGeneralPruebas;
	}	
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Condiciones de Operacion".
	 * @param capituloPruebas
	 * @param subCapituloPruebas
	 * @param seccionPruebas
	 * @param apartadoPruebas
	 * @return lista de archivos anexos.
	 */
	public void consultarDesgloseCondicionesCargaPagina(String capituloPruebas, String subCapituloPruebas, 
			String seccionPruebas,String apartadoPruebas) {		
				
		this.archivosGeneralCondiciones = consultarArchivosAnexosProyecto(capituloPruebas, subCapituloPruebas, seccionPruebas, apartadoPruebas);
	}
	
	public List<ArchivosProyecto> consultarDesgloseCondiciones(String capituloPruebas, String subCapituloPruebas, 
			String seccionPruebas,String apartadoPruebas) {		
				
		this.archivosGeneralCondiciones = consultarArchivosAnexosProyecto(capituloPruebas, subCapituloPruebas, seccionPruebas, apartadoPruebas);
		return this.archivosGeneralCondiciones;
	}	
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Especificacion".
	 * @param capituloPruebas
	 * @param subCapituloPruebas
	 * @param seccionPruebas
	 * @param apartadoPruebas
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseEspecificacionCargaPagina(String capituloPruebas, String subCapituloPruebas, 
			String seccionPruebas,String apartadoPruebas) {		
				
		this.archivosGeneralEspecificacion = consultarArchivosAnexosProyecto(capituloPruebas, subCapituloPruebas, seccionPruebas, apartadoPruebas);
	}
	
	public List<ArchivosProyecto> consultarDesgloseEspecificacion(String capituloPruebas, String subCapituloPruebas, 
			String seccionPruebas,String apartadoPruebas) {		
				
		this.archivosGeneralEspecificacion = consultarArchivosAnexosProyecto(capituloPruebas, subCapituloPruebas, seccionPruebas, apartadoPruebas);
		return this.archivosGeneralEspecificacion;
	}	
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Aislamiento".
	 * @param capituloPruebas
	 * @param subCapituloPruebas
	 * @param seccionPruebas
	 * @param apartadoPruebas
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseAislamientoCargaPagina(String capituloPruebas, String subCapituloPruebas, 
			String seccionPruebas,String apartadoPruebas) {		
				
		this.archivosGeneralAislamiento = consultarArchivosAnexosProyecto(capituloPruebas, subCapituloPruebas, seccionPruebas, apartadoPruebas);
	}
	
	public List<ArchivosProyecto> consultarDesgloseAislamiento(String capituloPruebas, String subCapituloPruebas, 
			String seccionPruebas,String apartadoPruebas) {		
				
		this.archivosGeneralAislamiento = consultarArchivosAnexosProyecto(capituloPruebas, subCapituloPruebas, seccionPruebas, apartadoPruebas);
		return this.archivosGeneralAislamiento;
	}	
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Accidentes".
	 * @param capituloAccidentes
	 * @param subCapituloAccidentes
	 * @param seccionAccidentes
	 * @param apartadoAccidentes
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseAccidentesCargaPagina(String capituloAccidentes, String subCapituloAccidentes, 
			String seccionAccidentes,String apartadoAccidentes) {		
				
		this.archivosGeneralAccidentes = consultarArchivosAnexosProyecto(capituloAccidentes, subCapituloAccidentes, seccionAccidentes, apartadoAccidentes);
	}
	
	public List<ArchivosProyecto> consultarDesgloseAccidentes(String capituloAccidentes, String subCapituloAccidentes, 
			String seccionAccidentes,String apartadoAccidentes) {		
				
		this.archivosGeneralAccidentes = consultarArchivosAnexosProyecto(capituloAccidentes, subCapituloAccidentes, seccionAccidentes, apartadoAccidentes);
		return this.archivosGeneralAccidentes;
	}	
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Metodologias".
	 * @param capituloMetodologias
	 * @param subCapituloMetodologias
	 * @param seccionMetodologias
	 * @param apartadoMetodologias
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseMetodologiasCargaPagina(String capituloMetodologias, String subCapituloMetodologias, 
			String seccionMetodologias,String apartadoMetodologias) {		
				
		this.archivosGeneralMetodologias = consultarArchivosAnexosProyecto(capituloMetodologias, subCapituloMetodologias, seccionMetodologias, apartadoMetodologias);
	}
	
	public List<ArchivosProyecto> consultarDesgloseMetodologias(String capituloMetodologias, String subCapituloMetodologias, 
			String seccionMetodologias,String apartadoMetodologias) {		
				
		this.archivosGeneralMetodologias = consultarArchivosAnexosProyecto(capituloMetodologias, subCapituloMetodologias, seccionMetodologias, apartadoMetodologias);
		return this.archivosGeneralMetodologias;
	}	
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Radios Potenciales de Afectacion".
	 * @param capituloRadios
	 * @param subCapituloRadios
	 * @param seccionRadios
	 * @param apartadoRadios
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseRadiosCargaArchivo(String capituloRadios, String subCapituloRadios, 
			String seccionRadios,String apartadoRadios) {		
		
		capituloUbicado = "3";
				
		this.archivosGeneralRadios = consultarArchivosAnexosProyecto(capituloRadios, subCapituloRadios, seccionRadios, apartadoRadios);
	}
	
	public List<ArchivosProyecto> consultarDesgloseRadios(String capituloRadios, String subCapituloRadios, 
			String seccionRadios,String apartadoRadios) {		
		
		capituloUbicado = "3";
				
		this.archivosGeneralRadios = consultarArchivosAnexosProyecto(capituloRadios, subCapituloRadios, seccionRadios, apartadoRadios);
		return this.archivosGeneralRadios;
	}	
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Interacciones de Riesgo".
	 * @param capituloInteracciones
	 * @param subCapituloInteracciones
	 * @param seccionInteracciones
	 * @param apartadoInteracciones
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseInteraccionesCargaPagina(String capituloInteracciones, String subCapituloInteracciones, 
			String seccionInteracciones,String apartadoInteracciones) {		
				
		this.archivosGeneralInteracciones = consultarArchivosAnexosProyecto(capituloInteracciones, subCapituloInteracciones, seccionInteracciones, apartadoInteracciones);
	}
	
	public List<ArchivosProyecto> consultarDesgloseInteracciones(String capituloInteracciones, String subCapituloInteracciones, 
			String seccionInteracciones,String apartadoInteracciones) {		
				
		this.archivosGeneralInteracciones = consultarArchivosAnexosProyecto(capituloInteracciones, subCapituloInteracciones, seccionInteracciones, apartadoInteracciones);
		return this.archivosGeneralInteracciones;
	}
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Efectos sobre el Area de Influencia".
	 * @param capituloEfectos
	 * @param subCapituloEfectos
	 * @param seccionEfectos
	 * @param apartadoEfectos
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseEfectosCargaPagina(String capituloEfectos, String subCapituloEfectos, 
			String seccionEfectos,String apartadoEfectos) {		
				
		this.archivosGeneralEfectos = consultarArchivosAnexosProyecto(capituloEfectos, subCapituloEfectos, seccionEfectos, apartadoEfectos);
	}
	
	public List<ArchivosProyecto> consultarDesgloseEfectos(String capituloEfectos, String subCapituloEfectos, 
			String seccionEfectos,String apartadoEfectos) {		
				
		this.archivosGeneralEfectos = consultarArchivosAnexosProyecto(capituloEfectos, subCapituloEfectos, seccionEfectos, apartadoEfectos);
		return this.archivosGeneralEfectos;
	}	
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Recomendaciones Tecnico/Operativas".
	 * @param capituloRecomendaciones
	 * @param subCapituloRecomendaciones
	 * @param seccionRecomendaciones
	 * @param apartadoRecomendaciones
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseRecomendacionesCargaPagina(String capituloRecomendaciones, String subCapituloRecomendaciones, 
			String seccionRecomendaciones,String apartadoRecomendaciones) {		
		
		capituloUbicado = "4";
				
		this.archivosGeneralRecomendaciones = consultarArchivosAnexosProyecto(capituloRecomendaciones, subCapituloRecomendaciones, seccionRecomendaciones, apartadoRecomendaciones);
	}
	
	public List<ArchivosProyecto> consultarDesgloseRecomendaciones(String capituloRecomendaciones, String subCapituloRecomendaciones, 
			String seccionRecomendaciones,String apartadoRecomendaciones) {		
		
		capituloUbicado = "4";
				
		this.archivosGeneralRecomendaciones = consultarArchivosAnexosProyecto(capituloRecomendaciones, subCapituloRecomendaciones, seccionRecomendaciones, apartadoRecomendaciones);
		return this.archivosGeneralRecomendaciones;
	}	
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Sistemas de Seguridad".
	 * @param capituloSistemas
	 * @param subCapituloSistemas
	 * @param seccionSistemas
	 * @param apartadoSistemas
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseSistemasCargaPagina(String capituloSistemas, String subCapituloSistemas, 
			String seccionSistemas,String apartadoSistemas) {		
				
		this.archivosGeneralSistemas = consultarArchivosAnexosProyecto(capituloSistemas, subCapituloSistemas, seccionSistemas, apartadoSistemas);
	}
	
	public List<ArchivosProyecto> consultarDesgloseSistemas(String capituloSistemas, String subCapituloSistemas, 
			String seccionSistemas,String apartadoSistemas) {		
				
		this.archivosGeneralSistemas = consultarArchivosAnexosProyecto(capituloSistemas, subCapituloSistemas, seccionSistemas, apartadoSistemas);
		return this.archivosGeneralSistemas;
	}
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Medidas Preventivas".
	 * @param capituloMedidas
	 * @param subCapituloMedidas
	 * @param seccionMedidas
	 * @param apartadoMedidas
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseMedidasCargaPagina(String capituloMedidas, String subCapituloMedidas, 
			String seccionMedidas,String apartadoMedidas) {		
				
		this.archivosGeneralMedidas = consultarArchivosAnexosProyecto(capituloMedidas, subCapituloMedidas, seccionMedidas, apartadoMedidas);
	}
	
	public List<ArchivosProyecto> consultarDesgloseMedidas(String capituloMedidas, String subCapituloMedidas, 
			String seccionMedidas,String apartadoMedidas) {		
				
		this.archivosGeneralMedidas = consultarArchivosAnexosProyecto(capituloMedidas, subCapituloMedidas, seccionMedidas, apartadoMedidas);
		return this.archivosGeneralMedidas;
	}	
	
	/**
	 * Permite consulta la lista de archivos de los anexos de la seccion "Planos de localizacion y Fotografias".
	 * @param capituloPlanos
	 * @param subCapituloPlanos
	 * @param seccionPlanos
	 * @param apartadoPlanos
	 * @return la lista de archivos de anexos.
	 */
	public void consultarDesglosePlanosCargaPagina(String capituloPlanos,String subCapituloPlanos, 
			String seccionPlanos, String apartadoPlanos) {	
		
		capituloUbicado = "6";
		
		this.archivosGeneralPlanos = consultarArchivosAnexosProyecto(capituloPlanos, subCapituloPlanos, seccionPlanos, apartadoPlanos);
	}
	
	public List<ArchivosProyecto> consultarDesglosePlanos(String capituloPlanos,String subCapituloPlanos, 
			String seccionPlanos, String apartadoPlanos) {	
		
		capituloUbicado = "6";
		
		this.archivosGeneralPlanos = consultarArchivosAnexosProyecto(capituloPlanos, subCapituloPlanos, seccionPlanos, apartadoPlanos);
		return this.archivosGeneralPlanos;
	}
	
	
	/**
	 * Permite consultar la lista de archivos de los anexos de la seccion "Otros Anexos".
	 * @param capituloOtros
	 * @param subCapituloOtros
	 * @param seccionOtros
	 * @param apartadoOtros
	 * @return la lista de archivos anexos.
	 */
	public void consultarDesgloseOtrosCargaPagina(String capituloOtros, String subCapituloOtros, 
			String seccionOtros,String apartadoOtros) {		
				
		this.archivosGeneralOtros = consultarArchivosAnexosProyecto(capituloOtros, subCapituloOtros, seccionOtros, apartadoOtros);
	}
	
	public List<ArchivosProyecto> consultarDesgloseOtros(String capituloOtros, String subCapituloOtros, 
			String seccionOtros,String apartadoOtros) {		
				
		this.archivosGeneralOtros = consultarArchivosAnexosProyecto(capituloOtros, subCapituloOtros, seccionOtros, apartadoOtros);
		return this.archivosGeneralOtros;
	}	
	
	// ================================================ EJECUCION DE TRANSACCIONES =========================================== //
	
	/**
	 * Guarda el archivo en la BD y el Servidor
	 */
	public void guardarArchivo() {

		id = miaDao.getMaxArchivoEtapaProyecto(this.folioProyecto, this.serialProyecto, 
				this.archivoTemporal.getCapituloId(), this.archivoTemporal.getSubCapituloId(), this.archivoTemporal.getSeccionId(), this.archivoTemporal.getApartadoId());

		this.archivoTemporal.setId(id);
		try {

			
			bandera = cargaBean.GuardaAnexos(archivoTemporal);
			if (bandera == true) {
			

			String capitulo = String.valueOf(this.archivoTemporal.getCapituloId());
			String subCapitulo = String.valueOf(this.archivoTemporal.getSubCapituloId());
			String seccion = String.valueOf(this.archivoTemporal.getSeccionId());
			String apartado = String.valueOf(this.archivoTemporal.getApartadoId());
			
			//Si se selecciono la seccion de CONCLUSIONES PARA GUARDAR
			if (seccionSeleccionada.equals(ArchivosAnexosEnum.CONCLUSIONES.getSubCapSeleccionado())) {	
				archivosGeneralConclusiones = consultarDesgloseConclusiones(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.SITUACIONGENERAL.getSubCapSeleccionado())) {
				archivosGeneralSituacionGen = consultarDesgloseSituacionGen(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.INFORMETECNICO.getSubCapSeleccionado())) {
				archivosGeneralInformeTec = consultarDesgloseInformeTec(capitulo, subCapitulo, seccion, apartado);			
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.BASESDISENIO.getSubCapSeleccionado())) {
				archivosGeneralBases = consultarDesgloseBases(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.PROYECTOCIVIL.getSubCapSeleccionado())) {
				archivosGeneralCivil = consultarDesgloseCivil(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.PROYECTOMECANICO.getSubCapSeleccionado())) {
				archivosGeneralMecanico = consultarDesgloseMecanico(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.PROYECTOINCENDIO.getSubCapSeleccionado())) {
				archivosGeneralIncendio = consultarDesgloseIncendio(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.DESCRIPCIONDETALLADA.getSubCapSeleccionado())) {
				archivosGeneralDescripcion = consultarDesgloseDescripcion(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.ALMACENAMIENTO.getSubCapSeleccionado())) {
				archivosGeneralAlmacenamiento = consultarDesgloseAlmacenamiento(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.EQUIPOSAUX.getSubCapSeleccionado())) {
				archivosGeneralEquipos = consultarDesgloseEquipos(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.PRUEBASVERIFICACION.getSubCapSeleccionado())) {
				archivosGeneralPruebas = consultarDesglosePruebas(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.CONDICIONES.getSubCapSeleccionado())) {
				archivosGeneralCondiciones = consultarDesgloseCondiciones(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.ESPECIFICACION.getSubCapSeleccionado())) {
				archivosGeneralEspecificacion = consultarDesgloseEspecificacion(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.AISLAMIENTO.getSubCapSeleccionado())) {
				archivosGeneralAislamiento = consultarDesgloseAislamiento(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.ANTECEDENTESACCIDENTES.getSubCapSeleccionado())) {
				archivosGeneralAccidentes = consultarDesgloseAccidentes(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.METODOLOGIAS.getSubCapSeleccionado())) {
				archivosGeneralMetodologias = consultarDesgloseMetodologias(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.RADIOS.getSubCapSeleccionado())) {
				archivosGeneralRadios = consultarDesgloseRadios(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.INTERACCIONES.getSubCapSeleccionado())) {
				archivosGeneralInteracciones = consultarDesgloseInteracciones(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.EFECTOS.getSubCapSeleccionado())) {
				archivosGeneralEfectos = consultarDesgloseEfectos(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.RECOMENDACIONES.getSubCapSeleccionado())) {
				archivosGeneralRecomendaciones = consultarDesgloseRecomendaciones(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.SISTEMAS.getSubCapSeleccionado())) {
				archivosGeneralSistemas = consultarDesgloseSistemas(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.MEDIDAS.getSubCapSeleccionado())) {
				archivosGeneralMedidas = consultarDesgloseMedidas(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.PLANOS.getSubCapSeleccionado())) {
				archivosGeneralPlanos = consultarDesglosePlanos(capitulo, subCapitulo, seccion, apartado);
			} else if (seccionSeleccionada.equals(ArchivosAnexosEnum.OTROS.getSubCapSeleccionado())) {
				archivosGeneralOtros = consultarDesgloseOtros(capitulo, subCapitulo, seccion, apartado);
			}	
			
			msj = "";
			cargaBean = new CargaBean();
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalSubirArchivo.hide();modalAlertArchivo.show()");
			}else {
				msj = "Se requiere cargar un archivo";
				
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Hubo un error en el contenido del archivo o al guardar", null));
			e.printStackTrace();
		}
				
		cargaBean = new CargaBean();
	}	
	
	/**
	 * Permite eliminar el o los archivos anexos seleccionados.
	 */
	public void eliminarArchivosAnexosProyecto() {
		try {		
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.CONCLUSIONES.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosConclusionesSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());
				}				

				archivosGeneralConclusiones = consultarDesgloseConclusiones(this.capitulo, this.subCapitulo, this.seccion, this.apartado);
				listaArchivosConclusionesSeleccionados = new ArrayList<ArchivosProyecto>();
			}

			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.SITUACIONGENERAL.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosSituacionGenSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}	
				
				archivosGeneralSituacionGen = consultarDesgloseSituacionGen(this.capitulo, this.subCapitulo, this.seccion, this.apartado);
				listaArchivosSituacionGenSeleccionados = new ArrayList<ArchivosProyecto>();
			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.INFORMETECNICO.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosInformeTecSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralInformeTec = consultarDesgloseInformeTec(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosInformeTecSeleccionados = new ArrayList<ArchivosProyecto>();

			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.BASESDISENIO.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosBaseSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralBases = consultarDesgloseBases(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosBaseSeleccionados = new ArrayList<ArchivosProyecto>();

			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.PROYECTOCIVIL.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosCivilSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralCivil = consultarDesgloseCivil(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosCivilSeleccionados = new ArrayList<ArchivosProyecto>();

			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.PROYECTOMECANICO.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosMecanicoSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralMecanico = consultarDesgloseMecanico(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosMecanicoSeleccionados = new ArrayList<ArchivosProyecto>();

			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.PROYECTOINCENDIO.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosIncendioSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralIncendio = consultarDesgloseIncendio(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosIncendioSeleccionados = new ArrayList<ArchivosProyecto>();
			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.DESCRIPCIONDETALLADA.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosDescripcionSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralDescripcion = consultarDesgloseDescripcion(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosDescripcionSeleccionados = new ArrayList<ArchivosProyecto>();

			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.ALMACENAMIENTO.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosAlmacenamientoSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralAlmacenamiento = consultarDesgloseAlmacenamiento(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosAlmacenamientoSeleccionados = new ArrayList<ArchivosProyecto>();
			}	
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.EQUIPOSAUX.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosEquiposSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralEquipos = consultarDesgloseEquipos(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosEquiposSeleccionados = new ArrayList<ArchivosProyecto>();

			}			
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.PRUEBASVERIFICACION.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosPruebasSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralPruebas = consultarDesglosePruebas(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosPruebasSeleccionados = new ArrayList<ArchivosProyecto>();

			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.CONDICIONES.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosCondicionesSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralCondiciones = consultarDesgloseCondiciones(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosCondicionesSeleccionados = new ArrayList<ArchivosProyecto>();

			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.ESPECIFICACION.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosEspecificacionSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralEspecificacion = consultarDesgloseEspecificacion(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosEspecificacionSeleccionados = new ArrayList<ArchivosProyecto>();

			}			
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.AISLAMIENTO.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosAislamientoSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralAislamiento = consultarDesgloseAislamiento(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosAislamientoSeleccionados = new ArrayList<ArchivosProyecto>();
			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.ANTECEDENTESACCIDENTES.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosAccidentesSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralAccidentes = consultarDesgloseAccidentes(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosAccidentesSeleccionados = new ArrayList<ArchivosProyecto>();

			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.METODOLOGIAS.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosMetodologiasSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralMetodologias = consultarDesgloseMetodologias(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosMetodologiasSeleccionados = new ArrayList<ArchivosProyecto>();

			}			
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.RADIOS.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosRadiosSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralRadios = consultarDesgloseRadios(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosRadiosSeleccionados = new ArrayList<ArchivosProyecto>();

			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.INTERACCIONES.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosInteraccionesSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralInteracciones = consultarDesgloseInteracciones(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosInteraccionesSeleccionados = new ArrayList<ArchivosProyecto>();

			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.EFECTOS.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosEfectosSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralEfectos = consultarDesgloseEfectos(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosEfectosSeleccionados = new ArrayList<ArchivosProyecto>();

			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.RECOMENDACIONES.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosRecomendacionesSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralRecomendaciones = consultarDesgloseRecomendaciones(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosRecomendacionesSeleccionados = new ArrayList<ArchivosProyecto>();

				
			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.SISTEMAS.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosSistemasSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralSistemas = consultarDesgloseSistemas(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosSistemasSeleccionados = new ArrayList<ArchivosProyecto>();

			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.MEDIDAS.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosMedidasSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralMedidas = consultarDesgloseMedidas(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosMedidasSeleccionados = new ArrayList<ArchivosProyecto>();

			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.PLANOS.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosPlanosSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralPlanos = consultarDesglosePlanos(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosPlanosSeleccionados = new ArrayList<ArchivosProyecto>();

			}
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.OTROS.getSubCapSeleccionado())) {
				for (ArchivosProyecto archivosProyecto : listaArchivosOtrosSeleccionados) {
					archivosAnexosDAO.eliminarArchivoProyecto(this.folioProyecto, this.serialProyecto, archivosProyecto.getCapituloId(),
							archivosProyecto.getSubCapituloId(), archivosProyecto.getSeccionId(), archivosProyecto.getApartadoId(), archivosProyecto.getId());					
				}				

				archivosGeneralOtros = consultarDesgloseOtros(this.capitulo, this.subCapitulo, this.seccion, this.apartado);			
				listaArchivosOtrosSeleccionados = new ArrayList<ArchivosProyecto>();

			}
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("dlgEliminarArchivoAnexo.hide();modalAlertEArchivo.show()");				

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	/**
	 * Permite consultar la lista de Archivos Anexos del Proyecto segun su capitulo, subcapitulo, seccion y apartado.
	 * @param capitulo
	 * @param subCapitulo
	 * @param seccion
	 * @param apartado
	 * @return la lista de archivos anexos.
	 */
	public List<ArchivosProyecto> consultarArchivosAnexosProyecto(String capitulo, String subCapitulo, String seccion, String apartado) {
		archivoTemporal = new ArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capitulo), 
				Short.parseShort(subCapitulo), Short.parseShort(seccion), Short.parseShort(apartado));
		archivosAnexosDAO = new ArchivosAnexosDAO();
		archivosGeneral = archivosAnexosDAO.consultarArchivosProyecto(this.folioProyecto, this.serialProyecto, Short.parseShort(capitulo), 
				Short.parseShort(subCapitulo), Short.parseShort(seccion), Short.parseShort(apartado));
		
		return archivosGeneral;
	}
	
	/**
	 * Permite realizar la edicion del archivo de anexo seleccionado.
	 */
	public void editarArchivo() throws Exception {
		ArchivosProyecto archivoProyectoEditado = new ArchivosProyecto();
		archivoProyectoEditado = archivoEdicion;

		try {
			archivosAnexosDAO.editarArchivoProyecto(archivoProyectoEditado);
			
			if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.CONCLUSIONES.getSubCapSeleccionado())) {
				archivosGeneralConclusiones = consultarDesgloseConclusiones(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));
				
			} else if(this.seccionSeleccionada.equals(ArchivosAnexosEnum.SITUACIONGENERAL.getSubCapSeleccionado())) {
				archivosGeneralSituacionGen = consultarDesgloseSituacionGen(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));
				
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.INFORMETECNICO.getSubCapSeleccionado())) {
				archivosGeneralInformeTec = consultarDesgloseInformeTec(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));							
			
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.BASESDISENIO.getSubCapSeleccionado())) {
				archivosGeneralBases = consultarDesgloseBases(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));							
			
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.PROYECTOCIVIL.getSubCapSeleccionado())) {
				archivosGeneralCivil = consultarDesgloseCivil(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));							
			
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.PROYECTOMECANICO.getSubCapSeleccionado())) {
				archivosGeneralMecanico = consultarDesgloseMecanico(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));							
			
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.PROYECTOINCENDIO.getSubCapSeleccionado())) {
				archivosGeneralIncendio = consultarDesgloseIncendio(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));							
			
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.DESCRIPCIONDETALLADA.getSubCapSeleccionado())) {
				archivosGeneralDescripcion = consultarDesgloseDescripcion(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));							
			
			} else /*if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.HOJASSEGURIDAD.getSubCapSeleccionado())) {
				archivosGeneralHojas = consultarDesgloseHojas(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));	
				
			} else */if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.ALMACENAMIENTO.getSubCapSeleccionado())) {
				archivosGeneralAlmacenamiento = consultarDesgloseAlmacenamiento(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));	
				
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.EQUIPOSAUX.getSubCapSeleccionado())) {
				archivosGeneralEquipos = consultarDesgloseEquipos(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));	
				
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.PRUEBASVERIFICACION.getSubCapSeleccionado())) {
				archivosGeneralPruebas = consultarDesglosePruebas(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));	
				
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.CONDICIONES.getSubCapSeleccionado())) {
				archivosGeneralCondiciones = consultarDesgloseCondiciones(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));	
				
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.ESPECIFICACION.getSubCapSeleccionado())) {
				archivosGeneralEspecificacion = consultarDesgloseEspecificacion(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));	
				
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.AISLAMIENTO.getSubCapSeleccionado())) {
				archivosGeneralAislamiento = consultarDesgloseAislamiento(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));	
				
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.ANTECEDENTESACCIDENTES.getSubCapSeleccionado())) {
				archivosGeneralAccidentes = consultarDesgloseAccidentes(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));	
				
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.METODOLOGIAS.getSubCapSeleccionado())) {
				archivosGeneralMetodologias = consultarDesgloseMetodologias(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));	
				
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.RADIOS.getSubCapSeleccionado())) {
				archivosGeneralRadios = consultarDesgloseRadios(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));	
				
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.INTERACCIONES.getSubCapSeleccionado())) {
				archivosGeneralInteracciones = consultarDesgloseInteracciones(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));
				
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.EFECTOS.getSubCapSeleccionado())) {
				archivosGeneralEfectos = consultarDesgloseEfectos(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));		
				
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.RECOMENDACIONES.getSubCapSeleccionado())) {
				archivosGeneralRecomendaciones = consultarDesgloseRecomendaciones(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));		
				
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.SISTEMAS.getSubCapSeleccionado())) {
				archivosGeneralSistemas = consultarDesgloseSistemas(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));		
				
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.MEDIDAS.getSubCapSeleccionado())) {
				archivosGeneralMedidas = consultarDesgloseMedidas(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));		
				
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.PLANOS.getSubCapSeleccionado())) {
				archivosGeneralPlanos = consultarDesglosePlanos(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));		
				
			} else if (this.seccionSeleccionada.equals(ArchivosAnexosEnum.OTROS.getSubCapSeleccionado())) {
				archivosGeneralOtros = consultarDesgloseOtros(String.valueOf(archivoProyectoEditado.getCapituloId()), 
						String.valueOf(archivoProyectoEditado.getSubCapituloId()), String.valueOf(archivoProyectoEditado.getSeccionId()), 
						String.valueOf(archivoProyectoEditado.getApartadoId()));		
				
			}
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditarArchivo.hide();modalAlertEdArchivo.show()");
		} catch (Exception e) {
			// TODO: handle exception
		}

	}	
	
	/**
	 * Permite abrir el dialog para eliminar archivos.
	 */
	public void abrirModalEliminarArchivos() {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("dlgEliminarArchivoAnexo.show()");
	}
	/**
	 * @return the claveTramite
	 */
	public Integer getClaveTramite() {
		return claveTramite;
	}

	/**
	 * @param claveTramite the claveTramite to set
	 */
	public void setClaveTramite(Integer claveTramite) {
		this.claveTramite = claveTramite;
	}

	/**
	 * @return the proyecto
	 */
	public Proyecto getProyecto() {
		return proyecto;
	}

	/**
	 * @param proyecto the proyecto to set
	 */
	public void setProyecto(Proyecto proyecto) {
		this.proyecto = proyecto;
	}

	/**
	 * @return the id
	 */
	public Short getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Short id) {
		this.id = id;
	}

	/**
	 * @return the archivoTemporal
	 */
	public ArchivosProyecto getArchivoTemporal() {
		return archivoTemporal;
	}

	/**
	 * @param archivoTemporal the archivoTemporal to set
	 */
	public void setArchivoTemporal(ArchivosProyecto archivoTemporal) {
		this.archivoTemporal = archivoTemporal;
	}

	/**
	 * @return the cargaBean
	 */
	public CargaBean getCargaBean() {
		return cargaBean;
	}

	/**
	 * @param cargaBean the cargaBean to set
	 */
	public void setCargaBean(CargaBean cargaBean) {
		this.cargaBean = cargaBean;
	}

	/**
	 * @return the archivosGeneral
	 */
	public List<ArchivosProyecto> getArchivosGeneral() {
		return archivosGeneral;
	}

	/**
	 * @param archivosGeneral the archivosGeneral to set
	 */
	public void setArchivosGeneral(List<ArchivosProyecto> archivosGeneral) {
		this.archivosGeneral = archivosGeneral;
	}

	/**
	 * @return the listaArchivosSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosSeleccionados() {
		return listaArchivosSeleccionados;
	}

	/**
	 * @param listaArchivosSeleccionados the listaArchivosSeleccionados to set
	 */
	public void setListaArchivosSeleccionados(List<ArchivosProyecto> listaArchivosSeleccionados) {
		this.listaArchivosSeleccionados = listaArchivosSeleccionados;
	}

	/**
	 * @return the archivoEdicion
	 */
	public ArchivosProyecto getArchivoEdicion() {
		return archivoEdicion;
	}

	/**
	 * @param archivoEdicion the archivoEdicion to set
	 */
	public void setArchivoEdicion(ArchivosProyecto archivoEdicion) {
		this.archivoEdicion = archivoEdicion;
	}

	/**
	 * @return the folioProyecto
	 */
	public String getFolioProyecto() {
		return folioProyecto;
	}

	/**
	 * @param folioProyecto the folioProyecto to set
	 */
	public void setFolioProyecto(String folioProyecto) {
		this.folioProyecto = folioProyecto;
	}

	/**
	 * @return the serialProyecto
	 */
	public short getSerialProyecto() {
		return serialProyecto;
	}

	/**
	 * @param serialProyecto the serialProyecto to set
	 */
	public void setSerialProyecto(short serialProyecto) {
		this.serialProyecto = serialProyecto;
	}

	/**
	 * @return the seccionSeleccionada
	 */
	public String getSeccionSeleccionada() {
		return seccionSeleccionada;
	}

	/**
	 * @param seccionSeleccionada the seccionSeleccionada to set
	 */
	public void setSeccionSeleccionada(String seccionSeleccionada) {
		this.seccionSeleccionada = seccionSeleccionada;
	}

	/**
	 * @return the archivosGeneralConclusiones
	 */
	public List<ArchivosProyecto> getArchivosGeneralConclusiones() {
		return archivosGeneralConclusiones;
	}

	/**
	 * @param archivosGeneralConclusiones the archivosGeneralConclusiones to set
	 */
	public void setArchivosGeneralConclusiones(List<ArchivosProyecto> archivosGeneralConclusiones) {
		this.archivosGeneralConclusiones = archivosGeneralConclusiones;
	}

	/**
	 * @return the archivosGeneralSituacionGen
	 */
	public List<ArchivosProyecto> getArchivosGeneralSituacionGen() {
		return archivosGeneralSituacionGen;
	}

	/**
	 * @param archivosGeneralSituacionGen the archivosGeneralSituacionGen to set
	 */
	public void setArchivosGeneralSituacionGen(List<ArchivosProyecto> archivosGeneralSituacionGen) {
		this.archivosGeneralSituacionGen = archivosGeneralSituacionGen;
	}

	/**
	 * @return the archivosGeneralInformeTec
	 */
	public List<ArchivosProyecto> getArchivosGeneralInformeTec() {
		return archivosGeneralInformeTec;
	}

	/**
	 * @param archivosGeneralInformeTec the archivosGeneralInformeTec to set
	 */
	public void setArchivosGeneralInformeTec(List<ArchivosProyecto> archivosGeneralInformeTec) {
		this.archivosGeneralInformeTec = archivosGeneralInformeTec;
	}

	/**
	 * @return the listaArchivosConclusionesSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosConclusionesSeleccionados() {
		return listaArchivosConclusionesSeleccionados;
	}

	/**
	 * @param listaArchivosConclusionesSeleccionados the listaArchivosConclusionesSeleccionados to set
	 */
	public void setListaArchivosConclusionesSeleccionados(List<ArchivosProyecto> listaArchivosConclusionesSeleccionados) {
		this.listaArchivosConclusionesSeleccionados = listaArchivosConclusionesSeleccionados;
	}

	/**
	 * @return the listaArchivosSituacionGenSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosSituacionGenSeleccionados() {
		return listaArchivosSituacionGenSeleccionados;
	}

	/**
	 * @param listaArchivosSituacionGenSeleccionados the listaArchivosSituacionGenSeleccionados to set
	 */
	public void setListaArchivosSituacionGenSeleccionados(List<ArchivosProyecto> listaArchivosSituacionGenSeleccionados) {
		this.listaArchivosSituacionGenSeleccionados = listaArchivosSituacionGenSeleccionados;
	}

	/**
	 * @return the listaArchivosInformeTecSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosInformeTecSeleccionados() {
		return listaArchivosInformeTecSeleccionados;
	}

	/**
	 * @param listaArchivosInformeTecSeleccionados the listaArchivosInformeTecSeleccionados to set
	 */
	public void setListaArchivosInformeTecSeleccionados(List<ArchivosProyecto> listaArchivosInformeTecSeleccionados) {
		this.listaArchivosInformeTecSeleccionados = listaArchivosInformeTecSeleccionados;
	}

	/**
	 * @return the capitulo
	 */
	public String getCapitulo() {
		return capitulo;
	}

	/**
	 * @param capitulo the capitulo to set
	 */
	public void setCapitulo(String capitulo) {
		this.capitulo = capitulo;
	}

	/**
	 * @return the subCapitulo
	 */
	public String getSubCapitulo() {
		return subCapitulo;
	}

	/**
	 * @param subCapitulo the subCapitulo to set
	 */
	public void setSubCapitulo(String subCapitulo) {
		this.subCapitulo = subCapitulo;
	}

	/**
	 * @return the seccion
	 */
	public String getSeccion() {
		return seccion;
	}

	/**
	 * @param seccion the seccion to set
	 */
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	/**
	 * @return the apartado
	 */
	public String getApartado() {
		return apartado;
	}

	/**
	 * @param apartado the apartado to set
	 */
	public void setApartado(String apartado) {
		this.apartado = apartado;
	}

	/**
	 * @return the listaArchivosBaseSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosBaseSeleccionados() {
		return listaArchivosBaseSeleccionados;
	}

	/**
	 * @param listaArchivosBaseSeleccionados the listaArchivosBaseSeleccionados to set
	 */
	public void setListaArchivosBaseSeleccionados(List<ArchivosProyecto> listaArchivosBaseSeleccionados) {
		this.listaArchivosBaseSeleccionados = listaArchivosBaseSeleccionados;
	}

	/**
	 * @return the archivosGeneralBases
	 */
	public List<ArchivosProyecto> getArchivosGeneralBases() {
		return archivosGeneralBases;
	}

	/**
	 * @param archivosGeneralBases the archivosGeneralBases to set
	 */
	public void setArchivosGeneralBases(List<ArchivosProyecto> archivosGeneralBases) {
		this.archivosGeneralBases = archivosGeneralBases;
	}

	/**
	 * @return the capituloUbicado
	 */
	public String getCapituloUbicado() {
		return capituloUbicado;
	}

	/**
	 * @param capituloUbicado the capituloUbicado to set
	 */
	public void setCapituloUbicado(String capituloUbicado) {
		this.capituloUbicado = capituloUbicado;
	}

	/**
	 * @return the archivosGeneralCivil
	 */
	public List<ArchivosProyecto> getArchivosGeneralCivil() {
		return archivosGeneralCivil;
	}

	/**
	 * @param archivosGeneralCivil the archivosGeneralCivil to set
	 */
	public void setArchivosGeneralCivil(List<ArchivosProyecto> archivosGeneralCivil) {
		this.archivosGeneralCivil = archivosGeneralCivil;
	}

	/**
	 * @return the listaArchivosCivilSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosCivilSeleccionados() {
		return listaArchivosCivilSeleccionados;
	}

	/**
	 * @param listaArchivosCivilSeleccionados the listaArchivosCivilSeleccionados to set
	 */
	public void setListaArchivosCivilSeleccionados(List<ArchivosProyecto> listaArchivosCivilSeleccionados) {
		this.listaArchivosCivilSeleccionados = listaArchivosCivilSeleccionados;
	}

	/**
	 * @return the archivosGeneralMecanico
	 */
	public List<ArchivosProyecto> getArchivosGeneralMecanico() {
		return archivosGeneralMecanico;
	}

	/**
	 * @param archivosGeneralMecanico the archivosGeneralMecanico to set
	 */
	public void setArchivosGeneralMecanico(List<ArchivosProyecto> archivosGeneralMecanico) {
		this.archivosGeneralMecanico = archivosGeneralMecanico;
	}

	/**
	 * @return the listaArchivosMecanicoSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosMecanicoSeleccionados() {
		return listaArchivosMecanicoSeleccionados;
	}

	/**
	 * @param listaArchivosMecanicoSeleccionados the listaArchivosMecanicoSeleccionados to set
	 */
	public void setListaArchivosMecanicoSeleccionados(List<ArchivosProyecto> listaArchivosMecanicoSeleccionados) {
		this.listaArchivosMecanicoSeleccionados = listaArchivosMecanicoSeleccionados;
	}

	/**
	 * @return the archivosGeneralIncendio
	 */
	public List<ArchivosProyecto> getArchivosGeneralIncendio() {
		return archivosGeneralIncendio;
	}

	/**
	 * @param archivosGeneralIncendio the archivosGeneralIncendio to set
	 */
	public void setArchivosGeneralIncendio(List<ArchivosProyecto> archivosGeneralIncendio) {
		this.archivosGeneralIncendio = archivosGeneralIncendio;
	}

	/**
	 * @return the listaArchivosIncendioSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosIncendioSeleccionados() {
		return listaArchivosIncendioSeleccionados;
	}

	/**
	 * @param listaArchivosIncendioSeleccionados the listaArchivosIncendioSeleccionados to set
	 */
	public void setListaArchivosIncendioSeleccionados(List<ArchivosProyecto> listaArchivosIncendioSeleccionados) {
		this.listaArchivosIncendioSeleccionados = listaArchivosIncendioSeleccionados;
	}

	/**
	 * @return the archivosGeneralDescripcion
	 */
	public List<ArchivosProyecto> getArchivosGeneralDescripcion() {
		return archivosGeneralDescripcion;
	}

	/**
	 * @param archivosGeneralDescripcion the archivosGeneralDescripcion to set
	 */
	public void setArchivosGeneralDescripcion(List<ArchivosProyecto> archivosGeneralDescripcion) {
		this.archivosGeneralDescripcion = archivosGeneralDescripcion;
	}

	/**
	 * @return the listaArchivosDescripcionSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosDescripcionSeleccionados() {
		return listaArchivosDescripcionSeleccionados;
	}

	/**
	 * @param listaArchivosDescripcionSeleccionados the listaArchivosDescripcionSeleccionados to set
	 */
	public void setListaArchivosDescripcionSeleccionados(List<ArchivosProyecto> listaArchivosDescripcionSeleccionados) {
		this.listaArchivosDescripcionSeleccionados = listaArchivosDescripcionSeleccionados;
	}

	/**
	 * @return the archivosGeneralHojas
	 */
	/*
	public List<ArchivosProyecto> getArchivosGeneralHojas() {
		return archivosGeneralHojas;
	}
    */
	/**
	 * @param archivosGeneralHojas the archivosGeneralHojas to set
	 */
	/*
	public void setArchivosGeneralHojas(List<ArchivosProyecto> archivosGeneralHojas) {
		this.archivosGeneralHojas = archivosGeneralHojas;
	}
	*/
	/**
	 * @return the listaArchivosHojasSeleccionados
	 */
	/*
	public List<ArchivosProyecto> getListaArchivosHojasSeleccionados() {
		return listaArchivosHojasSeleccionados;
	}
	*/
	/**
	 * @param listaArchivosHojasSeleccionados the listaArchivosHojasSeleccionados to set
	 */
	/*
	public void setListaArchivosHojasSeleccionados(List<ArchivosProyecto> listaArchivosHojasSeleccionados) {
		this.listaArchivosHojasSeleccionados = listaArchivosHojasSeleccionados;
	}
	*/
	public List<ArchivosProyecto> getArchivosGeneralRadios() {
		return archivosGeneralRadios;
	}

	public void setArchivosGeneralRadios(List<ArchivosProyecto> archivosGeneralRadios) {
		this.archivosGeneralRadios = archivosGeneralRadios;
	}

	public List<ArchivosProyecto> getArchivosGeneralInteracciones() {
		return archivosGeneralInteracciones;
	}

	public void setArchivosGeneralInteracciones(List<ArchivosProyecto> archivosGeneralInteracciones) {
		this.archivosGeneralInteracciones = archivosGeneralInteracciones;
	}

	public List<ArchivosProyecto> getArchivosGeneralEfectos() {
		return archivosGeneralEfectos;
	}

	public void setArchivosGeneralEfectos(List<ArchivosProyecto> archivosGeneralEfectos) {
		this.archivosGeneralEfectos = archivosGeneralEfectos;
	}

	public List<ArchivosProyecto> getListaArchivosRadiosSeleccionados() {
		return listaArchivosRadiosSeleccionados;
	}

	public void setListaArchivosRadiosSeleccionados(List<ArchivosProyecto> listaArchivosRadiosSeleccionados) {
		this.listaArchivosRadiosSeleccionados = listaArchivosRadiosSeleccionados;
	}

	public List<ArchivosProyecto> getListaArchivosInteraccionesSeleccionados() {
		return listaArchivosInteraccionesSeleccionados;
	}

	public void setListaArchivosInteraccionesSeleccionados(List<ArchivosProyecto> listaArchivosInteraccionesSeleccionados) {
		this.listaArchivosInteraccionesSeleccionados = listaArchivosInteraccionesSeleccionados;
	}

	public List<ArchivosProyecto> getListaArchivosEfectosSeleccionados() {
		return listaArchivosEfectosSeleccionados;
	}

	public void setListaArchivosEfectosSeleccionados(List<ArchivosProyecto> listaArchivosEfectosSeleccionados) {
		this.listaArchivosEfectosSeleccionados = listaArchivosEfectosSeleccionados;
	}

	/**
	 * @return the archivosGeneralAlmacenamiento
	 */
	public List<ArchivosProyecto> getArchivosGeneralAlmacenamiento() {
		return archivosGeneralAlmacenamiento;
	}

	/**
	 * @param archivosGeneralAlmacenamiento the archivosGeneralAlmacenamiento to set
	 */
	public void setArchivosGeneralAlmacenamiento(List<ArchivosProyecto> archivosGeneralAlmacenamiento) {
		this.archivosGeneralAlmacenamiento = archivosGeneralAlmacenamiento;
	}

	/**
	 * @return the archivosGeneralEquipos
	 */
	public List<ArchivosProyecto> getArchivosGeneralEquipos() {
		return archivosGeneralEquipos;
	}

	/**
	 * @param archivosGeneralEquipos the archivosGeneralEquipos to set
	 */
	public void setArchivosGeneralEquipos(List<ArchivosProyecto> archivosGeneralEquipos) {
		this.archivosGeneralEquipos = archivosGeneralEquipos;
	}

	/**
	 * @return the archivosGeneralPruebas
	 */
	public List<ArchivosProyecto> getArchivosGeneralPruebas() {
		return archivosGeneralPruebas;
	}

	/**
	 * @param archivosGeneralPruebas the archivosGeneralPruebas to set
	 */
	public void setArchivosGeneralPruebas(List<ArchivosProyecto> archivosGeneralPruebas) {
		this.archivosGeneralPruebas = archivosGeneralPruebas;
	}

	/**
	 * @return the listaArchivosAlmacenamientoSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosAlmacenamientoSeleccionados() {
		return listaArchivosAlmacenamientoSeleccionados;
	}

	/**
	 * @param listaArchivosAlmacenamientoSeleccionados the listaArchivosAlmacenamientoSeleccionados to set
	 */
	public void setListaArchivosAlmacenamientoSeleccionados(
			List<ArchivosProyecto> listaArchivosAlmacenamientoSeleccionados) {
		this.listaArchivosAlmacenamientoSeleccionados = listaArchivosAlmacenamientoSeleccionados;
	}

	/**
	 * @return the listaArchivosEquiposSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosEquiposSeleccionados() {
		return listaArchivosEquiposSeleccionados;
	}

	/**
	 * @param listaArchivosEquiposSeleccionados the listaArchivosEquiposSeleccionados to set
	 */
	public void setListaArchivosEquiposSeleccionados(List<ArchivosProyecto> listaArchivosEquiposSeleccionados) {
		this.listaArchivosEquiposSeleccionados = listaArchivosEquiposSeleccionados;
	}

	/**
	 * @return the listaArchivosPruebasSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosPruebasSeleccionados() {
		return listaArchivosPruebasSeleccionados;
	}

	/**
	 * @param listaArchivosPruebasSeleccionados the listaArchivosPruebasSeleccionados to set
	 */
	public void setListaArchivosPruebasSeleccionados(List<ArchivosProyecto> listaArchivosPruebasSeleccionados) {
		this.listaArchivosPruebasSeleccionados = listaArchivosPruebasSeleccionados;
	}

	/**
	 * @return the archivosGeneralCondiciones
	 */
	public List<ArchivosProyecto> getArchivosGeneralCondiciones() {
		return archivosGeneralCondiciones;
	}

	/**
	 * @param archivosGeneralCondiciones the archivosGeneralCondiciones to set
	 */
	public void setArchivosGeneralCondiciones(List<ArchivosProyecto> archivosGeneralCondiciones) {
		this.archivosGeneralCondiciones = archivosGeneralCondiciones;
	}

	/**
	 * @return the archivosGeneralEspecificacion
	 */
	public List<ArchivosProyecto> getArchivosGeneralEspecificacion() {
		return archivosGeneralEspecificacion;
	}

	/**
	 * @param archivosGeneralEspecificacion the archivosGeneralEspecificacion to set
	 */
	public void setArchivosGeneralEspecificacion(List<ArchivosProyecto> archivosGeneralEspecificacion) {
		this.archivosGeneralEspecificacion = archivosGeneralEspecificacion;
	}

	/**
	 * @return the archivosGeneralAislamiento
	 */
	public List<ArchivosProyecto> getArchivosGeneralAislamiento() {
		return archivosGeneralAislamiento;
	}

	/**
	 * @param archivosGeneralAislamiento the archivosGeneralAislamiento to set
	 */
	public void setArchivosGeneralAislamiento(List<ArchivosProyecto> archivosGeneralAislamiento) {
		this.archivosGeneralAislamiento = archivosGeneralAislamiento;
	}

	/**
	 * @return the listaArchivosEspecificacionSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosEspecificacionSeleccionados() {
		return listaArchivosEspecificacionSeleccionados;
	}

	/**
	 * @param listaArchivosEspecificacionSeleccionados the listaArchivosEspecificacionSeleccionados to set
	 */
	public void setListaArchivosEspecificacionSeleccionados(
			List<ArchivosProyecto> listaArchivosEspecificacionSeleccionados) {
		this.listaArchivosEspecificacionSeleccionados = listaArchivosEspecificacionSeleccionados;
	}

	/**
	 * @return the listaArchivosAislamientoSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosAislamientoSeleccionados() {
		return listaArchivosAislamientoSeleccionados;
	}

	/**
	 * @param listaArchivosAislamientoSeleccionados the listaArchivosAislamientoSeleccionados to set
	 */
	public void setListaArchivosAislamientoSeleccionados(List<ArchivosProyecto> listaArchivosAislamientoSeleccionados) {
		this.listaArchivosAislamientoSeleccionados = listaArchivosAislamientoSeleccionados;
	}

	/**
	 * @return the listaArchivosCondicionesSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosCondicionesSeleccionados() {
		return listaArchivosCondicionesSeleccionados;
	}

	/**
	 * @param listaArchivosCondicionesSeleccionados the listaArchivosCondicionesSeleccionados to set
	 */
	public void setListaArchivosCondicionesSeleccionados(List<ArchivosProyecto> listaArchivosCondicionesSeleccionados) {
		this.listaArchivosCondicionesSeleccionados = listaArchivosCondicionesSeleccionados;
	}

	public List<ArchivosProyecto> getArchivosGeneralRecomendaciones() {
		return archivosGeneralRecomendaciones;
	}

	public void setArchivosGeneralRecomendaciones(List<ArchivosProyecto> archivosGeneralRecomendaciones) {
		this.archivosGeneralRecomendaciones = archivosGeneralRecomendaciones;
	}

	public List<ArchivosProyecto> getArchivosGeneralSistemas() {
		return archivosGeneralSistemas;
	}

	public void setArchivosGeneralSistemas(List<ArchivosProyecto> archivosGeneralSistemas) {
		this.archivosGeneralSistemas = archivosGeneralSistemas;
	}

	public List<ArchivosProyecto> getArchivosGeneralMedidas() {
		return archivosGeneralMedidas;
	}

	public void setArchivosGeneralMedidas(List<ArchivosProyecto> archivosGeneralMedidas) {
		this.archivosGeneralMedidas = archivosGeneralMedidas;
	}

	public List<ArchivosProyecto> getListaArchivosRecomendacionesSeleccionados() {
		return listaArchivosRecomendacionesSeleccionados;
	}

	public void setListaArchivosRecomendacionesSeleccionados(List<ArchivosProyecto> listaArchivosRecomendacionesSeleccionados) {
		this.listaArchivosRecomendacionesSeleccionados = listaArchivosRecomendacionesSeleccionados;
	}

	public List<ArchivosProyecto> getListaArchivosSistemasSeleccionados() {
		return listaArchivosSistemasSeleccionados;
	}

	public void setListaArchivosSistemasSeleccionados(List<ArchivosProyecto> listaArchivosSistemasSeleccionados) {
		this.listaArchivosSistemasSeleccionados = listaArchivosSistemasSeleccionados;
	}

	public List<ArchivosProyecto> getListaArchivosMedidasSeleccionados() {
		return listaArchivosMedidasSeleccionados;
	}

	public void setListaArchivosMedidasSeleccionados(List<ArchivosProyecto> listaArchivosMedidasSeleccionados) {
		this.listaArchivosMedidasSeleccionados = listaArchivosMedidasSeleccionados;
	}

	/**
	 * @return the archivosGeneralAccidentes
	 */
	public List<ArchivosProyecto> getArchivosGeneralAccidentes() {
		return archivosGeneralAccidentes;
	}

	/**
	 * @param archivosGeneralAccidentes the archivosGeneralAccidentes to set
	 */
	public void setArchivosGeneralAccidentes(List<ArchivosProyecto> archivosGeneralAccidentes) {
		this.archivosGeneralAccidentes = archivosGeneralAccidentes;
	}

	/**
	 * @return the archivosGeneralMetodologias
	 */
	public List<ArchivosProyecto> getArchivosGeneralMetodologias() {
		return archivosGeneralMetodologias;
	}

	/**
	 * @param archivosGeneralMetodologias the archivosGeneralMetodologias to set
	 */
	public void setArchivosGeneralMetodologias(List<ArchivosProyecto> archivosGeneralMetodologias) {
		this.archivosGeneralMetodologias = archivosGeneralMetodologias;
	}

	/**
	 * @return the listaArchivosAccidentesSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosAccidentesSeleccionados() {
		return listaArchivosAccidentesSeleccionados;
	}

	/**
	 * @param listaArchivosAccidentesSeleccionados the listaArchivosAccidentesSeleccionados to set
	 */
	public void setListaArchivosAccidentesSeleccionados(List<ArchivosProyecto> listaArchivosAccidentesSeleccionados) {
		this.listaArchivosAccidentesSeleccionados = listaArchivosAccidentesSeleccionados;
	}

	/**
	 * @return the listaArchivosMetodologiasSeleccionados
	 */
	public List<ArchivosProyecto> getListaArchivosMetodologiasSeleccionados() {
		return listaArchivosMetodologiasSeleccionados;
	}

	/**
	 * @param listaArchivosMetodologiasSeleccionados the listaArchivosMetodologiasSeleccionados to set
	 */
	public void setListaArchivosMetodologiasSeleccionados(List<ArchivosProyecto> listaArchivosMetodologiasSeleccionados) {
		this.listaArchivosMetodologiasSeleccionados = listaArchivosMetodologiasSeleccionados;
	}

	public List<ArchivosProyecto> getArchivosGeneralPlanos() {
		return archivosGeneralPlanos;
	}

	public void setArchivosGeneralPlanos(List<ArchivosProyecto> archivosGeneralPlanos) {
		this.archivosGeneralPlanos = archivosGeneralPlanos;
	}

	public List<ArchivosProyecto> getArchivosGeneralOtros() {
		return archivosGeneralOtros;
	}

	public void setArchivosGeneralOtros(List<ArchivosProyecto> archivosGeneralOtros) {
		this.archivosGeneralOtros = archivosGeneralOtros;
	}

	public List<ArchivosProyecto> getListaArchivosPlanosSeleccionados() {
		return listaArchivosPlanosSeleccionados;
	}

	public void setListaArchivosPlanosSeleccionados(List<ArchivosProyecto> listaArchivosPlanosSeleccionados) {
		this.listaArchivosPlanosSeleccionados = listaArchivosPlanosSeleccionados;
	}

	public List<ArchivosProyecto> getListaArchivosOtrosSeleccionados() {
		return listaArchivosOtrosSeleccionados;
	}

	public void setListaArchivosOtrosSeleccionados(List<ArchivosProyecto> listaArchivosOtrosSeleccionados) {
		this.listaArchivosOtrosSeleccionados = listaArchivosOtrosSeleccionados;
	}

	public String getMsj() {
		return msj;
	}

	public void setMsj(String msj) {
		this.msj = msj;
	}

	public boolean isBandera() {
		return bandera;
	}

	public void setBandera(boolean bandera) {
		this.bandera = bandera;
	}

	public long getIndexRow() {
		return indexRow;
	}

	public void setIndexRow(long indexRow) {
		this.indexRow = indexRow;
	}
}
