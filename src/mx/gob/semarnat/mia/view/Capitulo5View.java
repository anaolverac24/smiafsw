/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import mx.gob.semarnat.mia.dao.MiaDao;
import mx.gob.semarnat.mia.model.CatEtapa;
import mx.gob.semarnat.mia.model.CatTipoImpacto;
import mx.gob.semarnat.mia.model.ImpacAmbProyecto;
import mx.gob.semarnat.mia.model.MedPrevImpactProy;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.catalogos.SectorProyecto;
import mx.gob.semarnat.mia.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.mia.validacion.Validacion;

import org.primefaces.context.RequestContext;

/**
 *
 * @author Ramon Hidalgo 
 */
@ManagedBean
@ViewScoped
public class Capitulo5View extends CapitulosComentarios implements Serializable {

    private String seccion;
    //private MiaDao miaDao = new MiaDao();
    private Proyecto proyecto = new Proyecto();
    private String sector = "";
    private Integer claveTramite = 0;
    private short subsec = 0;
    private List<ImpacAmbProyecto> evaluacion = new ArrayList<ImpacAmbProyecto>();
    private List<ImpacAmbProyecto> selectedEva = new ArrayList<ImpacAmbProyecto>();
    private ImpacAmbProyecto evaNuevo = new ImpacAmbProyecto();
    private ImpacAmbProyecto editEva = new ImpacAmbProyecto();
    private ImpacAmbProyecto reservedEva = new ImpacAmbProyecto();
    private MedPrevImpactProy medidaPreventiva = new MedPrevImpactProy();
    private MedPrevImpactProy editMedidaPreventiva = new MedPrevImpactProy();
    private String descripcionEva;
    
    /**
     * Numero secuencial del registro "Resultados de evaluación de los impactos ambientales" a editar
     */
    private long rowIndexResEvaIAEditar;


	public Capitulo5View() {
        // Recupera proyecto de sesion
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
        claveTramite =  (Integer) context.getExternalContext().getSessionMap().get("claveTramite");
        subsec = (Short) context.getExternalContext().getSessionMap().get("subsector");
        //Cargar Proyecto
        try {
            this.proyecto = miaDao.proyecto(folioProyecto, serialProyecto);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }

        if (proyecto == null) {
            this.proyecto = new Proyecto(folioProyecto, serialProyecto);
        }

        System.out.println("Folio " + folioProyecto);
        System.out.println("Serial " + serialProyecto);

        evaluacion = miaDao.getImpacAmbProyecto(folioProyecto, serialProyecto);
        
        if (proyecto.getNsub() != null) {
            SubsectorProyecto su = miaDao.getSubSector(proyecto.getNsub());
            SectorProyecto sp = miaDao.getSectorProyecto(su.getNsec());
            sector = sp.getSector();
        }
        
        //Comentarios proyecto - eescalona
        super.setProyectoInfo((short)5, folioProyecto, serialProyecto);

    }
    
    public void agregarEval() {
        Short id = 0;
        try {
            id = miaDao.getMaxImpacto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            id++;
        } catch (Exception w2) {

        }
        if (id == null) {
            id = 0;
        }

        ImpacAmbProyecto i = new ImpacAmbProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        i.setEtapaId(new CatEtapa((short)1));
        i.setImpactoId(new CatTipoImpacto((short)1));
        try {
            miaDao.merge(i);
        } catch (Exception e2) {
            e2.printStackTrace();
        }

        evaluacion.add(i);
    }
    
    /**
     * Metodo que crea un nuevo objeto de ImpacAmbProyecto
     */
    public void crearImpacto(){
    	evaNuevo = new ImpacAmbProyecto();
    }
    /**
     * Metodo que Guarda un registro de Impacto
     */
    public void agregarImpacto() {
    	medidaPreventiva = new MedPrevImpactProy();
        Short id = 0;

        try {
            id = miaDao.getMaxImpacto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            if(id == null){
            	id = 1;
            }else{
            	id++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ImpacAmbProyecto registroEvaluacion = new ImpacAmbProyecto();
        registroEvaluacion = new ImpacAmbProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),id);
        registroEvaluacion.setEtapaId(evaNuevo.getEtapaId());
        registroEvaluacion.setImpacCarac(evaNuevo.getImpacCarac());
        registroEvaluacion.setImpacEva(evaNuevo.getImpacEva());
        registroEvaluacion.setImpacIndic(evaNuevo.getImpacIndic());
        registroEvaluacion.setImpactDesc(evaNuevo.getImpactDesc());
        registroEvaluacion.setImpactIndent(evaNuevo.getImpactIndent());
        registroEvaluacion.setImpactoId(evaNuevo.getImpactoId());
        
        medidaPreventiva = new MedPrevImpactProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),id);
        medidaPreventiva.setEtapaId(evaNuevo.getEtapaId());
        medidaPreventiva.setPrevImpactonom(evaNuevo.getImpactIndent());
        
        reservedEva = new ImpacAmbProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),id);
        reservedEva.setEtapaId(evaNuevo.getEtapaId());
        reservedEva.setImpactoId(evaNuevo.getImpactoId());
               
        try {
            miaDao.persist(registroEvaluacion);
            miaDao.persist(medidaPreventiva);
        	evaluacion = miaDao.getImpacAmbProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalAgregar.hide();modalMensaje.show()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Metodo que obtiene un objeto de tipo ImpacAmbProyecto a partir de un id
     */
    public void consultarImpacto(){
    	try {
	    	evaluacion.get(0);
			Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
			int id = 0;
			String idAccidente = params.get("id").toString();
			id = Integer.parseInt(idAccidente);
			System.out.println("Registro de Accidente: "+id);
			editEva = new ImpacAmbProyecto();
			editMedidaPreventiva = new MedPrevImpactProy();
			editEva = miaDao.getImpacAmbProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) id);
			if(reservedEva.getImpacAmbProyectoPK() != null){
				if(editEva.getImpacAmbProyectoPK().getImpacAmbproyId() == reservedEva.getImpacAmbProyectoPK().getImpacAmbproyId()){
					editEva.setEtapaId(reservedEva.getEtapaId());
					editEva.setImpactoId(reservedEva.getImpactoId());
				}
			}
	//		evaluacion = miaDao.getImpacAmbProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
	//		for (ImpacAmbProyecto impacAmbProyecto : evaluacion) {
	//			if(impacAmbProyecto.getImpacAmbProyectoPK().getImpacAmbproyId() == id){
	//				editEva = impacAmbProyecto;
	//			}
	//		}
			
				editMedidaPreventiva = miaDao.getMedPrevImpactProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) id);
			
			// Numero secuencial a mostrar en "Editar registro #"
			rowIndexResEvaIAEditar = Integer.parseInt(params.get("prowindexResEvaIA").toString());
    	} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    /**
     * Metodo que Edita un registro de Impacto
     */
    public void editarImpacto() {
    	ImpacAmbProyecto registroEvaluacion = new ImpacAmbProyecto();
        registroEvaluacion = editEva;
        editMedidaPreventiva.setEtapaId(editEva.getEtapaId());
        editMedidaPreventiva.setPrevImpactonom(editEva.getImpactIndent());
        try {
            miaDao.merge(editEva);
            miaDao.merge(editMedidaPreventiva);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditar.hide();modalMensajeEditar.show()");
        } catch (Exception e) {
            e.printStackTrace();
        }
        evaluacion = miaDao.getImpacAmbProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    }
    
    /**
     * Metodo que Eliminar un registro de Impacto
     */
    public void eliminarImpacto() {
    	int size = evaluacion.size();
    	int id = 0;
    	while (evaluacion.size()==size) {
        	for (ImpacAmbProyecto impacAmbProyecto : selectedEva) {
    			
    			id = impacAmbProyecto.getImpacAmbProyectoPK().getImpacAmbproyId();
    	        try {
    		          miaDao.eliminarImpacto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),(short) id);
    		          miaDao.eliminarMedPrevImpactProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),(short) id);
    		          evaluacion = miaDao.getImpacAmbProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    		      } catch (Exception e2) {
    		          e2.printStackTrace();
    		      }
    		}
        	evaluacion = miaDao.getImpacAmbProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    		RequestContext context = RequestContext.getCurrentInstance();
        	if(evaluacion.size()!=size){
        		context.execute("modalEliminar.hide();modalMensajeEliminar.show()");
        	}
		}
    }
    
    /**
     * 
     * @param vinculacion
     */
	public void mostrarDescripcionImpacto(String Descripcion) {
		this.setDescripcionEva(Descripcion);
	}

    public void eliminarEva(ActionEvent e) {
        Short idt = (Short) e.getComponent().getAttributes().get("idt");
        ImpacAmbProyecto cp = new ImpacAmbProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt);
        evaluacion.remove(cp);
        try {
            miaDao.eliminarImpacto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt);
        } catch (Exception ex) {
            System.out.println("Error al eliminar criterio");
            ex.printStackTrace();
        }
    }
    public void guardar() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        try {
            miaDao.guardar(proyecto);
            if (proyecto.getDescMetutil() != null) {
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if (proyecto.getEstudioRiesgoId() != null && proyecto.getEstudioRiesgoId()==1){
                    try {
                        miaDao.guardarAvance(proyecto, new Short("3"), "51");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("6"), "51");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }                    
                }
                //</editor-fold>
            }
            //Fragmento de codigo que anteriormente guardaba todos los cambios realizados a la tabla principal de
            //la pagina Resultados de Evalucion de Impactos Ambientales
//            for (ImpacAmbProyecto i:evaluacion){
//                miaDao.merge(i);
//            }
            
            if (evaluacion.size() >= 1) {
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if (proyecto.getEstudioRiesgoId() != null && proyecto.getEstudioRiesgoId()==1){
                    try {
                        miaDao.guardarAvance(proyecto, new Short("3"), "52");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("5"), "52");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            
                           try {
                Validacion v = new Validacion();
                String msg = v.validar(seccion, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                if (!msg.isEmpty()) {
                    return;
                }
            } catch (Exception err) {
                err.printStackTrace();
            }

            
            reqcontEnv.execute("parent.actMenu();");
            //reqcontEnv.execute("alert('Su información ha sido guardada con Exito.')");
            reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");

            if (Integer.parseInt(miaDao.avanceProyecto(proyecto)) == 99) {
                //reqcontEnv.execute("alert('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.')");
            	reqcontEnv.execute("mensajes('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.', 'success')");
            }
            
            //Guardado de componentes en información adicional - eescalona
            super.guardarComentarios();

        } catch (Exception e) {
            e.printStackTrace();
            //reqcontEnv.execute("alert('No se ha podido guardar la información, Intente mas tarde.')");
            reqcontEnv.execute("mensajes('No se ha podido guardar la información, Intente más tarde.', 'danger')");
        }
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the sector
     */
    public String getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(String sector) {
        this.sector = sector;
    }

    /**
     * @return the evaluacion
     */
    public List<ImpacAmbProyecto> getEvaluacion() {
        return evaluacion;
    }

    /**
     * @param evaluacion the evaluacion to set
     */
    public void setEvaluacion(List<ImpacAmbProyecto> evaluacion) {
        this.evaluacion = evaluacion;
    }

    public Integer getClaveTramite() {
        return claveTramite;
    }

    public void setClaveTramite(Integer claveTramite) {
        this.claveTramite = claveTramite;
    }

    public short getSubsec() {
        return subsec;
    }

    public void setSubsec(short subsec) {
        this.subsec = subsec;
    }

    /**
     * @return the seccion
     */
    public String getSeccion() {
        return seccion;
    }

    /**
     * @param seccion the seccion to set
     */
    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

	public List<ImpacAmbProyecto> getSelectedEva() {
		return selectedEva;
	}

	public void setSelectedEva(List<ImpacAmbProyecto> selectedEva) {
		this.selectedEva = selectedEva;
	}

	public ImpacAmbProyecto getEvaNuevo() {
		return evaNuevo;
	}

	public void setEvaNuevo(ImpacAmbProyecto evaNuevo) {
		this.evaNuevo = evaNuevo;
	}

	public ImpacAmbProyecto getEditEva() {
		return editEva;
	}

	public void setEditEva(ImpacAmbProyecto editEva) {
		this.editEva = editEva;
	}

	public MedPrevImpactProy getMedidaPreventiva() {
		return medidaPreventiva;
	}

	public void setMedidaPreventiva(MedPrevImpactProy medidaPreventiva) {
		this.medidaPreventiva = medidaPreventiva;
	}

	public ImpacAmbProyecto getReservedEva() {
		return reservedEva;
	}

	public void setReservedEva(ImpacAmbProyecto reservedEva) {
		this.reservedEva = reservedEva;
	}

	public String getDescripcionEva() {
		return descripcionEva;
	}

	public void setDescripcionEva(String descripcionEva) {
		this.descripcionEva = descripcionEva;
	}
	
	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla Resultados de evaluacion de los impactos ambientales
	 */
	public long getRowIndexResEvaIAEditar() {
		return rowIndexResEvaIAEditar;
	}

	/**
	 * Asigna valor a variable que guarda Numero secuencial de registro a editar en Resultados de evaluacion de los impactos ambientales
	 * @param rowIndexResEvaIAEditar
	 */
	public void setRowIndexResEvaIAEditar(long rowIndexResEvaIAEditar) {
		this.rowIndexResEvaIAEditar = rowIndexResEvaIAEditar;
	}

}
