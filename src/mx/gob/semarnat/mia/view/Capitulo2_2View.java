/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIData;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import mx.gob.semarnat.mia.dao.CaracPartProyDAO;
import mx.gob.semarnat.mia.dao.CatObraDAO;
import mx.gob.semarnat.mia.dao.CatUnidadMedidaDAO;
import mx.gob.semarnat.mia.dao.SigeiaDao;
import mx.gob.semarnat.mia.model.CaracPartProy;
import mx.gob.semarnat.mia.model.CaracPartProyPK;
import mx.gob.semarnat.mia.model.CatLey;
import mx.gob.semarnat.mia.model.CatNaturaleza;
import mx.gob.semarnat.mia.model.CatObra;
import mx.gob.semarnat.mia.model.CatTemporalidad;
import mx.gob.semarnat.mia.model.LeyFedEstProyecto;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.SueloVegetacionProyecto;
import mx.gob.semarnat.mia.model.SustanciaProyecto;
import mx.gob.semarnat.mia.model.UsoSueloVeget;
import mx.gob.semarnat.mia.model.catalogos.CatUnidadMedida;
import mx.gob.semarnat.mia.util.Log;
import mx.gob.semarnat.mia.validacion.Validacion;
import mx.gob.semarnat.mia.view.to.CaracPartProyTO;
import mx.gob.semarnat.mia.view.to.CatalogoTO;

/**
 *
 * @author mauricio
 */
@ManagedBean
@ViewScoped
public class Capitulo2_2View extends CapitulosComentarios implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 4392374226080095424L;
	private String seccion;
    private Short NSubSector = (short) 0;
    
     
    
    
    
    private CaracPartProyTO caractTO ;// Capitulo2_2View.caractTO 
    private List<CaracPartProyTO> listaCaractTO ;
    private List<CaracPartProyTO> listaCaractTOSelected;
    
    

    private List<CatNaturaleza> catNat ;
    private List<CatTemporalidad> catTemp ;
    private List<CatUnidadMedida> catMedida ;

    
    
    
    private List<CatalogoTO> catNatSel = new ArrayList<CatalogoTO>();
    private List<CatalogoTO> catTempSel = new ArrayList<CatalogoTO>();
    private List<CatalogoTO> catMedidaSel = new ArrayList<CatalogoTO>();

    private List<UsoSueloVeget> dimensionesUso = new ArrayList<UsoSueloVeget>();
    //private List<SueloVegetacionProyecto> sueloVegetacion = new ArrayList<SueloVegetacionProyecto>();

    private Proyecto proyecto = new Proyecto();
    
    private SustanciaHelper sustanciaSeleccionada;
    private Integer claveTramite = 0;
    private short subsec = 0;
   
    /**
     * Objeto para agregar o editar las Caracteristicas particulares del proyecto.
     */
    private CaracPartProy caracPartProy;
    
    /**
     * Bandera que permite indicar si el dialog muestra los datos para editar o agregar una obra.
     */
    private boolean mostrarEdicion;
    /**
     * Permite manejar los servicios dao del catalogo de unidad de medida.
     */
    private CatUnidadMedidaDAO catUnidadMedidaDAO;
    /**
     * Permite manejar los servicios dao del catalogo de obras de SIGEIA.
     */
    private CatObraDAO catObraDAO;
    /**
     * Objeto utilizado para las unidades de medida de las obras.
     */
    private CatUnidadMedida catUnidadMedida;
    /**
     * Objeto utilizado para las obras de SIGEIA.
     */
    private CatObra catObra;
    /**
     * Lista de obras seleccionadas de la tabla del Capitulo 2
     */
    private List<CaracPartProyTO> listaCaractTOSeleccionado = new ArrayList<CaracPartProyTO>();
    /**
     * Lista que permite mostrar el catalogo de obras de SIGEIA.
     */
    private List<CatObra> listaCatObra;
    /**
     * Lista de caracteristicas a eliminar.
     */
	private List<CaracPartProyTO> caractSeleccionados;    
    /**
     * Objeto para las transacciones de la tabla caracteristica DAO.
     */
	private CaracPartProyDAO caracPartProyDAO = new CaracPartProyDAO();
	/**
	 * Se genera el objeto para mostrar en la edicion la unidad de medida.
	 */
	private CaracPartProyTO caracPartProyTO;
	
	private SigeiaDao sigeiaDao = new SigeiaDao();
	
    private final static Logger logger = Logger.getLogger(Capitulo2_2View.class);
    
    /**
     * Numero secuencial del registro "Obra" a editar en tabla "Caracteristicas particulares del proyecto"
     */
    private int rowIndexCPPEditar;
    

	public Capitulo2_2View() {
    	logger.debug("******************************* init Capitulo2_2View");
    	
    	listaCaractTO = new ArrayList<CaracPartProyTO>();
    	listaCaractTOSeleccionado = new ArrayList<>();
    	listaCatObra = new ArrayList<CatObra>();
    	caracPartProy = new CaracPartProy();
		catUnidadMedida = new CatUnidadMedida();
		caracPartProy.setNaturalezaId(new CatNaturaleza());
		caracPartProy.setTemporalidadId(new CatTemporalidad());
		catObra = new CatObra();
		caracPartProyTO = new CaracPartProyTO();
    	
		
        // Recupera proyecto de sesion
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
        claveTramite = (Integer) context.getExternalContext().getSessionMap().get("claveTramite");
        //subsec = (Short) context.getExternalContext().getSessionMap().get("subsector");
        subsec = new Short("8");

        catNat = miaDao.getCatNaturaleza();
        catTemp = miaDao.getCatTemporalidad();
        catMedida = miaDao.getCatUnidadMedida();

        //Cargar Proyecto
        try {
            //this.proyecto = miaDao.proyecto(folioProyecto, serialProyecto);
        	this.proyecto = miaDao.proyecto("8280",(short) 1);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }

        if (proyecto == null) {
            this.proyecto = new Proyecto(folioProyecto, serialProyecto);
            this.proyecto.setClaveProyecto("0");
        }

        for (CatNaturaleza c : catNat) {
        	CatalogoTO tmp = new CatalogoTO(c.getNaturalezaId(), c.getNaturalezaDescripcion());
            catNatSel.add(tmp);
        }

        for (CatTemporalidad t : catTemp) {
        	CatalogoTO tmp = new CatalogoTO(t.getTemporalidadId(), t.getTemporalidadDescripcion());
            catTempSel.add(tmp);
        }

        

//        for (CatUnidadMedida m : catMedida) {
//            SelectItem tmp = new SelectItem(m, m.getCtunDesc());
//        }
                
        //Se consultan las caracteristicas del proyecto.
        consultarCaracteristicasProyecto();
        
        System.out.println("sust " + sustanciaSeleccionada);

        //Comentarios proyecto - eescalona
        //super.setProyectoInfo((short) 2, folioProyecto, serialProyecto);

    }

    /**
     * Permite consultar las caracteristicas del proyecto.
     */
    public void consultarCaracteristicasProyecto() {
        try {
            List<CaracPartProy> caracteristicasPartProy = new ArrayList<CaracPartProy>();
            
            listaCaractTO = new ArrayList<CaracPartProyTO>();
			//consulta en la tabla del promovente, si existe un registro de SIGEIA.
            caracteristicasPartProy = caracPartProyDAO.getCaracPartSIGEIAPROM(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            //Si no contiene registros de SIGEIA.
            if (caracteristicasPartProy.isEmpty()){

				short contadorConsecutivoSIGEIA = 1;
				short contadorConsecutivoPROMOVENTE = 1;

    			//Se consulta la informacion de SIGEIA.
    			List<Object[]> listaSueloVegSIGEIA = sigeiaDao.consultarCaracPartProySIGEIA(proyecto.getProyectoPK().getFolioProyecto());
    			//Se itera la lista de registros de Suelo Vegetacion de SIGEIA.
    			for (Object[] objects : listaSueloVegSIGEIA) {
    				CaracPartProy caracPartProy = new CaracPartProy();
        			CaracPartProyTO caracPartProyTO= new CaracPartProyTO();
        			caracPartProyTO.setNombreObra(objects[0].toString());
        			
        			caracPartProyTO.setSuperficie(objects[2].toString());

        			caracPartProyTO.setNaturaleza(new CatalogoTO());
        			caracPartProyTO.setTemporalidad(new CatalogoTO());
        			
        			
        			CatUnidadMedida catUnidadMedida = new CatUnidadMedida();            			
        			catUnidadMedida.setCtunDesc("m2");
        			
        			CatalogoTO catalogoTO = new CatalogoTO();
        			catalogoTO.setDescripcion(catUnidadMedida.getCtunDesc());
        			caracPartProyTO.setUnidad(catalogoTO);
        			
        			caracPartProyTO.setEstatusSigProm("0");        			
        			
    				Short maxCaracteristica = caracPartProyDAO.getMaxCaracteristica(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    				
    				if (maxCaracteristica == null) {
    					maxCaracteristica = 0;
    				} else {
    					maxCaracteristica++;
    				}			
    				
    				CaracPartProyPK caracPartProyPK = new CaracPartProyPK();
    				caracPartProyPK.setCaracteristicaId(maxCaracteristica);
      				
    				caracPartProyTO.setCaracteristicaID(contadorConsecutivoSIGEIA);
    				
    				contadorConsecutivoSIGEIA++;
    				
    				caracPartProyPK.setFolioProyecto(proyecto.getProyectoPK().getFolioProyecto());
    				caracPartProyPK.setSerialProyecto(proyecto.getProyectoPK().getSerialProyecto());
    				
    				caracPartProy.setCaracPartProyPK(caracPartProyPK);
    				
    				Short maxValueSequence = caracPartProyDAO.consultarMAXCaracSequence(); 
    				//Se obtiene el maximo valor de la secuencia.
    				if (maxValueSequence == null) {
    					maxValueSequence = 1;
            			caracPartProyTO.setId(maxValueSequence);
    					caracPartProy.setIdCaracSeq(maxValueSequence);
    				} else {
            			caracPartProyTO.setId((short) (maxValueSequence + 1));
    					caracPartProy.setIdCaracSeq((short) (maxValueSequence + 1));
    				}
    				caracPartProy.setNombreObra(caracPartProyTO.getNombreObra());
    				caracPartProy.setEstatusSigProm(caracPartProyTO.getEstatusSigProm());
    				
    				String superficie = caracPartProyTO.getSuperficie().replaceAll(",", "");
    				
    				caracPartProy.setSuperficie(Double.parseDouble(superficie));
    				caracPartProy.setNaturalezaId(null);
    				caracPartProy.setTemporalidadId(null);
    				caracPartProyDAO.guardarCaracteristica(caracPartProy);

        			listaCaractTO.add(caracPartProyTO);            		

    			}    			

            	//Se consulta la informacion del promovente.
                caracteristicasPartProy = caracPartProyDAO.getCaracPart(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

            	//Se itera la lista de caracteristicas.
            	for(CaracPartProy caracPartProy:caracteristicasPartProy){
            			
            			CaracPartProyTO caracPartProyTO= new CaracPartProyTO();
            			caracPartProyTO.setCaracteristicaID(contadorConsecutivoPROMOVENTE);
            			
            			contadorConsecutivoPROMOVENTE++;
            			
            			caracPartProyTO.setObraActividad(caracPartProy.getObraActividad());
            			caracPartProyTO.setNombreObra(caracPartProy.getNombreObra());
            			caracPartProyTO.setDescripcion(caracPartProy.getDescripcion());
            			caracPartProyTO.setDescTrunked(caracPartProyTO.getDescripcion()!=null && caracPartProyTO.getDescripcion().length()>200 ? caracPartProyTO.getDescripcion().substring(0,200):caracPartProyTO.getDescripcion());
            			caracPartProyTO.setId(caracPartProy.getIdCaracSeq());
            		
            			String superficie = caracPartProy.getSuperficie().toString();
            			
            			caracPartProyTO.setSuperficie(superficie);
            			
            			if (caracPartProy.getNaturalezaId() == null && caracPartProy.getTemporalidadId() == null) {
            				caracPartProyTO.setNaturaleza(new CatalogoTO());
            				caracPartProyTO.setTemporalidad(new CatalogoTO());
            			} else {
                			caracPartProyTO.setNaturaleza(new CatalogoTO(caracPartProy.getNaturalezaId().getNaturalezaId(),caracPartProy.getNaturalezaId().getNaturalezaDescripcion() != null 
                					? caracPartProy.getNaturalezaId().getNaturalezaDescripcion()
                							: caracPartProyDAO.consultarNaturaleza(caracPartProy.getNaturalezaId().getNaturalezaId()).getNaturalezaDescripcion()));
                			
                			caracPartProyTO.setTemporalidad(new CatalogoTO(caracPartProy.getTemporalidadId().getTemporalidadId(),caracPartProy.getTemporalidadId().getTemporalidadDescripcion() != null 
                					? caracPartProy.getTemporalidadId().getTemporalidadDescripcion() 
                							:caracPartProyDAO.consultarTemporalidad(caracPartProy.getTemporalidadId().getTemporalidadId()).getTemporalidadDescripcion()));
						}           			
            			
            			CatUnidadMedida catUnidadMedida = caracPartProyDAO.getCatUnidadMedida(caracPartProy.getCtunClve());            			
            			caracPartProyTO.setUnidad(catUnidadMedida!=null ?new CatalogoTO(catUnidadMedida.getCtunClve(),catUnidadMedida.getCtunAbre()):null);
            			caracPartProyTO.setEstatusSigProm(caracPartProy.getEstatusSigProm());        			

            			listaCaractTO.add(caracPartProyTO);            			
            			
            	}
            } else {
            	
				short contadorConsecutivoSIGEIA = 1;
				short contadorConsecutivoPROMOVENTE = 1;
				
            	//Se consulta la informacion del promovente.
            	caracteristicasPartProy = caracPartProyDAO.getCaracPartOrderBySIGEIA(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            	//Se itera la lista de caracteristicas del proyecto de SIGEIA.
            	for(CaracPartProy caracPartProy:caracteristicasPartProy){
            			
            			CaracPartProyTO caracPartProyTO= new CaracPartProyTO();
            			
            			if (caracPartProy.getEstatusSigProm().equals("0")) {
                			caracPartProyTO.setCaracteristicaID(contadorConsecutivoSIGEIA);
                			contadorConsecutivoSIGEIA++;
						} else {
							caracPartProyTO.setCaracteristicaID(contadorConsecutivoPROMOVENTE);
							contadorConsecutivoPROMOVENTE++;
						}
            			
            			caracPartProyTO.setObraActividad(caracPartProy.getObraActividad());
            			caracPartProyTO.setNombreObra(caracPartProy.getNombreObra());
            			caracPartProyTO.setDescripcion(caracPartProy.getDescripcion());
            			caracPartProyTO.setDescTrunked(caracPartProyTO.getDescripcion()!=null && caracPartProyTO.getDescripcion().length()>200 ? caracPartProyTO.getDescripcion().substring(0,200):caracPartProyTO.getDescripcion());
            			caracPartProyTO.setId(caracPartProy.getIdCaracSeq());
            			caracPartProyTO.setSuperficie(caracPartProy.getSuperficie().toString());
            			
            			if (caracPartProy.getNaturalezaId() != null && caracPartProy.getTemporalidadId() != null) {
                			caracPartProyTO.setNaturaleza(new CatalogoTO(caracPartProy.getNaturalezaId().getNaturalezaId(),caracPartProy.getNaturalezaId().getNaturalezaDescripcion() != null 
                					? caracPartProy.getNaturalezaId().getNaturalezaDescripcion()
                							: caracPartProyDAO.consultarNaturaleza(caracPartProy.getNaturalezaId().getNaturalezaId()).getNaturalezaDescripcion()));
                			
                			caracPartProyTO.setTemporalidad(new CatalogoTO(caracPartProy.getTemporalidadId().getTemporalidadId(),caracPartProy.getTemporalidadId().getTemporalidadDescripcion() != null 
                					? caracPartProy.getTemporalidadId().getTemporalidadDescripcion() 
                							:caracPartProyDAO.consultarTemporalidad(caracPartProy.getTemporalidadId().getTemporalidadId()).getTemporalidadDescripcion()));							
						} else {
                			caracPartProyTO.setNaturaleza(new CatalogoTO());
                			caracPartProyTO.setTemporalidad(new CatalogoTO());
						}            			
            			
            			if (caracPartProy.getCtunClve() != null) {
                			CatUnidadMedida catUnidadMedida = caracPartProyDAO.getCatUnidadMedida(caracPartProy.getCtunClve());
                			caracPartProyTO.setUnidad(catUnidadMedida!=null ?new CatalogoTO(catUnidadMedida.getCtunClve(),catUnidadMedida.getCtunAbre()):null);							
						} else {
		        			CatUnidadMedida catUnidadMedida = new CatUnidadMedida();            			
		        			catUnidadMedida.setCtunDesc("ha");
		        			
		        			CatalogoTO catalogoTO = new CatalogoTO();
		        			catalogoTO.setDescripcion(catUnidadMedida.getCtunDesc());
		        			caracPartProyTO.setUnidad(catalogoTO);		        			
						}

            			caracPartProyTO.setEstatusSigProm(caracPartProy.getEstatusSigProm());        			

            			listaCaractTO.add(caracPartProyTO);            			
            			
            		}
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
    
    
    /****************************/
    boolean selectAllCaracteristicas;
    
    private UIData caracteristicasDataTable;	
    
    
    
    
    
	/**
	 * @return the caracteristicasDataTable
	 */
	public UIData getCaracteristicasDataTable() {
		return caracteristicasDataTable;
	}


	/**
	 * @param caracteristicasDataTable the caracteristicasDataTable to set
	 */
	public void setCaracteristicasDataTable(UIData caracteristicasDataTable) {
		this.caracteristicasDataTable = caracteristicasDataTable;
	}


	/**
	 * @return the selectAllCaracteristicas
	 */
	public boolean isSelectAllCaracteristicas() {
		return selectAllCaracteristicas;
	}


	/**
	 * @param selectAllCaracteristicas the selectAllCaracteristicas to set
	 */
	public void setSelectAllCaracteristicas(boolean selectAllCaracteristicas) {
		this.selectAllCaracteristicas = selectAllCaracteristicas;
	}



	private Map<CaracPartProyTO,Boolean> caractTOSelected = new HashMap<CaracPartProyTO, Boolean>(){

		private static final long serialVersionUID = -3360838896781243282L;

		@Override
		public Boolean get(Object object) {
			if (isSelectAllCaracteristicas()){
				caractTOSelected.put( (CaracPartProyTO) object, Boolean.TRUE);
				return Boolean.TRUE;
			}
			if (!containsKey(object)){
				return Boolean.FALSE;	
			}
			return super.get(object);
		};

	}; 
	
	
	
    /**
	 * @return the caractTOSelected
	 */
	public Map<CaracPartProyTO, Boolean> getCaractTOSelected() {
		return caractTOSelected;
	}


	public void selectAllCaracteristicasListener(ValueChangeEvent ae){
		System.out.println("******************************* init selectAllCaracteristicasListener");
		selectAllCaracteristicas = ((Boolean) ae.getNewValue()).booleanValue();
		if (!selectAllCaracteristicas){
			clearEntitiesSelected();
		}
		System.out.println("******************************* end selectAllCaracteristicasListener");
	}
    
    
    public void clearEntitiesSelected(){
		selectAllCaracteristicas = false;
		caractTOSelected.clear();
	}
    
    public List<CaracPartProyTO> getAllInvoicesSelected(){
		if (isSelectAllCaracteristicas()){
			return getListaCaractTO();
		}
		final List<CaracPartProyTO> result = new ArrayList<CaracPartProyTO>();
	    final Iterator<?> iterator = caractTOSelected.keySet().iterator();
	    while (iterator.hasNext()) {
	    	CaracPartProyTO key = (CaracPartProyTO) iterator.next();
	    	if (caractTOSelected.get(key)){	
	    		result.add(key);
	    	}
	    }
	    
		return result;
	}

	public void selectCaracteristicasListener(ValueChangeEvent event){
		System.out.println("******************************* init selectCaracteristicasListener");
 		selectAllCaracteristicas = false;
 		caractTOSelected.put((CaracPartProyTO)caracteristicasDataTable.getRowData(), (Boolean) event.getNewValue());
	}
    
    /***************************/
    
    
    
    public void eliminarCarac(ActionEvent evt) {
        Short id = (Short) evt.getComponent().getAttributes().get("idt");
        CaracPartProy c = new CaracPartProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        try {
            miaDao.eliminarCaracPartProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        } catch (Exception e) {

        }
        //caracteristicas.remove(c);
    }

    public void agrCarac() {
        Short id = 0;

        try {
            id = miaDao.getMaxCarac(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            id = (short) (id.intValue() + 1);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (id == null) {
            id = 0;
        }

        CaracPartProy cp = new CaracPartProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        cp.setNaturalezaId(new CatNaturaleza((short) 1));
        cp.setTemporalidadId(new CatTemporalidad((short) 1));
        try {
            miaDao.merge(cp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //caracteristicas.add(cp);

    }

//    public void guardar() {
//        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
//
//        try {
//            System.out.println("Validar " + seccion);
//            Validacion v = new Validacion();
//            String msg = v.validar(getSeccion(), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
//            if (!msg.isEmpty()) {
//                return;
//            }
//        } catch (Exception err) {
//            err.printStackTrace();
//        }
//
//        //<editor-fold desc="Guardado Apartado 27 Caracteristicas Particulares de Proyecto" defaultstate="collapsed">
//        try {
//            miaDao.guardar(proyecto);
//            
//            for (CaracPartProy c : caracteristicas) {
//                miaDao.merge(c);
//            }
//            if (caracteristicas.size() >= 1) {
//                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
//                try {
//                    miaDao.guardarAvance(proyecto, new Short("2"), "27");
//                    if (Integer.parseInt(miaDao.avanceProyecto(proyecto)) == 99) {
//                        reqcontEnv.execute("alert('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.')");
//                    }
//                } catch (Exception e) {
//                    System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
//                }
//                //</editor-fold>
//            }
//            reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
//        } catch (Exception e) {
//            Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e.getMessage());
//            e.printStackTrace();
//            reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.')");
//        }        //</editor-fold>
//
//        //Guardado de componentes en información adicional - eescalona
//        super.guardarComentarios();
//
//    }
    
 
    public void guardar() {
    	RequestContext reqcontEnv = RequestContext.getCurrentInstance();

    	try {
    		System.out.println("Validar " + seccion);
    		Validacion v = new Validacion();
    		String msg = v.validar(getSeccion(), proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    		if (!msg.isEmpty()) {
    			return;
    		}
    	} catch (Exception err) {
    		err.printStackTrace();
    	}
    	
		try {
			try {
				miaDao.guardarAvance(proyecto, new Short("2"), "27");
				if (Integer.parseInt(miaDao.avanceProyecto(proyecto)) == 99) {
					reqcontEnv.execute(
							"mensajes('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.', 'success')");
				}
			} catch (Exception e) {
				System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
			}
			reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
		} catch (Exception e) {
			Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),
					e.getMessage());
			e.printStackTrace();
			reqcontEnv.execute("mensajes('No se ha podido guardar la información, intente más tarde.', 'danger')");
		}

    }    

    
    public void guardarObras() {
		System.out.println("Entrando a obras");
	}
    
    /**
     * Permite eliminar la obra seleccionada.
     */
    public void eliminarObra() {
		//Si se seleccionaron obras.
    	if (listaCaractTOSeleccionado.size() > 0) {
    		for (CaracPartProyTO caracPartProyTO : listaCaractTOSeleccionado) {
    			try {
    				//Se eliminan las caracteristicas seleccionadas.
					int caracEliminadas = miaDao.eliminarCaracPartProySeleccionados(caracPartProyTO.getId());
					
					if (caracEliminadas > 0) {
						RequestContext context = RequestContext.getCurrentInstance();
						context.execute("dlgEliminarObra.hide();dlgEliminacionExitosa.show()");
						consultarCaracteristicasProyecto();
					}
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}
		}
	}
    
    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the NSubSector
     */
    public Short getNSubSector() {
        return NSubSector;
    }

    /**
     * @param NSubSector the NSubSector to set
     */
    public void setNSubSector(Short NSubSector) {
        this.NSubSector = NSubSector;
    }



    /**
     * @return the catNat
     */
    public List<CatNaturaleza> getCatNat() {
        return catNat;
    }

    /**
     * @param catNat the catNat to set
     */
    public void setCatNat(List<CatNaturaleza> catNat) {
        this.catNat = catNat;
    }

    /**
     * @return the catTemp
     */
    public List<CatTemporalidad> getCatTemp() {
        return catTemp;
    }

    /**
     * @param catTemp the catTemp to set
     */
    public void setCatTemp(List<CatTemporalidad> catTemp) {
        this.catTemp = catTemp;
    }

    /**
     * @return the catMedida
     */
    public List<CatUnidadMedida> getCatMedida() {
        return catMedida;
    }

    /**
     * @param catMedida the catMedida to set
     */
    public void setCatMedida(List<CatUnidadMedida> catMedida) {
        this.catMedida = catMedida;
    }

    /**
     * @return the dimensionesUso
     */
    public List<UsoSueloVeget> getDimensionesUso() {
        return dimensionesUso;
    }

    /**
     * @param dimensionesUso the dimensionesUso to set
     */
    public void setDimensionesUso(List<UsoSueloVeget> dimensionesUso) {
        this.dimensionesUso = dimensionesUso;
    }

    
    public void verifica() {

    }

    public SustanciaHelper getSustanciaSeleccionada() {
        return sustanciaSeleccionada;
    }

    public void setSustanciaSeleccionada(SustanciaHelper sustanciaSeleccionada) {
        this.sustanciaSeleccionada = sustanciaSeleccionada;
    }

//    /**
//     * @return the sueloVegetacion
//     */
//    public List<SueloVegetacionProyecto> getSueloVegetacion() {
//        return sueloVegetacion;
//    }
//
//    /**
//     * @param sueloVegetacion the sueloVegetacion to set
//     */
//    public void setSueloVegetacion(List<SueloVegetacionProyecto> sueloVegetacion) {
//        this.sueloVegetacion = sueloVegetacion;
//    }

    public Integer getClaveTramite() {
        return claveTramite;
    }

    public void setClaveTramite(Integer claveTramite) {
        this.claveTramite = claveTramite;
    }

    public short getSubsec() {
        return subsec;
    }

    public void setSubsec(short subsec) {
        this.subsec = subsec;
    }

    /**
     * @return the seccion
     */
    public String getSeccion() {
        return seccion;
    }

    /**
     * @param seccion the seccion to set
     */
    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public class SustanciaHelper {

        private SustanciaProyecto sustanciah;

        public SustanciaHelper(SustanciaProyecto sustanciah) {
            this.sustanciah = sustanciah;
        }

        public SustanciaProyecto getSustanciah() {
            return sustanciah;
        }

        public void setSustanciah(SustanciaProyecto sustanciah) {
            this.sustanciah = sustanciah;
        }

    }
    
    /**
     * Permite obtener el id de la obra seleccionada a editar.
     * @param idObraSeleccionada a editar.
     */
    public void abrirEditarObra(short idCaracSeq, int rowIndexCPP) {
		mostrarEdicion = true;
		
		try {
			// Número secuencial a mostrar en "Editar registro #"
			rowIndexCPPEditar = rowIndexCPP;
			
			caracPartProy = new CaracPartProy();
			caracPartProy = miaDao.getCaracPartPorObra(idCaracSeq);
			
			if (caracPartProy.getNaturalezaId() == null && caracPartProy.getTemporalidadId() == null) {
				caracPartProy.setNaturalezaId(new CatNaturaleza());
				caracPartProy.setTemporalidadId(new CatTemporalidad());
			}
			
			//se cargan los datos del los cambos de la naturaleza y la temporalidad.
			catNat = caracPartProyDAO.getCatNaturaleza();
			catTemp = caracPartProyDAO.getCatTemporalidad();
			catObraDAO = new CatObraDAO();
			listaCatObra = catObraDAO.consultarObraActividadSIGEIA();
			
			if (caracPartProy.getCtunClve() == null) {
				CatalogoTO catalogoTO = new CatalogoTO();
				catalogoTO.setDescripcion("metros cuadrados");
				this.caracPartProyTO.setUnidad(catalogoTO);				
			} else {
				CatUnidadMedida catUnidadMedida = miaDao.getCatUnidadMedida(caracPartProy.getCtunClve());            			
				this.caracPartProyTO.setUnidad(catUnidadMedida!=null ?new CatalogoTO(catUnidadMedida.getCtunClve(),catUnidadMedida.getCtunDesc()):null);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
    
    /**
     * Permite abrir la ventana de dialog para guardar una obra.
     */
    public void abrirGuardarObra() {
		mostrarEdicion = false;
		
		limpiarLeyFederal();
		
		catNat = new ArrayList<CatNaturaleza>();
		catTemp = new ArrayList<CatTemporalidad>();
		catMedida = new ArrayList<CatUnidadMedida>();
		listaCatObra = new ArrayList<CatObra>();
		
		catUnidadMedida = new CatUnidadMedida();
		
		catUnidadMedidaDAO = new CatUnidadMedidaDAO();
		catObraDAO = new CatObraDAO();
		
		try {
			catMedida = catUnidadMedidaDAO.consultarUnidadesMedida();
			listaCatObra = catObraDAO.consultarObraActividadSIGEIA();
			catNat = caracPartProyDAO.getCatNaturaleza();
			catTemp = caracPartProyDAO.getCatTemporalidad();			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
    
    /**
     * Permite cargar los datos de las caracteristicas particulares a eliminar.
     */
    public void abrirEliminarObra() {
    	caractSeleccionados = listaCaractTOSeleccionado;
    	
		//Si se seleccionaron leyes federales.
    	if (caractSeleccionados != null && caractSeleccionados.size() > 0) {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("dlgEliminarObra.show()");
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("dlgSeleccionCarac.show()");
		}
    }
    
    /**
     * Permite guardar o actualizar la caracteristica particular del proyecto
     */
    public void guardarObra() {

		CaracPartProyPK caracPartProyPK = new CaracPartProyPK();
		caracPartProyPK.setFolioProyecto(proyecto.getProyectoPK().getFolioProyecto());
		caracPartProyPK.setSerialProyecto(proyecto.getProyectoPK().getSerialProyecto());
				
    	//Si ya existe el registro
    	if (caracPartProy.getIdCaracSeq() != null) {
    		
    		try {
    			
				caracPartProyDAO.actualizarCaracteristica(caracPartProy);
			
	    		RequestContext context = RequestContext.getCurrentInstance();
	    		context.execute("dlgEditarObra.hide();dlgEdicionCaracExitosa.show()");
				
	    		caracPartProy = new CaracPartProy();
				caracPartProy.setNaturalezaId(new CatNaturaleza());
				caracPartProy.setTemporalidadId(new CatTemporalidad());
				
				consultarCaracteristicasProyecto();
    		} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		

		} else {
			try {
				Short maxCaracteristica = caracPartProyDAO.getMaxCaracteristica(caracPartProyPK.getFolioProyecto(), caracPartProyPK.getSerialProyecto());
				
				if (maxCaracteristica == null) {
					maxCaracteristica = 0;
				} else {
					maxCaracteristica++;
				}			
				
				caracPartProyPK.setCaracteristicaId(maxCaracteristica);
				caracPartProy.setCaracPartProyPK(caracPartProyPK);
				
				Short maxValueSequence = caracPartProyDAO.consultarMAXCaracSequence(); 
				//Se obtiene el maximo valor de la secuencia.
				if (maxValueSequence == null) {
					maxValueSequence = 1;
					caracPartProy.setIdCaracSeq(maxValueSequence);
				} else {
					caracPartProy.setIdCaracSeq((short) (maxValueSequence + 1));
				}
				
				caracPartProy.setEstatusSigProm("1");
				caracPartProyDAO.guardarCaracteristica(caracPartProy);

	    		RequestContext context = RequestContext.getCurrentInstance();
	    		context.execute("dlgEditarObra.hide();dlgEdicionExitosa.show()");	

	    		caracPartProy = new CaracPartProy();
				caracPartProy.setNaturalezaId(new CatNaturaleza());
				caracPartProy.setTemporalidadId(new CatTemporalidad());
	    		
				consultarCaracteristicasProyecto();


			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    		
	}

    /**
     * Permite limpiar los campos de las ventanas modales.
     */
    public void limpiarLeyFederal() {
		this.caracPartProy = new CaracPartProy();
		CatNaturaleza naturaleza = new CatNaturaleza();
		CatTemporalidad temporalidad = new CatTemporalidad();
		this.caracPartProy.setNaturalezaId(naturaleza);
		this.caracPartProy.setTemporalidadId(temporalidad);
		
	}
    
    
//////////////////
    
    
    
    
	/**
	 * @return the caractTO
	 */
	public CaracPartProyTO getCaractTO() {
		return caractTO;
	}




	/**
	 * @param caractTO the caractTO to set
	 */
	public void setCaractTO(CaracPartProyTO caractTO) {
		this.caractTO = caractTO;
	}




	/**
	 * @return the listaCaractTO
	 */
	public List<CaracPartProyTO> getListaCaractTO() {
		return listaCaractTO;
	}




	/**
	 * @param listaCaractTO the listaCaractTO to set
	 */
	public void setListaCaractTO(List<CaracPartProyTO> listaCaractTO) {
		this.listaCaractTO = listaCaractTO;
	}




	/**
	 * @return the listaCaractTOSelected
	 */
	public List<CaracPartProyTO> getListaCaractTOSelected() {
		return listaCaractTOSelected;
	}




	/**
	 * @param listaCaractTOSelected the listaCaractTOSelected to set
	 */
	public void setListaCaractTOSelected(List<CaracPartProyTO> listaCaractTOSelected) {
		this.listaCaractTOSelected = listaCaractTOSelected;
	}




	/**
	 * @return the catNatSel
	 */
	public List<CatalogoTO> getCatNatSel() {
		return catNatSel;
	}




	/**
	 * @param catNatSel the catNatSel to set
	 */
	public void setCatNatSel(List<CatalogoTO> catNatSel) {
		this.catNatSel = catNatSel;
	}




	/**
	 * @return the catTempSel
	 */
	public List<CatalogoTO> getCatTempSel() {
		return catTempSel;
	}




	/**
	 * @param catTempSel the catTempSel to set
	 */
	public void setCatTempSel(List<CatalogoTO> catTempSel) {
		this.catTempSel = catTempSel;
	}




	/**
	 * @return the catMedidaSel
	 */
	public List<CatalogoTO> getCatMedidaSel() {
		return catMedidaSel;
	}




	/**
	 * @param catMedidaSel the catMedidaSel to set
	 */
	public void setCatMedidaSel(List<CatalogoTO> catMedidaSel) {
		this.catMedidaSel = catMedidaSel;
	}


	/**
	 * @return the listaCaractTOSeleccionado
	 */
	public List<CaracPartProyTO> getListaCaractTOSeleccionado() {
		return listaCaractTOSeleccionado;
	}


	/**
	 * @param listaCaractTOSeleccionado the listaCaractTOSeleccionado to set
	 */
	public void setListaCaractTOSeleccionado(List<CaracPartProyTO> listaCaractTOSeleccionado) {
		this.listaCaractTOSeleccionado = listaCaractTOSeleccionado;
	}

	
	/**
	 * @return the caracPartProy
	 */
	public CaracPartProy getCaracPartProy() {
		return caracPartProy;
	}


	/**
	 * @param caracPartProy the caracPartProy to set
	 */
	public void setCaracPartProy(CaracPartProy caracPartProy) {
		this.caracPartProy = caracPartProy;
	}


	/**
	 * @return the mostrarEdicion
	 */
	public boolean isMostrarEdicion() {
		return mostrarEdicion;
	}


	/**
	 * @param mostrarEdicion the mostrarEdicion to set
	 */
	public void setMostrarEdicion(boolean mostrarEdicion) {
		this.mostrarEdicion = mostrarEdicion;
	}


	/**
	 * @return the catUnidadMedida
	 */
	public CatUnidadMedida getCatUnidadMedida() {
		return catUnidadMedida;
	}


	/**
	 * @param catUnidadMedida the catUnidadMedida to set
	 */
	public void setCatUnidadMedida(CatUnidadMedida catUnidadMedida) {
		this.catUnidadMedida = catUnidadMedida;
	}


	/**
	 * @return the listaCatObra
	 */
	public List<CatObra> getListaCatObra() {
		return listaCatObra;
	}


	/**
	 * @param listaCatObra the listaCatObra to set
	 */
	public void setListaCatObra(List<CatObra> listaCatObra) {
		this.listaCatObra = listaCatObra;
	}


	/**
	 * @return the catObra
	 */
	public CatObra getCatObra() {
		return catObra;
	}


	/**
	 * @param catObra the catObra to set
	 */
	public void setCatObra(CatObra catObra) {
		this.catObra = catObra;
	}


	/**
	 * @return the caractSeleccionados
	 */
	public List<CaracPartProyTO> getCaractSeleccionados() {
		return caractSeleccionados;
	}


	/**
	 * @param caractSeleccionados the caractSeleccionados to set
	 */
	public void setCaractSeleccionados(List<CaracPartProyTO> caractSeleccionados) {
		this.caractSeleccionados = caractSeleccionados;
	}

	/**
	 * @return the caracPartProyTO
	 */
	public CaracPartProyTO getCaracPartProyTO() {
		return caracPartProyTO;
	}

	/**
	 * @param caracPartProyTO the caracPartProyTO to set
	 */
	public void setCaracPartProyTO(CaracPartProyTO caracPartProyTO) {
		this.caracPartProyTO = caracPartProyTO;
	}

	/**
	 * @return the sigeiaDao
	 */
	public SigeiaDao getSigeiaDao() {
		return sigeiaDao;
	}

	/**
	 * @param sigeiaDao the sigeiaDao to set
	 */
	public void setSigeiaDao(SigeiaDao sigeiaDao) {
		this.sigeiaDao = sigeiaDao;
	}
	
	
	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla Caracteristicas particulares del proyecto
	 */
	public int getRowIndexCPPEditar() {
		return rowIndexCPPEditar;
	}

	/**
	 * Asigna valor a variable que guarda Numero secuencial de registro a editar en Caracteristicas particulares del proyecto
	 * @param rowIndexCPPEditar the rowIndexCPPEditar to set
	 */
	public void setRowIndexCPPEditar(int rowIndexCPPEditar) {
		this.rowIndexCPPEditar = rowIndexCPPEditar;
	}
	
}
