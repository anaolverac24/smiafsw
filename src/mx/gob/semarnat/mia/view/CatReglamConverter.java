package mx.gob.semarnat.mia.view;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import mx.gob.semarnat.mia.model.CatReglamento;
import mx.gob.semarnat.mia.view.capitulo1.SelectItemsBaseConverter;

/**
 * The Class PersonaConverter.
 */
@ManagedBean (name="reglamConverter")
public class CatReglamConverter extends SelectItemsBaseConverter {
    
    /* (non-Javadoc)
     * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
    	System.out.println("Entra a método " + value);
        if (!(value instanceof CatReglamento) || ((CatReglamento) value).getReglamentoId() == 0) {
            return null;
        }

        return String.valueOf(((CatReglamento) value).getReglamentoId());
    }
}
