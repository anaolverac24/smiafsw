package mx.gob.semarnat.mia.view;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import mx.gob.semarnat.mia.model.CatEstEsp;
import mx.gob.semarnat.mia.view.capitulo1.SelectItemsBaseConverter;

/**
 * The Class PersonaConverter.
 */
@ManagedBean (name="estudiosEspecialesConverter")
public class EstudiosEspecialesConverter extends SelectItemsBaseConverter {
    
    /* (non-Javadoc)
     * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof CatEstEsp) || ((CatEstEsp) value).getEstudioId() == 0) {
            return null;
        }

        return String.valueOf(((CatEstEsp) value).getEstudioId());
    }
}
