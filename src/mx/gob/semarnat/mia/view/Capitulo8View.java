/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.xml.bind.annotation.XmlElementDecl.GLOBAL;

import mx.gob.semarnat.mia.model.AnexosProyecto;
import mx.gob.semarnat.mia.model.GlosarioProyecto;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.catalogos.SectorProyecto;
import mx.gob.semarnat.mia.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.mia.util.Log;
import mx.gob.semarnat.mia.validacion.Validacion;

import org.primefaces.context.RequestContext;

import com.sun.org.glassfish.external.probe.provider.annotations.Probe;

/**
 *
 * @author Ramon Hidalgo
 */
@ManagedBean
@ViewScoped
public class Capitulo8View extends CapitulosComentarios implements Serializable {
    private String seccion;

    //private MiaDao miaDao = new MiaDao();
    private Proyecto proyecto = new Proyecto();
    private String sector = "";
    private Integer claveTramite = 0;
    private short subsec = 0;
    private List<AnexosProyecto> otros = new ArrayList<AnexosProyecto>();
    private List<AnexosProyecto> fotos = new ArrayList<AnexosProyecto>();
    private List<AnexosProyecto> video = new ArrayList<AnexosProyecto>();
    private List<GlosarioProyecto> glosario = new ArrayList<GlosarioProyecto>();
    private String glosarioclob;

    public void agregarGlosario() {
        Short id = 0;
        try {
            id = miaDao.getMaxGlosario(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            id++;
        } catch (Exception e2) {

        }

        if (id == null) {
            id = 0;
        }

        GlosarioProyecto gp = new GlosarioProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        try {
            miaDao.merge(gp);
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        glosario.add(gp);
    }

    public void eliminarGlosario(ActionEvent evt) {
        Short id = (Short) evt.getComponent().getAttributes().get("idt");
        GlosarioProyecto gp = new GlosarioProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);

        glosario.remove(gp);
        try {
            miaDao.eliminarGlosario(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Capitulo8View() {
        // Recupera proyecto de sesion
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
        claveTramite =  (Integer) context.getExternalContext().getSessionMap().get("claveTramite");
        subsec = (Short) context.getExternalContext().getSessionMap().get("subsector");
        //Cargar Proyecto
        try {
            this.proyecto = miaDao.proyecto(folioProyecto, serialProyecto);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }

        if (proyecto == null) {
            this.proyecto = new Proyecto(folioProyecto, serialProyecto);
        }

        System.out.println("Folio " + folioProyecto);
        System.out.println("Serial " + serialProyecto);

        if (proyecto.getNsub() != null) {
            SubsectorProyecto su = miaDao.getSubSector(proyecto.getNsub());
            SectorProyecto sp = miaDao.getSectorProyecto(su.getNsec());
            sector = sp.getSector();
        }

        glosario = miaDao.getGlosario(folioProyecto, serialProyecto);
        
        glosarioclob = proyecto.getGlosario();
        
        //Comentarios proyecto - eescalona
        super.setProyectoInfo((short)8, folioProyecto, serialProyecto);

    }

    public void guardar() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        try {
            miaDao.guardar(proyecto);
            reqcontEnv.execute("parent.actMenu();");
            //reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
            reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
            
            if (proyecto.getManifestacionRes() != null) {
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId() != null && proyecto.getEstudioRiesgoId() == 1){
                    try {
                        miaDao.guardarAvance(proyecto, new Short("3"), "81");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else{
                    try {
                        miaDao.guardarAvance(proyecto, new Short("8"), "81");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            System.out.println("Error encontrado!  "+glosario.size());
            if(glosario.size()>=1){
                for (GlosarioProyecto g:glosario){
                miaDao.merge(g);
            }
            }
            
            if (glosarioclob.length()>= 1){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId() != null && proyecto.getEstudioRiesgoId() == 1){
                    try {
                        miaDao.guardarAvance(proyecto, new Short("1"), "82");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else{
                    try {
                        miaDao.guardarAvance(proyecto, new Short("2"), "82");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            
            if (proyecto.getBiblioProy() != null){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId() != null && proyecto.getEstudioRiesgoId() == 1){
                    try {
                        miaDao.guardarAvance(proyecto, new Short("1"), "83");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else{
                    try {
                        miaDao.guardarAvance(proyecto, new Short("2"), "83");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            if (Integer.parseInt(miaDao.avanceProyecto(proyecto)) == 99) {
                //reqcontEnv.execute("alert('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.')");
            	reqcontEnv.execute("mensajes('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.', 'success')");
            }
      
            
                           try {
                Validacion v = new Validacion();
                String msg = v.validar(seccion, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                if (!msg.isEmpty()) {
                    return;
                }
            } catch (Exception err) {
                err.printStackTrace();
            }

            //Guardado de componentes en información adicional - eescalona
            super.guardarComentarios();

        } catch (Exception e) {
            Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e.getMessage());
            e.printStackTrace();
            //reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.')");
            reqcontEnv.execute("mensajes('No se ha podido guardar la información, Intente más tarde.', 'danger')");
        }
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the sector
     */
    public String getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(String sector) {
        this.sector = sector;
    }

    /**
     * @return the otros
     */
    public List<AnexosProyecto> getOtros() {
        return otros;
    }

    /**
     * @param otros the otros to set
     */
    public void setOtros(List<AnexosProyecto> otros) {
        this.otros = otros;
    }

    /**
     * @return the fotos
     */
    public List<AnexosProyecto> getFotos() {
        return fotos;
    }

    /**
     * @param fotos the fotos to set
     */
    public void setFotos(List<AnexosProyecto> fotos) {
        this.fotos = fotos;
    }

    /**
     * @return the video
     */
    public List<AnexosProyecto> getVideo() {
        return video;
    }

    /**
     * @param video the video to set
     */
    public void setVideo(List<AnexosProyecto> video) {
        this.video = video;
    }

    /**
     * @return the glosario
     */
    public List<GlosarioProyecto> getGlosario() {
        return glosario;
    }

    /**
     * @param glosario the glosario to set
     */
    public void setGlosario(List<GlosarioProyecto> glosario) {
        this.glosario = glosario;
    }

    public Integer getClaveTramite() {
        return claveTramite;
    }

    public void setClaveTramite(Integer claveTramite) {
        this.claveTramite = claveTramite;
    }

    public short getSubsec() {
        return subsec;
    }

    public void setSubsec(short subsec) {
        this.subsec = subsec;
    }

    /**
     * @return the seccion
     */
    public String getSeccion() {
        return seccion;
    }

    /**
     * @param seccion the seccion to set
     */
    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

	public String getGlosarioclob() {
		return glosarioclob;
	}

	public void setGlosarioclob(String glosarioclob) {
		this.glosarioclob = glosarioclob;
	}

	 
    
    
    

}
