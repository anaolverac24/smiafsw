package mx.gob.semarnat.mia.view.capitulo1;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

/**
 * The Class PersonaConverter.
 */
@ManagedBean (name="obraActConverter")
public class ObraActividadConverter extends SelectItemsBaseConverter {
    
    /* (non-Javadoc)
     * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof ObraActividad) || ((ObraActividad) value).getId() == null) {
            return null;
        }

        return String.valueOf(((ObraActividad) value).getId());
    }
}
