/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view.capitulo1;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author marcog
 */
public class Pregunta implements Serializable {
    private String pregunta;
    private List<Respuesta> listaRespuesta;
    private Respuesta seleccion;

    /**
     * @return the pregunta
     */
    public String getPregunta() {
        return pregunta;
    }

    /**
     * @param pregunta the pregunta to set
     */
    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    /**
     * @return the listaRespuesta
     */
    public List<Respuesta> getListaRespuesta() {
        return listaRespuesta;
    }

    /**
     * @param listaRespuesta the listaRespuesta to set
     */
    public void setListaRespuesta(List<Respuesta> listaRespuesta) {
        this.listaRespuesta = listaRespuesta;
    }

    /**
     * @return the seleccion
     */
    public Respuesta getSeleccion() {
        return seleccion;
    }

    /**
     * @param seleccion the seleccion to set
     */
    public void setSeleccion(Respuesta seleccion) {
        this.seleccion = seleccion;
    }
    
}
