/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view.capitulo1;

import java.io.Serializable;
import java.util.List;


public class ReiaProyObrasTO implements Serializable {
   
    /**
	 * 
	 */
	private static final long serialVersionUID = 8148158178993886814L;

	private String folio;
    
    private ObraActividad obra;    

    private Long clave; // esta es la clave buena REIA_PROY_OBRAS.clave unica para el registro
    
    private short clavetemp; // clave temp que se pone en el campo # de la tabla pestaña 5 ( actividades reguladas )
    
    private String exceptuada;
    
    private String principal;

    private List<Condiciones> listaCondiciones;
    
    /**
	 * @return the folio
	 */
	public String getFolio() {
		return folio;
	}

	/**
	 * @param folio the folio to set
	 */
	public void setFolio(String folio) {
		this.folio = folio;
	}

	/**
	 * @return the obra
	 */
	public ObraActividad getObra() {
		return obra;
	}

	/**
	 * @param obra the obra to set
	 */
	public void setObra(ObraActividad obra) {
		this.obra = obra;
	}

	/**
	 * @return the clave
	 */
	public Long getClave() {
		return clave;
	}

	/**
	 * @param clave the clave to set
	 */
	public void setClave(Long clave) {
		this.clave = clave;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (clave != null ? clave.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReiaProyObrasTO)) {
            return false;
        }
        ReiaProyObrasTO other = (ReiaProyObrasTO) object;
        if ((this.clave == null && other.clave != null) || (this.clave != null && !this.clave.equals(other.clave))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.view.capitulo1[ clave=" + clave + " ]";
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

	/**
	 * @return the listaCondiciones
	 */
	public List<Condiciones> getListaCondiciones() {
		return listaCondiciones;
	}

	/**
	 * @param listaCondiciones the listaCondiciones to set
	 */
	public void setListaCondiciones(List<Condiciones> listaCondiciones) {
		this.listaCondiciones = listaCondiciones;
	}

	public short getClavetemp() {
		return clavetemp;
	}

	public void setClavetemp(short clavetemp) {
		this.clavetemp = clavetemp;
	}

	/**
	 * @return the exceptuada
	 */
	public String getExceptuada() {
		return exceptuada;
	}

	/**
	 * @param exceptuada the exceptuada to set
	 */
	public void setExceptuada(String exceptuada) {
		this.exceptuada = exceptuada;
	}

    
    
}
