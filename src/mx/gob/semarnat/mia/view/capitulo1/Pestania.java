/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view.capitulo1;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Paul Montoya
 */
public class Pestania implements Serializable {
    
    private int id;
    private String titulo;
    private boolean desabilitado = true;

    private List<Pregunta> listaPregunta;
    
    public Pestania() {}
    
    public Pestania(int id, String titulo) {
        this.id = id;
        this.titulo = titulo;
    }
    
    public Pestania(int id, String titulo, List<Pregunta> listaPregunta) {
        this.id = id;
        this.titulo = titulo;
        this.listaPregunta=listaPregunta;
    }

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public boolean isDesabilitado() {
        return desabilitado;
    }

    public void setDesabilitado(boolean desabilitado) {
        this.desabilitado = desabilitado;
    }
    
    
    /**
     * @return the listaPregunta
     */
    public List<Pregunta> getListaPregunta() {
        return listaPregunta;
    }

    /**
     * @param listaPregunta the listaPregunta to set
     */
    public void setListaPregunta(List<Pregunta> listaPregunta) {
        this.listaPregunta = listaPregunta;
    }
    
    
    
}
