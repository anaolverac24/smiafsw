/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view.capitulo1;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIData;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import Token.DecToken;
import mx.gob.semarnat.mia.dao.MiaDao;
import mx.gob.semarnat.mia.dao.ProyectoDAO;
import mx.gob.semarnat.mia.dao.ReiaCategoriasDAO;
// import javax.servlet.http.HttpServletRequest;
import mx.gob.semarnat.mia.dao.ReiaCondicionesDAO;
import mx.gob.semarnat.mia.dao.ReiaObrasDAO;
import mx.gob.semarnat.mia.dao.ReiaProyCondicionesDAO;
import mx.gob.semarnat.mia.dao.ReiaProyObrasDAO;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.ReiaCategorias;
// import mx.gob.semarnat.mia.model.ReiaCategorias;
import mx.gob.semarnat.mia.model.ReiaCondiciones;
import mx.gob.semarnat.mia.model.ReiaObras;
import mx.gob.semarnat.mia.model.ReiaObrasCondiciones;
import mx.gob.semarnat.mia.model.ReiaProyCondiciones;
import mx.gob.semarnat.mia.model.ReiaProyObras;
import mx.gob.semarnat.mia.view.to.CatalogoTO;

/**
 *
 * @author Paul Montoya
 */
@ManagedBean(name = "revisionObra")
@ViewScoped
public class RevisionObrasView implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(RevisionObrasView.class);

	private int currentStep = 0;

	private boolean validaPrimeraPregunta;
	
	//private boolean respuesta1;
	private Boolean respuesta1;

	//private boolean respuesta1Tab2;
	private Boolean respuesta1Tab2;

	//private boolean respuesta1Tab3;
	private Boolean respuesta1Tab3;

	private List<ObraActividad> listaObrasActividad;

	private List<ObraActividad> listaObrasActividadTab2;

	private List<Condiciones> listaCondicionesTab1;

	private ObraActividad selectedObraActividad;

	private List<ObraActividad> selectedObraActividadTab2;

	private static final int ZERO = 0;

	private static final int UNO = 1;

	private ReiaObrasDAO reiaObrasDAO = new ReiaObrasDAO();

	private ReiaCondicionesDAO reiaCondicionesDAO = new ReiaCondicionesDAO();

	private ReiaProyCondicionesDAO reiaProyCondicionesDAO = new ReiaProyCondicionesDAO();

	// private String continueStep2="Continue con el paso número 2. Presencia en
	// ecosistemas costeros xx";

	private String validacionexceptuada = ""; // 0: SI requiere validacion, 1:
												// NO requiere validacion

	private ReiaProyObrasDAO reiaProyObrasDAO = new ReiaProyObrasDAO();

	private static final String ID_REQ_PROYECTO = "msj.proyrequiereevaluacion.";

	private static final String ID_NO_REQ_PROYECTO = "msj.proynorequiereevaluacion.";

	private static final String BUNDLE_NAME = "resources/mensajesobra";

	private static final String SELECCION_OPCION = "msj.seleccioneValue";

	private static final String ID_MSJ_CONTINUAR = "msj.deseaContinuar";

	private static final String ID_MSJ_FINALIZAR = "msj.wizard.tab5.finalizar";

	private static final String ID_MSJ_OBRANO = "msj.obrano.";
	
	private static final String ID_MSJ_ADVERTENCIA_FINALIZACION = "msj.advertenciafinalizacion";

	private static final long ESTATUS_CATEGORIA_TAB5 = 1L;

	private static final int NUM_MAX_PAG = 10;

	private static final String EXCEPTUADA_NO = "0"; // SI requiere validacion

	private static final String EXCEPTUADA_SI = "1"; // NO requiere validacion

	private static final String COMPUERTA_Y = "y";

	private static final String COMPUERTA_O = "o";

	private static final String CLASS_REQUIERE_MIA = "alert-warning";
	private static final String CLASS_NO_REQUIERE_MIA = "alert-info";

	private String proyRequiereEvaluacion;

	private String proyNoRequiereEvaluacion;

	private String seleccione;

	private String mensajeTab;

	private String mensajeTab5;

	private String mensajeDialogo;

	private int indexTab;

	private int indexTabActual;

	private boolean btnSiguienteActivo;

	private List<ReiaProyObras> listaReiaProyObrasTab5;

	
	//private boolean respuesta1Tab4;
	private Boolean respuesta1Tab4;

	private List<ReiaCategoriasTO> listaCategoriasTab5;

	private ReiaCategoriasTO categoriasTab5Selected;

	private List<Condiciones> listaCondicionesTab5;

	// private List<ReiaProyObrasTO> listaReiaProyObrasTab5Selected;

	private String folioProyecto;
	
	private String userFolioProy;

	private boolean botonFinalizar;

	private String msjFinalizar;

	private boolean enabledFinalizar;

	private ResourceBundle resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);

	private String nombreObra;
	
	private String msjAdvertenciaFinalizacion;
	
	private boolean muestraadvertencia = Boolean.FALSE;
	
	private long idObraPrincipalSeleccionada;

	private List<ReiaProyObras> obrasSeleccionadas = new ArrayList<ReiaProyObras>();

	// @ManagedProperty(value = "#{principalView}")
	// private PrincipalView principalView;
	//
	/**
	 * Creates a new instance of miView
	 */
	public RevisionObrasView() {
	}

	@PostConstruct
	public void init() {
		setListaObrasActividad(new ArrayList<ObraActividad>());
		listaCondicionesTab1 = new ArrayList<Condiciones>();
		
		seleccione = resourceBundle.getString(SELECCION_OPCION);
		mensajeDialogo = resourceBundle.getString(ID_MSJ_CONTINUAR);
		msjFinalizar = resourceBundle.getString(ID_MSJ_FINALIZAR);
		msjAdvertenciaFinalizacion = resourceBundle.getString(ID_MSJ_ADVERTENCIA_FINALIZACION);
		indexTab = 1;
		indexTabActual = 1;
		muestraadvertencia = Boolean.FALSE;
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		String q = req.getQueryString();
		
		//folioProyecto = q.substring(q.indexOf("folioProy") + "folioProy".length() + 1);
        try{    
        userFolioProy = q.substring(q.indexOf("folioProy")+"folioProy".length()+1);
        }catch(NullPointerException nu){
                userFolioProy= (String)   context.getExternalContext().getSessionMap().get("folioProyecto");

        }
        
        System.out.println("folio Ant -- > " + userFolioProy);        
                                    
        //Primero se desencripta el folio del proyecto
                                       
        try
        {
            DecToken decToken = new DecToken ();            
            String vFolioEncriptado = userFolioProy;
                    
            folioProyecto = decToken.DecryptCentralToken(vFolioEncriptado);

            System.out.println("desencriptado--->    " + userFolioProy);
            
            
        }catch (Exception msg){
        	folioProyecto=userFolioProy;
        	LOGGER.info("No se pudo desencriptar el folio");
            //msg.printStackTrace();
        }		
		
		btnSiguienteActivo = true;
		buscarHistorialExistente();
	}

	/**
	 ************* FUNCIONES PARA RECUPRAR EL HISTORIAL DE LA INFORMACION DE LAS PESTAÑAS
	 * **************
	 */

	/**
	 * Busca si ya existen registros en el historial para este folio de proyecto
	 */
	public void buscarHistorialExistente() {
		int obrasGuardadas = reiaProyObrasDAO.countByFolio(folioProyecto);
		if (obrasGuardadas != 0) {
			llenarObrasSeleccionadas();
		}
	}

	/**
	 * Manda a llenar a las obras de cada pestaña con el historial de la base de
	 * datos
	 */
	public void llenarObrasSeleccionadas() {
		llenarObraTab1();
		llenarObraTab2();
		llenarObraTab3();
		llenarObraTab4();
		indexTab = indexTabActual;
		actualizaBotonSiguiente();
		indexTab = 1;
		indexTabActual++;
	}

	/**
	 * Llena la informacion precargada de la base de datos de la pestaña 1
	 */
	public void llenarObraTab1() {
		List<ReiaProyObras> reiaObrasTab = reiaProyObrasDAO.findByFolioCategoria(folioProyecto, 16L);
		if (reiaObrasTab != null && !reiaObrasTab.isEmpty()) {
			ReiaProyObras reiaObraGuardada = reiaObrasTab.get(0);
			LOGGER.debug("Folio: " + reiaObraGuardada.getFolio() + " Id obra" + reiaObraGuardada.getObra().getIdObra());
			ReiaObras reiaObra = reiaObrasDAO.find(reiaObraGuardada.getObra().getIdObra());

			if (reiaObra.getIdObra().equals(getObraNoPorTab())) {
				selectedObraActividad3 = new ObraActividad(reiaObra.getIdObra(), reiaObra.getObra(),
						reiaObra.getExcepcion());
				respuesta1 = Boolean.FALSE;
			} else {
				changeValueFirstQuestionSi();
				for (ObraActividad obraActividad : getListaObrasActividad()) {
					if (obraActividad.getId() == reiaObra.getIdObra()) {
						selectedObraActividad = obraActividad;
					}
				}
				llenarCondicionesTab1(reiaObraGuardada.getClave(), reiaObra.getIdObra());
			}
		}
	}

	/**
	 * Llena la informacion precargada de la base de datos de la pestaña 2
	 */
	public void llenarObraTab2() {
		List<ReiaProyObras> reiaObrasTab = reiaProyObrasDAO.findByFolioCategoria(folioProyecto, 18L);
		if (reiaObrasTab != null && !reiaObrasTab.isEmpty()) {
			indexTabActual++;

			selectedObraActividadTab2 = new ArrayList<ObraActividad>();
			if (reiaObrasTab.size() == 1 && reiaObrasTab.get(0).getObra().getIdObra().equals(getObraNoPorTab())) {
				respuesta1Tab2 = Boolean.FALSE;
			} else {
				changeValueFirstQuestionTab2Si();
				selectedObraActividadTab2 = new ArrayList<ObraActividad>();
				for (ReiaProyObras reiaProyObra : reiaObrasTab) {
					LOGGER.debug("Folio: " + reiaProyObra.getFolio() + " Id obra" + reiaProyObra.getObra().getIdObra());
					ReiaObras reiaObra = reiaObrasDAO.find(reiaProyObra.getObra().getIdObra());
					for (ObraActividad obraActividad : getListaObrasActividadTab2()) {
						if (obraActividad.getId() == reiaObra.getIdObra()) {
							selectedObraActividadTab2.add(obraActividad);
						}
					}
				}
			}
		}
	}

	/**
	 * Llena la informacion precargada de la base de datos de la pestaña 3
	 */
	public void llenarObraTab3() {
		List<ReiaProyObras> reiaObrasTab = reiaProyObrasDAO.findByFolioCategoria(folioProyecto, 19L);
		if (reiaObrasTab != null && !reiaObrasTab.isEmpty()) {
			indexTabActual++;

			ReiaProyObras reiaObraGuardada = reiaObrasTab.get(0);
			LOGGER.debug("Folio: " + reiaObraGuardada.getFolio() + " Id obra" + reiaObraGuardada.getObra().getIdObra());
			ReiaObras reiaObra = reiaObrasDAO.find(reiaObraGuardada.getObra().getIdObra());

			if (reiaObra.getIdObra().equals(getObraNoPorTab())) {
				selectedObraActividad3 = new ObraActividad(reiaObra.getIdObra(), reiaObra.getObra(),
						reiaObra.getExcepcion());
				respuesta1Tab3 = Boolean.FALSE;
			} else {
				changeValueThirdQuestionSi();
				for (ObraActividad obraActividad : getListaObrasActividad3()) {
					if (obraActividad.getId() == reiaObra.getIdObra()) {
						selectedObraActividad3 = obraActividad;
					}
				}
			}
		}
	}

	/**
	 * Llena la informacion precargada de la base de datos de la pestaña 4
	 */
	public void llenarObraTab4() {
		List<ReiaProyObras> reiaObrasTab = reiaProyObrasDAO.findByFolioCategoria(folioProyecto, 20L);
		if (reiaObrasTab != null & !reiaObrasTab.isEmpty()) {
			indexTabActual++;

			ReiaProyObras reiaObraGuardada = reiaObrasTab.get(0);
			LOGGER.debug("Folio: " + reiaObraGuardada.getFolio() + " Id obra" + reiaObraGuardada.getObra().getIdObra());
			ReiaObras reiaObra = reiaObrasDAO.find(reiaObraGuardada.getObra().getIdObra());

			if (reiaObra.getIdObra().equals(getObraNoPorTab())) {
				selectedObraActividad4 = new ObraActividad(reiaObra.getIdObra(), reiaObra.getObra(),
						reiaObra.getExcepcion());
				respuesta1Tab4 = Boolean.FALSE;
			} else {
				changeValueFourQuestionSi();
				for (ObraActividad obraActividad : getListaObrasActividad4()) {
					if (obraActividad.getId() == reiaObra.getIdObra()) {
						selectedObraActividad4 = obraActividad;
					}
				}
			}
		}
	}

	private Long getObraNoPorTab() {
		Long idObraNo = Long.parseLong(resourceBundle.getString(ID_MSJ_OBRANO + indexTabActual));
		return idObraNo;
	}

	/**
	 * Llena las condiciones precargadas de la obra de la pestaña 1
	 */
	public void llenarCondicionesTab1(Long claveObraProy, Long idObra) {
		List<ReiaProyCondiciones> reiaCondicionesObra = reiaProyCondicionesDAO.findByClaveObra(claveObraProy, idObra);
		for (ReiaProyCondiciones reiaProyCondicion : reiaCondicionesObra) {
			LOGGER.debug("Clave Obra Proy: " + reiaProyCondicion.getClaveObraProy().getClave() + "Condicion: " + reiaProyCondicion.getCondicion());
			ReiaCondiciones reiaCondicion = reiaCondicionesDAO.find(reiaProyCondicion.getCondicion());
			Condiciones condiciones = new Condiciones();
			condiciones.setId(reiaCondicion.getIdCondicion());
			condiciones.setCondicion(reiaCondicion.getCondicion());
			condiciones.setInciso(reiaCondicion.getInciso());
			condiciones.setRespuestaUser(reiaProyCondicion.getRespuesta() == 1L ? true : false);
			condiciones.setClaveObraProy(reiaProyCondicion.getClaveObraProy());
			listaCondicionesTab1.add(condiciones);
		}
	}

	/**
	 ************* FUNCIONES DE LAS PESTAÑAS **************
	 */

	private void reset() {
		selectedObraActividad = null;
	}

	public String onFlowProcess() {
		LOGGER.info("onFlowProcess....");
		String step = null;

		switch (indexTab) {
		case 1:
			saveObras(generarListaObras(selectedObraActividad), listaCondicionesTab1);
			break;
		case 2:
			saveObrasTab2(getSelectedObraActividadTab2());
			break;
		case 3:

			saveObras(generarListaObras(selectedObraActividad3), null);

			break;
		case 4:
			saveObras(generarListaObras(selectedObraActividad4), null);
			System.out.println("Pasando a la pestaña 5.....");
			loadTab5();
			botonFinalizar = Boolean.TRUE;			
			break;
		case 5:

			break;
		}

		indexTab++;
		indexTabActual++;
		return step;
	}

	/**
	 * Valida las condiciones
	 */
	public void validarCondiciones() {
		LOGGER.info("validarCondiciones.....");
		LOGGER.info("indexTab....." + indexTab);
		mensajeTab = "";
		proyRequiereEvaluacion = resourceBundle.getString(ID_REQ_PROYECTO + indexTab);
		proyNoRequiereEvaluacion = resourceBundle.getString(ID_NO_REQ_PROYECTO + indexTab);
		
		validaPrimeraPregunta=Boolean.TRUE;
		
		
		try{
			if (indexTab == 1) {
				validarCondicionesTab1();
	
			} else if (indexTab == 2) {
				validarCondicionesTab2();
	
			} else if (indexTab == 3) {
				validarCondicionesTab3();
	
			} else if (indexTab == 4) {
				validarCondicionesTab4();
			}
		}catch(NullPointerException npe){
			validaPrimeraPregunta=Boolean.FALSE;
		}
		
	}

	/**
	 * Se settea la obra por default al no elejir "no" como respuesta de la
	 * primer pregunta de cada pestaña
	 */
	private void elejirObraNo() {
		Long idObraNo = Long.valueOf(resourceBundle.getString(ID_MSJ_OBRANO + indexTab));
		ReiaObras reiaObraNo = reiaObrasDAO.find(idObraNo);
		if (indexTab == 1) {
			selectedObraActividad = new ObraActividad(reiaObraNo.getIdObra(), reiaObraNo.getObra(),
					reiaObraNo.getExcepcion());
		} else if (indexTab == 2) {
			if (selectedObraActividadTab2 == null) {
				selectedObraActividadTab2 = new ArrayList<ObraActividad>();
			}
			selectedObraActividadTab2.clear();
			selectedObraActividadTab2
					.add(new ObraActividad(reiaObraNo.getIdObra(), reiaObraNo.getObra(), reiaObraNo.getExcepcion()));
		} else if (indexTab == 3) {
			selectedObraActividad3 = new ObraActividad(reiaObraNo.getIdObra(), reiaObraNo.getObra(),
					reiaObraNo.getExcepcion());
		} else if (indexTab == 4) {
			selectedObraActividad4 = new ObraActividad(reiaObraNo.getIdObra(), reiaObraNo.getObra(),
					reiaObraNo.getExcepcion());
		}
	}

	private void validarCondicionesTab1() {
		if (respuesta1) {
			if (selectedObraActividad != null) {
				validacionexceptuada = EXCEPTUADA_NO;

				if (listaCondicionesTab1 == null || listaCondicionesTab1.isEmpty()) {
					if (selectedObraActividad.getExcepcion().equals(0L)) {
						mensajeTab = proyRequiereEvaluacion;
						validacionexceptuada = EXCEPTUADA_NO;
					} else {
						mensajeTab = proyNoRequiereEvaluacion;
						validacionexceptuada = EXCEPTUADA_SI;
					}
				} else {
					if (validaCondicionesTab1()) {
						mensajeTab = proyRequiereEvaluacion;
						validacionexceptuada = EXCEPTUADA_NO;
					} else {
						mensajeTab = proyNoRequiereEvaluacion;
						validacionexceptuada = EXCEPTUADA_SI;
					}
				}

			} else {
				// FacesContext.getCurrentInstance().addMessage(null, new
				// FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR",
				// seleccione));
				LOGGER.debug(seleccione);
				validacionexceptuada = EXCEPTUADA_NO;
			}
		} else {
			elejirObraNo();
			// si a la primer pregunta respondió que su proyecto no requiere
			// cambio de uso de suelo
			mensajeTab = proyNoRequiereEvaluacion;
			validacionexceptuada = EXCEPTUADA_SI;
		}
	}

	public void changeValueFirstQuestion() {
		reset();
		if (getListaObrasActividad() == null || getListaObrasActividad().size() == ZERO) {
			setListaObrasActividad(new ArrayList<ObraActividad>());
			List<ReiaObras> obras = obtenerObrasPorCategoria(16L);
			for (ReiaObras obra : obras) {
				getListaObrasActividad().add(new ObraActividad(obra.getIdObra(), obra.getObra(), obra.getExcepcion()));
			}
		}

	}

	public void changeValueObrasActividades() {
		listaCondicionesTab1 = new ArrayList<Condiciones>();
		LOGGER.info("*****************" + selectedObraActividad);
		if (selectedObraActividad != null) {
			listaCondicionesTab1 = findCondiciones(selectedObraActividad.getId());
		}
		actualizaBotonSiguiente();
	}

	private List<Condiciones> findCondiciones(Long idObra) {
		List<Condiciones> rtn = new ArrayList<Condiciones>();
		List<ReiaObrasCondiciones> listaCondiciones = findReiaCondiciones(idObra);
		if (listaCondiciones != null && !listaCondiciones.isEmpty()) {
			for (ReiaObrasCondiciones reiaCondiciones : listaCondiciones) {
				Condiciones condiciones = new Condiciones();
				condiciones.setId(reiaCondiciones.getIdCondicion().getIdCondicion());
				condiciones.setCondicion(reiaCondiciones.getIdCondicion().getCondicion());
				condiciones.setInciso(reiaCondiciones.getIdCondicion().getInciso());
				condiciones.setCompuerta(reiaCondiciones.getCompuerta());
				rtn.add(condiciones);
			}
		}
		return rtn;

	}
	
	/**
	 * Valida si alguna de las respuestas de las condiciones son nulas, es decir que no se selecciono "si" o "no" para la condicion
	 * @param listaCondiciones de la obra seleccionada
	 * @return bandera booleana
	 */
	private boolean todasCondicionesSeleccionadas(List<Condiciones> listaCondiciones) {
		for (Condiciones condiciones : listaCondiciones) {
			if (condiciones.isRespuestaUser() == null) {
				return false;
			}
		}
		return true;
	}

	private boolean validaCondicionesTab1() {
		boolean rtn = Boolean.FALSE;
		if (listaCondicionesTab1 != null && !listaCondicionesTab1.isEmpty()) {
			//Valida que las condiciones de la obra seleccionada tengan sus respuestas seleccionadas
			if (!this.todasCondicionesSeleccionadas(listaCondicionesTab1)) {
				//manda una excepcion de tipo NullPointerException para ser cachada por el 
				//metodo que lo llama y mostrar el mensaje de error
				throw new NullPointerException();
			}
			for (Condiciones condiciones : listaCondicionesTab1) {
				if (condiciones.isRespuestaUser()) {
					rtn = Boolean.TRUE;
					break;
				}
			}
		}

		return rtn;

	}

	private void saveObras(List<ObraActividad> listaObrasSeleccionadas,
			List<Condiciones> listaCondicionesSelecionadas) {
		LOGGER.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$saveObras");
		saveObras(listaObrasSeleccionadas, listaCondicionesSelecionadas, Boolean.FALSE);
	}

	private void saveObras(List<ObraActividad> listaObrasSeleccionadas, List<Condiciones> listaCondicionesSelecionadas,
			boolean principal) {
		LOGGER.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$saveObras");
		if (listaObrasSeleccionadas != null && !listaObrasSeleccionadas.isEmpty()) {

			for (ObraActividad obraActividad : listaObrasSeleccionadas) {

				if (obraActividad == null) {
					continue;
				}
				ReiaProyObras ReiaProyObras = new ReiaProyObras();

				// ReiaProyObras.setFolio(getClaveProyecto()!=null &&
				// !getClaveProyecto().equals("0")?getClaveProyecto()
				// :folioProyecto);
				ReiaProyObras.setFolio(folioProyecto);
				// La obra en su campo Principal simpre debe guardarse con 0 hasta elegir la que sera principal al dar clic al boton Finalizar
				ReiaProyObras.setPrincipal("0");
				ReiaObras reiaObras = reiaObrasDAO.find(obraActividad.getId());
				ReiaProyObras.setObra(reiaObras);
				//Guarda el valor de la excepcion pero en convinacion con las condiciones de la obra, lo cual
				//puede hacer que se guarde un valor distinto al que viene en el campo "excepcion" de la obra
				ReiaProyObras.setExceptuada(
						obraActividad.getExeptuada() != null ? obraActividad.getExeptuada() : validacionexceptuada);				
				reiaProyObrasDAO.create(ReiaProyObras);
				
				if (listaCondicionesSelecionadas != null && !listaCondicionesSelecionadas.isEmpty()) {
					saveCondiciones(listaCondicionesSelecionadas, ReiaProyObras);
				}
			}
		}
	}

	private void saveObrasTab2(
			List<ObraActividad> listaObrasSeleccionadas/*
														 * , List<Condiciones>
														 * listaCondicionesSelecionadas
														 */) {
		LOGGER.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$saveObras");
		if (listaObrasSeleccionadas != null && !listaObrasSeleccionadas.isEmpty()) {

			for (ObraActividad obraActividad : listaObrasSeleccionadas) {

				if (obraActividad == null)
					continue;

				ReiaProyObras ReiaProyObras = new ReiaProyObras();

				// ReiaProyObras.setFolio(getClaveProyecto()!=null &&
				// !getClaveProyecto().equals("0")?getClaveProyecto() :"11376");
				ReiaProyObras.setFolio(folioProyecto);
				ReiaProyObras.setPrincipal("0");
				ReiaObras reiaObras = reiaObrasDAO.find(obraActividad.getId());
				ReiaProyObras.setObra(reiaObras);
				// en el caso de ecosistemas costeros, necesita guardar para el
				// campo exceptuada, lo que traiga la obra en su campo excepcion
				ReiaProyObras.setExceptuada(obraActividad.getExcepcion().toString());
				reiaProyObrasDAO.create(ReiaProyObras);

			}
		}
	}

	private void saveCondiciones(List<Condiciones> listaCondicionesSelecionadas, ReiaProyObras claveObrasProy) {

		LOGGER.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$saveCondiciones");

		if (listaCondicionesSelecionadas != null && !listaCondicionesSelecionadas.isEmpty()) {
			for (Condiciones condiciones : listaCondicionesSelecionadas) {
				ReiaProyCondiciones reiaProyCondiciones = new ReiaProyCondiciones();
				reiaProyCondiciones.setCondicion(condiciones.getId());
				// reiaProyCondiciones.setFolio(getClaveProyecto()!=null &&
				// !getClaveProyecto().equals("0")?getClaveProyecto()
				// :folioProyecto);
				// reiaProyCondiciones.setFolio(folioProyecto);
				reiaProyCondiciones.setClaveObraProy(claveObrasProy);
				reiaProyCondiciones.setRespuesta(condiciones.isRespuestaUser() ? 1L : 0);
				reiaProyCondicionesDAO.create(reiaProyCondiciones);
			}

		}

	}

	public void actualizaBotonSiguiente() {
		if (indexTabActual == 1) {
			if (selectedObraActividad != null) {
				btnSiguienteActivo = true;
			} else {
				btnSiguienteActivo = false;
			}
		} else if (indexTabActual == 2) {
			if (selectedObraActividadTab2 != null && !selectedObraActividadTab2.isEmpty()) {
				btnSiguienteActivo = true;
			} else {
				btnSiguienteActivo = false;
			}
		} else if (indexTabActual == 3) {
			if (selectedObraActividad3 != null) {
				btnSiguienteActivo = true;
			} else {
				btnSiguienteActivo = false;
			}
		} else if (indexTabActual == 4) {
			if (selectedObraActividad4 != null) {
				btnSiguienteActivo = true;
			} else {
				btnSiguienteActivo = false;
			}
		}

	}

	private List<ReiaObras> obtenerObrasPorCategoria(Long categoria) {
		return reiaObrasDAO.obtenerObrasPorCategoria(categoria);
	}

	private List<ReiaObrasCondiciones> findReiaCondiciones(Long idObra) {
		return reiaCondicionesDAO.findCondicionesPorObra(idObra);
	}

	/**
	 * @return the currentStep
	 */
	public int getCurrentStep() {
		return currentStep;
	}

	/**
	 * @param currentStep
	 *            the currentStep to set
	 */
	public void setCurrentStep(int currentStep) {
		this.currentStep = currentStep;
	}

//	/**
//	 * @return the respuesta1
//	 */
//	public boolean isRespuesta1() {
//		return respuesta1;
//	}
//
//	/**
//	 * @param respuesta1
//	 *            the respuesta1 to set
//	 */
//	public void setRespuesta1(boolean respuesta1) {
//		this.respuesta1 = respuesta1;
//	}

	
	
	
	/**
	 * @return the listaObrasActividad
	 */
	public List<ObraActividad> getListaObrasActividad() {
		return listaObrasActividad;
	}

	/**
	 * @return the respuesta1
	 */
	public Boolean getRespuesta1() {
		return respuesta1;
	}

	/**
	 * @param respuesta1 the respuesta1 to set
	 */
	public void setRespuesta1(Boolean respuesta1) {
		this.respuesta1 = respuesta1;
	}

	/**
	 * @param listaObrasActividad
	 *            the listaObrasActividad to set
	 */
	public void setListaObrasActividad(List<ObraActividad> listaObrasActividad) {
		this.listaObrasActividad = listaObrasActividad;
	}

	/**
	 * @return the selectedObraActividad
	 */
	public ObraActividad getSelectedObraActividad() {
		return selectedObraActividad;
	}

	/**
	 * @param selectedObraActividad
	 *            the selectedObraActividad to set
	 */
	public void setSelectedObraActividad(ObraActividad selectedObraActividad) {
		this.selectedObraActividad = selectedObraActividad;
	}

	/**
	 * @return the listaCondicionesTab1
	 */
	public List<Condiciones> getListaCondicionesTab1() {
		return listaCondicionesTab1;
	}

	/**
	 * @param listaCondicionesTab1
	 *            the listaCondicionesTab1 to set
	 */
	public void setListaCondicionesTab1(List<Condiciones> listaCondicionesTab1) {
		this.listaCondicionesTab1 = listaCondicionesTab1;
	}

	/**
	 * @param principalView
	 *            the principalView to set
	 */
	// public void setPrincipalView(PrincipalView principalView) {
	// this.principalView = principalView;
	// }

//	/**
//	 * @return the respuesta1Tab2
//	 */
//	public boolean isRespuesta1Tab2() {
//		return respuesta1Tab2;
//	}
//
//	/**
//	 * @param respuesta1Tab2
//	 *            the respuesta1Tab2 to set
//	 */
//	public void setRespuesta1Tab2(boolean respuesta1Tab2) {
//		this.respuesta1Tab2 = respuesta1Tab2;
//	}



//	/**
//	 * @return the respuesta1Tab3
//	 */
//	public boolean isRespuesta1Tab3() {
//		return respuesta1Tab3;
//	}

	/**
	 * @return the respuesta1Tab2
	 */
	public Boolean getRespuesta1Tab2() {
		return respuesta1Tab2;
	}

	/**
	 * @param respuesta1Tab2 the respuesta1Tab2 to set
	 */
	public void setRespuesta1Tab2(Boolean respuesta1Tab2) {
		this.respuesta1Tab2 = respuesta1Tab2;
	}

	
	
	
	
//	/**
//	 * @param respuesta1Tab3
//	 *            the respuesta1Tab3 to set
//	 */
//	public void setRespuesta1Tab3(boolean respuesta1Tab3) {
//		this.respuesta1Tab3 = respuesta1Tab3;
//	}

	/**
	 * @return the respuesta1Tab3
	 */
	public Boolean getRespuesta1Tab3() {
		return respuesta1Tab3;
	}

	/**
	 * @param respuesta1Tab3 the respuesta1Tab3 to set
	 */
	public void setRespuesta1Tab3(Boolean respuesta1Tab3) {
		this.respuesta1Tab3 = respuesta1Tab3;
	}

	
//	public boolean isRespuesta1Tab4() {
//		return respuesta1Tab4;
//	}
//
//	public void setRespuesta1Tab4(boolean respuesta1Tab4) {
//		this.respuesta1Tab4 = respuesta1Tab4;
//	}

	
	
	
	/**
	 * @return the mensajeDialogo
	 */
	public String getMensajeDialogo() {
		return mensajeDialogo;
	}

	/**
	 * @return the respuesta1Tab4
	 */
	public Boolean getRespuesta1Tab4() {
		return respuesta1Tab4;
	}

	/**
	 * @param respuesta1Tab4 the respuesta1Tab4 to set
	 */
	public void setRespuesta1Tab4(Boolean respuesta1Tab4) {
		this.respuesta1Tab4 = respuesta1Tab4;
	}

	/**
	 * @param mensajeDialogo
	 *            the mensajeDialogo to set
	 */
	public void setMensajeDialogo(String mensajeDialogo) {
		this.mensajeDialogo = mensajeDialogo;
	}

	/**
	 * @return the mensajeTab
	 */
	public String getMensajeTab() {
		return mensajeTab;
	}

	/**
	 * @param mensajeTab
	 *            the mensajeTab to set
	 */
	public void setMensajeTab(String mensajeTab) {
		this.mensajeTab = mensajeTab;
	}

	/**
	 * @return the mensajeTab5
	 */
	public String getMensajeTab5() {
		return mensajeTab5;
	}

	/**
	 * @param mensajeTab5
	 *            the mensajeTab5 to set
	 */
	public void setMensajeTab5(String mensajeTab5) {
		this.mensajeTab5 = mensajeTab5;
	}

	/********************* INICIA Tab2 *****************/

	@SuppressWarnings("unused")
	private void validarCondicionesTab2() {

		if (respuesta1Tab2) {

			if (selectedObraActividadTab2 != null && !selectedObraActividadTab2.isEmpty()) {

				mensajeTab = proyNoRequiereEvaluacion;
				// validacionexceptuada = EXCEPTUADA_SI; // esta variable no se
				// requiere para pestaña 2

				LOGGER.info("Obras seleccionadas en Ecosistemas costeros..");
				int totalobras = selectedObraActividadTab2.size();
				int no_excepcion = 0;
				int si_excepcion = 0;
				for (ObraActividad obra : selectedObraActividadTab2) {

					LOGGER.info("obra_id :" + obra.getId() + " - categoría :" + obra.getCategoria() + " - descripcion :"
							+ obra.getDescripcion() + " - excepcion :" + obra.getExcepcion());
					if (obra.getExcepcion() == 0) {
						no_excepcion++;
					} else {
						si_excepcion++;
					}
				}

				// si selecciona una obra con excepción = 0 entonces mostrar
				// mensaje de que si requiere evaluación
				if (no_excepcion > 0) {
					mensajeTab = proyRequiereEvaluacion;
				} else {
					mensajeTab = proyNoRequiereEvaluacion;
				}

			} else {
				LOGGER.debug(seleccione);
			}
		} else {
			elejirObraNo();
			mensajeTab = proyNoRequiereEvaluacion;

		}
	}

	public void changeValueFirstQuestionTab2() {
		LOGGER.info("init changeValueFirstQuestionTab2");
		selectedObraActividadTab2 = null;
		Long idObraNo = Long.valueOf(resourceBundle.getString(ID_MSJ_OBRANO + 2));

		if (getListaObrasActividadTab2() == null || getListaObrasActividadTab2().size() == ZERO) {
			setListaObrasActividadTab2(new ArrayList<ObraActividad>());
			List<ReiaObras> obras = obtenerObrasPorCategoria(18L);
			for (ReiaObras obra : obras) {
				//Se valida que la obra que se itera no sea la de la opcion "NO"
				if (!obra.getIdObra().equals(idObraNo)) {
					getListaObrasActividadTab2()
					.add(new ObraActividad(obra.getIdObra(), obra.getObra(), obra.getExcepcion()));
				}				
			}
		}
	}

	public void changeValueFirstQuestionTab2Si() {
		LOGGER.info("init changeValueFirstQuestionTab2Si");
		respuesta1Tab2 = Boolean.TRUE;
		changeValueFirstQuestionTab2();
		actualizaBotonSiguiente();
	}

	public void changeValueFirstQuestionTab2No() {
		LOGGER.info("init changeValueFirstQuestionTab2No");
		respuesta1Tab2 = Boolean.FALSE;
		elejirObraNo();
		actualizaBotonSiguiente();
	}

	/********************* INICIA Tab3 *****************/
	private void validarCondicionesTab3() {
		if (respuesta1Tab3) {
			if (selectedObraActividad3 != null) {
				mensajeTab = proyRequiereEvaluacion;
				validacionexceptuada = EXCEPTUADA_NO;

				if (selectedObraActividad3.getExcepcion().equals(0L)) {
					mensajeTab = proyRequiereEvaluacion;
					validacionexceptuada = EXCEPTUADA_NO;
				} else {
					mensajeTab = proyNoRequiereEvaluacion;
					validacionexceptuada = EXCEPTUADA_SI;
				}
			} else {
				LOGGER.debug(seleccione);
				validacionexceptuada = EXCEPTUADA_NO;
			}
		} else {
			elejirObraNo();
			mensajeTab = proyNoRequiereEvaluacion;
			validacionexceptuada = EXCEPTUADA_SI;
		}
	}

	/**
	 * @return the listaObrasActividadTab2
	 */
	public List<ObraActividad> getListaObrasActividadTab2() {
		return listaObrasActividadTab2;
	}

	/**
	 * @param listaObrasActividadTab2
	 *            the listaObrasActividadTab2 to set
	 */
	public void setListaObrasActividadTab2(List<ObraActividad> listaObrasActividadTab2) {
		this.listaObrasActividadTab2 = listaObrasActividadTab2;
	}

	/**
	 * @return the selectedObraActividadTab2
	 */
	public List<ObraActividad> getSelectedObraActividadTab2() {
		return selectedObraActividadTab2;
	}

	/**
	 * @param selectedObraActividadTab2
	 *            the selectedObraActividadTab2 to set
	 */
	public void setSelectedObraActividadTab2(List<ObraActividad> selectedObraActividadTab2) {
		this.selectedObraActividadTab2 = selectedObraActividadTab2;
	}

	/**
	 * @return the indexTab
	 */
	public int getIndexTab() {
		return indexTab;
	}

	/**
	 * @param indexTab
	 *            the indexTab to set
	 */
	public void setIndexTab(int indexTab) {
		System.out.println("indexTab=" + indexTab);
		this.indexTab = indexTab;
		botonFinalizar = indexTab == 5;
		System.out.println("botonFinalizar=" + botonFinalizar);
		
		
		if(!btnSiguienteActivo && indexTab==indexTabActual){
			if(indexTab==1 && (respuesta1==null || !respuesta1)){
				btnSiguienteActivo=Boolean.TRUE;
			}else if(indexTab==2 && (respuesta1Tab2==null || !respuesta1Tab2)){
				btnSiguienteActivo=Boolean.TRUE;
			}else if(indexTab==3 && (respuesta1Tab3==null ||!respuesta1Tab3)){
				btnSiguienteActivo=Boolean.TRUE;
			}if(indexTab==4 && (respuesta1Tab4==null ||!respuesta1Tab4)){
				btnSiguienteActivo=Boolean.TRUE;
			}
		}
		
		if (botonFinalizar) {
			loadTab5();
			enabledFinalizar = listaReiaProyObrasTab5 != null && !listaReiaProyObrasTab5.isEmpty();
			System.out.println("enabledFinalizar=" + enabledFinalizar);
			mensajeTab = msjFinalizar;
		}

	}

	/************* FINALIZA TAB 2 ********/
	/************************/

	public void changeValueFirstQuestionNo() {
		LOGGER.info("changeValueFirstQuestionNo()");
		respuesta1 = Boolean.FALSE;
		elejirObraNo();
		actualizaBotonSiguiente();
	}

	public void changeValueFirstQuestionSi() {
		LOGGER.info("changeValueFirstQuestionSi()");
		respuesta1 = Boolean.TRUE;
		changeValueFirstQuestion();
		actualizaBotonSiguiente();
	}

	public void changeValueFirstQuestionNo(Long id) {
		LOGGER.info("changeValueFirstQuestionNo(" + id + ")");
		changeRespListaCondiciones(id, Boolean.FALSE);

	}

	public void changeValueFirstQuestionSi(Long id) {
		LOGGER.info("changeValueFirstQuestionSi(" + id + ")");
		changeRespListaCondiciones(id, Boolean.TRUE);
	}

	private void changeRespListaCondiciones(Long id, boolean flag) {
		if (listaCondicionesTab1 != null && !listaCondicionesTab1.isEmpty()) {
			for (int r = 0; r < listaCondicionesTab1.size(); r++) {
				if (listaCondicionesTab1.get(r).getId().equals(id)) {
					listaCondicionesTab1.get(r).setRespuestaUser(flag);
					break;
				}
			}
		}
	}

	// PESTAÑA 3. Mantos de Agua
	private ObraActividad selectedObraActividad3;
	private List<ObraActividad> listaObrasActividad3;

	public void changeValueThirdQuestionNo() {
		LOGGER.info("changeValueThirdQuestionNo()");
		respuesta1Tab3 = Boolean.FALSE;
		elejirObraNo();
		actualizaBotonSiguiente();
	}

	public void changeValueThirdQuestionSi() {
		LOGGER.info("changeValueThirdQuestionSi()");
		respuesta1Tab3 = Boolean.TRUE;
		changeValueThirdQuestion();
		actualizaBotonSiguiente();
	}

	public void changeValueThirdQuestion() {
		LOGGER.info("init changeValueThirdQuestion");
		reset3();

		if (getListaObrasActividad3() == null || getListaObrasActividad3().size() == ZERO) {
			setListaObrasActividad3(new ArrayList<ObraActividad>());
			List<ReiaObras> obras = obtenerObrasPorCategoria(19L);
			for (ReiaObras obra : obras) {
				getListaObrasActividad3().add(new ObraActividad(obra.getIdObra(), obra.getObra(), obra.getExcepcion()));
			}
		}
	}

	private void reset3() {
		selectedObraActividad3 = null;
	}

	/**
	 * @return the selectedObraActividad3
	 */
	public ObraActividad getSelectedObraActividad3() {
		return selectedObraActividad3;
	}

	/**
	 * @param selectedObraActividad3
	 *            the selectedObraActividad3 to set
	 */
	public void setSelectedObraActividad3(ObraActividad selectedObraActividad3) {
		this.selectedObraActividad3 = selectedObraActividad3;
	}

	/**
	 * @return the listaObrasActividad3
	 */
	public List<ObraActividad> getListaObrasActividad3() {
		return listaObrasActividad3;
	}

	/**
	 * @param listaObrasActividad3
	 *            the listaObrasActividad3 to set
	 */
	public void setListaObrasActividad3(List<ObraActividad> listaObrasActividad3) {
		this.listaObrasActividad3 = listaObrasActividad3;
	}

	// PESTAÑA 4. ANP
	private ObraActividad selectedObraActividad4;
	private List<ObraActividad> listaObrasActividad4;

	private void validarCondicionesTab4() {
		if (respuesta1Tab4) {
			if (selectedObraActividad4 != null) {
				mensajeTab = proyRequiereEvaluacion;
				validacionexceptuada = EXCEPTUADA_NO;

				if (selectedObraActividad4.getExcepcion().equals(0L)) {
					mensajeTab = proyRequiereEvaluacion;
					validacionexceptuada = EXCEPTUADA_NO;
				} else {
					mensajeTab = proyNoRequiereEvaluacion;
					validacionexceptuada = EXCEPTUADA_SI;
				}
			} else {
				LOGGER.debug(seleccione);
				validacionexceptuada = EXCEPTUADA_NO;
			}
		} else {
			elejirObraNo();
			mensajeTab = proyNoRequiereEvaluacion;
			validacionexceptuada = EXCEPTUADA_SI;
		}
	}

	public void changeValueFourQuestionNo() {
		LOGGER.info("changeValueFourQuestionNo()");
		respuesta1Tab4 = Boolean.FALSE;
		elejirObraNo();
		actualizaBotonSiguiente();
	}

	public void changeValueFourQuestionSi() {
		LOGGER.info("changeValueFourQuestionSi()");
		respuesta1Tab4 = Boolean.TRUE;
		changeValueFourQuestion();
		actualizaBotonSiguiente();
	}

	public void changeValueFourQuestion() {
		LOGGER.info("init changeValueFourQuestion");
		reset4();

		if (getListaObrasActividad4() == null || getListaObrasActividad4().size() == ZERO) {
			setListaObrasActividad4(new ArrayList<ObraActividad>());
			List<ReiaObras> obras = obtenerObrasPorCategoria(20L);
			for (ReiaObras obra : obras) {
				getListaObrasActividad4().add(new ObraActividad(obra.getIdObra(), obra.getObra(), obra.getExcepcion()));
			}
		}
	}

	private void reset4() {
		selectedObraActividad4 = null;
	}

	/**
	 * @return the selectedObraActividad4
	 */
	public ObraActividad getSelectedObraActividad4() {
		return selectedObraActividad4;
	}

	/**
	 * @param selectedObraActividad4
	 *            the selectedObraActividad4 to set
	 */
	public void setSelectedObraActividad4(ObraActividad selectedObraActividad4) {
		this.selectedObraActividad4 = selectedObraActividad4;
	}

	/**
	 * @return the listaObrasActividad3
	 */
	public List<ObraActividad> getListaObrasActividad4() {
		return listaObrasActividad4;
	}

	/**
	 * @param listaObrasActividad3
	 *            the listaObrasActividad3 to set
	 */
	public void setListaObrasActividad4(List<ObraActividad> listaObrasActividad4) {
		this.listaObrasActividad4 = listaObrasActividad4;
	}

	private List<ObraActividad> generarListaObras(ObraActividad obraActividad) {
		List<ObraActividad> listaObras = new ArrayList<ObraActividad>();
		LOGGER.info("obraActividad=" + obraActividad);
		listaObras.add(obraActividad);
		return listaObras;
	}

	/**
	 * @return the indexTabActual
	 */
	public int getIndexTabActual() {
		return indexTabActual;
	}

	/**
	 * @param indexTabActual
	 *            the indexTabActual to set
	 */
	public void setIndexTabActual(int indexTabActual) {
		this.indexTabActual = indexTabActual;
	}

	/**
	 * @return the btnSiguienteActivo
	 */
	public boolean isBtnSiguienteActivo() {
		return btnSiguienteActivo;
	}

	/**
	 * @param btnSiguienteActivo
	 *            the btnSiguienteActivo to set
	 */
	public void setBtnSiguienteActivo(boolean btnSiguienteActivo) {
		this.btnSiguienteActivo = btnSiguienteActivo;
	}

	int numpag;

	/****************** INICIA TAB 5 *****************************/
	private void loadTab5() {		
		System.out.println("\nEN LOAD TAB5 ****************************");		
		this.listaReiaProyObrasTab5 = reiaProyObrasDAO.findByFolioObrasCapturadas(folioProyecto);
		enabledFinalizar = listaReiaProyObrasTab5 != null && !listaReiaProyObrasTab5.isEmpty();
		System.out.println("Total de obras: " + listaReiaProyObrasTab5.size());
	}
	
	/**
	 * Elimina las obras seleccionadas en la tabla del tab5
	 */
	public void eliminarObrasSeleccionadas() {
		System.out.println("Eliminando " + obrasSeleccionadas.size() + " obras............");
		Long condiciones = 0L;
		for (ReiaProyObras obra : obrasSeleccionadas) {
			condiciones = reiaProyCondicionesDAO.countCondiciones(obra.getClave());
			System.out.println("La Obra tiene: " + condiciones + " condiciones");
			if(condiciones != 0){
				reiaProyCondicionesDAO.eliminarExplosivoActividadEtapaHijo(obra.getClave());
			}
			reiaProyObrasDAO.remove(obra);
		}
		loadTab5();
		obrasSeleccionadas.clear();
		mensajeTab5 = "Obras eliminadas exitosamente.";
		System.out.println("Obras eliminadas exitosamente!");
	}

	/****************************/
	/****************************/
	boolean selectAllReiaProyObrasTab5;

	private UIData reiaProyObrasDataTable;

	/**
	 * @return the reiaProyObrasDataTable
	 */
	public UIData getReiaProyObrasDataTable() {
		return reiaProyObrasDataTable;
	}

	/**
	 * @param reiaProyObrasDataTable
	 *            the reiaProyObrasDataTable to set
	 */
	public void setReiaProyObrasDataTable(UIData reiaProyObrasDataTable) {
		this.reiaProyObrasDataTable = reiaProyObrasDataTable;
	}

	/**
	 * @return the selectAllReiaProyObrasTab5
	 */
	public boolean isSelectAllReiaProyObrasTab5() {
		return selectAllReiaProyObrasTab5;
	}

	/**
	 * @param selectAllReiaProyObrasTab5
	 *            the selectAllReiaProyObrasTab5 to set
	 */
	public void setSelectAllReiaProyObrasTab5(boolean selectAllReiaProyObrasTab5) {
		this.selectAllReiaProyObrasTab5 = selectAllReiaProyObrasTab5;
	}

	private Map<ReiaProyObrasTO, Boolean> reiaProyObrasTOSelected = new HashMap<ReiaProyObrasTO, Boolean>() {

		private static final long serialVersionUID = -336083889699124999L;

		@Override
		public Boolean get(Object object) {
			if (isSelectAllReiaProyObrasTab5()) {
				reiaProyObrasTOSelected.put((ReiaProyObrasTO) object, Boolean.TRUE);
				return Boolean.TRUE;
			}
			if (!containsKey(object)) {
				return Boolean.FALSE;
			}
			return super.get(object);
		};

	};

	/**
	 * @return the caractTOSelected
	 */
	public Map<ReiaProyObrasTO, Boolean> getReiaProyObrasTOSelected() {
		return reiaProyObrasTOSelected;
	}

	private PaginationHelper pagination;
	private int selectedItemIndex;
	@SuppressWarnings("rawtypes")
	private DataModel dtmdl = null;

	private void recreateModel() {
		dtmdl = null;
	}

	@SuppressWarnings("unused")
	private void recreatePagination() {
		pagination = null;
	}

	List<Integer> listaPaginas;

	/**
	 * @return the listaPaginas
	 */
	public List<Integer> getListaPaginas() {
		return listaPaginas;
	}

	/**
	 * @param listaPaginas
	 *            the listaPaginas to set
	 */
	public void setListaPaginas(List<Integer> listaPaginas) {
		this.listaPaginas = listaPaginas;
	}

	/**
	 * @return the listaCategoriasTab5
	 */
	public List<ReiaCategoriasTO> getListaCategoriasTab5() {
		return listaCategoriasTab5;
	}

	/**
	 * @param listaCategoriasTab5
	 *            the listaCategoriasTab5 to set
	 */
	public void setListaCategoriasTab5(List<ReiaCategoriasTO> listaCategoriasTab5) {
		this.listaCategoriasTab5 = listaCategoriasTab5;
	}

	/**
	 * @return the categoriasTab5Selected
	 */
	public ReiaCategoriasTO getCategoriasTab5Selected() {
		return categoriasTab5Selected;
	}

	/**
	 * @param categoriasTab5Selected
	 *            the categoriasTab5Selected to set
	 */

	/**
	 * @return the listaObrasActividadTab5
	 */
	public List<ObraActividad> getListaObrasActividadTab5() {
		return listaObrasActividadTab5;
	}

	/**
	 * @param listaObrasActividadTab5
	 *            the listaObrasActividadTab5 to set
	 */
	public void setListaObrasActividadTab5(List<ObraActividad> listaObrasActividadTab5) {
		this.listaObrasActividadTab5 = listaObrasActividadTab5;
	}

	/**
	 * @return the obrasActividadTab5Selected
	 */
	public ObraActividad getObrasActividadTab5Selected() {
		return obrasActividadTab5Selected;
	}

	/**
	 * @param obrasActividadTab5Selected
	 *            the obrasActividadTab5Selected to set
	 */
	public void setObrasActividadTab5Selected(ObraActividad obrasActividadTab5Selected) {
		this.obrasActividadTab5Selected = obrasActividadTab5Selected;
	}

	/**
	 * @return the listaCondicionesTab5
	 */
	public List<Condiciones> getListaCondicionesTab5() {
		return listaCondicionesTab5;
	}

	/**
	 * @param listaCondicionesTab5
	 *            the listaCondicionesTab5 to set
	 */
	public void setListaCondicionesTab5(List<Condiciones> listaCondicionesTab5) {
		this.listaCondicionesTab5 = listaCondicionesTab5;
	}

	private List<ObraActividad> listaObrasActividadTab5;

	private ObraActividad obrasActividadTab5Selected;

	public void setCategoriasTab5Selected(ReiaCategoriasTO categoriasTab5Selected) {
		this.categoriasTab5Selected = categoriasTab5Selected;
	}

	public void loadAddActidadesRegulas() {
		LOGGER.info("loadAddActidadesRegulas");
		ReiaCategoriasDAO reiaCategoriasDAO = new ReiaCategoriasDAO();
		listaCategoriasTab5 = new ArrayList<ReiaCategoriasTO>();
		categoriasTab5Selected = null;
		listaObrasActividadTab5 = new ArrayList<ObraActividad>();
		obrasActividadTab5Selected = null;

		List<ReiaCategorias> listaCategoriasEntity = reiaCategoriasDAO.findByEstatus(ESTATUS_CATEGORIA_TAB5);
		for (ReiaCategorias reiaCategorias : listaCategoriasEntity) {
			ReiaCategoriasTO reiaCategoriasTO = new ReiaCategoriasTO();
			reiaCategoriasTO.setCategoria(reiaCategorias.getCategoria());
			reiaCategoriasTO.setIdCategoria(reiaCategorias.getIdCategoria());
			listaCategoriasTab5.add(reiaCategoriasTO);
		}

		LOGGER.info("categoriasTab5Selected" + categoriasTab5Selected);
	}

	public void changeValueCategoriasTab5() {
		LOGGER.info("changeValueCategoriasTab5");
		listaObrasActividadTab5 = new ArrayList<ObraActividad>();
		obrasActividadTab5Selected = null;
		if (categoriasTab5Selected != null) {
			List<ReiaObras> listaObrasCategorias = obtenerObrasPorCategoria(categoriasTab5Selected.getIdCategoria());

			for (ReiaObras reiaObras : listaObrasCategorias) {
				listaObrasActividadTab5
						.add(new ObraActividad(reiaObras.getIdObra(), reiaObras.getObra(), reiaObras.getExcepcion()));
			}
		}

	}

	public void resetDialog() {
		System.out.println("resetDialog()");
		categoriasTab5Selected = null;
	}

	public void changeValueObrasTab5() {
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ changeValueObrasTab5");
		listaCondicionesTab5 = new ArrayList<Condiciones>();
		if (obrasActividadTab5Selected != null) {

			listaCondicionesTab5 = findCondiciones(obrasActividadTab5Selected.getId());

		}
	}

	public void changeValueQuestionT5No(Long id) {
		LOGGER.info("changeValueQuestionT5No(" + id + ")");
		changeRespListaCondicionesT5(id, Boolean.FALSE);

	}

	public void changeValuetQuestionT5Si(Long id) {
		LOGGER.info("changeValuetQuestionT5Si(" + id + ")");
		changeRespListaCondicionesT5(id, Boolean.TRUE);
	}

	private void changeRespListaCondicionesT5(Long id, boolean flag) {
		if (listaCondicionesTab5 != null && !listaCondicionesTab5.isEmpty()) {
			for (int r = ZERO; r < listaCondicionesTab5.size(); r++) {
				if (listaCondicionesTab5.get(r).getId().equals(id)) {
					listaCondicionesTab5.get(r).setRespuestaUser(flag);
					break;
				}
			}
		}
	}

	public void saveObraTab5() {
		//Context para ejecutar comandos en la vista
		RequestContext context = RequestContext.getCurrentInstance();
		//Valida que la lista de condiciones de la obra tengan su respuesta seleccionada
		if (listaCondicionesTab5 != null && !this.todasCondicionesSeleccionadas(listaCondicionesTab5)) {
			//Si no estan todas las preguntas con respuestas se muestra la modal con el mensaje de error
			context.execute("$('#modalError').modal('show')");//$('#modalError').modal('show')
			return;
		}
		
		obrasActividadTab5Selected.setExeptuada(algoritmo() ? "1" : "0");

		proyNoRequiereEvaluacion = mensajeTab5 = obrasActividadTab5Selected.getExeptuada().equals("1")
				? resourceBundle.getString(ID_REQ_PROYECTO + indexTab)
				: resourceBundle.getString(ID_NO_REQ_PROYECTO + indexTab);
		saveObras(generarListaObras(obrasActividadTab5Selected), listaCondicionesTab5,
				listaReiaProyObrasTab5 != null && !listaReiaProyObrasTab5.isEmpty() ? Boolean.FALSE : Boolean.TRUE);
		pagination = null;
		selectedItemIndex = ZERO;
		dtmdl = null;
		loadTab5();
		if (botonFinalizar) {
			enabledFinalizar = listaReiaProyObrasTab5 != null && !listaReiaProyObrasTab5.isEmpty();
		}
		mensajeTab = msjFinalizar;
		context.execute("$('#myModalAdd').modal('hide')");
		context.execute("$('#myModalTab5').modal('show')");
		System.out.println("TTTTTTTTTTTTTTTTTTTTTTTTTTT   mensajeTab5 =" + mensajeTab5);
	}

	/**
	 * @return the botonFinalizar
	 */
	public boolean isBotonFinalizar() {
		return botonFinalizar;
	}

	/**
	 * @param botonFinalizar
	 *            the botonFinalizar to set
	 */
	public void setBotonFinalizar(boolean botonFinalizar) {
		this.botonFinalizar = botonFinalizar;
	}

	/**
	 * @return the enabledFinalizar
	 */
	public boolean isEnabledFinalizar() {
		return enabledFinalizar;
	}

	/**
	 * @param enabledFinalizar
	 *            the enabledFinalizar to set
	 */
	public void setEnabledFinalizar(boolean enabledFinalizar) {
		this.enabledFinalizar = enabledFinalizar;
	}

	/**
	 * @return the nombreObra
	 */
	public String getNombreObra() {
		return nombreObra;
	}

	/**
	 * @param nombreObra
	 *            the nombreObra to set
	 */
	public void setNombreObra(String nombreObra) {
		this.nombreObra = nombreObra;
	}

	private boolean showMsjFinalizar;

	/**
	 * @return the showMsjFinalizar
	 */
	public boolean isShowMsjFinalizar() {
		return showMsjFinalizar;
	}

	/**
	 * @param showMsjFinalizar
	 *            the showMsjFinalizar to set
	 */
	public void setShowMsjFinalizar(boolean showMsjFinalizar) {
		this.showMsjFinalizar = showMsjFinalizar;
	}

	public void changeModalFinalizar() {
		showMsjFinalizar = Boolean.TRUE;
	}

	public void continueNombreFinalizar() {
		showMsjFinalizar = Boolean.TRUE;
		muestraadvertencia = Boolean.TRUE;
	}

	public void saveNombreFinalizar() {
		System.out.println("*************************Guardando Proyecto, nombre: " + nombreObra + " Obra principal: " + idObraPrincipalSeleccionada);
		RequestContext context = RequestContext.getCurrentInstance();
		
		if (nombreObra == null || nombreObra.equals("") || idObraPrincipalSeleccionada == 0) {
			context.execute("$('#dlgModalAdvertencia').modal('show')");
			return;
		}
		Long idPrincipal = null;
		
		for (ReiaProyObras reiaProyObras : listaReiaProyObrasTab5) {
			if (reiaProyObras.getClave() == idObraPrincipalSeleccionada) {
				idPrincipal = reiaProyObras.getObra().getIdObra();
				reiaProyObras.setPrincipal("1");
				reiaProyObrasDAO.edit(reiaProyObras);
				break;
			}
		}
		if (idPrincipal != null) {
			ReiaObras reiaObras = reiaObrasDAO.find(idPrincipal);
			System.out.println("reiaObras" + reiaObras.getIdObra());
			Proyecto proyecto = new Proyecto(folioProyecto, (short) 1);
			proyecto.setProyNombre(nombreObra);
			proyecto.setNrama(reiaObras.getRama() != null ? reiaObras.getRama().shortValue() : null);
			proyecto.setNsub(reiaObras.getSubsector() != null ? reiaObras.getSubsector().shortValue() : null);
			proyecto.setNtipo(reiaObras.getTipo() != null ? reiaObras.getTipo().shortValue() : null);
			ProyectoDAO proyectoDAO = new ProyectoDAO();
			proyectoDAO.create(proyecto);
			
			// guardamos el proyecto para luego meterle un avance de 4%
			MiaDao miaDao = new MiaDao();			
			try {
				miaDao.merge(proyecto);
				
				// este guardado se hace sustituyendo al guardado que hacia el mia al inicio ( en el apartado de datos generales del proyecto ) pero ya 
				// no lo hace, por eso se hace aqui
				miaDao.guardarAvance(proyecto, new Short("4"), "11");
                
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		    try {
				ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI()+"?folioProy="+ userFolioProy);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		muestraadvertencia = Boolean.FALSE;
		context.execute("$('#myModalName').modal('hide')");
	}

	private boolean algoritmo() {
		boolean requiereMia = Boolean.FALSE;
		if (obrasActividadTab5Selected != null && (listaCondicionesTab5 == null || listaCondicionesTab5.isEmpty())) {
			if (obrasActividadTab5Selected.getExcepcion().equals(new Long(ZERO))) {
				requiereMia = Boolean.TRUE;
			} else if (obrasActividadTab5Selected.getExcepcion().equals(new Long(UNO))) {
				requiereMia = Boolean.FALSE;
			}
		} else if (obrasActividadTab5Selected != null) {
			if (obrasActividadTab5Selected.getExcepcion().equals(new Long(ZERO)) && compuertasIguales(COMPUERTA_O)) {
				if (!respuestasIguales(Boolean.FALSE)) {
					requiereMia = Boolean.TRUE;
				} else {
					requiereMia = Boolean.FALSE;
				}
			} else if (obrasActividadTab5Selected.getExcepcion().equals(new Long(ZERO))
					&& compuertasIguales(COMPUERTA_Y)) {
				if (respuestasIguales(Boolean.TRUE)) {
					requiereMia = Boolean.TRUE;
				} else {
					requiereMia = Boolean.FALSE;
				}
			} else if (obrasActividadTab5Selected.getExcepcion().equals(new Long(UNO))
					&& compuertasIguales(COMPUERTA_O)) {
				if (!respuestasIguales(Boolean.TRUE)) {
					requiereMia = Boolean.TRUE;
				} else {
					requiereMia = Boolean.FALSE;
				}
			} else if (obrasActividadTab5Selected.getExcepcion().equals(new Long(UNO))
					&& compuertasIguales(COMPUERTA_Y)) {
				if (respuestasIguales(Boolean.FALSE)) {
					requiereMia = Boolean.TRUE;
				} else {
					requiereMia = Boolean.FALSE;
				}
			}
		}
		return requiereMia;
	}

	private boolean compuertasIguales(String compuerta) {
		boolean iguales = Boolean.TRUE;
		if (listaCondicionesTab5 != null && !listaCondicionesTab5.isEmpty()) {
			for (Condiciones condiciones : listaCondicionesTab5) {
				if (!condiciones.getCompuerta().equals(compuerta)) {
					iguales = Boolean.FALSE;
					break;
				}
			}
		}
		return iguales;
	}

	private boolean respuestasIguales(boolean respuesta) {
		boolean iguales = Boolean.TRUE;
		if (listaCondicionesTab5 != null && !listaCondicionesTab5.isEmpty()) {
			for (Condiciones condiciones : listaCondicionesTab5) {
				if (!condiciones.isRespuestaUser() == respuesta) {
					iguales = Boolean.FALSE;
					break;
				}
			}
		}
		return iguales;
	}

	/**
	 * @return the validaPrimeraPregunta
	 */
	public boolean isValidaPrimeraPregunta() {
		return validaPrimeraPregunta;
	}

	/**
	 * @param validaPrimeraPregunta the validaPrimeraPregunta to set
	 */
	public void setValidaPrimeraPregunta(boolean validaPrimeraPregunta) {
		this.validaPrimeraPregunta = validaPrimeraPregunta;
	}

	public String getMsjFinalizar() {
		return msjFinalizar;
	}

	public void setMsjFinalizar(String msjFinalizar) {
		this.msjFinalizar = msjFinalizar;
	}

	public String getMsjAdvertenciaFinalizacion() {
		return msjAdvertenciaFinalizacion;
	}

	public void setMsjAdvertenciaFinalizacion(String msjAdvertenciaFinalizacion) {
		this.msjAdvertenciaFinalizacion = msjAdvertenciaFinalizacion;
	}

	public boolean isMuestraadvertencia() {
		return muestraadvertencia;
	}

	public void setMuestraadvertencia(boolean muestraadvertencia) {
		this.muestraadvertencia = muestraadvertencia;
	}

	/**
	 * @return the listaReiaProyObrasTab5
	 */
	public List<ReiaProyObras> getListaReiaProyObrasTab5() {
		return listaReiaProyObrasTab5;
	}

	/**
	 * @param listaReiaProyObrasTab5 the listaReiaProyObrasTab5 to set
	 */
	public void setListaReiaProyObrasTab5(List<ReiaProyObras> listaReiaProyObrasTab5) {
		this.listaReiaProyObrasTab5 = listaReiaProyObrasTab5;
	}

	/**
	 * @return the obrasSeleccionadas
	 */
	public List<ReiaProyObras> getObrasSeleccionadas() {
		return obrasSeleccionadas;
	}

	/**
	 * @param obrasSeleccionadas the obrasSeleccionadas to set
	 */
	public void setObrasSeleccionadas(List<ReiaProyObras> obrasSeleccionadas) {
		this.obrasSeleccionadas = obrasSeleccionadas;
	}

	/**
	 * @return the idObraPrincipalSeleccionada
	 */
	public long getIdObraPrincipalSeleccionada() {
		return idObraPrincipalSeleccionada;
	}

	/**
	 * @param idObraPrincipalSeleccionada the idObraPrincipalSeleccionada to set
	 */
	public void setIdObraPrincipalSeleccionada(long idObraPrincipalSeleccionada) {		
		this.idObraPrincipalSeleccionada = idObraPrincipalSeleccionada;
	}
}
