/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view.capitulo1;

import mx.gob.semarnat.mia.view.to.CatalogoTO;

/**
 *
 * @author marcog
 */
public class ObraActividad {
    private Long id;
    private String descripcion;
    private Long excepcion;
    private CatalogoTO categoria;
    private String exeptuada;
    private String styleClass;
    
    public ObraActividad() {    	
    }
    
    public ObraActividad(Long id,String descripcion,Long excepcion){
        this.id=id;
        this.descripcion=descripcion;
        this.excepcion=excepcion;
    }
    
    
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "ObraActividad{" + "id=" + id + ", descripcion=" + descripcion + ", excepcion=" + excepcion + '}';
    }
    
    
    
    
    /**
	 * @return the categoria
	 */
	public CatalogoTO getCategoria() {
		return categoria;
	}


	/**
	 * @param categoria the categoria to set
	 */
	public void setCategoria(CatalogoTO categoria) {
		this.categoria = categoria;
	}


	/**
     * @return the excepcion
     */
    public Long getExcepcion() {
        return excepcion;
    }

    /**
     * @param excepcion the excepcion to set
     */
    public void setExcepcion(Long excepcion) {
        this.excepcion = excepcion;
    }


	/**
	 * @return the exeptuada
	 */
	public String getExeptuada() {
		return exeptuada;
	}


	/**
	 * @param exeptuada the exeptuada to set
	 */
	public void setExeptuada(String exeptuada) {
		this.exeptuada = exeptuada;
	}


	/**
	 * @return the styleClass
	 */
	public String getStyleClass() {
		return styleClass;
	}


	/**
	 * @param styleClass the styleClass to set
	 */
	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}
    
    
    
}
