package mx.gob.semarnat.mia.view.capitulo1;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

/**
 * The Class PersonaConverter.
 */
@ManagedBean (name="categoriaConverter")
public class CategoriaConverter2 extends SelectItemsBaseConverter {
    
    /* (non-Javadoc)
     * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof ReiaCategoriasTO) || ((ReiaCategoriasTO) value).getIdCategoria() == null) {
            return null;
        }

        return String.valueOf(((ReiaCategoriasTO) value).getIdCategoria());
    }
}
