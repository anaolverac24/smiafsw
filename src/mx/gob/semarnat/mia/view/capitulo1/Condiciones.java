/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view.capitulo1;

import mx.gob.semarnat.mia.model.ReiaProyObras;

/**
 *
 * @author marcog
 */
public class Condiciones {
    
    private Long id;
    
    private String condicion;
    
    private Long respuesta;
    
    private Long inciso;
    
    private Long antecedente;

    private Boolean respuestaUser;
     
    private String compuerta;
    
    private ReiaProyObras claveObraProy;
    
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the condicion
     */
    public String getCondicion() {
        return condicion;
    }

    /**
     * @param condicion the condicion to set
     */
    public void setCondicion(String condicion) {
        this.condicion = condicion;
    }

    /**
     * @return the respuesta
     */
    public Long getRespuesta() {
        return respuesta;
    }

    /**
     * @return the inciso
     */
    public Long getInciso() {
        return inciso;
    }

    /**
     * @param inciso the inciso to set
     */
    public void setInciso(Long inciso) {
        this.inciso = inciso;
    }

    /**
     * @return the antecedente
     */
    public Long getAntecedente() {
        return antecedente;
    }

    /**
     * @param antecedente the antecedente to set
     */
    public void setAntecedente(Long antecedente) {
        this.antecedente = antecedente;
    }

    /**
     * @return the respuestaUser
     */
    public Boolean isRespuestaUser() {
        return respuestaUser;
    }

    /**
	 * @return the compuerta
	 */
	public String getCompuerta() {
		return compuerta;
	}

	/**
	 * @param compuerta the compuerta to set
	 */
	public void setCompuerta(String compuerta) {
		this.compuerta = compuerta;
	}

	/**
	 * @return the respuestaUser
	 */
	public Boolean getRespuestaUser() {
		return respuestaUser;
	}

	/**
	 * @param respuestaUser the respuestaUser to set
	 */
	public void setRespuestaUser(Boolean respuestaUser) {
		this.respuestaUser = respuestaUser;
	}

	/**
	 * @return the claveObraProy
	 */
	public ReiaProyObras getClaveObraProy() {
		return claveObraProy;
	}

	/**
	 * @param claveObraProy the claveObraProy to set
	 */
	public void setClaveObraProy(ReiaProyObras claveObraProy) {
		this.claveObraProy = claveObraProy;
	}

}
