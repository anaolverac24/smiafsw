/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import mx.gob.semarnat.mia.dao.ArchivoBean;
import mx.gob.semarnat.mia.dao.CargaBean;
import mx.gob.semarnat.mia.dao.SigeiaDao;
import mx.gob.semarnat.mia.dao.SueloVegetacionDetalleDAO;
import mx.gob.semarnat.mia.dao.VegFaseDAO;
import mx.gob.semarnat.mia.dao.VegGrupoDAO;
import mx.gob.semarnat.mia.dao.VegTipoDAO;
import mx.gob.semarnat.mia.model.AccidentesProyecto;
import mx.gob.semarnat.mia.model.ActividadEtapa;
import mx.gob.semarnat.mia.model.ArchivosProyecto;
import mx.gob.semarnat.mia.model.AvanceProyecto;
import mx.gob.semarnat.mia.model.CatClasificacion;
import mx.gob.semarnat.mia.model.CatClasificacionB;
import mx.gob.semarnat.mia.model.CatContaminante;
import mx.gob.semarnat.mia.model.CatEtapa;
import mx.gob.semarnat.mia.model.CatServicio;
import mx.gob.semarnat.mia.model.CatSustanciaAltamRiesgosa;
import mx.gob.semarnat.mia.model.ContaminanteProyecto;
import mx.gob.semarnat.mia.model.ContaminanteProyectoPK;
import mx.gob.semarnat.mia.model.CriteriosProyecto;
import mx.gob.semarnat.mia.model.EtapaProyecto;
import mx.gob.semarnat.mia.model.GlosarioProyecto;
import mx.gob.semarnat.mia.model.ImpacAmbProyecto;
import mx.gob.semarnat.mia.model.InfobioProyecto;
import mx.gob.semarnat.mia.model.InfobioProyectoPK;
import mx.gob.semarnat.mia.model.InversionEtapas;
import mx.gob.semarnat.mia.model.InversionEtapasPK;
import mx.gob.semarnat.mia.model.MedPrevImpactProy;
import mx.gob.semarnat.mia.model.PrediocolinProy;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.ServicioProyecto;
import mx.gob.semarnat.mia.model.SueloVegetacionDetalle;
import mx.gob.semarnat.mia.model.SueloVegetacionProyecto;
import mx.gob.semarnat.mia.model.SustanciaProyecto;
import mx.gob.semarnat.mia.model.UsoSueloVeget;
import mx.gob.semarnat.mia.model.VegFase;
import mx.gob.semarnat.mia.model.VegGrupo;
import mx.gob.semarnat.mia.model.VegTipo;
import mx.gob.semarnat.mia.model.catalogos.CatUnidadMedida;
import mx.gob.semarnat.mia.util.Contaminanteu;
import mx.gob.semarnat.mia.util.Log;
import mx.gob.semarnat.mia.util.NodeSueloUtil;
import mx.gob.semarnat.mia.validacion.Validacion;

import org.primefaces.context.RequestContext;
import org.primefaces.model.TreeNode;


/**
 *
 * @author mauricio
 */
@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class Capitulo2View extends CapitulosComentarios implements Serializable {

    private String seccion = "";
	//private MiaDao miaDao = new MiaDao();
    private SigeiaDao sinDao = new SigeiaDao();
    private Proyecto proyecto = new Proyecto();
    private Short NSubSector = (short) 0;
    private DecimalFormat df = new DecimalFormat("##.##");
    // Seleccion del sitio
    private List<CriteriosProyecto> criterios = new ArrayList<CriteriosProyecto>();
    private List<CriteriosProyecto>crit_seleccionados = new ArrayList<CriteriosProyecto>();
    private List<CriteriosProyecto> selectedCriterios;
    private CriteriosProyecto selectedCriterio;
    private CriteriosProyecto editCriterio;
    // Ubicacion
    private Boolean domEsta = false;
    private Boolean domLoca = false;
    //Inversión y Empleos
    private List<InversionEtapas> inversionE = new ArrayList<InversionEtapas>();
    private InversionEtapas inversionEtapas;
    private boolean proyInversionPE;
    
    /**
     * Indica si la pagina web que se esta ejecutando es: Inversion requerida
     */
    private String pageInversionReq = "";


	// Servicios Requeridos
    private boolean capServicios = false;
    private List<ServicioProyecto> servicios = new ArrayList<ServicioProyecto>();
    private List<ServicioProyecto>serv_seleccionados = new ArrayList<ServicioProyecto>();
    private ServicioProyecto nuevoServicio;
    private ServicioProyecto editarServicio = new ServicioProyecto();
    
    // Informacion Bio
    private InfobioProyecto infoBio = new InfobioProyecto();
    private List<InfobioProyecto> infoBioList = new ArrayList<InfobioProyecto>();
    private Integer numElemBio = 0;
    // Duracion
    private EtapaProyecto etapaPreparacion;
    private EtapaProyecto etapaConstruccion;
    private EtapaProyecto etapaOperacion;
    private EtapaProyecto etapaAbandono;
    // Etapas
    private List<ActividadEtapa> actividadesPrepara = new ArrayList<ActividadEtapa>();
    private List<ActividadEtapa> actividadesConstru = new ArrayList<ActividadEtapa>();
    private List<ActividadEtapa> actividadesOperaci = new ArrayList<ActividadEtapa>();
    private List<ActividadEtapa> actividadesAbandon = new ArrayList<ActividadEtapa>();
    private Boolean verPreparacion = false;
    private Boolean verConstruccion = false;
    private Boolean verOperacion = false;
    private Boolean verAbandono = false;
    private Boolean verMsgEtapa = false;
    // Sustancias
    private boolean capSustancias = false;
    private List<SustanciaU> sustanciasList = new ArrayList<SustanciaU>();
    private List<SustanciaU> sustanciasListSelecciones = new ArrayList<SustanciaU>();
    private Boolean supera = false;
    // Explosivos
    private boolean capExplosivos = false;
    private List<ActividadEtapa> explosivosList = new ArrayList<ActividadEtapa>();
    // Contaminante
    private List<ContaminanteProyecto> contaminanteList = new ArrayList<ContaminanteProyecto>();
    private List<Contaminanteu> contaminanteuList = new ArrayList<Contaminanteu>();
    // Accidentes
    private List<AccidentesProyecto> accidenteList = new ArrayList<AccidentesProyecto>();
    private List<AccidentesProyecto> selectedAccidentes = new ArrayList<AccidentesProyecto>();
    private AccidentesProyecto accidente;
    private AccidentesProyecto editAccidente;
    
    // Uso suelo
    private List<PrediocolinProy> predCol = new ArrayList<PrediocolinProy>();
    private PrediocolinProy editPredio;
    private PrediocolinProy selectedPredio;
    private List<PrediocolinProy> selectedPredios;
    private List<SueloVegetacionProyecto> sueloVegetacion = new ArrayList<SueloVegetacionProyecto>();
    private List<SelectItem> catClasificacionB = new ArrayList<SelectItem>();
    // Dimensiones
    private List<UsoSueloVeget> usoSueloVeg = new ArrayList<UsoSueloVeget>();
    private List<EtapaProyecto> listaEtapas = new ArrayList<EtapaProyecto>();
    private List<SelectItem> sep = new ArrayList<SelectItem>();
    private List<EtapaProyecto> ep = new ArrayList<EtapaProyecto>();
    private Short id_add_bio;
    private Boolean servOtro = false;
    private Integer claveTramite = 0;
    private short subsec = 0;
    
    private short idSeleccionadoServicio;
    @SuppressWarnings("unused")
	private CatClasificacion clasificacionId;
    
    private PrediocolinProy predio;

    private String vseccion = "";
    
    private List<CatUnidadMedida> catUnidades;
    
    private String msj;
	private boolean bandera;
    
    @SuppressWarnings("unused")
	private int selectedItemIndex;
    
    // Sustancias Riesgosas
    private boolean proySustAplica;  // respuesta si o no
    // private List<CatEtapa> selectedEtapas;
    private CatEtapa selectedEtapa = new CatEtapa();    
    // private CatEtapa editEtapa;
    private SustanciaU editSustancia;
    private String sustanciaPromovente;  // sustancia que se guarda en el registro actual ( texto )
    
    private CatSustanciaAltamRiesgosa sustanciariesgosaId;
    private BigDecimal sustanciaCantidadAlmacenada; // cuando das de alta una nueva sustancia, guarda aqui la cantidad almacenada
    private short ctunClve;
    private String ctunDesc; // descripcion de unidad seleccionada
    private String sustanciaexcede = "0";  // 0: no, 1: si
    private String headerText;
    
    private ArchivosProyecto archivoTemporal = new ArchivosProyecto();
    private CargaBean cargaBean;
    
    private String primeraCarga = "1"; // indica que apenas se cargó la pantalla de sustancias, y no se ha dado clic alguno en la pantalla....

    //Atributos para los suelos y sus colindancias
    private TreeNode rootSuelos;
    private NodeSueloUtil nodeSueloUtil = new NodeSueloUtil();
    SueloVegetacionDetalleDAO detalleDao = new SueloVegetacionDetalleDAO();
    private SueloVegetacionDetalle sueloVegetacionDetalleSeleccionado = new SueloVegetacionDetalle();
    private VegGrupoDAO vegGrupoDAO = new VegGrupoDAO();
    private VegTipoDAO vegTipoDAO = new VegTipoDAO();
    private VegFaseDAO vegFaseDAO = new VegFaseDAO();
    private List<VegGrupo> listaVegGrupos = new ArrayList<VegGrupo>();
    private String headerTextDetalle;
    private String alertTextDetalle;
    private boolean edicionDetalleSuelo;
    private int idVegGrupo;
    private int idVegTipo;
    private int idVegFase;
    private double limiteSuperficie;
    private double superficieAcumulada;
    private double totalSuperficie;
    private double superficieIngresada;
	private SueloVegetacionProyecto editTipoVegetacion = new SueloVegetacionProyecto();
	private int idSueloVegetacionSelected;
	
	/**
	 * Numero secuencial del registro "Selección del sitio" a editar
	 */
	private int rowIndexSelSitEditar;
	
	/**
	 * Numero secuencial del registro "Selección del sitio" a editar
	 */
	private int rowIndexServReqEditar;
	
	/**
	 * Numero secuencial del registro "Uso de suelo de predios colindantes" a editar
	 */
	private int rowIndexUSPCEditar;
	
	/**
	 * Numero secuencial del registro "Sustancias riesgosas" a editar
	 */
	private int rowIndexSustREditar;
	
	/**
	 * Numero secuencial del registro "Posibles accidentes ambientales" a editar
	 */
	private long rowIndexPosAccAmbEditar;


	//<editor-fold desc="Constructor" defaultstate="collapsed">
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public Capitulo2View() {
    	//Cargando listas de Vegetacion
    	listaVegGrupos = vegGrupoDAO.findAll();
    	headerTextDetalle = "";
    	
        System.out.println("Capitulo 2 View");

        headerText = "";
        this.cargaBean = new CargaBean();
        selectedCriterio = new CriteriosProyecto();
        editCriterio = new CriteriosProyecto();
        catUnidades = miaDao.getCatUnidadMedida();
        predio = new PrediocolinProy();                

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator(',');
        df = new DecimalFormat("##.##", otherSymbols);

        // Recupera proyecto de sesion
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
        claveTramite = (Integer) context.getExternalContext().getSessionMap().get("claveTramite");
        subsec = (Short) context.getExternalContext().getSessionMap().get("subsector");
        id_add_bio = (Short) context.getExternalContext().getSessionMap().get("id_add_bio");
        
        System.out.println("Folio del Proyecto: "+folioProyecto);
        System.out.println("Serial del Proyecto: "+serialProyecto);
        System.out.println("SubSector del Proyecto: "+subsec);
        System.out.println("Rama del Proyecto: "+proyecto.getNrama());        

        //Cargar Proyecto
        try {
            this.proyecto = miaDao.proyecto(folioProyecto, serialProyecto);
            supera = proyecto.getEstudioRiesgoId().equals((short) 1);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }

        if (proyecto == null) {
            this.proyecto = new Proyecto(folioProyecto, serialProyecto);
        }

        if (proyecto.getNsub() != null) {
            NSubSector = proyecto.getNsub();
        }

        try {
        inversionEtapas = miaDao.getInversion(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }
        
        if (inversionEtapas == null) {
            this.inversionEtapas = new InversionEtapas();
        }
        
        if (proyecto.getProyMontoEtapasAplica() != null && proyecto.getProyMontoEtapasAplica().equals("N")){
        	this.proyInversionPE = false;
        	System.out.println("Aplica Monto por Etapas: " + proyInversionPE);
        }
        
        if (proyecto.getProyMontoEtapasAplica() != null && proyecto.getProyMontoEtapasAplica().equals("S")){
        	this.proyInversionPE = true;
        	System.out.println("Aplica Monto por Etapas: " + proyInversionPE);
        }
        System.out.println("Aplica Monto por Etapas Inicial: " + proyInversionPE);
        
        try {
            sueloVegetacion = miaDao.getSueloVegetacionProyecto(folioProyecto, serialProyecto);

            if (sueloVegetacion.isEmpty()) {

//                dimensionesUso = sinDao.usoSueloVegetacion(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                List<Object[]> d = sinDao.usoSueloVegetacion2(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                System.out.println("Uso de Suelo Vegetación ---------> " + d.size());
                for (Object[] u : d) {
                    Short id = miaDao.getMaxSueloVegetacionProyecto(folioProyecto, serialProyecto);
                    SueloVegetacionProyecto s = new SueloVegetacionProyecto(folioProyecto, serialProyecto, id);
                    System.out.println("id --> " + id + "  componente  " + u[0]);
                    s.setSueloComponente((String) u[0]);
                    s.setSueloDescripcion((String) u[1]);
                    s.setSueloTipoEcov((String) u[2]);
                    s.setSueloTipoGen((String) u[3]);
                    s.setSueloFaseVs((String) u[4]);
                    s.setUsoSueloInegi((String) u[7]);

//                    String usoSueloArea = (String) u[5];
                    Double sueloAreaS = (Double) u[5];
                    String usoSueloArea = Double.toString(sueloAreaS);
                    usoSueloArea = usoSueloArea.replace(",", "");
                    usoSueloArea = usoSueloArea.trim();
                    s.setSueloAreaSigeia(Double.parseDouble(usoSueloArea));

//                    String vSueloSuperficeProm = (String) u[6];
                    Double sueloSuperficie = (Double) u[6];
                    String vSueloSuperficeProm = Double.toString(sueloSuperficie);
                    vSueloSuperficeProm = vSueloSuperficeProm.replace(",", "");
                    vSueloSuperficeProm = vSueloSuperficeProm.trim();

                    s.setSueloSuperficeProm(Double.parseDouble(vSueloSuperficeProm));
//                    s.setUsoSueloInegi((String)u[7]);

                    System.out.println("se rompió el merge id --> " + id + "  descripcion  " + u[1]);
                    try {
                        miaDao.merge(s);
                    } catch (Exception ep) {
                        System.out.println("se rompió el merge id --> " + id + "  tipo ecov  " + u[2]);
                        ep.printStackTrace();
                    }
                    sueloVegetacion.add(s);
                }
            }
            rootSuelos = nodeSueloUtil.generarNodos(sueloVegetacion);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        //Vseccion 
        switch (proyecto.getNsub()) {
            case 4:
            case 7:
                vseccion = "6";
                break;
            case 6:
                vseccion = "7";
                break;
            default:
                vseccion = "5";
                break;
        }

// Ubicacion Proyecto establecido
        if (proyecto.getProyDomEstablecido() != null) {
            if (proyecto.getProyDomEstablecido().equals('S')) {
                domEsta = true;
                domLoca = false;
            }
            if (proyecto.getProyDomEstablecido().equals('N')) {
                domEsta = false;
                domLoca = true;
            }
        }

        // Criterios
        try {
            criterios = miaDao.getCriterios(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        } catch (Exception e) {
            System.out.println("Criterios no encontrados");
        }

        // Requiere servicios
        if (proyecto.getProyServreq() != null && proyecto.getProyServreq().equals('S')) {
            capServicios = true;
        }

		// Inversión y Empleos
		try {
			inversionE = miaDao.getInversiones(proyecto.getProyectoPK().getFolioProyecto(),
					proyecto.getProyectoPK().getSerialProyecto());
		} catch (Exception e) {
			System.out.println("Inversiones no encontradas");
		}

        // Requiere explosivos

        if (proyecto.getProyExplosivosAplica() != null && proyecto.getProyExplosivosAplica().equals("S")) {
            capExplosivos = true;
        }

        // Requiere sustancias
        if (proyecto.getProySustanciaAplica() != null && proyecto.getProySustanciaAplica().equals("S")) {
            capSustancias = true;
        }

        try {
            servicios = miaDao.getServicios(
                    getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
            if (servicios == null) {
                servicios = new ArrayList<ServicioProyecto>();
            }
            System.out.println("===== existen "+servicios.size()+ " servicios =========================================");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Duracion y etapas
        etapaPreparacion = cargaEtapaProyecto((short) 1);
        etapaConstruccion = cargaEtapaProyecto((short) 2);
        etapaOperacion = cargaEtapaProyecto((short) 3);
        etapaAbandono = cargaEtapaProyecto((short) 4);

        actividadesPrepara = miaDao.getActividadesEtapa(folioProyecto, serialProyecto, (short) 1);
        actividadesConstru = miaDao.getActividadesEtapa(folioProyecto, serialProyecto, (short) 2);
        actividadesOperaci = miaDao.getActividadesEtapa(folioProyecto, serialProyecto, (short) 3);
        actividadesAbandon = miaDao.getActividadesEtapa(folioProyecto, serialProyecto, (short) 4);

        verEtapas();

        // Sustancias
        for (SustanciaProyecto s : miaDao.getSustancias(folioProyecto, serialProyecto)) {
        	
        	sustanciasList.add(new SustanciaU(s, s.getSustanciaId().getSustanciaId().equals(new Short("9999")), 0, getUnidad(Short.parseShort(s.getSustanciaId().getSustanciaUnidad()))));
            
        }
        
        proySustAplica = Boolean.FALSE;
        proySustAplica = proyecto.getProySustanciaAplica()==null?Boolean.FALSE:(proyecto.getProySustanciaAplica().equalsIgnoreCase("S")?Boolean.TRUE:Boolean.FALSE);
        
        sustanciaexcede = "0";
        if(sustanciasList != null && sustanciasList.size()>0){
        	// proySustAplica = true;
        	        	
        	// como es el constructor.. si sustanciaexcede = 1 al final... la vista mostrara el mensaje correspondiente...
        	for (SustanciaU s : sustanciasList) {        		
        		if(s.model.getExcedeLimite().equalsIgnoreCase("1"))
        			sustanciaexcede = "1";        		
            }
        }
        
//        RequestContext context2 = RequestContext.getCurrentInstance();
//        if(proyecto.getEstudioRiesgoId()==1){        	
//        	context2.execute("mal1();");  
//        }else{
//        	context2.execute("mal2();");  
//        }
        
        System.out.println("\n\nSustancia aplica : " + proySustAplica);
                      
        
        // Explosivos
        explosivosList = miaDao.getActividadesEtapa(folioProyecto, serialProyecto);

        // Contaminantes
        contaminanteList = miaDao.getContaminantes(folioProyecto, serialProyecto);
        if (!contaminanteList.isEmpty()) {
            for (ContaminanteProyecto c1 : contaminanteList) {
                Contaminanteu c = new Contaminanteu();
                c.setContaminante(c1);
//                c.setContaminanteCat(c1.getCatContaminante());

                List<SelectItem> stmp = new ArrayList();
                List<CatContaminante> ltmp = new ArrayList();
                ltmp.clear();
                Short tipo = c1.getContaminanteTipo();
                if (tipo != null && tipo != 0) {
                    ltmp = miaDao.getContaminantes(tipo);
                    for (CatContaminante t : ltmp) {
                        SelectItem s = new SelectItem(t, t.getContaminanteNombre());
                        stmp.add(s);
                    }
                    c.setContaminanteCat(stmp);
                }
                contaminanteuList.add(c);

            }
//            contaminanteuList
//            ContaminanteProyecto c1 = new ContaminanteProyecto(folioProyecto, serialProyecto, etapaPreparacion.getEtapaProyectoPK().getEtapaId(), (short) 1);
//            ContaminanteProyecto c2 = new ContaminanteProyecto(folioProyecto, serialProyecto, etapaConstruccion.getEtapaProyectoPK().getEtapaId(), (short) 1);
//            ContaminanteProyecto c3 = new ContaminanteProyecto(folioProyecto, serialProyecto, etapaOperacion.getEtapaProyectoPK().getEtapaId(), (short) 1);
//            ContaminanteProyecto c4 = new ContaminanteProyecto(folioProyecto, serialProyecto, etapaAbandono.getEtapaProyectoPK().getEtapaId(), (short) 1);
//            contaminanteList.add(c1);
//            contaminanteList.add(c2);
//            contaminanteList.add(c3);
//            contaminanteList.add(c4);
        }

        //lista etapas del proyecto
        listaEtapas = miaDao.getEtapaProyecto(folioProyecto, serialProyecto);

        // Accidentes
        accidenteList = miaDao.getAccidente(folioProyecto, serialProyecto);

        try {
            ep = miaDao.getEtapaProyecto(folioProyecto, serialProyecto);
            for (EtapaProyecto e : ep) {
                SelectItem s = new SelectItem(e, e.getCatEtapa().getEtapaDescripcion());
                sep.add(s);
            }
        } catch (Exception e2) {

        }

        // Uso de suelo
        predCol = miaDao.getPrediocolinProy(folioProyecto, serialProyecto);

        // Info bio
        infoBioList = miaDao.getInfoBio(folioProyecto, serialProyecto);
        numElemBio = infoBioList.size();

        if (id_add_bio != null) {
            System.out.println("id_add_bio " + id_add_bio);
            infoBio = miaDao.getInfoBio(folioProyecto, serialProyecto, id_add_bio);
        } else {
            System.out.println("no hay id_add_bio ");
            Short id = 0;
            try {
                id = miaDao.getMaxInfoBio(folioProyecto, serialProyecto);
                id++;
            } catch (Exception ww) {

            }
            if (id == null) {
                id = 1;
            }
            infoBio = new InfobioProyecto(folioProyecto, serialProyecto, id);
        }

        //Comentarios proyecto - eescalona
        super.setProyectoInfo((short) 2, folioProyecto, serialProyecto);
        
        
        
    }
    //</editor-fold>
    
    /**
     * Crea un nuevo objeto SueloVegetacionDetalle
     */
    @SuppressWarnings("rawtypes")
	public void crearNuevoDetalleSuelo() {
    	System.out.println("Creando nuevo objeto de detalle de suelo....");
    	Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		short paramId = 0;
		String id = params.get("id").toString();
		paramId = Short.parseShort(id);
    	sueloVegetacionDetalleSeleccionado = new SueloVegetacionDetalle(proyecto.getProyectoPK().getFolioProyecto(),
    			proyecto.getProyectoPK().getSerialProyecto(), paramId);
    	sueloVegetacionDetalleSeleccionado.setVegGrupo(new VegGrupo());
    	sueloVegetacionDetalleSeleccionado.setVegTipo(new VegTipo());
    	sueloVegetacionDetalleSeleccionado.setVegFase(new VegFase());    	
    	
    	//Calculo de datos para la superficie en la ventana modal
    	superficieAcumulada = detalleDao.obtenerSumaSuperficieActual(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), paramId);
    	SueloVegetacionProyecto slP = miaDao.getSueloVegetacionProyecto(proyecto.getProyectoPK().getFolioProyecto(),
    			proyecto.getProyectoPK().getSerialProyecto(), paramId);
    	totalSuperficie = slP.getSueloAreaSigeia();
    	limiteSuperficie = totalSuperficie - superficieAcumulada;
    	limiteSuperficie =  Math.rint(limiteSuperficie*100)/100;
    	headerTextDetalle = "Registrar nuevo detalle para el suelo #" + paramId;
    	edicionDetalleSuelo = false;
    	alertTextDetalle = "Detalle de suelo guardado.";
    }
    
    /**
     * Carga el SueloVegetacionDetalle seleccionado
     */
    @SuppressWarnings("rawtypes")
	public void cargarDetalleSueloSeleccionado() {  
    	System.out.println("Recuperando detalle de suelo....");
    	Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int paramId = 0;
		String id = params.get("id").toString();
		paramId = Integer.parseInt(id);
    	this.sueloVegetacionDetalleSeleccionado = detalleDao.find(paramId);
    	idVegGrupo = sueloVegetacionDetalleSeleccionado.getVegGrupo().getIdVegGrupo();
    	idVegTipo = sueloVegetacionDetalleSeleccionado.getVegTipo().getIdVegTipo();
    	idVegFase = sueloVegetacionDetalleSeleccionado.getVegFase().getIdVegFase();
    	
    	//Calculo de datos para la superficie en la ventana modal
    	superficieAcumulada = detalleDao.obtenerSumaSuperficieActual(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),
    			sueloVegetacionDetalleSeleccionado.getSueloVegetacionProyecto().getSueloVegetacionProyectoPK().getSueloVegProyId());
    	SueloVegetacionProyecto slP = miaDao.getSueloVegetacionProyecto(proyecto.getProyectoPK().getFolioProyecto(),
    			proyecto.getProyectoPK().getSerialProyecto(), sueloVegetacionDetalleSeleccionado.getSueloVegetacionProyecto().getSueloVegetacionProyectoPK().getSueloVegProyId());
    	totalSuperficie = slP.getSueloAreaSigeia();
    	limiteSuperficie = totalSuperficie - (superficieAcumulada - sueloVegetacionDetalleSeleccionado.getSuperficie());
    	
    	headerTextDetalle = "Actualizar detalle de suelo #" + paramId;
    	edicionDetalleSuelo = true;
    	alertTextDetalle = "Detalle de suelo actualizado.";
    }
    
    public void consultarTipoVegetacion(){
    	Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idSueloVegetacion = params.get("id").toString();
		id = Integer.parseInt(idSueloVegetacion);
		System.out.println("Registro de Suelo: "+id);
		editTipoVegetacion = new SueloVegetacionProyecto();
		editTipoVegetacion = miaDao.getSueloVegetacionProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) id);
		setIdSueloVegetacionSelected(id);
    	
    }
    
    public void editarTipoVegetacion(){
    	try{
    		miaDao.merge(editTipoVegetacion);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditar.hide();modalMensajeEditar.show()");
    	} catch (Exception e) {
            e.printStackTrace();
        }
    	sueloVegetacion = miaDao.getSueloVegetacionProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    	rootSuelos = nodeSueloUtil.generarNodos(sueloVegetacion);
    	
    }
    
    /**
     * Guarda o actualiza el detalle de suelo
     */
    public void guardarDetalleSuelo() {    	
    	try {
    		if (!edicionDetalleSuelo) {
    			detalleDao.create(sueloVegetacionDetalleSeleccionado);
    			System.out.println("Guardando detalle de suelo....");
    		} else {
    			detalleDao.edit(sueloVegetacionDetalleSeleccionado);
    			System.out.println("Editando detalle de suelo....");
    		}			
		} catch (Exception e) {
			e.printStackTrace();
		}
    	cargarSuelosVegetacion();
    }
    
    /**
     * Elimina un detalle de suelo
     */
    public void eliminarDetalleSuelo() {
    	System.out.println("Eliminando detalle con Id: " + sueloVegetacionDetalleSeleccionado.getIdSueloVegetacionDetalle());
		try {
			detalleDao.remove(sueloVegetacionDetalleSeleccionado);
		} catch (Exception e) {
			e.printStackTrace();
		}
		alertTextDetalle = "Detalle de suelo eliminado.";
		cargarSuelosVegetacion();
    }
    
    /**
     * Carga los vegTipos
     */
    public void mostrarVegTipos() {
    	System.out.println("Mostrando hijos de Grupo: " + idVegGrupo);
    	VegGrupo veg = vegGrupoDAO.find(idVegGrupo);
    	sueloVegetacionDetalleSeleccionado.setVegGrupo(veg);
    }
    
    /**
     * Carga los vegFases
     */
    public void mostrarVegFases() {
    	System.out.println("Mostrando hijos de Tipo: " + idVegTipo);
    	VegTipo veg = vegTipoDAO.find(idVegTipo);
    	sueloVegetacionDetalleSeleccionado.setVegTipo(veg);
    	sueloVegetacionDetalleSeleccionado.setVegFase(sueloVegetacionDetalleSeleccionado.getVegTipo().getListaVegFases().get(0));
    	idVegFase = sueloVegetacionDetalleSeleccionado.getVegFase().getIdVegFase();
    }
    
    /**
     * Carga la fase seleccionada de tipo en sueloVegetacionDetalleSeleccionado
     */
    public void asignarFaseTipo() {
    	System.out.println("Seleccionando la fase: " + idVegFase);
    	VegFase veg = vegFaseDAO.find(idVegFase);
    	sueloVegetacionDetalleSeleccionado.setVegFase(veg);
    }
    
    /**
     * Carga los suelos de vegetacion
     */
    public void cargarSuelosVegetacion() {
    	try {
    		sueloVegetacion.clear();    		
            sueloVegetacion = miaDao.getSueloVegetacionProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            System.out.println("Tam suelos: " + sueloVegetacion.size());
            if (sueloVegetacion.isEmpty()) {

//                dimensionesUso = sinDao.usoSueloVegetacion(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                List<Object[]> d = sinDao.usoSueloVegetacion2(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                System.out.println("Uso de Suelo Vegetación ---------> " + d.size());
                for (Object[] u : d) {
                    Short id = miaDao.getMaxSueloVegetacionProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                    SueloVegetacionProyecto s = new SueloVegetacionProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
                    System.out.println("id --> " + id + "  componente  " + u[0]);
                    s.setSueloComponente((String) u[0]);
                    s.setSueloDescripcion((String) u[1]);
                    s.setSueloTipoEcov((String) u[2]);
                    s.setSueloTipoGen((String) u[3]);
                    s.setSueloFaseVs((String) u[4]);
                    s.setUsoSueloInegi((String) u[7]);

                    String usoSueloArea = (String) u[5];
                    usoSueloArea = usoSueloArea.replace(",", "");
                    usoSueloArea = usoSueloArea.trim();
                    s.setSueloAreaSigeia(Double.parseDouble(usoSueloArea));

                    String vSueloSuperficeProm = (String) u[6];
                    vSueloSuperficeProm = vSueloSuperficeProm.replace(",", "");
                    vSueloSuperficeProm = vSueloSuperficeProm.trim();

                    s.setSueloSuperficeProm(Double.parseDouble(vSueloSuperficeProm));
//                    s.setUsoSueloInegi((String)u[7]);

                    System.out.println("se rompió el merge id --> " + id + "  descripcion  " + u[1]);
                    try {
                        miaDao.merge(s);
                    } catch (Exception ep) {
                        System.out.println("se rompió el merge id --> " + id + "  tipo ecov  " + u[2]);
                        ep.printStackTrace();
                    }
                    sueloVegetacion.add(s);
                }
            }
            System.out.println("Convirtiendo suelos a nodos...");
            rootSuelos = nodeSueloUtil.generarNodos(sueloVegetacion);
        } catch (Exception e) {
            e.printStackTrace();
        }
    	rootSuelos = nodeSueloUtil.generarNodos(sueloVegetacion);
    }
    
    private CatUnidadMedida getUnidad(short unidad){
	    CatUnidadMedida unidadObj = null;	            	
	    unidadObj = miaDao.getCatUnidadMedidaPorClave(unidad);        
	   	return unidadObj;
    }

    public Short getEstudioRiesgoId() {
        try {
            if (proyecto.getEstudioRiesgoId() == null) {
                return 0;
            }
            return proyecto.getEstudioRiesgoId();
        } catch (Exception e) {
            return 0;
        }
    }

    private EtapaProyecto cargaEtapaProyecto(short etapaId) {
        EtapaProyecto ep = null;

        try {
            ep = miaDao.getEtapaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), etapaId);
        } catch (Exception e) {
            //e.printStackTrace();
        }

        if (ep == null) {
            ep = new EtapaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), etapaId);
        }

        ep.setEtapaDescripcionObraAct(".");

        try {
            miaDao.merge(ep);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(ep);
        return ep;
    }

    /**
     * Listenes seleccion domicilio establecido
     */
    public void selDomEsta() {
        if (proyecto.getProyDomEstablecido() != null) {
            if (proyecto.getProyDomEstablecido().equals('S')) {
                domEsta = true;
                domLoca = false;
            }
            if (proyecto.getProyDomEstablecido().equals('N')) {
                domEsta = false;
                domLoca = true;
            }
        }
    }

    public void muestraMensaje() {
    	
        System.out.println("Hola!! puedes ver este mensaje???");
    }

    public List<CriteriosProyecto> getCrit_seleccionados() {
		return crit_seleccionados;
	}

	public void setCrit_seleccionados(List<CriteriosProyecto> crit_seleccionados) {
		this.crit_seleccionados = crit_seleccionados;
	}

	public CriteriosProyecto getSelectedCriterio() {
        return selectedCriterio;
    }

    public void setSelectedCriterio(CriteriosProyecto selectedCriterio) {
        this.selectedCriterio = selectedCriterio;
    }

    public List<CriteriosProyecto> getSelectedCriterios() {
        return selectedCriterios;
    }

    public void setSelectedCriterios(List<CriteriosProyecto> selectedCriterios) {
        this.selectedCriterios = selectedCriterios;
    }
    
    /**
     * Genera un nuevo objeto criterio para realizar un alta
     */
    public void crearCriterio() {
    	RequestContext.getCurrentInstance().reset("fAgregar:pnlAgregar");
    	selectedCriterio = new CriteriosProyecto();
    }
    
    /**
     * Metodo postconstruct que se ejecuta al cargar la pagina
     */
    @PostConstruct
    public void init(){
    	selectedCriterio = new CriteriosProyecto();
    	selectedPredios = new ArrayList<PrediocolinProy>();
    	selectedAccidentes = new ArrayList<AccidentesProyecto>();
    	nuevoServicio = new ServicioProyecto();
    	servicios = miaDao.getServicios(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

    }
    
    /**
     * Agregar Criterio
     */
    public void agregarCriterio() {
        System.out.println("Agregando Criterio");
        
        if (criterios == null) {
            criterios = new ArrayList<CriteriosProyecto>();
        }
        
        System.out.println(
				"Datos Nuevos: " + selectedCriterio.getCriterio() + ", " + selectedCriterio.getCriterioDescripcion()
						+ ", " + selectedCriterio.getCriterioId().getCriterioNombre() + ", "
						+ selectedCriterio.getCriterioId().getCriterioId());
                
        Short id = 0;
        try {
            id = miaDao.getMaxCriterio(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            id = (short) (id.intValue() + 1);
            System.out.println("Registro: #" + id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (id == null) {
            id = 1;
            System.out.println("Registro era null pero ahora: #" + id);
        }
        

        CriteriosProyecto cp = new CriteriosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id, selectedCriterio.getCriterioId(), selectedCriterio.getCriterio(), selectedCriterio.getCriterioDescripcion());
        try {
        	System.out.println("MiaDao Persist!");
			miaDao.persist(cp);
			selectedCriterio = new CriteriosProyecto();
			RequestContext context = RequestContext.getCurrentInstance();
			System.out.println("Cerrando Modal Agregar y abriendo Modal Aviso");
			context.execute("modalAgregar.hide();modalAlert.show()");
			// System.out.println("Abriendo Modal Alerta Exitosa");
			// context.execute("modalAgregar.hide();");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Criterios Add!");
        criterios.add(cp);
    }

    /**
     * Editar un criterio ya existente
     */
    public void consultarCriterio() {
    	System.out.println("SELECCION DEL SITIO: Consultando registro.");
    	@SuppressWarnings("rawtypes")
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idCriterio = params.get("id").toString();
		id = Integer.parseInt(idCriterio);
		
		System.out.println("Actualizando criterio: "+id);
		editCriterio = new CriteriosProyecto();
		
		// Numero secuencial a mostrar en "Editar registro #"
		rowIndexSelSitEditar = Integer.parseInt(params.get("prowindexSS").toString());
		
		editCriterio = miaDao.getCriterio(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) id);
    }
    
    
    public void consultarSustancia() {
    	System.out.println("Consultando sustancia");
    	@SuppressWarnings("rawtypes")
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idSustancia = params.get("id").toString();
		id = Integer.parseInt(idSustancia);
		editSustancia = null;			
		SustanciaProyecto sp = miaDao.getSustancia(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short)id);		
		editSustancia = new SustanciaU(sp, sp.getSustanciaId().getSustanciaId().equals(new Short("9999")), 0, getUnidad(Short.parseShort(sp.getSustanciaId().getSustanciaUnidad())));
		
		sustanciariesgosaId = sp.getSustanciaId();
		sustanciaCantidadAlmacenada = sp.getSustanciaCantidadAlmacenada();
		ctunClve = sp.getCtunClve();
		ctunDesc = editSustancia.getUnidad().getCtunAbre();
		sustanciaPromovente = sp.getSustanciaPromovente();
		sustanciaexcede = editSustancia.getModel().getExcedeLimite();
				
		// Numero secuencial a mostrar en "Editar registro #"
		rowIndexSustREditar = Integer.parseInt(params.get("prowindexSust").toString());		
    }
    
    
    
    public void editarCriterio() {
    	CriteriosProyecto cp = new CriteriosProyecto();
    	cp = editCriterio;
        try {
            miaDao.persist(cp);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditar.hide();modalAlertE.show()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    // pool
    public void editarSustancia() {
    	System.out.println("editarSustancia..");
    	System.out.println("sustanciaexcede :" + sustanciaexcede);
    	SustanciaU su = editSustancia;
        try {
        	
        	su.model.setSustanciaPromovente(sustanciaPromovente);
        	su.model.setCtunClve(ctunClve);
        	su.model.setSustanciaCantidadAlmacenada(sustanciaCantidadAlmacenada);
        	su.model.setSustanciaId(sustanciariesgosaId);
        	su.model.setExcedeLimite(sustanciaexcede);
        	
        	
            miaDao.persist(su.model);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditar.hide();modalAlertE.show()");
			
			if (sustanciaexcede.equalsIgnoreCase("1")) {
		    	context.execute("mal1();"); // muestra el div de que alguna sustancia excede
		    	
		    	// muestra el menu superior de 'Estudio de Riesgo' ya que si esta editando es porque tiene seleccionado el boton 'si requiere sustancias'
		    	context.execute("parent.verER();");
		    	
		    	proyecto.setEstudioRiesgoId((short) 1);
		    	
		    } else {
		    			    	
		    	// valida si quita o no el menu superior de 'Estudio de Riesgo' porque no sabemos si hay mas sustancias que exceden para este proyecto...
		    	
		    	if (verificaCantidad()) {
		    		
		    		// muestra el div de que alguna sustancia excede
		    		context.execute("mal1();");  
		    	
		    		// muestra el menu 'Estudio de Riesgo'
			    	context.execute("parent.verER();");
			    	
	                proyecto.setEstudioRiesgoId((short) 1);
	                
	             
	            } else {
	            	
	            	// oculta el div de que alguna sustancia excede
		    		context.execute("mal2();");  
		    	
		    		// oculta el menu 'Estudio de Riesgo'
			    	context.execute("parent.verER2();");
		
	                proyecto.setEstudioRiesgoId((short) 0);
	            }
		    }

			// para guardar el estudioriesgoid
			miaDao.merge(proyecto);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Eliminar criterio
     *
     * @param e ActionEvent
     */
public void eliminarCriterio(Short idt) {
	System.out.println("Eliminando");
        CriteriosProyecto cp = new CriteriosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt);
        criterios.remove(cp);
        try {
            miaDao.eliminar(cp);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

public String eliminaCriterios() throws Exception {
	System.out.println("Eliminación Afirmativa");
    for (CriteriosProyecto criterioSeleccionado : crit_seleccionados) {              
        criterios.remove(criterioSeleccionado);        
        miaDao.eliminarCriterio(criterioSeleccionado.getCriteriosProyectoPK().getCriterioProyId());   
    }   
   return "SeleSitio";
}



public String tieneCriteriosSeleccionados(){
	System.out.println("Comprobando Chechbox´s");
	System.out.println(crit_seleccionados.size()+" seleccionados");
	if(!crit_seleccionados.isEmpty()){
		RequestContext context = RequestContext.getCurrentInstance();
		System.out.println("Abriendo modal de confirmación");
	    context.execute("eliminarCriterios.show()");	    
	}
	System.out.println("Saliendo del IF");
	return "SeleSitio";
}


public String tieneSustanciasSeleccionadas(){ 
	System.out.println("Comprobando Chechbox´s");
	System.out.println(sustanciasListSelecciones.size()+" seleccionados");
	if(!sustanciasListSelecciones.isEmpty()){
		RequestContext context = RequestContext.getCurrentInstance();
		System.out.println("Abriendo modal de confirmación");
	    context.execute("eliminarSustancias.show()");	    
	}	
	return "SeleSitio";
}


    public String getPorFederal() {
        double e = (proyecto.getProyInversionEstatalOri() != null) ? proyecto.getProyInversionEstatalOri().doubleValue() : 0.0;
        double f = (proyecto.getProyInversionFederalOri() != null) ? proyecto.getProyInversionFederalOri().doubleValue() : 0.0;
        double m = (proyecto.getProyInversionMunicipalOri() != null) ? proyecto.getProyInversionMunicipalOri().doubleValue() : 0.0;
        double p = (proyecto.getProyInversionPrivadaOri() != null) ? proyecto.getProyInversionPrivadaOri().doubleValue() : 0.0;
        double t = e + f + m + p;

        if (f == 0) {
            return "0";
        }

        double out = (f / t) * 100;
        return ("" + df.format(out));
    }

    public String getPorEstatal() {
        double e = (proyecto.getProyInversionEstatalOri() != null) ? proyecto.getProyInversionEstatalOri().doubleValue() : 0.0;
        double f = (proyecto.getProyInversionFederalOri() != null) ? proyecto.getProyInversionFederalOri().doubleValue() : 0.0;
        double m = (proyecto.getProyInversionMunicipalOri() != null) ? proyecto.getProyInversionMunicipalOri().doubleValue() : 0.0;
        double p = (proyecto.getProyInversionPrivadaOri() != null) ? proyecto.getProyInversionPrivadaOri().doubleValue() : 0.0;
        double t = e + f + m + p;

        if (e == 0) {
            return "0";
        }
        double out = ((e / t) * 100);
        return ("" + df.format(out));
    }

    public String getPorMunicipal() {
        double e = (proyecto.getProyInversionEstatalOri() != null) ? proyecto.getProyInversionEstatalOri().doubleValue() : 0.0;
        double f = (proyecto.getProyInversionFederalOri() != null) ? proyecto.getProyInversionFederalOri().doubleValue() : 0.0;
        double m = (proyecto.getProyInversionMunicipalOri() != null) ? proyecto.getProyInversionMunicipalOri().doubleValue() : 0.0;
        double p = (proyecto.getProyInversionPrivadaOri() != null) ? proyecto.getProyInversionPrivadaOri().doubleValue() : 0.0;
        double t = e + f + m + p;

        if (m == 0) {
            return "0";
        }
        double out = (m / t) * 100;
        return ("" + df.format(out));
    }

    public String getPorPrivada() {
        double e = (proyecto.getProyInversionEstatalOri() != null) ? proyecto.getProyInversionEstatalOri().doubleValue() : 0.0;
        double f = (proyecto.getProyInversionFederalOri() != null) ? proyecto.getProyInversionFederalOri().doubleValue() : 0.0;
        double m = (proyecto.getProyInversionMunicipalOri() != null) ? proyecto.getProyInversionMunicipalOri().doubleValue() : 0.0;
        double p = (proyecto.getProyInversionPrivadaOri() != null) ? proyecto.getProyInversionPrivadaOri().doubleValue() : 0.0;
        double t = e + f + m + p;
        if (p == 0) {
            return "0";
        }
        double out = (p / t) * 100;
        return ("" + df.format(out));
    }

    public String getPorTot() {
        double e = (proyecto.getProyInversionEstatalOri() != null) ? proyecto.getProyInversionEstatalOri().doubleValue() : 0.0;
        double f = (proyecto.getProyInversionFederalOri() != null) ? proyecto.getProyInversionFederalOri().doubleValue() : 0.0;
        double m = (proyecto.getProyInversionMunicipalOri() != null) ? proyecto.getProyInversionMunicipalOri().doubleValue() : 0.0;
        double p = (proyecto.getProyInversionPrivadaOri() != null) ? proyecto.getProyInversionPrivadaOri().doubleValue() : 0.0;
        double t = e + f + m + p;
        if (t == 0) {
            return ("0");

        }
        return ("100");
    }

    /**
     * Listener seleccion demandara servicios
     */
    public void selServicios() {
        if (proyecto.getProyServreq() != null) {
            if (proyecto.getProyServreq().equals('S')) {
                capServicios = true;
            }
            if (proyecto.getProyServreq().equals('N')) {
                capServicios = false;
            }
        }
    }

    /**
     * Listener seleccion demandara explosivos
     */
    public void selExplosivos() {
        if (proyecto.getProyExplosivosAplica() != null) {
            if (proyecto.getProyExplosivosAplica().equals("S")) {
                capExplosivos = true;
            }
            if (proyecto.getProyExplosivosAplica().equals("N")) {
                capExplosivos = false;
            }
        }
    }

    /**
     * Listener seleccion demandará sustancias
     */
    public void selSustancias() {
        if (proyecto.getProySustanciaAplica() != null) {
            if (proyecto.getProySustanciaAplica().equals("S")) {
                capSustancias = true;                
            }
            if (proyecto.getProySustanciaAplica().equals("N")) {
                capSustancias = false;               
            }
        }
    }

    public void obtieneServicioById(){
    	System.out.println("Iniciando edicion de servicio!");
    	@SuppressWarnings("rawtypes")
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		short id = 0;
		String idProy = params.get("idProy").toString();
		id = Short.parseShort(idProy);
		System.out.println("Editando servicio con id: "+id);
		idSeleccionadoServicio = id;
		editarServicio=new ServicioProyecto();
		
		// Numero secuencial a mostrar en "Editar registro #"
		rowIndexServReqEditar = Integer.parseInt(params.get("prowindexSR").toString());
		
		editarServicio=miaDao.getServicioByFolio(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
	}
    
    /**
     * Metodo para agregar un nuevo servicio
     */
	public void agregarServicio() {
        if (servicios == null) {
            servicios = new ArrayList<ServicioProyecto>();
        }

        Short id = 0;

        try {
            id = miaDao.getMaxServicio(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            if (id != 0) {
            	id = (short) (id.intValue() + 1);
			} else {
				id = 1;
			}
            
        } catch (Exception e) {
            id = 1;
            e.printStackTrace();
        }
        
        ServicioProyecto sp = new ServicioProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        
        sp.setServicioId(new CatServicio(nuevoServicio.getServicioId().getServicioId()));
        sp.setServicioNombre(		nuevoServicio.getServicioId().getServicioNombre());   
        sp.setServicioDisponible(	nuevoServicio.getServicioDisponible());
        sp.setServicioSuministrado(	nuevoServicio.getServicioSuministrado());
        sp.setServicioDescripcion(	nuevoServicio.getServicioDescripcion());
        sp.setEtapaId(				nuevoServicio.getCatEtapa().getEtapaId());
        sp.setCatEtapa(nuevoServicio.getCatEtapa());
        
                     
        try {
           miaDao.persist(sp);
           servicios = miaDao.getServicios(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
//           servicios.add(sp);
           nuevoServicio= new ServicioProyecto();
           RequestContext context = RequestContext.getCurrentInstance();
			context.execute("dlgAgregarServicio.hide(),dlgAvisoServicioAgregado.show()");
			
		RequestContext.getCurrentInstance().reset(":AgregarServicio:pnlAgregar");
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    /**
     * Metodo para editar un servicio
     */
    public void editarServicio(){
    	ServicioProyecto sp= new ServicioProyecto();
    	sp = editarServicio;
    	sp.setServicioNombre(editarServicio.getServicioId().getServicioNombre());
    	
        try {
            miaDao.persist(sp);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditar.hide();modalMensajeEditar.show()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Eliminar Servicio
     *
     * 
     * @param idt
     */
    
public void eliminarServicio(Short idt) {
        
        ServicioProyecto cp = new ServicioProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt);
//        servicios.remove(cp);
        servicios = miaDao.getServicios(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        try {
            miaDao.eliminar(cp);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * @return
     * @throws Exception
     */
    public void eliminaServicios() throws Exception {
        for (ServicioProyecto servicioSeleccionado : serv_seleccionados) {           
            miaDao.eliminarServicioByFolio(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),servicioSeleccionado.getServicioProyectoPK().getServicioProyId());
        }
        servicios = miaDao.getServicios(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        
        if (servicios.size() == 0) {
			servicios = new ArrayList<ServicioProyecto>();
			serv_seleccionados = new ArrayList<ServicioProyecto>();
		}        
    }
    
    public void tieneServiciosSeleccionados(){
    	//System.out.println("..............................Consultando si existe servicios seleccionados para borrar");
    	if(!serv_seleccionados.isEmpty()){
    		RequestContext context = RequestContext.getCurrentInstance();
    	    context.execute("dlgEliminarServicios.show()");
    	}
    }
    
    /**
     * Genera un nuevo objeto de Uso de Suelo para realizar un alta
     */
    public void crearUsoSuelo() {
    	System.out.println("crearUsoSuelo");
    	predio = new PrediocolinProy();
    	RequestContext.getCurrentInstance().reset("Agregar:pnlAgregar");		
		
    }
    
    /**
     * Metodo para consultar un registro de Uso de Suelo
     */
	public void consultarUsoSuelo() {
    	@SuppressWarnings("rawtypes")
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idPredio = params.get("id").toString();
		id = Integer.parseInt(idPredio);
		System.out.println("Registro de Suelo: "+id);
		editPredio = new PrediocolinProy();
		editPredio = miaDao.getPrediocolinProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) id);
		catClasificacionB = new ArrayList<SelectItem>();
        
		List<CatClasificacionB> n = miaDao.getCatClasificacionBById(editPredio.getClasificacionId().getClasificacionId());
        for (CatClasificacionB c : n) {
            SelectItem s = new SelectItem(c, c.getClasificacionB());
            catClasificacionB.add(s);
        }
        setCatClasificacionB(catClasificacionB);
            
        // Numero secuencial a mostrar en "Editar registro #"
		rowIndexUSPCEditar = Integer.parseInt(params.get("prowindexUSPC").toString());
    }
    
    /**
     * Metodo para editar un registro de Uso de Suelo
     */
    public void editarUsoSuelo() {
    	PrediocolinProy p = new PrediocolinProy();
    	p = editPredio;
        try {
            miaDao.persist(p);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditar.hide();modalMensajeEditar.show()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
      
    public void eliminarUsoSuelo() {
    	System.out.println( selectedPredios.size());
    	int id = 0;
    	for (PrediocolinProy prediocolinProy : selectedPredios) {
			System.out.println(prediocolinProy.getPrediocolinProyPK().getPredioColinId());
			System.out.println(prediocolinProy.getPredioNombre());
			System.out.println(prediocolinProy.getClasificacionId());
			System.out.println(prediocolinProy.getReferenciaId());
			System.out.println(prediocolinProy.getTemporalidadId());
			System.out.println(prediocolinProy.getPredioDescripcion());
			
			id = prediocolinProy.getPrediocolinProyPK().getPredioColinId();
	        try {
		          miaDao.eliminarPrediocolinProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),(short) id);
		          selectedPredios = new ArrayList<PrediocolinProy>();
		      } catch (Exception e2) {
		          e2.printStackTrace();
		      }
	        predCol = miaDao.getPrediocolinProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		}
    	
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("modalEliminar.hide();modalMensajeEliminar.show()");
//        public void eliminarUsoSuelo(ActionEvent e) {
//        Integer ids = (Integer) e.getComponent().getAttributes().get("idt");
//        Short idt = ids.shortValue();//(Short) e.getComponent().getAttributes().get("idt");
//        PrediocolinProy p = new PrediocolinProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt);
//        predCol.remove(p);
//
//        try {
//            miaDao.eliminarPrediocolinProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt);
//        } catch (Exception e2) {
//            e2.printStackTrace();
//        }
    }
    
    /**
     * Da de alta en base de datos un nuevo registro de Uso de Suelo
     */
    public void agregarUsoSuelo() {
        int id = 0;
//        if (predCol.isEmpty()) {
//            id = 1;
//        } else {
//            for (PrediocolinProy predio : predCol) {
//                if ( predio.getPrediocolinProyPK().getPredioColinId() > id ) {
//                    id = predio.getPrediocolinProyPK().getPredioColinId();
//                }
//            }
//            id++;
//        }
        try {
            id = miaDao.getMaxPrediocolinProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            id = id + 1;
        } catch (Exception e) {
            id = 1;
            //e.printStackTrace();
        }
        PrediocolinProy p = new PrediocolinProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) id);
        p.setPredioNombre(predio.getPredioNombre());
        p.setReferenciaId(predio.getReferenciaId());
        p.setClasificacionId(predio.getClasificacionId());
        p.setTemporalidadId(predio.getTemporalidadId());
        p.setPredioDescripcion(predio.getPredioDescripcion());
        p.setClasificacionBId(predio.getClasificacionBId());
        try {
            miaDao.persist(p);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalAgregar.hide();modalMensaje.show()");
        } catch (Exception e) {
            e.printStackTrace();
        }
        predCol = miaDao.getPrediocolinProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    }

    public void eliminarUsoSueloVegetacion(ActionEvent e) {
        Integer ids = (Integer) e.getComponent().getAttributes().get("idt");
        Short idt = ids.shortValue();//(Short) e.getComponent().getAttributes().get("idt");
        PrediocolinProy p = new PrediocolinProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt);
        predCol.remove(p);

        try {
            miaDao.eliminarPrediocolinProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void agregarUsoSueloVegetacion() {
        Short id = 0;

        try {
            id = miaDao.getMaxPrediocolinProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            id = (short) (id.intValue() + 1);
        } catch (Exception e) {
            id = 0;
            e.printStackTrace();
        }
        
        PrediocolinProy p = new PrediocolinProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        try {
            miaDao.merge(p);
        } catch (Exception e) {
            e.printStackTrace();
        }
        predCol.add(p);
    }

    /**
     * Nuevo Info Bio
     *
     * @param e
     */
//    public void nuevoBio(ActionEvent e) {
//        Short id = 0;
//
//        try {
//            id = miaDao.getMaxInfoBio(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
//        } catch (Exception ex) {
//           ex.printStackTrace();
//        }
//
//        if (id == null) {
//            id = 0;
//        }
//
//        infoBio = new InfobioProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
//        try {
//            miaDao.merge(infoBio);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//    public void nuevoBio(ActionEvent e) {
//        return "";
//    }
    public void newBio(ActionEvent e) {
        Integer temp = (Integer) (e.getComponent().getAttributes().get("temp")) + 1;
        System.out.println("Valor de temp: " + temp);
        Short idt = (Short) temp.shortValue();
        System.out.println("Valor de idt: " + idt);
        infoBio = new InfobioProyecto(new InfobioProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt));
        
        try {
            miaDao.merge(infoBio);
            FacesContext context = FacesContext.getCurrentInstance();
            context.getExternalContext().getSessionMap().put("id_add_bio", idt);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void editarBio(ActionEvent e) {
        Short idt = (Short) e.getComponent().getAttributes().get("idt");
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().put("id_add_bio", idt);

        System.out.println("Editar Info bio " + idt);
//
//        try {
//            infoBio = miaDao.getInfoBio(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt);
//        } catch (Exception e2) {
//            e2.printStackTrace();
//        }
    }

    /**
     * Agregar informacion bio
     */
    public String agregarBio() {
        try {
            miaDao.merge(infoBio);
            FacesContext context = FacesContext.getCurrentInstance();
            context.getExternalContext().getSessionMap().put("id_add_bio", null);
            String msg = "La información de este elemento ha sido guardada con éxito";
            
            //FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "La información de este elemento ha sido guardada con éxito", null);
            //context.addMessage("msgAlerts", message);
            
            //JOptionPane.showMessageDialog(null, "La info está OK" , "MIA", JOptionPane.INFORMATION_MESSAGE);            
            RequestContext reqcontEnv = RequestContext.getCurrentInstance();
            reqcontEnv.execute("mensajes('" + msg + "', 'success')");

        } catch (Exception e) {
            e.printStackTrace();
        }
        //infoBioList.add(infoBio);
        return "descPesqueros";
    }

    public void eliminarBio(ActionEvent e) {
        Short idt = (Short) e.getComponent().getAttributes().get("idt");
        System.out.println("idt " + idt);
        InfobioProyecto ibElemList = null;
        for (InfobioProyecto ib : infoBioList) {
            System.out.println("--idt " + ib.getInfobioProyectoPK().getInfoBioId());

            if (ib.getInfobioProyectoPK().getInfoBioId() == idt) {
                ibElemList = ib;
            }
        }
        if (ibElemList != null) {
            infoBioList.remove(ibElemList);
            try {
                miaDao.eliminarInfoBiotec(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /**
     * Etapas
     */
    public void agregarEtapaPreparacion() {
        CatEtapa ce = new CatEtapa((short) 1);
        Short id = null;

        try {
            id = miaDao.getMaxActividadEtapaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), ce.getEtapaId());
            id++;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (id == null) {
            id = 0;
        }

        ActividadEtapa ae = new ActividadEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1, id);
        try {
            miaDao.merge(ae);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (ActividadEtapa e : actividadesPrepara) {
            System.out.println("" + e);
        }
        System.out.println("" + ae);

        //       actividadesPrepara.add(ae);
    }

    public void verEtapas() {
        try {
            int tp = etapaPreparacion.getAnios() + etapaPreparacion.getMeses() + etapaPreparacion.getSemanas();
            int tc = etapaConstruccion.getAnios() + etapaConstruccion.getMeses() + etapaConstruccion.getSemanas();
            int to = etapaOperacion.getAnios() + etapaOperacion.getMeses() + etapaOperacion.getSemanas();
            int ta = etapaAbandono.getAnios() + etapaAbandono.getMeses() + etapaAbandono.getSemanas();

            System.out.println("tp " + tp);
            System.out.println("tc " + tc);
            System.out.println("to " + to);
            System.out.println("ta " + ta);

            verPreparacion = (tp > 0);
            verConstruccion = (tc > 0);
            verOperacion = (to > 0);
            verAbandono = (ta > 0);
            verMsgEtapa = (!verPreparacion && !verConstruccion && !verOperacion && !verAbandono);
            System.out.println("Existen Etapas: " + verMsgEtapa);
        } catch (Exception e) {
        	verMsgEtapa = true;
        	System.out.println("No se encontraron Etapas: " + verMsgEtapa);
        }
    }

    public void selEtapa() {
        verEtapas();
    }

    public void agregarEtapaConstruccion() {
        CatEtapa ce = new CatEtapa((short) 2);
        Short id = null;

        try {
            id = miaDao.getMaxActividadEtapaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), ce.getEtapaId());
            id++;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (id == null) {
            id = 0;
        }

        ActividadEtapa ae = new ActividadEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 2, id);
        try {
            miaDao.merge(ae);
        } catch (Exception e) {
            e.printStackTrace();
        }
        actividadesConstru.add(ae);
    }

    public void agregarEtapaOperacion() {
        CatEtapa ce = new CatEtapa((short) 3);
        Short id = null;

        try {
            id = miaDao.getMaxActividadEtapaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), ce.getEtapaId());
            id++;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (id == null) {
            id = 0;
        }

        ActividadEtapa ae = new ActividadEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 3, id);
        try {
            miaDao.merge(ae);
        } catch (Exception e) {
            e.printStackTrace();
        }
        actividadesOperaci.add(ae);
    }

    public void agregarEtapaAbandono() {
        CatEtapa ce = new CatEtapa((short) 4);
        Short id = null;

        try {
            id = miaDao.getMaxActividadEtapaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), ce.getEtapaId());
            id++;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (id == null) {
            id = 0;
        }

        ActividadEtapa ae = new ActividadEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 4, id);
        try {
            miaDao.merge(ae);
        } catch (Exception e) {
            e.printStackTrace();
        }
        actividadesAbandon.add(ae);
    }

    //<editor-fold desc="Agrega fila a la tabla dinámica de sustancias" defaultstate="collapsed">
    public void agregarSustancia() {
        System.out.println("Sustancia ");
        Short id = null;

        try {
            for (SustanciaU p : sustanciasList) {
                miaDao.merge(p.model);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            id = miaDao.getMaxSustancia(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            id++;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (id == null) {
            id = 1;
        }

        SustanciaProyecto sp = new SustanciaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        sp.setSustanciaId(new CatSustanciaAltamRiesgosa((short) 1));
        sp.setEtapaId(selectedEtapa);
        sp.setSustanciaId(sustanciariesgosaId);
        sp.setSustanciaCantidadAlmacenada(sustanciaCantidadAlmacenada);  
        sp.setCtunClve(ctunClve);
        sp.setSustanciaPromovente(sustanciaPromovente);
        sp.setExcedeLimite(sustanciaexcede);
        
        try {
            miaDao.merge(sp);
            sustanciasList.add(new SustanciaU(sp, false, 0, getUnidad(Short.parseShort(sp.getSustanciaId().getSustanciaUnidad()))));
            
            // refrescando todo los objetos de la modal de agregar...
            selectedEtapa = null;
            sustanciariesgosaId = null;
            sustanciaCantidadAlmacenada = BigDecimal.ZERO;
            ctunClve = 0;
            ctunDesc = "";
            sustanciaPromovente = "";
                        
            RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalAgregar.hide();modalAlert.show()");
			
        } catch (Exception e) {
            e.printStackTrace();
        }
        
      RequestContext context = RequestContext.getCurrentInstance();
      if (sustanciaexcede.equalsIgnoreCase("1")) {
    	  
    	// muestra el div de que alguna sustancia excede
    		context.execute("mal1();");  
    	
    		// muestra el menu 'Estudio de Riesgo'
  	    	context.execute("parent.verER();");
  	    	
         proyecto.setEstudioRiesgoId((short) 1);

      } else {
   
    	// valida si quita o no el menu superior de 'Estudio de Riesgo' porque no sabemos si hay mas sustancias que exceden para este proyecto...  	
    	  	if (!verificaCantidad()) {    	  		
    	      	
    	      	// oculta el div de que alguna sustancia excede
    	  		context.execute("mal2();");  
    	  	
    	  		// oculta el menu 'Estudio de Riesgo'
    		    	context.execute("parent.verER2();");

    	          proyecto.setEstudioRiesgoId((short) 0);
    	      }
      }
      
      try {
		miaDao.merge(proyecto);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
      
        
    }//</editor-fold>

    public void eliminarSustancia(ActionEvent e) {
        Short idt = (Short) e.getComponent().getAttributes().get("idt");
        System.out.println("idt " + idt);
        SustanciaU pb = null;
        for (SustanciaU p : sustanciasList) {
            System.out.println("--idt " + p.model.getSustanciaProyectoPK().getSustProyId());

            if (p.model.getSustanciaProyectoPK().getSustProyId() == idt) {
                pb = p;
            }
        }
        if (pb != null) {
            sustanciasList.remove(pb);
            try {
                
                miaDao.eliminarSustancia(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt);
                //Eliminar adjunto
                
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
    
    
    public void eliminarSustancias() throws Exception {
    	System.out.println("Eliminar sustancias de la bd...");
        for (SustanciaU sustancia_: sustanciasListSelecciones) {
        	//Eliminacion de Archivos en FileSystem
			if (sustancia_.getModel().getIdArchivoProyecto() != null) {
				int id = sustancia_.getModel().getIdArchivoProyecto();
				short idAnexo = (short) id;
				ArchivoBean.eliminarArchivoProyecto(idAnexo);
			}
        	sustanciasList.remove(sustancia_);
            miaDao.eliminarSustancia(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), sustancia_.getModel().getSustanciaProyectoPK().getSustProyId());   
        }
        
        sustanciaexcede = "0";
        for (SustanciaU s : sustanciasList) {
    		
    		if(s.model.getExcedeLimite().equalsIgnoreCase("1"))
    			sustanciaexcede = "1";        		
        }
        
        short est = proyecto.getEstudioRiesgoId();
                
        RequestContext context2 = RequestContext.getCurrentInstance();
	    if (sustanciaexcede.equalsIgnoreCase("0")) {
	    	
	    	// oculta el div de que alguna sustancia excede
    		context2.execute("mal2();");  
    	
    		// oculta el menu 'Estudio de Riesgo'
	    	context2.execute("parent.verER2();");

            proyecto.setEstudioRiesgoId((short) 0);
            
            if(est == 1){
           	 // para guardar el estudioriesgoid
           	 	miaDao.merge(proyecto);
            }
	    }
    }
    

    public void eliminarContaminante(ActionEvent event) {
        Short idt = (Short) event.getComponent().getAttributes().get("idt");
        Contaminanteu contaminante = null;
        
        for(Contaminanteu ele : contaminanteuList) {
            if ( ele.getContaminante().getContaminanteProyectoPK().getContaminanteId() == idt) {
                contaminante = ele;
                break;
            }
        }
        
        if (contaminante != null) {
            contaminanteuList.remove(contaminante);

            try {
                miaDao.elimina(ContaminanteProyecto.class, contaminante.getContaminante().getContaminanteProyectoPK());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public void agregarContaminante() {
        System.out.println("Contaminante ");
        Short id = null;

        try {
            id = miaDao.getMaxContaminante(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            id++;
        } catch (Exception e) {
            //e.printStackTrace();
        }
        if (id == null) {
            id = 1;
        }
        System.out.println("id " + id);
        Contaminanteu cu = new Contaminanteu();
        ContaminanteProyecto cp = new ContaminanteProyecto(
                new ContaminanteProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id));
        
        //cp.setCatContaminante(new CatContaminante((short)3));
        
        /*
        cp.setContaminanteEmisiones((short)3);
        cp.setContaminanteTipo(new Short((short) 1));
        cp.setEtapaId((short)1);
        */
        cu.setContaminante(cp);
//            ContaminanteProyecto c4 = new ContaminanteProyecto(folioProyecto, serialProyecto, etapaAbandono.getEtapaProyectoPK().getEtapaId(), (short) 1);

        try {
            //miaDao.merge(etapaAbandono);
            miaDao.merge(cp);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        contaminanteList.add(cp);
        contaminanteuList.add(cu);
    }

    /**
     * Metodo que crea un nuevo objeto de Accidente
     */
    public void crearAccidente(){
    	accidente = new AccidentesProyecto();
    }
    /**
     * Metodo que Guarda un registro de Accidente
     */
    public void agregarAccidente() {
        System.out.println("Accidente ");
        Short id = 0;

        try {
            id = miaDao.getMaxAccidente(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            id++;
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        AccidentesProyecto ap = new AccidentesProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        ap.setTipoAccidente(accidente.getTipoAccidente());
        ap.setEfectosSecundarios(accidente.getEfectosSecundarios());
        
        try {
            miaDao.merge(ap);
        	accidenteList = miaDao.getAccidente(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalAgregar.hide();modalMensaje.show()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Metodo para consultar un registro de Accidente
     */
    public void consultarAccidente() {
    	@SuppressWarnings("rawtypes")
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idAccidente = params.get("id").toString();
		id = Integer.parseInt(idAccidente);
		System.out.println("Registro de Accidente: "+id);
		editAccidente= new AccidentesProyecto();
		editAccidente = miaDao.getAccidente(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) id);
		
		// Numero secuencial a mostrar en "Editar registro #"
		rowIndexPosAccAmbEditar = Integer.parseInt(params.get("rowIndexPosAcc").toString());
    }
    
    /**
     * Metodo para editar un registro de Accidente
     */
    public void editarAccidente() {
    	AccidentesProyecto accidenteAmbiental = new AccidentesProyecto();
    	accidenteAmbiental = editAccidente;
        try {
            miaDao.persist(accidenteAmbiental);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditar.hide();modalMensajeEditar.show()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //<editor-fold desc="Eliminar registro de tabla dinamica" defaultstate="collapsed">
    /**
     * Metodo para borrar registros de Accidentes
     */
    public void eliminarAccidente() {
    	int id = 0;
    	for (AccidentesProyecto accidentesProyecto : selectedAccidentes) {
			
			id = accidentesProyecto.getAccidentesProyectoPK().getAccidenteId();
	        try {
		          miaDao.eliminarAccidenteProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),(short) id);
		      	  accidenteList = miaDao.getAccidente(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		      } catch (Exception e2) {
		          e2.printStackTrace();
		      }
		}
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("modalEliminar.hide();modalMensajeEliminar.show()");
    }//</editor-fold>

    //<editor-fold desc="Explosivos" defaultstate="collapsed">
    /**
     * Etapas
     */
    public void agregarExplosivo() {
        CatEtapa ce = new CatEtapa((short) 1);
        Short id = null;

        try {
            id = miaDao.getMaxActividadEtapaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), ce.getEtapaId());
            id++;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (id == null) {
            id = 0;
        }

        ActividadEtapa ae = new ActividadEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1, id);
        try {
            miaDao.merge(ae);
        } catch (Exception e) {
            e.printStackTrace();
        }
        explosivosList.add(ae);
//        actividadesPrepara.add(ae);
    }//</editor-fold>
    
    public void eliminarExplosivos(ActionEvent e) {
        Short idt = (Short) e.getComponent().getAttributes().get("idt");
        System.out.println("explosivos idt " + idt);
        ActividadEtapa pb = null;
        for (ActividadEtapa p : explosivosList) {
            if (p.getActividadEtapaPK().getActividadEtapaId() == idt) {
                pb = p;
            }
        }
        if (pb != null) {
            explosivosList.remove(pb);
            try {
                
                miaDao.eliminarActividadEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), 
                        pb.getActividadEtapaPK().getEtapaId(), idt);
                
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

	public void guardarSustancias() {
        System.out.println("Guardar Sustancias");
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();

        System.out.println("Sustancias: " + capSustancias);
        if(capSustancias)
        {
            if (verificaCantidad()) {
                System.out.println("Humbral >");

                reqcontEnv.execute("parent.verER();");
                proyecto.setEstudioRiesgoId((short) 1);
            } else {
                proyecto.setEstudioRiesgoId((short) 0);
            }
        }
        else
        { proyecto.setEstudioRiesgoId((short) 0); }
        
        //Se realiza el guardado de la ponderación para el apartado de sustancias
        Short vValorPonderacion = 0;
        if (claveTramite == 66 || claveTramite == 67){ //Si es una mia regional sólo guarda 1 porque tiene más apartados
            vValorPonderacion = 2;
        }else if (proyecto.getNsub()==2){ //Si es una mia particular y pertenece al subsector forestal le corresponde 3
                vValorPonderacion = 3;
        }else if (proyecto.getNsub()==7){ //Si es una mia particular y pertenece al subsector comunicaciones le corresponde 1
                vValorPonderacion = 1;
        }else{//Si es particular y no pertenece a comunicaciones ni forestal sólo se otorgan 2
                vValorPonderacion = 2;
        }
        
        try {
            miaDao.merge(proyecto);
            try {
                miaDao.guardarAvance(proyecto, vValorPonderacion, "214");
            } catch (Exception e) {
                System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
            }
        } catch (Exception e) {
        }
        
        //------------INICIO actualiza avance de capitulos 5,6,7 y 8 dependiendo si tiene o no estudios de riesgo---------
        //---------------------guardado de avance de capitulo 5-------------------------------
            List<ImpacAmbProyecto> evaluacion = new ArrayList<ImpacAmbProyecto>();
            evaluacion = miaDao.getImpacAmbProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            
            if (proyecto.getDescMetutil() != null) {
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if (proyecto.getEstudioRiesgoId() == 1){
                    try {
                        miaDao.guardarAvance(proyecto, new Short("3"), "51");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("6"), "51");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }                    
                }
                //</editor-fold>
            }
                        
            if (evaluacion.size() >= 1) {
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if (proyecto.getEstudioRiesgoId() == 1){
                    try {
                        miaDao.guardarAvance(proyecto, new Short("3"), "52");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("5"), "52");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
                    
            
        //---------------------guardado de avance de capitulo 6-------------------------------
            List<MedPrevImpactProy> medPreventivas = new ArrayList<MedPrevImpactProy>();
            medPreventivas = miaDao.getMedPrevImpactProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                        
            if(medPreventivas.size() >= 1){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId()==1)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("6"), "6");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else{
                    try {
                        miaDao.guardarAvance(proyecto, new Short("11"), "6");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            
        //---------------------guardado de avance de capitulo 7-------------------------------
            if(proyecto.getEsceConProy()!=null && proyecto.getEsceSinProy()!=null
                    && proyecto.getEsceConProyMed()!=null && proyecto.getPronosAmb()!=null)
            {
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId() == 1)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("3"), "71");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                } else {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("6"), "71");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            if (proyecto.getConclusionProy() != null && proyecto.getEvaAlternativas() != null) 
            {
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                 if(proyecto.getEstudioRiesgoId() == 1)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("3"), "72");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                } else {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("5"), "72");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                 }
                //</editor-fold>
            }
        //---------------------guardado de avance de capitulo 8-------------------------------
            List<GlosarioProyecto> glosario = new ArrayList<GlosarioProyecto>();
            glosario = miaDao.getGlosario(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            
            if (proyecto.getManifestacionRes() != null) {
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId() == 1){
                    try {
                        miaDao.guardarAvance(proyecto, new Short("3"), "81");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else{
                    try {
                        miaDao.guardarAvance(proyecto, new Short("8"), "81");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            if (glosario.size() >= 1){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId() == 1){
                    try {
                        miaDao.guardarAvance(proyecto, new Short("1"), "82");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else{
                    try {
                        miaDao.guardarAvance(proyecto, new Short("2"), "82");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            if (proyecto.getBiblioProy() != null){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId() == 1){
                    try {
                        miaDao.guardarAvance(proyecto, new Short("1"), "83");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else{
                    try {
                        miaDao.guardarAvance(proyecto, new Short("2"), "83");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            } 
        //---------------------guardado de avance de capitulo ERA-------------------------------
            List<AvanceProyecto> avanceCapERA = new ArrayList<AvanceProyecto>();
            avanceCapERA = miaDao.getAvanceCapERA(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),(short) 101);                        
            if(avanceCapERA.size() >= 1){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId()==0)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("0"), "101");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            avanceCapERA = miaDao.getAvanceCapERA(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),(short) 102);                        
            if(avanceCapERA.size() >= 1){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId()==0)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("0"), "102");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            avanceCapERA = miaDao.getAvanceCapERA(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),(short) 103);                        
            if(avanceCapERA.size() >= 1){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId()==0)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("0"), "103");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            avanceCapERA = miaDao.getAvanceCapERA(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),(short) 104);                        
            if(avanceCapERA.size() >= 1){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId()==0)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("0"), "104");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            avanceCapERA = miaDao.getAvanceCapERA(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),(short) 105);                        
            if(avanceCapERA.size() >= 1){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId()==0)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("0"), "105");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
        //------------FIN actualiza avance de capitulos 5,6,7 y 8 dependiendo si tiene o no estudios de riesgo---------

        try {
            Validacion v = new Validacion();
            String msg = v.validar(seccion, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            if (!msg.isEmpty()) {
                return;
            }
        } catch (Exception err) {
            err.printStackTrace();
        }

        try {
            for (SustanciaU s : sustanciasList) {
                miaDao.merge(s.model);
            }
            if (proyecto.getProySustanciaAplica().equals("S")) {
                if (sustanciasList.size() >= 1) {
                    //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                    try {
                        if (proyecto.getNsub().equals(new Short("2"))) {
                            miaDao.guardarAvance(proyecto, new Short("3"), "214");
                        } else {
                            if (proyecto.getNsub().equals(new Short("7"))) {
                                miaDao.guardarAvance(proyecto, new Short("1"), "214");
                            } else {
                                miaDao.guardarAvance(proyecto, new Short("2"), "214");
                            }
                        }
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                    //</editor-fold>
                }
            } else if (proyecto.getProySustanciaAplica().equals("N")) {
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                try {
                    if (proyecto.getNsub().equals(new Short("2"))) {
                        miaDao.guardarAvance(proyecto, new Short("3"), "214");
                    } else {
                        if (proyecto.getNsub().equals(new Short("7"))) {
                            miaDao.guardarAvance(proyecto, new Short("1"), "214");
                        } else {
                            miaDao.guardarAvance(proyecto, new Short("2"), "214");
                        }
                    }
                } catch (Exception e) {
                    System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                }
                //</editor-fold>
            }

            reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success');");
            if(capSustancias)
            {
                if (supera) {
                    reqcontEnv.execute("mal1();");
                } else {
                    reqcontEnv.execute("mal2();");
                }
            }
            else
            {
                reqcontEnv.execute("mal2();");
            }
        } catch (Exception e) {
            Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e.getMessage());
            reqcontEnv.execute("mensajes('No se ha podido guardar la información, intente mas tarde.', 'danger')");
            e.printStackTrace();
        }
    }

    //<editor-fold desc="Guarda proyecto" defaultstate="collapsed">
    /**
     * Guardar
     */
    public void guardarProyecto() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();

        try {
            try {
                FacesContext context = FacesContext.getCurrentInstance();
                Object o = context.getExternalContext().getSessionMap().get("clvMunici");

                if (o != null) {
                    String t = (String) o;
                    if (!t.isEmpty()) {
                        String mun = t;
                        String ent = t.substring(0, 2);
                        System.out.println("Entidad " + ent);
                        System.out.println("municipio " + mun);
                        proyecto.setProyEntAfectado(ent);
                        proyecto.setProyMunAfectado(mun);
                    }
                }
            } catch (Exception e3) {
                Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e3.getMessage());
            }
            //guardado de estado y entidad más afectados provisional

//            if (proyecto.getProyEntAfectado() == null) {
//                proyecto.setProyEntAfectado("09");
//            }
//            if (proyecto.getProyMunAfectado() == null) {
//                proyecto.setProyMunAfectado("09005");
//            }//fin de cambio provisional
//            if (proyecto.getProyDescNat().length() > 3500) {
//                reqcontEnv.execute("mensajes('No es posible guardar la información, su captura contiene " + proyecto.getProyDescNat().length() + " caracteres, de 3500 permitidos, tome en cuenta que el formato tambien es considerado.', 'danger')");
//            } else {
                miaDao.merge(proyecto);

                Short vValorPonderacion = 0; //Se utiliza para dar el valor al apartado, dependiendo del sector y el tipo de trámite
                
                //<editor-fold desc="Guardado Apartado 2.1 Naturaleza del proyecto" defaultstate="collapsed">
                if (proyecto.getProyDescNat() != null) {
                    //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                    try {
                        System.out.println("[Exito] Error al cargar avance: ");
                        miaDao.guardarAvance(proyecto, new Short("1"), "21");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                    //</editor-fold>
                }
                //</editor-fold>

                //<editor-fold desc="Guardado Apartado 2.2 Selección del Sitio" defaultstate="collapsed">
                try {
//                    for (CriteriosProyecto c : criterios) {
//                        miaDao.merge(c);
//                    }
                    if (criterios.size() >= 1) {//todos
                        //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                        try {
                            miaDao.guardarAvance(proyecto, new Short("1"), "22");
                        } catch (Exception e) {
                            System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                        }
                        //</editor-fold>
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //</editor-fold>

                //<editor-fold desc="Guardado Apartado 2.3 ubicación física del proyecto" defaultstate="collapsed">
                if (proyecto.getProyDomEstablecido() != null) {
                    //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                    try {
                        miaDao.guardarAvance(proyecto, new Short("2"), "23");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                    //</editor-fold>
                }
                //</editor-fold>                        

                //<editor-fold desc="Guardado Apartado 2.4 Inversión Requerida" defaultstate="collapsed">
                if (proyecto.getNsub() != null) {

                    // Solo si la página web actual es "Inversión y empleos" se puede guardar el campo Monto por Etapas
					if (!this.proyInversionPE && pageInversionReq.equals("S")) {
						// Actualiza valor indicando que no se cuenta con la Inversion por etapas
                		proyecto.setProyMontoEtapasAplica("N");
            		}
					
					if (this.proyInversionPE) {
						// Usuario ha seleccionado que cuenta con datos para Inversion por etapas
                		proyecto.setProyMontoEtapasAplica("S");
                        
                		InversionEtapas ie = new InversionEtapas(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1);		
                		ie = inversionEtapas;
                		InversionEtapasPK iepk = new InversionEtapasPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1);
                		ie.setInversionEtapasPK(iepk);
                        try {
                            miaDao.persist(ie);
                            //Si los valores que el usuario ingreso son nulos, se igualan a 0
                            if (ie.getInversionPreparacion() == null) {
								ie.setInversionPreparacion(new BigDecimal("0"));
							}
                            if (ie.getInversionConstruccion() == null) {
								ie.setInversionConstruccion(new BigDecimal("0"));
							}
                            if (ie.getInversionOperacion() == null) {
								ie.setInversionOperacion(new BigDecimal("0"));
							}
                            if (ie.getInversionAbandono() == null) {
								ie.setInversionAbandono(new BigDecimal("0"));
							}
                            //******//
                            if (ie.getEmpleosTemporalesPrep() == null) {
                            	ie.setEmpleosTemporalesPrep(new BigDecimal("0"));
							}
                            if (ie.getEmpleosTemporalesCons() == null) {
                            	ie.setEmpleosTemporalesCons(new BigDecimal("0"));
							}
                            if (ie.getEmpleosTemporalesOper() == null) {
                            	ie.setEmpleosTemporalesOper(new BigDecimal("0"));
							}
                            if (ie.getEmpleosTemporalesAban() == null) {
                            	ie.setEmpleosTemporalesAban(new BigDecimal("0"));
							}
                            //******//
                            if (ie.getEmpleosPermanentesPrep() == null) {
                            	ie.setEmpleosPermanentesPrep(new BigDecimal("0"));
							}
                            if (ie.getEmpleosPermanentesCons() == null) {
                            	ie.setEmpleosPermanentesCons(new BigDecimal("0"));
							}
                            if (ie.getEmpleosPermanentesOper() == null) {
                            	ie.setEmpleosPermanentesOper(new BigDecimal("0"));
							}
                            if (ie.getEmpleosPermanentesAban() == null) {
                            	ie.setEmpleosPermanentesAban(new BigDecimal("0"));
							}
                            //******//
                            if (ie.getMedidasMitigacion() == null) {
								ie.setMedidasMitigacion(new BigDecimal("0"));
							}
                            
                            // If para guardar los datos de etapas en PROYECTO 
							BigDecimal invPrep, invCons, invOper, invAban;
							if (ie.getInversionPreparacion() != null) {
								invPrep = ie.getInversionPreparacion();
							} else {
								invPrep = new BigDecimal("0");
							}
							if (ie.getInversionConstruccion() != null) {
								invCons = ie.getInversionConstruccion();
							} else {
								invCons = new BigDecimal("0");
							}
							if (ie.getInversionOperacion() != null) {
								invOper = ie.getInversionOperacion();
							} else {
								invOper = new BigDecimal("0");
							}
							if (ie.getInversionAbandono() != null) {
								invAban = ie.getInversionAbandono();
							} else {
								invAban = new BigDecimal("0");
							}

							Integer empTempPrep, empTempCons, empTempOper, empTempAban;
							if (ie.getEmpleosTemporalesPrep() != null) {
								empTempPrep = ie.getEmpleosTemporalesPrep().intValue();
							} else {
								empTempPrep = 0;
							}
							if (ie.getEmpleosTemporalesCons() != null) {
								empTempCons = ie.getEmpleosTemporalesCons().intValue();
							} else {
								empTempCons = 0;
							}
							if (ie.getEmpleosTemporalesOper() != null) {
								empTempOper = ie.getEmpleosTemporalesOper().intValue();
							} else {
								empTempOper = 0;
							}
							if (ie.getEmpleosTemporalesAban() != null) {
								empTempAban = ie.getEmpleosTemporalesAban().intValue();
							} else {
								empTempAban = 0;
							}

							Integer empPermPrep, empPermCons, empPermOper, empPermAban;
							if (ie.getEmpleosPermanentesPrep() != null) {
								empPermPrep = ie.getEmpleosPermanentesPrep().intValue();
							} else {
								empPermPrep = 0;
							}
							if (ie.getEmpleosPermanentesCons() != null) {
								empPermCons = ie.getEmpleosPermanentesCons().intValue();
							} else {
								empPermCons = 0;
							}
							if (ie.getEmpleosPermanentesOper() != null) {
								empPermOper = ie.getEmpleosPermanentesOper().intValue();
							} else {
								empPermOper = 0;
							}
							if (ie.getEmpleosPermanentesAban() != null) {
								empPermAban = ie.getEmpleosPermanentesAban().intValue();
							} else {
								empPermAban = 0;
							}

							proyecto.setProyInversionRequerida(invPrep.add(invCons).add(invOper).add(invAban));
							if (ie.getMedidasMitigacion() != null) {
								proyecto.setProyMedidasPrevencion(ie.getMedidasMitigacion());
							}
							proyecto.setProyEmpleosTemporales(empTempPrep + empTempCons + empTempOper + empTempAban);
							proyecto.setProyEmpleosPermanentes(empPermPrep + empPermCons + empPermOper + empPermAban);
							
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
					
					if (//proyecto.getNsub().equals(new Short("7"))
                        proyecto.getProyInversionRequerida() != null
                        && proyecto.getProyMedidasPrevencion() != null
                        && proyecto.getProyEmpleosPermanentes() != null
                        && proyecto.getProyEmpleosTemporales() != null) {
                        //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                        try {
                            miaDao.guardarAvance(proyecto, new Short("1"), "24");
                        } catch (Exception e) {
                            System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                        }
                        //</editor-fold>
                    }
                }
                //</editor-fold>

                //<editor-fold desc="Guardado Apartado 2.5 Dimensiones del Proyecto (No tiene guardado, sólo es consulta)" defaultstate="collapsed">
                //</editor-fold>
                //<editor-fold desc="Guardado Apartado 2.6 Servicios requeridos del proyecto" defaultstate="collapsed">
                if (proyecto.getNsub()==7){ //Si es una mia particular y pertenece al subsector comunicaciones le corresponde 3
                        vValorPonderacion = 3;
                }else{//Si es particular y no pertenece a comunicaciones sólo se otorgan 2
                        vValorPonderacion = 2;
                }
                
                try {
                    for (ServicioProyecto s : servicios) {
                        s.setEtapaId(s.getCatEtapa().getEtapaId());
                        miaDao.merge(s);
                    }
                    if (proyecto.getProyServreq() != null && proyecto.getProyServreq().equals('S')) {
                        if (servicios.size() >= 1) {//todos
                            //<editor-fold desc="guardado de avance local y SINATEC (Dimensiones del Proyecto y Servicios Requeridos)" defaultstate="collapsed">
                            try {
                                miaDao.guardarAvance(proyecto, vValorPonderacion, "26");
                            } catch (Exception e) {
                                System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                            }
                            //</editor-fold>
                        }                        
                    } else if (proyecto.getProyServreq() != null && proyecto.getProyServreq().equals('N')) {
                        //<editor-fold desc="guardado de avance local y SINATEC (Dimensiones del Proyecto y Servicios Requeridos)" defaultstate="collapsed">
                        try {
                            miaDao.guardarAvance(proyecto, vValorPonderacion, "26");
                        } catch (Exception e) {
                            System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                        }
                        //</editor-fold>
                    }                     
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //</editor-fold>

                //<editor-fold desc="Guardado Apartado 2.7 Características Particulares del Proyecto, el guardado se hace en Capitulo2_2view.java" defaultstate="collapsed">
                //</editor-fold>
                //<editor-fold desc="Guardado Apartado 2.8 Uso de Suelos y/o Cuerpos de Agua" defaultstate="collapsed">
                try {
                    for (SueloVegetacionProyecto s : sueloVegetacion) {
                        miaDao.merge(s);
                    }
                    //Se guardaban los cambios de la tabla de Uso de Suelo
//                    for (PrediocolinProy p : predCol) {
//                        miaDao.merge(p);
//                    }
                    if (predCol.size() >= 1 && !sueloVegetacion.isEmpty()) {
                        //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                        try {
                            miaDao.guardarAvance(proyecto, new Short("2"), "28");
                        } catch (Exception e) {
                            System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                        }
                        //</editor-fold>
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //</editor-fold>

                //<editor-fold desc="Guardado Apartado 2.9 Descripción de las Obras Principales del Proyecto" defaultstate="collapsed">
                if (proyecto.getNsub() != null) {
                    if (proyecto.getNsub().equals(new Short("6"))
                            && proyecto.getProyIndActivitipoind() != null
                            && proyecto.getProyIndProcyoperaciones() != null
                            && proyecto.getProyIndProcSistemasagua() != null
                            && proyecto.getProyIndProcSistemasconge() != null
                            && proyecto.getProyIndCapacidadDisenio() != null
                            && proyecto.getProyIndDiagramaproc() != null) {
                        //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                        try {
                            miaDao.guardarAvance(proyecto, new Short("2"), "29");
                        } catch (Exception e) {
                            System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                        }
                        //</editor-fold>
                    }
                }
                //</editor-fold>

                //<editor-fold desc="Guardado Apartado 2.10 Programa general de trabajo" defaultstate="collapsed">
                if (proyecto.getProyTiempoVidaMeses() != null || proyecto.getProyTiempoVidaAnios() != null) {
                    //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                    try {
                        if (!proyecto.getNsub().equals(new Short("3")) && !proyecto.getNsub().equals(new Short("6")) && !proyecto.getNsub().equals(new Short("7"))) {
                            miaDao.guardarAvance(proyecto, new Short("2"), "210");
                        } else {
                            if(claveTramite == 66 || claveTramite == 67){ //Debe guardar 2% para la mia regional
                                miaDao.guardarAvance(proyecto, new Short("2"), "210");
                            }else{
                                miaDao.guardarAvance(proyecto, new Short("1"), "210");
                            }
                        }
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                    //</editor-fold>
                }
                //</editor-fold>

                //<editor-fold desc="Guardado Apartado 2.11 Información Biotecnológica de las Especies" defaultstate="collapsed">
                if (infoBioList.size() >= 1) {
                    if (proyecto.getNsub().equals(new Short("4"))) {
                        //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                        try {
                            miaDao.guardarAvance(proyecto, new Short("2"), "211");
                        } catch (Exception e) {
                            System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                        }
                        //</editor-fold>
                    }
                }

                //</editor-fold>
                //<editor-fold desc="Guardado Apartado 2.12 Etapas del Proyecto" defaultstate="collapsed">
                try {
                    if (etapaConstruccion.getActividadEtapaList().size() >= 1 || etapaAbandono.getActividadEtapaList().size() >= 1 || etapaOperacion.getActividadEtapaList().size() >= 1 || etapaPreparacion.getActividadEtapaList().size() >= 1) {
                        try {
                            miaDao.merge(etapaConstruccion);
                            miaDao.merge(etapaAbandono);
                            miaDao.merge(etapaOperacion);
                            miaDao.merge(etapaPreparacion);
                            //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                            try {
                                miaDao.guardarAvance(proyecto, new Short("2"), "212");
                            } catch (Exception e) {
                                System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                            }
                            //</editor-fold>
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Listas de actividades vacía");
                }
                //</editor-fold>

                //<editor-fold desc="Guardado Apartado 2.13 Otros Insumos se encuentra en Cap_2_10_InsumosView.java" defaultstate="collapsed">
                //</editor-fold>
                //<editor-fold desc="Guardado Apartado 2.14 Sustancias Peligorsas está en el método GuardarSustancias()" defaultstate="collapsed">
                if (!contaminanteuList.isEmpty()) {
                    System.out.println("guardando contaminante");
                    for (Contaminanteu c : contaminanteuList) {

                        ContaminanteProyecto c1 = 
                                new ContaminanteProyecto(
                                        new ContaminanteProyectoPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), c.getContaminante().getContaminanteProyectoPK().getContaminanteId()));
                        //c1.setCatContaminante(c.getContaminante().getCatContaminante());
                        c1.setContaminanteCantidad(c.getContaminante().getContaminanteCantidad());
                        c1.setContaminanteMedidaControl(c.getContaminante().getContaminanteMedidaControl());
                        c1.setCtunClave(c.getContaminante().getCtunClave());
    //                    c1.setEtapaProyecto(c.getContaminante().getEtapaProyecto());
                        c1.setEtapaId(c.getContaminante().getEtapaProyecto() != null ?
                                c.getContaminante().getEtapaProyecto().getEtapaProyectoPK().getEtapaId(): null);
                        c1.setContaminanteEmisiones( c.getContaminante().getCatContaminante() != null ?
                                c.getContaminante().getCatContaminante().getContaminanteId() : null);
                        c1.setFuenteEmisora(c.getContaminante().getFuenteEmisora());
                        c1.setObservaciones(c.getContaminante().getObservaciones());
                        c1.setContaminanteTipo(c.getContaminante().getContaminanteTipo());
                        miaDao.merge(c1);
                    }
                }
                //</editor-fold>
                //<editor-fold desc="Guardado Apartado 2.15 Utilización de Explosivos" defaultstate="collapsed">            
                try {
                    for (ActividadEtapa a : explosivosList) {
                        miaDao.merge(a);
                    }
                    if (proyecto.getProyExplosivosAplica() != null && proyecto.getProyExplosivosAplica().compareTo("S") == 0) {
                        if (explosivosList.size() >= 1) {
                            if ((!proyecto.getNsub().equals(new Short("2"))
                                    && !proyecto.getNsub().equals(new Short("4"))
                                    && !proyecto.getNsub().equals(new Short("6")))
                                    || claveTramite == 66 || claveTramite ==67){
                                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                                try {
                                    miaDao.guardarAvance(proyecto, new Short("2"), "215");
                                } catch (Exception e) {
                                    System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                                }
                                //</editor-fold>
                            }
                        }
                    } else if (proyecto.getProyExplosivosAplica() != null && proyecto.getProyExplosivosAplica().compareTo("N") == 0) {
                        if ((!proyecto.getNsub().equals(new Short("2"))
                                && !proyecto.getNsub().equals(new Short("4"))
                                && !proyecto.getNsub().equals(new Short("6")))
                                || claveTramite == 66 || claveTramite ==67)  {
                            //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                            try {
                                miaDao.guardarAvance(proyecto, new Short("2"), "215");
                            } catch (Exception e) {
                                System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                            }
                            //</editor-fold>
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //</editor-fold>

                //<editor-fold desc="Guardado Apartado 2.16 Generación, Manejo y Disposición de Residuos Peligrosos" defaultstate="collapsed">
//                try {
//                    for (ContaminanteProyecto c : contaminanteList) {
//                        miaDao.merge(c);
//                    }
//                    if (contaminanteList.size() >= 0) {
//                        //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
//                        try {
//                            if (proyecto.getNsub().equals(new Short("2"))) {
//                                if(claveTramite == 66 || claveTramite == 67){//Si es una mia regional se debe guardar 2%
//                                    miaDao.guardarAvance(proyecto, new Short("2"), "216");
//                                }else{
//                                    miaDao.guardarAvance(proyecto, new Short("3"), "216");
//                                }
//                            } else {
//                                miaDao.guardarAvance(proyecto, new Short("2"), "216");
//                            }
//                        } catch (Exception e) {
//                            System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
//                        }
//                        //</editor-fold>
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                //</editor-fold>

                //<editor-fold desc="Guardado Apartado 2.17 Posibles Accidentes Ambientales" defaultstate="collapsed">
                try {
                	//Se guardaban los accidentes de la lista
//                    for (AccidentesProyecto a : accidenteList) {
//                        miaDao.merge(a);
//                    }
                    if (accidenteList.size() >= 1) {
                        if (proyecto.getNsub().equals(new Short("3"))) {
                            //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                            try {
                                miaDao.guardarAvance(proyecto, new Short("1"), "217");
                            } catch (Exception e) {
                                System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                            }
                            //</editor-fold>
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //</editor-fold>

                try {
                    Validacion v = new Validacion();
                    String msg = v.validar(seccion, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                    if (!msg.isEmpty()) {
                        return;
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
                super.guardarComentarios();
                
                System.out.println("Su información ha sido guardada con éxito");
                reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
//            }
        } catch (Exception e) {
            Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e.getMessage());
            e.printStackTrace();
            System.out.println("No se ha podido guardar la información, intente mas tarde.");
            reqcontEnv.execute("mensajes('No se ha podido guardar la información, intente más tarde.', 'danger')");            
        }

        /*
         try {
         for (ActividadEtapa a : explosivosList) {
         miaDao.merge(a);
         }
         //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
         if (!proyecto.getNsub().equals(new Short("2")) && !proyecto.getNsub().equals(new Short("4")) && !proyecto.getNsub().equals(new Short("6"))) {
         try {
         miaDao.guardarAvance(proyecto, new Short("2"), "215");
         } catch (Exception e) {
         System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
         }
         }
         //</editor-fold>
         } catch (Exception e) {
         e.printStackTrace();
         }*/
        //Guardado de componentes en información adicional - eescalona
        

        if (Integer.parseInt(miaDao.avanceProyecto(proyecto)) == 99) {
            reqcontEnv.execute("mensajes('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.', 'success')");
        }
    }//</editor-fold>
    
    
    
    
    /**
     * Guardar
     */
    public void guardarProyectoSustancia() {  // pool
    	
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        System.out.println("\n\nguardarProyectoSustancia");
        System.out.println("Sustancia aplica : " + proySustAplica);
        proyecto.setProySustanciaAplica(proySustAplica?"S":"N");
        System.out.println("Sustancia aplica : " + proyecto.getProySustanciaAplica());
          
        if(proySustAplica)
        {
            if (verificaCantidad()) {
            	
            	// muestra el div de que alguna sustancia excede
            	reqcontEnv.execute("mal1();");  
            	
	    		// muestra el menu 'Estudio de Riesgo'
	      		reqcontEnv.execute("parent.verER();");      		
	      		proyecto.setEstudioRiesgoId((short)1);
            } else {
            	
            	// oculta el div de que alguna sustancia excede
            	reqcontEnv.execute("mal2();");
    	  		
            	// oculta el menu 'Estudio de Riesgo'
            	reqcontEnv.execute("parent.verER2();");        
                proyecto.setEstudioRiesgoId((short) 0);
            }
        }
        else
        { 
        	// oculta el div de que alguna sustancia excede
        	reqcontEnv.execute("mal2();");
        	
        	// oculta el menu 'Estudio de Riesgo'
        	reqcontEnv.execute("parent.verER2();");        	
        	proyecto.setEstudioRiesgoId((short)0);
        }
        
        //Se realiza el guardado de la ponderación para el apartado de sustancias
        Short vValorPonderacion = 0;
        if (claveTramite == 66 || claveTramite == 67){ //Si es una mia regional sólo guarda 1 porque tiene más apartados
            vValorPonderacion = 2;
        }else if (proyecto.getNsub()==2){ //Si es una mia particular y pertenece al subsector forestal le corresponde 3
                vValorPonderacion = 3;
        }else if (proyecto.getNsub()==7){ //Si es una mia particular y pertenece al subsector comunicaciones le corresponde 1
                vValorPonderacion = 1;
        }else{//Si es particular y no pertenece a comunicaciones ni forestal sólo se otorgan 2
                vValorPonderacion = 2;
        }
        
        try {
            miaDao.merge(proyecto);
            try {
                miaDao.guardarAvance(proyecto, vValorPonderacion, "214");
            } catch (Exception e) {
                System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
            }
        } catch (Exception e) {
        }
        
        //------------INICIO actualiza avance de capitulos 5,6,7 y 8 dependiendo si tiene o no estudios de riesgo---------
        //---------------------guardado de avance de capitulo 5-------------------------------
            List<ImpacAmbProyecto> evaluacion = new ArrayList<ImpacAmbProyecto>();
            evaluacion = miaDao.getImpacAmbProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            
            if (proyecto.getDescMetutil() != null) {
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if (proyecto.getEstudioRiesgoId() == 1){
                    try {
                        miaDao.guardarAvance(proyecto, new Short("3"), "51");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("6"), "51");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }                    
                }
                //</editor-fold>
            }
                        
            if (evaluacion.size() >= 1) {
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if (proyecto.getEstudioRiesgoId() == 1){
                    try {
                        miaDao.guardarAvance(proyecto, new Short("3"), "52");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("5"), "52");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
                    
            
        //---------------------guardado de avance de capitulo 6-------------------------------
            List<MedPrevImpactProy> medPreventivas = new ArrayList<MedPrevImpactProy>();
            medPreventivas = miaDao.getMedPrevImpactProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                        
            if(medPreventivas.size() >= 1){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId()==1)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("6"), "6");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else{
                    try {
                        miaDao.guardarAvance(proyecto, new Short("11"), "6");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            
        //---------------------guardado de avance de capitulo 7-------------------------------
            if(proyecto.getEsceConProy()!=null && proyecto.getEsceSinProy()!=null
                    && proyecto.getEsceConProyMed()!=null && proyecto.getPronosAmb()!=null)
            {
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId() == 1)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("3"), "71");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                } else {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("6"), "71");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            if (proyecto.getConclusionProy() != null && proyecto.getEvaAlternativas() != null) 
            {
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                 if(proyecto.getEstudioRiesgoId() == 1)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("3"), "72");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                } else {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("5"), "72");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                 }
                //</editor-fold>
            }
        //---------------------guardado de avance de capitulo 8-------------------------------
            List<GlosarioProyecto> glosario = new ArrayList<GlosarioProyecto>();
            glosario = miaDao.getGlosario(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            
            if (proyecto.getManifestacionRes() != null) {
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId() == 1){
                    try {
                        miaDao.guardarAvance(proyecto, new Short("3"), "81");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else{
                    try {
                        miaDao.guardarAvance(proyecto, new Short("8"), "81");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            if (glosario.size() >= 1){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId() == 1){
                    try {
                        miaDao.guardarAvance(proyecto, new Short("1"), "82");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else{
                    try {
                        miaDao.guardarAvance(proyecto, new Short("2"), "82");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            if (proyecto.getBiblioProy() != null){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId() == 1){
                    try {
                        miaDao.guardarAvance(proyecto, new Short("1"), "83");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else{
                    try {
                        miaDao.guardarAvance(proyecto, new Short("2"), "83");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            } 
        //---------------------guardado de avance de capitulo ERA-------------------------------
            List<AvanceProyecto> avanceCapERA = new ArrayList<AvanceProyecto>();
            avanceCapERA = miaDao.getAvanceCapERA(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),(short) 101);                        
            if(avanceCapERA.size() >= 1){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId()==0)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("0"), "101");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            avanceCapERA = miaDao.getAvanceCapERA(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),(short) 102);                        
            if(avanceCapERA.size() >= 1){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId()==0)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("0"), "102");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            avanceCapERA = miaDao.getAvanceCapERA(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),(short) 103);                        
            if(avanceCapERA.size() >= 1){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId()==0)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("0"), "103");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            avanceCapERA = miaDao.getAvanceCapERA(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),(short) 104);                        
            if(avanceCapERA.size() >= 1){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId()==0)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("0"), "104");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            avanceCapERA = miaDao.getAvanceCapERA(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),(short) 105);                        
            if(avanceCapERA.size() >= 1){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId()==0)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("0"), "105");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
        //------------FIN actualiza avance de capitulos 5,6,7 y 8 dependiendo si tiene o no estudios de riesgo---------

        try {
            Validacion v = new Validacion();
            String msg = v.validar(seccion, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            if (!msg.isEmpty()) {
                return;
            }
        } catch (Exception err) {
            err.printStackTrace();
        }

        try {
            for (SustanciaU s : sustanciasList) {
                miaDao.merge(s.model);
            }
            if (proyecto.getProySustanciaAplica().equals("S")) {
                if (sustanciasList.size() >= 1) {
                    //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                    try {
                        if (proyecto.getNsub().equals(new Short("2"))) {
                            miaDao.guardarAvance(proyecto, new Short("3"), "214");
                        } else {
                            if (proyecto.getNsub().equals(new Short("7"))) {
                                miaDao.guardarAvance(proyecto, new Short("1"), "214");
                            } else {
                                miaDao.guardarAvance(proyecto, new Short("2"), "214");
                            }
                        }
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                    //</editor-fold>
                }
            } else if (proyecto.getProySustanciaAplica().equals("N")) {
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                try {
                    if (proyecto.getNsub().equals(new Short("2"))) {
                        miaDao.guardarAvance(proyecto, new Short("3"), "214");
                    } else {
                        if (proyecto.getNsub().equals(new Short("7"))) {
                            miaDao.guardarAvance(proyecto, new Short("1"), "214");
                        } else {
                            miaDao.guardarAvance(proyecto, new Short("2"), "214");
                        }
                    }
                } catch (Exception e) {
                    System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                }
                //</editor-fold>
            }
            
            System.out.println("Su información ha sido guardada con éxito");
            reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");

        } catch (Exception e) {
        	e.printStackTrace();
            Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e.getMessage());
            reqcontEnv.execute("mensajes('No se ha podido guardar la información, intente más tarde.', 'danger')");
        }
    }

    
    
    public void iniciarArchivo() {
    	System.out.println("iniciarArchivo...");
    	archivoTemporal = new ArchivosProyecto(
    			getProyecto().getProyectoPK().getFolioProyecto(), 
    			getProyecto().getProyectoPK().getSerialProyecto(), 
    			(short) 2,
    			(short) 2,
    			(short) Integer.parseInt(this.vseccion),
    			(short) 0);
    	
    	System.out.println("id de archivo : " + archivoTemporal.getSeqId());
    	
    	consultarSustancia();
    	
    	this.headerText = "Agregar archivo a la sustancia #" + editSustancia.getModel().getSustanciaProyectoPK().getSustProyId();
    	
    	cargaBean = new CargaBean();
    	msj = " ";
    	
    	/*Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		short pduId = 0;
		String id = params.get("id").toString();
		pduId = Short.parseShort(id);
		this.pduSeleccionado = getMiaDao().getPdumProyecto(pduId);
		this.seleccionEdicion = true;
		this.headerText = "Guardar archivo para el PDU #" + pduSeleccionado.getPdumProyectoPK().getPdumId();
		this.alertText = "Archivo guardado correctamente";*/
    }

    
    public void guardarArchivo() {
    	short id;
    	System.out.println("\n\nguardarArchivo...");
		id = miaDao.getMaxArchivoEtapaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), 
				archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(), archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());

		archivoTemporal.setId(id);
    	System.out.println("id de archivo : " + archivoTemporal.getSeqId());
    	try {
    		bandera = cargaBean.GuardaAnexos(archivoTemporal);
			if (bandera == true) {

	    	this.editSustancia.getModel().setIdArchivoProyecto(archivoTemporal.getSeqId());
	    	// this.pduSeleccionado.setIdArchivoProyecto(archivoTemporal.getSeqId());
	    	// agregarPdu();
	    	System.out.println("id de archivo despues de GuardarAnexox: " + archivoTemporal.getSeqId());
	    	System.out.println("id de archivo editSustancia.getModel(): " + editSustancia.getModel().getIdArchivoProyecto());

	    	miaDao.persist(editSustancia.getModel());			
			// sustanciasList.get(2).getModel().setIdArchivoProyecto(editSustancia.getModel().getIdArchivoProyecto());
			
			// Sustancias
			sustanciasList = new ArrayList<SustanciaU>();
	        for (SustanciaProyecto s : miaDao.getSustancias(editSustancia.getModel().getSustanciaProyectoPK().getFolioProyecto(), editSustancia.getModel().getSustanciaProyectoPK().getSerialProyecto())) {	        	
	        	sustanciasList.add(new SustanciaU(s, s.getSustanciaId().getSustanciaId().equals(new Short("9999")), 0, getUnidad(Short.parseShort(s.getSustanciaId().getSustanciaUnidad()))));	            
	        }
	        
	        proySustAplica = Boolean.FALSE;
	        if(sustanciasList != null && sustanciasList.size()>0){
	        	proySustAplica = Boolean.TRUE;
	        }
	        msj = "";
	        cargaBean = new CargaBean();
	        
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("dlgSubirArchivo.hide();modalAlerta.show()");
	        
			RequestContext.getCurrentInstance().update("fTabla:tablaDatos");
			
			
			}else {
				msj = "Se requiere cargar un archivo";
				
			}

	    	
    	} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    }
    
    

    //<editor-fold desc="Getters & Setters" defaultstate="collapsed">
    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    public Boolean getSupera() {
        return supera;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the NSubSector
     */
    public Short getNSubSector() {
        return NSubSector;
    }

    /**
     * @param NSubSector the NSubSector to set
     */
    public void setNSubSector(Short NSubSector) {
        this.NSubSector = NSubSector;
    }

    /**
     * @return the criterios
     */
    public List<CriteriosProyecto> getCriterios() {
        return criterios;
    }

    /**
     * @param criterios the criterios to set
     */
    public void setCriterios(List<CriteriosProyecto> criterios) {
        this.criterios = criterios;
    }

    /**
     * @return the domEsta
     */
    public Boolean getDomEsta() {
        return domEsta;
    }

    /**
     * @param domEsta the domEsta to set
     */
    public void setDomEsta(Boolean domEsta) {
        this.domEsta = domEsta;
    }

    /**
     * @return the domLoca
     */
    public Boolean getDomLoca() {
        return domLoca;
    }

    /**
     * @param domLoca the domLoca to set
     */
    public void setDomLoca(Boolean domLoca) {
        this.domLoca = domLoca;
    }

    /**
     * @return the capServicios
     */
    public boolean isCapServicios() {
        return capServicios;
    }

    /**
     * @param capServicios the capServicios to set
     */
    public void setCapServicios(boolean capServicios) {
        this.capServicios = capServicios;
    }

    /**
     * @return the servicios
     */
    public List<ServicioProyecto> getServicios() {
        return servicios;
    }

    /**
     * @param servicios the servicios to set
     */
    public void setServicios(List<ServicioProyecto> servicios) {
        this.servicios = servicios;
    }

    /**
     * @return the infoBio
     */
    public InfobioProyecto getInfoBio() {
        return infoBio;
    }

    /**
     * @param infoBio the infoBio to set
     */
    public void setInfoBio(InfobioProyecto infoBio) {
        this.infoBio = infoBio;
    }

    /**
     * @return the infoBioList
     */
    public List<InfobioProyecto> getInfoBioList() {
        return infoBioList;
    }

    /**
     * @param infoBioList the infoBioList to set
     */
    public void setInfoBioList(List<InfobioProyecto> infoBioList) {
        this.infoBioList = infoBioList;
    }

    /**
     * @return the actividadesPrepara
     */
    public List<ActividadEtapa> getActividadesPrepara() {
        return actividadesPrepara;
    }

    /**
     * @param actividadesPrepara the actividadesPrepara to set
     */
    public void setActividadesPrepara(List<ActividadEtapa> actividadesPrepara) {
        this.actividadesPrepara = actividadesPrepara;
    }

    /**
     * @return the actividadesConstru
     */
    public List<ActividadEtapa> getActividadesConstru() {
        return actividadesConstru;
    }

    /**
     * @param actividadesConstru the actividadesConstru to set
     */
    public void setActividadesConstru(List<ActividadEtapa> actividadesConstru) {
        this.actividadesConstru = actividadesConstru;
    }

    /**
     * @return the actividadesOperaci
     */
    public List<ActividadEtapa> getActividadesOperaci() {
        return actividadesOperaci;
    }

    /**
     * @param actividadesOperaci the actividadesOperaci to set
     */
    public void setActividadesOperaci(List<ActividadEtapa> actividadesOperaci) {
        this.actividadesOperaci = actividadesOperaci;
    }

    /**
     * @return the actividadesAbandon
     */
    public List<ActividadEtapa> getActividadesAbandon() {
        return actividadesAbandon;
    }

    /**
     * @param actividadesAbandon the actividadesAbandon to set
     */
    public void setActividadesAbandon(List<ActividadEtapa> actividadesAbandon) {
        this.actividadesAbandon = actividadesAbandon;
    }

    /**
     * @return the sustanciasList
     */
    public List<SustanciaU> getSustanciasList() {
        return sustanciasList;
    }

    /**
     * @param sustanciasList the sustanciasList to set
     */
    public void setSustanciasList(List<SustanciaU> sustanciasList) {
        this.sustanciasList = sustanciasList;
    }

	/**
     * @return the etapaPreparacion
     */
    public EtapaProyecto getEtapaPreparacion() {
        return etapaPreparacion;
    }

    /**
     * @param etapaPreparacion the etapaPreparacion to set
     */
    public void setEtapaPreparacion(EtapaProyecto etapaPreparacion) {
        this.etapaPreparacion = etapaPreparacion;
    }

    /**
     * @return the etapaConstruccion
     */
    public EtapaProyecto getEtapaConstruccion() {
        return etapaConstruccion;
    }

    /**
     * @param etapaConstruccion the etapaConstruccion to set
     */
    public void setEtapaConstruccion(EtapaProyecto etapaConstruccion) {
        this.etapaConstruccion = etapaConstruccion;
    }

    /**
     * @return the etapaOperacion
     */
    public EtapaProyecto getEtapaOperacion() {
        return etapaOperacion;
    }

    /**
     * @param etapaOperacion the etapaOperacion to set
     */
    public void setEtapaOperacion(EtapaProyecto etapaOperacion) {
        this.etapaOperacion = etapaOperacion;
    }

    /**
     * @return the etapaAbandono
     */
    public EtapaProyecto getEtapaAbandono() {
        return etapaAbandono;
    }

    /**
     * @param etapaAbandono the etapaAbandono to set
     */
    public void setEtapaAbandono(EtapaProyecto etapaAbandono) {
        this.etapaAbandono = etapaAbandono;
    }

    /**
     * @return the explosivosList
     */
    public List<ActividadEtapa> getExplosivosList() {
        return explosivosList;
    }

    /**
     * @param explosivosList the explosivosList to set
     */
    public void setExplosivosList(List<ActividadEtapa> explosivosList) {
        this.explosivosList = explosivosList;
    }

    /**
     * @return the contaminanteList
     */
    public List<ContaminanteProyecto> getContaminanteList() {
        return contaminanteList;
    }

    /**
     * @param contaminanteList the contaminanteList to set
     */
    public void setContaminanteList(List<ContaminanteProyecto> contaminanteList) {
        this.contaminanteList = contaminanteList;
    }

    /**
     * @return the verPreparacion
     */
    public Boolean getVerPreparacion() {
        return verPreparacion;
    }

    /**
     * @param verPreparacion the verPreparacion to set
     */
    public void setVerPreparacion(Boolean verPreparacion) {
        this.verPreparacion = verPreparacion;
    }

    /**
     * @return the verConstruccion
     */
    public Boolean getVerConstruccion() {
        return verConstruccion;
    }

    /**
     * @param verConstruccion the verConstruccion to set
     */
    public void setVerConstruccion(Boolean verConstruccion) {
        this.verConstruccion = verConstruccion;
    }

    /**
     * @return the verOperacion
     */
    public Boolean getVerOperacion() {
        return verOperacion;
    }

    /**
     * @param verOperacion the verOperacion to set
     */
    public void setVerOperacion(Boolean verOperacion) {
        this.verOperacion = verOperacion;
    }

    /**
     * @return the verAbandono
     */
    public Boolean getVerAbandono() {
        return verAbandono;
    }

    /**
     * @param verAbandono the verAbandono to set
     */
    public void setVerAbandono(Boolean verAbandono) {
        this.verAbandono = verAbandono;
    }

    /**
     * @return the verMsgEtapa
     */
    public Boolean getVerMsgEtapa() {
        return verMsgEtapa;
    }

    /**
     * @param verMsgEtapa the verMsgEtapa to set
     */
    public void setVerMsgEtapa(Boolean verMsgEtapa) {
        this.verMsgEtapa = verMsgEtapa;
    }

    /**
     * @return the accidenteList
     */
    public List<AccidentesProyecto> getAccidenteList() {
        return accidenteList;
    }

    /**
     * @param accidenteList the accidenteList to set
     */
    public void setAccidenteList(List<AccidentesProyecto> accidenteList) {
        this.accidenteList = accidenteList;
    }

    /**
     * @return the ep
     */
    public List<EtapaProyecto> getEp() {
        return ep;
    }

    /**
     * @param ep the ep to set
     */
    public void setEp(List<EtapaProyecto> ep) {
        this.ep = ep;
    }

    /**
     * @return the sep
     */
    public List<SelectItem> getSep() {
        return sep;
    }

    /**
     * @param sep the sep to set
     */
    public void setSep(List<SelectItem> sep) {
        this.sep = sep;
    }

    public Boolean getServOtro() {
        return servOtro;
    }

    public void setServOtro(Boolean servOtro) {
        this.servOtro = servOtro;
    }

    /**
     * @return the predCol
     */
    public List<PrediocolinProy> getPredCol() {
        return predCol;
    }

    /**
     * @param predCol the predCol to set
     */
    public void setPredCol(List<PrediocolinProy> predCol) {
        this.predCol = predCol;
    }

    /**
     * @return the usoSueloVeg
     */
    public List<UsoSueloVeget> getUsoSueloVeg() {
        return usoSueloVeg;
    }

    /**
     * @param usoSueloVeg the usoSueloVeg to set
     */
    public void setUsoSueloVeg(List<UsoSueloVeget> usoSueloVeg) {
        this.usoSueloVeg = usoSueloVeg;
    }//</editor-fold>

    //<editor-fold desc="Verificar si supera la cantidad permitida" defaultstate="collapsed">
    public boolean verificaCantidad() {
        System.out.println("verifica");
        boolean s = false;
        for (SustanciaU sp : sustanciasList) {
            try {
                if (!s) {
                    System.out.println("valor " + sp.getModel().getSustanciaCantidadAlmacenada().doubleValue());
                    System.out.println("umbral " + sp.getModel().getSustanciaId().getSustanciaCantidad().doubleValue());

                    s = sp.getModel().getSustanciaCantidadAlmacenada().doubleValue()
                            > sp.getModel().getSustanciaId().getSustanciaCantidad().doubleValue()
                            && !sp.getOtra();
                } else {
                    break;
                }
            } catch (NullPointerException e) {
            }
        }
        if (s) {
            System.out.println("sup");
        }
        supera = s;
        System.out.println(s);
        return s;
    }//</editor-fold>
    
    
    public boolean verificaCantidadAgregar() {
        System.out.println("verifica");
        boolean s = false;
        
                    System.out.println("valor " + sustanciaCantidadAlmacenada.doubleValue());
                    System.out.println("umbral " + sustanciariesgosaId.getSustanciaCantidad().doubleValue());

                    s = sustanciaCantidadAlmacenada.doubleValue()
                            > sustanciariesgosaId.getSustanciaCantidad().doubleValue()
                            && (sustanciariesgosaId.getSustanciaDescripcion().equalsIgnoreCase("Otra")==false);
     
        sustanciaexcede = "0";
        if(s)
        	sustanciaexcede = "1";
        
        return s;
    }
    
    
    public void verificaOtra(){
    	    	
    	CatSustanciaAltamRiesgosa riesgosa = getSustanciariesgosaId();
    	
    	if(riesgosa != null){
    		    		
    		System.out.println(riesgosa.getSustanciaDescripcion());
    		// ctunClve = riesgosa.getSustanciaId();
    		
    		// ctunClve = sp.getCtunClve();
    		ctunDesc = getUnidad(Short.parseShort(riesgosa.getSustanciaUnidad())).getCtunAbre();
    		

    	}
    	
    	
    	
    }

    //<editor-fold desc="autocompletar" defaultstate="collapsed">
    @SuppressWarnings({ "unchecked", "unused", "rawtypes" })
	public List<CatSustanciaAltamRiesgosa> completarSustancias(String qry) {
        List<CatSustanciaAltamRiesgosa> list = (List<CatSustanciaAltamRiesgosa>) miaDao.listado_like(CatSustanciaAltamRiesgosa.class, "sustanciaDescripcion", qry.concat("%").trim().toUpperCase());
        if (list.isEmpty()) {
            if (list == null) {
                list = new ArrayList();
                list.add((CatSustanciaAltamRiesgosa) miaDao.busca(CatSustanciaAltamRiesgosa.class, new Short("9999")));
            } else {
                list.add((CatSustanciaAltamRiesgosa) miaDao.busca(CatSustanciaAltamRiesgosa.class, new Short("9999")));

            }
        }
        return list;
    }//</editor-fold>

    public Integer getClaveTramite() {
        return claveTramite;
    }

    public void setClaveTramite(Integer claveTramite) {
        this.claveTramite = claveTramite;
    }

    public short getSubsec() {
        return subsec;
    }

    public void setSubsec(short subsec) {
        this.subsec = subsec;
    }

    /**
     * @return the contaminanteuList
     */
    public List<Contaminanteu> getContaminanteuList() {
        return contaminanteuList;
    }

    /**
     * @param contaminanteuList the contaminanteuList to set
     */
    public void setContaminanteuList(List<Contaminanteu> contaminanteuList) {
        this.contaminanteuList = contaminanteuList;
    }

    /**
     * @return the vseccion
     */
    public String getVseccion() {
        return vseccion;
    }

    /**
     * @param vseccion the vseccion to set
     */
    public void setVseccion(String vseccion) {
        this.vseccion = vseccion;
    }

    /**
     * @return the seccion
     */
    public String getSeccion() {
        return seccion;
    }

    /**
     * @param seccion the seccion to set
     */
    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public List<CatUnidadMedida> getCatUnidades() {
        return catUnidades;
    }

    public void setCatUnidades(List<CatUnidadMedida> catUnidades) {
        this.catUnidades = catUnidades;
    }

    public List<SueloVegetacionProyecto> getSueloVegetacion() {
        return sueloVegetacion;
    }

    public void setSueloVegetacion(List<SueloVegetacionProyecto> sueloVegetacion) {
        this.sueloVegetacion = sueloVegetacion;
    }

   public class SustanciaU {

        //<editor-fold desc="Clase para la carga de campo << Otra >> en sustancias" defaultstate="collapsed">
        private SustanciaProyecto model;
        private Boolean otra;
        private Integer idAnexo;
        CatUnidadMedida unidad;

        public SustanciaU(SustanciaProyecto model, Boolean otra, Integer idAnexo, CatUnidadMedida unidad) {
            this.model = model;
            this.otra = otra;
            this.idAnexo = idAnexo;
            this.unidad = unidad;
        }

        public SustanciaProyecto getModel() {
            return model;
        }

        public void setModel(SustanciaProyecto model) {
            this.model = model;
        }

        public Boolean getOtra() {
            if (getModel().getSustanciaId() != null) {
                otra = getModel().getSustanciaId().getSustanciaId().equals(new Short("9999"));
            }
            return otra;
        }

        public boolean getAnexo() {
            return model.getSustanciaProyectoPK() != null;
        }

        public void setOtra(Boolean otra) {
            this.otra = otra;
        }

        public Integer getIdAnexo() {
            return idAnexo;
        }

        public void setIdAnexo(Integer idAnexo) {
            this.idAnexo = idAnexo;
        }

        public void actualizaCtunClve() {
            CatUnidadMedida unidadMedida;
            if (getModel().getSustanciaId() != null && getModel().getSustanciaId().getSustanciaId() != 9999) {
                unidadMedida = obtieneSustancia(getModel().getSustanciaId().getSustanciaUnidad());
                if (unidadMedida != null) {
                    getModel().setCtunClve(unidadMedida.getCtunClve());
                }
            }
        }//</editor-fold>

		public CatUnidadMedida getUnidad() {
			return unidad;
		}

		public void setUnidad(CatUnidadMedida unidad) {
			this.unidad = unidad;
		}
     
        
    }
    
    private CatUnidadMedida obtieneSustancia(String nombre) {
        CatUnidadMedida unidad = null;
        for(CatUnidadMedida unidadMedida : catUnidades) {
            if (unidadMedida.getCtunAbre().trim().compareToIgnoreCase(nombre.trim()) == 0 || 
                    unidadMedida.getCtunDesc().trim().compareToIgnoreCase(nombre.trim()) == 0 ) {
                unidad = unidadMedida;
                break;
            }
        }
        
        return unidad;
    }
    

    public List<EtapaProyecto> getListaEtapas() {
        return listaEtapas;
    }

    public void setListaEtapas(List<EtapaProyecto> listaEtapas) {
        this.listaEtapas = listaEtapas;
    }

    public Integer getNumElemBio() {
        return numElemBio;
    }

    public void setNumElemBio(Integer numElemBio) {
        this.numElemBio = numElemBio;
    }

    public boolean isCapExplosivos() {
        return capExplosivos;
    }

    public void setCapExplosivos(boolean capExplosivos) {
        this.capExplosivos = capExplosivos;
    }

    public boolean isCapSustancias() {
        return capSustancias;
    }

    public void setCapSustancias(boolean capSustancias) {
        this.capSustancias = capSustancias;
    }

	public CriteriosProyecto getEditCriterio() {
		return editCriterio;
	}

	public void setEditCriterio(CriteriosProyecto editCriterio) {
		this.editCriterio = editCriterio;
	}

	public List<PrediocolinProy> getSelectedPredios() {
		return selectedPredios;
	}

	public void setSelectedPredios(List<PrediocolinProy> selectedPredios) {
		this.selectedPredios = selectedPredios;
	}

	public PrediocolinProy getPredio() {
		return predio;
	}

	public void setPredio(PrediocolinProy predio) {
		this.predio = predio;
	}

	public PrediocolinProy getSelectedPredio() {
		return selectedPredio;
	}

	public void setSelectedPredio(PrediocolinProy selectedPredio) {
		this.selectedPredio = selectedPredio;
	}

	/**
	 * @return the nuevoServicio
	 */
	public ServicioProyecto getNuevoServicio() {
		return nuevoServicio;
	}

	/**
	 * @param nuevoServicio the nuevoServicio to set
	 */
	public void setNuevoServicio(ServicioProyecto nuevoServicio) {
		this.nuevoServicio = nuevoServicio;
	}

	/**
	 * @return the serv_seleccionados
	 */
	public List<ServicioProyecto> getServ_seleccionados() {
		return serv_seleccionados;
	}

	/**
	 * @param serv_seleccionados the serv_seleccionados to set
	 */
	public void setServ_seleccionados(List<ServicioProyecto> serv_seleccionados) {
		this.serv_seleccionados = serv_seleccionados;
	}

	public PrediocolinProy getEditPredio() {
		return editPredio;
	}

	public void setEditPredio(PrediocolinProy editPredio) {
		this.editPredio = editPredio;
	}
	
	public void respuestaAplicaSi(){
    	this.proySustAplica = Boolean.TRUE;
    	muestraDivTablaDatos();
    	primeraCarga = "0"; // indica que ya se clicó un boton despues de la carga inicial de la pantalla
    }
    
    public void respuestaAplicaNo(){
    	this.proySustAplica = Boolean.FALSE;    	
    	ocultaDivTablaDatos();
    	primeraCarga = "0"; // indica que ya se clicó un boton despues de la carga inicial de la pantalla
    }
    
    
    public void muestraDivTablaDatos(){
    	RequestContext context = RequestContext.getCurrentInstance();
		context.execute("muestraDivTablaDatos();");
    }
    public void ocultaDivTablaDatos(){
	    RequestContext context = RequestContext.getCurrentInstance();
		context.execute("ocultaDivTablaDatos();");
    }

    
	public boolean isProySustAplica() {		
		return proySustAplica;
	}

	public void setProySustAplica(boolean proySustAplica) {
		this.proySustAplica = proySustAplica;
	}	

	public void respuestaMontoSi(){
    	this.proyInversionPE = true;
    	
    	RequestContext.getCurrentInstance().reset("fMontos:pnlEtapas");
    	
    	inversionEtapas.setInversionPreparacion(null);
    	inversionEtapas.setInversionConstruccion(null);
    	inversionEtapas.setInversionOperacion(null);
    	inversionEtapas.setInversionAbandono(null);
    	inversionEtapas.setMedidasMitigacion(null);
    	
    	inversionEtapas.setEmpleosTemporalesPrep(null);
    	inversionEtapas.setEmpleosTemporalesCons(null);
    	inversionEtapas.setEmpleosTemporalesOper(null);
    	inversionEtapas.setEmpleosTemporalesAban(null);
    	
    	inversionEtapas.setEmpleosPermanentesPrep(null);
    	inversionEtapas.setEmpleosPermanentesCons(null);
    	inversionEtapas.setEmpleosPermanentesOper(null);
    	inversionEtapas.setEmpleosPermanentesAban(null); 
    	
    	// Limpia campos de tabla: Subsector Comunicaciones(7) y Rama Infraestructura Carretera (30)
    	limpiarTablaInfraestructuraCarretera();
    }
    
    public void respuestaMontoNo(){
    	this.proyInversionPE = false;
    	
    	RequestContext.getCurrentInstance().reset("fMontos:pnlMontos");
    	
    	proyecto.setProyInversionRequerida(null);
    	proyecto.setProyMedidasPrevencion(null);
    	proyecto.setProyEmpleosPermanentes(null);
    	proyecto.setProyEmpleosTemporales(null);
    	
		// Limpia campos de tabla: Subsector Comunicaciones(7) y Rama Infraestructura Carretera (30)
    	limpiarTablaInfraestructuraCarretera();
    }
    
    /**
     * Limpia valores de variables llenadas en tabla: Subsector Comunicaciones(7) y Rama Infraestructura Carretera (30)
     */
    public void limpiarTablaInfraestructuraCarretera()
    {
    	if (NSubSector==7 && proyecto.getNrama()==30) {
    		RequestContext.getCurrentInstance().reset("fMontos:origenPnl");
        	proyecto.setProyInversionFederalOri(null);
        	proyecto.setProyInversionEstatalOri(null);
        	proyecto.setProyInversionMunicipalOri(null);
        	proyecto.setProyInversionPrivadaOri(null);
    	}
    }
    
	/**
	 * @return the proyInversionPE
	 */
	public boolean isProyInversionPE() {
		return proyInversionPE;
	}

	/**
	 * @param proyInversionPE the proyInversionPE to set
	 */
	public void setProyInversionPE(boolean proyInversionPE) {
		this.proyInversionPE = proyInversionPE;
	}

	public List<SustanciaU> getSustanciasListSelecciones() {
		return sustanciasListSelecciones;
	}

	public void setSustanciasListSelecciones(List<SustanciaU> sustanciasListSelecciones) {
		this.sustanciasListSelecciones = sustanciasListSelecciones;
	}
    
    

	/**
	 * @return the editarServicio
	 */
	public ServicioProyecto getEditarServicio() {
		return editarServicio;
	}

	/**
	 * @param editarServicio the editarServicio to set
	 */
	public void setEditarServicio(ServicioProyecto editarServicio) {
		this.editarServicio = editarServicio;
	}

	public List<AccidentesProyecto> getSelectedAccidentes() {
		return selectedAccidentes;
	}

	public void setSelectedAccidentes(List<AccidentesProyecto> selectedAccidentes) {
		this.selectedAccidentes = selectedAccidentes;
	}

	public AccidentesProyecto getAccidente() {
		return accidente;
	}

	public void setAccidente(AccidentesProyecto accidente) {
		this.accidente = accidente;
	}

	public AccidentesProyecto getEditAccidente() {
		return editAccidente;
	}

	public void setEditAccidente(AccidentesProyecto editAccidente) {
		this.editAccidente = editAccidente;
	}

	public CatEtapa getSelectedEtapa() {
		return selectedEtapa;
	}

	public void setSelectedEtapa(CatEtapa selectedEtapa) {
		this.selectedEtapa = selectedEtapa;
	}

	public CatSustanciaAltamRiesgosa getSustanciariesgosaId() {
		return sustanciariesgosaId;
	}

	public void setSustanciariesgosaId(CatSustanciaAltamRiesgosa sustanciariesgosaId) {
		this.sustanciariesgosaId = sustanciariesgosaId;
	}

	public BigDecimal getSustanciaCantidadAlmacenada() {
		return sustanciaCantidadAlmacenada;
	}

	public void setSustanciaCantidadAlmacenada(BigDecimal sustanciaCantidadAlmacenada) {
		this.sustanciaCantidadAlmacenada = sustanciaCantidadAlmacenada;
	}

	/***
	 * clave de la unidad seleccionada
	 * @return
	 */
	public short getCtunClve() {
		return ctunClve;
	}

	/***
	 * clave de la unidad seleccionada
	 * @param ctunClve
	 */
	public void setCtunClve(short ctunClve) {
		this.ctunClve = ctunClve;
	}
	
	/***
	 * descripcion de la unidad seleccionada
	 * @return
	 */
	public String getCtunDesc() {
		return ctunDesc;
	}

	/***
	 * descripcion de la unidad seleccionada
	 * @param ctunDesc
	 */
	public void setCtunDesc(String ctunDesc) {
		this.ctunDesc = ctunDesc;
	}

	public SustanciaU getEditSustancia() {
		return editSustancia;
	}

	public void setEditSustancia(SustanciaU editSustancia) {
		this.editSustancia = editSustancia;
	}
	public String getSustanciaPromovente() {
		return sustanciaPromovente;
	}
	public void setSustanciaPromovente(String sustanciaPromovente) {
		this.sustanciaPromovente = sustanciaPromovente;
	}

	/***
	 * indica si la sustancia actual excede la cantidad
	 * @return
	 */
	public String getSustanciaexcede() {
		return sustanciaexcede;
	}

	/***
	 * indica si la sustancia actual excede la cantidad
	 * @param sustanciaexcede
	 */
	public void setSustanciaexcede(String sustanciaexcede) {
		this.sustanciaexcede = sustanciaexcede;
	}

	/**
	 * @return the inversionE
	 */
	public List<InversionEtapas> getInversionE() {
		return inversionE;
	}

	/**
	 * @param inversionE the inversionE to set
	 */
	public void setInversionE(List<InversionEtapas> inversionE) {
		this.inversionE = inversionE;
	}

	/**
	 * @return the inversionEtapas
	 */
	public InversionEtapas getInversionEtapas() {
		return inversionEtapas;
	}

	/**
	 * @param inversionEtapas the inversionEtapas to set
	 */
	public void setInversionEtapas(InversionEtapas inversionEtapas) {
		this.inversionEtapas = inversionEtapas;
	}

	public List<SelectItem> getCatClasificacionB() {
//        if (catClasificacionB.isEmpty()) {
//            List<CatClasificacionB> n = miaDao.getCatClasificacionB();
//            for (CatClasificacionB c : n) {
//                SelectItem s = new SelectItem(c, c.getClasificacionB());
//                catClasificacionB.add(s);
//            }
//        }
		return catClasificacionB;
	}

	public void setCatClasificacionB(List<SelectItem> catClasificacionB) {
		this.catClasificacionB = catClasificacionB;
	}

	public ArchivosProyecto getArchivoTemporal() {
		return archivoTemporal;
	}

	public void setArchivoTemporal(ArchivosProyecto archivoTemporal) {
		this.archivoTemporal = archivoTemporal;
	}

	public CargaBean getCargaBean() {
		return cargaBean;
	}

	public void setCargaBean(CargaBean cargaBean) {
		this.cargaBean = cargaBean;
	}

	/***
	 * primeraCarga="1" indica que apenas se cargo la pantalla de sustancias, y no se ha dado click alguno en la pantlla
	 * @return
	 */
	public String getPrimeraCarga() {
		return primeraCarga;
	}

	/***
	 * primeraCarga="1" indica que apenas se cargo la pantalla de sustancias, y no se ha dado click alguno en la pantlla
	 * @param primeraCarga
	 */
	public void setPrimeraCarga(String primeraCarga) {
		this.primeraCarga = primeraCarga;
	}

	public String getHeaderText() {
		return headerText;
	}

	public void setHeaderText(String headerText) {
		this.headerText = headerText;
	}

	
	
	
	public void changeClasificacion(ValueChangeEvent event) {
		System.out.println(event);
		CatClasificacion cc =  (CatClasificacion) event.getNewValue();
		catClasificacionB = new ArrayList<SelectItem>();
            List<CatClasificacionB> n = miaDao.getCatClasificacionBById(cc.getClasificacionId());
            for (CatClasificacionB c : n) {
                SelectItem s = new SelectItem(c, c.getClasificacionB());
                catClasificacionB.add(s);
            }
            setCatClasificacionB(catClasificacionB);
	}

	/**
	 * @return the rootSuelos
	 */
	public TreeNode getRootSuelos() {
		return rootSuelos;
	}

	/**
	 * @param rootSuelos the rootSuelos to set
	 */
	public void setRootSuelos(TreeNode rootSuelos) {
		this.rootSuelos = rootSuelos;
	}

	/**
	 * @return the listaVegGrupos
	 */
	public List<VegGrupo> getListaVegGrupos() {
		return listaVegGrupos;
	}

	/**
	 * @param listaVegGrupos the listaVegGrupos to set
	 */
	public void setListaVegGrupos(List<VegGrupo> listaVegGrupos) {
		this.listaVegGrupos = listaVegGrupos;
	}

	/**
	 * @return the sueloVegetacionDetalleSeleccionado
	 */
	public SueloVegetacionDetalle getSueloVegetacionDetalleSeleccionado() {
		return sueloVegetacionDetalleSeleccionado;
	}

	/**
	 * @param sueloVegetacionDetalleSeleccionado the sueloVegetacionDetalleSeleccionado to set
	 */
	public void setSueloVegetacionDetalleSeleccionado(
			SueloVegetacionDetalle sueloVegetacionDetalleSeleccionado) {
		this.sueloVegetacionDetalleSeleccionado = sueloVegetacionDetalleSeleccionado;
	}

	/**
	 * @return the headerTextDetalle
	 */
	public String getHeaderTextDetalle() {
		return headerTextDetalle;
	}

	/**
	 * @param headerTextDetalle the headerTextDetalle to set
	 */
	public void setHeaderTextDetalle(String headerTextDetalle) {
		this.headerTextDetalle = headerTextDetalle;
	}

	/**
	 * @return the alertTextDetalle
	 */
	public String getAlertTextDetalle() {
		return alertTextDetalle;
	}

	/**
	 * @param alertTextDetalle the alertTextDetalle to set
	 */
	public void setAlertTextDetalle(String alertTextDetalle) {
		this.alertTextDetalle = alertTextDetalle;
	}

	/**
	 * @return the idVegGrupo
	 */
	public int getIdVegGrupo() {
		return idVegGrupo;
	}

	/**
	 * @param idVegGrupo the idVegGrupo to set
	 */
	public void setIdVegGrupo(int idVegGrupo) {
		this.idVegGrupo = idVegGrupo;
	}

	/**
	 * @return the idVegTipo
	 */
	public int getIdVegTipo() {
		return idVegTipo;
	}

	/**
	 * @param idVegTipo the idVegTipo to set
	 */
	public void setIdVegTipo(int idVegTipo) {
		this.idVegTipo = idVegTipo;
	}

	/**
	 * @return the idVegFase
	 */
	public int getIdVegFase() {
		return idVegFase;
	}

	/**
	 * @param idVegFase the idVegFase to set
	 */
	public void setIdVegFase(int idVegFase) {
		this.idVegFase = idVegFase;
	}
	
	public void crearSustancia(ActionEvent evt) {
		
		System.out.println("Crear sustancia");
		RequestContext.getCurrentInstance().reset("fAgregar:pnlAgregar");		
				
		selectedEtapa = null;
        sustanciariesgosaId = null;
        sustanciaCantidadAlmacenada = BigDecimal.ZERO;
        ctunClve = 0;
        ctunDesc = "";
        sustanciaPromovente = "";
        // sustanciaexcede = "0";
        
		RequestContext.getCurrentInstance().execute("modalAgregar.show();");		
	}

	/**
	 * @return the limiteSuperficie
	 */
	public double getLimiteSuperficie() {
		return limiteSuperficie;
	}

	/**
	 * @param limiteSuperficie the limiteSuperficie to set
	 */
	public void setLimiteSuperficie(double limiteSuperficie) {
		this.limiteSuperficie = limiteSuperficie;
	}

	/**
	 * @return the superficieAcumulada
	 */
	public double getSuperficieAcumulada() {
		return superficieAcumulada;
	}

	/**
	 * @param superficieAcumulada the superficieAcumulada to set
	 */
	public void setSuperficieAcumulada(double superficieAcumulada) {
		this.superficieAcumulada = superficieAcumulada;
	}

	/**
	 * @return the totalSuperficie
	 */
	public double getTotalSuperficie() {
		return totalSuperficie;
	}

	/**
	 * @param totalSuperficie the totalSuperficie to set
	 */
	public void setTotalSuperficie(double totalSuperficie) {
		this.totalSuperficie = totalSuperficie;
	}

	/**
	 * @return the superficieIngresada
	 */
	public double getSuperficieIngresada() {
		return superficieIngresada;
	}

	/**
	 * @param superficieIngresada the superficieIngresada to set
	 */
	public void setSuperficieIngresada(double superficieIngresada) {
		this.superficieIngresada = superficieIngresada;
	}

	/**
	 * @return the edicionDetalleSuelo
	 */
	public boolean isEdicionDetalleSuelo() {
		return edicionDetalleSuelo;
	}

	/**
	 * @param edicionDetalleSuelo the edicionDetalleSuelo to set
	 */
	public void setEdicionDetalleSuelo(boolean edicionDetalleSuelo) {
		this.edicionDetalleSuelo = edicionDetalleSuelo;
	}

	public short getIdSeleccionadoServicio() {
		return idSeleccionadoServicio;
	}

	public void setIdSeleccionadoServicio(short idSeleccionadoServicio) {
		this.idSeleccionadoServicio = idSeleccionadoServicio;
	}

	public int getIdSueloVegetacionSelected() {
		return idSueloVegetacionSelected;
	}

	public void setIdSueloVegetacionSelected(int idSueloVegetacionSelected) {
		this.idSueloVegetacionSelected = idSueloVegetacionSelected;
	}

	public SueloVegetacionProyecto getEditTipoVegetacion() {
		return editTipoVegetacion;
	}

	public void setEditTipoVegetacion(SueloVegetacionProyecto editTipoVegetacion) {
		this.editTipoVegetacion = editTipoVegetacion;
	}

	public String getMsj() {
		return msj;
	}

	public void setMsj(String msj) {
		this.msj = msj;
	}

	public boolean isBandera() {
		return bandera;
	}

	public void setBandera(boolean bandera) {
		this.bandera = bandera;
	}
	
	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla Seleccion de Criterio
	 */
    public int getRowIndexSelSitEditar() {
		return rowIndexSelSitEditar;
	}

    /**
     * Asigna valor a variable que guarda Numero secuencial de Criterio a editar en Seleccion del sitio
     * @param rowIndexSelSitEditar
     */
	public void setRowIndexSelSitEditar(int rowIndexSelSitEditar) {
		this.rowIndexSelSitEditar = rowIndexSelSitEditar;
	}
	
	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla Servicios Requeridos
	 */
	public int getRowIndexServReqEditar() {
		return rowIndexServReqEditar;
	}

	/**
	 * Asigna valor a variable que guarda Numero secuencial de Servicio requerido a editar
	 * @param rowIndexServReqEditar
	 */
	public void setRowIndexServReqEditar(int rowIndexServReqEditar) {
		this.rowIndexServReqEditar = rowIndexServReqEditar;
	}
	
	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla Uso de suelo de predios colindantes
	 */
	public int getRowIndexUSPCEditar() {
		return rowIndexUSPCEditar;
	}

	/**
	 * Asigna valor a variable que guarda Numero secuencial de Uso de suelo de predios colindantes a editar
	 * @param rowIndexUSPCEditar
	 */
	public void setRowIndexUSPCEditar(int rowIndexUSPCEditar) {
		this.rowIndexUSPCEditar = rowIndexUSPCEditar;
	}
	
	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla Sustancias Riesgosas
	 */
	public int getRowIndexSustREditar() {
		return rowIndexSustREditar;
	}

	/**
	 * Asigna valor a variable que guarda Numero secuencial de Sustancia riesgosa a editar
	 * @param rowIndexSustREditar
	 */
	public void setRowIndexSustREditar(int rowIndexSustREditar) {
		this.rowIndexSustREditar = rowIndexSustREditar;
	}
	
	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla Posibles accidentes ambientales
	 */
	public long getRowIndexPosAccAmbEditar() {
		return rowIndexPosAccAmbEditar;
	}

	/**
	 * Asigna valor a variable que guarda Numero secuencial de Posible accidente ambiental a editar
	 * @param rowIndexPosAccAmbEditar
	 */
	public void setRowIndexPosAccAmbEditar(long rowIndexPosAccAmbEditar) {
		this.rowIndexPosAccAmbEditar = rowIndexPosAccAmbEditar;
	}	

	/**
	 * @return Indica si la página web sobre la que está operando es: Inversión requerida 
	 */
	public String getPageInversionReq() {
		return pageInversionReq;
	}

	/**
	 * Asigna valor a variable que indica si la página web sobre la que se está operando es: Inversión requerida
	 * @param pageInversionReq the pageInversionReq to set
	 */
	public void setPageInversionReq(String pageInversionReq) {
		this.pageInversionReq = pageInversionReq;
	}
	
}