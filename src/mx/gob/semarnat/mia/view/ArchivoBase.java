/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;


public class ArchivoBase extends Cap{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2333943556689696631L;
	protected HttpServletResponse response;
	
	protected void processRequest(String myneType, String responseHeaderType, String pathFile, String fileName, String extension)
            throws ServletException, IOException {
    	response = ((HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse());
	    response.setContentType("text/html;charset=UTF-8");
	    
	    ServletOutputStream  outs =  response.getOutputStream ();
	    //---------------------------------------------------------------
	    // Set the output data's mime type
	    //---------------------------------------------------------------
	    response.setContentType( myneType );  // MIME type for pdf doc
	    //---------------------------------------------------------------
	    // create an input stream from fileURL
	    //---------------------------------------------------------------	
	    File file=new File(pathFile);
	    
	    //------------------------------------------------------------
	    // Content-disposition header - don't open in browser and
	    // set the "Save As..." filename.
	    // *There is reportedly a bug in IE4.0 which  ignores this...
	    //------------------------------------------------------------	    
	    response.setHeader("Content-disposition", responseHeaderType + "; filename=\"" + fileName + "." + extension+"\"");
	
	    BufferedInputStream  bis = null; 
	    BufferedOutputStream bos = null;
	    try {
	
	        InputStream isr=new FileInputStream(file);
	        bis = new BufferedInputStream(isr);
	        bos = new BufferedOutputStream(outs);
	        byte[] buff = new byte[2048];
	        int bytesRead;
	        // Simple read/write loop.
	        while(-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
	            bos.write(buff, 0, bytesRead);
	        }
	    } 
	    catch(Exception e)
	    {
	        System.out.println("Exception ----- Message ---"+e);
	    } finally {
	        if (bis != null)
	            bis.close();
	        if (bos != null)
	            bos.close();
	    }
    }    
    
	
 }
