package mx.gob.semarnat.mia.view;

import java.util.List;
import mx.gob.semarnat.mia.dao.MiaDao;
import mx.gob.semarnat.mia.model.ComentarioProyecto;

/**
 * @author eescalona
 * 
 */
public class CapitulosComentarios {
    //eescalona    
    private List<ComentarioProyecto> lstComentProy;
    private String ubicActual;
    private String strInfoAdicional;
    private short serialInfo;
    
    private Short capitulo;
    private String folioProyecto;
    private Short serialProyecto;
    
    protected final MiaDao miaDao = new MiaDao();

    public CapitulosComentarios() {
        this.capitulo = null;
        this.folioProyecto = null;
        this.serialProyecto = null;
        
    }
    
    public void setProyectoInfo(short capitulo, String folioProyecto, short serialProyecto) {
        this.capitulo = capitulo;
        this.folioProyecto = folioProyecto;
        this.serialProyecto = serialProyecto;

        //Comentarios proyecto - eescalona
        int iFolio = Integer.parseInt(this.folioProyecto);
        this.lstComentProy = miaDao.getComentarioProy(iFolio, this.capitulo);
//        this.serialInfo = (short)3;
        this.serialInfo = this.serialProyecto;
        System.out.println("lstComentProy.size() --> " + this.lstComentProy.size());
        
    }
    
    public void guardarComentarios() {
        //Guardado de componentes en información adicional - eescalona
        miaDao.guardarSeccionModificada(this.folioProyecto, this.capitulo, strInfoAdicional);
        
    }
    
    /**
     * Método que obtiene el serial del proyecto y la cadena de identificadores si existen seriales
     * @return <string>serial|subcapitulo|seccion|apartado</string>
     * @author eescalona
     */
    public String getStrComentProy() {
        String cadena = "";
        int idx = 0, valorInfo;
        boolean bInfoAdic, bInfoOpc;
        
        for (ComentarioProyecto ele : lstComentProy){
            /**
             * 1 - Tiene Información adicional
             * 2 - Tiene Sección modificada (información opcional)
             * 3 - Tiene ambas
             */
            valorInfo = 0;
            bInfoAdic = false;
            bInfoOpc = false;
            if ( ele.getInfoAdicional() != null && !ele.getInfoAdicional().toString().isEmpty() ) {
                bInfoAdic = true;
            }
            if ( ele.getSecModificada() != null && !ele.getSecModificada().toString().isEmpty() ) {
                bInfoOpc = true;
            }
            
            if (bInfoAdic && bInfoOpc) {
                valorInfo = 3;
            } else if (bInfoOpc) {
                valorInfo = 2;
            } else if (bInfoAdic) {
                valorInfo = 1;
            } else {
                valorInfo = 0;
            }
            
            cadena += valorInfo + "_" +
                    ele.getComentarioProyectoPK().getSubcapituloId() + "-" +
                    ele.getComentarioProyectoPK().getSeccionId() + "-" +
                    ele.getComentarioProyectoPK().getApartadoId();
            
            idx++;
            if (idx < lstComentProy.size()) {
                cadena += "|";
            }
        }
        return Short.toString(this.serialInfo) + "|" + cadena;//Se agrega en el objeto el serial del proyecto
    }
    
    /** N/A
     * Método que determina si un capitulo tiene información adicional
     * @param subCapitulo
     * @param seccion
     * @param apartado
     * @return <boolean>Tiene información adicional</boolean>
     * 
     * @author eescalona
     */
    public boolean existeInfoAdicional(short subCapitulo, short seccion, short apartado) {
        boolean bRes = false;
        for (ComentarioProyecto comentario : this.lstComentProy) {
            if (comentario.getComentarioProyectoPK().getSubcapituloId() == subCapitulo &&
                    comentario.getComentarioProyectoPK().getSeccionId() == seccion &&
                    comentario.getComentarioProyectoPK().getApartadoId() == apartado) {
                bRes = true;
            }
        }
        
        return bRes;
    }	
    
	//Accesores - eescalona
    public List<ComentarioProyecto> getLstComentProy() {
        return lstComentProy;
    }
    public String getUbicActual() {
        return ubicActual;
    }
    public void setUbicActual(String ubicActual) {
        this.ubicActual = ubicActual;
    }	
    public String getStrInfoAdicional() {
        return strInfoAdicional;
    }
    public void setStrInfoAdicional(String strInfoAdicional) {        
        System.out.println("strInfoAdicional --> " + strInfoAdicional);
        this.strInfoAdicional = strInfoAdicional;
    }    
    
}
