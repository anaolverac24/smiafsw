package mx.gob.semarnat.mia.view;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import mx.gob.semarnat.mia.view.capitulo1.SelectItemsBaseConverter;
import mx.gob.semarnat.mia.view.to.CatEspeciesAcuiferosTO;

/**
 * The Class PersonaConverter.
 */
@ManagedBean (name="especiesAcuiferosConverter")
public class EspeciesAcuiferosConverter extends SelectItemsBaseConverter {
    
    /* (non-Javadoc)
     * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof CatEspeciesAcuiferosTO) || ((CatEspeciesAcuiferosTO) value).getId() == null) {
            return null;
        }

        return String.valueOf(((CatEspeciesAcuiferosTO) value).getId());
    }
}
