/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import mx.gob.semarnat.mia.model.EstudioRiesgoProyecto;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.catalogos.SectorProyecto;
import mx.gob.semarnat.mia.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.mia.util.Log;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Ramon Hidalgo
 */
@ManagedBean
@ViewScoped
public class EstudioRiesgoView extends CapitulosComentarios implements Serializable {

    //private MiaDao miaDao = new MiaDao();
    private Proyecto proyecto = new Proyecto();
    private String sector = "";
    private EstudioRiesgoProyecto estudio = new EstudioRiesgoProyecto();
    private Integer claveTramite = 0;
    private short subsec = 0;
    
    
    public EstudioRiesgoView() {
        // Recupera proyecto de sesion
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
        claveTramite =  (Integer) context.getExternalContext().getSessionMap().get("claveTramite");
        subsec = (Short) context.getExternalContext().getSessionMap().get("subsector");
        //Cargar Proyecto
        try {
            this.proyecto = miaDao.proyecto(folioProyecto, serialProyecto);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }

        if (proyecto == null) {
            this.proyecto = new Proyecto(folioProyecto, serialProyecto);
        }

        System.out.println("Folio " + folioProyecto);
        System.out.println("Serial " + serialProyecto);

        if (proyecto.getNsub() != null) {
            SubsectorProyecto su = miaDao.getSubSector(proyecto.getNsub());
            SectorProyecto sp = miaDao.getSectorProyecto(su.getNsec());
            sector = sp.getSector();
        }


        estudio = miaDao.cargaEstudioRiesgo(proyecto);
        
        //Comentarios proyecto - eescalona
        super.setProyectoInfo((short)10, folioProyecto, serialProyecto);


    }

    public void guardar() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        try {
            miaDao.guardar(proyecto);
            miaDao.merge(estudio);
            reqcontEnv.execute("parent.actMenu();");
            reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
            
            //<editor-fold desc="Datos del Responsable del Estudio de Riesgo" defaultstate="collapsed">
            if (estudio.getEstudioRfcRpte() != null){ //ESTUDIO_RFC_RPTE
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                try {
                    miaDao.guardarAvance(proyecto, new Short("6"), "101");
                    System.out.println("guarda avance capitulo: 101");
                } catch (Exception e) {
                    System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                }
                //</editor-fold>
            }                
            //</editor-fold>
            
            //<editor-fold desc="Escenario de los Riesgos Ambientales" defaultstate="collapsed">
            if (estudio.getEstudioDescBasesDise() != null || estudio.getEstudioProyectoCivil() != null || estudio.getEstudioProyectoMecanico() != null || estudio.getEstudioProyectoIncendio() != null 
                    || estudio.getEstudioDetalldaProceso() != null || estudio.getEstudioAlmacenamiento() != null || estudio.getEstudioProcesoAuxiliar() != null || estudio.getEstudioPruebasVerificacion() != null
                    || estudio.getEstudioOperacion() != null || estudio.getEstudioCuartoControl() != null || estudio.getEstudioSistemaAislamiento() != null || estudio.getEstudioAntecedentesAcciInci() != null
                    || estudio.getEstudioMetodologiaIdenJerar() != null){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                try {
                    miaDao.guardarAvance(proyecto, new Short("4"), "102");
                    System.out.println("guarda avance capitulo: 102");
                } catch (Exception e) {
                    System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                }
                //</editor-fold>
            }                
            //</editor-fold>
            
            //<editor-fold desc="Descripción de las Medidas de Protección en Torno a las Instalaciones" defaultstate="collapsed">
            if (estudio.getEstudioRadiosPotenciales() != null || estudio.getEstudioInteraccionesRiesgo() != null || estudio.getEstudioEfectosAreaInflu()!= null){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                try {
                    miaDao.guardarAvance(proyecto, new Short("4"), "103");
                    System.out.println("guarda avance capitulo: 103");
                } catch (Exception e) {
                    System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                }
                //</editor-fold>
            }                
            //</editor-fold>                        
            
            //<editor-fold desc="Señalamiento de las Medidas de Seguridad y Preventivas en Materia Ambiental" defaultstate="collapsed">
            if (estudio.getEstudioRecomendacionesTecOp() != null || estudio.getEstudioSistemasSeguridad() != null || estudio.getEstudioMedidasPreventivas()!= null){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                try {
                    miaDao.guardarAvance(proyecto, new Short("4"), "104");
                    System.out.println("guarda avance capitulo: 104");
                } catch (Exception e) {
                    System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                }
                //</editor-fold>
            }                
            //</editor-fold>  
            
            //<editor-fold desc="Resumen" defaultstate="collapsed">
            if (estudio.getEstudioConclusionesERA() != null || estudio.getEstudioResumenSituacion() != null || estudio.getEstudioInformeTecnico()!= null){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                try {
                    miaDao.guardarAvance(proyecto, new Short("4"), "105");
                    System.out.println("guarda avance capitulo: 105");
                } catch (Exception e) {
                    System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                }
                //</editor-fold>
            }                
            //</editor-fold> 
            
            if (Integer.parseInt(miaDao.avanceProyecto(proyecto)) == 99) {
                reqcontEnv.execute("alert('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.')");
            }
            
            //Guardado de componentes en información adicional - eescalona
            super.guardarComentarios();

        } catch (Exception e) {
            e.printStackTrace();
            Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e.getMessage());
            reqcontEnv.execute("alert('No se ha podido guardar la información, Intente mas tarde.')");
        }
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the sector
     */
    public String getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(String sector) {
        this.sector = sector;
    }

    /**
     * @return the estudio
     */
    public EstudioRiesgoProyecto getEstudio() {
        return estudio;
    }

    /**
     * @param estudio the estudio to set
     */
    public void setEstudio(EstudioRiesgoProyecto estudio) {
        this.estudio = estudio;
    }

    public Integer getClaveTramite() {
        return claveTramite;
    }

    public void setClaveTramite(Integer claveTramite) {
        this.claveTramite = claveTramite;
    }

    public short getSubsec() {
        return subsec;
    }

    public void setSubsec(short subsec) {
        this.subsec = subsec;
    }


}
