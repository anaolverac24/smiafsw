/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.catalogos.SectorProyecto;
import mx.gob.semarnat.mia.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.mia.util.Log;
import mx.gob.semarnat.mia.validacion.Validacion;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Ramon Hidalgo
 */
@ManagedBean
@ViewScoped
public class Capitulo7View extends CapitulosComentarios implements Serializable {
    private String seccion;

    //private MiaDao miaDao = new MiaDao();
    private Proyecto proyecto = new Proyecto();
    private String sector = "";
    private Integer claveTramite = 0;
    private short subsec = 0;
    
    public Capitulo7View() {
        // Recupera proyecto de sesion
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
        claveTramite =  (Integer) context.getExternalContext().getSessionMap().get("claveTramite");
        subsec = (Short) context.getExternalContext().getSessionMap().get("subsector");
        //Cargar Proyecto
        try {
            this.proyecto = miaDao.proyecto(folioProyecto, serialProyecto);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }

        if (proyecto == null) {
            this.proyecto = new Proyecto(folioProyecto, serialProyecto);
        }

        System.out.println("Folio " + folioProyecto);
        System.out.println("Serial " + serialProyecto);

        if (proyecto.getNsub() != null) {
            SubsectorProyecto su = miaDao.getSubSector(proyecto.getNsub());
            SectorProyecto sp = miaDao.getSectorProyecto(su.getNsec());
            sector = sp.getSector();
        }
        
        //Comentarios proyecto - eescalona
        super.setProyectoInfo((short)7, folioProyecto, serialProyecto);

    }

    public void guardar() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        try {
            miaDao.guardar(proyecto);
            reqcontEnv.execute("parent.actMenu();");
            //reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
            reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
            
            if(proyecto.getEsceConProy()!=null
                    && proyecto.getEsceSinProy()!=null
                    && proyecto.getEsceConProyMed()!=null
                    && proyecto.getPronosAmb()!=null){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId() != null && proyecto.getEstudioRiesgoId()==1)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("3"), "71");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                } else {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("6"), "71");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            
            if (proyecto.getConclusionProy() != null
                    && proyecto.getEvaAlternativas() != null) {
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                 if(proyecto.getEstudioRiesgoId() != null && proyecto.getEstudioRiesgoId()==1)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("3"), "72");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                } else {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("5"), "72");

                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                 }
                //</editor-fold>
            }    
            
            if (Integer.parseInt(miaDao.avanceProyecto(proyecto)) == 99) {
                //reqcontEnv.execute("alert('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.')");
            	reqcontEnv.execute("mensajes('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.', 'success')");
            }
          
                           try {
                Validacion v = new Validacion();
                String msg = v.validar(seccion, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                if (!msg.isEmpty()) {
                    return;
                }
            } catch (Exception err) {
                err.printStackTrace();
            }

            //Guardado de componentes en información adicional - eescalona
            super.guardarComentarios();

        } catch (Exception e) {
            Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e.getMessage());
            e.printStackTrace();
            //reqcontEnv.execute("alert('No se ha podido guardar la información, Intente mas tarde.')");
            reqcontEnv.execute("mensajes('No se ha podido guardar la información, Intente más tarde.', 'danger')");
        }
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the sector
     */
    public String getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(String sector) {
        this.sector = sector;
    }

    public short getSubsec() {
        return subsec;
    }

    public void setSubsec(short subsec) {
        this.subsec = subsec;
    }

    /**
     * @return the claveTramite
     */
    public Integer getClaveTramite() {
        return claveTramite;
    }

    /**
     * @param claveTramite the claveTramite to set
     */
    public void setClaveTramite(Integer claveTramite) {
        this.claveTramite = claveTramite;
    }

    /**
     * @return the seccion
     */
    public String getSeccion() {
        return seccion;
    }

    /**
     * @param seccion the seccion to set
     */
    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

}
