/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import mx.gob.semarnat.mia.model.CatEtapa;
import mx.gob.semarnat.mia.model.ImpacAmbProyecto;
import mx.gob.semarnat.mia.model.MedPrevImpactProy;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.catalogos.SectorProyecto;
import mx.gob.semarnat.mia.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.mia.util.Log;
import mx.gob.semarnat.mia.validacion.Validacion;

import org.primefaces.context.RequestContext;

/**
 *
 * @author Ramon Hidalgo
 */
@ManagedBean
@ViewScoped
public class Capitulo6View extends CapitulosComentarios implements Serializable {
    private String seccion;

    //private MiaDao miaDao = new MiaDao();
    private Proyecto proyecto = new Proyecto();
    private String sector = "";
    private Integer claveTramite = 0;
    private short subsec = 0;
    private List<MedPrevImpactProy> medPreventivas = new ArrayList<MedPrevImpactProy>();
    private List<MedPrevImpactProy> medPreventivasPS = new ArrayList<MedPrevImpactProy>();
    private List<MedPrevImpactProy> medPreventivasC = new ArrayList<MedPrevImpactProy>();
    private List<MedPrevImpactProy> medPreventivasOM = new ArrayList<MedPrevImpactProy>();
    private List<MedPrevImpactProy> medPreventivasA = new ArrayList<MedPrevImpactProy>();
    private MedPrevImpactProy editMedidaPreventiva = new MedPrevImpactProy();
    
    private List<SelectItem> comboImpacto = new ArrayList<SelectItem>();
    
    /**
     * Numero secuencial del registro a editar en cada etapa 
     *  de la pantalla "Medidas preventivas y de mitigación de los impactos ambientales"
     */
    private int rowIndexMedPrevMit;
    

	public Capitulo6View() {
        // Recupera proyecto de sesion
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
        claveTramite = (Integer) context.getExternalContext().getSessionMap().get("claveTramite");
        subsec = (Short) context.getExternalContext().getSessionMap().get("subsector");
        //Cargar Proyecto
        try {
            this.proyecto = miaDao.proyecto(folioProyecto, serialProyecto);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }

        if (proyecto == null) {
            this.proyecto = new Proyecto(folioProyecto, serialProyecto);
        }

        System.out.println("Folio " + folioProyecto);
        System.out.println("Serial " + serialProyecto);

        if (proyecto.getNsub() != null) {
            SubsectorProyecto su = miaDao.getSubSector(proyecto.getNsub());
            SectorProyecto sp = miaDao.getSectorProyecto(su.getNsec());
            sector = sp.getSector();
        }

        List<String> comboImpactoTmp = miaDao.getImpactos(folioProyecto, serialProyecto);
        for (String s:comboImpactoTmp){
            SelectItem st = new SelectItem(s, s);
            comboImpacto.add(st);
        }
        medPreventivas = miaDao.getMedPrevImpactProy(folioProyecto, serialProyecto);
        iniciarListasEtapa();
        
        //Comentarios proyecto - eescalona
        super.setProyectoInfo((short)6, folioProyecto, serialProyecto);
        
    }
    
    /**
     * Metodo que se encarga de Cargar listas de Medidas de Prevencion de Impacto por etapa
     */
    public void iniciarListasEtapa(){
        medPreventivasPS = new ArrayList<MedPrevImpactProy>();
        medPreventivasC = new ArrayList<MedPrevImpactProy>();
        medPreventivasOM = new ArrayList<MedPrevImpactProy>();
        medPreventivasA = new ArrayList<MedPrevImpactProy>();
        for (MedPrevImpactProy medPrevImpactProy : medPreventivas) {
			if (medPrevImpactProy.getEtapaId().getEtapaId() == 1) {
				medPreventivasPS.add(medPrevImpactProy);
			}
			if (medPrevImpactProy.getEtapaId().getEtapaId() == 2) {
				medPreventivasC.add(medPrevImpactProy);
			}
			if (medPrevImpactProy.getEtapaId().getEtapaId() == 3) {
				medPreventivasOM.add(medPrevImpactProy);
			}
			if (medPrevImpactProy.getEtapaId().getEtapaId() == 4) {
				medPreventivasA.add(medPrevImpactProy);
			}
        	
		}
//        medPreventivasPS = miaDao.getMedPrevImpactProyByEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 1);
//        medPreventivasC = miaDao.getMedPrevImpactProyByEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 2);
//        medPreventivasOM = miaDao.getMedPrevImpactProyByEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 3);
//        medPreventivasA = miaDao.getMedPrevImpactProyByEtapa(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) 4);
    }
    
    /**
     * Metodo que se encarga de obtener un registro de Medida Preventiva
     */
    public void consultarMed(){
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idMedida = params.get("id").toString();
		id = Integer.parseInt(idMedida);
		System.out.println("Registro de Medida Preventiva: "+id);
		editMedidaPreventiva = new MedPrevImpactProy();
		editMedidaPreventiva = miaDao.getMedPrevImpactProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) id);
		
		// Numero secuencial a mostrar en "Editar registro #"
		rowIndexMedPrevMit = Integer.parseInt(params.get("prowindex").toString());    	
    }
    
    public void editarMed(){
    	MedPrevImpactProy medPreventiva = new MedPrevImpactProy();
    	medPreventiva = editMedidaPreventiva;
    	try {
			miaDao.persist(medPreventiva);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditar.hide();modalMensajeEditar.show()");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	medPreventivas = miaDao.getMedPrevImpactProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    	iniciarListasEtapa();
    }

    public void agregarMed() {
        Short id = miaDao.getMaxMedPrevImpactProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        MedPrevImpactProy m = new MedPrevImpactProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        m.setEtapaId(new CatEtapa((short)1));
        try {
            miaDao.merge(m);
        } catch (Exception e) {
            e.printStackTrace();
        }
        medPreventivas.add(m);
    }

    public void eliminarMed(ActionEvent evt) {
        Short id = (Short) evt.getComponent().getAttributes().get("idt");
        MedPrevImpactProy m = new MedPrevImpactProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        try {
            miaDao.eliminarMedPrevImpactProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        medPreventivas.remove(m);
    }

    public void guardar() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        try {
            miaDao.guardar(proyecto);
            
//            for (MedPrevImpactProy n: medPreventivas){
//                miaDao.merge(n);
//            }
            
            if(medPreventivas.size() >= 1){
                //<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
                if(proyecto.getEstudioRiesgoId()==1)
                {
                    try {
                        miaDao.guardarAvance(proyecto, new Short("6"), "6");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }else{
                    try {
                        miaDao.guardarAvance(proyecto, new Short("11"), "6");
                    } catch (Exception e) {
                        System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
                    }
                }
                //</editor-fold>
            }
            
                           try {
                Validacion v = new Validacion();
                String msg = v.validar(seccion, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
                if (!msg.isEmpty()) {
                    return;
                }
            } catch (Exception err) {
                err.printStackTrace();
            }

            reqcontEnv.execute("parent.actMenu();");
            //reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
            reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");

            if (Integer.parseInt(miaDao.avanceProyecto(proyecto)) == 99) {
                //reqcontEnv.execute("alert('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.')");
            	reqcontEnv.execute("mensajes('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.', 'success')");
            }
            
            //Guardado de componentes en información adicional - eescalona
            super.guardarComentarios();

        } catch (Exception e) { 
            Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e.getMessage());
            e.printStackTrace();
           // reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.')");
            reqcontEnv.execute("mensajes('No se ha podido guardar la información, Intente más tarde.', 'danger')");
        }
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the sector
     */
    public String getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(String sector) {
        this.sector = sector;
    }

    /**
     * @return the medPreventivas
     */
    public List<MedPrevImpactProy> getMedPreventivas() {
        return medPreventivas;
    }

    /**
     * @param medPreventivas the medPreventivas to set
     */
    public void setMedPreventivas(List<MedPrevImpactProy> medPreventivas) {
        this.medPreventivas = medPreventivas;
    }

    public Integer getClaveTramite() {
        return claveTramite;
    }

    public void setClaveTramite(Integer claveTramite) {
        this.claveTramite = claveTramite;
    }

    public short getSubsec() {
        return subsec;
    }

    public void setSubsec(short subsec) {
        this.subsec = subsec;
    }

    /**
     * @return the comboImpacto
     */
    public List<SelectItem> getComboImpacto() {
        return comboImpacto;
    }

    /**
     * @param comboImpacto the comboImpacto to set
     */
    public void setComboImpacto(List<SelectItem> comboImpacto) {
        this.comboImpacto = comboImpacto;
    }

    /**
     * @return the seccion
     */
    public String getSeccion() {
        return seccion;
    }

    /**
     * @param seccion the seccion to set
     */
    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

	public MedPrevImpactProy getEditMedidaPreventiva() {
		return editMedidaPreventiva;
	}

	public void setEditMedidaPreventiva(MedPrevImpactProy editMedidaPreventiva) {
		this.editMedidaPreventiva = editMedidaPreventiva;
	}

	public List<MedPrevImpactProy> getMedPreventivasPS() {
		return medPreventivasPS;
	}

	public void setMedPreventivasPS(List<MedPrevImpactProy> medPreventivasPS) {
		this.medPreventivasPS = medPreventivasPS;
	}

	public List<MedPrevImpactProy> getMedPreventivasC() {
		return medPreventivasC;
	}

	public void setMedPreventivasC(List<MedPrevImpactProy> medPreventivasC) {
		this.medPreventivasC = medPreventivasC;
	}

	public List<MedPrevImpactProy> getMedPreventivasOM() {
		return medPreventivasOM;
	}

	public void setMedPreventivasOM(List<MedPrevImpactProy> medPreventivasOM) {
		this.medPreventivasOM = medPreventivasOM;
	}

	public List<MedPrevImpactProy> getMedPreventivasA() {
		return medPreventivasA;
	}

	public void setMedPreventivasA(List<MedPrevImpactProy> medPreventivasA) {
		this.medPreventivasA = medPreventivasA;
	}

	
	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla Selección de Criterio
	 */
	public int getRowIndexMedPrevMit() {
		return rowIndexMedPrevMit;
	}

	/**
	 * @param rowIndexMedPrevMit
	 */
	public void setRowIndexMedPrevMit(int rowIndexMedPrevMit) {
		this.rowIndexMedPrevMit = rowIndexMedPrevMit;
	}
  
}
