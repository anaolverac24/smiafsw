/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.view.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class ContaminanteProyectoTO implements Serializable {

	private static final long serialVersionUID = 1L;
    
    
    private String folioProyecto;
    
    private short serialProyecto;
    
    private short contaminanteId;

	private Short etapaId;

	private String etapaDescripcion;

    private Short contaminanteTipo;

    private String contaminanteTipoDesc;
    
    private Short contaminanteEmisiones;

    private BigDecimal contaminanteCantidad;

    private Short ctunClave;

    private String contaminanteMedidaControl;

    private String fuenteEmisora;
    
    private String fuenteEmisoraTrunk;

    private String observaciones; 

    private String observacionesTrunk;
    
    private CatContaminanteTO catContaminante;
    
    
    private EtapaProyectoTO etapaProyecto;

    private String descEmiResiDes;
    
    private String descEmiResiDesTrunk;
    
    private short ctunClv;
    
    private CatUnidadMedidaTO unidadMedida;
    
    public ContaminanteProyectoTO() {
    }


    public BigDecimal getContaminanteCantidad() {
        return contaminanteCantidad;
    }

    public void setContaminanteCantidad(BigDecimal contaminanteCantidad) {
        this.contaminanteCantidad = contaminanteCantidad;
    }

    public Short getCtunClave() {
        return ctunClave;
    }

    public void setCtunClave(Short ctunClave) {
        this.ctunClave = ctunClave;
    }

    public String getFuenteEmisora() {
        return fuenteEmisora;
    }

    public void setFuenteEmisora(String fuenteEmisora) {
        this.fuenteEmisora = fuenteEmisora;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

//    public CatContaminante getCatContaminante() {
//        return catContaminante;
//    }
//
//    public void setCatContaminante(CatContaminante catContaminante) {
//        this.catContaminante = catContaminante;
//    }

    public EtapaProyectoTO getEtapaProyecto() {
        return etapaProyecto;
    }

    public void setEtapaProyecto(EtapaProyectoTO etapaProyecto) {
        this.etapaProyecto = etapaProyecto;
    }





//    /**
//     * @return the contaminanteId
//     */
//    public Short getContaminanteId() {
//        return contaminanteId;
//    }
//
//    /**
//     * @param contaminanteId the contaminanteId to set
//     */
//    public void setContaminanteId(Short contaminanteId) {
//        this.contaminanteId = contaminanteId;
//    }

    public Short getEtapaId() {
        return etapaId;
    }

    public void setEtapaId(Short etapaId) {
        this.etapaId = etapaId;
    }

    
    
    /**
	 * @return the etapaDescripcion
	 */
	public String getEtapaDescripcion() {
		return etapaDescripcion;
	}


	/**
	 * @param etapaDescripcion the etapaDescripcion to set
	 */
	public void setEtapaDescripcion(String etapaDescripcion) {
		this.etapaDescripcion = etapaDescripcion;
	}


	public Short getContaminanteTipo() {
        return contaminanteTipo;
    }

    public void setContaminanteTipo(Short contaminanteTipo) {
        this.contaminanteTipo = contaminanteTipo;
    }

    
    
    /**
	 * @return the contaminanteTipoDesc
	 */
	public String getContaminanteTipoDesc() {
		return contaminanteTipoDesc;
	}


	/**
	 * @param contaminanteTipoDesc the contaminanteTipoDesc to set
	 */
	public void setContaminanteTipoDesc(String contaminanteTipoDesc) {
		this.contaminanteTipoDesc = contaminanteTipoDesc;
	}


	public Short getContaminanteEmisiones() {
        return contaminanteEmisiones;
    }

    public void setContaminanteEmisiones(Short contaminanteEmisiones) {
        this.contaminanteEmisiones = contaminanteEmisiones;
    }

    public String getContaminanteMedidaControl() {
        return contaminanteMedidaControl;
    }

    public void setContaminanteMedidaControl(String contaminanteMedidaControl) {
        this.contaminanteMedidaControl = contaminanteMedidaControl;
    }

    public CatContaminanteTO getCatContaminante() {
        return catContaminante;
    }

    public void setCatContaminante(CatContaminanteTO catContaminante) {
        this.catContaminante = catContaminante;
    }


	/**
	 * @return the folioProyecto
	 */
	public String getFolioProyecto() {
		return folioProyecto;
	}


	/**
	 * @param folioProyecto the folioProyecto to set
	 */
	public void setFolioProyecto(String folioProyecto) {
		this.folioProyecto = folioProyecto;
	}


	/**
	 * @return the serialProyecto
	 */
	public short getSerialProyecto() {
		return serialProyecto;
	}


	/**
	 * @param serialProyecto the serialProyecto to set
	 */
	public void setSerialProyecto(short serialProyecto) {
		this.serialProyecto = serialProyecto;
	}


	/**
	 * @return the contaminanteId
	 */
	public short getContaminanteId() {
		return contaminanteId;
	}


	/**
	 * @param contaminanteId the contaminanteId to set
	 */
	public void setContaminanteId(short contaminanteId) {
		this.contaminanteId = contaminanteId;
	}
   
    
    /* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + contaminanteId;
		result = prime * result + ((folioProyecto == null) ? 0 : folioProyecto.hashCode());
		result = prime * result + serialProyecto;
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContaminanteProyectoTO other = (ContaminanteProyectoTO) obj;
		if (contaminanteId != other.contaminanteId)
			return false;
		if (folioProyecto == null) {
			if (other.folioProyecto != null)
				return false;
		} else if (!folioProyecto.equals(other.folioProyecto))
			return false;
		if (serialProyecto != other.serialProyecto)
			return false;
		return true;
	}


	/**
	 * @return the descEmiResiDes
	 */
	public String getDescEmiResiDes() {
		return descEmiResiDes;
	}


	/**
	 * @param descEmiResiDes the descEmiResiDes to set
	 */
	public void setDescEmiResiDes(String descEmiResiDes) {
		this.descEmiResiDes = descEmiResiDes;
	}


	/**
	 * @return the ctunClv
	 */
	public short getCtunClv() {
		return ctunClv;
	}


	/**
	 * @param ctunClv the ctunClv to set
	 */
	public void setCtunClv(short ctunClv) {
		this.ctunClv = ctunClv;
	}


	/**
	 * @return the unidadMedida
	 */
	public CatUnidadMedidaTO getUnidadMedida() {
		return unidadMedida;
	}


	/**
	 * @param unidadMedida the unidadMedida to set
	 */
	public void setUnidadMedida(CatUnidadMedidaTO unidadMedida) {
		this.unidadMedida = unidadMedida;
	}


	/**
	 * @return the fuenteEmisoraTrunk
	 */
	public String getFuenteEmisoraTrunk() {
		return fuenteEmisoraTrunk;
	}


	/**
	 * @param fuenteEmisoraTrunk the fuenteEmisoraTrunk to set
	 */
	public void setFuenteEmisoraTrunk(String fuenteEmisoraTrunk) {
		this.fuenteEmisoraTrunk = fuenteEmisoraTrunk;
	}


	/**
	 * @return the descEmiResiDesTrunk
	 */
	public String getDescEmiResiDesTrunk() {
		return descEmiResiDesTrunk;
	}


	/**
	 * @param descEmiResiDesTrunk the descEmiResiDesTrunk to set
	 */
	public void setDescEmiResiDesTrunk(String descEmiResiDesTrunk) {
		this.descEmiResiDesTrunk = descEmiResiDesTrunk;
	}


	/**
	 * @return the observacionesTrunk
	 */
	public String getObservacionesTrunk() {
		return observacionesTrunk;
	}


	/**
	 * @param observacionesTrunk the observacionesTrunk to set
	 */
	public void setObservacionesTrunk(String observacionesTrunk) {
		this.observacionesTrunk = observacionesTrunk;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ContaminanteProyectoTO [folioProyecto=" + folioProyecto + ", serialProyecto=" + serialProyecto
				+ ", contaminanteId=" + contaminanteId + ", etapaId=" + etapaId + ", etapaDescripcion="
				+ etapaDescripcion + ", contaminanteTipo=" + contaminanteTipo + ", contaminanteTipoDesc="
				+ contaminanteTipoDesc + ", contaminanteEmisiones=" + contaminanteEmisiones + ", contaminanteCantidad="
				+ contaminanteCantidad + ", ctunClave=" + ctunClave + ", contaminanteMedidaControl="
				+ contaminanteMedidaControl + ", fuenteEmisora=" + fuenteEmisora + ", fuenteEmisoraTrunk="
				+ fuenteEmisoraTrunk + ", observaciones=" + observaciones + ", observacionesTrunk=" + observacionesTrunk
				+ ", catContaminante=" + catContaminante + ", etapaProyecto=" + etapaProyecto + ", descEmiResiDes="
				+ descEmiResiDes + ", descEmiResiDesTrunk=" + descEmiResiDesTrunk + ", ctunClv=" + ctunClv
				+ ", unidadMedida=" + unidadMedida + "]";
	}

	
	 
}
