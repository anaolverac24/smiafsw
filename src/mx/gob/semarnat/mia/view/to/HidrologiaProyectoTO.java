package mx.gob.semarnat.mia.view.to;
// Generated 16/04/2015 03:02:18 PM by Hibernate Tools 3.6.0


import java.math.BigDecimal;

import javax.persistence.Column;


public class HidrologiaProyectoTO  implements java.io.Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = 352783095829099094L;
	
	private BigDecimal hidroId;
    private String folioProyecto;
    private BigDecimal serialProyecto;
    
     private String hidroCuenca;
     private String hidroSubcuenca;
     private String hidroMicrocuenca;


    public String getHidroCuenca() {
        return this.hidroCuenca;
    }
    
    public void setHidroCuenca(String hidroCuenca) {
        this.hidroCuenca = hidroCuenca;
    }

    

    public String getHidroSubcuenca() {
        return this.hidroSubcuenca;
    }
    
    public void setHidroSubcuenca(String hidroSubcuenca) {
        this.hidroSubcuenca = hidroSubcuenca;
    }

    
    @Column(name="HIDRO_MICROCUENCA", length=100)
    public String getHidroMicrocuenca() {
        return this.hidroMicrocuenca;
    }
    
    public void setHidroMicrocuenca(String hidroMicrocuenca) {
        this.hidroMicrocuenca = hidroMicrocuenca;
    }

    







	/**
	 * @return the hidroId
	 */
	public BigDecimal getHidroId() {
		return hidroId;
	}






	/**
	 * @param hidroId the hidroId to set
	 */
	public void setHidroId(BigDecimal hidroId) {
		this.hidroId = hidroId;
	}






	/**
	 * @return the folioProyecto
	 */
	public String getFolioProyecto() {
		return folioProyecto;
	}






	/**
	 * @param folioProyecto the folioProyecto to set
	 */
	public void setFolioProyecto(String folioProyecto) {
		this.folioProyecto = folioProyecto;
	}

	/**
	 * @return the serialProyecto
	 */
	public BigDecimal getSerialProyecto() {
		return serialProyecto;
	}






	/**
	 * @param serialProyecto the serialProyecto to set
	 */
	public void setSerialProyecto(BigDecimal serialProyecto) {
		this.serialProyecto = serialProyecto;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HidrologiaProyectoTO [hidroId=" + hidroId + ", folioProyecto=" + folioProyecto + ", serialProyecto="
				+ serialProyecto + ", hidroCuenca=" + hidroCuenca + ", hidroSubcuenca=" + hidroSubcuenca
				+ ", hidroMicrocuenca=" + hidroMicrocuenca + "]";
	}




}


