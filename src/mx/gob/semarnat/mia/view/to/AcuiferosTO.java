/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.view.to;

import java.io.Serializable;
import java.util.Date;


/**
 *
 * @author mauricio
 */

public class AcuiferosTO implements Serializable {
    private static final long serialVersionUID = 1L;

  


	private String numFolio;

    private String cveProy;

    private String cveArea;

    private short version;

    private String clvAcui;

    private String nomAcui;

    private String descDispo;
    
    private Date fechaDof;
    
    private String sobreexp;
    
    private boolean sobreexpBoolean;
    
    private Double supEa;
    
    private String proy;
    
    private String comp;
    
    private String descrip;
    
    private Double areabuffer;
    
    private Double area;
    
    
    private Date fechaHora;
    

    public AcuiferosTO() {
    }


	/**
	 * @return the numFolio
	 */
	public String getNumFolio() {
		return numFolio;
	}


	/**
	 * @param numFolio the numFolio to set
	 */
	public void setNumFolio(String numFolio) {
		this.numFolio = numFolio;
	}


	/**
	 * @return the cveProy
	 */
	public String getCveProy() {
		return cveProy;
	}


	/**
	 * @param cveProy the cveProy to set
	 */
	public void setCveProy(String cveProy) {
		this.cveProy = cveProy;
	}


	/**
	 * @return the cveArea
	 */
	public String getCveArea() {
		return cveArea;
	}


	/**
	 * @param cveArea the cveArea to set
	 */
	public void setCveArea(String cveArea) {
		this.cveArea = cveArea;
	}


	/**
	 * @return the version
	 */
	public short getVersion() {
		return version;
	}


	/**
	 * @param version the version to set
	 */
	public void setVersion(short version) {
		this.version = version;
	}


	/**
	 * @return the clvAcui
	 */
	public String getClvAcui() {
		return clvAcui;
	}


	/**
	 * @param clvAcui the clvAcui to set
	 */
	public void setClvAcui(String clvAcui) {
		this.clvAcui = clvAcui;
	}


	/**
	 * @return the nomAcui
	 */
	public String getNomAcui() {
		return nomAcui;
	}


	/**
	 * @param nomAcui the nomAcui to set
	 */
	public void setNomAcui(String nomAcui) {
		this.nomAcui = nomAcui;
	}


	/**
	 * @return the descDispo
	 */
	public String getDescDispo() {
		return descDispo;
	}


	/**
	 * @param descDispo the descDispo to set
	 */
	public void setDescDispo(String descDispo) {
		this.descDispo = descDispo;
	}


	/**
	 * @return the fechaDof
	 */
	public Date getFechaDof() {
		return fechaDof;
	}


	/**
	 * @param fechaDof the fechaDof to set
	 */
	public void setFechaDof(Date fechaDof) {
		this.fechaDof = fechaDof;
	}


	/**
	 * @return the sobreexp
	 */
	public String getSobreexp() {
		return sobreexp;
	}


	/**
	 * @param sobreexp the sobreexp to set
	 */
	public void setSobreexp(String sobreexp) {
		this.sobreexp = sobreexp;
	}


	/**
	 * @return the supEa
	 */
	public Double getSupEa() {
		return supEa;
	}


	/**
	 * @param supEa the supEa to set
	 */
	public void setSupEa(Double supEa) {
		this.supEa = supEa;
	}


	/**
	 * @return the proy
	 */
	public String getProy() {
		return proy;
	}


	/**
	 * @param proy the proy to set
	 */
	public void setProy(String proy) {
		this.proy = proy;
	}


	/**
	 * @return the comp
	 */
	public String getComp() {
		return comp;
	}


	/**
	 * @param comp the comp to set
	 */
	public void setComp(String comp) {
		this.comp = comp;
	}


	/**
	 * @return the descrip
	 */
	public String getDescrip() {
		return descrip;
	}


	/**
	 * @param descrip the descrip to set
	 */
	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}


	/**
	 * @return the areabuffer
	 */
	public Double getAreabuffer() {
		return areabuffer;
	}


	/**
	 * @param areabuffer the areabuffer to set
	 */
	public void setAreabuffer(Double areabuffer) {
		this.areabuffer = areabuffer;
	}


	/**
	 * @return the area
	 */
	public Double getArea() {
		return area;
	}


	/**
	 * @param area the area to set
	 */
	public void setArea(Double area) {
		this.area = area;
	}


	/**
	 * @return the fechaHora
	 */
	public Date getFechaHora() {
		return fechaHora;
	}


	/**
	 * @param fechaHora the fechaHora to set
	 */
	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}


	/**
	 * @return the sobreexpBoolean
	 */
	public boolean isSobreexpBoolean() {
		return sobreexpBoolean;
	}


	/**
	 * @param sobreexpBoolean the sobreexpBoolean to set
	 */
	public void setSobreexpBoolean(boolean sobreexpBoolean) {
		this.sobreexpBoolean = sobreexpBoolean;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AcuiferosTO [numFolio=" + numFolio + ", cveProy=" + cveProy + ", cveArea=" + cveArea + ", version="
				+ version + ", clvAcui=" + clvAcui + ", nomAcui=" + nomAcui + ", descDispo=" + descDispo + ", fechaDof="
				+ fechaDof + ", sobreexp=" + sobreexp + ", supEa=" + supEa + ", proy=" + proy + ", comp=" + comp
				+ ", descrip=" + descrip + ", areabuffer=" + areabuffer + ", area=" + area + ", fechaHora=" + fechaHora
				+ "]";
	}
    
    
	  /* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((cveArea == null) ? 0 : cveArea.hashCode());
			result = prime * result + ((cveProy == null) ? 0 : cveProy.hashCode());
			result = prime * result + ((numFolio == null) ? 0 : numFolio.hashCode());
			result = prime * result + version;
			return result;
		}


		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			AcuiferosTO other = (AcuiferosTO) obj;
			if (cveArea == null) {
				if (other.cveArea != null)
					return false;
			} else if (!cveArea.equals(other.cveArea))
				return false;
			if (cveProy == null) {
				if (other.cveProy != null)
					return false;
			} else if (!cveProy.equals(other.cveProy))
				return false;
			if (numFolio == null) {
				if (other.numFolio != null)
					return false;
			} else if (!numFolio.equals(other.numFolio))
				return false;
			if (version != other.version)
				return false;
			return true;
		}    
}
