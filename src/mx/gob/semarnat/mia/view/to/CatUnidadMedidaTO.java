/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view.to;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class CatUnidadMedidaTO implements Serializable {
    private static final long serialVersionUID = 1L;
   
    private Short ctunClve;
    private String ctunAbre;
    private String ctunDesc;
    private String ctunTipo;
    private String ctunFac;
    private short estatus;

    public CatUnidadMedidaTO() {
    }

    public CatUnidadMedidaTO(Short ctunClve) {
        this.ctunClve = ctunClve;
    }

    public CatUnidadMedidaTO(Short ctunClve, String ctunAbre, String ctunDesc, short estatus) {
        this.ctunClve = ctunClve;
        this.ctunAbre = ctunAbre;
        this.ctunDesc = ctunDesc;
        this.estatus = estatus;
    }

    public Short getCtunClve() {
        return ctunClve;
    }

    public void setCtunClve(Short ctunClve) {
        this.ctunClve = ctunClve;
    }

    public String getCtunAbre() {
        return ctunAbre;
    }

    public void setCtunAbre(String ctunAbre) {
        this.ctunAbre = ctunAbre;
    }

    public String getCtunDesc() {
        return ctunDesc;
    }

    public void setCtunDesc(String ctunDesc) {
        this.ctunDesc = ctunDesc;
    }

    public String getCtunTipo() {
        return ctunTipo;
    }

    public void setCtunTipo(String ctunTipo) {
        this.ctunTipo = ctunTipo;
    }

    public String getCtunFac() {
        return ctunFac;
    }

    public void setCtunFac(String ctunFac) {
        this.ctunFac = ctunFac;
    }

    public short getEstatus() {
        return estatus;
    }

    public void setEstatus(short estatus) {
        this.estatus = estatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ctunClve != null ? ctunClve.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatUnidadMedidaTO)) {
            return false;
        }
        CatUnidadMedidaTO other = (CatUnidadMedidaTO) object;
        if ((this.ctunClve == null && other.ctunClve != null) || (this.ctunClve != null && !this.ctunClve.equals(other.ctunClve))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return ctunClve+"";
    }
    
}
