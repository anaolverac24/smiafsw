/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.view.to;

import java.io.Serializable;


public class CatContaminanteTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Short contaminanteId;
    
    private String contaminanteNombre;
    
    //private List<ContaminanteProyecto> contaminanteProyectoList;
    
    private CatTipoContaminanteTO tipoContaminanteId;

    public CatContaminanteTO() {
    }

    public CatContaminanteTO(Short contaminanteId) {
        this.contaminanteId = contaminanteId;
    }

    public CatContaminanteTO(Short contaminanteId, String contaminanteNombre) {
        this.contaminanteId = contaminanteId;
        this.contaminanteNombre = contaminanteNombre;
    }

    public Short getContaminanteId() {
        return contaminanteId;
    }

    public void setContaminanteId(Short contaminanteId) {
        this.contaminanteId = contaminanteId;
    }

    public String getContaminanteNombre() {
        return contaminanteNombre;
    }

    public void setContaminanteNombre(String contaminanteNombre) {
        this.contaminanteNombre = contaminanteNombre;
    }


    public CatTipoContaminanteTO getTipoContaminanteId() {
        return tipoContaminanteId;
    }

    public void setTipoContaminanteId(CatTipoContaminanteTO tipoContaminanteId) {
        this.tipoContaminanteId = tipoContaminanteId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contaminanteId != null ? contaminanteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatContaminanteTO)) {
            return false;
        }
        CatContaminanteTO other = (CatContaminanteTO) object;
        if ((this.contaminanteId == null && other.contaminanteId != null) || (this.contaminanteId != null && !this.contaminanteId.equals(other.contaminanteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatContaminante[ contaminanteId=" + contaminanteId + " ]";
    }
    
}
