/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.view.to;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author mauricio
 */

public class EstudiosEspProyTO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
    private String folioSerial;    
    private short serialProyecto;        
    private short estudioId;    
    private Short estudioEspecial;    
    private String anexoUrl;    
    private String anexoDescripcion;    
    private String anexoNombre;        
    private BigDecimal anexoTamanio;    
    private String anexoExtension;    
    private String nombrePromovente;    
    private short  capituloId;    
    private short seccionId;    
    private String reservado;    
    private boolean reservadoB;

    public EstudiosEspProyTO() {
    }


	/**
	 * @return the folioSerial
	 */
	public String getFolioSerial() {
		return folioSerial;
	}







	/**
	 * @param folioSerial the folioSerial to set
	 */
	public void setFolioSerial(String folioSerial) {
		this.folioSerial = folioSerial;
	}







	/**
	 * @return the serialProyecto
	 */
	public short getSerialProyecto() {
		return serialProyecto;
	}







	/**
	 * @param serialProyecto the serialProyecto to set
	 */
	public void setSerialProyecto(short serialProyecto) {
		this.serialProyecto = serialProyecto;
	}







	/**
	 * @return the estudioId
	 */
	public short getEstudioId() {
		return estudioId;
	}







	/**
	 * @param estudioId the estudioId to set
	 */
	public void setEstudioId(short estudioId) {
		this.estudioId = estudioId;
	}







	/**
	 * @return the estudioEspecial
	 */
	public Short getEstudioEspecial() {
		return estudioEspecial;
	}







	/**
	 * @param estudioEspecial the estudioEspecial to set
	 */
	public void setEstudioEspecial(Short estudioEspecial) {
		this.estudioEspecial = estudioEspecial;
	}







	/**
	 * @return the anexoUrl
	 */
	public String getAnexoUrl() {
		return anexoUrl;
	}







	/**
	 * @param anexoUrl the anexoUrl to set
	 */
	public void setAnexoUrl(String anexoUrl) {
		this.anexoUrl = anexoUrl;
	}







	/**
	 * @return the anexoDescripcion
	 */
	public String getAnexoDescripcion() {
		return anexoDescripcion;
	}







	/**
	 * @param anexoDescripcion the anexoDescripcion to set
	 */
	public void setAnexoDescripcion(String anexoDescripcion) {
		this.anexoDescripcion = anexoDescripcion;
	}







	/**
	 * @return the anexoNombre
	 */
	public String getAnexoNombre() {
		return anexoNombre;
	}







	/**
	 * @param anexoNombre the anexoNombre to set
	 */
	public void setAnexoNombre(String anexoNombre) {
		this.anexoNombre = anexoNombre;
	}







	/**
	 * @return the anexoTamanio
	 */
	public BigDecimal getAnexoTamanio() {
		return anexoTamanio;
	}







	/**
	 * @param anexoTamanio the anexoTamanio to set
	 */
	public void setAnexoTamanio(BigDecimal anexoTamanio) {
		this.anexoTamanio = anexoTamanio;
	}







	/**
	 * @return the anexoExtension
	 */
	public String getAnexoExtension() {
		return anexoExtension;
	}







	/**
	 * @param anexoExtension the anexoExtension to set
	 */
	public void setAnexoExtension(String anexoExtension) {
		this.anexoExtension = anexoExtension;
	}







	/**
	 * @return the nombrePromovente
	 */
	public String getNombrePromovente() {
		return nombrePromovente;
	}







	/**
	 * @param nombrePromovente the nombrePromovente to set
	 */
	public void setNombrePromovente(String nombrePromovente) {
		this.nombrePromovente = nombrePromovente;
	}







	/**
	 * @return the capituloId
	 */
	public short getCapituloId() {
		return capituloId;
	}







	/**
	 * @param capituloId the capituloId to set
	 */
	public void setCapituloId(short capituloId) {
		this.capituloId = capituloId;
	}







	/**
	 * @return the seccionId
	 */
	public short getSeccionId() {
		return seccionId;
	}







	/**
	 * @param seccionId the seccionId to set
	 */
	public void setSeccionId(short seccionId) {
		this.seccionId = seccionId;
	}







	/**
	 * @return the reservado
	 */
	public String getReservado() {
		return reservado;
	}







	/**
	 * @param reservado the reservado to set
	 */
	public void setReservado(String reservado) {
		this.reservado = reservado;
	}







	/**
	 * @return the reservadoB
	 */
	public boolean isReservadoB() {
		return reservadoB;
	}







	/**
	 * @param reservadoB the reservadoB to set
	 */
	public void setReservadoB(boolean reservadoB) {
		this.reservadoB = reservadoB;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + estudioId;
		result = prime * result + ((folioSerial == null) ? 0 : folioSerial.hashCode());
		result = prime * result + serialProyecto;
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EstudiosEspProyTO other = (EstudiosEspProyTO) obj;
		if (estudioId != other.estudioId)
			return false;
		if (folioSerial == null) {
			if (other.folioSerial != null)
				return false;
		} else if (!folioSerial.equals(other.folioSerial))
			return false;
		if (serialProyecto != other.serialProyecto)
			return false;
		return true;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EstudiosEspProyTO [folioSerial=" + folioSerial + ", serialProyecto=" + serialProyecto + ", estudioId="
				+ estudioId + ", estudioEspecial=" + estudioEspecial + ", anexoUrl=" + anexoUrl + ", anexoDescripcion="
				+ anexoDescripcion + ", anexoNombre=" + anexoNombre + ", anexoTamanio=" + anexoTamanio
				+ ", anexoExtension=" + anexoExtension + ", nombrePromovente=" + nombrePromovente + ", capituloId="
				+ capituloId + ", seccionId=" + seccionId + ", reservado=" + reservado + ", reservadoB=" + reservadoB
				+ "]";
	}

        
    
}
