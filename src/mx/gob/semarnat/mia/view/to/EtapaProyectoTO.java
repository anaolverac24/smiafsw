/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.view.to;

import java.io.Serializable;

/**
 *
 * @author mauricio
 */
public class EtapaProyectoTO implements Serializable {
   

	
    private static final long serialVersionUID = 1L;
    
    
    private String folioProyecto;
    
    private short serialProyecto;
    
    private short etapaId;
    

    private short meses;

    private short anios;

    private String etapaDescripcionObraAct;

    private Short semanas;

    private String descEtapa;
    
    //private CatEtapa catEtapa;
    
    //private Proyecto proyecto;
    
    

    public EtapaProyectoTO() {
    }


	/**
	 * @return the folioProyecto
	 */
	public String getFolioProyecto() {
		return folioProyecto;
	}


	/**
	 * @param folioProyecto the folioProyecto to set
	 */
	public void setFolioProyecto(String folioProyecto) {
		this.folioProyecto = folioProyecto;
	}


	/**
	 * @return the serialProyecto
	 */
	public short getSerialProyecto() {
		return serialProyecto;
	}


	/**
	 * @param serialProyecto the serialProyecto to set
	 */
	public void setSerialProyecto(short serialProyecto) {
		this.serialProyecto = serialProyecto;
	}


	/**
	 * @return the etapaId
	 */
	public short getEtapaId() {
		return etapaId;
	}


	/**
	 * @param etapaId the etapaId to set
	 */
	public void setEtapaId(short etapaId) {
		this.etapaId = etapaId;
	}


	/**
	 * @return the meses
	 */
	public short getMeses() {
		return meses;
	}


	/**
	 * @param meses the meses to set
	 */
	public void setMeses(short meses) {
		this.meses = meses;
	}


	/**
	 * @return the anios
	 */
	public short getAnios() {
		return anios;
	}


	/**
	 * @param anios the anios to set
	 */
	public void setAnios(short anios) {
		this.anios = anios;
	}


	/**
	 * @return the etapaDescripcionObraAct
	 */
	public String getEtapaDescripcionObraAct() {
		return etapaDescripcionObraAct;
	}


	/**
	 * @param etapaDescripcionObraAct the etapaDescripcionObraAct to set
	 */
	public void setEtapaDescripcionObraAct(String etapaDescripcionObraAct) {
		this.etapaDescripcionObraAct = etapaDescripcionObraAct;
	}


	/**
	 * @return the semanas
	 */
	public Short getSemanas() {
		return semanas;
	}


	/**
	 * @param semanas the semanas to set
	 */
	public void setSemanas(Short semanas) {
		this.semanas = semanas;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + etapaId;
		result = prime * result + ((folioProyecto == null) ? 0 : folioProyecto.hashCode());
		result = prime * result + serialProyecto;
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EtapaProyectoTO other = (EtapaProyectoTO) obj;
		if (etapaId != other.etapaId)
			return false;
		if (folioProyecto == null) {
			if (other.folioProyecto != null)
				return false;
		} else if (!folioProyecto.equals(other.folioProyecto))
			return false;
		if (serialProyecto != other.serialProyecto)
			return false;
		return true;
	}


	/**
	 * @return the descEtapa
	 */
	public String getDescEtapa() {
		return descEtapa;
	}


	/**
	 * @param descEtapa the descEtapa to set
	 */
	public void setDescEtapa(String descEtapa) {
		this.descEtapa = descEtapa;
	}


	
	
}
