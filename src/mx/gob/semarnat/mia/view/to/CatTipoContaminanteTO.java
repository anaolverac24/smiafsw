/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.view.to;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */

public class CatTipoContaminanteTO implements Serializable {

	private static final long serialVersionUID = 1L;

    private Short tipoContaminanteId;
    
    private String tipoContaminanteDescripcion;

    public CatTipoContaminanteTO() {
    }

    public CatTipoContaminanteTO(Short tipoContaminanteId) {
        this.tipoContaminanteId = tipoContaminanteId;
    }

    public CatTipoContaminanteTO(Short tipoContaminanteId, String tipoContaminanteDescripcion) {
        this.tipoContaminanteId = tipoContaminanteId;
        this.tipoContaminanteDescripcion = tipoContaminanteDescripcion;
    }

    public Short getTipoContaminanteId() {
        return tipoContaminanteId;
    }

    public void setTipoContaminanteId(Short tipoContaminanteId) {
        this.tipoContaminanteId = tipoContaminanteId;
    }

    public String getTipoContaminanteDescripcion() {
        return tipoContaminanteDescripcion;
    }

    public void setTipoContaminanteDescripcion(String tipoContaminanteDescripcion) {
        this.tipoContaminanteDescripcion = tipoContaminanteDescripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tipoContaminanteId != null ? tipoContaminanteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatTipoContaminanteTO)) {
            return false;
        }
        CatTipoContaminanteTO other = (CatTipoContaminanteTO) object;
        if ((this.tipoContaminanteId == null && other.tipoContaminanteId != null) || (this.tipoContaminanteId != null && !this.tipoContaminanteId.equals(other.tipoContaminanteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gob.semarnat.mia.model.CatTipoContaminante[ tipoContaminanteId=" + tipoContaminanteId + " ]";
    }
    
}
