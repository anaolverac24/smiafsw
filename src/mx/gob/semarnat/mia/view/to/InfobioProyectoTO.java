/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.semarnat.mia.view.to;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Administrador
 */
public class InfobioProyectoTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String folioProyecto;
    private short serialProyecto;
    private short infoBioId;

    private String bioNombre;
    private String bioNombreCorto;
    private String bioOrigen;
    private String bioOrigenCorto;
    private String bioNumorgCultivar;
    private String bioNumorgCultivarCorto;
    private String bioDescAtributosAme;
    private String bioDescAtributosAmeCorto;
    
    private Character bioEspForrajera;
    private boolean  bioEspForrajeraBoolean;
    /**
     * Indicador de la fila seleccionada del registro a eliminar o actualizar.
     */
    private Long rowObra = 0L;
    
    private Character 	bioCrias;
    private boolean  	bioCriasBoolean;
    
    private Character 	bioSemillas;
    private boolean  	bioSemillasBoolean;
    
    private Character 	bioPostlarvas;
    private boolean  	bioPostlarvasBoolean;
        
    private Character bioJuveniles;
    private boolean bioJuvenilesBoolean;
    
    private Character bioAdultReprod;
    private boolean bioAdultReprodBoolean;
    
    private Long bioNumciclos;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    private BigDecimal bioBiomasaInic;
    private String bioUnidadInic;
    private BigDecimal bioBiomasaEsperada;
    private String bioUnidadEsperada;
    
    private String bioUnidadAlmacen;      
    private String bioTipoAlimento;
    private BigDecimal bioCantAlimento;
    private String bioFormaAlmacen;
    
    
    private String bioTipabono;
    private String bioUsoAbono;
    private String bioUnidadSuministro;
    private BigDecimal bioCantSuministro;
    private String bioFormAlmacAbono;

    private CatalogoTO infoBioProyecto;
    
    public InfobioProyectoTO() {
    }

    public InfobioProyectoTO( String folioProyecto,  short serialProyecto, short infoBioId) {
        this.folioProyecto = folioProyecto;
        this.serialProyecto = serialProyecto;
        this.infoBioId = infoBioId;
    }

    
    
    
    /**
	 * @return the folioProyecto
	 */
	public String getFolioProyecto() {
		return folioProyecto;
	}

	/**
	 * @param folioProyecto the folioProyecto to set
	 */
	public void setFolioProyecto(String folioProyecto) {
		this.folioProyecto = folioProyecto;
	}

	/**
	 * @return the serialProyecto
	 */
	public short getSerialProyecto() {
		return serialProyecto;
	}

	/**
	 * @param serialProyecto the serialProyecto to set
	 */
	public void setSerialProyecto(short serialProyecto) {
		this.serialProyecto = serialProyecto;
	}

	/**
	 * @return the infoBioId
	 */
	public short getInfoBioId() {
		return infoBioId;
	}

	/**
	 * @param infoBioId the infoBioId to set
	 */
	public void setInfoBioId(short infoBioId) {
		this.infoBioId = infoBioId;
	}

	public String getBioNombre() {
        return bioNombre;
    }

    public void setBioNombre(String bioNombre) {
        this.bioNombre = bioNombre;
    }

    public String getBioOrigen() {
        return bioOrigen;
    }

    public void setBioOrigen(String bioOrigen) {
        this.bioOrigen = bioOrigen;
    }

    public String getBioNumorgCultivar() {
        return bioNumorgCultivar;
    }

    public void setBioNumorgCultivar(String bioNumorgCultivar) {
        this.bioNumorgCultivar = bioNumorgCultivar;
    }

    public String getBioDescAtributosAme() {
        return bioDescAtributosAme;
    }

    public void setBioDescAtributosAme(String bioDescAtributosAme) {
        this.bioDescAtributosAme = bioDescAtributosAme;
    }

    public Character getBioEspForrajera() {
        return bioEspForrajera;
    }

    public void setBioEspForrajera(Character bioEspForrajera) {
        this.bioEspForrajera = bioEspForrajera;
    }

    
    
    /**
	 * @return the bioEspForrajeraBoolean
	 */
	public boolean isBioEspForrajeraBoolean() {
		return bioEspForrajeraBoolean;
	}

	/**
	 * @param bioEspForrajeraBoolean the bioEspForrajeraBoolean to set
	 */
	public void setBioEspForrajeraBoolean(boolean bioEspForrajeraBoolean) {
		this.bioEspForrajeraBoolean = bioEspForrajeraBoolean;
	}

	public Character getBioCrias() {
        return bioCrias;
    }

    public void setBioCrias(Character bioCrias) {
        this.bioCrias = bioCrias;
    }

    public Character getBioSemillas() {
        return bioSemillas;
    }

    public void setBioSemillas(Character bioSemillas) {
        this.bioSemillas = bioSemillas;
    }

    public Character getBioPostlarvas() {
        return bioPostlarvas;
    }

    public void setBioPostlarvas(Character bioPostlarvas) {
        this.bioPostlarvas = bioPostlarvas;
    }

    public Character getBioJuveniles() {
        return bioJuveniles;
    }

    public void setBioJuveniles(Character bioJuveniles) {
        this.bioJuveniles = bioJuveniles;
    }

    public Character getBioAdultReprod() {
        return bioAdultReprod;
    }

    public void setBioAdultReprod(Character bioAdultReprod) {
        this.bioAdultReprod = bioAdultReprod;
    }

    public Long getBioNumciclos() {
        return bioNumciclos;
    }

    public void setBioNumciclos(Long bioNumciclos) {
        this.bioNumciclos = bioNumciclos;
    }

    public BigDecimal getBioBiomasaInic() {
        return bioBiomasaInic;
    }

    public void setBioBiomasaInic(BigDecimal bioBiomasaInic) {
        this.bioBiomasaInic = bioBiomasaInic;
    }

    public String getBioUnidadInic() {
        return bioUnidadInic;
    }

    public void setBioUnidadInic(String bioUnidadInic) {
        this.bioUnidadInic = bioUnidadInic;
    }

    public BigDecimal getBioBiomasaEsperada() {
        return bioBiomasaEsperada;
    }

    public void setBioBiomasaEsperada(BigDecimal bioBiomasaEsperada) {
        this.bioBiomasaEsperada = bioBiomasaEsperada;
    }

    public String getBioUnidadEsperada() {
        return bioUnidadEsperada;
    }

    public void setBioUnidadEsperada(String bioUnidadEsperada) {
        this.bioUnidadEsperada = bioUnidadEsperada;
    }

    public String getBioTipoAlimento() {
        return bioTipoAlimento;
    }

    public void setBioTipoAlimento(String bioTipoAlimento) {
        this.bioTipoAlimento = bioTipoAlimento;
    }

    public BigDecimal getBioCantAlimento() {
        return bioCantAlimento;
    }

    public void setBioCantAlimento(BigDecimal bioCantAlimento) {
        this.bioCantAlimento = bioCantAlimento;
    }

    public String getBioFormaAlmacen() {
        return bioFormaAlmacen;
    }

    public void setBioFormaAlmacen(String bioFormaAlmacen) {
        this.bioFormaAlmacen = bioFormaAlmacen;
    }

    public String getBioTipabono() {
        return bioTipabono;
    }

    public void setBioTipabono(String bioTipabono) {
        this.bioTipabono = bioTipabono;
    }

    public String getBioUsoAbono() {
        return bioUsoAbono;
    }

    public void setBioUsoAbono(String bioUsoAbono) {
        this.bioUsoAbono = bioUsoAbono;
    }

    public BigDecimal getBioCantSuministro() {
        return bioCantSuministro;
    }

    public void setBioCantSuministro(BigDecimal bioCantSuministro) {
        this.bioCantSuministro = bioCantSuministro;
    }

    public String getBioFormAlmacAbono() {
        return bioFormAlmacAbono;
    }

    public void setBioFormAlmacAbono(String bioFormAlmacAbono) {
        this.bioFormAlmacAbono = bioFormAlmacAbono;
    }

    
    

	/**
	 * @return the infoBioProyecto
	 */
	public CatalogoTO getInfoBioProyecto() {
		return infoBioProyecto;
	}

	/**
	 * @param infoBioProyecto the infoBioProyecto to set
	 */
	public void setInfoBioProyecto(CatalogoTO infoBioProyecto) {
		this.infoBioProyecto = infoBioProyecto;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "InfobioProyectoTO [folioProyecto=" + folioProyecto + ", serialProyecto=" + serialProyecto
				+ ", infoBioId=" + infoBioId + ", bioNombre=" + bioNombre + ", bioOrigen=" + bioOrigen
				+ ", bioNumorgCultivar=" + bioNumorgCultivar + ", bioDescAtributosAme=" + bioDescAtributosAme
				+ ", bioEspForrajera=" + bioEspForrajera + ", bioCrias=" + bioCrias + ", bioSemillas=" + bioSemillas
				+ ", bioPostlarvas=" + bioPostlarvas + ", bioJuveniles=" + bioJuveniles + ", bioAdultReprod="
				+ bioAdultReprod + ", bioNumciclos=" + bioNumciclos + ", bioBiomasaInic=" + bioBiomasaInic
				+ ", bioUnidadInic=" + bioUnidadInic + ", bioBiomasaEsperada=" + bioBiomasaEsperada
				+ ", bioUnidadEsperada=" + bioUnidadEsperada + ", bioTipoAlimento=" + bioTipoAlimento
				+ ", bioCantAlimento=" + bioCantAlimento + ", bioFormaAlmacen=" + bioFormaAlmacen + ", bioTipabono="
				+ bioTipabono + ", bioUsoAbono=" + bioUsoAbono + ", bioCantSuministro=" + bioCantSuministro
				+ ", bioFormAlmacAbono=" + bioFormAlmacAbono + "]";
	}

    
    /* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((folioProyecto == null) ? 0 : folioProyecto.hashCode());
		result = prime * result + infoBioId;
		result = prime * result + serialProyecto;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InfobioProyectoTO other = (InfobioProyectoTO) obj;
		if (folioProyecto == null) {
			if (other.folioProyecto != null)
				return false;
		} else if (!folioProyecto.equals(other.folioProyecto))
			return false;
		if (infoBioId != other.infoBioId)
			return false;
		if (serialProyecto != other.serialProyecto)
			return false;
		return true;
	}

	/**
	 * @return the bioCriasBoolean
	 */
	public boolean isBioCriasBoolean() {
		return bioCriasBoolean;
	}

	/**
	 * @param bioCriasBoolean the bioCriasBoolean to set
	 */
	public void setBioCriasBoolean(boolean bioCriasBoolean) {
		this.bioCriasBoolean = bioCriasBoolean;
	}

	/**
	 * @return the bioSemillasBoolean
	 */
	public boolean isBioSemillasBoolean() {
		return bioSemillasBoolean;
	}

	/**
	 * @param bioSemillasBoolean the bioSemillasBoolean to set
	 */
	public void setBioSemillasBoolean(boolean bioSemillasBoolean) {
		this.bioSemillasBoolean = bioSemillasBoolean;
	}

	/**
	 * @return the bioPostlarvasBoolean
	 */
	public boolean isBioPostlarvasBoolean() {
		return bioPostlarvasBoolean;
	}

	/**
	 * @param bioPostlarvasBoolean the bioPostlarvasBoolean to set
	 */
	public void setBioPostlarvasBoolean(boolean bioPostlarvasBoolean) {
		this.bioPostlarvasBoolean = bioPostlarvasBoolean;
	}

	/**
	 * @return the bioJuvenilesBoolean
	 */
	public boolean isBioJuvenilesBoolean() {
		return bioJuvenilesBoolean;
	}

	/**
	 * @param bioJuvenilesBoolean the bioJuvenilesBoolean to set
	 */
	public void setBioJuvenilesBoolean(boolean bioJuvenilesBoolean) {
		this.bioJuvenilesBoolean = bioJuvenilesBoolean;
	}

	/**
	 * @return the bioAdultReprodBoolean
	 */
	public boolean isBioAdultReprodBoolean() {
		return bioAdultReprodBoolean;
	}

	/**
	 * @param bioAdultReprodBoolean the bioAdultReprodBoolean to set
	 */
	public void setBioAdultReprodBoolean(boolean bioAdultReprodBoolean) {
		this.bioAdultReprodBoolean = bioAdultReprodBoolean;
	}

	/**
	 * @return the bioUnidadSuministro
	 */
	public String getBioUnidadSuministro() {
		return bioUnidadSuministro;
	}

	/**
	 * @param bioUnidadSuministro the bioUnidadSuministro to set
	 */
	public void setBioUnidadSuministro(String bioUnidadSuministro) {
		this.bioUnidadSuministro = bioUnidadSuministro;
	}

	/**
	 * @return the bioUnidadAlmacen
	 */
	public String getBioUnidadAlmacen() {
		return bioUnidadAlmacen;
	}

	/**
	 * @param bioUnidadAlmacen the bioUnidadAlmacen to set
	 */
	public void setBioUnidadAlmacen(String bioUnidadAlmacen) {
		this.bioUnidadAlmacen = bioUnidadAlmacen;
	}

	/**
	 * @return the bioNombreCorto
	 */
	public String getBioNombreCorto() {
		return bioNombreCorto;
	}

	/**
	 * @param bioNombreCorto the bioNombreCorto to set
	 */
	public void setBioNombreCorto(String bioNombreCorto) {
		this.bioNombreCorto = bioNombreCorto;
	}

	/**
	 * @return the bioOrigenCorto
	 */
	public String getBioOrigenCorto() {
		return bioOrigenCorto;
	}

	/**
	 * @param bioOrigenCorto the bioOrigenCorto to set
	 */
	public void setBioOrigenCorto(String bioOrigenCorto) {
		this.bioOrigenCorto = bioOrigenCorto;
	}

	/**
	 * @return the bioNumorgCultivarCorto
	 */
	public String getBioNumorgCultivarCorto() {
		return bioNumorgCultivarCorto;
	}

	/**
	 * @param bioNumorgCultivarCorto the bioNumorgCultivarCorto to set
	 */
	public void setBioNumorgCultivarCorto(String bioNumorgCultivarCorto) {
		this.bioNumorgCultivarCorto = bioNumorgCultivarCorto;
	}

	/**
	 * @return the bioDescAtributosAmeCorto
	 */
	public String getBioDescAtributosAmeCorto() {
		return bioDescAtributosAmeCorto;
	}

	/**
	 * @param bioDescAtributosAmeCorto the bioDescAtributosAmeCorto to set
	 */
	public void setBioDescAtributosAmeCorto(String bioDescAtributosAmeCorto) {
		this.bioDescAtributosAmeCorto = bioDescAtributosAmeCorto;
	}

	/**
	 * @return the rowObra
	 */
	public Long getRowObra() {
		return rowObra;
	}

	/**
	 * @param rowObra the rowObra to set
	 */
	public void setRowObra(Long rowObra) {
		this.rowObra = rowObra;
	}

    
}
