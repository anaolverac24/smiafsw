/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import org.primefaces.context.RequestContext;

import mx.gob.semarnat.mia.dao.CatContaminanteDAO;
import mx.gob.semarnat.mia.dao.CatTipoContaminanteDAO;
import mx.gob.semarnat.mia.dao.ContaminanteProyectoDAO;
import mx.gob.semarnat.mia.dao.EtapaProyectoDAO;
import mx.gob.semarnat.mia.dao.UnidadMedidaDAO;
import mx.gob.semarnat.mia.model.CatContaminante;
import mx.gob.semarnat.mia.model.CatTipoContaminante;
import mx.gob.semarnat.mia.model.ContaminanteProyecto;
import mx.gob.semarnat.mia.model.ContaminanteProyectoPK;
import mx.gob.semarnat.mia.model.CriteriosProyecto;
import mx.gob.semarnat.mia.model.EtapaProyecto;
import mx.gob.semarnat.mia.model.EtapaProyectoPK;
import mx.gob.semarnat.mia.model.catalogos.CatUnidadMedida;
import mx.gob.semarnat.mia.view.capitulo1.PaginationHelper;
import mx.gob.semarnat.mia.view.to.CatContaminanteTO;
import mx.gob.semarnat.mia.view.to.CatTipoContaminanteTO;
import mx.gob.semarnat.mia.view.to.CatUnidadMedidaTO;
import mx.gob.semarnat.mia.view.to.ContaminanteProyectoTO;
import mx.gob.semarnat.mia.view.to.EtapaProyectoTO;


/**
 *
 * @author mauricio
 */

@ManagedBean(name = "contaminantes")
@ViewScoped
public class Capitulo2ViewContaminantes extends AbstractTable<ContaminanteProyectoTO> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ContaminanteProyectoDAO contaminanteProyectoDAO;
	EtapaProyectoDAO etapaProyectoDAO;
	CatContaminanteDAO catContaminanteDAO;
	CatTipoContaminanteDAO catTipoContaminanteDAO;
	UnidadMedidaDAO unidadMedidaDAO;
	
	
	ContaminanteProyectoTO to;

	
	List<EtapaProyectoTO> listaEtapaP;
	EtapaProyectoTO etapaSelected;
	
	List<CatTipoContaminanteTO> listaTipo;
	CatTipoContaminanteTO tipoSelected;
	
	List<CatContaminanteTO> listaContaminante;
	CatContaminanteTO contaminanteSelected; 
	
	List<CatUnidadMedidaTO> listaUnidad;
	CatUnidadMedidaTO unidadSeleted;

	private String titleDialog;
	
	private static final String TITLE_ADD="Agregar registro";
	private static final String TITLE_EDIT="Editar registro #";
	
	boolean add=false;
	
	private Integer claveTramite = 0;
	
	private List<ContaminanteProyecto> contaminantesProy = new ArrayList<ContaminanteProyecto>();
    private List<ContaminanteProyecto>conta_seleccionados = new ArrayList<ContaminanteProyecto>();

    /**
	 * Numero secuencial del registro "Generacion, manejo y disposicion de residuos solidos, liquidos y emisiones a la atmosfera" a editar
	 */
	private int rowIndexResSolEditar;
	
	public Capitulo2ViewContaminantes(){
		contaminanteProyectoDAO= new ContaminanteProyectoDAO();
		etapaProyectoDAO= new EtapaProyectoDAO();
		catContaminanteDAO= new CatContaminanteDAO ();
		catTipoContaminanteDAO=new CatTipoContaminanteDAO();
		unidadMedidaDAO= new UnidadMedidaDAO();
		
		listaEtapaP=new ArrayList<EtapaProyectoTO> ();
		listaTipo= new ArrayList<CatTipoContaminanteTO> ();
		listaUnidad= new ArrayList<CatUnidadMedidaTO>();
		
		listTable=transformLista(contaminanteProyectoDAO.findByFolioSerial(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto()));
		loadListaPaginas();
		to=  new ContaminanteProyectoTO();
		
		FacesContext context = FacesContext.getCurrentInstance();
		claveTramite = (Integer) context.getExternalContext().getSessionMap().get("claveTramite");
		
		contaminantesProy = miaDao.getContaminantes(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
		
	}
	
	
   	private List<ContaminanteProyectoTO> transformLista(List<ContaminanteProyecto> listaEntity){
   		List<ContaminanteProyectoTO> listaTO= new ArrayList<ContaminanteProyectoTO>();
   			for(ContaminanteProyecto contaminanteProyecto: listaEntity){
   				listaTO.add(transformEntityToTO(contaminanteProyecto));
   			}
   		return listaTO;
   	}
	
   	
   	private ContaminanteProyectoTO transformEntityToTO(ContaminanteProyecto entity){
   		ContaminanteProyectoTO contaminanteProyectoTO = new ContaminanteProyectoTO();
   		contaminanteProyectoTO.setEtapaId(entity.getEtapaId());
   		EtapaProyecto etapaProyecto=etapaProyectoDAO.find(new EtapaProyectoPK(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), entity.getEtapaId())); 
   		CatTipoContaminante catTipoContaminante=catTipoContaminanteDAO.find(entity.getContaminanteTipo());
   		contaminanteProyectoTO.setContaminanteTipo(entity.getContaminanteTipo());
   		contaminanteProyectoTO.setContaminanteId(entity.getContaminanteProyectoPK().getContaminanteId());
   		contaminanteProyectoTO.setCatContaminante(transformEntityToTO(entity.getCatContaminante()));   //Emisiones, Residuos o Descargas		
   		contaminanteProyectoTO.setEtapaDescripcion(etapaProyecto.getCatEtapa().getEtapaDescripcion());
   		contaminanteProyectoTO.setContaminanteTipoDesc(catTipoContaminante.getTipoContaminanteDescripcion());
   		contaminanteProyectoTO.setFuenteEmisora(entity.getFuenteEmisora());
   		contaminanteProyectoTO.setDescEmiResiDes(entity.getDescEmiResiDes());
   		contaminanteProyectoTO.setContaminanteCantidad(entity.getContaminanteCantidad());
   		CatUnidadMedida  catUnidadMedida =unidadMedidaDAO.find(entity.getCtunClave());
   		contaminanteProyectoTO.setCtunClave(entity.getCtunClave());   		
   		contaminanteProyectoTO.setUnidadMedida( new CatUnidadMedidaTO(catUnidadMedida.getCtunClve(), catUnidadMedida.getCtunAbre(),catUnidadMedida.getCtunDesc(), catUnidadMedida.getEstatus()));
   		contaminanteProyectoTO.setObservaciones(entity.getObservaciones());
   		contaminanteProyectoTO.setObservacionesTrunk(contaminanteProyectoTO.getObservaciones()!=null && contaminanteProyectoTO.getObservaciones().length()>200? contaminanteProyectoTO.getObservaciones().substring(0, 200)+"...":contaminanteProyectoTO.getObservaciones());
   		contaminanteProyectoTO.setDescEmiResiDesTrunk(contaminanteProyectoTO.getDescEmiResiDes()!=null && contaminanteProyectoTO.getDescEmiResiDes().length()>200?contaminanteProyectoTO.getDescEmiResiDes().substring(0,200)+"...":contaminanteProyectoTO.getDescEmiResiDes());
   		contaminanteProyectoTO.setFuenteEmisoraTrunk (contaminanteProyectoTO.getFuenteEmisora()!=null && contaminanteProyectoTO.getFuenteEmisora().length()>200?contaminanteProyectoTO.getFuenteEmisora().substring(0, 200)+"...":contaminanteProyectoTO.getFuenteEmisora());
   		
   		return contaminanteProyectoTO;
   	}
	
   	
   	private CatContaminanteTO transformEntityToTO (CatContaminante entity){
   		CatContaminanteTO to= new CatContaminanteTO();
   		if (entity!=null){
   			to.setContaminanteNombre(entity.getContaminanteNombre());
   			to.setContaminanteId(entity.getContaminanteId());
   		}else{
   			to=null;
   		}
   		return to;
   	}


   	private EtapaProyectoTO transformEntityToTO(EtapaProyecto entity){
   		EtapaProyectoTO to = new EtapaProyectoTO();
   		if (entity.getAnios()>0 || entity.getMeses()>0 || entity.getSemanas()>0){
   			to.setEtapaDescripcionObraAct(entity.getEtapaDescripcionObraAct());
   			to.setEtapaId(entity.getEtapaProyectoPK().getEtapaId());
   			to.setFolioProyecto(entity.getEtapaProyectoPK().getFolioProyecto());
   			to.setSerialProyecto(entity.getEtapaProyectoPK().getSerialProyecto());
   			to.setDescEtapa(entity.getCatEtapa().getEtapaDescripcion());
   		}else{
   			to=null;
   		}
   		return to;
   	}
   	   	
   	private List<EtapaProyectoTO> transformListaEtapa(List<EtapaProyecto> listaEntity){
   		List<EtapaProyectoTO> listaTO= new ArrayList<EtapaProyectoTO>();
   			for(EtapaProyecto contaminanteProyecto: listaEntity){
   				EtapaProyectoTO to=transformEntityToTO(contaminanteProyecto); 
   				if (to!=null){
   					listaTO.add(to);
   				}
   			}
   		return listaTO;
   	}   	
   	
	private CatTipoContaminanteTO transformEntityToTO(CatTipoContaminante entity){
		CatTipoContaminanteTO to = new CatTipoContaminanteTO();
		to.setTipoContaminanteId(entity.getTipoContaminanteId());
		to.setTipoContaminanteDescripcion(entity.getTipoContaminanteDescripcion());
   		return to;
   	}
	
   	private List<CatTipoContaminanteTO> transformListaTipo(List<CatTipoContaminante> listaEntity){
   		List<CatTipoContaminanteTO> listaTO= new ArrayList<CatTipoContaminanteTO>();
   			for(CatTipoContaminante entity: listaEntity){
   			
   					listaTO.add(transformEntityToTO(entity));
   			
   			}
   		return listaTO;
   	} 
   	
   	
   	private List <CatUnidadMedidaTO> transformListaUnidad(List <CatUnidadMedida> listaEntity){
   		List <CatUnidadMedidaTO> listaTO= new ArrayList <CatUnidadMedidaTO>(); 
   		for(CatUnidadMedida entity:listaEntity){
   			listaTO.add(new CatUnidadMedidaTO(entity.getCtunClve(), entity.getCtunAbre(),entity.getCtunDesc(), entity.getEstatus()));
   		}
   		return listaTO;
   	}
   	
	public PaginationHelper getPagination() {
		
		if (pagination == null) {

			pagination = new PaginationHelper(NUM_MAX_PAG) {
				@Override
				public int getItemsCount() {
					return contaminanteProyectoDAO.countByFolioSerial(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
				}

				@SuppressWarnings({ "rawtypes", "unchecked" })
				@Override
				public DataModel createPageDataModel() {

					return new ListDataModel(transformLista(contaminanteProyectoDAO.findRangeByFolioSerial(
							new int[] { getPageFirstItem(), getPageFirstItem() + getPageSize() },
							getProyecto().getProyectoPK().getFolioProyecto(),getProyecto().getProyectoPK().getSerialProyecto())));
				}
			};
		}
		return pagination;
	}

	@Override
	void updateCurrentItem() {
		
		int count = contaminanteProyectoDAO.countByFolioSerial(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
		if (selectedItemIndex >= count) {
			// selected index cannot be bigger than number of items:
			selectedItemIndex = count - UNO;
			// go to previous page if last page disappeared:
			if (pagination.getPageFirstItem() >= count) {
				pagination.previousPage();
			}
		}
		if (selectedItemIndex >= CERO) {
			listTable = transformLista(contaminanteProyectoDAO.findRangeByFolioSerial(
					new int[] { pagination.getPageFirstItem(),
							pagination.getPageFirstItem() + pagination.getPageSize() },
					getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto()));
		}
	}

	public void loadAdd(){
		
		titleDialog= TITLE_ADD;
		add=Boolean.TRUE;
		RequestContext.getCurrentInstance().update("formDialog:myDialogEdit");
		RequestContext.getCurrentInstance().reset("formDialog:pnlAddContaminante");
		loadSelect();
		to= new ContaminanteProyectoTO();		

		etapaSelected= null;		
		tipoSelected= null;		
		contaminanteSelected= null; 		
		unidadSeleted= null;
		
//		RequestContext.getCurrentInstance().update("formDialog:pnlAddContaminante");
//		RequestContext.getCurrentInstance().update("formDialog:selectAllEntities");
		RequestContext.getCurrentInstance().execute("myDialogEdit.show();");
	}
	
	private void loadSelect(){
		
		List<EtapaProyecto> listaEtapaProy= etapaProyectoDAO.findByFolioSerial(getProyecto().getProyectoPK().getFolioProyecto(),getProyecto().getProyectoPK().getSerialProyecto());		
		listaEtapaP.clear();
		listaTipo.clear();
		listaEtapaP=transformListaEtapa(listaEtapaProy);
		listaTipo= transformListaTipo(catTipoContaminanteDAO.findAll());
		listaUnidad= transformListaUnidad(unidadMedidaDAO.findAll());
	}
	
	
	
	public void changeValueTipo(){
		listaContaminante= new ArrayList<CatContaminanteTO>();
		if (tipoSelected!=null && tipoSelected.getTipoContaminanteId()!=null){
			List<CatContaminante>  lCont=catContaminanteDAO.findByIdTipo(tipoSelected.getTipoContaminanteId());
			for(CatContaminante entity:lCont){
				listaContaminante.add(transformEntityToTO(entity));
			}
		}
	}
	
	public void guardarAvance() {
		RequestContext reqcontEnv = RequestContext.getCurrentInstance();
		try {
			if (listTable.size() >= 1) {
				try {
					if (getProyecto().getNsub().equals(new Short("2"))) {
						if (claveTramite == 66 || claveTramite == 67) {// Si es una mia regional se debe guardar 2%
							miaDao.guardarAvance(getProyecto(), new Short("2"), "216");
						} else {
							miaDao.guardarAvance(getProyecto(), new Short("3"), "216");
						}
					} else {
						miaDao.guardarAvance(getProyecto(), new Short("2"), "216");
					}
				} catch (Exception e) {
					System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
				}
			}

			reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
		} catch (Exception e) {
			reqcontEnv.execute("mensajes('No se ha podido guardar la información, Intente más tarde.', 'success')");
			e.printStackTrace();
		}
	}

	public String tieneContaminantes(){
		System.out.println("Comprobando Chechbox´s");
		System.out.println(conta_seleccionados.size()+" seleccionados");
		if(!conta_seleccionados.isEmpty()){
			RequestContext context = RequestContext.getCurrentInstance();
			System.out.println("Abriendo modal de confirmación");
		    context.execute("myDialogDelete.show()");	    
		}
		System.out.println("Saliendo del IF");
		return "residuosSolidos";
	}
	
	public String eliminaContaminantes() throws Exception {
		System.out.println("Eliminación Afirmativa");
	    for (ContaminanteProyecto contaminanteSeleccionado : conta_seleccionados) {              
	    	contaminantesProy.remove(contaminanteSeleccionado);
	        miaDao.eliminarCriterio(contaminanteSeleccionado.getContaminanteProyectoPK().getContaminanteId());   
	    }   
	   return "residuosSolidos";
	}
	
	/**
	 * @return the listaEtapaP
	 */
	public List<EtapaProyectoTO> getListaEtapaP() {
		return listaEtapaP;
	}


	/**
	 * @param listaEtapaP the listaEtapaP to set
	 */
	public void setListaEtapaP(List<EtapaProyectoTO> listaEtapaP) {
		this.listaEtapaP = listaEtapaP;
	}


	/**
	 * @return the etapaSelected
	 */
	public EtapaProyectoTO getEtapaSelected() {
		return etapaSelected;
	}


	/**
	 * @param etapaSelected the etapaSelected to set
	 */
	public void setEtapaSelected(EtapaProyectoTO etapaSelected) {
		this.etapaSelected = etapaSelected;
	}


	/**
	 * @return the listaTipo
	 */
	public List<CatTipoContaminanteTO> getListaTipo() {
		return listaTipo;
	}


	/**
	 * @param listaTipo the listaTipo to set
	 */
	public void setListaTipo(List<CatTipoContaminanteTO> listaTipo) {
		this.listaTipo = listaTipo;
	}


	/**
	 * @return the tipoSelected
	 */
	public CatTipoContaminanteTO getTipoSelected() {
		return tipoSelected;
	}


	/**
	 * @param tipoSelected the tipoSelected to set
	 */
	public void setTipoSelected(CatTipoContaminanteTO tipoSelected) {
		this.tipoSelected = tipoSelected;
	}


	/**
	 * @return the listaContaminante
	 */
	public List<CatContaminanteTO> getListaContaminante() {
		return listaContaminante;
	}


	/**
	 * @param listaContaminante the listaContaminante to set
	 */
	public void setListaContaminante(List<CatContaminanteTO> listaContaminante) {
		this.listaContaminante = listaContaminante;
	}


	/**
	 * @return the contaminanteSelected
	 */
	public CatContaminanteTO getContaminanteSelected() {
		return contaminanteSelected;
	}


	/**
	 * @param contaminanteSelected the contaminanteSelected to set
	 */
	public void setContaminanteSelected(CatContaminanteTO contaminanteSelected) {
		this.contaminanteSelected = contaminanteSelected;
	}


	/**
	 * @return the to
	 */
	public ContaminanteProyectoTO getTo() {
		return to;
	}


	/**
	 * @param to the to to set
	 */
	public void setTo(ContaminanteProyectoTO to) {
		this.to = to;
	}


	/**
	 * @return the unidadMedidaDAO
	 */
	public UnidadMedidaDAO getUnidadMedidaDAO() {
		return unidadMedidaDAO;
	}


	/**
	 * @param unidadMedidaDAO the unidadMedidaDAO to set
	 */
	public void setUnidadMedidaDAO(UnidadMedidaDAO unidadMedidaDAO) {
		this.unidadMedidaDAO = unidadMedidaDAO;
	}


	/**
	 * @return the unidadSeleted
	 */
	public CatUnidadMedidaTO getUnidadSeleted() {
		return unidadSeleted;
	}


	/**
	 * @param unidadSeleted the unidadSeleted to set
	 */
	public void setUnidadSeleted(CatUnidadMedidaTO unidadSeleted) {
		this.unidadSeleted = unidadSeleted;
	}


	/**
	 * @return the listaUnidad
	 */
	public List<CatUnidadMedidaTO> getListaUnidad() {
		return listaUnidad;
	}


	/**
	 * @param listaUnidad the listaUnidad to set
	 */
	public void setListaUnidad(List<CatUnidadMedidaTO> listaUnidad) {
		this.listaUnidad = listaUnidad;
	}
	


	public void guardarAll(){
		if (add){
			contaminanteProyectoDAO.create(transformTOToEntity(to,add));
		}else{
			contaminanteProyectoDAO.edit(transformTOToEntity(to,add));
		}
		RequestContext context = RequestContext.getCurrentInstance();
	//	listTable=transformLista(contaminanteProyectoDAO.findByFolioSerial(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto()));
	//	dataTable=null;
	//	loadListaPaginas();
		context.reset("foDatosGen:listaReiaProyObras");
		context.execute("myDialogEdit.hide();");
		context.execute("modalAlert.show();");
		
	}

	
	private  ContaminanteProyecto transformTOToEntity( ContaminanteProyectoTO to, boolean add){
		ContaminanteProyecto entity=null;
		if (add){
		entity= new ContaminanteProyecto(new 	ContaminanteProyectoPK(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(),
											 (short)(contaminanteProyectoDAO.getLastByFolioSerial(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto())+1)));
		}else{
			entity=contaminanteProyectoDAO.find(new 	ContaminanteProyectoPK(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(),to.getContaminanteId()));
		}
		entity.setEtapaId(etapaSelected.getEtapaId());
		entity.setContaminanteTipo(tipoSelected.getTipoContaminanteId());
		entity.setContaminanteEmisiones(contaminanteSelected.getContaminanteId());
		entity.setContaminanteCantidad(to.getContaminanteCantidad());
		entity.setCtunClave(unidadSeleted.getCtunClve());
		entity.setObservaciones(to.getObservaciones());
		entity.setDescEmiResiDes(to.getDescEmiResiDes());
		entity.setFuenteEmisora(to.getFuenteEmisora());		
   		return entity;
   	}

	public void cargarEdicion(ContaminanteProyectoTO conProTO){
		
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		// Número secuencial a mostrar en "Editar registro #"
		rowIndexResSolEditar = Integer.parseInt(params.get("prowindexRS").toString());
		
		RequestContext.getCurrentInstance().reset("formDialog:pnlAddContaminante");
				
		loadAdd();
		add=Boolean.FALSE;
		titleDialog = TITLE_EDIT + " " + rowIndexResSolEditar;
		RequestContext.getCurrentInstance().update("formDialog:myDialogEdit");
		to= new ContaminanteProyectoTO();
		to.setEtapaId(conProTO.getEtapaId());
		to.setContaminanteTipo(conProTO.getContaminanteTipo());
		to.setCatContaminante( conProTO.getCatContaminante());
		to.setCtunClave(conProTO.getCtunClave());
		to.setObservaciones(conProTO.getObservaciones());
		to.setContaminanteCantidad(conProTO.getContaminanteCantidad());
		to.setFuenteEmisora(conProTO.getFuenteEmisora());
		to.setDescEmiResiDes(conProTO.getDescEmiResiDes());
		to.setContaminanteId(conProTO.getContaminanteId());
		//this.to=conProTO;
		for(EtapaProyectoTO et: listaEtapaP){
			if(et.getEtapaId()==conProTO.getEtapaId()){
				etapaSelected=et;
				break;
			}
		}
		
		
		
		for(CatTipoContaminanteTO tipo: listaTipo){
			if(tipo.getTipoContaminanteId().equals(conProTO.getContaminanteTipo())){
				tipoSelected=tipo;
				break;
			}
		}
		changeValueTipo();
		for(CatContaminanteTO cont:listaContaminante){
			if(cont.getContaminanteId().equals(conProTO.getCatContaminante().getContaminanteId())){
				contaminanteSelected=cont;
				break;
			}
		}
		
		
		for(CatUnidadMedidaTO uni:listaUnidad){
			if(uni.getCtunClve().equals(conProTO.getCtunClave())){
				unidadSeleted=uni;
				break;
			}
		}		
		
//		RequestContext.getCurrentInstance().update("formDialog:pnlAddContaminante");
//		RequestContext.getCurrentInstance().update("formDialog:selectAllEntities");
		RequestContext.getCurrentInstance().execute("myDialogEdit.show();");
	}
	
	
	
	public void previoDelete() {
		List<ContaminanteProyectoTO> listaDelete = getSelectedRow();
		if (listaDelete != null && !listaDelete.isEmpty()) {
			RequestContext.getCurrentInstance().execute("myDialogDelete.show();");
		}
	}

	public void delete() {
		List<ContaminanteProyectoTO> listaDelete = getSelectedRow();
		for (ContaminanteProyectoTO to : listaDelete) {			
			ContaminanteProyecto entity = contaminanteProyectoDAO.find(new ContaminanteProyectoPK(getProyecto().getProyectoPK().getFolioProyecto(),
					getProyecto().getProyectoPK().getSerialProyecto(),to.getContaminanteId()))  ;			
			contaminanteProyectoDAO.remove(entity);

		}
		RequestContext context = RequestContext.getCurrentInstance();
		//listTable=transformLista(contaminanteProyectoDAO.findByFolioSerial(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto()));
		//dataTable=null;
		//loadListaPaginas();
		context.reset("foDatosGen:panelConsulta");
		context.update("foDatosGen:panelConsulta");
		context.execute("myDialogDelete.hide();");
		context.execute("dlgAvisoBorrado.show();");
//		RequestContext.getCurrentInstance().update("foDatosGen:listaReiaProyObras");
//		RequestContext.getCurrentInstance().execute("myDialogDelete.hide();");
//		
	}


	/**
	 * @return the titleDialog
	 */
	public String getTitleDialog() {
		return titleDialog;
	}


	/**
	 * @param titleDialog the titleDialog to set
	 */
	public void setTitleDialog(String titleDialog) {
		this.titleDialog = titleDialog;
	}


	/**
	 * @return the claveTramite
	 */
	public Integer getClaveTramite() {
		return claveTramite;
	}


	/**
	 * @param claveTramite the claveTramite to set
	 */
	public void setClaveTramite(Integer claveTramite) {
		this.claveTramite = claveTramite;
	}


	/**
	 * @return the contaminantesProy
	 */
	public List<ContaminanteProyecto> getContaminantesProy() {
		return contaminantesProy;
	}


	/**
	 * @param contaminantesProy the contaminantesProy to set
	 */
	public void setContaminantesProy(List<ContaminanteProyecto> contaminantesProy) {
		this.contaminantesProy = contaminantesProy;
	}


	/**
	 * @return the conta_seleccionados
	 */
	public List<ContaminanteProyecto> getConta_seleccionados() {
		return conta_seleccionados;
	}


	/**
	 * @param conta_seleccionados the conta_seleccionados to set
	 */
	public void setConta_seleccionados(List<ContaminanteProyecto> conta_seleccionados) {
		this.conta_seleccionados = conta_seleccionados;
	}


	/**
	 * @return the rowIndexResSolEditar
	 */
	public int getRowIndexResSolEditar() {
		return rowIndexResSolEditar;
	}


	/**
	 * @param rowIndexResSolEditar the rowIndexResSolEditar to set
	 */
	public void setRowIndexResSolEditar(int rowIndexResSolEditar) {
		this.rowIndexResSolEditar = rowIndexResSolEditar;
	}
	
}
