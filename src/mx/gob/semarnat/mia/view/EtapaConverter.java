package mx.gob.semarnat.mia.view;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import mx.gob.semarnat.mia.view.capitulo1.SelectItemsBaseConverter;
import mx.gob.semarnat.mia.view.to.EtapaProyectoTO;

/**
 * The Class PersonaConverter.
 */
@ManagedBean (name="etapaConverter")
public class EtapaConverter extends SelectItemsBaseConverter {
    
    /* (non-Javadoc)
     * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof EtapaProyectoTO) || ((EtapaProyectoTO) value).getEtapaId() == 0) {
            return null;
        }

        return String.valueOf(((EtapaProyectoTO) value).getEtapaId() );
    }
}
