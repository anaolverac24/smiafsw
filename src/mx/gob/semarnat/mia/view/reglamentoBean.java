package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import oracle.net.aso.c;

import org.hibernate.mapping.Array;






import mx.gob.semarnat.mia.model.CatReglamento;
import mx.gob.semarnat.mia.model.ReglamentoProyecto;


@ManagedBean(name="reglamento")
@ViewScoped
public class reglamentoBean extends CapitulosComentarios implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2168266480158086178L;
	
	private ReglamentoProyecto objeto;
	private List<CatReglamento> catalogo;
	
	
	public reglamentoBean()
	{
		objeto = new ReglamentoProyecto();
		catalogo = new ArrayList<CatReglamento>();
	}
	
	public void listaReglamentos()
	{
		objeto=null; 
		
		catalogo = miaDao.getCatReglamento();
		
		if(catalogo.size()!=0)
		{
			System.out.println("La lista de catalogos tamaño: "+catalogo.size());
			
			for(CatReglamento c: catalogo)
			{
				System.out.println("ID DEL REGLAMENTO: "+ c.getReglamentoId() + "   Descripción: " +c.getReglamentoDescripcion());
			}
			
		}		
		else
		{
			System.out.println("La lista de catalogos viene vácia");
		}
		
	}

	
	
	
	////////////////////////////////GETTERS AND SETTERS//////////////////////////////////////////////////
	
	public ReglamentoProyecto getObjeto() {
		return objeto;
	}

	public void setObjeto(ReglamentoProyecto objeto) {
		this.objeto = objeto;
	}

	public List<CatReglamento> getCatalogo() {
		return catalogo;
	}

	public void setCatalogo(List<CatReglamento> catalogo) {
		this.catalogo = catalogo;
	}
}
