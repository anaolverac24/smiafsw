/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import mx.gob.semarnat.mia.dao.MiaDao;
import mx.gob.semarnat.mia.dao.SigeiaDao;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.Proysig;
import mx.gob.semarnat.mia.model.UsoSueloVeget;

/**
 * @author mauricio
 */
@ManagedBean
@ViewScoped
public class SIGEIAView implements Serializable {

    private MiaDao miaDao = new MiaDao();
    private SigeiaDao sinDao = new SigeiaDao();
    private Proyecto proyecto = new Proyecto();
    private List<Object[]> edoAfectado = new ArrayList<Object[]>();
    private List<Object[]> todosEdos = new ArrayList<Object[]>();
    private List<Object[]> proySig = new ArrayList<Object[]>();
    //Dimensiones del proyecto
    private List<Proysig> dimensionesProy = new ArrayList<Proysig>();
    private List<UsoSueloVeget> dimensionesUso = new ArrayList<UsoSueloVeget>();
    private List<Object[]> dimensionesUso2 = new ArrayList<Object[]>();
    //numero de estados afectados
    private int estadosAfectados = 0;

    public SIGEIAView() {
        // Recupera proyecto de sesion
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");

        //Cargar Proyecto
        try {
            this.proyecto = miaDao.proyecto(folioProyecto, serialProyecto);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }

        if (proyecto == null) {
            this.proyecto = new Proyecto(folioProyecto, serialProyecto);
        }

        todosEdos = estadosAfetados();
        estadosAfectados = sinDao.soloEstadosAfectados(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());

        try {
//          dimensionesProy = sinDao.proySig(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            proySig = sinDao.proySig2(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            //dimensionesUso = sinDao.usoSueloVegetacion(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
            dimensionesUso2 = sinDao.usoSueloVegetacion2(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Double getTotObra() {
        return (Double) sinDao.proySigTotObra(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    }

    public Double getTotPredio() {
        return (Double) sinDao.proySigTotPredio(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
    }

    public final List<Object[]> estadosAfetados() {
//        List<Object[]> p = dao.proySigSum(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());

        List<Object[]> pObra = sinDao.mpiosSumObra(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
        List<Object[]> pPredio = sinDao.mpiosSumPred(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());

        setEdoAfectado(null);
        todosEdos = null;
        String mensaje = "";

        Integer id;
//
////        String aux2;
//
        //---------------si trae obras o predios
        if (!pObra.isEmpty() || !pPredio.isEmpty()) {
            //----------si trae obras Y predios
            if (!pObra.isEmpty() && !pPredio.isEmpty()) {
                id = Integer.parseInt(pObra.get(pObra.size() - 1)[0].toString());
                System.out.print("Obra afectada --->>>  "+id);
                if (id != 0) {
                    edoAfectado = sinDao.edoMasAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
                }
            }
            //----------si trae obras Y NO predios
            if (!pObra.isEmpty() && pPredio.isEmpty()) {
                id = Integer.parseInt(pObra.get(pObra.size() - 1)[0].toString());
                System.out.print("Predios afectada --->>>  "+id);
                if (id != 0) {
                    edoAfectado = sinDao.edoMasAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
                }
            }
            //----------si NO trae obras Y SI predios
            if (pObra.isEmpty() && !pPredio.isEmpty()) {
                id = Integer.parseInt(pPredio.get(pPredio.size() - 1)[0].toString());
                if (id != 0) {
                    edoAfectado = sinDao.edoMasAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), id);
                    setEdoAfectado(edoAfectado);
                }
            }
        }

        todosEdos = sinDao.edoTodosAfectado(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getClaveProyecto(), getProyecto().getProyectoPK().getSerialProyecto());

        if (pObra.isEmpty() || pPredio.isEmpty()) {
            //mensaje = "Debe georrefenciar correctamente su proyecto";
            todosEdos = null;
        }
        if (mensaje.length() != 0) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Debe georrefenciar correctamente su proyecto", null);
            FacesContext.getCurrentInstance().addMessage(null, message);
        }

        try{
            FacesContext context = FacesContext.getCurrentInstance();
            context.getExternalContext().getSessionMap().put("clvMunici", todosEdos.get(0)[1]);
        }catch(Exception e3){
            
        }
        
        return todosEdos;
    }

    /**
     * @return Url SIGEIA
     * @throws NoSuchAlgorithmException
     */
    public String getUrl() throws NoSuchAlgorithmException {
        String url = Constantes.URL_SIGEIA;
        String secureParam = "usuario=" + Constantes.USUARIO_SIGEIA
                + "&password=" + Constantes.PASSWORD_SIGEIA
                + "&noFolio=" + proyecto.getProyectoPK().getFolioProyecto()
                + "&claveProyecto="+proyecto.getClaveProyecto()
                + "&version=" + proyecto.getProyectoPK().getSerialProyecto()
                + "&tipoTramite=" + Constantes.TRAMITE
                + "&estatus=" + proyecto.getEstatusProyecto();
        return url.concat(secureParam);
    }
    
    public static String getUrlPdf(String vFolio, String vClave, String vSerial, String vEstatus) {
        String url = Constantes.URL_SIGEIA;
        String secureParam = "usuario=" + Constantes.USUARIO_SIGEIA
                + "&password=" + Constantes.PASSWORD_SIGEIA
                + "&noFolio=" + vFolio
                + "&claveProyecto="+vClave+"&version=" + vSerial
                + "&tipoTramite=" + Constantes.TRAMITE
                + "&estatus=" + vEstatus;
        return url.concat(secureParam);
    }
    
    

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the edoAfectado
     */
    public List<Object[]> getEdoAfectado() {
        return edoAfectado;
    }

    /**
     * @param edoAfectado the edoAfectado to set
     */
    public void setEdoAfectado(List<Object[]> edoAfectado) {
        this.edoAfectado = edoAfectado;
    }

    /**
     * @return the todosEdos
     */
    public List<Object[]> getTodosEdos() {
        return todosEdos;
    }

    /**
     * @param todosEdos the todosEdos to set
     */
    public void setTodosEdos(List<Object[]> todosEdos) {
        this.todosEdos = todosEdos;
    }

    /**
     * @return the dimensionesProy
     */
    public List<Proysig> getDimensionesProy() {
        return dimensionesProy;
    }

    /**
     * @param dimensionesProy the dimensionesProy to set
     */
    public void setDimensionesProy(List<Proysig> dimensionesProy) {
        this.dimensionesProy = dimensionesProy;
    }

    /**
     * @return the dimensionesUso
     */
    public List<UsoSueloVeget> getDimensionesUso() {
        return dimensionesUso;
    }

    /**
     * @param dimensionesUso the dimensionesUso to set
     */
    public void setDimensionesUso(List<UsoSueloVeget> dimensionesUso) {
        this.dimensionesUso = dimensionesUso;
    }

    /**
     * @return the proySig
     */
    public List<Object[]> getProySig() {
        return proySig;
    }

    /**
     * @param proySig the proySig to set
     */
    public void setProySig(List<Object[]> proySig) {
        this.proySig = proySig;
    }

    /**
     * @return the dimensionesUso2
     */
    public List<Object[]> getDimensionesUso2() {
        return dimensionesUso2;
    }

    /**
     * @param dimensionesUso2 the dimensionesUso2 to set
     */
    public void setDimensionesUso2(List<Object[]> dimensionesUso2) {
        this.dimensionesUso2 = dimensionesUso2;
    }

    public int getEstadosAfectados() {
		return estadosAfectados;
		
	}
    
    public void setEstadosAfectados(int estadosAfectados) {
		this.estadosAfectados = estadosAfectados;
	}

}
