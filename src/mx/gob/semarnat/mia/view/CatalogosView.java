/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import com.sun.org.apache.bcel.internal.generic.NEW;

import mx.gob.semarnat.mia.dao.MiaDao;
import mx.gob.semarnat.mia.model.CatClasificacion;
import mx.gob.semarnat.mia.model.CatClasificacionB;
import mx.gob.semarnat.mia.model.CatContaminante;
import mx.gob.semarnat.mia.model.CatConvenios;
import mx.gob.semarnat.mia.model.CatCriterio;
import mx.gob.semarnat.mia.model.CatEstEsp;
import mx.gob.semarnat.mia.model.CatEtapa;
import mx.gob.semarnat.mia.model.CatLey;
import mx.gob.semarnat.mia.model.CatNorma;
import mx.gob.semarnat.mia.model.CatPoet;
import mx.gob.semarnat.mia.model.CatReferencia;
import mx.gob.semarnat.mia.model.CatReglamento;
import mx.gob.semarnat.mia.model.CatServicio;
import mx.gob.semarnat.mia.model.CatSustanciaAltamRiesgosa;
import mx.gob.semarnat.mia.model.CatTemporalidad;
import mx.gob.semarnat.mia.model.CatTipoContaminante;
import mx.gob.semarnat.mia.model.CatTipoImpacto;
import mx.gob.semarnat.mia.model.EtapaProyecto;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.catalogos.CatTipoAsen;
import mx.gob.semarnat.mia.model.catalogos.CatUnidadMedida;
import mx.gob.semarnat.mia.model.catalogos.CatVialidad;

/**
 *
 * @author mauricio
 */
@ManagedBean
@ViewScoped
public class CatalogosView implements Serializable {

    private final MiaDao miaDao = new MiaDao();
    private Proyecto proyecto = new Proyecto();
    private final List<SelectItem> catReferencia = new ArrayList<SelectItem>();
    private final List<SelectItem> catTemporalidad = new ArrayList<SelectItem>();
    private final List<SelectItem> catCriterios = new ArrayList<SelectItem>();
    private final List<SelectItem> catServicio = new ArrayList<SelectItem>();
    private final List<SelectItem> catEtapa = new ArrayList<SelectItem>();
    private final List<SelectItem> catEtapasDelProyecto = new ArrayList<SelectItem>();
    private List<CatEtapa> catEtapasDelProyectoShort = new ArrayList<CatEtapa>();
    private final List<SelectItem> catSustanciaAltaRiesgo = new ArrayList<SelectItem>();
    private final List<SelectItem> catContaminante = new ArrayList<SelectItem>();
    private final List<SelectItem> catLeyFederal = new ArrayList<SelectItem>();
    private final List<SelectItem> catLeyEstatal = new ArrayList<SelectItem>();
    private final List<SelectItem> catReglamento = new ArrayList<SelectItem>();
    private final List<SelectItem> catNormas = new ArrayList<SelectItem>();
    private final List<SelectItem> catConvenios = new ArrayList<SelectItem>();
    private final List<SelectItem> catClasificacion = new ArrayList<SelectItem>();
    private final List<SelectItem> catClasificacionB = new ArrayList<SelectItem>();
    private final List<SelectItem> catTipoImpacto = new ArrayList<SelectItem>();
    private final List<SelectItem> catTipoContaminante = new ArrayList<SelectItem>();
    private List<CatNorma> catalogoNormas = miaDao.getCatNormas();
    private List<CatVialidad> tiposVialidad = miaDao.getTipoVialidad();
    private List<CatTipoAsen> tipoAsentamiento = miaDao.getTipoAsentamientos();
    private List<CatUnidadMedida> catMedida = miaDao.getCatUnidadMedida();
    private CatUnidadMedida catMedidaObj = null; 
    private List<CatTipoContaminante> catTipoCont = miaDao.getCatTipoContaminante();
    private final List<SelectItem> catPoet = new ArrayList<>();
    private final List<SelectItem> catEstEsps= new ArrayList<SelectItem>();
    

    public List<CatEtapa> getCatEtapasDelProyectoShort() {
        if (catEtapasDelProyectoShort.isEmpty()) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.getExternalContext().getSessionMap().get("userFolioProy");
            String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
            Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");

            List<EtapaProyecto> l = miaDao.getEtapaProyecto(folioProyecto, serialProyecto);
            for (EtapaProyecto e : l) {
                int total = e.getAnios() + e.getMeses();
                total += (e.getSemanas() != null ? e.getSemanas() : 0);
                if (total > 0) {
                    catEtapasDelProyectoShort.add(e.getCatEtapa());
                }
            }

//            catEtapasDelProyectoShort = miaDao.getCatEtapaProyecto();
        }
        return catEtapasDelProyectoShort;
    }

    
    public List<SelectItem> getCatTipoContaminante() {
        if (catTipoContaminante.isEmpty()) {
            List<CatTipoContaminante> t = miaDao.getCatTipoContaminante();
            for (CatTipoContaminante c : t) {
                SelectItem tmp = new SelectItem(c, c.getTipoContaminanteDescripcion());
                catTipoContaminante.add(tmp);
            }
        }
        return catTipoContaminante;
    }

    public List<SelectItem> getCatTipoImpacto() {
        if (catTipoImpacto.isEmpty()) {
            List<CatTipoImpacto> t = miaDao.getCatTipoImpacto();
            for (CatTipoImpacto c : t) {
                SelectItem tmp = new SelectItem(c, c.getTipoImpactoDescripcion());
                catTipoImpacto.add(tmp);
            }
        }
        return catTipoImpacto;
    }

    public List<SelectItem> getCatReferencia() {
        if (catReferencia.isEmpty()) {
            List<CatReferencia> t = miaDao.getCatReferencia();
            for (CatReferencia c : t) {
                SelectItem tmp = new SelectItem(c, c.getReferenciaDescripcion());
                catReferencia.add(tmp);
            }
        }
        return catReferencia;
    }

    public List<SelectItem> getCatTemporalidad() {
        if (catTemporalidad.isEmpty()) {
            List<CatTemporalidad> t = miaDao.getCatTemporalidad();
            for (CatTemporalidad c : t) {
                SelectItem tmp = new SelectItem(c, c.getTemporalidadDescripcion());
                catTemporalidad.add(tmp);
            }
        }
        return catTemporalidad;
    }

    public List<SelectItem> getCatClasificacion() {
        if (catClasificacion.isEmpty()) {
            List<CatClasificacion> n = miaDao.getCatClasificacion();
            for (CatClasificacion c : n) {
                SelectItem s = new SelectItem(c, c.getClasificacionDescripcion());
                catClasificacion.add(s);
            }
        }
        return catClasificacion;
    }

    public List<SelectItem> getCatNormas() {
        if (catNormas.isEmpty()) {
            List<CatNorma> n = miaDao.getCatNormas();
            for (CatNorma c : n) {
                SelectItem s = new SelectItem(c, c.getNormaNombre());
                catNormas.add(s);
            }
        }
        return catNormas;
    }

    public List<SelectItem> getCatConvenios() {
        if (catConvenios.isEmpty()) {
            List<CatConvenios> n = miaDao.getCatConvenios();
            for (CatConvenios c : n) {
                SelectItem s = new SelectItem(c, c.getConvenioDescripcion());
                catConvenios.add(s);
            }
        }
        return catConvenios;
    }

    public List<SelectItem> getCatReglamento() {
        if (catReglamento.isEmpty()) {
            List<CatReglamento> e = miaDao.getCatReglamento();
            for (CatReglamento r : e) {
                SelectItem tmp = new SelectItem(r, r.getReglamentoDescripcion());
                catReglamento.add(tmp);
            }
        }
        return catReglamento;
    }

    public List<SelectItem> getCatLeyFederal() {
        if (catLeyFederal.isEmpty()) {
            List<CatLey> t = miaDao.getCatLeyFederal();
            for (CatLey c : t) {
                SelectItem tmp = new SelectItem(c, c.getLeyNombre());
                catLeyFederal.add(tmp);
            }
        }
        return catLeyFederal;
    }

    public List<SelectItem> getCatLeyEstatal() {
        if (catLeyEstatal.isEmpty()) {
            List<CatLey> t = miaDao.getCatLeyEstatal();
            for (CatLey c : t) {
                SelectItem tmp = new SelectItem(c, c.getLeyNombre());
                catLeyEstatal.add(tmp);
            }
        }
        return catLeyFederal;
    }

    public List<SelectItem> getCatContaminante() {
        if (catContaminante.isEmpty()) {
            List<CatContaminante> catCon = miaDao.getContaminantes();
            for (CatContaminante c : catCon) {
                SelectItem s = new SelectItem(c, c.getContaminanteNombre());
                catContaminante.add(s);
            }
        }
        return catContaminante;
    }

    public List<SelectItem> getCatCriterios() {
        if (catCriterios.isEmpty()) {
            List<CatCriterio> catCriteriosList = miaDao.getCriterios();
            for (CatCriterio c : catCriteriosList) {
                SelectItem s = new SelectItem(c, c.getCriterioNombre());
                catCriterios.add(s);
            }
        }

        return catCriterios;
    }
    
    
    public List<SelectItem>getCatEstEsps()
    {
    	if(catEstEsps.isEmpty())
    	{
    		List<CatEstEsp> catEstEspslist = miaDao.getEstudiosEspeciales();
    		for(CatEstEsp esp: catEstEspslist)
    		{
    			SelectItem s = new SelectItem(esp, esp.getEstudioDescripcion());
    			catEstEsps.add(s);
    		}
    	}
    	return catEstEsps;
    }

    public List<SelectItem> getCatServicio() {
        if (catServicio.isEmpty()) {
            List<CatServicio> tmp = miaDao.getServicios();
            for (CatServicio c : tmp) {
                SelectItem s = new SelectItem(c, c.getServicioNombre());
                catServicio.add(s);
            }
        }
        return catServicio;
    }

    public List<SelectItem> getCatEtapa() {
        if (catEtapa.isEmpty()) {
            List<CatEtapa> tmp = miaDao.getEtapa();
            for (CatEtapa c : tmp) {
                SelectItem s = new SelectItem(c, c.getEtapaDescripcion());
                catEtapa.add(s);
            }
        }
        return catEtapa;
    }

    public List<SelectItem> getCatEtapasDelProyecto() {
//        if (catEtapasDelProyecto.isEmpty()) {
//            List<CatEtapa> tmp = miaDao.getEtapa();
//            for (CatEtapa c : tmp) {
//                SelectItem s = new SelectItem(c, c.getEtapaDescripcion());
//                catEtapa.add(s);
//            }
//        }
        if (catEtapasDelProyecto.isEmpty()) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.getExternalContext().getSessionMap().get("userFolioProy");
            String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
            Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");

            List<EtapaProyecto> l = miaDao.getEtapaProyecto(folioProyecto, serialProyecto);
            for (EtapaProyecto e : l) {
                int total = e.getAnios() + e.getMeses();
                total += (e.getSemanas() != null ? e.getSemanas() : 0);
                if (total > 0) {
                	SelectItem s = new SelectItem(e.getCatEtapa(), e.getCatEtapa().getEtapaDescripcion());
                	catEtapasDelProyecto.add(s);
                }
            }

//            catEtapasDelProyectoShort = miaDao.getCatEtapaProyecto();
        }
        return catEtapasDelProyecto;
    }

    public List<SelectItem> getCatSustanciaAltaRiesgo() {
        if (catSustanciaAltaRiesgo.isEmpty()) {
            List<CatSustanciaAltamRiesgosa> c = miaDao.getSustanciaAltaRiesgosa();
            for (CatSustanciaAltamRiesgosa ca : c) {
                SelectItem s = new SelectItem(ca, ca.getSustanciaDescripcion());
                catSustanciaAltaRiesgo.add(s);
            }
        }
        return catSustanciaAltaRiesgo;
    }

    /**
     * @return the tiposVialidad
     */
    public List<CatVialidad> getTiposVialidad() {
        return tiposVialidad;
    }

    /**
     * @param tiposVialidad the tiposVialidad to set
     */
    public void setTiposVialidad(List<CatVialidad> tiposVialidad) {
        this.tiposVialidad = tiposVialidad;
    }

    /**
     * @return the tipoAsentamiento
     */
    public List<CatTipoAsen> getTipoAsentamiento() {
        return tipoAsentamiento;
    }

    /**
     * @param tipoAsentamiento the tipoAsentamiento to set
     */
    public void setTipoAsentamiento(List<CatTipoAsen> tipoAsentamiento) {
        this.tipoAsentamiento = tipoAsentamiento;
    }

    /**
     * @return the catMedida
     */
    public List<CatUnidadMedida> getCatMedida() {
        return catMedida;
    }

    /**
     * @param catMedida the catMedida to set
     */
    public void setCatMedida(List<CatUnidadMedida> catMedida) {
        this.catMedida = catMedida;
    }
    
    

    public CatUnidadMedida getCatMedidaObj() {
		return catMedidaObj;
	}


	public void setCatMedidaObj(CatUnidadMedida catMedidaObj) {
		this.catMedidaObj = catMedidaObj;
	}
	
	public void getUnidadMedidaPorClave (short ctunClve){
		catMedidaObj = miaDao.getCatUnidadMedidaPorClave(ctunClve);
	}


	/**
     * @return the catTipoCont
     */
    public List<CatTipoContaminante> getCatTipoCont() {
        return catTipoCont;
    }

    /**
     * @param catTipoCont the catTipoCont to set
     */
    public void setCatTipoCont(List<CatTipoContaminante> catTipoCont) {
        this.catTipoCont = catTipoCont;
    }

    public List<CatNorma> getCatalogoNormas() {
        return catalogoNormas;
    }

    public void setCatalogoNormas(List<CatNorma> catalogoNormas) {
        this.catalogoNormas = catalogoNormas;
    }


	public List<SelectItem> getCatPoet() {
        if (catPoet.isEmpty()) {
            List<CatPoet> catPoetList = miaDao.getPoet();
            for (CatPoet c : catPoetList) {
                SelectItem s = new SelectItem(c.getPoetDescripcion(), c.getPoetDescripcion());
                catPoet.add(s);
            }
        }
		return catPoet;
	}


	public List<SelectItem> getCatClasificacionB() {
        if (catClasificacionB.isEmpty()) {
            List<CatClasificacionB> n = miaDao.getCatClasificacionB();
            for (CatClasificacionB c : n) {
                SelectItem s = new SelectItem(c, c.getClasificacionB());
                catClasificacionB.add(s);
            }
        }
		return catClasificacionB;
	}


	public Proyecto getProyecto() {
		return proyecto;
	}


	public void setProyecto(Proyecto proyecto) {
		this.proyecto = proyecto;
	}
    
	
	
	
}
