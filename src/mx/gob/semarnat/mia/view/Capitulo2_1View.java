/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import mx.gob.semarnat.mia.dao.MiaDao;
import mx.gob.semarnat.mia.dao.SigeiaDao;
import mx.gob.semarnat.mia.model.CatCriterio;
import mx.gob.semarnat.mia.model.CriteriosProyecto;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.util.Log;
import org.primefaces.context.RequestContext;

/**
 *
 * @author mauricio
 */
@ManagedBean
@ViewScoped
public class Capitulo2_1View extends CapitulosComentarios implements Serializable {

    //private MiaDao miaDao = new MiaDao();
    private SigeiaDao sinDao = new SigeiaDao();
    private Proyecto proyecto = new Proyecto();
    private Short NSubSector = (short) 0;
    private Integer claveTramite = 0;
    private short subsec = 0;
    //Seleccion del sitio
    private List<CriteriosProyecto> criterios = new ArrayList<CriteriosProyecto>();
    private List<SelectItem> criteriosSel = new ArrayList<SelectItem>();
    private List<CatCriterio> catCriterios = miaDao.getCriterios();

    public Capitulo2_1View() {

        // Recupera proyecto de sesion
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
        claveTramite = (Integer) context.getExternalContext().getSessionMap().get("claveTramite");
        subsec = (Short) context.getExternalContext().getSessionMap().get("subsector");
        //Cargar Proyecto
        try {
            this.proyecto = miaDao.proyecto(folioProyecto, serialProyecto);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }

        if (proyecto == null) {
            this.proyecto = new Proyecto(folioProyecto, serialProyecto);
        }

        //Comentarios proyecto - eescalona
        super.setProyectoInfo((short) 2, folioProyecto, serialProyecto);

    }

    public void agregarCriterio() {
        System.out.println("Agregando Criterio");
        if (criterios == null) {
            criterios = new ArrayList<CriteriosProyecto>();
        }

        Short id = 0;

        try {
            id = miaDao.getMaxCriterio(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
            id = (short) (id.intValue() + 1);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (id == null) {
            id = 0;
        }
        System.out.println("" + proyecto.getProyectoPK().getFolioProyecto());
        System.out.println("" + proyecto.getProyectoPK().getSerialProyecto());
        System.out.println("" + id);

        CriteriosProyecto cp = new CriteriosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
        cp.setCriterioId(new CatCriterio((short) 1));
        try {
            miaDao.merge(cp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        criterios.add(cp);
    }

    public void eliminarCriterio(ActionEvent e) {
        Short idt = (Short) e.getComponent().getAttributes().get("idt");
        CriteriosProyecto cp = new CriteriosProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt);
        criterios.remove(cp);
    }

    public void guardarProyecto() {
        RequestContext reqcontEnv = RequestContext.getCurrentInstance();
        try {
            miaDao.guardar(proyecto);
            reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
        } catch (Exception e) {
            e.printStackTrace();
            Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e.getMessage());
            reqcontEnv.execute("mensajes('No se ha podido guardar la información, intente más tarde.', 'danger')");
        }

        //Guardado de componentes en información adicional - eescalona
        super.guardarComentarios();
    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the NSubSector
     */
    public Short getNSubSector() {
        return NSubSector;
    }

    /**
     * @param NSubSector the NSubSector to set
     */
    public void setNSubSector(Short NSubSector) {
        this.NSubSector = NSubSector;
    }

    /**
     * @return the catCriterios
     */
    public List<CatCriterio> getCatCriterios() {
        return catCriterios;
    }

    /**
     * @param catCriterios the catCriterios to set
     */
    public void setCatCriterios(List<CatCriterio> catCriterios) {
        this.catCriterios = catCriterios;
    }

    public Integer getClaveTramite() {
        return claveTramite;
    }

    public void setClaveTramite(Integer claveTramite) {
        this.claveTramite = claveTramite;
    }

    public short getSubsec() {
        return subsec;
    }

    public void setSubsec(short subsec) {
        this.subsec = subsec;
    }
}
