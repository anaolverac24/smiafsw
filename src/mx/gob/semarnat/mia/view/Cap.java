/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import javax.faces.context.FacesContext;
import mx.gob.semarnat.mia.dao.MiaDao;
import mx.gob.semarnat.mia.model.Proyecto;

/**
 *
 * @author mauricio
 */
public class Cap extends CapitulosComentarios implements Serializable{

    //private MiaDao miaDao = new MiaDao();
    private Proyecto proyecto = new Proyecto();
    private Short NSubSector = (short) 0;

    public Cap() {
        // Recupera proyecto de sesion
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().get("userFolioProy");
        String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
        Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");

        //Cargar Proyecto
        try {
            this.proyecto = miaDao.proyecto(folioProyecto, serialProyecto);
            //super.setProyectoInfo((short)3, folioProyecto, serialProyecto);
        } catch (Exception e) {
            System.out.println("No encontrado");
        }

        if (proyecto == null) {
            this.proyecto = new Proyecto(folioProyecto, serialProyecto);
        }

        if (proyecto.getNsub() != null) {
            NSubSector = proyecto.getNsub();
        }

    }

    /**
     * @return the proyecto
     */
    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * @param proyecto the proyecto to set
     */
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    /**
     * @return the NSubSector
     */
    public Short getNSubSector() {
        return NSubSector;
    }

    /**
     * @param NSubSector the NSubSector to set
     */
    public void setNSubSector(Short NSubSector) {
        this.NSubSector = NSubSector;
    }

    /**
     * @return the miaDao
     */
    public MiaDao getMiaDao() {
        return miaDao;
    }

    /**
     * @param miaDao the miaDao to set
     */
//    public void setMiaDao(MiaDao miaDao) {
//        this.miaDao = miaDao;
//    }
}
