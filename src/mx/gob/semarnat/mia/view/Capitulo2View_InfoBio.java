/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import mx.gob.semarnat.mia.dao.CatEspeciesAcuiferosDAO;
import mx.gob.semarnat.mia.dao.InfobioProyectoDAO;
import mx.gob.semarnat.mia.dao.MiaDao;
import mx.gob.semarnat.mia.dao.UnidadMedidaDAO;
import mx.gob.semarnat.mia.model.CatEspeciesAcuiferos;
import mx.gob.semarnat.mia.model.InfobioProyecto;
import mx.gob.semarnat.mia.model.InfobioProyectoPK;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.catalogos.CatUnidadMedida;
import mx.gob.semarnat.mia.view.capitulo1.PaginationHelper;
import mx.gob.semarnat.mia.view.to.CatEspeciesAcuiferosTO;
import mx.gob.semarnat.mia.view.to.CatUnidadMedidaTO;
import mx.gob.semarnat.mia.view.to.InfobioProyectoTO;
import oracle.net.aso.a;

import org.primefaces.context.RequestContext;

import com.sun.org.apache.bcel.internal.generic.NEW;

/**
 *
 * @author mauricio
 */
@ManagedBean(name = "cap2_infoBio")
@ViewScoped
public class Capitulo2View_InfoBio extends AbstractTable<InfobioProyectoTO> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8322239470519097249L;

	InfobioProyectoDAO infobioProyecto = new InfobioProyectoDAO();

	InfobioProyectoTO infobioProyectoAdd;

	CatEspeciesAcuiferosDAO catEspeciesAcuiferosDAO;

	UnidadMedidaDAO unidadMedidaDAO;

	// private static final String FOLIO_TEMP = "8440";

	// private short serialProyecto;

	// private String folio;

	private List<CatEspeciesAcuiferosTO> listaEspeciesAcuiferos;

	private CatEspeciesAcuiferosTO especiesAcuiferosSelected;

	private List<CatUnidadMedidaTO> listaUnidadInicia;

	private List<CatUnidadMedidaTO> listaUnidadEsperada;
	
	/**
	 * Objet para instanciar la clase MiaDao
	 */
	private MiaDao dao = new MiaDao();	
	
	/**
	 * Lista agregada para hacer la prueba de los campos
	 */
	private List<CatUnidadMedida> medidas  = new ArrayList<>();
	private CatUnidadMedida medidaS;//m	edida seleccionada
	
	/**
	 * Permite consultar la lista de Informacion biotecnologica.
	 */
	private List<InfobioProyectoTO> listaInfoBio;
	/**
	 * Permite consultar la lista de Informacion seleccionada a eliminar
	 */
	private List<InfobioProyectoTO> listaInfoBioSeleccionadas = new ArrayList<InfobioProyectoTO>();

	private CatUnidadMedidaTO unidadIniciaSeleccionada;
	
	private CatUnidadMedidaTO unidadEsperadaSeleccionada;

	private CatUnidadMedidaTO unidadAlimentoSeleccionada;

	private CatUnidadMedidaTO unidadAbonoSeleccionada;

	private static final String TIPO_UNIDAD = "masa";

	private boolean add;

	private static final Character SI = 'S';
	private static final Character NO = 'N';

	private static final String EDIT = "EDIT";

	private boolean showDialog = Boolean.FALSE;
	private boolean showMessageDelete = Boolean.FALSE;

	@SuppressWarnings("rawtypes")
	@PostConstruct
	public void init() {
		
		// serialProyecto=(short)1;
		// folio=FOLIO_TEMP;
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String infobio = (String) facesContext.getExternalContext().getSessionMap().get("infobio");
		
//		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
//		String infobio = (String) params.get(EDIT);
		
		if (infobio != null && !infobio.equals("-1")) {
			add = Boolean.FALSE;
			infobioProyectoAdd = new InfobioProyectoTO();
			infobioProyectoAdd.setInfoBioId(Short.parseShort(infobio));
			
			infobioProyecto = new InfobioProyectoDAO();
						
			InfobioProyecto infoBioProyectoConsultado = infobioProyecto
					.consultarInfoBioPorId(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), Short.parseShort(infobio));
			
			
			if (infoBioProyectoConsultado != null) {
				infobioProyectoAdd.setFolioProyecto(infoBioProyectoConsultado.getInfobioProyectoPK().getFolioProyecto());
				infobioProyectoAdd.setSerialProyecto(infoBioProyectoConsultado.getInfobioProyectoPK().getSerialProyecto());
				infobioProyectoAdd.setInfoBioId(infoBioProyectoConsultado.getInfobioProyectoPK().getInfoBioId());

				infobioProyectoAdd.setBioNombre(infoBioProyectoConsultado.getBioNombre());
				infobioProyectoAdd.setBioOrigen(infoBioProyectoConsultado.getBioOrigen());
				infobioProyectoAdd.setBioNumorgCultivar(infoBioProyectoConsultado.getBioNumorgCultivar());
				infobioProyectoAdd.setBioDescAtributosAme(infoBioProyectoConsultado.getBioDescAtributosAme());
				infobioProyectoAdd.setBioNombreCorto(infoBioProyectoConsultado.getBioNombreCorto());
				infobioProyectoAdd.setBioOrigenCorto(infoBioProyectoConsultado.getBioOrigenCorto());
				infobioProyectoAdd.setBioNumorgCultivarCorto(infoBioProyectoConsultado.getBioNumOrgCultivarCorto());
				infobioProyectoAdd.setBioDescAtributosAmeCorto(infoBioProyectoConsultado.getBioDescAtributosAmeCorto());	
				infobioProyectoAdd.setBioCantAlimento(infoBioProyectoConsultado.getBioCantAlimento());
				infobioProyectoAdd.setBioBiomasaInic(infoBioProyectoConsultado.getBioBiomasaInic());
				infobioProyectoAdd.setBioBiomasaEsperada(infoBioProyectoConsultado.getBioBiomasaEsperada());
				infobioProyectoAdd.setBioCantSuministro(infoBioProyectoConsultado.getBioCantSuministro());
				infobioProyectoAdd.setBioCrias(infoBioProyectoConsultado.getBioCrias());
				infobioProyectoAdd.setBioEspForrajera(infoBioProyectoConsultado.getBioEspForrajera());
				infobioProyectoAdd.setBioUsoAbono(infoBioProyectoConsultado.getBioUsoAbono());
				infobioProyectoAdd.setBioUnidadSuministro(infoBioProyectoConsultado.getBioUnidadSuministro());
				infobioProyectoAdd.setBioUnidadAlmacen(infoBioProyectoConsultado.getBioUnidadAlmacen());
				infobioProyectoAdd.setBioTipoAlimento(infoBioProyectoConsultado.getBioTipoAlimento());
				infobioProyectoAdd.setBioTipabono(infoBioProyectoConsultado.getBioTipabono());
				infobioProyectoAdd.setBioSemillas(infoBioProyectoConsultado.getBioSemillas());
				infobioProyectoAdd.setBioPostlarvas(infoBioProyectoConsultado.getBioPostlarvas());
				infobioProyectoAdd.setBioJuveniles(infoBioProyectoConsultado.getBioJuveniles());
				infobioProyectoAdd.setBioFormAlmacAbono(infoBioProyectoConsultado.getBioFormAlmacAbono());
				infobioProyectoAdd.setBioFormaAlmacen(infoBioProyectoConsultado.getBioFormaAlmacen());

			}
			
		} else {
			add = Boolean.TRUE;
			getIniciaInfoBio() ;
			
			infobioProyectoAdd = new InfobioProyectoTO();
			infobioProyectoAdd.setBioEspForrajeraBoolean(Boolean.TRUE);
			infobioProyectoAdd.setBioCriasBoolean(Boolean.TRUE);
			infobioProyectoAdd.setBioSemillasBoolean(Boolean.TRUE);
			infobioProyectoAdd.setBioPostlarvasBoolean(Boolean.TRUE);
			infobioProyectoAdd.setBioJuvenilesBoolean(Boolean.TRUE);
			infobioProyectoAdd.setBioAdultReprodBoolean(Boolean.TRUE);
			
		}
		
		listaUnidadInicia = new ArrayList<CatUnidadMedidaTO>();// catUniMedidaConverter
		listaUnidadEsperada = new ArrayList<CatUnidadMedidaTO>();
		unidadMedidaDAO = new UnidadMedidaDAO();
		List<CatUnidadMedida> listCat = unidadMedidaDAO.findByTipo(TIPO_UNIDAD);
		
		for (CatUnidadMedida catUnidadMedida : listCat) {
			CatUnidadMedidaTO catUnidadMedidaTO = new CatUnidadMedidaTO();
			catUnidadMedidaTO.setCtunClve(catUnidadMedida.getCtunClve());
			catUnidadMedidaTO.setCtunDesc(catUnidadMedida.getCtunDesc());
			
			listaUnidadInicia.add(catUnidadMedidaTO);
			listaUnidadEsperada.add(catUnidadMedidaTO);

		}
		
		catEspeciesAcuiferosDAO = new CatEspeciesAcuiferosDAO();
		List<CatEspeciesAcuiferos> listaCatEspeciesAcuiferos = catEspeciesAcuiferosDAO.findAll();
		listaEspeciesAcuiferos = new ArrayList<CatEspeciesAcuiferosTO>();
		
		for (CatEspeciesAcuiferos catEspeciesAcuiferos : listaCatEspeciesAcuiferos) {
			CatEspeciesAcuiferosTO cat = new CatEspeciesAcuiferosTO();
			cat.setId(catEspeciesAcuiferos.getId());
			cat.setDescripcion(catEspeciesAcuiferos.getDescripcion());
			listaEspeciesAcuiferos.add(cat);
		}		
		
		/**
		 * Llenado de la lista para las medidas
		 * 
		 * la lista es llenada con datos de la tabla de catalogos medidas
		 */
		medidas = dao.getCatUnidadMedida(); 
		
		


	}	
	
	/**
	 * Permite consultar los registros de Informacion Biotecnologica
	 * @return
	 */
	public void getIniciaInfoBio() {// cap2_infoBio

		infobioProyecto = new InfobioProyectoDAO();
		List<InfobioProyecto> listaInfobioProyecto = infobioProyecto
				.findByFolio(getProyecto().getProyectoPK().getFolioProyecto());
		
//		listTable = convListaInfobioProyecto(listaInfobioProyecto);
		listaInfoBio = convListaInfobioProyecto(listaInfobioProyecto);
//		loadListaPaginas();
	}

	private List<InfobioProyectoTO> convListaInfobioProyecto(List<InfobioProyecto> listaInfobioProyecto) {
		List<InfobioProyectoTO> lista = new ArrayList<InfobioProyectoTO>();
		for (InfobioProyecto infobioProyecto : listaInfobioProyecto) {
			InfobioProyectoTO infobioProyectoTO = new InfobioProyectoTO();
			infobioProyectoTO.setFolioProyecto(infobioProyecto.getInfobioProyectoPK().getFolioProyecto());
			infobioProyectoTO.setSerialProyecto(infobioProyecto.getInfobioProyectoPK().getSerialProyecto());
			infobioProyectoTO.setInfoBioId(infobioProyecto.getInfobioProyectoPK().getInfoBioId());

			infobioProyectoTO.setBioNombre(infobioProyecto.getBioNombre());
			infobioProyectoTO.setBioOrigen(infobioProyecto.getBioOrigen());
			infobioProyectoTO.setBioNumorgCultivar(infobioProyecto.getBioNumorgCultivar());
			infobioProyectoTO.setBioDescAtributosAme(infobioProyecto.getBioDescAtributosAme());
			infobioProyectoTO.setBioNombreCorto(infobioProyecto.getBioNombreCorto());
			infobioProyectoTO.setBioOrigenCorto(infobioProyecto.getBioOrigenCorto());
			infobioProyectoTO.setBioNumorgCultivarCorto(infobioProyecto.getBioNumOrgCultivarCorto());
			infobioProyectoTO.setBioDescAtributosAmeCorto(infobioProyecto.getBioDescAtributosAmeCorto());
			
			lista.add(infobioProyectoTO);
		}

		return lista;
	}

	public PaginationHelper getPagination() {
		
		infobioProyecto = new InfobioProyectoDAO();
		if (pagination == null) {

			pagination = new PaginationHelper(NUM_MAX_PAG) {
				@Override
				public int getItemsCount() {
					return infobioProyecto.countByFolio(getProyecto().getProyectoPK().getFolioProyecto());
				}

				@SuppressWarnings({ "rawtypes", "unchecked" })
				@Override
				public DataModel createPageDataModel() {

					return new ListDataModel(convListaInfobioProyecto(infobioProyecto.findByFolioRange(
							new int[] { getPageFirstItem(), getPageFirstItem() + getPageSize() },
							getProyecto().getProyectoPK().getFolioProyecto())));
				}
			};
		}
		return pagination;
	}

	@Override
	void updateCurrentItem() {
		
		int count = infobioProyecto.countByFolio(getProyecto().getProyectoPK().getFolioProyecto());
		if (selectedItemIndex >= count) {

			// selected index cannot be bigger than number of items:
			selectedItemIndex = count - UNO;

			// go to previous page if last page disappeared:
			if (pagination.getPageFirstItem() >= count) {

				pagination.previousPage();
			}
		}
		if (selectedItemIndex >= CERO) {
			listTable = convListaInfobioProyecto(infobioProyecto.findByFolioRange(
					new int[] { pagination.getPageFirstItem(),
							pagination.getPageFirstItem() + pagination.getPageSize() },
					getProyecto().getProyectoPK().getFolioProyecto()));
		}
	}

	public String getLoadModal() {

		listaUnidadInicia = new ArrayList<CatUnidadMedidaTO>();// catUniMedidaConverter
		listaUnidadEsperada = new ArrayList<CatUnidadMedidaTO>();
		unidadMedidaDAO = new UnidadMedidaDAO();
		List<CatUnidadMedida> listCat = unidadMedidaDAO.findByTipo(TIPO_UNIDAD);
		for (CatUnidadMedida catUnidadMedida : listCat) {
			CatUnidadMedidaTO catUnidadMedidaTO = new CatUnidadMedidaTO();
			catUnidadMedidaTO.setCtunClve(catUnidadMedida.getCtunClve());
			catUnidadMedidaTO.setCtunDesc(catUnidadMedida.getCtunDesc());
			
			listaUnidadInicia.add(catUnidadMedidaTO);
			listaUnidadEsperada.add(catUnidadMedidaTO);

		}
		if (add) {
			infobioProyectoAdd = new InfobioProyectoTO();
			infobioProyectoAdd.setBioEspForrajeraBoolean(Boolean.TRUE);
			infobioProyectoAdd.setBioCriasBoolean(Boolean.TRUE);
			infobioProyectoAdd.setBioSemillasBoolean(Boolean.TRUE);
			infobioProyectoAdd.setBioPostlarvasBoolean(Boolean.TRUE);
			infobioProyectoAdd.setBioJuvenilesBoolean(Boolean.TRUE);
			infobioProyectoAdd.setBioAdultReprodBoolean(Boolean.TRUE);
		} else {			
			infobioProyecto = new InfobioProyectoDAO();
			InfobioProyecto entity = infobioProyecto.getByPK(infobioProyectoAdd.getInfoBioId());
			
			infobioProyecto = new InfobioProyectoDAO();
			
			
			FacesContext facesContext = FacesContext.getCurrentInstance();
			String infoBio = (String) facesContext.getExternalContext().getSessionMap().get("infobio");
			
			InfobioProyecto infoBioProyectoConsultado = infobioProyecto
					.consultarInfoBioPorId(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), Short.parseShort(infoBio));
			
			
			if (infoBioProyectoConsultado != null) {
				infobioProyectoAdd.setFolioProyecto(infoBioProyectoConsultado.getInfobioProyectoPK().getFolioProyecto());
				infobioProyectoAdd.setSerialProyecto(infoBioProyectoConsultado.getInfobioProyectoPK().getSerialProyecto());
				infobioProyectoAdd.setInfoBioId(infoBioProyectoConsultado.getInfobioProyectoPK().getInfoBioId());

				infobioProyectoAdd.setBioNombre(infoBioProyectoConsultado.getBioNombre());
				infobioProyectoAdd.setBioOrigen(infoBioProyectoConsultado.getBioOrigen());
				infobioProyectoAdd.setBioNumorgCultivar(infoBioProyectoConsultado.getBioNumorgCultivar());
				infobioProyectoAdd.setBioDescAtributosAme(infoBioProyectoConsultado.getBioDescAtributosAme());
				infobioProyectoAdd.setBioNombreCorto(infoBioProyectoConsultado.getBioNombreCorto());
				infobioProyectoAdd.setBioOrigenCorto(infoBioProyectoConsultado.getBioOrigenCorto());
				infobioProyectoAdd.setBioNumorgCultivarCorto(infoBioProyectoConsultado.getBioNumOrgCultivarCorto());
				infobioProyectoAdd.setBioDescAtributosAmeCorto(infoBioProyectoConsultado.getBioDescAtributosAmeCorto());	
				infobioProyectoAdd.setBioCantAlimento(infoBioProyectoConsultado.getBioCantAlimento());
				infobioProyectoAdd.setBioBiomasaInic(infoBioProyectoConsultado.getBioBiomasaInic());
				infobioProyectoAdd.setBioBiomasaEsperada(infoBioProyectoConsultado.getBioBiomasaEsperada());
				infobioProyectoAdd.setBioCantSuministro(infoBioProyectoConsultado.getBioCantSuministro());
				infobioProyectoAdd.setBioCrias(infoBioProyectoConsultado.getBioCrias());
				infobioProyectoAdd.setBioEspForrajera(infoBioProyectoConsultado.getBioEspForrajera());
				infobioProyectoAdd.setBioUsoAbono(infoBioProyectoConsultado.getBioUsoAbono());
				infobioProyectoAdd.setBioUnidadSuministro(infoBioProyectoConsultado.getBioUnidadSuministro());
				infobioProyectoAdd.setBioUnidadAlmacen(infoBioProyectoConsultado.getBioUnidadAlmacen());
				infobioProyectoAdd.setBioTipoAlimento(infoBioProyectoConsultado.getBioTipoAlimento());
				infobioProyectoAdd.setBioTipabono(infoBioProyectoConsultado.getBioTipabono());
				infobioProyectoAdd.setBioSemillas(infoBioProyectoConsultado.getBioSemillas());
				infobioProyectoAdd.setBioPostlarvas(infoBioProyectoConsultado.getBioPostlarvas());
				infobioProyectoAdd.setBioJuveniles(infoBioProyectoConsultado.getBioJuveniles());
				infobioProyectoAdd.setBioFormAlmacAbono(infoBioProyectoConsultado.getBioFormAlmacAbono());
				infobioProyectoAdd.setBioFormaAlmacen(infoBioProyectoConsultado.getBioFormaAlmacen());
				infobioProyectoAdd.setBioNumciclos(infoBioProyectoConsultado.getBioNumciclos());
				
				if (infobioProyectoAdd.getBioEspForrajera() != null) {
					if (infobioProyectoAdd.getBioEspForrajera().equals('S')) {
						infobioProyectoAdd.setBioEspForrajeraBoolean(true);
					} else {
						infobioProyectoAdd.setBioEspForrajeraBoolean(false);
					}					
				}
				
				//Si contiene crias informacion biotecnologica 
				if (infoBioProyectoConsultado.getBioCrias() != null) {
					if (infoBioProyectoConsultado.getBioCrias().equals('S')) {
						infobioProyectoAdd.setBioCriasBoolean(true);
					} else {
						infobioProyectoAdd.setBioCriasBoolean(false);
					}
				}
				
				//Si contiene semillas informacion biotecnologica 
				if (infoBioProyectoConsultado.getBioSemillas() != null) {
					if (infoBioProyectoConsultado.getBioSemillas().equals('S')) {
						infobioProyectoAdd.setBioSemillasBoolean(true);
					} else {
						infobioProyectoAdd.setBioSemillasBoolean(false);
					}
				}
				
				//Si contiene postlarvas informacion biotecnologica 
				if (infoBioProyectoConsultado.getBioPostlarvas() != null) {
					if (infoBioProyectoConsultado.getBioPostlarvas().equals('S')) {
						infobioProyectoAdd.setBioPostlarvasBoolean(true);
					} else {
						infobioProyectoAdd.setBioPostlarvasBoolean(false);
					}
				}
				
				//Si contiene postlarvas informacion biotecnologica 
				if (infoBioProyectoConsultado.getBioJuveniles() != null) {
					if (infoBioProyectoConsultado.getBioJuveniles().equals('S')) {
						infobioProyectoAdd.setBioJuvenilesBoolean(true);
					} else {
						infobioProyectoAdd.setBioJuvenilesBoolean(false);
					}
				}				
				
				//Si contiene postlarvas informacion biotecnologica 
				if (infoBioProyectoConsultado.getBioAdultReprod() != null) {
					if (infoBioProyectoConsultado.getBioAdultReprod().equals('S')) {
						infobioProyectoAdd.setBioAdultReprodBoolean(true);
					} else {
						infobioProyectoAdd.setBioAdultReprodBoolean(false);
					}
				}				
				
				CatEspeciesAcuiferosTO catEspeciesAcuiferos = new CatEspeciesAcuiferosTO();
				catEspeciesAcuiferos.setId(infoBioProyectoConsultado.getEspeciesAcuiferos().getId());
				especiesAcuiferosSelected = catEspeciesAcuiferos;

				CatUnidadMedida catUnidadMedida = infobioProyecto.consultarCatUnidadMedidaByNombre(infoBioProyectoConsultado.getBioUnidadInic());				
				unidadIniciaSeleccionada = new CatUnidadMedidaTO();
				unidadIniciaSeleccionada.setCtunClve(catUnidadMedida.getCtunClve());
				
				CatUnidadMedida catUnidadMedidaEsperada = infobioProyecto.consultarCatUnidadMedidaByNombre(infoBioProyectoConsultado.getBioUnidadEsperada());
				unidadEsperadaSeleccionada = new CatUnidadMedidaTO();
				unidadEsperadaSeleccionada.setCtunClve(catUnidadMedidaEsperada.getCtunClve());

				CatUnidadMedida catUnidadMedidaAlimento = infobioProyecto.consultarCatUnidadMedidaByNombre(infoBioProyectoConsultado.getBioUnidadAlmacen());
				unidadAlimentoSeleccionada = new CatUnidadMedidaTO();
				unidadAlimentoSeleccionada.setCtunClve(catUnidadMedidaAlimento.getCtunClve());
				
				CatUnidadMedida catUnidadMedidaSuministro = infobioProyecto.consultarCatUnidadMedidaByNombre(infoBioProyectoConsultado.getBioUnidadSuministro());
				unidadAbonoSeleccionada = new CatUnidadMedidaTO();
				unidadAbonoSeleccionada.setCtunClve(catUnidadMedidaSuministro.getCtunClve());
				
				facesContext.getExternalContext().getSessionMap().remove("infobio");
			}			
			
//			infobioProyectoAdd = transformEntityToTO(entity);
		}

		
		catEspeciesAcuiferosDAO = new CatEspeciesAcuiferosDAO();
		List<CatEspeciesAcuiferos> listaCatEspeciesAcuiferos = catEspeciesAcuiferosDAO.findAll();
		listaEspeciesAcuiferos = new ArrayList<CatEspeciesAcuiferosTO>();
		for (CatEspeciesAcuiferos catEspeciesAcuiferos : listaCatEspeciesAcuiferos) {
			CatEspeciesAcuiferosTO cat = new CatEspeciesAcuiferosTO();
			cat.setId(catEspeciesAcuiferos.getId());
			cat.setDescripcion(catEspeciesAcuiferos.getDescripcion());
			listaEspeciesAcuiferos.add(cat);
		}
		return null;
	}
	
	/**
	 * Permite realizar la edicion de la informacion biotecnologica seleccionada.
	 */
	public void editarInfoBio() {
		
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String infobio = (String) params.get(EDIT);
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.getExternalContext().getSessionMap().put("infobio", infobio);

	}

	public void changeValueInfoBioForrajeroNo() {
		
		if (infobioProyectoAdd != null) {
			infobioProyectoAdd.setBioEspForrajeraBoolean(Boolean.FALSE);
		}

	}

	public void changeValueInfoBioForrajeroSi() {
		
		if (infobioProyectoAdd != null) {
			infobioProyectoAdd.setBioEspForrajeraBoolean(Boolean.TRUE);
		}
	}

	///// crias
	public void changeValueInfoBioCriasNo() {
		
		if (infobioProyectoAdd != null) {
			infobioProyectoAdd.setBioCriasBoolean(Boolean.FALSE);
		}

	}

	public void changeValueInfoBioCriasSi() {
		
		if (infobioProyectoAdd != null) {
			infobioProyectoAdd.setBioCriasBoolean(Boolean.TRUE);
		}
	}

	// semillas
	public void changeValueInfoBioSemillasNo() {
		
		if (infobioProyectoAdd != null) {
			infobioProyectoAdd.setBioSemillasBoolean(Boolean.FALSE);
		}

	}

	public void changeValueInfoBioSemillasSi() {
		
		if (infobioProyectoAdd != null) {
			infobioProyectoAdd.setBioSemillasBoolean(Boolean.TRUE);
		}
	}

	// Postlarvas
	public void changeValueInfoBioPostlarvasNo() {
		
		if (infobioProyectoAdd != null) {
			infobioProyectoAdd.setBioPostlarvasBoolean(Boolean.FALSE);
		}

	}

	public void changeValueInfoBioPostlarvasSi() {
		
		if (infobioProyectoAdd != null) {
			infobioProyectoAdd.setBioPostlarvasBoolean(Boolean.TRUE);
		}
	}

	/// Juveniles
	public void changeValueInfoBioJuvenilesNo() {
		
		if (infobioProyectoAdd != null) {
			infobioProyectoAdd.setBioJuvenilesBoolean(Boolean.FALSE);
		}

	}

	public void changeValueInfoBioJuvenilesSi() {
		
		if (infobioProyectoAdd != null) {
			infobioProyectoAdd.setBioJuvenilesBoolean(Boolean.TRUE);
		}
	}

	// Adultos Reproductivos
	public void changeValueInfoBioAdultReprodNo() {
		
		if (infobioProyectoAdd != null) {
			infobioProyectoAdd.setBioAdultReprodBoolean(Boolean.FALSE);
		}

	}

	public void changeValueInfoBioAdultReprodSi() {
		
		if (infobioProyectoAdd != null) {
			infobioProyectoAdd.setBioAdultReprodBoolean(Boolean.TRUE);
		}
	}

	public void guardar() {
		
		if (add) {
			save();
		} else {
			edit();
		}

	}

	private void save() {
		
		infobioProyecto = new InfobioProyectoDAO();
		infobioProyecto.create(transformEntityToTO(infobioProyectoAdd));
		showDialog = Boolean.TRUE;

//		try {
//			miaDao.guardarAvance(getProyecto(), new Short("2"), "2");
//		} catch (Exception e) {
//			System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
//		}
	}
	
	public void guardarAvance() {
		RequestContext reqcontEnv = RequestContext.getCurrentInstance();
		try {
			if (listTable.size() >= 1) {
				try {
					getMiaDao().guardarAvance(getProyecto(), new Short("2"), "211");
				} catch (Exception e) {
					System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
				}
			}

			reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
		} catch (Exception e) {
			reqcontEnv.execute("mensajes('No se ha podido guardar la información, intente más tarde.', 'success')");
			e.printStackTrace();
		}
	}
	
	/**
	 * Permite guardar el avance del proyecto para Informacion Biotecnologica.
	 */
	public void guardarAvanceBiotecnologico() {
		RequestContext reqcontEnv = RequestContext.getCurrentInstance();
		try {
			try {
				getMiaDao().guardarAvance(getProyecto(), new Short("2"), "211");
			} catch (Exception e) {
				System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
			}
			reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
		} catch (Exception e) {
			reqcontEnv.execute("mensajes('No se ha podido guardar la información, intente más tarde.', 'danger')");
			e.printStackTrace();
		}
	}	

	private void edit() {
		
		infobioProyecto = new InfobioProyectoDAO();
		infobioProyecto.edit(transformEntityToTO(infobioProyectoAdd));
		showDialog = Boolean.TRUE;
	}

	public void changeValueAdd() {
		
		add = Boolean.TRUE;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.getExternalContext().getSessionMap().remove("infobio");
	}

	/**
	 * @return the infobioProyectoAdd
	 */
	public InfobioProyectoTO getInfobioProyectoAdd() {
		return infobioProyectoAdd;
	}

	/**
	 * @param infobioProyectoAdd
	 *            the infobioProyectoAdd to set
	 */
	public void setInfobioProyectoAdd(InfobioProyectoTO infobioProyectoAdd) {
		this.infobioProyectoAdd = infobioProyectoAdd;
	}

	/**
	 * @return the listaEspeciesAcuiferos
	 */
	public List<CatEspeciesAcuiferosTO> getListaEspeciesAcuiferos() {
		return listaEspeciesAcuiferos;
	}

	/**
	 * @param listaEspeciesAcuiferos
	 *            the listaEspeciesAcuiferos to set
	 */
	public void setListaEspeciesAcuiferos(List<CatEspeciesAcuiferosTO> listaEspeciesAcuiferos) {
		this.listaEspeciesAcuiferos = listaEspeciesAcuiferos;
	}

	/**
	 * @return the especiesAcuiferosSelected
	 */
	public CatEspeciesAcuiferosTO getEspeciesAcuiferosSelected() {
		return especiesAcuiferosSelected;
	}

	/**
	 * @param especiesAcuiferosSelected
	 *            the especiesAcuiferosSelected to set
	 */
	public void setEspeciesAcuiferosSelected(CatEspeciesAcuiferosTO especiesAcuiferosSelected) {
		this.especiesAcuiferosSelected = especiesAcuiferosSelected;
	}

	/**
	 * @return the listaUnidadInicia
	 */
	public List<CatUnidadMedidaTO> getListaUnidadInicia() {
		return listaUnidadInicia;
	}

	/**
	 * @param listaUnidadInicia
	 *            the listaUnidadInicia to set
	 */
	public void setListaUnidadInicia(List<CatUnidadMedidaTO> listaUnidadInicia) {
		this.listaUnidadInicia = listaUnidadInicia;
	}

	/**
	 * @return the unidadIniciaSeleccionada
	 */
	public CatUnidadMedidaTO getUnidadIniciaSeleccionada() {
		return unidadIniciaSeleccionada;
	}

	/**
	 * @param unidadIniciaSeleccionada
	 *            the unidadIniciaSeleccionada to set
	 */
	public void setUnidadIniciaSeleccionada(CatUnidadMedidaTO unidadIniciaSeleccionada) {
		this.unidadIniciaSeleccionada = unidadIniciaSeleccionada;
	}

	/**
	 * @return the listaUnidadEsperada
	 */
	public List<CatUnidadMedidaTO> getListaUnidadEsperada() {
		return listaUnidadEsperada;
	}

	/**
	 * @param listaUnidadEsperada
	 *            the listaUnidadEsperada to set
	 */
	public void setListaUnidadEsperada(List<CatUnidadMedidaTO> listaUnidadEsperada) {
		this.listaUnidadEsperada = listaUnidadEsperada;
	}

	/**
	 * @return the unidadEsperadaSeleccionada
	 */
	public CatUnidadMedidaTO getUnidadEsperadaSeleccionada() {
		return unidadEsperadaSeleccionada;
	}

	/**
	 * @param unidadEsperadaSeleccionada
	 *            the unidadEsperadaSeleccionada to set
	 */
	public void setUnidadEsperadaSeleccionada(CatUnidadMedidaTO unidadEsperadaSeleccionada) {
		this.unidadEsperadaSeleccionada = unidadEsperadaSeleccionada;
	}

	/**
	 * @return the add
	 */
	public boolean isAdd() {
		return add;
	}

	/**
	 * @param add
	 *            the add to set
	 */
	public void setAdd(boolean add) {
		this.add = add;
	}

	/**
	 * @return the unidadAlimentoSeleccionada
	 */
	public CatUnidadMedidaTO getUnidadAlimentoSeleccionada() {
		return unidadAlimentoSeleccionada;
	}

	/**
	 * @param unidadAlimentoSeleccionada
	 *            the unidadAlimentoSeleccionada to set
	 */
	public void setUnidadAlimentoSeleccionada(CatUnidadMedidaTO unidadAlimentoSeleccionada) {
		this.unidadAlimentoSeleccionada = unidadAlimentoSeleccionada;
	}

	/**
	 * @return the unidadAbonoSeleccionada
	 */
	public CatUnidadMedidaTO getUnidadAbonoSeleccionada() {
		return unidadAbonoSeleccionada;
	}

	/**
	 * @param unidadAbonoSeleccionada
	 *            the unidadAbonoSeleccionada to set
	 */
	public void setUnidadAbonoSeleccionada(CatUnidadMedidaTO unidadAbonoSeleccionada) {
		this.unidadAbonoSeleccionada = unidadAbonoSeleccionada;
	}

	private InfobioProyecto transformEntityToTO(InfobioProyectoTO to) {

		InfobioProyectoPK infobioProyectoPK;
		if (add) {
			infobioProyectoPK = new InfobioProyectoPK(getProyecto().getProyectoPK().getFolioProyecto(),
					getProyecto().getProyectoPK().getSerialProyecto(), infobioProyecto.getSequence());
		} else {
			infobioProyectoPK = new InfobioProyectoPK(getProyecto().getProyectoPK().getFolioProyecto(),
					getProyecto().getProyectoPK().getSerialProyecto(), to.getInfoBioId());
		}
		InfobioProyecto entity = new InfobioProyecto(infobioProyectoPK);

		entity.setBioNombre(to.getBioNombre());
		entity.setBioOrigen(to.getBioOrigen());
		entity.setBioNumorgCultivar(to.getBioNumorgCultivar());
		entity.setBioDescAtributosAme(to.getBioDescAtributosAme());
		entity.setBioEspForrajera(to.isBioEspForrajeraBoolean() ? SI : NO);
		entity.setBioCrias(to.isBioCriasBoolean() ? SI : NO);
		entity.setBioSemillas(to.isBioSemillasBoolean() ? SI : NO);
		entity.setBioPostlarvas(to.isBioPostlarvasBoolean() ? SI : NO);
		entity.setBioJuveniles(to.isBioJuvenilesBoolean() ? SI : NO);
		entity.setBioAdultReprod(to.isBioAdultReprodBoolean() ? SI : NO);
		entity.setBioNumciclos(to.getBioNumciclos());
		
		entity.setBioBiomasaInic(to.getBioBiomasaInic());
		entity.setBioBiomasaEsperada(to.getBioBiomasaEsperada());
		
		entity.setBioUnidadInic(unidadIniciaSeleccionada != null ? unidadIniciaSeleccionada.getCtunDesc() : null);		
		entity.setBioUnidadEsperada(unidadEsperadaSeleccionada != null ? unidadEsperadaSeleccionada.getCtunDesc() : null);
		
		entity.setBioTipoAlimento(to.getBioTipoAlimento());
		entity.setBioCantAlimento(to.getBioCantAlimento());
		entity.setBioFormaAlmacen(to.getBioFormaAlmacen());
		entity.setBioUnidadAlmacen(unidadAlimentoSeleccionada != null ? unidadAlimentoSeleccionada.getCtunDesc() : null);
		entity.setBioTipabono(to.getBioTipabono());
		entity.setBioUsoAbono(to.getBioUsoAbono());
		entity.setBioCantSuministro(to.getBioCantSuministro());
		entity.setBioUnidadSuministro(unidadAbonoSeleccionada != null ? unidadAbonoSeleccionada.getCtunDesc() : null);
		entity.setBioFormAlmacAbono(to.getBioFormAlmacAbono());

		catEspeciesAcuiferosDAO = new CatEspeciesAcuiferosDAO();
		CatEspeciesAcuiferos catEspeciesAcuiferos = catEspeciesAcuiferosDAO.find(especiesAcuiferosSelected.getId());
		entity.setEspeciesAcuiferos(catEspeciesAcuiferos);

		

		return entity;
	}

	private InfobioProyectoTO transformEntityToTO(InfobioProyecto entity) {
		InfobioProyectoTO to = new InfobioProyectoTO();
		to.setInfoBioId(entity.getInfobioProyectoPK().getInfoBioId());
		to.setFolioProyecto(entity.getInfobioProyectoPK().getFolioProyecto());
		to.setBioNombre(removeEspacioDerecha(entity.getBioNombre()));
		to.setBioOrigen(removeEspacioDerecha(entity.getBioOrigen()));
		to.setBioNumorgCultivar(entity.getBioNumorgCultivar());
		to.setBioDescAtributosAme(removeEspacioDerecha(entity.getBioDescAtributosAme()));
		to.setBioEspForrajeraBoolean(entity.getBioEspForrajera() != null && entity.getBioEspForrajera().equals(SI));

		to.setBioCriasBoolean(entity.getBioCrias() != null && entity.getBioCrias().equals(SI));
		to.setBioSemillasBoolean(entity.getBioSemillas() != null && entity.getBioSemillas().equals(SI));
		to.setBioPostlarvasBoolean(entity.getBioPostlarvas() != null && entity.getBioPostlarvas().equals(SI));
		to.setBioJuvenilesBoolean(entity.getBioJuveniles() != null && entity.getBioJuveniles().equals(SI));
		to.setBioAdultReprodBoolean(entity.getBioAdultReprod() != null && entity.getBioAdultReprod().equals(SI));
		to.setBioNumciclos(entity.getBioNumciclos());
		to.setBioBiomasaInic(entity.getBioBiomasaInic());
		to.setBioNombreCorto(entity.getBioNombreCorto());
		to.setBioOrigenCorto(entity.getBioOrigenCorto());
		to.setBioNumorgCultivarCorto(entity.getBioNumOrgCultivarCorto());
		to.setBioDescAtributosAmeCorto(entity.getBioDescAtributosAmeCorto());

		unidadIniciaSeleccionada = getUnidadByDesc(entity.getBioUnidadInic());
		to.setBioBiomasaEsperada(entity.getBioBiomasaEsperada());

		unidadEsperadaSeleccionada = getUnidadByDesc(entity.getBioUnidadEsperada());
		to.setBioTipoAlimento(removeEspacioDerecha(entity.getBioTipoAlimento()));
		to.setBioCantAlimento(entity.getBioCantAlimento());
		to.setBioFormaAlmacen(removeEspacioDerecha(entity.getBioFormaAlmacen()));
		unidadAlimentoSeleccionada = getUnidadByDesc(entity.getBioUnidadAlmacen());
		to.setBioTipabono(removeEspacioDerecha(entity.getBioTipabono()));
		to.setBioUsoAbono(removeEspacioDerecha(entity.getBioUsoAbono()));
		to.setBioCantSuministro(entity.getBioCantSuministro());
		unidadAbonoSeleccionada = getUnidadByDesc(entity.getBioUnidadSuministro());
		to.setBioFormAlmacAbono(removeEspacioDerecha(entity.getBioFormAlmacAbono()));

		if (entity.getEspeciesAcuiferos() != null) {
			CatEspeciesAcuiferosTO catEspeciesAcuiferosTO = new CatEspeciesAcuiferosTO();
			catEspeciesAcuiferosTO.setId(entity.getEspeciesAcuiferos().getId());
			catEspeciesAcuiferosTO.setDescripcion(entity.getEspeciesAcuiferos().getDescripcion());
			especiesAcuiferosSelected = catEspeciesAcuiferosTO;
		}

		

		return to;
	}

	private CatUnidadMedidaTO getUnidadByDesc(String des) {
		CatUnidadMedidaTO rtn = null;
		for (CatUnidadMedidaTO catUnidadMedidaTO : listaUnidadInicia) {
			if (catUnidadMedidaTO.getCtunDesc().equals(des)) {
				rtn = catUnidadMedidaTO;
				break;
			}
		}
		return rtn;
	}

	public void previoDelete() {
		List<InfobioProyectoTO> listaDelete = getSelectedRow();
		if (listaDelete != null && !listaDelete.isEmpty()) {
			showMessageDelete = Boolean.TRUE;
		}
	}

	/**
	 * Permite eliminar la lista de registros de informacion biotecnologica seleccionada.
	 */
	public void delete() {
		List<InfobioProyectoTO> listaDelete = listaInfoBioSeleccionadas;
		for (InfobioProyectoTO infobioProyectoTO : listaDelete) {
			
			InfobioProyecto entity = infobioProyecto.getByPK(infobioProyectoTO.getInfoBioId());
			
			infobioProyecto.remove(entity);

		}
		getIniciaInfoBio();
		listaInfoBioSeleccionadas = new ArrayList<InfobioProyectoTO>();
		showMessageDelete = Boolean.FALSE;
	}

	/**
	 * @return the showDialog
	 */
	public boolean isShowDialog() {
		return showDialog;
	}

	/**
	 * @param showDialog
	 *            the showDialog to set
	 */
	public void setShowDialog(boolean showDialog) {
		this.showDialog = showDialog;
	}

	/**
	 * @return the showMessageDelete
	 */
	public boolean isShowMessageDelete() {
		return showMessageDelete;
	}

	/**
	 * @param showMessageDelete
	 *            the showMessageDelete to set
	 */
	public void setShowMessageDelete(boolean showMessageDelete) {
		this.showMessageDelete = showMessageDelete;
	}

	private String removeEspacioDerecha(String cad){
		while(cad!=null && cad.length()-1>0 && cad.substring(cad.length()-1,cad.length()).equals(" ")){
			cad=cad.substring(0, cad.length()-1);
		}
		return cad;
	}
	
	
	
	public void initMessage(){
		showMessageDelete = Boolean.FALSE;
	}

	/**
	 * @return the listaInfoBio
	 */
	public List<InfobioProyectoTO> getListaInfoBio() {
		return listaInfoBio;
	}

	/**
	 * @param listaInfoBio the listaInfoBio to set
	 */
	public void setListaInfoBio(List<InfobioProyectoTO> listaInfoBio) {
		this.listaInfoBio = listaInfoBio;
	}

	/**
	 * @return the listaInfoBioSeleccionadas
	 */
	public List<InfobioProyectoTO> getListaInfoBioSeleccionadas() {
		return listaInfoBioSeleccionadas;
	}

	/**
	 * @param listaInfoBioSeleccionadas the listaInfoBioSeleccionadas to set
	 */
	public void setListaInfoBioSeleccionadas(List<InfobioProyectoTO> listaInfoBioSeleccionadas) {
		this.listaInfoBioSeleccionadas = listaInfoBioSeleccionadas;
	}

	/**
	 * @return the medidas
	 */
	public List<CatUnidadMedida> getMedidas() {
		return medidas;
	}

	/**
	 * @param medidas the medidas to set
	 */
	public void setMedidas(List<CatUnidadMedida> medidas) {
		this.medidas = medidas;
	}

	/**
	 * @return the medidaS
	 */
	public CatUnidadMedida getMedidaS() {
		return medidaS;
	}

	/**
	 * @param medidaS the medidaS to set
	 */
	public void setMedidaS(CatUnidadMedida medidaS) {
		this.medidaS = medidaS;
	}	
	
}
