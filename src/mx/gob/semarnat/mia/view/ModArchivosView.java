/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import mx.gob.semarnat.mia.dao.VisorDao;
import mx.gob.semarnat.mia.model.AnexosProyecto;
import mx.gob.semarnat.mia.model.EspecificacionAnexo;
import mx.gob.semarnat.mia.model.EstudiosEspProy;
import mx.gob.semarnat.mia.model.SustanciaAnexo;

/**
 *
 * @author mauricio
 */
@ManagedBean(name = "modArchivosView")
@RequestScoped
public class ModArchivosView {

    private String folioProyecto;
    private String especifiacionId;
    private Short serialProyecto;
    private Short normaId;

    private List<EspecificacionAnexo> especificacionAnexos = new ArrayList<EspecificacionAnexo>();
    private List<AnexosProyecto> anexosProyecto = new ArrayList<AnexosProyecto>();
    private List<SustanciaAnexo> sustanciasProyecto = new ArrayList<SustanciaAnexo>();
    private List<EstudiosEspProy> estudiosProyecto = new ArrayList<EstudiosEspProy>();
    private Boolean verEspAnexos;
    private Boolean verAnexos;
    private Boolean verSustancias;
    private Boolean verEstudios;

    @ManagedProperty("#{param.info}")
    private String info;

    @PostConstruct
    public void init() {
        String[] args = info.split(",");

        if (args.length == 4) {
//            System.out.println("BEAN _ INFO " + info);

            folioProyecto = args[1];
            especifiacionId = args[0];
            serialProyecto = new Short(args[2]);
            normaId = new Short(args[3]);

//            System.out.println("Folio " + folioProyecto);
//            System.out.println("Espec " + especifiacionId);
//            System.out.println("Serial " + serialProyecto);
//            System.out.println("Norma " + normaId);

            especificacionAnexos = VisorDao.getEspecifiacionesAnexos(folioProyecto, especifiacionId, serialProyecto, normaId);
        }

        if (args.length == 6) {
            String folioProyecto = "";
            Short serialProyecto = (short) 0;
            short capituloId = (short) 0;
            short subcapituloId = (short) 0;
            short seccionId = (short) 0;
            short apartadoId = (short) 0;
            capituloId = new Short(args[0]);
            subcapituloId = new Short(args[1]);
            seccionId = new Short(args[2]);
            apartadoId = new Short(args[3]);
            folioProyecto = args[4];
            serialProyecto = new Short(args[5]);
            anexosProyecto = VisorDao.getAnexos(capituloId, subcapituloId, seccionId, apartadoId, folioProyecto, serialProyecto);
            
        }
        if(args.length==3){
            Short sustanciaProyId;
            folioProyecto = args[0];
            serialProyecto = new Short(args[1]);
            sustanciaProyId = new Short(args[2]);
            sustanciasProyecto = VisorDao.getSustanciaAnexos(folioProyecto, serialProyecto, sustanciaProyId);
        }
        
        if (args.length == 7) {
            String folioProyecto = "";
            Short serialProyecto = (short) 0;
            Short estudioProyId;
            short capituloId = (short) 0;
            short subcapituloId = (short) 0;
            short seccionId = (short) 0;
            short apartadoId = (short) 0;
            capituloId = new Short(args[0]);
            subcapituloId = new Short(args[1]);
            seccionId = new Short(args[2]);
            apartadoId = new Short(args[3]);
            folioProyecto = args[4];
            serialProyecto = new Short(args[5]);
            estudioProyId = new Short(args[6]);
            estudiosProyecto = VisorDao.getEstudiosEspProy(folioProyecto, serialProyecto,estudioProyId);
            
        }

        if (especificacionAnexos.isEmpty()) {
            verEspAnexos = false;
        } else {
            verEspAnexos = true;
        }
        if (anexosProyecto.isEmpty()) {
            verAnexos = false;
        } else {
            verAnexos = true;
        }
        
        verSustancias = sustanciasProyecto.isEmpty();
        if(estudiosProyecto.isEmpty()){
            verEstudios = false;
        }else{
            verEstudios = true;
        }
    }

    public ModArchivosView() {
//        System.out.println("info " + info);
    }

    /**
     * @return the info
     */
    public String getInfo() {
        return info;
    }

    /**
     * @param info the info to set
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * @return the especificacionAnexos
     */
    public List<EspecificacionAnexo> getEspecificacionAnexos() {
        return especificacionAnexos;
    }

    /**
     * @param especificacionAnexos the especificacionAnexos to set
     */
    public void setEspecificacionAnexos(List<EspecificacionAnexo> especificacionAnexos) {
        this.especificacionAnexos = especificacionAnexos;
    }

    /**
     * @return the verEspAnexos
     */
    public Boolean getVerEspAnexos() {
        return verEspAnexos;
    }

    /**
     * @param verEspAnexos the verEspAnexos to set
     */
    public void setVerEspAnexos(Boolean verEspAnexos) {
        this.verEspAnexos = verEspAnexos;
    }

    /**
     * @return the anexosProyecto
     */
    public List<AnexosProyecto> getAnexosProyecto() {
        return anexosProyecto;
    }

    /**
     * @param anexosProyecto the anexosProyecto to set
     */
    public void setAnexosProyecto(List<AnexosProyecto> anexosProyecto) {
        this.anexosProyecto = anexosProyecto;
    }

    /**
     * @return the verAnexos
     */
    public Boolean getVerAnexos() {
        return verAnexos;
    }

    /**
     * @param verAnexos the verAnexos to set
     */
    public void setVerAnexos(Boolean verAnexos) {
        this.verAnexos = verAnexos;
    }

    /**
     * @return the sustanciasProyecto
     */
    public List<SustanciaAnexo> getSustanciasProyecto() {
        return sustanciasProyecto;
    }

    /**
     * @param sustanciasProyecto the sustanciasProyecto to set
     */
    public void setSustanciasProyecto(List<SustanciaAnexo> sustanciasProyecto) {
        this.sustanciasProyecto = sustanciasProyecto;
    }

    /**
     * @return the verSustancias
     */
    public Boolean getVerSustancias() {
        return verSustancias;
    }

    /**
     * @param verSustancias the verSustancias to set
     */
    public void setVerSustancias(Boolean verSustancias) {
        this.verSustancias = verSustancias;
    }

    public List<EstudiosEspProy> getEstudiosProyecto() {
        return estudiosProyecto;
    }

    public void setEstudiosProyecto(List<EstudiosEspProy> estudiosProyecto) {
        this.estudiosProyecto = estudiosProyecto;
    }

    public Boolean getVerEstudios() {
        return verEstudios;
    }

    public void setVerEstudios(Boolean verEstudios) {
        this.verEstudios = verEstudios;
    }
    
}
