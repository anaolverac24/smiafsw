/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import mx.gob.semarnat.mia.dao.AcuiferosDAO;
import mx.gob.semarnat.mia.dao.ProyectoDAO;
import mx.gob.semarnat.mia.model.Acuiferos;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.view.capitulo1.PaginationHelper;
import mx.gob.semarnat.mia.view.to.AcuiferosTO;

/**
 *
 * @author Ramon Hidalgo
 */
@ManagedBean(name = "hidrologiaSub")
@ViewScoped
public class Capitulo4View2HidrologiaSubterranea extends AbstractTable<AcuiferosTO> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3197009275689756012L;

	private Proyecto proyecto;



	private AcuiferosDAO acuiferosDAO;

	private Integer claveTramite = 0;

	private static final String SOBRE_EXPLOTADO_SI = "Si";

	private String hidroSubtObserva;
    private String hidroSupObserva;

	private String analisisbioVeg;

	private String descGeos;
	
	private String descSuelo;

	private String climaFenMetObserv;
	
	private String tempFenMetDescrip;
	 
	public Capitulo4View2HidrologiaSubterranea() {

		getCapituloHidrologiaSub();
	}

	public String getCapituloHidrologiaSub() {
		acuiferosDAO = new AcuiferosDAO();
		proyecto = new Proyecto();
		// Recupera proyecto de sesion
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().get("userFolioProy");
		String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
		Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
		claveTramite = (Integer) context.getExternalContext().getSessionMap().get("claveTramite");

		// Cargar Proyecto
		try {
			this.proyecto = miaDao.proyecto(folioProyecto, serialProyecto);
		} catch (Exception e) {
			System.out.println("No encontrado");
		}

		if (proyecto == null) {
			this.proyecto = new Proyecto(folioProyecto, serialProyecto);
		}

		listTable = new ArrayList<AcuiferosTO>();
		List<Acuiferos> listaEntity = acuiferosDAO.findByFolioVersion(folioProyecto, serialProyecto);
		listTable.addAll(transformList(listaEntity));
		loadListaPaginas();
		hidroSubtObserva = getProyecto().getHidroSubtObserva();
		hidroSupObserva = getProyecto().getHidroSuPObserva();
		analisisbioVeg = getProyecto().getAnalisisbioVeg();
		descGeos= getProyecto().getDescGeos();
		descSuelo=getProyecto().getDescSuelo();
		climaFenMetObserv=getProyecto().getClimaFenMetObserv();
		tempFenMetDescrip=getProyecto().getTempFenMetDescrip();
		return null;
	}

	public PaginationHelper getPagination() {

		if (pagination == null) {

			pagination = new PaginationHelper(NUM_MAX_PAG) {
				@Override
				public int getItemsCount() {
					return acuiferosDAO.countByFolioVersion(proyecto.getProyectoPK().getFolioProyecto(),
							proyecto.getProyectoPK().getSerialProyecto());
				}

				@SuppressWarnings({ "rawtypes", "unchecked" })
				@Override
				public DataModel createPageDataModel() {

					return new ListDataModel(transformList(acuiferosDAO.findRangeFolioVersion(
							new int[] { getPageFirstItem(), getPageFirstItem() + getPageSize() },
							proyecto.getProyectoPK().getFolioProyecto(),
							proyecto.getProyectoPK().getSerialProyecto())));
				}
			};
		}
		return pagination;
	}

	@Override
	void updateCurrentItem() {

		int count = acuiferosDAO.countByFolioVersion(proyecto.getProyectoPK().getFolioProyecto(),
				proyecto.getProyectoPK().getSerialProyecto());
		if (selectedItemIndex >= count) {

			// selected index cannot be bigger than number of items:
			selectedItemIndex = count - UNO;

			// go to previous page if last page disappeared:
			if (pagination.getPageFirstItem() >= count) {

				pagination.previousPage();
			}
		}
		if (selectedItemIndex >= CERO) {
			listTable = transformList(acuiferosDAO.findRangeFolioVersion(
					new int[] { pagination.getPageFirstItem(),
							pagination.getPageFirstItem() + pagination.getPageSize() },
					proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
		}
	}

	private AcuiferosTO transformEntityToTO(Acuiferos acuiferos) {
		AcuiferosTO to = new AcuiferosTO();
		to.setNomAcui(acuiferos.getClvAcui() + " " + acuiferos.getNomAcui());
		to.setDescDispo(acuiferos.getDescDispo());
		to.setFechaDof(acuiferos.getFechaDof());
		to.setSobreexp(acuiferos.getSobreexp());
		to.setSobreexpBoolean(
				SOBRE_EXPLOTADO_SI.equalsIgnoreCase(acuiferos.getSobreexp()) ? Boolean.TRUE : Boolean.FALSE);
		return to;
	}

	private List<AcuiferosTO> transformList(List<Acuiferos> listaEntity) {

		List<AcuiferosTO> listaTO = new ArrayList<AcuiferosTO>();
		for (Acuiferos acuiferos : listaEntity) {
			listaTO.add(transformEntityToTO(acuiferos));
		}
		return listaTO;
	}

	/**
	 * @return the proyecto
	 */
	public Proyecto getProyecto() {
		return proyecto;
	}

	/**
	 * @param proyecto
	 *            the proyecto to set
	 */
	public void setProyecto(Proyecto proyecto) {
		this.proyecto = proyecto;
	}

	/**
	 * @return the hidroSubtObserva
	 */
	public String getHidroSubtObserva() {
		return hidroSubtObserva;
	}

	/**
	 * @param hidroSubtObserva
	 *            the hidroSubtObserva to set
	 */
	public void setHidroSubtObserva(String hidroSubtObserva) {
		this.hidroSubtObserva = hidroSubtObserva;
	}

	/**
	 * @return the analisisbioVeg
	 */
	public String getAnalisisbioVeg() {
		return analisisbioVeg;
	}

	/**
	 * @param analisisbioVeg
	 *            the analisisbioVeg to set
	 */
	public void setAnalisisbioVeg(String analisisbioVeg) {
		this.analisisbioVeg = analisisbioVeg;
	}

	/**
	 * @return the descGeos
	 */
	public String getDescGeos() {
		return descGeos;
	}

	/**
	 * @param descGeos
	 *            the descGeos to set
	 */
	public void setDescGeos(String descGeos) {
		this.descGeos = descGeos;
	}

	
	
	/**
	 * @return the descSuelo
	 */
	public String getDescSuelo() {
		return descSuelo;
	}

	/**
	 * @param descSuelo the descSuelo to set
	 */
	public void setDescSuelo(String descSuelo) {
		this.descSuelo = descSuelo;
	}
	
	
	

	/**
	 * @return the climaFenMetObserv
	 */
	public String getClimaFenMetObserv() {
		return climaFenMetObserv;
	}

	/**
	 * @param climaFenMetObserv the climaFenMetObserv to set
	 */
	public void setClimaFenMetObserv(String climaFenMetObserv) {
		this.climaFenMetObserv = climaFenMetObserv;
	}

	/**
	 * @return the tempFenMetDescrip
	 */
	public String getTempFenMetDescrip() {
		return tempFenMetDescrip;
	}

	/**
	 * @param tempFenMetDescrip the tempFenMetDescrip to set
	 */
	public void setTempFenMetDescrip(String tempFenMetDescrip) {
		this.tempFenMetDescrip = tempFenMetDescrip;
	}

	public void saveObs() {
		System.out.println("saveObs____________________________");
		ProyectoDAO proyecto = new ProyectoDAO();
		getProyecto().setHidroSubtObserva(hidroSubtObserva);
		getProyecto().setHidroSuPObserva(hidroSupObserva);
		proyecto.edit(getProyecto());
	}

	public void saveAnalisis() {
		System.out.println("saveAnalisis____________________________");
		ProyectoDAO proyecto = new ProyectoDAO();
		getProyecto().setAnalisisbioVeg(analisisbioVeg);
		proyecto.edit(getProyecto());
	}

	public void saveDescGeo() {
		System.out.println("saveDescGeo____________________________"+descGeos);
		ProyectoDAO proyecto = new ProyectoDAO();
		getProyecto().setDescGeos(descGeos);
		proyecto.edit(getProyecto());
	}
	
	public void saveDescSuelo(){
		System.out.println("saveDescSuelo____________________________"+descSuelo);
		ProyectoDAO proyecto = new ProyectoDAO();
		getProyecto().setDescSuelo(descSuelo);
		proyecto.edit(getProyecto());
	}

	
	public void saveClimaFenMetObserv(){
		System.out.println("saveClimaFenMetObserv____________________________"+climaFenMetObserv);
		ProyectoDAO proyecto = new ProyectoDAO();
		getProyecto().setClimaFenMetObserv(climaFenMetObserv);
		proyecto.edit(getProyecto());		
	}
	
	public void saveTempFenMetDescrip(){
		System.out.println("saveTempFenMetDescrip____________________________"+tempFenMetDescrip);
		ProyectoDAO proyecto = new ProyectoDAO();
		getProyecto().setTempFenMetDescrip(tempFenMetDescrip);
		proyecto.edit(getProyecto());	
	}

	public String getHidroSupObserva() {
		return hidroSupObserva;
	}

	public void setHidroSupObserva(String hidroSupObserva) {
		this.hidroSupObserva = hidroSupObserva;
	}
	
	
}
