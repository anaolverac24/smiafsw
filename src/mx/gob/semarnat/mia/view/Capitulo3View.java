/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Template
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import mx.gob.semarnat.mia.dao.ArchivoBean;
import mx.gob.semarnat.mia.dao.ArchivosAnexosDAO;
import mx.gob.semarnat.mia.dao.CargaBean;
import mx.gob.semarnat.mia.dao.LeyFedEstDAO;
import mx.gob.semarnat.mia.dao.ReglamentoDAO;
import mx.gob.semarnat.mia.dao.SigeiaDao;
import mx.gob.semarnat.mia.model.AnpEstatal;
import mx.gob.semarnat.mia.model.AnpFederal;
import mx.gob.semarnat.mia.model.AnpMuicipal;
import mx.gob.semarnat.mia.model.AnpProyecto;
import mx.gob.semarnat.mia.model.ArchivosProyecto;
import mx.gob.semarnat.mia.model.ArticuloReglaAnp;
import mx.gob.semarnat.mia.model.CatLey;
import mx.gob.semarnat.mia.model.CatReglamento;
import mx.gob.semarnat.mia.model.ConvenioProy;
import mx.gob.semarnat.mia.model.ConvenioProyPK;
import mx.gob.semarnat.mia.model.DisposicionProyecto;
import mx.gob.semarnat.mia.model.InstrUrbanos;
import mx.gob.semarnat.mia.model.LeyFedEstProyecto;
import mx.gob.semarnat.mia.model.LeyFedEstProyectoPK;
import mx.gob.semarnat.mia.model.PdumProyecto;
import mx.gob.semarnat.mia.model.PoetmProyecto;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.ReglamentoProyecto;
import mx.gob.semarnat.mia.model.ReglamentoProyectoPK;
import mx.gob.semarnat.mia.model.catalogos.SectorProyecto;
import mx.gob.semarnat.mia.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.mia.util.ANPu;
import mx.gob.semarnat.mia.util.Log;
import mx.gob.semarnat.mia.util.PDUu;
import mx.gob.semarnat.mia.util.POETu;
import mx.gob.semarnat.mia.util.VDecretou;
import mx.gob.semarnat.mia.util.VProgramau;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
/**
 *
 * @author Ramon Hidalgo
 */
@ManagedBean
@ViewScoped
public class Capitulo3View extends CapitulosComentarios implements Serializable {

	private String seccion;
	//private MiaDao miaDao = new MiaDao();
	private SigeiaDao siDao = new SigeiaDao();
	private ReglamentoDAO reglamentoDAO = new ReglamentoDAO();
	private boolean capServicios = false;


	private Proyecto proyecto = new Proyecto();
	private String sector = "";

	/**
	 * Permite mostrar la vinculacion de la ley;
	 */
	private String vinculacionLey;
	private String vinculacionPoet;
	private String vinculacionReg;

	// Leyes
	private List<LeyFedEstProyecto> leyesFederal = new ArrayList<LeyFedEstProyecto>();
	private List<LeyFedEstProyecto> leyesEstatal = new ArrayList<LeyFedEstProyecto>();
	/**
	 * Lista de leyes federales seleccionadas.
	 */
	private List<LeyFedEstProyecto> leyesFederalesSeleccionadas = new ArrayList<LeyFedEstProyecto>();


	// Reglamentos
	private List<ReglamentoProyecto> reglamentos = new ArrayList<ReglamentoProyecto>();  //Lista para la consulta 
	private List<ReglamentoProyecto> reglamentosS = new ArrayList<ReglamentoProyecto>();
	private ReglamentoProyecto reglam; //Objeto para las altas
	private ReglamentoProyecto reglamed;
	private ArchivosProyecto archivoTemporalReglamento = new ArchivosProyecto();
	/**
	 * Objeto que almacena la informacion del catalogo de leyes federales.
	 */
	private List<CatReglamento> catReg;   
	private List<ReglamentoProyecto> reglamento_Seleccionado = new ArrayList<ReglamentoProyecto>(); //Lista para los regalemntos a eliminar
	private ReglamentoProyecto reglamento = new ReglamentoProyecto(); //Objeto para la alta y edicion de un reglamento
	private ReglamentoProyecto reglamEd;
	private short valor;    
	private CatReglamento catReglamento;
	private List<CatReglamento> catalogo;
	private String aplica;
	
	/**
	  * Numero secuencial del registro "Reglamento" a editar
	  */
	private long rowIndexReglamEditar;
	
	

	// Acuerdos y convenios
	private List<ConvenioProy> convenios = new ArrayList<ConvenioProy>();
	private List<ConvenioProy> conveniosElegidos= new ArrayList<ConvenioProy>();
	private ConvenioProy agrega=null;
	private ConvenioProy edita=null;
	
	/**
	  * Numero secuencial del registro a editar en tabla "Acuerdos, convenios y tratados internacionales en materia ambiental"
	  */
	private int rowIndexAcuerdoEditar;
	
	
	
	// Disposiciones
	private boolean capDisposiciones = false;
	private List<DisposicionProyecto> disposiciones = new ArrayList<DisposicionProyecto>();

	
	// POET
	private List<POETu> poet = new ArrayList<POETu>();
	private List<PoetmProyecto> poetProyecto = new ArrayList<PoetmProyecto>();
	private List<PoetmProyecto> selectedPoet = new ArrayList<PoetmProyecto>();
	private PoetmProyecto poetm;
	private PoetmProyecto editPoet;
	private ArchivosProyecto archivoTemporal = new ArchivosProyecto();
	
	/**
	  * Numero secuencial del registro "POET" a editar
	  */
	private long rowIndexPOETEditar;
	
	
	
	// ANP
	private List<ANPu> anpu = new ArrayList<ANPu>();
	private List<AnpProyecto> anp = new ArrayList<AnpProyecto>();
	private List<ArticuloReglaAnp> anpArticulo = new ArrayList<ArticuloReglaAnp>();
	private List<ArticuloReglaAnp> anpRegla = new ArrayList<ArticuloReglaAnp>();

	// Decretou
	private List<VDecretou> vinDec = new ArrayList<VDecretou>();
	// Progrma
	private List<VProgramau> vinPro = new ArrayList<VProgramau>();
	// PDU1
	private List<PDUu> pdu1 = new ArrayList<PDUu>();
	private List<PdumProyecto> pduProyecto = new ArrayList<PdumProyecto>();
	// PDU2
	private List<PDUu> pdu2 = new ArrayList<PDUu>();
	private Integer claveTramite = 0;
	private short subsec = 0;    
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

	private boolean respuestaIncidePDU;
	private boolean respuestaPOET;
	private String respuestaPregunta;

	/**
	 * Id del la ley seleccionada para la edicion.
	 */
	private short idLeySeqSeleccionada;
	/**
	 * Permite mostrar el panel para guardar o editar una ley federal.
	 */
	private boolean mostrarEdicion;
	/**
	 * Permite mostrar el panel para guardar o editar una ley estatal.
	 */
	private boolean mostrarEdicionEstatal;
	/**
	 * Permite mostrar el catalogo de leyes Federales.
	 */
	private List<CatLey> catalogoLeyesFederales;
	/**
	 * Objeto que almacena la informacion del catalogo de leyes federales.
	 */
	private CatLey catLey;
	/**
	 * Objeto que permite obtener la informacion de las leyes federales y estatales del proyecto.
	 */
	private LeyFedEstProyecto leyFedEstProyecto;
	/**
	 * Permite realizar las transacciones de las leyes federales y estatales.
	 */
	private LeyFedEstDAO leyFedEstDAO = new LeyFedEstDAO();	
	/**
	 * Lista de leyes federales o estatales seleccionadas para eliminar.
	 */
	private List<LeyFedEstProyecto> leyesFedEstSeleccionadas;	
	/**
	 * Permite mostrar la caja de texto de la captura de la ley federal.
	 */
	private boolean mostrarCapturaLeyFederal;
	private boolean mostrarCapturaRegla;
	/**
	 * Permite indicar las leyes estatales seleccionadas.
	 */
	private List<LeyFedEstProyecto> leyesEstatalesSeleccionadas = new ArrayList<LeyFedEstProyecto>();
	/**
	 * Permite mostrar ultima fecha de actualizacion de la ley.
	 */
	private boolean mostrarUltimaFecha;
	/**
	 * Fecha de la ultima actualizacion de la ley.
	 */
	private Date fechaUltimaActualizacion = new Date();
	/**
	 * Permite cargar el archivo temporal de leyes federales.
	 */
	private ArchivosProyecto archivoTemporalFederal = new ArchivosProyecto();
	/**
	 * Permite cargar el archivo temporal de leyes estatales.
	 */
	private ArchivosProyecto archivoTemporalEstatal = new ArchivosProyecto();
	/**
	 * Objeto que permite obtener los metodos para visualizar o cargar un archivo.
	 */
	private CargaBean cargaBean;
	/**
	 * Ley federal seleccionada para carga un nuevo archivo.
	 */
	private LeyFedEstProyecto leyFedSeleccionado;
	/**
	 * Ley estatal seleccionada para cargar un nuevo archivo
	 */
	private LeyFedEstProyecto leyEstSeleccionado;
	private ArchivosAnexosDAO archivosAnexosDAO = new ArchivosAnexosDAO(); 
	
	/**
	 * Numero secuencial del registro "Ley federal" a editar
	 */
	private int rowIndexLeyFedEditar;

	/**
	 * Numero secuencial del registro "Ley estatal" a editar
	 */
	private int rowIndexLeyEstEditar;
	
	private String msj;
	private boolean bandera;


	final static Logger LOGGER = Logger.getLogger(Capitulo3View.class);

	
	public Capitulo3View() {
		this.respuestaPregunta = "N";
		LOGGER.info("CARGANDO LOG4J ==========================================================================================");
		catLey = new CatLey();
		leyFedEstProyecto = new LeyFedEstProyecto();    
		leyFedEstProyecto.setLeyId(catLey);

		cargaBean = new CargaBean();

		leyEstSeleccionado = new LeyFedEstProyecto();
		leyFedSeleccionado = new LeyFedEstProyecto();    	
		// Recupera proyecto de sesion
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().get("userFolioProy");
		String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
		Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
		claveTramite =  (Integer) context.getExternalContext().getSessionMap().get("claveTramite"); 
		//Cargar Proyecto
		//        if (proyecto.getProyectoPK() == null) {
		//            this.proyecto.setProyectoPK(new ProyectoPK(folioProyecto, serialProyecto));
		//        }        
		try {
			this.proyecto = miaDao.proyecto(folioProyecto, serialProyecto);
			//this.proyecto = miaDao.proyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
		} catch (Exception e) {
			System.out.println("No encontrado");
		}

		System.out.println("Folio " + folioProyecto);
		System.out.println("Serial " + serialProyecto);

		if (proyecto.getNsub() != null) {
			SubsectorProyecto su = miaDao.getSubSector(proyecto.getNsub());
			SectorProyecto sp = miaDao.getSectorProyecto(su.getNsec());
			sector = sp.getSector();
		}

		// Leyes
		leyesEstatal = leyFedEstDAO.getLeyesEst(folioProyecto, serialProyecto);
		leyesFederal = leyFedEstDAO.getLeyesFed(folioProyecto, serialProyecto);

		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

		// reglamentos
		/*
		reglamentos = miaDao.getReglamentos(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto());
		for (ReglamentoProyecto reglamento : reglamentos) {
			try {
				if (reglamento.getReglamentoFechaPublicacion() != null) {
					reglamento.setFechaPublicacion(sdf.parse(reglamento.getReglamentoFechaPublicacion()));
				}
			} catch (Exception e) { e.printStackTrace(); }
		}
		*/

		// convenios
		convenios = miaDao.getConvevios(folioProyecto, serialProyecto);
		for (ConvenioProy convenio : convenios) {
			try {
				if (convenio.getConvenioFechaPublicacion() != null) {
					convenio.setFechaPublicacion(sdf.parse(convenio.getConvenioFechaPublicacion()));
				}
			} catch (Exception e) { e.printStackTrace(); }
		}


		// disposiciones
		disposiciones = miaDao.getDisposiciones(folioProyecto, serialProyecto);

		// Requiere disposiciones
		if (proyecto.getProyOdisposicionAplica() != null && proyecto.getProyOdisposicionAplica().equals("S")) {
			capDisposiciones = true;
		}

		// poet
		cargaPoets();

		//ANP
		cargaANPu();
		// PDU
		cargaPDUS();


		//Reglamentos

		consultarReglamento();
//
//		consultarReglamento();

		catReglamento = new CatReglamento();
		listaReglamentos();
		reglam = new ReglamentoProyecto();
		reglam.setReglamentoId(catReglamento);

		reglamed = new ReglamentoProyecto();
		reglamed.setReglamentoId(catReglamento);




		//Comentarios proyecto - eescalona
		super.setProyectoInfo((short)3, folioProyecto, serialProyecto);

		if (convenios != null) {
			capServicios = true;
			aplica = "S";
		}else {
			capServicios = false;
		}

	}


	///===========================================================================================================================================================================  


	public void agregarANPArticulos() {
		System.out.println("add amp art");
		Short id = 0;
		ArticuloReglaAnp t = new ArticuloReglaAnp(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id, (short) 1);
		t.setAnpArticuloRegla("A");
		anpArticulo.add(t);
	}

	public void eliminarANPArticulos(ActionEvent e) {
		Short idt = (Short) e.getComponent().getAttributes().get("idt");
		Short idt2 = (Short) e.getComponent().getAttributes().get("idt2");
		ArticuloReglaAnp a = new ArticuloReglaAnp(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt, idt2);
		anpArticulo.remove(a);
		try {
			miaDao.eliminar(a);
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}

	public void agregarANPRegla() {
		System.out.println("add amp art");
		Short id = 0;
		ArticuloReglaAnp t = new ArticuloReglaAnp(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id, (short) 1);
		t.setAnpArticuloRegla("R");
		anpRegla.add(t);
	}

	public void eliminarANPRegla(ActionEvent e) {
		Short idt = (Short) e.getComponent().getAttributes().get("idt");
		Short idt2 = (Short) e.getComponent().getAttributes().get("idt2");
		ArticuloReglaAnp a = new ArticuloReglaAnp(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt, idt2);

		anpRegla.remove(a);
		try {
			miaDao.eliminar(a);
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}

	private void cargaANPu() {
		List<AnpEstatal> e = siDao.getAnpEstatal(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		for (AnpEstatal t : e) {
			ANPu u = new ANPu();
			u.setAPN(t.getNombre());
			u.setCategoria(t.getCategoria());
			u.setCompatible(t.getComp());
			u.setCriterio("");
			u.setFechaDecreto(t.getFDec());
			u.setFechaPrograma(t.getFechaHora());
			u.setVinculacion(t.getDescrip());
			u.setId(t.getAnpEstatalPK().getIdr());
			u.setTipo("");
			anpu.add(u);
		}
		List<AnpMuicipal> e2 = siDao.getAnpMuicipal(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		for (AnpMuicipal t : e2) {
			ANPu u = new ANPu();
			u.setAPN(t.getNombre());
			u.setCategoria(t.getCategoria());
			u.setCompatible(t.getComp());
			u.setCriterio("");
			u.setFechaDecreto(t.getFDec());
			u.setFechaPrograma(t.getFechaHora());
			u.setVinculacion(t.getDescrip());
			u.setId(t.getAnpMuicipalPK().getIdr());
			u.setTipo("");
			anpu.add(u);
		}
		List<AnpFederal> e3 = siDao.getAnpFederal(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		for (AnpFederal t : e3) {
			ANPu u = new ANPu();
			u.setAPN(t.getNombre());
			u.setCategoria(t.getCatDecret());
			u.setCompatible(t.getComp());
			u.setCriterio("");
			u.setFechaDecreto(t.getUltDecret());
			u.setFechaPrograma(t.getFechaHora());
			u.setVinculacion(t.getDescrip());
			u.setId(t.getAnpFederalPK().getIdr());
			u.setTipo("");
			anpu.add(u);
		}

		//      anp = siDao.anpProyecto(folioProyecto, serialProyecto);
	}

	/**
	 * Metodo que se utiliza para saber si aplican acuerdos o no
	 */
	public void selAcuerdos() {
		if (aplica != null) {
			if (aplica.equals("S")) {
				capServicios = true;
			}
			if (aplica.equals("N")) {
				capServicios = false;
			}
		}
	}

	private void cargaPoets() {
		poetProyecto = miaDao.getPoetmProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

		if (poetProyecto.isEmpty()) {
			poet.clear();
			List<Object[]> gral = siDao.getOeReg1_2(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
			for (Object [] o : gral) {
				POETu p = new POETu();
				p.setTipo((String)o[0]);
				p.setNombreInstrumento((String)o[1]);
				p.setNumNom((String)o[2]);
				p.setPoliticaAmbiental((String)o[3]);
				p.setUso((String)o[4]);
				p.setCriterios((String)o[5]);
				p.setCompatibles("");
				p.setVinculacion("");
				poet.add(p);
			}

			List<Object[]> gral2 = siDao.getOeReg2_2(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
			for (Object[] o : gral2) {
				POETu p = new POETu();
				p.setTipo((String)o[0]);
				p.setNombreInstrumento((String)o[1]);
				p.setNumNom((String)o[2]);
				p.setPoliticaAmbiental((String)o[3]);
				p.setUso((String)o[4]);
				p.setCriterios((String)o[5]);
				p.setCompatibles("");
				p.setVinculacion("");
				poet.add(p);
			}

			List<Object[]> gral3 = siDao.getOeReg3_2(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
			for (Object[] o : gral3) {
				POETu p = new POETu();
				p.setTipo((String)o[0]);
				p.setNombreInstrumento((String)o[1]);
				p.setNumNom((String)o[2]);
				p.setPoliticaAmbiental((String)o[3]);
				p.setUso((String)o[4]);
				p.setCriterios((String)o[5]);
				p.setCompatibles("");
				p.setVinculacion("");
				poet.add(p);
			}

			List<Object[]> gral4 = siDao.getOeGral_2(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
			for (Object[] o : gral4) {
				POETu p = new POETu();
				p.setTipo("General");
				p.setNombreInstrumento((String)o[0]);
				p.setNumNom("" + (String)o[1].toString());
				p.setPoliticaAmbiental((String)o[2]);
				p.setUso("");
				p.setCriterios("");
				p.setCompatibles("");
				p.setVinculacion("");
				poet.add(p);
			}

			List<Object[]> gral5 = siDao.getOeMarinos_2(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
			for (Object[] o : gral5) {
				POETu p = new POETu();
				p.setTipo((String)o[0]);
				p.setNombreInstrumento((String)o[1]);
				p.setNumNom((String)o[2]);
				p.setPoliticaAmbiental((String)o[3]);
				p.setUso((String)o[4]);
				p.setCriterios((String)o[5]);
				p.setCompatibles("");
				p.setVinculacion("");
				poet.add(p);
			}

			List<Object[]> gral6 = siDao.getOePoligEnvol_2(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getClaveProyecto(), proyecto.getClaveProyecto(), proyecto.getProyectoPK().getSerialProyecto());
			for (Object[] o : gral6) {
				POETu p = new POETu();
				p.setTipo((String)o[0]);
				p.setNombreInstrumento((String)o[1]);
				p.setNumNom((String)o[2]);
				p.setPoliticaAmbiental((String)o[3]);
				p.setUso((String)o[4]);
				p.setCriterios((String)o[5]);
				p.setCompatibles("");
				p.setVinculacion("");
				poet.add(p);
			}
			//            List<PoetmProyecto> pmp = miaDao.getPoetMProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
			//            for (PoetmProyecto o : pmp) {
			//                POETu p = new POETu();
			//                p.setTipo("POEM_TIPO");
			//                p.setNombreInstrumento(o.getPoetmNombreInstrumento());
			//                p.setNumNom(o.getPoetmNombreUga());
			//                p.setPoliticaAmbiental(o.getPoetmPoliticaAmbiental());
			//                p.setUso(o.getPoetmUsoPredominante());
			//                p.setCriterios(o.getPoetmCriterio());
			//                p.setCompatibles(o.getPoetmCompatible());
			//                p.setVinculacion(o.getPoetmVinculacion());
			//                p.setId(o.getPoetmProyectoPK().getPoetmId());
			//                poet.add(p);
			//            }

			for (POETu u : poet) {
				Short id = miaDao.getMaxPoetmProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
				PoetmProyecto m = new PoetmProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
				//                m.setPoetmCompatible(u.getCompatibles());
				m.setPoetmCriterio(u.getCriterios());
				m.setPoetmNombreInstrumento(u.getNombreInstrumento());
				m.setPoetmNombreUga(u.getNumNom());
				m.setPoetmPoliticaAmbiental(u.getPoliticaAmbiental());
				m.setPoetmTipo(u.getTipo());
				m.setPoetmUsoPredominante(u.getUso());
				m.setPoetmVinculacion(u.getVinculacion());
				try {
					miaDao.merge(m);
				} catch (Exception e) {
					e.printStackTrace();
				}
				poetProyecto.add(m);
			}
		}
		
        respuestaPOET = Boolean.FALSE;
        respuestaPOET = proyecto.getProySupPoetPdu()==null?Boolean.FALSE:Boolean.TRUE;
	}

	private void cargaPDUS() {
		pduProyecto = miaDao.getPdumProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		if (pduProyecto.isEmpty()) {
			List<InstrUrbanos> u = siDao.getInstrUbr(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), proyecto.getClaveProyecto());
			for (InstrUrbanos t : u) {
				Short id = miaDao.getMaxPdumProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
				PdumProyecto p = new PdumProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
				p.setPdumClaveUsos(t.getClvUsocp());
				//                p.setPdumCos();
				//                p.setPdumCus();
				p.setPdumNombre(t.getInstUrb());
				p.setPdumUsos(t.getUsoclaspol());
				try {
					miaDao.merge(p);
				} catch (Exception e) {
					e.printStackTrace();
				}
				pduProyecto.add(p);
			}
		}
		
		this.respuestaIncidePDU = Boolean.FALSE;
		this.respuestaIncidePDU = proyecto.getProyPduAplica()==null?Boolean.FALSE:(proyecto.getProyPduAplica().equalsIgnoreCase("S")?Boolean.TRUE:Boolean.FALSE);
		//        List<PdumProyecto> u2 = miaDao.getPdum(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		//        for (PdumProyecto t : u2) {
		//            PDUu u3 = new PDUu();
		//            u3.setClaveUsos(t.getPdumClaveUsos());
		//            u3.setCos(t.getPdumCos());
		//            u3.setCus(t.getPdumCus());
		//            u3.setNombrePlan(t.getPdumNombre());
		//            u3.setTipo("Pdum");
		//            u3.setUsos(t.getPdumUsos());
		//            u3.setVinculacion(t.getPdumVinculacion());
		//        }
	}

	public void agregarPdum() {
		short id = 0;

		PDUu u = new PDUu();
		u.setId(id);
		u.setTipo("Pdum");

		pdu1.add(u);
	}




	public void eliminarPoet() {
		int id = 0;
		for (PoetmProyecto poetmProyecto : selectedPoet) {
			id = poetmProyecto.getPoetmProyectoPK().getPoetmId();
			PoetmProyecto poetmp = new PoetmProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) id);
			poetProyecto.remove(poetmp);
			try {
				//Eliminacion de Archivos en FileSystem
    			if (poetmProyecto.getIdArchivoProyecto() != null) {
    				ArchivoBean.eliminarArchivoProyecto(poetmProyecto.getIdArchivoProyecto());
    			}
				miaDao.eliminarPoetM(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) id);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}

		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("modalEliminar.hide();modalMensajeEliminar.show()");
		cargaPoets();


		//        Short idt = (Short) e.getComponent().getAttributes().get("idt");
		//        System.out.println("Eliminar idt " + idt);
		//        try {
		//            miaDao.eliminarPoetM(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt);
		//        } catch (Exception e2) {
		//            e2.printStackTrace();
		//        }
		//        cargaPoets();
	}
	/**
	 * Metodo para inicializar el objeto poetm para realizar un alta de registro
	 */
	public void crearPoet(){
		poetm = new PoetmProyecto();
	}
	/**
	 * Metodo para dar de alta un registro de POET
	 */
	public void agregarPoet() {
		Short id = 0;
		try {
			id = miaDao.getMaxPoetM(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
			id++;
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (id == null) {
			id = 0;
		}
		PoetmProyecto poetp = new PoetmProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
		poetp.setPoetmTipo(poetm.getPoetmTipo());
		poetp.setPoetmNombreInstrumento(poetm.getPoetmNombreInstrumento());
		poetp.setPoetmNombreUga(poetm.getPoetmNombreUga());
		poetp.setPoetmPoliticaAmbiental(poetm.getPoetmPoliticaAmbiental());
		poetp.setPoetmUsoPredominante(poetm.getPoetmUsoPredominante());
		poetp.setPoetmCriterio(poetm.getPoetmCriterio());
		poetp.setPoetmCompatible(poetm.getPoetmCompatible());
		poetp.setPoetmVinculacion(poetm.getPoetmVinculacion());
		poetp.setClaveOrigenRegistro(1);

		try {
			miaDao.persist(poetp);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalAgregar.hide();modalMensaje.show()");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		poetProyecto = miaDao.getPoetmProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		//        poetProyecto.add(poetp);
		//        p.setTipo("POEM_TIPO");
		//        p.setId(id);
		//        poet.add(poetp);
	}


	/**
	 * Metodo para consultar un registro de POET
	 */
	public void consultarPoet() {
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		int id = 0;
		String idPoet = params.get("id").toString();
		id = Integer.parseInt(idPoet);
		System.out.println("Registro de Poet: "+id);
//		editPoet= new PoetmProyecto();
		editPoet = miaDao.getPoet(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) id);
		
		// Numero secuencial a mostrar en "Editar registro #"
		rowIndexPOETEditar = Integer.parseInt(params.get("prowindexPOET").toString());
	}

	/**
	 * Metodo para editar un registro de POET
	 */
	public void editarPoet() {
		PoetmProyecto poetN = new PoetmProyecto();
		poetN = editPoet;
		try {
			miaDao.merge(poetN);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditar.hide();modalMensajeEditar.show()");
		} catch (Exception e) {
			e.printStackTrace();
		}
		poetProyecto = miaDao.getPoetmProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
	}

	/**
	 * Inicializa un nuevo archivo para ser cargado
	 */
	public void iniciarArchivo() {
		archivoTemporal = new ArchivosProyecto();
		archivoTemporal = new ArchivosProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), 
				(short) 3, (short) 4, (short) 0, (short) 0);
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		short pId = 0;
		String id = params.get("pid").toString();
		pId = Short.parseShort(id);
		this.editPoet = miaDao.getPoet(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), pId);
		cargaBean = new CargaBean();
		msj = "";
	}

	/**
	 * Guarda el archivo pdf en la BD y el Servidor 
	 */
	public void guardarArchivo() {
		try {
	    	short id;
			id = miaDao.getMaxArchivoEtapaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), 
					archivoTemporal.getCapituloId(), archivoTemporal.getSubCapituloId(), archivoTemporal.getSeccionId(), archivoTemporal.getApartadoId());

			archivoTemporal.setId(id);
			
			bandera = cargaBean.GuardaAnexos(archivoTemporal);
			if (bandera == true) {
			
			Short idSeq = archivosAnexosDAO.consultarMaxIdSeq();
			if (idSeq != null) {
				this.editPoet.setIdArchivoProyecto((short) (idSeq));
			} else {
				this.editPoet.setIdArchivoProyecto((short) 1);
			}
//			this.editPoet.setIdArchivoProyecto(archivoTemporal.getSeqId());

			miaDao.merge(editPoet);
			msj = "";
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalSubirArchivo.hide();modalAlertArchP.show()");
			
			}else {
				msj = "Se requiere cargar un archivo";
				
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		poetProyecto = miaDao.getPoetmProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
	}
	
	public void eliminarLeyFed(ActionEvent e) {
		Short idt = (Short) e.getComponent().getAttributes().get("idt");
		System.out.println("Eliminar idt " + idt);
		try {
			miaDao.eliminarLeyFedEst(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt, "F");
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		leyesFederal = miaDao.getLeyesFed(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		leyesEstatal = miaDao.getLeyesEst(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
	}

	public void agregarLeyFed() {
		Short id;

		//        try {
		//            id = miaDao.getMaxLeyFedEst(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		//            id++;
		//        } catch (Exception e2) {
		//            e2.printStackTrace();
		//        }
		//        if (id == null) {
		//            id = 0;
		//        }

		if (leyesFederal == null || leyesFederal.isEmpty()) {
			id = 1;
		} else {
			id = obtenerMaximoLeyes(leyesFederal);
		}

		LeyFedEstProyecto l = new LeyFedEstProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
		l.setProyecto(proyecto);
		l.setLeyFeBandera("F");
		leyesFederal.add(l);
	}

	private Short obtenerMaximoLeyes(List<LeyFedEstProyecto> lst) {
		Short idx = 0;
		for(LeyFedEstProyecto ele : lst) {
			if (ele.getLeyFedEstProyectoPK().getLeyFeId() > idx) {
				idx = ele.getLeyFedEstProyectoPK().getLeyFeId();
			}
		}
		idx++;

		return idx;
	}

	public void eliminarLeyEst(ActionEvent e) {
		Short idt = (Short) e.getComponent().getAttributes().get("idt");
		System.out.println("Eliminar idt " + idt);
		try {
			miaDao.eliminarLeyFedEst(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt, "E");
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		leyesFederal = miaDao.getLeyesFed(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		leyesEstatal = miaDao.getLeyesEst(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
	}

	public void agregarLeyEst() {
		Short id;

		//        try {
		//            id = miaDao.getMaxLeyFedEst(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		//            id++;
		//        } catch (Exception e2) {
		//            e2.printStackTrace();
		//        }
		//        if (id == null) {
		//            id = 0;
		//        }
		if (leyesEstatal == null || leyesEstatal.isEmpty()) {
			id = 1000;
		} else {
			id = obtenerMaximoLeyes(leyesEstatal);
		}

		LeyFedEstProyecto l = new LeyFedEstProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
		l.setProyecto(proyecto);
		l.setLeyFeBandera("E");
		leyesEstatal.add(l);
	}



	public void crearReglamento(){
		reglam = new ReglamentoProyecto();
		catReglamento.setReglamentoId(null);
		reglam.setReglamentoId(catReglamento);
	}

	public void agregarReglamento() {
		Short id = 0;

		try {
			id = miaDao.getMaxReglamento(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
			id++;
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		if (id == null) {
			id = 0;
		}

		//        LeyFedEstProyecto l = new LeyFedEstProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
		ReglamentoProyecto rp = new ReglamentoProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
		rp.setProyecto(proyecto);
		//reglamentos.add(rp);
	}
	/**
	 * Se encarga de Inicializar el archivo Temporal de para cargar en Reglamentos
	 */
	public void iniciarArchivoReglamento() {
		archivoTemporalReglamento = new ArchivosProyecto();
		archivoTemporalReglamento = new ArchivosProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), 
				(short) 3, (short) 2, (short) 0, (short) 0);
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		short idr = 0;
		String id = params.get("id").toString();
		idr = Short.parseShort(id);
		this.reglamed = reglamentoDAO.consultarReglamentoPorID(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),idr);
	
		cargaBean = new CargaBean();
		
		msj = "";
	}
	
	/**
	 * Guarda el archivo pdf en la BD y el Servidor 
	 */
	public void guardarArchivoReglamento() {
		try {
	    	short id;
			id = miaDao.getMaxArchivoEtapaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), 
					archivoTemporalReglamento.getCapituloId(), archivoTemporalReglamento.getSubCapituloId(), archivoTemporalReglamento.getSeccionId(), archivoTemporalReglamento.getApartadoId());

			archivoTemporalReglamento.setId(id);
			
			bandera = cargaBean.GuardaAnexos(archivoTemporalReglamento);
			if (bandera == true) {
			
			Short idSeq = archivosAnexosDAO.consultarMaxIdSeq();
			if (idSeq != null) {
				this.reglamed.setIdArchivoProyecto((short) (idSeq));
			} else {
				this.reglamed.setIdArchivoProyecto((short) 1);
			}

//			this.reglamed.setIdArchivoProyecto(archivoTemporalReglamento.getSeqId());
			miaDao.merge(reglamed);
			msj = "";
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalSubirArchivo.hide();modalAlertArchP.show()");
			
			}else {
				msj = "Se requiere cargar un archivo";
				
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		reglamentos = miaDao.getReglamentos(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
	}
	
	public void elimiarConvenio(ActionEvent evt) {
		Short id = (Short) evt.getComponent().getAttributes().get("idt");
		ConvenioProy c = new ConvenioProy(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id);
		try {
			miaDao.elimina(ConvenioProy.class, new ConvenioProyPK(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), id));
		} catch (Exception e) {

		}
		convenios.remove(c);
	}

	@SuppressWarnings("unused")
	public void creaConvenio(){
		Short convenioId=0;

		try {
			convenioId = miaDao.getMaxConvenios(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());

			if(convenioId==null){
				convenioId=1;
			}else{
				convenioId++;
			}

			agrega=new ConvenioProy(proyecto.getProyectoPK().getFolioProyecto(),proyecto.getProyectoPK().getSerialProyecto(),convenioId);	
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalAgregar.show()");

		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}

	public void agregarConvenio() {
		agrega.setConvenioDescripcion(agrega.getConvenioId().getConvenioDescripcion());
		agrega.setConvenioFechaPublicacion(sdf.format(agrega.getFechaPublicacion()));

		try{
			miaDao.persist(agrega);
			convenios.add(agrega);
			agrega = new ConvenioProy();
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalAgregar.hide(),modalConvenioAgregado.show()");
			RequestContext.getCurrentInstance().reset(":agregar:pnlAgregar");
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	public void consultaConvenioId(){
		short convenioId=0;
		@SuppressWarnings("rawtypes")
		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String convenioIdSeleccionado= params.get("id").toString();
		convenioId=Short.parseShort(convenioIdSeleccionado);
		edita = new ConvenioProy();
		edita=miaDao.getconvenioByFolio(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),convenioId);
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("modalEditar.show()");
				
		// Numero secuencial a mostrar en "Editar registro #"
		rowIndexAcuerdoEditar = Integer.parseInt(params.get("prowindexAcuerdo").toString());
	}

	public void editarConvenio(){
		ConvenioProy sp = new ConvenioProy();

		edita.getConvenioProyPK().setFolioProyecto(proyecto.getProyectoPK().getFolioProyecto()); 
		edita.getConvenioProyPK().setSerialProyecto(proyecto.getProyectoPK().getSerialProyecto());
		edita.setConvenioDescripcion(edita.getConvenioId().getConvenioDescripcion());
		edita.setConvenioFechaPublicacion(sdf.format(edita.getFechaPublicacion()));
		sp = edita;
		try {
			miaDao.merge(sp);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEditar.hide();modalMensajeEditar.show()");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void tieneConveniosSeleccionados(){
		if(!conveniosElegidos.isEmpty()){
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("modalEliminar.show()");
		}
	}


	public void eliminarConvenios(){
		try{
			if(!conveniosElegidos.isEmpty()){
				for(ConvenioProy i: conveniosElegidos){
					miaDao.eliminaConvenioByFolio(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),i.getConvenioProyPK().getConvenioProyId());
					convenios.remove(i);
				}

//				RequestContext context = RequestContext.getCurrentInstance();
//				context.execute("modalEliminar.hide(),modalBorrado.show();");
				convenios = miaDao.getConvevios(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	public void seleAnp(SelectEvent evt) {
		ArticuloReglaAnp t = new ArticuloReglaAnp();
		t.setAnpArticuloRegla("" + new Date().getTime());
		anpArticulo.add(t);
	}

	/**
	 * Listener seleccion requerirá otras disposiciones
	 */
	public void selDisposiciones() {
		if (proyecto.getProyOdisposicionAplica()!= null) {
			if (proyecto.getProyOdisposicionAplica().equals("S")) {
				capDisposiciones = true;
			}
			if (proyecto.getProyOdisposicionAplica().equals("N")) {
				capDisposiciones = false;
			}
		}
	}

	public void guardar() {
		RequestContext reqcontEnv = RequestContext.getCurrentInstance();
		String msg = "";
		try {
			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			miaDao.guardar(proyecto);
//
//			for (LeyFedEstProyecto l : leyesFederal) {
//				if (l.getLeyId().getLeyId() != 9999) {
//					l.setLeyFeDescrip(null);
//				}
//				miaDao.merge(l);
//			}

//			for (LeyFedEstProyecto l : leyesEstatal) {
//				miaDao.merge(l);
//			}
			if (leyesFederal.size() >= 1 || leyesEstatal.size() >= 1) {
				//<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
				try {
					miaDao.guardarAvance(proyecto, new Short("2"), "31");
				} catch (Exception e) {
					System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
				}
				//</editor-fold>
			}

//			for (ReglamentoProyecto r : reglamentos) {
//				if ( r.getFechaPublicacion() != null ) {
//					r.setReglamentoFechaPublicacion(sdf.format(r.getFechaPublicacion()));
//				}
//				miaDao.merge(r);
//			}
			if (reglamentos.size() >= 1) {
				//<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
				try {
					miaDao.guardarAvance(proyecto, new Short("2"), "32");
				} catch (Exception e) {
					System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
				}
				//</editor-fold>
			}

			for (ConvenioProy c : convenios) {
				if ( c.getFechaPublicacion() != null ) {
					c.setConvenioFechaPublicacion(sdf.format(c.getFechaPublicacion()));
				}
				miaDao.merge(c);
			}
			//            if (convenios.size() >= 1) {
			//<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
			try {
				miaDao.guardarAvance(proyecto, new Short("1"), "37");
			} catch (Exception e) {
				System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
			}
			//</editor-fold>

			//            }


			// Guardar avance del proyecto: Otras Disposiciones
			try {
				if (disposiciones.size() >= 1) {
					try {
						miaDao.guardarAvance(proyecto, new Short("1"), "38");
						System.out.println("Otras Disposiciones: Después de guardar avance.");
					} catch (Exception e) {
						System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
					}
				}
			}
			catch (NullPointerException ex) {
				System.out.println("Aún no ha sido capturado el apartado de otras disposiciones");
			}


			//            for (POETu p : poet) {
			//                if (p.getTipo().equals("POEM_TIPO")) {
			//                    PoetmProyecto m = new PoetmProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), p.getId());
			//                    m.setPoetmCompatible(p.getCompatibles());
			//                    m.setPoetmCriterio(p.getCriterios());
			//                    m.setPoetmNombreInstrumento(p.getNombreInstrumento());
			//                    m.setPoetmNombreUga(p.getNumNom());
			//                    m.setPoetmPoliticaAmbiental(p.getPoliticaAmbiental());
			//                    m.setPoetmTipo(p.getTipo());
			//                    m.setPoetmUsoPredominante(p.getUso());
			//                    m.setPoetmVinculacion(p.getVinculacion());
			//                    miaDao.merge(m);
			//                }
			//            }
			//            for (PoetmProyecto p : poetProyecto) {
			//                miaDao.merge(p);
			//            }

			if (poetProyecto.size() >= 1) {
				//<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
				try {
					miaDao.guardarAvance(proyecto, new Short("2"), "34");
				} catch (Exception e) {
					System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
				}
				//</editor-fold>
			}

			for (ArticuloReglaAnp a : anpArticulo) {
				miaDao.merge(a);
			}

			for (ArticuloReglaAnp a : anpRegla) {
				miaDao.merge(a);
			}

			if (anpArticulo.size() >= 1 && anpRegla.size() >= 1) {
				//<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
				try {
					miaDao.guardarAvance(proyecto, new Short("2"), "35");
				} catch (Exception e) {
					System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
				}
				//</editor-fold>
			}

			for (PdumProyecto p : pduProyecto) {
				miaDao.merge(p);
			}

			if (pduProyecto.size() >= 1){
				//<editor-fold desc="guardado de avance local y SINATEC" defaultstate="collapsed">
				try {
					miaDao.guardarAvance(proyecto, new Short("2"), "36");
				} catch (Exception e) {
					System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
				}
				//</editor-fold>
			}

			/*
			try {
				if (seccion.compareTo("3.8") != 0) {
					Validacion v = new Validacion();
					msg = v.validar(seccion, proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
				}
				if (!msg.isEmpty()) {
					return;
				}
			} catch (Exception err) {
				err.printStackTrace();
			}
			*/
			
			//reqcontEnv.execute("alert('Su información ha sido guardada con éxito.')");
			reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");

			if (Integer.parseInt(miaDao.avanceProyecto(proyecto)) == 99) {
				//reqcontEnv.execute("alert('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.')");
				reqcontEnv.execute("mensajes('Ha completado el registro de datos de la Manifestación de Impacto Ambiental.', 'success')");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.error(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), e.getMessage());

			//reqcontEnv.execute("alert('No se ha podido guardar la información, Intente más tarde.')");
			reqcontEnv.execute("mensajes('No se ha podido guardar la información, Intente más tarde.', 'danger')");
		}

		//Guardado de componentes en información adicional - eescalona
		super.guardarComentarios();        
	}

	/**
	 * Permite guardar el avance del proyecto.
	 */
	public void guardarAvance() {
		RequestContext reqcontEnv = RequestContext.getCurrentInstance();
		try {
			miaDao.guardarAvance(proyecto, new Short("2"), "31");
			reqcontEnv.execute("mensajes('Su información ha sido guardada con éxito.', 'success')");
		} catch (Exception e) {
			System.out.println("[ERR] Error al cargar avance: " + e.getCause().getMessage());
			reqcontEnv.execute("mensajes('No se ha podido guardar la información, Intente más tarde.', 'danger')");
						
		}
	}    

	/**
	 * Permite mostrar la vinculacion en el dialog.
	 * @param vinculacion.
	 */
	public void mostrarVinculacion(String vinculacion) {
		this.vinculacionLey = vinculacion;
	}

	public void mostrarVinculacionPoet(String vinculacion) {
		this.setVinculacionPoet(vinculacion);
	}


	public void mostrarVinculacionReg(String vinculacion) {
		this.vinculacionReg = vinculacion;
	}

	/**
	 * Permite mostrar el titulo correspondiente a Editar Ley Federal.
	 */
	public void abrirEditarLeyFed(short idLeySeq, int rowIndexLeyF) {
		// Numero secuencial a mostrar en "Editar registro #"
		rowIndexLeyFedEditar = rowIndexLeyF;
		limpiarLeyFederal();
		this.idLeySeqSeleccionada = idLeySeq;
		this.leyFedEstProyecto = leyFedEstDAO.consultarLeyFederalPorId(this.idLeySeqSeleccionada);
		mostrarEdicion = true;

	}

	/**
	 * Permite actualizar el dialog con el panel de guardado de ley estatal.
	 */
	public void abrirGuardarLeyFed() {
		this.leyFedEstProyecto = new LeyFedEstProyecto();
		CatLey catLey = new CatLey();
		this.leyFedEstProyecto.setLeyId(catLey);
		mostrarEdicion = false;
		catalogoLeyesFederales = miaDao.getCatLeyFederal();
		RequestContext.getCurrentInstance().reset(":formEditarLeyFed:opAgregarLeyFed");
	}    

	public void cargaReglamentos()
	{

		catReg = miaDao.getCatReglamento();
	}

	/**
	 * Permite actualizar el dialog con el panel de edicion de ley federal.
	 */
	public void abrirEditarLeyEst(short idLeySeq, int rowIndexLeyE) {
		// Numero secuencial a mostrar en "Editar registro #"
		rowIndexLeyEstEditar = rowIndexLeyE;
		limpiarLeyFederal();
		this.idLeySeqSeleccionada = idLeySeq;
		this.leyFedEstProyecto = leyFedEstDAO.consultarLeyProyectoPorId(this.idLeySeqSeleccionada);
		mostrarEdicionEstatal = true;		

	}

	/**
	 * Permite actualizar el dialog con el panel de guardado de ley federal.
	 */
	public void abrirGuardarLeyEst() {
		this.leyFedEstProyecto = new LeyFedEstProyecto();
		CatLey catLey = new CatLey();
		this.leyFedEstProyecto.setLeyId(catLey);

		mostrarEdicionEstatal = false;

		RequestContext.getCurrentInstance().reset(":formEditarLeyFed:opEditarLeyFed");		

	}    
	/**
	 * Permite abrir la ventana de dialog para eliminar.
	 */
	public void abrirDialogEliminar() {
		//Si se seleccionaron leyes federales.
		if (leyesFederalesSeleccionadas != null && leyesFederalesSeleccionadas.size() > 0) {

		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("dlgSeleccionLey.show()");
		}
	}

	/**
	 * Permite abrir la ventana de dialog para eliminar leyes estatales
	 */
	public void abrirDialogoEliminarLeyEst() {
		//Si se seleccionaron leyes estatales.
		if (leyesEstatalesSeleccionadas != null && leyesEstatalesSeleccionadas.size() > 0) {
			//			RequestContext context = RequestContext.getCurrentInstance();
			//			context.execute("dlgEliminarLeyEst.show()");
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("dlgSeleccionLey.show()");
		}		
	}

	/**
	 * Permite eliminar una ley federal seleccionada.
	 */
	public void eliminarLeyFederal() {
		leyFedEstDAO =  new LeyFedEstDAO();
		int leyesEliminadas = 0;
		if (leyesFederalesSeleccionadas != null && leyesFederalesSeleccionadas.size() > 0) {
			for (LeyFedEstProyecto leyFedEstProyecto : leyesFederalesSeleccionadas) {
				try {
	    			//Eliminacion de archivo
	    			if (leyFedEstProyecto.getIdArchivoProyecto() != null) {
	    				ArchivoBean.eliminarArchivoProyecto(leyFedEstProyecto.getIdArchivoProyecto());
	    			}
					leyesEliminadas = leyFedEstDAO.eliminarLeyFedEst(leyFedEstProyecto.getIdLeySeq());
				} catch (Exception e) {
					e.printStackTrace();
					return;
				}
			}	
			/**
			 * Si se eliminaron leyes.
			 */
			if (leyesEliminadas > 0) {
				//Se consulta de nuevo la lista para revisar los cambios afectados.
				leyesFederal = leyFedEstDAO.getLeyesFed(proyecto.getProyectoPK().getFolioProyecto(),
						proyecto.getProyectoPK().getSerialProyecto());

				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("dlgEliminarLeyFed.hide(); dlgElimiLeyFedExitosa.show()");
				leyesFederalesSeleccionadas = new ArrayList<LeyFedEstProyecto>();
			}

			leyesFederalesSeleccionadas = new ArrayList<>();
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("dlgSeleccionLey.show()");
			return;
		}
	}

	/**
	 * Permite eliminar una ley estatal seleccionada.
	 */
	public void eliminarLeyEstatal() {
		leyFedEstDAO =  new LeyFedEstDAO();
		int leyesEstatalesEliminadas = 0;
		if (leyesEstatalesSeleccionadas != null && leyesEstatalesSeleccionadas.size() > 0) {
			for (LeyFedEstProyecto leyFedEstProyecto : leyesEstatalesSeleccionadas) {
				try {
					//Eliminacion de Archivos en FileSystem
	    			if (leyFedEstProyecto.getIdArchivoProyecto() != null) {
	    				ArchivoBean.eliminarArchivoProyecto(leyFedEstProyecto.getIdArchivoProyecto());
	    			}
					leyesEstatalesEliminadas = leyFedEstDAO.eliminarLeyFedEst(leyFedEstProyecto.getIdLeySeq());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}	

			if (leyesEstatalesEliminadas > 0) {
				//Se consulta de nuevo la lista para revisar los cambios afectados.
				leyesEstatal = leyFedEstDAO.getLeyesEst(proyecto.getProyectoPK().getFolioProyecto(),
						proyecto.getProyectoPK().getSerialProyecto());
				leyesEstatalesSeleccionadas = new ArrayList<LeyFedEstProyecto>();
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("dlgEliminarLeyEst.hide(); dlgElimiLeyFedExitosa.show()");
			}

//			leyesEstatalesSeleccionadas = new ArrayList<>();

		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("dlgSeleccionLey.show()");		

			return;
		}		
	}  
	/**
	 * Permite mostrar u ocultar el panel para ingresar una nueva ley federal.
	 * @param idLeySeleccionada
	 */
	public void mostrarCapturaLey(short idLeySeleccionada) {
		if (idLeySeleccionada != 9999) {
			mostrarCapturaLeyFederal = false;
			mostrarUltimaFecha = true;
			CatLey catLey = miaDao.consultarLeyFederal(idLeySeleccionada);
			this.leyFedEstProyecto.getLeyId().setLeyFechaModificacion(catLey.getLeyFechaModificacion());
		} else {
			mostrarCapturaLeyFederal = true;
			mostrarUltimaFecha = false;
			this.leyFedEstProyecto.getLeyId().setLeyFechaModificacion(null);
		}
	}

	/**
	 * Permite guardar la ley estatal del proyecto.
	 */
	public void guardarLeyEstatalProy() {
		if (leyFedEstProyecto.getIdLeySeq() != null) {
			//Se actualiza la ley.
			try {

				//Si se asigna una fecha de actualizacion.
				if (fechaUltimaActualizacion != null) {
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String fechaActualizacionConvert = simpleDateFormat.format(fechaUltimaActualizacion);
					String convertFecha = "(DOF " + fechaActualizacionConvert + ")";					
					this.leyFedEstProyecto.setFechaUltimaActualizacion(convertFecha);
				}				

				leyFedEstDAO.actualizarLeyFedProy(leyFedEstProyecto);

				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("dlgEditarLeyEst.hide();dlgActualizacionExitosa.show()");				

				leyesEstatal = leyFedEstDAO.getLeyesEst(this.proyecto.getProyectoPK().getFolioProyecto(), 
						this.proyecto.getProyectoPK().getSerialProyecto());
				this.leyFedEstProyecto = new LeyFedEstProyecto();
				this.leyFedEstProyecto.setLeyId(new CatLey());

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			//Se consultar el valor maximo del id de la ley.
			Short idMax = leyFedEstDAO.consultarMaxIdLeySeq();


			if (idMax != null) {
				this.leyFedEstProyecto.setIdLeySeq((short) (idMax + 1));
			} else {
				this.leyFedEstProyecto.setIdLeySeq((short) 1);
			}

			//Si la ley es capturada por el promovente.

			this.leyFedEstProyecto.setLeyFeBandera("E");
			String folioProyecto = this.proyecto.getProyectoPK().getFolioProyecto();
			short serialProyecto = this.proyecto.getProyectoPK().getSerialProyecto();

			Short idLeyMax = leyFedEstDAO.getMaxLeyFedEst();

			if (idLeyMax != null) {
				idLeyMax = (short) (idLeyMax + 1);
			} else {
				idLeyMax = 1;
			}


			LeyFedEstProyectoPK leyFedEstProyectoPK = new LeyFedEstProyectoPK();
			leyFedEstProyectoPK.setFolioProyecto(folioProyecto);
			leyFedEstProyectoPK.setSerialProyecto(serialProyecto);		
			leyFedEstProyectoPK.setLeyFeId(idLeyMax);

			this.leyFedEstProyecto.setLeyFedEstProyectoPK(leyFedEstProyectoPK);
			this.leyFedEstProyecto.setLeyId(null);
			try {
				//Si se asigna una fecha de actualizacion.
				if (fechaUltimaActualizacion != null) {
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String fechaActualizacionConvert = simpleDateFormat.format(fechaUltimaActualizacion);
					String convertFecha = "(DOF " + fechaActualizacionConvert + ")";					
					this.leyFedEstProyecto.setFechaUltimaActualizacion(convertFecha);
				}


				//Se guarda la ley federal del proyecto.
				leyFedEstDAO.guardarLeyFedProy(this.leyFedEstProyecto);
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("dlgEditarLeyEst.hide();dlgGuardadoExitoso.show()");

				leyesEstatal = leyFedEstDAO.getLeyesEst(folioProyecto, serialProyecto);
				this.leyFedEstProyecto = new LeyFedEstProyecto();
				this.leyFedEstProyecto.setLeyId(new CatLey());
				mostrarCapturaLeyFederal = false;

			} catch (Exception e) {			
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}			
	}    

	/**
	 * Permite guardar la ley federal del proyecto.
	 */
	public void guardarLeyFederalProy() {
		if (leyFedEstProyecto.getIdLeySeq() != null) {
			//Se actualiza la ley.
			try {
				//Si se asigna una fecha de actualizacion.
				if (fechaUltimaActualizacion != null) {
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String fechaActualizacionConvert = simpleDateFormat.format(fechaUltimaActualizacion);
					String convertFecha = "(DOF " + fechaActualizacionConvert + ")";					
					this.leyFedEstProyecto.setFechaUltimaActualizacion(convertFecha);
				}				

				leyFedEstDAO.actualizarLeyFederalProy(leyFedEstProyecto);

				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("dlgEditarLeyFed.hide();dlgActualizacionExitosa.show()");				

				leyesFederal = leyFedEstDAO.getLeyesFed(this.proyecto.getProyectoPK().getFolioProyecto(), 
						this.proyecto.getProyectoPK().getSerialProyecto());
				this.leyFedEstProyecto = new LeyFedEstProyecto();
				this.leyFedEstProyecto.setLeyId(new CatLey());

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			//Se consultar el valor maximo del id de la ley.
			Short idMax = leyFedEstDAO.consultarMaxIdLeySeq();

			if (idMax != null) {
				this.leyFedEstProyecto.setIdLeySeq((short) (idMax + 1));
			} else {
				this.leyFedEstProyecto.setIdLeySeq((short) 1);
			}

			//Si la ley es capturada por el promovente.
			if (this.leyFedEstProyecto.getLeyId().getLeyId() != 9999) {
				CatLey catLey = leyFedEstDAO.consultarLeyPorId(this.leyFedEstProyecto.getLeyId().getLeyId());
				this.leyFedEstProyecto.setLeyFeDescrip(catLey.getLeyNombre());
			}

			this.leyFedEstProyecto.setLeyFeBandera("F");

			String folioProyecto = this.proyecto.getProyectoPK().getFolioProyecto();
			short serialProyecto = this.proyecto.getProyectoPK().getSerialProyecto();

			Short idLeyMax = leyFedEstDAO.getMaxLeyFedEst();

			if (idLeyMax != null) {
				idLeyMax = (short) (idLeyMax + 1);
			} else {
				idLeyMax = 1;
			}

			LeyFedEstProyectoPK leyFedEstProyectoPK = new LeyFedEstProyectoPK();
			leyFedEstProyectoPK.setFolioProyecto(folioProyecto);
			leyFedEstProyectoPK.setSerialProyecto(serialProyecto);		
			leyFedEstProyectoPK.setLeyFeId(idLeyMax);

			this.leyFedEstProyecto.setLeyFedEstProyectoPK(leyFedEstProyectoPK);

			//Si se asigna una fecha de actualizacion.
			if (fechaUltimaActualizacion != null) {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
				String fechaActualizacionConvert = simpleDateFormat.format(fechaUltimaActualizacion);
				String convertFecha = "(DOF " + fechaActualizacionConvert + ")";					
				this.leyFedEstProyecto.setFechaUltimaActualizacion(convertFecha);
			}
			
			try {
				//Se guarda la ley federal del proyecto.
				leyFedEstDAO.guardarLeyFedProy(this.leyFedEstProyecto);
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("dlgEditarLeyFed.hide();dlgGuardadoExitoso.show()");

				leyesFederal = leyFedEstDAO.getLeyesFed(folioProyecto, serialProyecto);
				this.leyFedEstProyecto = new LeyFedEstProyecto();
				this.leyFedEstProyecto.setLeyId(new CatLey());
				mostrarCapturaLeyFederal = false;

			} catch (Exception e) {			
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
	/**
	 * Inicializa un nuevo archivo federal para ser cargado
	 */
	public void iniciarArchivoFederal() {
		archivoTemporalFederal = new ArchivosProyecto();
		archivoTemporalFederal = new ArchivosProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), 
				(short) 3, (short) 1, (short) 1, (short) 0);

		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		short idLeySeq = 0;
		String idLeyParam = params.get("idLeySeq").toString();
		idLeySeq = Short.parseShort(idLeyParam);

		this.leyFedSeleccionado = leyFedEstDAO.consultarLeyProyectoArchivos(idLeySeq);
		
		cargaBean = new CargaBean();
		
		msj = "";
	}

	/**
	 * Inicializa un nuevo archivo estatal para ser cargado
	 */
	public void iniciarArchivoEstatal() {
		archivoTemporalEstatal = new ArchivosProyecto();
		archivoTemporalEstatal = new ArchivosProyecto(getProyecto().getProyectoPK().getFolioProyecto(), getProyecto().getProyectoPK().getSerialProyecto(), 
				(short) 3, (short) 1, (short) 2, (short) 0);

		Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		short idLeySeq = 0;
		String idLeyParam = params.get("idLeySeq").toString();
		idLeySeq = Short.parseShort(idLeyParam);

		this.leyEstSeleccionado = leyFedEstDAO.consultarLeyProyectoArchivos(idLeySeq);

		cargaBean = new CargaBean();

		msj = "";
	}    
	/**
	 * Permite guardar un nuevo archivo federal.
	 */
	public void guardarArchivoFederal() {
		try {
	    	short id;
			id = miaDao.getMaxArchivoEtapaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), 
					archivoTemporalFederal.getCapituloId(), archivoTemporalFederal.getSubCapituloId(), archivoTemporalFederal.getSeccionId(), archivoTemporalFederal.getApartadoId());

			archivoTemporalFederal.setId(id);
			

			
			bandera = cargaBean.GuardaAnexos(archivoTemporalFederal);
			if (bandera == true) {

			Short idSeq = archivosAnexosDAO.consultarMaxIdSeq();
			if (idSeq != null) {
				this.leyFedSeleccionado.setIdArchivoProyecto((short) (idSeq));
			} else {
				this.leyFedSeleccionado.setIdArchivoProyecto((short) 1);
			}

			leyFedEstDAO.actualizarLeyFederalProy(leyFedSeleccionado);
			leyesFederal = leyFedEstDAO.getLeyesFed(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
			msj = "";
			RequestContext requestContext = RequestContext.getCurrentInstance();
			requestContext.execute("modalAlertaFederal.show(); modalSubirArchivoFederal.hide();");
			
			}else {
				msj = "Se requiere cargar un archivo";
				
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		
	}
	/**
	 * Permite guardar un nuevo archivo estatal.
	 */
	public void guardarArchivoEstatal() {
		try {
	    	short id;
			id = miaDao.getMaxArchivoEtapaProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), 
					archivoTemporalEstatal.getCapituloId(), archivoTemporalEstatal.getSubCapituloId(), archivoTemporalEstatal.getSeccionId(), archivoTemporalEstatal.getApartadoId());

			archivoTemporalEstatal.setId(id);
			this.leyEstSeleccionado.setIdArchivoProyecto(archivoTemporalEstatal.getSeqId());
			
			bandera = cargaBean.GuardaAnexos(archivoTemporalEstatal);
			if (bandera == true) {
			Short idSeq = archivosAnexosDAO.consultarMaxIdSeq();
			if (idSeq != null) {
				this.leyEstSeleccionado.setIdArchivoProyecto((short) (idSeq));
			} else {
				this.leyEstSeleccionado.setIdArchivoProyecto((short) 1);
			}

			leyFedEstDAO.actualizarLeyFederalProy(leyEstSeleccionado);
			leyesEstatal = leyFedEstDAO.getLeyesEst(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
			msj=" ";
			RequestContext requestContext = RequestContext.getCurrentInstance();
			requestContext.execute("modalAlertaEstatal.show(); modalSubirArchivoEstatal.hide();");
			
			}else {
				msj = "Se requiere cargar un archivo";
				
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}    

	/**
	 * Permite limpiar los campos de las ventanas modales.
	 */
	public void limpiarLeyFederal() {
		this.leyFedEstProyecto = new LeyFedEstProyecto();
		CatLey catLey = new CatLey();
		this.leyFedEstProyecto.setLeyId(catLey);
		RequestContext context = RequestContext.getCurrentInstance();
		context.reset(":formEditarLeyFed:opEditarLeyFed");
		context.reset(":formEditarLeyEst:opEditarLeyEst");
	}
	
	/**
	 * Permite limpiar los campos de las ventanas modales.
	 */
	public void limpiarReglamento() {
		RequestContext context = RequestContext.getCurrentInstance();
		context.reset(":fEditar:pnlEditar");
	}

	public void limpiarOtra() {

	}

	public void respuestaPDUSi(){
		this.respuestaIncidePDU = true;
		this.respuestaPregunta = "S";
	}

	public void respuestaPDUNo(){
		this.respuestaIncidePDU = false;
		this.respuestaPregunta = "N";
	}

	public void respuestaPOETSi(){
		this.respuestaPOET = true;
	}

	public void respuestaPOETNo(){
		this.respuestaPOET = false;
	}

	/**
	 * @return the proyecto
	 */
	public Proyecto getProyecto() {
		return proyecto;
	}

	/**
	 * @param proyecto the proyecto to set
	 */
	public void setProyecto(Proyecto proyecto) {
		this.proyecto = proyecto;
	}

	/**
	 * @return the sector
	 */
	public String getSector() {
		return sector;
	}

	/**
	 * @param sector the sector to set
	 */
	public void setSector(String sector) {
		this.sector = sector;
	}

	/**
	 * @return the leyesFederal
	 */
	public List<LeyFedEstProyecto> getLeyesFederal() {
		return leyesFederal;
	}

	/**
	 * @param leyesFederal the leyesFederal to set
	 */
	public void setLeyesFederal(List<LeyFedEstProyecto> leyesFederal) {
		this.leyesFederal = leyesFederal;
	}

	/**
	 * @return the leyesEstatal
	 */
	public List<LeyFedEstProyecto> getLeyesEstatal() {
		return leyesEstatal;
	}

	/**
	 * @param leyesEstatal the leyesEstatal to set
	 */
	public void setLeyesEstatal(List<LeyFedEstProyecto> leyesEstatal) {
		this.leyesEstatal = leyesEstatal;
	}

	/**
	 * @return the reglamentos
	 
	public List<ReglamentoProyecto> getReglamentos() {
		return reglamentos;
	}
	 */
	/**
	 * @param reglamentos the reglamentos to set
	 
	public void setReglamentos(List<ReglamentoProyecto> reglamentos) {
		this.reglamentos = reglamentos;
	}
	*/

	/**
	 * @return the convenios
	 */
	public List<ConvenioProy> getConvenios() {
		return convenios;
	}

	/**
	 * @param convenios the convenios to set
	 */
	public void setConvenios(List<ConvenioProy> convenios) {
		this.convenios = convenios;
	}

	/**
	 * @return the disposiciones
	 */
	public List<DisposicionProyecto> getDisposiciones() {
		return disposiciones;
	}

	/**
	 * @param disposiciones the disposiciones to set
	 */
	public void setDisposiciones(List<DisposicionProyecto> disposiciones) {
		this.disposiciones = disposiciones;
	}

	/**
	 * @return the poet
	 */
	public List<POETu> getPoet() {
		return poet;
	}

	/**
	 * @param poet the poet to set
	 */
	public void setPoet(List<POETu> poet) {
		this.poet = poet;
	}

	/**
	 * @return the vinDec
	 */
	public List<VDecretou> getVinDec() {
		return vinDec;
	}

	/**
	 * @param vinDec the vinDec to set
	 */
	public void setVinDec(List<VDecretou> vinDec) {
		this.vinDec = vinDec;
	}

	/**
	 * @return the vinPro
	 */
	public List<VProgramau> getVinPro() {
		return vinPro;
	}

	/**
	 * @param vinPro the vinPro to set
	 */
	public void setVinPro(List<VProgramau> vinPro) {
		this.vinPro = vinPro;
	}

	/**
	 * @return the pdu1
	 */
	public List<PDUu> getPdu1() {
		return pdu1;
	}

	/**
	 * @param pdu1 the pdu1 to set
	 */
	public void setPdu1(List<PDUu> pdu1) {
		this.pdu1 = pdu1;
	}

	/**
	 * @return the pdu2
	 */
	public List<PDUu> getPdu2() {
		return pdu2;
	}

	/**
	 * @param pdu2 the pdu2 to set
	 */
	public void setPdu2(List<PDUu> pdu2) {
		this.pdu2 = pdu2;
	}

	/**
	 * @return the anp
	 */
	public List<AnpProyecto> getAnp() {
		return anp;
	}

	/**
	 * @param anp the anp to set
	 */
	public void setAnp(List<AnpProyecto> anp) {
		this.anp = anp;
	}

	/**
	 * @return the anpArticulo
	 */
	public List<ArticuloReglaAnp> getAnpArticulo() {
		return anpArticulo;
	}

	/**
	 * @param anpArticulo the anpArticulo to set
	 */
	public void setAnpArticulo(List<ArticuloReglaAnp> anpArticulo) {
		this.anpArticulo = anpArticulo;
	}

	/**
	 * @return the anpRegla
	 */
	public List<ArticuloReglaAnp> getAnpRegla() {
		return anpRegla;
	}

	/**
	 * @param anpRegla the anpRegla to set
	 */
	public void setAnpRegla(List<ArticuloReglaAnp> anpRegla) {
		this.anpRegla = anpRegla;
	}

	/**
	 * @return the anpu
	 */
	public List<ANPu> getAnpu() {
		return anpu;
	}

	/**
	 * @param anpu the anpu to set
	 */
	public void setAnpu(List<ANPu> anpu) {
		this.anpu = anpu;
	}

	/**
	 * @return the poetProyecto
	 */
	public List<PoetmProyecto> getPoetProyecto() {
		return poetProyecto;
	}

	/**
	 * @param poetProyecto the poetProyecto to set
	 */
	public void setPoetProyecto(List<PoetmProyecto> poetProyecto) {
		this.poetProyecto = poetProyecto;
	}

	/**
	 * @return the pduProyecto
	 */
	public List<PdumProyecto> getPduProyecto() {
		return pduProyecto;
	}

	/**
	 * @param pduProyecto the pduProyecto to set
	 */
	public void setPduProyecto(List<PdumProyecto> pduProyecto) {
		this.pduProyecto = pduProyecto;
	}

	/**
	 * @return the claveTramite
	 */
	public Integer getClaveTramite() {
		return claveTramite;
	}

	/**
	 * @param claveTramite the claveTramite to set
	 */
	public void setClaveTramite(Integer claveTramite) {
		this.claveTramite = claveTramite;
	}

	public boolean isCapDisposiciones() {
		return capDisposiciones;
	}

	public void setCapDisposiciones(boolean capDisposiciones) {
		this.capDisposiciones = capDisposiciones;
	}

	/**
	 * @return the seccion
	 */
	public String getSeccion() {
		return seccion;
	}

	/**
	 * @param seccion the seccion to set
	 */
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	/**
	 * @return the respuestaIncidePDU
	 */
	public boolean isRespuestaIncidePDU() {
		return respuestaIncidePDU;
	}

	/**
	 * @param respuestaIncidePDU the respuestaIncidePDU to set
	 */
	public void setRespuestaIncidePDU(boolean respuestaIncidePDU) {
		this.respuestaIncidePDU = respuestaIncidePDU;
	}

	/**
	 * @return the leyesFederalesSeleccionadas
	 */
	public List<LeyFedEstProyecto> getLeyesFederalesSeleccionadas() {
		return leyesFederalesSeleccionadas;
	}

	/**
	 * @param leyesFederalesSeleccionadas the leyesFederalesSeleccionadas to set
	 */
	public void setLeyesFederalesSeleccionadas(List<LeyFedEstProyecto> leyesFederalesSeleccionadas) {
		this.leyesFederalesSeleccionadas = leyesFederalesSeleccionadas;
	}

	/**
	 * @return the vinculacionLey
	 */
	public String getVinculacionLey() {
		return vinculacionLey;
	}

	/**
	 * @param vinculacionLey the vinculacionLey to set
	 */
	public void setVinculacionLey(String vinculacionLey) {
		this.vinculacionLey = vinculacionLey;
	}

	/**
	 * @return the idLeySeqSeleccionada
	 */
	public short getIdLeySeqSeleccionada() {
		return idLeySeqSeleccionada;
	}

	/**
	 * @param idLeySeqSeleccionada the idLeySeqSeleccionada to set
	 */
	public void setIdLeySeqSeleccionada(short idLeySeqSeleccionada) {
		this.idLeySeqSeleccionada = idLeySeqSeleccionada;
	}

	/**
	 * @return the mostrarEdicion
	 */
	public boolean isMostrarEdicion() {
		return mostrarEdicion;
	}

	/**
	 * @param mostrarEdicion the mostrarEdicion to set
	 */
	public void setMostrarEdicion(boolean mostrarEdicion) {
		this.mostrarEdicion = mostrarEdicion;
	}

	public List<PoetmProyecto> getSelectedPoet() {
		return selectedPoet;
	}

	public void setSelectedPoet(List<PoetmProyecto> selectedPoet) {
		this.selectedPoet = selectedPoet;
	}

	public PoetmProyecto getPoetm() {
		return poetm;
	}

	public void setPoetm(PoetmProyecto poetm) {
		this.poetm = poetm;
	}

	public PoetmProyecto getEditPoet() {
		return editPoet;
	}

	public void setEditPoet(PoetmProyecto editPoet) {
		this.editPoet = editPoet;
	}

	/**
	 * @return the reglamentoSeleccionado
	 */
	public List<ReglamentoProyecto> getReglamentoSeleccionado() {
		return reglamento_Seleccionado;
	}

	/**
	 * @param reglamentoSeleccionado the reglamentoSeleccionado to set
	 */
	public void setReglamentoSeleccionado(List<ReglamentoProyecto> reglamentoSeleccionado) {
		this.reglamento_Seleccionado = reglamentoSeleccionado;
	}

	/*
	 * Getter y Setter para el objeto de reglamento
	 */
	public ReglamentoProyecto getReglamento() {
		return reglamento;
	}

	public void setReglamento(ReglamentoProyecto reglamento) {
		this.reglamento = reglamento;
	}


	/**
	 * @return the mostrarEdicionEstatal
	 */
	public boolean isMostrarEdicionEstatal() {
		return mostrarEdicionEstatal;
	}

	/**
	 * @param mostrarEdicionEstatal the mostrarEdicionEstatal to set
	 */
	public void setMostrarEdicionEstatal(boolean mostrarEdicionEstatal) {
		this.mostrarEdicionEstatal = mostrarEdicionEstatal;
	}

	/**
	 * @return the catalogoLeyesFederales
	 */
	public List<CatLey> getCatalogoLeyesFederales() {
		return catalogoLeyesFederales;
	}

	/**
	 * @param catalogoLeyesFederales the catalogoLeyesFederales to set
	 */
	public void setCatalogoLeyesFederales(List<CatLey> catalogoLeyesFederales) {
		this.catalogoLeyesFederales = catalogoLeyesFederales;
	}

	/**
	 * @return the catLey
	 */
	public CatLey getCatLey() {
		return catLey;
	}

	/**
	 * @param catLey the catLey to set
	 */
	public void setCatLey(CatLey catLey) {
		this.catLey = catLey;
	}

	/**
	 * @return the leyFedEstProyecto
	 */
	public LeyFedEstProyecto getLeyFedEstProyecto() {
		return leyFedEstProyecto;
	}

	/**
	 * @param leyFedEstProyecto the leyFedEstProyecto to set
	 */
	public void setLeyFedEstProyecto(LeyFedEstProyecto leyFedEstProyecto) {
		this.leyFedEstProyecto = leyFedEstProyecto;
	}

	/**
	 * @return the mostrarCapturaLeyFederal
	 */
	public boolean isMostrarCapturaLeyFederal() {
		return mostrarCapturaLeyFederal;
	}

	/**
	 * @param mostrarCapturaLeyFederal the mostrarCapturaLeyFederal to set
	 */
	public void setMostrarCapturaLeyFederal(boolean mostrarCapturaLeyFederal) {
		this.mostrarCapturaLeyFederal = mostrarCapturaLeyFederal;
	}


	public boolean isRespuestaPOET() {
		return respuestaPOET;
	}

	public void setRespuestaPOET(boolean respuestaPOET) {
		this.respuestaPOET = respuestaPOET;
	}

	/**
	 * @return the leyesEstatalesSeleccionadas
	 */
	public List<LeyFedEstProyecto> getLeyesEstatalesSeleccionadas() {
		return leyesEstatalesSeleccionadas;
	}

	/**
	 * @param leyesEstatalesSeleccionadas the leyesEstatalesSeleccionadas to set
	 */
	public void setLeyesEstatalesSeleccionadas(List<LeyFedEstProyecto> leyesEstatalesSeleccionadas) {
		this.leyesEstatalesSeleccionadas = leyesEstatalesSeleccionadas;
	}

	/**
	 * @return the mostrarUltimaFecha
	 */
	public boolean isMostrarUltimaFecha() {
		return mostrarUltimaFecha;
	}

	/**
	 * @param mostrarUltimaFecha the mostrarUltimaFecha to set
	 */
	public void setMostrarUltimaFecha(boolean mostrarUltimaFecha) {
		this.mostrarUltimaFecha = mostrarUltimaFecha;
	}

	/**
	 * @return the fechaUltimaActualizacion
	 */
	public Date getFechaUltimaActualizacion() {
		return fechaUltimaActualizacion;
	}

	/**
	 * @param fechaUltimaActualizacion the fechaUltimaActualizacion to set
	 */
	public void setFechaUltimaActualizacion(Date fechaUltimaActualizacion) {
		this.fechaUltimaActualizacion = fechaUltimaActualizacion;
	}

	/**
	 * @return the leyesFedEstSeleccionadas
	 */
	public List<LeyFedEstProyecto> getLeyesFedEstSeleccionadas() {
		return leyesFedEstSeleccionadas;
	}

	/**
	 * @param leyesFedEstSeleccionadas the leyesFedEstSeleccionadas to set
	 */
	public void setLeyesFedEstSeleccionadas(List<LeyFedEstProyecto> leyesFedEstSeleccionadas) {
		this.leyesFedEstSeleccionadas = leyesFedEstSeleccionadas;
	}		

	public List<ConvenioProy> getConveniosElegidos() {
		return conveniosElegidos;
	}

	public void setConveniosElegidos(List<ConvenioProy> conveniosElegidos) {
		this.conveniosElegidos = conveniosElegidos;
	}

	public ConvenioProy getAgrega() {
		return agrega;
	}

	public void setAgrega(ConvenioProy agrega) {
		this.agrega = agrega;
	}

	public ConvenioProy getEdita() {
		return edita;
	}

	public void setEdita(ConvenioProy edita) {
		this.edita = edita;
	}

	
	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla POET
	 */
	public long getRowIndexPOETEditar() {
		return rowIndexPOETEditar;
	}


	/**
	 * Asigna valor a variable que guarda Numero secuencial de registro a editar en tabla POET
	 * @param rowIndexPOETEditar
	 */
	public void setRowIndexPOETEditar(long rowIndexPOETEditar) {
		this.rowIndexPOETEditar = rowIndexPOETEditar;
	}


	//"$"#"##########################################"$%"#$%"#$################################################################################//
	//"$"#"##########################################"$%"#$%"#$################################################################################//
	//"$"#"##########################################APARTADO PARA LAS ACCIONES QUE############################################################//
	//"$"#"##########################################SE REALIZARAN PARA REGLAMENTOS############################################################//
	//"$"#"##########################################"$%"#$%"#$################################################################################//

	//Consulta a base de datos y llena la lista para mostrar en tabla

	public void consultarReglamento( )
	{      
		reglamentos= miaDao.getReglamentos(proyecto.getProyectoPK().getFolioProyecto(),proyecto.getProyectoPK().getSerialProyecto());

		System.out.println("Reglamentos");
		for(ReglamentoProyecto r: reglamentos)
		{
			//		     	  System.out.println("ID: "+r.getReglamentoProyectoPK().getReglamentoProyId()+"\nDescripcion: "+ r.getReglamentoId().getReglamentoDescripcion()+ r.getReglamentoDescripcion()+"\n Fecha: "+r.getReglamentoArticulo()+"\nVinculacion: "+r.getReglamentoViculacion());
		}    	     
	}

	//===============================================METODO PARA ELIMINAR=======================================================================================
	public void eliminarReglamento(ActionEvent e) {
		//	    	ReglamentoProyecto rp = new ReglamentoProyecto(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt);
		Short idt = (Short) e.getComponent().getAttributes().get("idt");
		try {
			miaDao.eliminarReglamento(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), idt);
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		//reglamentos = miaDao.getReglamentos(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
	}

	public void eliminarReglamentos() {		 

		try {
			for (ReglamentoProyecto p : reglamentosS) {
				//Eliminacion de Archivos en FileSystem
    			if (p.getIdArchivoProyecto() != null) {
    				ArchivoBean.eliminarArchivoProyecto(p.getIdArchivoProyecto());
    			}
				reglamentoDAO.eliminarReglamento(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(), (short) p.getReglamentoProyectoPK().getReglamentoProyId());
				reglamentos= reglamentoDAO.consultarReglamentos(proyecto.getProyectoPK().getFolioProyecto(),proyecto.getProyectoPK().getSerialProyecto());
				reglamentosS = new ArrayList<>();
			}			
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	public void tieneReglamentosSeleccionados(){
		//System.out.println("..............................Consultando si existe servicios seleccionados para borrar");
		if(!reglamentosS.isEmpty()){
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("eliminarReglamento.show()");
		}
	}




	//=================================================METODO PARA AGREGAR=======================================================================================
	public void agregaReglamento()
	{
		System.out.println("Reglamento Agrega");		 
		if(reglam.getReglamentoId().getReglamentoId() !=9999)
		{
			try {

				Short id = miaDao.getMaxReglamento(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
				if (id != null) 
				{
					id = (short) (id + 1);
				} 
				else 
				{
					id = 1;
				}

				ReglamentoProyecto n = new ReglamentoProyecto();
				
				ReglamentoProyectoPK reglamentoProyectoPK = new ReglamentoProyectoPK();
				reglamentoProyectoPK.setFolioProyecto(proyecto.getProyectoPK().getFolioProyecto());
				reglamentoProyectoPK.setSerialProyecto(proyecto.getProyectoPK().getSerialProyecto());
				reglamentoProyectoPK.setReglamentoProyId(id);
				
				
				n.setReglamentoProyectoPK(reglamentoProyectoPK);
				
				n.setReglamentoId(reglam.getReglamentoId());
				for (CatReglamento catReglamento : catalogo) {
					System.out.println(catReglamento.getReglamentoId());
					if(catReglamento.getReglamentoId() == reglam.getReglamentoId().getReglamentoId()){
						n.setReglamentoDescripcion(catReglamento.getReglamentoDescripcion());
					}
				}
//				n.setReglamentoDescripcion(catalogo.get(reglam.getReglamentoId().getReglamentoId()).getReglamentoDescripcion());
				n.setReglamentoFechaPublicacion(sdf.format(reglam.getFechaPublicacion()));
				n.setReglamentoArticulo(reglam.getReglamentoArticulo());

				//				n.setReglamentoCapitulo(reglam.getReglamentoCapitulo());
				//				n.setReglamentoFraccion(reglam.getReglamentoFraccion());

				n.setReglamentoViculacion(reglam.getReglamentoViculacion());

				System.out.println("Asigno Valores va a la base");
				reglamentoDAO.guardarReglamento(n);

				RequestContext context = RequestContext.getCurrentInstance();
				RequestContext.getCurrentInstance().reset(":fAgregar:pnlAgregar");
				context.execute("modalAgregar.hide();guardarAlert.show()");
				System.out.println("Salio de la base ya  hizo el guardado");
			
				reglamentos= reglamentoDAO.consultarReglamentos(proyecto.getProyectoPK().getFolioProyecto(),proyecto.getProyectoPK().getSerialProyecto());
				
				RequestContext.getCurrentInstance().reset(":fAgregar:pnlAgregar");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("HO HO HO Algo salio mal"+ e.getMessage());
			}
		} else if (reglam.getReglamentoId().getReglamentoId() == 9999) {

			try {

				Short id = miaDao.getMaxReglamento(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
				if (id != null) 
				{
					id = (short) (id + 1);
				} 
				else 
				{
					id = 1;
				}


//				id=miaDao.getMaxReglamento(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
				System.out.println("Entra a crear Objet");
				ReglamentoProyecto n = new ReglamentoProyecto();
				
				ReglamentoProyectoPK reglamentoProyectoPK = new ReglamentoProyectoPK();
				reglamentoProyectoPK.setFolioProyecto(proyecto.getProyectoPK().getFolioProyecto());
				reglamentoProyectoPK.setSerialProyecto(proyecto.getProyectoPK().getSerialProyecto());
				reglamentoProyectoPK.setReglamentoProyId(id);
				
				
				n.setReglamentoProyectoPK(reglamentoProyectoPK);
				
				System.out.println("Va a asignar los valores");
				for (CatReglamento catReglamentos : catalogo) {
					if(catReglamentos.getReglamentoId() == 9999){
						n.setReglamentoDescripcion(catReglamentos.getReglamentoDescripcion());
					}
				}
				n.setReglamentoFechaPublicacion(sdf.format(reglam.getFechaPublicacion()));
				n.setReglamentoArticulo(reglam.getReglamentoArticulo());
				n.setReglamentoViculacion(reglam.getReglamentoViculacion());
			
				CatReglamento catReglamento = new CatReglamento();
				catReglamento.setReglamentoId(reglam.getReglamentoId().getReglamentoId());
			
				n.setReglamentoId(catReglamento);
				System.out.println("Asigno Valores va a la base");			
				reglamentoDAO.guardarReglamento(n);
				RequestContext context = RequestContext.getCurrentInstance();
				RequestContext.getCurrentInstance().reset(":fAgregar:pnlAgregar");
				context.execute("modalAgregar.hide();guardarAlert.show()");
				System.out.println("Salio de la base ya  hizo el guardado");
				reglamentos= reglamentoDAO.consultarReglamentos(proyecto.getProyectoPK().getFolioProyecto(),proyecto.getProyectoPK().getSerialProyecto());
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("HO HO HO Algo salio mal"+ e.getMessage());
			}
		}
		
		
		
	}

	public void guardarR()
	{
		agregaReglamento();

	}


	//===================================================METODO PARA EDITAR=======================================================================================

	public void consultarEditar(ActionEvent e) 
	{
		limpiarReglamento();
		reglamed = new ReglamentoProyecto();
		System.out.println("Consultando");
		Short idt = (Short) e.getComponent().getAttributes().get("idt");

		System.out.println("Actualizando criterio: "+idt);
		reglamed = new ReglamentoProyecto();

		reglamed = reglamentoDAO.consultarReglamentoPorID(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto(),idt);
		
		Date fechaUltimaActualizacion;

		try {
			// Numero secuencial a mostrar en "Editar registro #"
			rowIndexReglamEditar = (Long) e.getComponent().getAttributes().get("atrowindexReglam");
			
			DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH); 
			fechaUltimaActualizacion = sdf.parse(reglamed.getReglamentoFechaPublicacion());
			reglamed.setFechaPublicacion(fechaUltimaActualizacion);

		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		System.out.println("ID: "+reglamed.getReglamentoId().getReglamentoId());
		System.out.println("Descripcion: "+reglamed.getReglamentoDescripcion());
		System.out.println(reglamed.getReglamentoFechaPublicacion());
	}

	public void editarReglamento() {
		ReglamentoProyecto cp = new ReglamentoProyecto();

		cp = reglamed;
		cp.setReglamentoFechaPublicacion(sdf.format(reglamed.getFechaPublicacion()));
		try {
			miaDao.merge(cp);
			
			reglamentos = reglamentoDAO.consultarReglamentos(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		} catch (Exception e) {
			e.printStackTrace();
		}
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("modalEditar.hide();modalAlertEditar.show()");
	}





	//============================================================================================================================================================



	public void listaReglamentos()
	{
		catalogo = miaDao.getCatReglamento();

		if(catalogo.size()!=0)
		{
			System.out.println("La lista de catalogos tamaño: "+catalogo.size());

			for(CatReglamento c: catalogo)
			{
				System.out.println("ID DEL REGLAMENTO: "+ c.getReglamentoId() + "   Descripción: " +c.getReglamentoDescripcion());
			}

		}		
		else
		{
			System.out.println("La lista de catalogos viene vacia");
		}
	}
	//===========================================================================================================================================================







	//Lista para obtener los Reglamentos desde base
	public List<ReglamentoProyecto> getreglamentos() {
		return reglamentos;
	}

	public void setreglamentos(List<ReglamentoProyecto> reglamentos) {

		this.reglamentos = reglamentos;
	}

	//lista para los reglamentos seleccionados por usuer
	public List<ReglamentoProyecto> getReglamentosS() {
		return reglamentosS;
	}

	public void setReglamentosS(List<ReglamentoProyecto> reglamentosS) {
		this.reglamentosS = reglamentosS;
	}

	//Objeto que nos servira para las altas y las ediciones
	public ReglamentoProyecto getReglam() {

		return reglam;
	}

	public void setReglam(ReglamentoProyecto reglam) {

		this.reglam = reglam;
	}

	//Catalogo de Reglamentos
	public List<CatReglamento> getCatReg() {
		CatReglamento c = new CatReglamento();
		c.getReglamentoDescripcion();
		return catReg;
	}

	public void setCatReg(List<CatReglamento> catReg) {
		this.catReg = catReg;
	}

	//Bandera para captura de otra regla
	public boolean isMostrarCapturaRegla() {
		return mostrarCapturaRegla;
	}



	public void setMostrarCapturaRegla(boolean mostrarCapturaRegla) {
		this.mostrarCapturaRegla = mostrarCapturaRegla;
	}

	//Vinculacion
	public String getVinculacionReg() {
		return vinculacionReg;
	}

	public void setVinculacionReg(String vinculacionReg) {
		this.vinculacionReg = vinculacionReg;
	}

	public ArchivosProyecto getArchivoTemporal() {
		return archivoTemporal;
	}

	public void setArchivoTemporal(ArchivosProyecto archivoTemporal) {
		this.archivoTemporal = archivoTemporal;
	}

	public CargaBean getCargaBean() {
		return cargaBean;
	}

	public void setCargaBean(CargaBean cargaBean) {
		this.cargaBean = cargaBean;
	}	


	//============================================================================================================================================================
	public ReglamentoProyecto getReglamed() {
		return reglamed;
	}

	public void setReglamed(ReglamentoProyecto reglamed) {

		this.reglamed = reglamed;
	}

	public List<CatReglamento> getCatalogo() {
		return catalogo;
	}

	public void setCatalogo(List<CatReglamento> catalogo) {
		this.catalogo = catalogo;	
	}
	//============================================================================================================================================================


	public short getValor() {
		return valor;
	}

	public void setValor(short valor) {
		this.valor = valor;
	}


	/**
	 * @return the archivoTemporalFederal
	 */
	public ArchivosProyecto getArchivoTemporalFederal() {
		return archivoTemporalFederal;
	}


	/**
	 * @param archivoTemporalFederal the archivoTemporalFederal to set
	 */
	public void setArchivoTemporalFederal(ArchivosProyecto archivoTemporalFederal) {
		this.archivoTemporalFederal = archivoTemporalFederal;
	}


	/**
	 * @return the archivoTemporalEstatal
	 */
	public ArchivosProyecto getArchivoTemporalEstatal() {
		return archivoTemporalEstatal;
	}


	/**
	 * @param archivoTemporalEstatal the archivoTemporalEstatal to set
	 */
	public void setArchivoTemporalEstatal(ArchivosProyecto archivoTemporalEstatal) {
		this.archivoTemporalEstatal = archivoTemporalEstatal;
	}


	/**
	 * @return the leyFedSeleccionado
	 */
	public LeyFedEstProyecto getLeyFedSeleccionado() {
		return leyFedSeleccionado;
	}


	/**
	 * @param leyFedSeleccionado the leyFedSeleccionado to set
	 */
	public void setLeyFedSeleccionado(LeyFedEstProyecto leyFedSeleccionado) {
		this.leyFedSeleccionado = leyFedSeleccionado;
	}


	/**
	 * @return the leyEstSeleccionado
	 */
	public LeyFedEstProyecto getLeyEstSeleccionado() {
		return leyEstSeleccionado;
	}


	/**
	 * @param leyEstSeleccionado the leyEstSeleccionado to set
	 */
	public void setLeyEstSeleccionado(LeyFedEstProyecto leyEstSeleccionado) {
		this.leyEstSeleccionado = leyEstSeleccionado;
	}


	public String getAplica() {
		return aplica;
	}


	public void setAplica(String aplica) {
		this.aplica = aplica;
	}


	public boolean isCapServicios() {
		return capServicios;
	}


	public void setCapServicios(boolean capServicios) {
		this.capServicios = capServicios;
	}


	public String getVinculacionPoet() {
		return vinculacionPoet;
	}


	public void setVinculacionPoet(String vinculacionPoet) {
		this.vinculacionPoet = vinculacionPoet;
	}


	public ArchivosProyecto getArchivoTemporalReglamento() {
		return archivoTemporalReglamento;
	}


	public void setArchivoTemporalReglamento(
			ArchivosProyecto archivoTemporalReglamento) {
		this.archivoTemporalReglamento = archivoTemporalReglamento;
	}


	public String getMsj() {
		return msj;
	}


	public void setMsj(String msj) {
		this.msj = msj;
	}


	public boolean isBandera() {
		return bandera;
	}


	public void setBandera(boolean bandera) {
		this.bandera = bandera;
	}

	
	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla Leyes federales
	 */
	public int getRowIndexLeyFedEditar() {
		return rowIndexLeyFedEditar;
	}

	
	/**
	 * Asigna valor a variable que guarda Numero secuencial de registro a editar en Leyes federales
	 * @param rowIndexLeyFedEditar
	 */
	public void setRowIndexLeyFedEditar(int rowIndexLeyFedEditar) {
		this.rowIndexLeyFedEditar = rowIndexLeyFedEditar;
	}

	
	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla Leyes estatales
	 */
	public int getRowIndexLeyEstEditar() {
		return rowIndexLeyEstEditar;
	}

	
	/**
	 * Asigna valor a variable que guarda Numero secuencial de registro a editar en Leyes estatales
	 * @param rowIndexLeyEstEditar
	 */
	public void setRowIndexLeyEstEditar(int rowIndexLeyEstEditar) {
		this.rowIndexLeyEstEditar = rowIndexLeyEstEditar;
	}
	
	
	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla Reglamentos
	 */
	public long getRowIndexReglamEditar() {
		return rowIndexReglamEditar;
	}

	
	/**
	 * Asigna valor a variable que guarda Numero secuencial de Reglamento a editar
	 * @param rowIndexReglamEditar
	 */
	public void setRowIndexReglamEditar(long rowIndexReglamEditar) {
		this.rowIndexReglamEditar = rowIndexReglamEditar;
	}
	
	
	/**
	 * @return Numero secuencial (en la Vista) para un registro de tabla "Acuerdos, convenios y tratados internacionales en materia ambiental"
	 */
	public int getRowIndexAcuerdoEditar() {
		return rowIndexAcuerdoEditar;
	}


	/**
	 * Asigna valor a variable que guarda Numero secuencial de registro a editar en tabla "Acuerdos, convenios y tratados internacionales en materia ambiental"
	 * @param rowIndexAcuerdoEditar
	 */
	public void setRowIndexAcuerdoEditar(int rowIndexAcuerdoEditar) {
		this.rowIndexAcuerdoEditar = rowIndexAcuerdoEditar;
	}
	
}
