/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import mx.gob.semarnat.mia.dao.ProyectoDAO;
import mx.gob.semarnat.mia.model.Proyecto;

/**
 *
 * @author Extend
 */
@ManagedBean(name="glosario")
@ViewScoped
public class Capitulo8ViewGlosario extends Cap implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9090497706130274752L;

	private ProyectoDAO proyectoDAO;
	
	String glosarioProyecto;
	
	
	public Capitulo8ViewGlosario(){
		glosarioProyecto=getProyecto().getGlosario();
	}
	
	/**
	 * @return the glosarioProyecto
	 */
	public String getGlosarioProyecto() {
		return glosarioProyecto;
	}

	/**
	 * @param glosarioProyecto the glosarioProyecto to set
	 */
	public void setGlosarioProyecto(String glosarioProyecto) {
		this.glosarioProyecto = glosarioProyecto;
	}

	/*
	 * Metodo para guardar el texto del glosario
	 * */
    public void saveGlosario(){
    	proyectoDAO= new ProyectoDAO ();
    	Proyecto proyectoG=proyectoDAO.find(getProyecto().getProyectoPK());
    	proyectoG.setGlosario(glosarioProyecto);
    	proyectoDAO.edit(proyectoG);
    	
    Capitulo8View obj = new Capitulo8View();
    obj.guardar();
    	
    } 
    
    
    
    
    
    
 }
