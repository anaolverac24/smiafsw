/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import org.primefaces.context.RequestContext;

import mx.gob.semarnat.mia.dao.HidrologiaProyectoDAO;
import mx.gob.semarnat.mia.dao.SigeiaDao;
import mx.gob.semarnat.mia.model.HidrologiaProyecto;
import mx.gob.semarnat.mia.model.HidrologiaProyectoId;
import mx.gob.semarnat.mia.model.Proyecto;
import mx.gob.semarnat.mia.model.catalogos.SectorProyecto;
import mx.gob.semarnat.mia.model.catalogos.SubsectorProyecto;
import mx.gob.semarnat.mia.view.capitulo1.PaginationHelper;
import mx.gob.semarnat.mia.view.to.HidrologiaProyectoTO;

/**
 *
 * @author Ramon Hidalgo
 */
@ManagedBean(name = "hidrologia")
@ViewScoped
public class Capitulo4View2Hidrologia extends AbstractTable<HidrologiaProyectoTO> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3197009275689756012L;

	// private MiaDao miaDao = new MiaDao();
	private SigeiaDao siDao;
	private Proyecto proyecto = new Proyecto();

	private HidrologiaProyectoDAO hidrologiaProyectoDAO;

	private HidrologiaProyectoTO to = new HidrologiaProyectoTO();
	private HidrologiaProyecto hp = new HidrologiaProyecto();
	private List<HidrologiaProyecto> lista = new ArrayList<>(); 

	private List<Object[]> microCuencaTmp = new ArrayList<Object[]>();

	private Integer claveTramite = 0;

	public Capitulo4View2Hidrologia() {
		hidrologiaProyectoDAO = new HidrologiaProyectoDAO();
		getCapitulo4View2Hidrologia();
		loadListaPaginas();
	}

	public String getCapitulo4View2Hidrologia() {
		// Recupera proyecto de sesion
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().get("userFolioProy");
		String folioProyecto = (String) context.getExternalContext().getSessionMap().get("folioProyecto");
		Short serialProyecto = (Short) context.getExternalContext().getSessionMap().get("serialProyecto");
		claveTramite = (Integer) context.getExternalContext().getSessionMap().get("claveTramite");
		// Cargar Proyecto
		try {
			this.proyecto = miaDao.proyecto(folioProyecto, serialProyecto);
		} catch (Exception e) {
			System.out.println("No encontrado");
		}

		if (proyecto == null) {
			this.proyecto = new Proyecto(folioProyecto, serialProyecto);
		}

		

		if (proyecto.getNsub() != null) {
			SubsectorProyecto su = miaDao.getSubSector(proyecto.getNsub());
			SectorProyecto sp = miaDao.getSectorProyecto(su.getNsec());

		}

		siDao = new SigeiaDao();
		// microCuenca = siDao.getMicrocuenca(folioProyecto, serialProyecto,
		// proyecto.getClaveProyecto());
		// microCuenca2 = siDao.getMicrocuenca2(folioProyecto, serialProyecto,
		// proyecto.getClaveProyecto());
		List<HidrologiaProyecto> hidrologia = new ArrayList<HidrologiaProyecto>();
		listTable = new ArrayList<HidrologiaProyectoTO>();
		lista = new ArrayList<HidrologiaProyecto>();
		lista = hidrologiaProyectoDAO.getHidrologia(folioProyecto, serialProyecto);
		hidrologia = hidrologiaProyectoDAO.getHidrologia(folioProyecto, serialProyecto);
		// Si no existen registros en hidrologia se insertan desde Microcuenca
		if (hidrologia == null || hidrologia.isEmpty()) {
			HidrologiaProyecto hp;
			BigDecimal hidroId = BigDecimal.ZERO;
			for (Object[] obj : microCuencaTmp) {
				microCuencaTmp = siDao.getMicrocuenca2(folioProyecto, serialProyecto, proyecto.getClaveProyecto());
				hidroId = hidroId.add(BigDecimal.ONE);
				hp = new HidrologiaProyecto(
						new HidrologiaProyectoId(hidroId, folioProyecto, new BigDecimal(serialProyecto)));
				hp.setHidroCuenca(obj[0].toString());
				hp.setHidroSubcuenca(obj[1].toString());
				hp.setHidroMicrocuenca(obj[2].toString());
				siDao.agrega(hp);
				listTable.add(transformaEntityTO(hp));
			}

		} else {
			for (HidrologiaProyecto hidrologiaProyecto : hidrologia) {
				listTable.add(transformaEntityTO(hidrologiaProyecto));
			}
		}
		to = new HidrologiaProyectoTO();
		loadListaPaginas();
		return null;
	}

	public void edit(){
    
    merge();
    List<HidrologiaProyecto> hidrologia = new ArrayList<HidrologiaProyecto>();
    listTable = new ArrayList<HidrologiaProyectoTO>();
    
    hidrologia = hidrologiaProyectoDAO.getHidrologia(to.getFolioProyecto(), to.getSerialProyecto().shortValue());
    lista = hidrologiaProyectoDAO.getHidrologia(to.getFolioProyecto(), to.getSerialProyecto().shortValue());
    listTable=transformListEstityToTO(hidrologia);
    RequestContext.getCurrentInstance().update(":formHidroSuperficial:dataTableHidro");
    }

	private void merge() {
		siDao = new SigeiaDao();
		HidrologiaProyectoDAO hpDAO = new HidrologiaProyectoDAO();
		HidrologiaProyecto hidrologiaProyecto = new HidrologiaProyecto(
				new HidrologiaProyectoId(to.getHidroId(), to.getFolioProyecto(), to.getSerialProyecto()));
		hidrologiaProyecto.setHidroCuenca(to.getHidroCuenca());
		hidrologiaProyecto.setHidroSubcuenca(to.getHidroSubcuenca());
		hidrologiaProyecto.setHidroMicrocuenca(to.getHidroMicrocuenca());

		hpDAO.edit(hidrologiaProyecto);

	}

	public void cargarEdicion(HidrologiaProyectoTO to) {
		this.to = new HidrologiaProyectoTO();
		this.to = to;
		to = new HidrologiaProyectoTO();
	}
	
	public void cargarEdicion(HidrologiaProyecto hp) {
		this.hp = new HidrologiaProyecto();
		this.hp = hp;
		hp = new HidrologiaProyecto();
	}
	
	public void editarHidrologia(){
		HidrologiaProyectoDAO hpDAO = new HidrologiaProyectoDAO();
		HidrologiaProyecto hidrologiaProyecto = new HidrologiaProyecto();
//		HidrologiaProyecto hidrologiaProyecto = new HidrologiaProyecto(
//				new HidrologiaProyectoId(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto()));
		hidrologiaProyecto.setId(hp.getId());
		hidrologiaProyecto.setHidroCuenca(hp.getHidroCuenca());
		hidrologiaProyecto.setHidroSubcuenca(hp.getHidroSubcuenca());
		hidrologiaProyecto.setHidroMicrocuenca(hp.getHidroMicrocuenca());

		try {
//			hpDAO.edit(hidrologiaProyecto);
			miaDao.merge(hidrologiaProyecto);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
	    lista = hidrologiaProyectoDAO.getHidrologia(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("myDialogEdit.hide();avisoHidroSuper.show()");
		context.update(":formHidroSuperficial:dataTableHidro");
	    
	}
	
	public void actualizarTabla(){
	    lista = hidrologiaProyectoDAO.getHidrologia(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		RequestContext context = RequestContext.getCurrentInstance();
		context.update(":formHidroSuperficial:dataTableHidro");
	}

	private HidrologiaProyectoTO transformaEntityTO(HidrologiaProyecto hidrologia) {
		HidrologiaProyectoTO to = new HidrologiaProyectoTO();
		to.setHidroId(hidrologia.getId().getHidroId());
		to.setFolioProyecto(hidrologia.getId().getFolioProyecto());
		to.setSerialProyecto(hidrologia.getId().getSerialProyecto());

		to.setHidroCuenca(hidrologia.getHidroCuenca());
		to.setHidroSubcuenca(hidrologia.getHidroSubcuenca());
		to.setHidroMicrocuenca(hidrologia.getHidroMicrocuenca());

		return to;
	}

	/**
	 * @return the to
	 */
	public HidrologiaProyectoTO getTo() {
		return to;
	}

	/**
	 * @param to
	 *            the to to set
	 */
	public void setTo(HidrologiaProyectoTO to) {
		this.to = to;
	}

	@Override
	public PaginationHelper getPagination() {		
		if (pagination == null) {

			pagination = new PaginationHelper(NUM_MAX_PAG) {
				@Override
				public int getItemsCount() {
					return hidrologiaProyectoDAO.countHidrologia(proyecto.getProyectoPK().getFolioProyecto(),
							proyecto.getProyectoPK().getSerialProyecto());
				}

				@SuppressWarnings({ "rawtypes", "unchecked" })
				@Override
				public DataModel createPageDataModel() {

					return new ListDataModel(transformListEstityToTO(hidrologiaProyectoDAO.findRangeHidrologia(
							new int[] { getPageFirstItem(), getPageFirstItem() + getPageSize() },
							proyecto.getProyectoPK().getFolioProyecto(),
							proyecto.getProyectoPK().getSerialProyecto())));
				}
			};
		}
		return pagination;
	}

	@Override
	void updateCurrentItem() {
		
		int count = hidrologiaProyectoDAO.countHidrologia(proyecto.getProyectoPK().getFolioProyecto(), proyecto.getProyectoPK().getSerialProyecto());
		if (selectedItemIndex >= count) {

			// selected index cannot be bigger than number of items:
			selectedItemIndex = count - UNO;

			// go to previous page if last page disappeared:
			if (pagination.getPageFirstItem() >= count) {

				pagination.previousPage();
			}
		}
		if (selectedItemIndex >= CERO) {
			listTable = transformListEstityToTO(hidrologiaProyectoDAO.findRangeHidrologia((
					new int[] { pagination.getPageFirstItem(),
							pagination.getPageFirstItem() + pagination.getPageSize() }),
					proyecto.getProyectoPK().getFolioProyecto(),proyecto.getProyectoPK().getSerialProyecto()));
		}
	}

	private List<HidrologiaProyectoTO> transformListEstityToTO(List<HidrologiaProyecto> listaHidrologia) {
		List<HidrologiaProyectoTO> listaTO = new ArrayList<HidrologiaProyectoTO>();

		for (HidrologiaProyecto hidrologiaP : listaHidrologia) {
			listaTO.add(transformaEntityTO(hidrologiaP));
		}
		return listaTO;
	}

	public List<HidrologiaProyecto> getLista() {
		return lista;
	}

	public void setLista(List<HidrologiaProyecto> lista) {
		this.lista = lista;
	}

	public HidrologiaProyecto getHp() {
		return hp;
	}

	public void setHp(HidrologiaProyecto hp) {
		this.hp = hp;
	}

}
