/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.semarnat.mia.validacion;

import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import mx.gob.semarnat.mia.dao.MiaDao;
import mx.gob.semarnat.mia.model.Proyecto;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Mauricio Solis
 */
public class Validacion {

    private final JSONParser parser = new JSONParser();
    private final Map<String, Object> config;
    private final MiaDao dao;

    public Validacion() throws IOException, ParseException {
        config = (Map<String, Object>) parser.parse(new InputStreamReader(Validacion.class.getResourceAsStream("/mx/gob/semarnat/mia/validacion/validaciones.json")), containerFactory);
        dao = new MiaDao();
    }

    /**
     * Validar seccion
     *
     * @param seccion
     * @param folio
     * @param serial
     */
    public String validar(String seccion, String folio, short serial) {
        String msg = "";
        Object o = config.get(seccion);

        if (o == null) {
            return "";
        }

        Map<String, Object> aValidar = (Map) o;
        for (String s : aValidar.keySet()) {
            Map<String, Object> c = (Map<String, Object>) aValidar.get(s);

            if (c.get("tipo").equals("campo_vacio")) {
                String campo = (String) c.get("campo");
                String val = dao.getCampoProyecto(folio, serial, campo);
                if (val == null || val.isEmpty()) {
                    msg = (String) c.get("msg");
                }
            }
            if (c.get("tipo").equals("lista_vacia")) {
                if (c.get("dep") != null) {
                    List<String> dep = (LinkedList<String>) c.get("dep");
                    for (String s2:dep){
                        System.out.println("s2 "+ s2);
                    }
                    String val = dao.getCampoProyecto(folio, serial, dep.get(0));

                    System.out.println("Val "+ val);
                    
                    if(val == null)
                    	return "";

                    if (!val.trim().equals(dep.get(1).trim())){
                        return "";
                    }
                    
                }

                String lista = (String) c.get("lista");
                String count = dao.getRegistrosLista(folio, serial, lista);
                if (count.equals("0")) {
                    System.out.println("lista vacia");
                    msg += (String) c.get("msg") + " ";
                } else {
                    System.out.println("lista con registros");

                }

            }
        }

        if (!msg.isEmpty()) {
            System.out.println("msg " + msg);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", msg));
        }

        return msg;
    }

    public static void main(String[] a) {
        try {
            Validacion v = new Validacion();
            v.validar("2.2", "5783", (short) 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final static ContainerFactory containerFactory = new ContainerFactory() {
        @Override
        public List creatArrayContainer() {
            return new LinkedList();
        }

        @Override
        public Map createObjectContainer() {
            return new LinkedHashMap();
        }

    };
}
