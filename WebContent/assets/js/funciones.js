function contar(form,name,result) 
{ //alert("form: " +form+"   name: "+name+"  result: "+result);
    
    var n,t;
    n=0;
    n = document.forms[form][name].value.length;  //alert(n);
    t = 5000;
    if (n > t) {
          alert('A revasado el limite de caracteres permitidos(5,000 caracteres)');
      document.forms[form][name].value = document.forms[form][name].value.substring(0, t);

    }
    else {
      document.forms[form][result].value = t-n;
    }
}

function addRow(tableID,seccion) 
{

	   var table = document.getElementById(tableID);
	   var rowCount = table.rows.length;
	   var row = table.insertRow(rowCount);
	   var cell1 = "";
	   var element1 = "";
	   var elementAux = "";
	   var suma=0;
	   
	   
	   cell1 = row.insertCell(0);
	   element1 = document.createElement("img");
	   element1.src = "../../../assets/images/gobmx/icons/panel-collapsed.png";	   
	   element1.id = rowCount;
	   element1.alt= "borrar";
           element1.title="borrar title";		   
	   //element1.className = "miCheck";  	   
	   crearEvento(element1, "click", function()
	   {	//alert("entro: "); 
			//alert('rown count: '+ rowCount);	   
			if (confirm("Esta usted seguro de borrar el registro?"))
			{   //borra fila en cuestion			
                            table.deleteRow(rowCount);
                            rowCount--;
                            if(seccion === 'superfProy')
                            {
                                var table2 = document.getElementById(tableID);
                                var rowCount2 = table.rows.length; 
                                var sum = 0;
                                var sum2 = 0;
                                for(var i=0; i<rowCount2; i++) 
                                {                                       
                                    var row2 = table2.rows[i];					
                                    //alert(row2.cells[2].childNodes[0]+'  i:'+i);					
                                    var txtTotPredObr = row2.cells[2].childNodes[0];
                                    var cmbPredObr = row2.cells[1].childNodes[0];
                                    if(null !== txtTotPredObr) 
                                    {
                                        if(cmbPredObr.id ==="cmbPO" && cmbPredObr.options[cmbPredObr.selectedIndex].text === 'Obra') 
                                        { 
                                            if(txtTotPredObr.id === 'supTot') 
                                            { sum = parseInt(sum) + parseInt(txtTotPredObr.value);}
                                        }
                                        if(cmbPredObr.id ==="cmbPO" && cmbPredObr.options[cmbPredObr.selectedIndex].text === 'Predio') 
                                        { 
                                            if(txtTotPredObr.id === 'supTot') 
                                            { sum2 = parseInt(sum2) + parseInt(txtTotPredObr.value);}
                                        }						 
                                    }
                                }			   
                                document.getElementById("totObra").value = sum;	
                                document.getElementById("totPredio").value = sum2;
                            //finaliza realiza suma de total de predio y obra
                            }
                            //finaliza realiza suma de total de predio y obra	
                            return true; 
			}
			else{ return false; }				
		});
	   cell1.appendChild(element1); 	   
	   if(seccion === 'etapas')
	   {
		   var cell2 = row.insertCell(1); 	
		   var element2 = "";
		   
		   element2 = document.createElement("input");	
		   element2.type = "file";
		   cell2.appendChild(element2);	 		   
	   }
	   if(seccion === 'sustancias')
	   {		   
		   var option = "";
		   var element2 = "";
		   
		   var cell2 = row.insertCell(1);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Tipo Sustancia";
		   element2.add(option);
		   cell2.appendChild(element2);	
		   
		   var cell3 = row.insertCell(2);
		   element2 = document.createElement("input");	
		   element2.type = "text";
		   element2.value = "0";
		   cell3.appendChild(element2);
		   
		   var cell4 = row.insertCell(3);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Unidad";
		   element2.add(option);
		   cell4.appendChild(element2);	
		   
		   var cell5 = row.insertCell(4);
		   element2 = document.createElement("input");	
		   element2.type = "file";
		   cell5.appendChild(element2);	   
		   
	   }
	   if(seccion === 'residuos')
	   {		   
		   var option = "";
		   var element2 = "";
                   
                                                      
		   var cell2 = row.insertCell(1);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "-Selección-";
                   element2.add(option);
                   option = document.createElement("option");
                   option.text = "Ambiental";
                   element2.add(option);
                   option = document.createElement("option");
                   option.text = "Socioeconomico";
                   element2.add(option);
                   option =document.createElement("option");
                   option.text = "Técnico";
                   element2.add(option);
                   cell2.appendChild(element2);
                   
                   option = document.createElement("option");
		   option.text = "Predio";
		   element2.add(option);
		   
   	       var cell3 = row.insertCell(2);
		   element2 = document.createElement("input");	
		   element2.name = "textArea";
		   option.className = "textArea";
		   element2.rows ="1"; 
		   cell3.appendChild(element2);
	   }
	   if(seccion === 'adjuntaArch')
	   {
		   var cell2 = row.insertCell(1); 	
		   var element2 = document.createElement("input");	
		   element2.type = "file";
		   cell2.appendChild(element2);
	   } 
	   if(seccion === 'impactoAmbiental')
	   {		   
		   var option = "";
		   var element2 = "";
		   
		   var cell2 = row.insertCell(1);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Tipo Impacto";
		   element2.add(option);
		   cell2.appendChild(element2);	
		   
   	       var cell3 = row.insertCell(2);
		   element2 = document.createElement("textarea");	
		   cell3.appendChild(element2);	
		   
		   var cell4 = row.insertCell(3);
		   element2 = document.createElement("textarea");	   
		   cell4.appendChild(element2);
		   
		   var cell5 = row.insertCell(4);	
		   element2 = document.createElement("textarea");	
		   cell5.appendChild(element2);			   
		   
	   }
	   if(seccion === 'planosAdc')
	   { 
                var element2 = "";	
                
                var cell2= row.insertCell(1);
                element2 = document.createElement("textarea");
                element2.name = "txtArea";
                element2.rows = "1";
                cell2.appendChild(element2);
                
                var cell3 = row.insertCell(2);
                element2 = document.createElement("textarea");	
                element2.name = "txtArea";
                element2.rows="1";		   
                cell3.appendChild(element2);
                
                
                var cell5 = row.insertCell(3); 			   	   
                element2 = document.createElement("input");	
                element2.type = "file";
                cell5.appendChild(element2);	 	   
		   
	   }
           if(seccion == 'Leyes')
           {
               
           }
           
           
           
	   if(seccion === 'superfProy')
	   {		   
		   var option = "";
		   var element2 = "";
		   
		   
		   var cell2 = row.insertCell(1);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "- Seleccione -";
		   element2.add(option);
		   option = document.createElement("option");
		   option.text = "Obra";
		   element2.add(option);		  
		   option = document.createElement("option");
		   option.text = "Predio";
		   element2.add(option);
                   option = document.createElement("option");
                   option.text = "Sistema Ambiental";
                   element2.add(option);
                   option = document.createElement("option");
                   option.text = "Área de Influencia";
                   element2.add(option);
                   element2.id="cmbPO";
                   
                   
                   crearEvento(element2, "change", function(){
			//inicia realiza suma de total de predio y obra	
                        var table2 = document.getElementById(tableID);
                        var rowCount2 = table.rows.length; 
                        var sum = 0;
                        var sum2 = 0;
                        for(var i=0; i<rowCount2; i++) 
                        {                                       
                            var row2 = table2.rows[i];					
                            //alert(row2.cells[2].childNodes[0]+'  i:'+i);					
                            var txtTotPredObr = row2.cells[2].childNodes[0];
                            var cmbPredObr = row2.cells[1].childNodes[0];
                            if(null !== txtTotPredObr) 
                            {
                                if(cmbPredObr.id ==="cmbPO" && cmbPredObr.options[cmbPredObr.selectedIndex].text === 'Obra') 
                                { 
                                    if(txtTotPredObr.id === 'supTot') 
                                    { sum = parseInt(sum) + parseInt(txtTotPredObr.value);}
                                }
                                if(cmbPredObr.id ==="cmbPO" && cmbPredObr.options[cmbPredObr.selectedIndex].text === 'Predio') 
                                { 
                                    if(txtTotPredObr.id === 'supTot') 
                                    { sum2 = parseInt(sum2) + parseInt(txtTotPredObr.value);}
                                }						 
                            }
                        }			   
                        document.getElementById("totObra").value = sum;	
                        document.getElementById("totPredio").value = sum2;
                    //finaliza realiza suma de total de predio y obra
			
		   });
                   cell2.appendChild(element2);	
                   
                   
                   
                 
		   
		   var cell3 = row.insertCell(2);
		   element2 = document.createElement("input");	
		   element2.type = "text";
		   element2.id = "supTot";
		   		   
		   crearEvento(element2, "change", function(){
			//inicia realiza suma de total de predio y obra	
                        var table2 = document.getElementById(tableID);
                        var rowCount2 = table.rows.length; 
                        var sum = 0;
                        var sum2 = 0;
                        for(var i=0; i<rowCount2; i++) 
                        {                                       
                            var row2 = table2.rows[i];					
                            //alert(row2.cells[2].childNodes[0]+'  i:'+i);					
                            var txtTotPredObr = row2.cells[2].childNodes[0];
                            var cmbPredObr = row2.cells[1].childNodes[0];
                            if(null !== txtTotPredObr) 
                            {
                                if(cmbPredObr.id ==="cmbPO" && cmbPredObr.options[cmbPredObr.selectedIndex].text === 'Obra') 
                                { 
                                    if(txtTotPredObr.id === 'supTot') 
                                    { sum = parseInt(sum) + parseInt(txtTotPredObr.value);}
                                }
                                if(cmbPredObr.id ==="cmbPO" && cmbPredObr.options[cmbPredObr.selectedIndex].text === 'Predio') 
                                { 
                                    if(txtTotPredObr.id === 'supTot') 
                                    { sum2 = parseInt(sum2) + parseInt(txtTotPredObr.value);}
                                }						 
                            }
                        }			   
                        document.getElementById("totObra").value = sum;	
                        document.getElementById("totPredio").value = sum2;
                    //finaliza realiza suma de total de predio y obra
			
		   });
                   cell3.appendChild(element2);			

		   
		   var cell4 = row.insertCell(3);	
		   element2 = document.createElement("input");	
                   element2.type = "Number";
		   
		   cell4.appendChild(element2);	
		   
		   var cell5 = row.insertCell(4);	
		   element2 = document.createElement("input");	
		   element2.name = "text";
		  	
		   cell5.appendChild(element2);			   
	   }	   
           if(seccion === 'hojasSeg')
	   {		   
		   var option = "";
		   var element2 = "";
		   
		   var cell2 = row.insertCell(1);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "TipoSustancia";
		   element2.add(option);
		   cell2.appendChild(element2);
		   
		   
		   var cell3 = row.insertCell(2);
		   element2 = document.createElement("input");	
		   element2.type = "file";
		   cell3.appendChild(element2);	   
		   
	   }
           
           if(seccion === 'servreq')
           {
               var option = "";
               var element2 = "";
               
               var cell2 = row.insertCell(1);
               element2 = document.createElement("select");
               option = document.createElement("option");
               option.text = "Servicio";
               element2.add(option);
               cell2.appendChild(element2);
               
               var cell3 = row.insertCell(2);
               element2 = document.createElement("select");
               option = document.createElement("option");
               option.text = "Etapa";
               element2.add(option);
               cell3.appendChild(element2);
               
               var tit = 'Si    -     ';
               var tit2 = 'No'; 
               var cell4 = row.insertCell(3);
               element2 = document.createElement("input");
               element2.type = "checkbox";
               element2.value = tit;
               cell4.appendChild(element2);
               cell4.appendChild(document.createTextNode(tit));
               
               
              var element3 = document.createElement("input");
               element3.type = "checkbox";
               element3.value = tit2;
               cell4.appendChild(element3);
               cell4.appendChild(document.createTextNode(tit2));
               
               var cell5 = row.insertCell(4);	
		   element2 = document.createElement("input");	
		   element2.name = "text";
		   cell5.appendChild(element2);
                   
               var cell6 = row.insertCell(5);
                element2 =document.createElement("input");
                element2.name = "text";
                cell6.appendChild(element2);
               
               
           }

}

function deleteRow(tableID)
{
	   try 
	   {
		   var table = document.getElementById(tableID);
		   var rowCount = table.rows.length; 
	
		   for(var i=0; i<rowCount; i++) 
		   {
				var row = table.rows[i];
				
				
				
				var chkbox = row.cells[0].childNodes[0].childNodes[0];
				if(null != chkbox && true == chkbox.checked) 
				{	//alert(i);
					 table.deleteRow(i);
					 rowCount--;
					 i--;
				}
		   }
	
	   }catch(e) { alert(e);  }
}



/*

function addRow(tableID,seccion) 
{

	   var table = document.getElementById(tableID);
	   var rowCount = table.rows.length;
	   var row = table.insertRow(rowCount);
	   var cell1 = "";
	   var element1 = "";
	   var elementAux = "";
	   var suma=0;
	   
	   elementAux = document.createElement("div");	
	   elementAux.className="contenedorMiCheck";
	   
	   cell1 = row.insertCell(0);
	   element1 = document.createElement("input");
	   element1.type = "checkbox";	
	   
	   
	   element1.className = "miCheck";  	   
	   crearEvento(element1, "click", function()
	   {	//alert("entro: "+tableID);   
		   try 
		   {  
			   var table = document.getElementById(tableID);
			   var rowCount = table.rows.length; 
				
			   for(var i=0; i<rowCount; i++) 
			   {
					var row = table.rows[i];					
					//alert(row.cells[0].childNodes[0].childNodes[0]+'  i:'+i);					
					var chkbox = row.cells[0].childNodes[0].childNodes[0];
					if(null != chkbox && true == chkbox.checked) 
					{
						 table.deleteRow(i);
						 rowCount--;
						 i--;
					}
			   }
		
		   }catch(e) { alert(e);  }	
		});
	   elementAux.appendChild(element1); 
	   cell1.appendChild(elementAux); 
	
	   
	   if(seccion == 'etapas')
	   {
		   var cell2 = row.insertCell(1); 	
		   var element2 = "";
		   
		   element2 = document.createElement("input");	
		   element2.type = "text";
		   element2.value = "Archivo";
		   element2.disabled = true;
		   cell2.appendChild(element2);	   
		   
	   }
	   if(seccion == 'sustancias')
	   {		   
		   var option = "";
		   var element2 = "";
		   
		   var cell2 = row.insertCell(1);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "TipoSustancia";
		   element2.add(option);
		   cell2.appendChild(element2);	
		   
		   var cell3 = row.insertCell(2);
		   element2 = document.createElement("input");	
		   element2.type = "text";
		   element2.value = "0";
		   cell3.appendChild(element2);
		   
		   var cell4 = row.insertCell(3);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Unidad";
		   element2.add(option);
		   cell4.appendChild(element2);	
		   
		   var cell5 = row.insertCell(4);
		   element2 = document.createElement("input");	
		   element2.type = "button";
		   element2.value = "Adjunar archivo";
		   cell5.appendChild(element2);	   
		   
	   }
	   if(seccion == 'residuos')
	   {		   
		   var option = "";
		   var element2 = "";
		   
		   var cell2 = row.insertCell(1);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Tipo contaminante";
		   element2.add(option);
		   cell2.appendChild(element2);	
		   
   	       var cell3 = row.insertCell(2);
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Etapa";
		   element2.add(option);
		   cell3.appendChild(element2);	
		   
		   var cell4 = row.insertCell(3);
		   element2 = document.createElement("input");	
		   element2.type = "text";
		   element2.value = "0";		   
		   cell4.appendChild(element2);
		   
		   var cell5 = row.insertCell(4);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Unidad";
		   element2.add(option);
		   cell5.appendChild(element2);		     
		   
		   var cell6 = row.insertCell(5);
		   element2 = document.createElement("textarea");	
		   element2.name = "txtArea";
		   element2.className = "textArea";	
		   element2.rows="1";			   
		   cell6.appendChild(element2);	  
		   
		   
	   }
	   if(seccion == 'adjuntaArch')
	   {
		   var cell2 = row.insertCell(1); 	
		   var element2 = document.createElement("input");	
		   element2.type = "text";
		   element2.value = "Archivo";
		   element2.disabled = true;
		   cell2.appendChild(element2);
	   } 
	   if(seccion == 'impactoAmbiental')
	   {		   
		   var option = "";
		   var element2 = "";
		   
		   var cell2 = row.insertCell(1);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Tipo Impacto";
		   element2.add(option);
		   cell2.appendChild(element2);	
		   
   	       var cell3 = row.insertCell(2);
		   element2 = document.createElement("textarea");	
		   cell3.appendChild(element2);	
		   
		   var cell4 = row.insertCell(3);
		   element2 = document.createElement("textarea");	   
		   cell4.appendChild(element2);
		   
		   var cell5 = row.insertCell(4);	
		   element2 = document.createElement("textarea");	
		   cell5.appendChild(element2);			   
		   
	   }
	   if(seccion == 'planosAdc')
	   { 
	   		var element2 = "";	
			
		   	var cell3 = row.insertCell(1);
		   element2 = document.createElement("textarea");	
		   element2.name = "txtArea";
		   element2.rows="1";		   
		   cell3.appendChild(element2);			   
		   
		   var cell2 = row.insertCell(2); 			   	   
		   element2 = document.createElement("input");	
		   element2.type = "file";	
		   cell2.appendChild(element2);	 	   
		   
	   }
	   if(seccion == 'superfProy')
	   {		   
		   var option = "";
		   var element2 = "";
		   
		   
		   var cell2 = row.insertCell(1);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "- Seleccione -";
		   element2.add(option);
		   option = document.createElement("option");
		   option.text = "Obra";
		   element2.add(option);		  
		   option = document.createElement("option");
		   option.text = "Predio";
		   element2.add(option);
		   cell2.appendChild(element2);	
		   
		   var cell3 = row.insertCell(2);
		   element2 = document.createElement("input");	
		   element2.type = "text";
		   element2.id = "supTot";
		   element2.value = "0";		   
		   crearEvento(element2, "change", function(){
			// Hacemos la sumatoria
			suma = document.getElementById("totObra").value;  //alert(this.value);
			suma = parseInt(suma) + parseInt(this.value);
			document.getElementById("totObra").value = suma;
			});
			cell3.appendChild(element2);			

		   
		   var cell4 = row.insertCell(3);	
		   element2 = document.createElement("select");	
		   option = document.createElement("option");
		   option.text = "Unidad";
		   element2.add(option);
		   cell4.appendChild(element2);	
		   
		   var cell5 = row.insertCell(4);	
		   element2 = document.createElement("textarea");	
		   element2.name = "txtArea";
		   element2.rows="1";	
		   cell5.appendChild(element2);			   
	   }	   
}

function deleteRow(tableID)
{
	   try 
	   {
		   var table = document.getElementById(tableID);
		   var rowCount = table.rows.length; 
	
		   for(var i=0; i<rowCount; i++) 
		   {
				var row = table.rows[i];
				
				
				
				var chkbox = row.cells[0].childNodes[0].childNodes[0];
				if(null != chkbox && true == chkbox.checked) 
				{	//alert(i);
					 table.deleteRow(i);
					 rowCount--;
					 i--;
				}
		   }
	
	   }catch(e) { alert(e);  }
}
*/




function crearEvento(elemento, evento, funcion) 
{
    if (elemento.addEventListener) 
	{ elemento.addEventListener(evento, funcion, false);} 
	else 
	{ elemento.attachEvent("on" + evento, funcion);}
}

function evaluaCuestNorma(forma,seccion)  //
{
	//div Cuestionario1
	// radio Preg1Nor116   Preg2Nor116   Preg3Nor116
	/*alert(forma.name);
	if(forma.Preg1Nor116[1].checked)
	{  alert('holaaa'); } */
	
	document.getElementById('Tabla1').style.visibility="hidden";
	document.getElementById('Tabla2').style.visibility="hidden";
	document.getElementById('Tabla3').style.visibility="hidden";
	document.getElementById('Tabla4').style.visibility="hidden";
	document.getElementById('Tabla5').style.visibility="hidden";
	document.getElementById('Tabla6').style.visibility="hidden";
	document.getElementById('Tabla7').style.visibility="hidden";
	document.getElementById('Tabla8').style.visibility="hidden";
	document.getElementById('Tabla9').style.visibility="hidden";
	document.getElementById('DescripcionParque').style.visibility="hidden";		
	document.getElementById('DescripcionOrd').style.visibility="hidden";
	document.getElementById('DescripcionDes').style.visibility="hidden";	
        document.getElementById('DescripcionDesPOET').style.visibility="hidden";
	
	//norma 116
	if(seccion === "Cuestionario1" && forma.Preg1Nor116[1].checked && forma.Preg2Nor116[1].checked && forma.Preg3Nor116[1].checked)
	{		//alert('seccion: ' + seccion + '   formaP1: ' + forma.Preg1Nor116[1].checked+'    formaP2: ' + forma.Preg2Nor116[1].checked + '   formaP3: ' + forma.Preg3Nor116[1].checked);
		document.getElementById('Tabla1').style.visibility="visible";
                //mostrar seccion de botones
                document.getElementById('Botones1').style.visibility="visible";
                document.getElementById('Botones1').style.top="-10170px";
	} 
	else if((seccion === "Cuestionario1" && forma.Preg1Nor116[0].checked) 
	    || (seccion === "Cuestionario1" && forma.Preg2Nor116[0].checked) 
		|| (seccion === "Cuestionario1" && forma.Preg3Nor116[0].checked))
	{ 
            alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)");
            
            if (confirm("¿Desea abandonar la captura?")) 
            { //respuesta afirmativa, cerrar aplicación
                //alert('cierra aplicaion');
                               
                 this.window.close();
                //return true ;
            }
            else 
            { //respuesta negativa, continua con el llenado del IP
                //return false ;
                //alert('continua captura IP');
            }

        
        }
	//norma 120
	if(seccion === "Cuestionario2")
	{	
		document.getElementById('NomCatCli').style.visibility="hidden";
                document.getElementById('NomCatCli2').style.visibility="hidden";
		document.getElementById('TipoDeClimas').style.visibility="hidden";
		document.getElementById('TipoDeVegetacion').style.visibility="hidden";		
		document.getElementById('ResSIPreg4Nor120').style.visibility="hidden";
		document.getElementById('ResNOPreg4Nor120').style.visibility="hidden";
		document.getElementById('NomPreg4Nor120').style.visibility="hidden";
		document.getElementById('Preg4Nor120').style.visibility="hidden";		
		forma.Preg4Nor120[0].style.visibility="hidden";
		forma.Preg4Nor120[1].style.visibility="hidden";
		document.getElementById('NomCatNor').style.visibility="hidden";
		document.getElementById('CatalogoDeLeyesMineras').style.visibility="hidden";
		document.getElementById('Tabla2').style.visibility="hidden";
	
		if(forma.Preg1Nor120[1].checked) //no a pregunta  ?Es zona agr?cola o ganadera? de norma 120
		{
			document.getElementById('NomCatCli').style.visibility="visible";
                        document.getElementById('NomCatCli2').style.visibility="visible";
			document.getElementById('TipoDeClimas').style.visibility="visible";
			document.getElementById('TipoDeVegetacion').style.visibility="visible";
			
			// sino no se leccionana nada de los cambos, se procede a IP
			if(forma.TipoDeClimas.options[forma.TipoDeClimas.selectedIndex].text === "Otro" && forma.TipoDeVegetacion.options[forma.TipoDeVegetacion.selectedIndex].text === "Otro")
			{
				document.getElementById('NomPreg4Nor120').style.visibility="visible";	
				forma.Preg4Nor120[0].style.visibility="visible";
				forma.Preg4Nor120[1].style.visibility="visible";
				document.getElementById('ResSIPreg4Nor120').style.visibility="visible";				
				document.getElementById('ResNOPreg4Nor120').style.visibility="visible";		
				document.getElementById('Preg4Nor120').style.visibility="visible";
				
				if (forma.Preg4Nor120[1].checked) // si selecciona no a  pregunta ?Se trata de minerales reservados a la federaci?n? de norna 120
				{
					document.getElementById('NomCatNor').style.visibility="visible";
					document.getElementById('CatalogoDeLeyesMineras').style.visibility="visible";  //Ninguno
					//document.getElementById('Tabla2').style.visibility="visible";
					
					
                                        if(forma.CatalogoDeLeyesMineras.options[forma.CatalogoDeLeyesMineras.selectedIndex].text === "- Seleccione -")
                                        { 
                                            document.getElementById('Tabla2').style.visibility="hidden";
                                            document.getElementById('Botones1').style.visibility="hidden";
					}
                                        else if(forma.CatalogoDeLeyesMineras.options[forma.CatalogoDeLeyesMineras.selectedIndex].text === "Otro")
					{						
						
                                                document.getElementById('Tabla2').style.visibility="visible";
                                                //mostrar seccion de botones
                                                document.getElementById('Botones1').style.visibility="visible";
                                                document.getElementById('Botones1').style.top="-9940px";
					}
					else if(forma.CatalogoDeLeyesMineras.options[forma.CatalogoDeLeyesMineras.selectedIndex].text !== "Otro" || forma.CatalogoDeLeyesMineras.options[forma.CatalogoDeLeyesMineras.selectedIndex].text !== "- Seleccione -")
					{
						alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)");
                                                if (confirm("¿Desea abandonar la captura?")) 
                                                { }
                                                else 
                                                { }
					}
				}
				if (forma.Preg4Nor120[0].checked) // si selecciona si a  pregunta ?Se trata de minerales reservados a la federaci?n? de norna 120
				{
					alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
                                        document.getElementById('Tabla2').style.visibility="hidden";
                                        //mostrar seccion de botones
                                        document.getElementById('Botones1').style.visibility="hidden";
                                        alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)");
                                        if (confirm("¿Desea abandonar la captura?")) 
                                        { }
                                        else 
                                        { }
					
				}
				
			}
			//de lo contrario pocede MIA
			if((forma.TipoDeClimas.options[forma.TipoDeClimas.selectedIndex].text === "Otro" && forma.TipoDeVegetacion.options[forma.TipoDeVegetacion.selectedIndex].text !== "Otro") 
				|| (forma.TipoDeClimas.options[forma.TipoDeClimas.selectedIndex].text !== "Otro" && forma.TipoDeVegetacion.options[forma.TipoDeVegetacion.selectedIndex].text === "Otro")
                                || (forma.TipoDeClimas.options[forma.TipoDeClimas.selectedIndex].text !== "Otro" && forma.TipoDeClimas.options[forma.TipoDeClimas.selectedIndex].text !== "- Seleccione -" && forma.TipoDeVegetacion.options[forma.TipoDeVegetacion.selectedIndex].text !== "Otro" && forma.TipoDeVegetacion.options[forma.TipoDeVegetacion.selectedIndex].text !== "- Seleccione -"))
			{
				alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
                                if (confirm("¿Desea abandonar la captura?")) 
                                { }
                                else 
                                { }
			}
			
		}
		else if(forma.Preg1Nor120[0].checked) //si a pregunta  ?Es zona agr?cola o ganadera? de norma 120
		{	
			alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
                        if (confirm("¿Desea abandonar la captura?")) 
                        { }
                        else 
                        { }
		}	
	}
	//norma 129
	if(seccion === "Cuestionario3" && forma.Preg1Nor129[1].checked && forma.Preg2Nor129[1].checked && forma.Preg3Nor129[1].checked)
	{		//alert('seccion: ' + seccion + '   formaP1: ' + forma.Preg1Nor116[1].checked+'    formaP2: ' + forma.Preg2Nor116[1].checked + '   formaP3: ' + forma.Preg3Nor116[1].checked);
		document.getElementById('Tabla3').style.visibility="visible";
                //mostrar seccion de botones
                document.getElementById('Botones1').style.visibility="visible";
                document.getElementById('Botones1').style.top="-10050px";
		
	} 
	if((seccion === "Cuestionario3" && forma.Preg1Nor129[0].checked) 
	    || (seccion === "Cuestionario3" && forma.Preg2Nor129[0].checked) 
		|| (seccion === "Cuestionario3" && forma.Preg3Nor129[0].checkede))
	{ 
            alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
            if (confirm("¿Desea abandonar la captura?")) 
            { }
            else 
            { }
        }
	
        
        //norma 130
	if(seccion === "Cuestionario4")
	{	
	
		document.getElementById('NomPreg2Nor130').style.visibility="hidden";
		document.getElementById('ResSIPreg2Nor130').style.visibility="hidden";
		document.getElementById('ResNOPreg2Nor130').style.visibility="hidden";		
		document.getElementById('Preg2Nor130').style.visibility="hidden";
		
		document.getElementById('Tabla4').style.visibility="hidden";
		forma.Preg2Nor130[0].style.visibility="hidden";
		forma.Preg2Nor130[1].style.visibility="hidden";
		
		if(forma.Preg1Nor130[1].checked) //no a pregunta   �Se encuentra dentro de un �rea neutral protegida? norma 130
		{						
				document.getElementById('NomPreg2Nor130').style.visibility="visible";	
				forma.Preg2Nor130[0].style.visibility="visible";
				forma.Preg2Nor130[1].style.visibility="visible";
				document.getElementById('ResSIPreg2Nor130').style.visibility="visible";				
				document.getElementById('ResNOPreg2Nor130').style.visibility="visible";		
				
				
				if (forma.Preg2Nor130[0].checked) // si selecciona si a  pregunta Requiere de cambio de uso de suelo de áreas forestales de norna 130
				{					
                                    alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
                                    if (confirm("¿Desea abandonar la captura?")) 
                                    { }
                                    else 
                                    { }
					
				}
				if (forma.Preg2Nor130[1].checked) // si selecciona no a  pregunta �Se trata de minerales reservados a la federaci�n? de norna 120
				{
                                    document.getElementById('Tabla4').style.visibility="visible";
                                    //mostrar seccion de botones
                                    document.getElementById('Botones1').style.visibility="visible";
                                    document.getElementById('Botones1').style.top="-10230px";
					
				}
			
		}
		else if(forma.Preg1Nor130[0].checked) //si a pregunta de norma 130
		{
			alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)");
                        if (confirm("¿Desea abandonar la captura?")) 
                        { }
                        else 
                        { }
		}	
	}
        
	//norma 141
	if(seccion === "Cuestionario5")
	{	
	
		document.getElementById('NomPreg2Nor141').style.visibility="hidden";
		document.getElementById('ResSIPreg2Nor141').style.visibility="hidden";
		document.getElementById('ResNOPreg2Nor141').style.visibility="hidden";		
		document.getElementById('Preg2Nor141').style.visibility="hidden";
		forma.Preg2Nor141[0].style.visibility="hidden";
		forma.Preg2Nor141[1].style.visibility="hidden";
		document.getElementById('NomCatNor141').style.visibility="hidden";
		document.getElementById('CatalogoDeLeyesMineras141').style.visibility="hidden";
		document.getElementById('Tabla5').style.visibility="hidden";
	
		if(forma.Preg1Nor141[1].checked) //no a pregunta   �Se encuentra dentro de un �rea neutral protegida? norma 141
		{						
				document.getElementById('NomPreg2Nor141').style.visibility="visible";	
				forma.Preg2Nor141[0].style.visibility="visible";
				forma.Preg2Nor141[1].style.visibility="visible";
				document.getElementById('ResSIPreg2Nor141').style.visibility="visible";				
				document.getElementById('ResNOPreg2Nor141').style.visibility="visible";		
				
				
				if (forma.Preg2Nor141[0].checked) // si selecciona si a  pregunta �Se trata de minerales reservados a la federaci�n? de norna 120
				{			
					document.getElementById('NomCatNor141').style.visibility="visible";
					document.getElementById('CatalogoDeLeyesMineras141').style.visibility="visible";  //Ninguno					
					
                                        if(forma.CatalogoDeLeyesMineras141.options[forma.CatalogoDeLeyesMineras141.selectedIndex].text === "- Seleccione -")
                                        {
                                            document.getElementById('Tabla5').style.visibility="hidden";
                                            //mostrar seccion de botones
                                            document.getElementById('Botones1').style.visibility="hidden";							
					}
                                        else if(forma.CatalogoDeLeyesMineras141.options[forma.CatalogoDeLeyesMineras141.selectedIndex].text === "Otro")
					{					
                                            document.getElementById('Tabla5').style.visibility="visible";
                                            //mostrar seccion de botones
                                            document.getElementById('Botones1').style.visibility="visible";
                                            document.getElementById('Botones1').style.top="-10130px"; 
					}
					else
					{
                                            document.getElementById('Tabla5').style.visibility="hidden";
                                            //mostrar seccion de botones
                                            document.getElementById('Botones1').style.visibility="hidden";
                                            alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
                                            if (confirm("¿Desea abandonar la captura?")) 
                                            { }
                                            else 
                                            { }
					}
				}
				if (forma.Preg2Nor141[1].checked) // si selecciona no a  pregunta �Se trata de minerales reservados a la federaci�n? de norna 120
				{
					document.getElementById('Tabla5').style.visibility="visible";
                                        //mostrar seccion de botones
                                        document.getElementById('Botones1').style.visibility="visible";
                                        document.getElementById('Botones1').style.top="-10130px";
					
				}
			
		}
		else if(forma.Preg1Nor141[0].checked) //si a pregunta  �Es zona agr�cola o ganadera? de norma 120
		{
			alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
                        if (confirm("¿Desea abandonar la captura?")) 
                        { }
                        else 
                        { }
		}	
	}
	
         //norma 149
	if(seccion === "Cuestionario6")
	{		
            document.getElementById('Tabla6').style.visibility="hidden";

            if(forma.Preg1Nor149[1].checked) //no a pregunta   �Se encuentra dentro de un �rea neutral protegida? norma 149
            {						
                document.getElementById('Tabla6').style.visibility="visible";	
                //mostrar seccion de botones
                document.getElementById('Botones1').style.visibility="visible";
                document.getElementById('Botones1').style.top="-10300px";
            }
            else if(forma.Preg1Nor149[0].checked) //si a pregunta de norma 130
            {
                alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
                if (confirm("¿Desea abandonar la captura?")) 
                { }
                else 
                { }
            }	
	}
    
        //norma 150
	if(seccion === "Cuestionario7")
	{	
		document.getElementById('NomCatCli150').style.visibility="hidden";
                document.getElementById('NomCatCli150_2').style.visibility="hidden";
		document.getElementById('TipoDeClimas150').style.visibility="hidden";
		document.getElementById('TipoDeVegetacion150').style.visibility="hidden";		
		document.getElementById('ResSIPreg4Nor150').style.visibility="hidden";
		document.getElementById('ResNOPreg4Nor150').style.visibility="hidden";
		document.getElementById('NomPreg4Nor150').style.visibility="hidden";
		document.getElementById('Preg4Nor150').style.visibility="hidden";		
		forma.Preg4Nor150[0].style.visibility="hidden";
		forma.Preg4Nor150[1].style.visibility="hidden";
		
		
		document.getElementById('ResSIPreg5Nor150').style.visibility="hidden";
		document.getElementById('ResNOPreg5Nor150').style.visibility="hidden";
		document.getElementById('NomPreg5Nor150').style.visibility="hidden";
		document.getElementById('Preg5Nor150').style.visibility="hidden";	
		forma.Preg5Nor150[0].style.visibility="hidden";
		forma.Preg5Nor150[1].style.visibility="hidden";
		document.getElementById('Tabla7').style.visibility="hidden";
	
		if(forma.Preg1Nor150[1].checked) //no a pregunta  �Es zona agr�cola o ganadera? de norma 150
		{
			document.getElementById('NomCatCli150').style.visibility="visible";
                        document.getElementById('NomCatCli150_2').style.visibility="visible";
			document.getElementById('TipoDeClimas150').style.visibility="visible";
			document.getElementById('TipoDeVegetacion150').style.visibility="visible";
			
			// sino no se leccionana nada de los cambos, se procede a IP
			if(forma.TipoDeClimas150.options[forma.TipoDeClimas150.selectedIndex].text === "Otro" && forma.TipoDeVegetacion150.options[forma.TipoDeVegetacion150.selectedIndex].text === "Otro")
			{
				document.getElementById('NomPreg4Nor150').style.visibility="visible";	
				forma.Preg4Nor150[0].style.visibility="visible";
				forma.Preg4Nor150[1].style.visibility="visible";
				document.getElementById('ResSIPreg4Nor150').style.visibility="visible";				
				document.getElementById('ResNOPreg4Nor150').style.visibility="visible";		
				document.getElementById('NomPreg5Nor150').style.visibility="visible";	
				forma.Preg5Nor150[0].style.visibility="visible";
				forma.Preg5Nor150[1].style.visibility="visible";
				document.getElementById('ResSIPreg5Nor150').style.visibility="visible";				
				document.getElementById('ResNOPreg5Nor150').style.visibility="visible";	
				
				if (forma.Preg4Nor150[0].checked || forma.Preg5Nor150[0].checked) // si selecciona si a pregunta �Se trata de un �rea natural protegida? de norna 150
				{						
						alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 		
						document.getElementById('Tabla7').style.visibility="hidden";
                                                if (confirm("¿Desea abandonar la captura?")) 
                                                { }
                                                else 
                                                { }
				}
				if (forma.Preg4Nor150[1].checked &&  forma.Preg5Nor150[1].checked) // si selecciona no a  pregunta �Se trata de un �rea natural protegida? de norna 150
				{
					document.getElementById('Tabla7').style.visibility="visible";
                                        //mostrar seccion de botones
                                        document.getElementById('Botones1').style.visibility="visible";
                                        document.getElementById('Botones1').style.top="-9950px";					
				}
				
			}
			//de lo contrario pocede MIA
                        if((forma.TipoDeClimas150.options[forma.TipoDeClimas150.selectedIndex].text === "Otro" && forma.TipoDeVegetacion150.options[forma.TipoDeVegetacion150.selectedIndex].text !== "Otro") 
				|| (forma.TipoDeClimas150.options[forma.TipoDeClimas150.selectedIndex].text !== "Otro" && forma.TipoDeVegetacion150.options[forma.TipoDeVegetacion150.selectedIndex].text === "Otro")
                                || (forma.TipoDeClimas150.options[forma.TipoDeClimas150.selectedIndex].text !== "Otro" && forma.TipoDeClimas150.options[forma.TipoDeClimas150.selectedIndex].text !== "- Seleccione -" && forma.TipoDeVegetacion150.options[forma.TipoDeVegetacion150.selectedIndex].text !== "Otro" && forma.TipoDeVegetacion150.options[forma.TipoDeVegetacion150.selectedIndex].text !== "- Seleccione -"))
			{
				alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
                                if (confirm("¿Desea abandonar la captura?")) 
                                { }
                                else 
                                { }
			}
		}
		else if(forma.Preg1Nor150[0].checked) //si a pregunta  �Es zona agr�cola o ganadera? de norma 120
		{
			alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
                        if (confirm("¿Desea abandonar la captura?")) 
                        { }
                        else 
                        { }
		}	
	}
	
	//norma 155
	if(seccion === "Cuestionario8")
	{	
	
		document.getElementById('NomPreg2Nor155').style.visibility="hidden";
		document.getElementById('ResSIPreg2Nor155').style.visibility="hidden";
		document.getElementById('ResNOPreg2Nor155').style.visibility="hidden";		
		document.getElementById('Preg2Nor155').style.visibility="hidden";	
		document.getElementById('Tabla8').style.visibility="hidden";
		forma.Preg2Nor155[0].style.visibility="hidden";
		forma.Preg2Nor155[1].style.visibility="hidden";
		
		if(forma.Preg1Nor155[1].checked) //no a pregunta   �Se encuentra dentro de un �rea neutral protegida? norma 155
		{						
				document.getElementById('NomPreg2Nor155').style.visibility="visible";	
				forma.Preg2Nor155[0].style.visibility="visible";
				forma.Preg2Nor155[1].style.visibility="visible";
				document.getElementById('ResSIPreg2Nor155').style.visibility="visible";				
				document.getElementById('ResNOPreg2Nor155').style.visibility="visible";		
				
				
				if (forma.Preg2Nor155[0].checked) // si selecciona si a  pregunta �Se trata de minerales reservados a la federaci�n? de norna 120
				{
                                    document.getElementById('Tabla8').style.visibility="visible";
                                    //mostrar seccion de botones
                                    document.getElementById('Botones1').style.visibility="visible";
                                    document.getElementById('Botones1').style.top="-10250px";					
				}
				if (forma.Preg2Nor155[1].checked) // si selecciona no a  pregunta �Se trata de minerales reservados a la federaci�n? de norna 120
				{
					document.getElementById('Tabla8').style.visibility="hidden";
                                        //mostrar seccion de botones
                                        document.getElementById('Botones1').style.visibility="hidden";
                                        
                                        alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
                                        if (confirm("¿Desea abandonar la captura?")) 
                                        { }
                                        else 
                                        { }
				}
			
		}
		else if(forma.Preg1Nor155[0].checked) //si a pregunta  �Es zona agr�cola o ganadera? de norma 120
		{
			alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
                        if (confirm("¿Desea abandonar la captura?")) 
                        { }
                        else 
                        { }
		}	
	}
	
	//norma 159
	if(seccion === "Cuestionario9")
	{	
	
		document.getElementById('NomPreg2Nor159').style.visibility="hidden";
		document.getElementById('ResSIPreg2Nor159').style.visibility="hidden";
		document.getElementById('ResNOPreg2Nor159').style.visibility="hidden";		
		document.getElementById('Preg2Nor159').style.visibility="hidden";		
		forma.Preg2Nor159[0].style.visibility="hidden";
		forma.Preg2Nor159[1].style.visibility="hidden";
		document.getElementById('Tabla9').style.visibility="hidden";
	
		if(forma.Preg1Nor159[1].checked) //no a pregunta   �Se encuentra dentro de un �rea neutral protegida? norma 159
		{						
				document.getElementById('NomPreg2Nor159').style.visibility="visible";	
				forma.Preg2Nor159[0].style.visibility="visible";
				forma.Preg2Nor159[1].style.visibility="visible";
				document.getElementById('ResSIPreg2Nor159').style.visibility="visible";				
				document.getElementById('ResNOPreg2Nor159').style.visibility="visible";		
				
				
				if (forma.Preg2Nor159[0].checked) // si selecciona si a  pregunta �Se trata de minerales reservados a la federaci�n? de norna 120
				{	
                                    document.getElementById('Tabla9').style.visibility="visible";
                                    //mostrar seccion de botones
                                    document.getElementById('Botones1').style.visibility="visible";
                                    document.getElementById('Botones1').style.top="-10250px";
				}
				if (forma.Preg2Nor159[1].checked) // si selecciona no a  pregunta �Se trata de minerales reservados a la federaci�n? de norna 120
				{
                                    document.getElementById('Tabla9').style.visibility="hidden";
                                    //mostrar seccion de botones
                                    document.getElementById('Botones1').style.visibility="hidden";
                                    alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 	
                                    if (confirm("¿Desea abandonar la captura?")) 
                                    { }
                                    else 
                                    { }
				}
			
		}
		else if(forma.Preg1Nor159[0].checked) //si a pregunta  �Es zona agr�cola o ganadera? de norma 120
		{
			alert("Debe presentar Manifestacion de Impacto Ambietal (MIA)"); 
                        if (confirm("¿Desea abandonar la captura?")) 
                        { }
                        else 
                        { }
		}	
	}
	
	
	
	
	
}

function borrado()
{
    if (confirm("Esta usted seguro de borrar el registro?"))
  { return true; }
  else{ return false; }	  
}

var lstUbicaciones = null;

function getInformacionAdicionalMulti(obj, frmNombre, cadena) {
    //Separamos por secciones
    var aSecciones = cadena.split("|");
    var aSeccion, aUbica;
    var idx;
    var strSeccion = "";
    
    lstUbicaciones = [];
    
    for (idx = 0; idx < aSecciones.length; idx++) {
        //Separamos los identificadores del titulo
        aSeccion = aSecciones[idx].split("-");
        aUbica = aSeccion[0].split(",");
        
        //Genera listado de ubicaciones
        lstUbicaciones.push(aUbica[0] + "-" + aUbica[1] + "-" + aUbica[2]);
        
        strSeccion = idx + 1;
        getInformacionAdicionalSeccion(obj, frmNombre, aUbica[0], aUbica[1], aUbica[2], aSeccion[1], strSeccion);
    }
}

function getInformacionAdicional(obj, frmNombre, subCapitulo, seccion, apartado, leyenda) {
	// console.log('getInformacionAdicional('+obj+', '+frmNombre+', '+subCapitulo+', '+seccion+', '+apartado+', '+leyenda+')');
    getInformacionAdicionalSeccion(obj, frmNombre, subCapitulo, seccion, apartado, leyenda, "");
}

function getInformacionAdicionalSeccion(obj, frmNombre, subCapitulo, seccion, apartado, leyenda, numSeccion) {
    var strObj = "" + obj;
    var aInfo = strObj.split('|');
    var bInfoAdic, bInfoOpc;
    var leyendaInfo, color;
    var comentario;
    var ubicacion = subCapitulo + "-" + seccion + "-" + apartado;
    var aComent = "";

    var serial = aInfo[0];

    for (var idx = 1; idx < aInfo.length; idx++ ) {
        comentario = aInfo[idx];
        aComent = comentario.split('_');

        if (ubicacion === aComent[1]) {
            if ( aComent[0] === '1') {
                bInfoAdic = true;
                bInfoOpc = false;
                leyendaInfo = "<div style='color: red'>Requiere<br/>Información Adicional</div>";
            } else if ( aComent[0] === '2') {
                bInfoAdic = false;
                bInfoOpc = true;
                leyendaInfo = "<div style='color: blue'>Sección modificada</div>";
            }else if ( aComent[0] === '3') {
                bInfoAdic = true;
                bInfoOpc = true;
                leyendaInfo = "<div style='color: red'>Requiere Información Adicional</div><div style='color: blue'>Sección modificada</div>";
            }
            break;
        }
    }

    document.getElementById(frmNombre + ":ubicActual").value = ubicacion;
    remoteUbicActual();
    
    var titulo = "";
    if (serial === "3") { //Solo es posible tener información adicional con serial 3
        if (bInfoAdic || bInfoOpc) {
            titulo =
                "	<table width='95%' " + 
                "		   style='border-style: solid; border-color: #B2CEDE; border-style: solid; border-width: 1px;'>" + 
                "		<tr> " + 
                "			<td align='left' width='75%'> " + 
                "				<h3>&nbsp;" + leyenda + "</h3> " + 
                "			</td> " + 
                "			<td align='center'  width='55%' style='background: #B2CEDE'>" + leyendaInfo + "</td> " + 
                "		</tr> " + 
                "	</table><br/>";
            //El componente pe:ckEditor no carga correctamente después del onload si no está en el DOM, 
            //por lo que solo se puede ocultar, no quitar del DOM
            //aunque tambien se puede ocultar después de que la página ha sido cargada
            //Si esta visible es porque no puede quitarse del DOM
            if (document.getElementById("componenteCaptura" + numSeccion) !== null) {
                if (document.getElementById("componenteCaptura" + numSeccion).style.display === "none" ) {
                   document.getElementById("componenteCaptura" + numSeccion).style.display="inline";
                } else {
                   document.getElementById("componenteCaptura" + numSeccion).style.visibility="visible";
                }
            }
            if ( document.getElementById("componenteGuarda") !== null ) {
                document.getElementById("componenteGuarda").style.display="inline";
            }
            if ( document.getElementById("componenteConsulta" + numSeccion) !== null) {
                document.getElementById("componenteConsulta" + numSeccion).style.display="none";
            }
            if ( document.getElementById("componenteActualiza" + numSeccion) !== null ) {
                document.getElementById("componenteActualiza" + numSeccion).style.display="none";
            }

        } else {
            titulo = "<h3>" + leyenda + "</h3> ";
            if ( document.getElementById("componenteCaptura" + numSeccion) !== null ) {
                document.getElementById("componenteCaptura" + numSeccion).style.display="none";
            }
            
//            if ( document.getElementById("componenteGuarda") !== null ) {
//                document.getElementById("componenteGuarda").style.display="none";
//            }
            
            if ( document.getElementById("componenteConsulta" + numSeccion) !== null ) {
                document.getElementById("componenteConsulta" + numSeccion).style.display="inline";
            }
            if ( document.getElementById("componenteActualiza" + numSeccion) !== null ) {
                document.getElementById("componenteActualiza" + numSeccion).style.display="inline";
            }
        }
    } else {
        titulo = "<h3>" + leyenda + "</h3> ";
        //Si esta visible es porque no puede quitarse del DOM
        if ( document.getElementById("componenteCaptura" + numSeccion) !== null ) {
            if (document.getElementById("componenteCaptura" + numSeccion).style.display === "none" ) {
               document.getElementById("componenteCaptura" + numSeccion).style.display = "inline";
            } else {
               document.getElementById("componenteCaptura" + numSeccion).style.visibility = "visible";
            }
        }
        if ( document.getElementById("componenteGuarda") !== null ) {
            document.getElementById("componenteGuarda").style.display="inline";
        }
        if ( document.getElementById("componenteConsulta" + numSeccion) !== null ) {
            document.getElementById("componenteConsulta" + numSeccion).style.display="none";
        }
        if ( document.getElementById("componenteActualiza" + numSeccion) !== null ) {
            document.getElementById("componenteActualiza" + numSeccion).style.display="none";
        }
    }

    if ( document.getElementById("titulo" + numSeccion) !== null ) {
        document.getElementById("titulo" + numSeccion).innerHTML = titulo;
    }
        
    //document.getElementById("foDatosGen:componenteCaptura").disabled = true;
    //alert(aInfo + "|" + bRes);
    //return bRes;
}   

function actualizaComponentesModifica(frmNombre) {
    actualizaComponentesModificaMulti(frmNombre, "");
}

function actualizaComponentesModificaMulti(frmNombre, numSeccion) {
    alert("Cuando finalice su edición e indique guardar, la información que actualice reemplazará la información que se encuentra almacenada.");
    document.getElementById("componenteConsulta" + numSeccion).style.display="none";
    document.getElementById("componenteActualiza" + numSeccion).style.display="none";
    document.getElementById("componenteCaptura" + numSeccion).style.display="inline";
    document.getElementById("componenteCaptura" + numSeccion).style.visibility = "visible";
    if ( document.getElementById("componenteGuarda") !== null ) {
        document.getElementById("componenteGuarda").style.display="inline";  
    }
    
    //Se ha solicitado cambiar algo que no requería información adicional
    if (numSeccion === "") {
        //Un componente por pantalla
        document.getElementById(frmNombre + ":strInfoAdicional").value = 
                 document.getElementById(frmNombre + ":strInfoAdicional").value + "|" +
                 document.getElementById(frmNombre + ":ubicActual").value;        
    } else {
        //Múltiples componentes por pantalla
        var ubicActual = lstUbicaciones[parseInt(numSeccion, 10) - 1];
        document.getElementById(frmNombre + ":strInfoAdicional").value = 
                document.getElementById(frmNombre + ":strInfoAdicional").value + "|" +
                ubicActual;
    }
    remoteUpdateInfoAdicional();
}

function capturaCantidades(event) {
    //alert(event.which);
    if(event.which === 8 || event.which === 0) return true;
    if (event.which === ".".charCodeAt(0)) return true;
    if (event.which < 48 || event.which > 57) return false;
    
}

function capturaEnteros(event) {
    if(event.which === 8 || event.which === 0) return true;
    if (event.which < 48 || event.which > 57) return false;
}
