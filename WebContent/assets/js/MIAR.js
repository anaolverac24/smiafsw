/*
 * 
 * Dependiendo del valor de tipoTramite se cambia el titulo del encabezado y en dado caso que el tramite no este
 * en la vista VEXDATOSTRAMITE manda un mensaje de TRAMITE NO RECONOCIDO el no afecta el funcionamiento y permite
 * seguir capturando datos
 * 
 * MP = MIA PARTICULAR MOD A, NO INCLUYE RIESGO
 * DM = MIA PARTICULAR MOD B, INCLUYE RIESGO
 * MG = MIA REGIONAL MOD B, INCLUYE RIESGO
 * DL = MIA REGIONAL MOD B, INCLUYE RIESGO
 * 
 * para las opciones en donde se mencione que incluye riesgo el Estudio de riesgo tiene que aparecer inmediantamente 
 * y exclusivamente para MG y DL tiene que aparecer la pagina justificacion 
 * 
 */



function titulo (e){
       //AAOC 12/08 Se cambian todos los titulos a minusculas
       switch(e){
                            case "MP":
                                //alert("MIA PARTICULAR MOD A, NO INCLUYE RIESGO");
                                //document.getElementById('tipoTram').innerHTML = "mia particular mod a, no incluye riesgo";
                               break;
                            case "DM":
                                //alert("MIA PARTICULAR MOD B,  INCLUYE RIESGO");
                                //document.getElementById('tipoTram').innerHTML = "mia particular mod b, incluye riesgo";
                                $("#ER").show();
                                
                                break;
                            case "MG":
                                //alert("MIA REGIONAL MOD A, NO INCLUYE RIESGO");
                                //document.getElementById('tipoTram').innerHTML = "mia regional mod a, no incluye riesgo";
                                $("#justificacion").show();
                                $("#otrosIns").show();
                                break;
                            case "DL":
                                //alert("MIA REGIONAL MOD B,  INCLUYE RIESGO");
                                //document.getElementById('tipoTram').innerHTML = "mia regional mod b, incluye riesgo";
                                $("#ER").show();
                                $("#justificacion").show();
                                $("#otrosIns").show();
                                break;
                            default :
                                //document.getElementById('tipoTram').innerHTML = "trámite no reconocido";
                                alert("TRAMITE NO RECONOCIDO " + tipoTramite);
                    }
    
    
    
}

function mensajes(msg, tipo){
	// alert(msg + ' ' + tipo);	
	$('#mensaje').append('<div class="alert alert-'+ tipo +' alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>'+msg+'</div>');			    
}

function mensajeSigeia(estados){ 
	if(estados==0){
		mensajes('Debe georreferenciar correctamente su proyecto','danger');
	}
}

function listaGuias(guias){
	var count=1;
	if(guias.length!=0){
		for(i in guias){			
			$('#utilidades').append('<li><a href="'+ guias[i] +'" target="_blank"><span class="glyphicon glyphicon-info-sign"></span><small> Guia '+ count + '</small></a></li>');
			count++;
		}
	}
}

/**
 * Permite validar los componentes del nombre y descripción del archivo anexo
 */
//function validarArchivosAnexos() {
//	var nombreArchivo = $("#formArchivosDisposicion\\:txtNombreArchivo").val();
//	var descripcionArchivo = $("#formArchivosDisposicion\\:txtDescripcionArchivo").val();
//	var archivoCargado = $("#formArchivosDisposicion\\:fileAnexos").val();
//	
//	if (nombreArchivo == ""  || descripcionArchivo == "" || archivoCargado == "") {
//		if (nombreArchivo == "") {
//			$('.textoNombreArchivo').remove();			
//	    	$('#mensajeNombre').append('<span class="textoNombreArchivo" style="color: red">El campo es requerido</span>');			    		
//		} else {
//			$('.textoNombreArchivo').remove();			
//		}
//		
//		if (descripcionArchivo == "") {
//			$('.textoDescripcionArchivo').remove();
//			$('#mensajeDescripcion').append('<span class="textoDescripcionArchivo" style="color: red">El campo es requerido</span>');			    				
//		} else{
//			$('.textoDescripcionArchivo').remove();
//		}
//		
//		if(archivoCargado == "") {
//			$('.textoArchivoAnexo').remove();
//			$('#mensajeArchivoAnexo').append('<span class="textoArchivoAnexo" style="color: red">El campo es requerido</span>');			
//		} else {
//			$('.textoArchivoAnexo').remove();			
//		}
//		
////		return;
//	} else{
//		$('#formArchivosDisposicion\\:mensajeNombre').hide();
//		$('#formArchivosDisposicion\\:mensajeDescripcion').hide();
//	}
//}