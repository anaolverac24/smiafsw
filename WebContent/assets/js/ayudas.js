/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function case_ayuda(e) {
    var o = {
        left: e.pageX + 14,
        top: e.pageY
    };
    
    $("#case_pnl_ayuda").show().offset(o);
    var rutaAyuda = window.location.pathname.substring(0, window.location.pathname.indexOf("/",2)) + "/Ayuda";
    
    $.get(rutaAyuda,
            {   cTramite: e.data.vClaveTramite,
                nsec: e.data.nsub,
                capitulo: e.data.capitulo,
                subcapitulo: e.data.subcapitulo,
                seccion: e.data.seccion,
                apartado: e.data.apartado},
    function(data) {
        $('#case_pnl_ayuda_cnt').html('<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button><h1 ><span class="icon-infocircle pull-left" style="padding-right:5px;"></span></h1><small class="text-left">'+data+'<small></div>');
    }
    );
}

$(document).ready(function() {
    $("#case_pnl_ayuda").click(function(e) {
    $("#case_pnl_ayuda").html('<div class="semarnat_ayuda_cont" id="case_pnl_ayuda_cnt"></div>');
    $("#case_pnl_ayuda").hide();
    });


});
        